# Módulo de traducción del compendio Pathfinder 2 para VTT de Foundry

Este repositorio es un módulo destinado a Foundry VTT (https://foundryvtt.com/).  

Este módulo ofrece una traducción al español de los datos de los objetos contenidos en los paquetes del sistema Foundry. Permiten al usuario jugar en Foundry con el sistema Pathfinder2, el juego de rol desarrollado y comercializado por Paizo. 

Permite al usuario traducir al español la descripción de los "artículos/objetos" contenidos en los packs de los compendios ingleses. 

También permite visualizar el nombre de cada artículo en vo-ve (versión original - versión español) o ve-vo (versión español - versión original) con la descripción en ve para facilitar la búsqueda, la toma en consideración de palabras clave y jugar en partidas que utilizan el español y el inglés.  

El módulo hace imprescindible el uso de los módulos  
1. Babele, este módulo y sus propias dependencias son necesarios para asegurar el soporte de traducción y sustitución cuando se muestra en el juego. Está desarrollado por _Simone Riccisi_.
2. Sistema PF2e, este módulo es el que asegura que Foundry se ocupe del propio sistema de juego con sus reglas y automatizaciones. Es necesario cargar los paquetes de compendios de datos del sistema de juego que son traducidos por este módulo.
3. PF2e companion compendia, este módulo es necesario para cargar los paquetes de compendios de datos específicos de los diferentes compañeros (la criatura del inventor, el compañero animal del druida o del explorador, el eidolon del conjurador, los compañeros no-muertos...). Se ha desarrollado por separado y probablemente será devorado por el sistema algún día.

El módulo sólo admite la traducción de compendios de datos. Se trata de un módulo complementario que te permite disponer de la versión española de la interfaz de usuario del sistema PF2e y, más concretamente, de la hoja de personaje en español y de los mensajes que aparecen en español en la interfaz y en la conversación cuando interactúas con el juego. **La interfaz de usuario es objeto de un módulo aparte**.

Esta dualidad permite a los hipanoparlantes jugar con los datos en V.O. mientras tienen la interfaz del juego en español o viceversa.

**En ningún caso Paizo o Devir reconocen, originan o patrocinan este módulo o proyecto.

La gama Pathfinder es una creación de **Paizo Publishing**. Está publicado oficialmente en español por **Devir Iberia**.

Este módulo está producido bajo la Licencia de Juego Abierto (OGL) y la Política de Uso de la Comunidad Pathfinder (PCUP). 

Está pensado para su uso con el sistema Pathfinder 2e con el software Foundry VTT 

El uso de este producto para permitir el juego con un producto comercial está estrictamente prohibido. No puede hacer que su producto dependa de este módulo a menos que sea gratuito.

En particular, no puede declarar este módulo en la sección "dependencias" (o en cualquier sección que pueda sustituirla) del archivo foundry module.json o system.json de su módulo, a menos que él mismo sea libre.

Además, si tiene intención de extraer o utilizar traducciones realizadas por aficionados para cualquier otro fin que no sea el uso de este módulo y que lleven el indicador de gratuidad, deberá **imperativamente** :
- respetar las licencias OGL y PCUP,
- y han obtenido el permiso del los traductores a españo (AsMcLeod). 

En particular, no podrá utilizar estas traducciones gratuitas realizadas por aficionados con fines comerciales.

Las traducciones oficiales utilizadas fueron objeto de un trabajo adicional por parte de los colaboradores del módulo destinado a suprimir las referencias de las páginas, formatear las tablas, insertar los enlaces necesarios para navegar en el software Foundry entre los objetos y realizar las automatizaciones que permite el sistema. Los archivos también se actualizaron progresivamente con las erratas publicadas por Paizo.

Algunos textos también se han revisado ligeramente en aras de la coherencia y la uniformidad en el uso de ciertas palabras o cuando la traducción no parecía la más adecuada al concepto, o cuando la traducción era errónea. Las erratas se aplican tal como aparecen en el original.