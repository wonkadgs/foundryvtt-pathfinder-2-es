#!/usr/bin/python3
# -*- coding: utf-8 -*-
##
## Este script permite limpiar un directorio de traducción eliminando los elementos que ya no están en el directorio
## compendium EN
##
import json
import os

from libdata import readFolder, dataToFile, getPacks, getValue, getList, equals, print_error, print_warning
# =================================
#  ---     PACK TO ARCHIVE      ---
# =================================
pack_to_archive = "equipment"

ROOT = "../"
print('Starting archiving on %s pack' % (pack_to_archive))

# =================================
# read pack files and generate dict
# =================================
with open("%spacks/%s.db" % (ROOT, pack_to_archive), 'r', encoding='utf8') as f:
  content = f.readlines()

entries = {}
for line in content:
  try:
    obj = json.loads(line)
  except:
    print_error("Invalid json %s at line %d" % (FILE, count))
    continue

  if '$$deleted' in obj:
    continue

  entries[obj['_id']] = { }

# =====================================
# create archive directory if necessary
# =====================================
if not os.path.exists("%sarchive" % (ROOT)):
  os.makedirs("%sarchive" % (ROOT))
if not os.path.exists("%sarchive/%s/" % (ROOT, pack_to_archive)):
  os.makedirs("%sarchive/%s/" % (ROOT, pack_to_archive))

# ==========================
# read translations
# ==========================
packData = readFolder("%sdata/%s/" % (ROOT, pack_to_archive))

# ========================
# archive deleted elements
# ========================
for id in packData[0]:
  if not id in entries:
    print("Archiving %s" % (packData[0][id]['filename']))
    os.replace("%sdata/%s/%s" % (ROOT, pack_to_archive, packData[0][id]['filename']),
               "%sarchive/%s/%s" % (ROOT, pack_to_archive, packData[0][id]['filename']))