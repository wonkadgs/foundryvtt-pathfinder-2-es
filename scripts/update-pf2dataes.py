#!/usr/bin/python3
# -*- coding: utf-8 -*-

############################################################
# Script para actualizar el proyecto pf2-data-es
# 
# Este script se utiliza para generar los datos del proyecto
# pf2-data-en y subirlo.
#
# Utiliza los datos en inglés y cualquier
# traducción para generar un archivo .json por tipo de
# datos.
#
# Si un dato no está presente en el proyecto pf2-data-es
# Es este script el que tendrás que modificar para añadirlos.
#
############################################################

import json
import os            
import datetime

from libdata import *
from statuses import *

PACKS        = "../packs/"
WEBSITE_DATA = "../../pf2-data-es/"

packs = getPacks()

for pack in packs:   
  
  list = []
    
# lista de datos de los ficheros de traducción ES
  esDatas = {}
  
  #############################################
  # leer todos los datos disponibles para el paquete especificado
  #############################################
  pack_id = pack['id']
  path = "../data/%s/" % pack_id
  all_files = os.listdir(path)
  print("Lectura del paquete de datos %s (%d ficheros)" % (pack_id, len(all_files)))
  for fpath in all_files:
    
    # el fichero de datos contiene los datos traducidos según el formato específico documentado en fileToData
    data = fileToData(path + fpath)

    if data['status'] == ST_NINGUNA or data['status'] == ST_AUTO_TRAD \
      or data['status'] == ST_AUTO_GOOGTRAD  or data['status'] == ST_VACIA:
      continue

    # item id
    data_id = data['id']

    # contiene los datos traducidos a ES
    translation = {
      'status': data['status'],
      'name': data['nameES'],
      'description': data['descrES']
    }

    # contiene datos en inglés sólo disponibles en el archivo ES
    miscData = {      
    }

    # data ES
    esData = {
      'translation': translation,
      'misc': miscData
    }

    ###################################################
    # Recuperación de los datos del fichero .htm español
    ###################################################
    #
    # Aquí es donde añadimos contenido traducido específico dependiendo del tipo de pack
    #
    # Si hay un campo en particular devuelto por fileToData cuya traducción ES nos interesa,
    # Aquí es donde puedes añadirlo a la parte de 'translation'
    #
    # Para el formato del array 'data', necesitas ver la documentación de la función fileToData
    # del archivo libdata.py
    #
    # La información puede copiarse en dos lugares:
    #
    # En el diccionario 'translation' si es la traducción ES
    # En el diccionario 'misc' si es un dato en inglés no disponible en el JSON Foundry
    #
    # El valor de la clave (por ejemplo, benefits) debe ser idéntico en el archivo de salida
    # entre la versión ES y EN.

    # feats
    # https://gitlab.com/ashmcleod/foundryvtt-pathfinder-2-es/-/tree/master/data/feats
    if pack_id == 'feats':
      # almacenar el campo benefitsES en las traducciones
      # y guardar los benefitsEN el campo misc para que aparezca en inglés
      addIfNotNull(translation, 'benefits', emptyAsNull(tryGetDict(data, 'benefitsES')))
      addIfNotNull(miscData, 'benefits', emptyAsNull(tryGetDict(data, 'benefitsEN')))

    # spells
    # https://gitlab.com/ashmcleod/foundryvtt-pathfinder-2-es/-/tree/master/data/spells
    if pack_id == 'spells':
      addIfNotNull(translation, 'target', emptyAsNull(tryGetDict(data, 'dataES', 'Target')))
      addIfNotNull(miscData, 'target', emptyAsNull(tryGetDict(data, 'dataEN', 'Target')))
      addIfNotNull(translation, 'range', emptyAsNull(tryGetDict(data, 'dataES', 'Range')))
      addIfNotNull(miscData, 'range', emptyAsNull(tryGetDict(data, 'dataEN', 'Range')))
      addIfNotNull(translation, 'materials', emptyAsNull(tryGetDict(data, 'dataES', 'Material')))
      addIfNotNull(miscData, 'materials', emptyAsNull(tryGetDict(data, 'dataEN', 'Material')))
      addIfNotNull(translation, 'secondaryCaster',  tryIntOrNone(emptyAsNull(tryGetDict(data, 'dataES', 'SecondaryCaster'))))
      addIfNotNull(miscData, 'secondaryCaster', tryIntOrNone(emptyAsNull(tryGetDict(data, 'dataEN', 'SecondaryCaster'))))
      addIfNotNull(translation, 'primaryCheck', emptyAsNull(tryGetDict(data, 'dataES', 'PrimaryCheck')))
      addIfNotNull(miscData, 'primaryCheck', emptyAsNull(tryGetDict(data, 'dataEN', 'PrimaryCheck')))
      addIfNotNull(translation, 'secondaryCheck', emptyAsNull(tryGetDict(data, 'dataES', 'SecondaryCheck')))
      addIfNotNull(miscData, 'secondaryCheck', emptyAsNull(tryGetDict(data, 'dataEN', 'SecondaryCheck')))
      addIfNotNull(translation, 'areaSize', emptyAsNull(tryGetDict(data, 'dataES', 'Areasize')))
      addIfNotNull(miscData, 'areaSize', emptyAsNull(tryGetDict(data, 'dataEN', 'Areasize')))

    # store translation
    esDatas[data_id] = esData

  #############################################
  # read original data from pf2 Foundry system
  #############################################
  if not 'desc' in pack['paths']:
    continue

  filename=pack["pack"] + "/" + pack_id + ".db" if "pack" in pack else PACKS + pack_id + ".db"
  descPathParts = pack['paths']['desc'].split('.')

  with open(filename, 'r', encoding='utf8') as f:
    content = f.readlines()

  count = 0
  for line in content:
    count += 1
    try:
      enJson = json.loads(line)
    except:
      print("Invalid json %s at line %d" % (filename, count))
      continue
    
    if '$$deleted' in enJson:
      continue

    # by default only id and name are copied
    dataJson = {
      '_id': enJson['_id'],
      'name': enJson['name']
    }
    
    # retrive description basée sur le chemin desc pack
    node = enJson
    i = 0
    while i < len(descPathParts) and descPathParts[i] in node:
      node = node[descPathParts[i]]
      i = i + 1
    if i == len(descPathParts):
      dataJson['description'] = node

    # add custom properties based on type

    try:
      ###################################################
      # Recuperación de datos del fichero en inglésRécupération données fichier json anglais
      ###################################################
      #
      # Aquí es donde personalizamos el contenido exportado según el tipo de pack (pack_id).
      #
      # La variable enJson contiene los datos json del archivo en inglés (foundry-vtt---pathfinder-2e)
      # El objetivo es copiar en la parte dataJson (exportar a pf2-data-es) los datos del fichero
      # inglés que nos interesa, manipulando los datos si es posible para que queden lo más claros posible
      #
      # El JSON inglés tiene una estructura específica, impuesta por el módulo Foundry.
      #
      # El interés de esta parte es copiar los datos pertinentes eliminando las especificidades
      # del formato Foundry para que el JSON sea lo más "lógico" posible.
      #
      # Ex:
      # el tipo de acción se almacena en un 'data': { 'actionType': { 'value': 'xxx' }
      # este script simplifica la estructura para simplemente almacenar esta información en un json { 'actionType': 'xxx' }

      # actions
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/actions.db
      if pack_id == 'actions':
        dataJson['actionType'] = enJson['data']['actionType']['value']

      # ancestries
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/ancestries.db
      if pack_id == 'ancestries':
        dataJson['additionalLanguages'] = enJson['data']['additionalLanguages']['value']
        dataJson['hp'] = enJson['data']['hp']
        dataJson['languages'] = enJson['data']['languages']['value']

      # ancestryfeatures
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/ancestryfeatures.db
      if pack_id == 'ancestryfeatures':
        dataJson['traits'] = enJson['data']['traits']['value']
        # inútil a priori, porque siempre el mismo
        # dataJson['featType'] = enJson['data']['featType']['value']
        # dataJson['level'] = enJson['data']['level']['value']
        # dataJson['actionType'] = enJson['data']['actionType']['value']

      # equipement
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/equipment.db
      if pack_id == 'equipment':
        data_type = dataJson['type'] = enJson['type']
        dataJson['price'] = enJson['data']['price']['value']
        dataJson['traits'] = enJson['data']['traits']['value']
        dataJson['rarity'] = enJson['data']['traits']['rarity']['value']
        if data_type == 'armor':
          # campos específicos del blindaje
          # se transforman en números
          dataJson['level'] = int(enJson['data']['level']['value'])
          dataJson['armor'] = int(enJson['data']['armor']['value'])
          dataJson['armorType'] = enJson['data']['armorType']['value']
          # propiedades que no desea copiar si no tienen valor (= 0 o vacías)
          addIfNotNull(dataJson, 'armorMaxDex', tryIntOrNone(emptyAsNull(enJson['data']['dex']['value'], '0')))
          addIfNotNull(dataJson, 'armorCheck', tryIntOrNone(emptyAsNull(enJson['data']['check']['value'], '0')))
          addIfNotNull(dataJson, 'armorStrength', tryIntOrNone(emptyAsNull(enJson['data']['strength']['value'], '0')))
          addIfNotNull(dataJson, 'armorEquippedBulk', tryIntOrNone(emptyAsNull(enJson['data']['equippedBulk']['value'])))
          addIfNotNull(dataJson, 'armorGroup', emptyAsNull(enJson['data']['group']['value']))
        if data_type == "weapon":
          # campos específicos del arma
          dataJson['level'] = int(enJson['data']['level']['value'])
          dataJson['weaponType'] = enJson['data']['weaponType']['value']
          dataJson['damage'] = {}
          dataJson['damage']['type'] = enJson['data']['damage']['damageType']
          dataJson['damage']['dice'] = enJson['data']['damage']['dice']
          dataJson['damage']['die'] = enJson['data']['damage']['die']
          # propiedades que no desea copiar si está vacío
          addIfNotNull(dataJson, 'group', tryGetDict(enJson, 'data', 'group', 'value'))
          addIfNotNull(dataJson, 'range', tryGetDict(enJson, 'data', 'range', 'value'))
          addIfNotNull(dataJson, 'weight', tryGetDict(enJson, 'data', 'weight', 'value'))

      # classes
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/classes.db
      if pack_id == 'classes':
        # no hay propiedades particulares, preferimos generar las páginas de clase manualmente por el momento
        pass
      
      # class features
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/classfeatures.db
      if pack_id == 'classfeatures':
        dataJson['level'] = int(enJson['data']['level']['value'])
        dataJson['traits'] = enJson['data']['traits']['value']
        pass

      # spells
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/spells.db
      if pack_id == 'spells':
        dataJson['school'] = enJson['data']['school']['value']
        dataJson['type'] = enJson['data']['category']['value'] # focus, ritual or spell
        dataJson['level'] = tryIntOrNone(enJson['data']['level']['value'])
        dataJson['traits'] = enJson['data']['traits']['value']
        addIfNotNull(dataJson, 'traditions', emptyAsNull(enJson['data']['traditions']['value']))
        dataJson['incantation'] = {}
        dataJson['incantation']['time'] = tryIntOrNone(enJson['data']['time']['value'])
        dataJson['incantation']['components'] = enJson['data']['components']

      # feats
      # cf. https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/packs/data/feats.db
      if pack_id == 'feats':
        dataJson['featType'] = enJson['data']['featType']['value']
        dataJson['level'] = tryIntOrNone(enJson['data']['level']['value'])
        dataJson['traits'] = enJson['data']['traits']['value']

    except Exception as ex:
      print("Unable to convert data from %s at line %d : %s" % (filename, count, ex))
  
#############################################
    # Añadir datos de traducción
    #############################################
    # 
    # Por defecto marcamos que no hay traducciones en el json:
    # "translations": { "es": { "status": "ninguna", ... } }
    translation = { 'status': ST_NINGUNA }

    # Toda la información de traducción se copia en "es".
    if dataJson['_id'] in esDatas:
      esData = esDatas[dataJson['_id']]

      # Copiar los datos de 'translation' a la parte de traducción
      translation = esData['translation']

      # Sobrescribir los datos EN con cualquier propiedad almacenada en misc
      misc = esData['misc']
      for key, value in misc.items():
        dataJson[key] = value

    # añadimos la traducción al final, para que las otras propiedades estén siempre antes de
    dataJson['translations'] = { 'es': translation }
      
    list.append(dataJson)

  with open(WEBSITE_DATA + pack['name'] + ".json", 'w') as outfile:
    json.dump(list, outfile, indent=3)
