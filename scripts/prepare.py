#!/usr/bin/python3
# -*- coding: utf-8 -*-

print('Starting imports')
import json
import yaml
import os
import re
import logging
import hashlib

from libdata import readFolder, dataToFile, getPacks, getValue, getList, equals, print_error, print_warning
from statuses import *

# reactivar para autotrad
# from libselenium import translator_driver, full_trad, TranslationData
# trans_cache = {
#   "8be24e128798f1654ca4d4605ec807db": TranslationData("<p>@Localizar[PF2E.NPC.Habilidades.Glosario.Darkvision]</p>", ST_AUTO_TRAD),
#   "70ef48e6bdb27dcc39f63a3b80e7d5fc": TranslationData("<p>@Localizar[PF2E.NPC.Habilidades.Glosario.CuraciónNegativa]</p>", ST_AUTO_TRAD)
# }

print('Preparing translation')
logging.basicConfig(filename='translation.log', level=logging.INFO)
  
ROOT="../"
if not os.path.isdir("%sdata/" % (ROOT)):
    os.mkdir("%sdata/" % (ROOT))
referencePaths = None
trans_done = 0
trans_chars = 0

def checkItems(entries, pack_id, isItems=False, isPages=False):
  global trans_cache, trans_done, trans_chars, driver
  folderPath = "%sdata/%s/" % (ROOT, pack_id)
  # ==========================
  # read all available entries
  # ==========================
  if not os.path.isdir(folderPath):
    os.mkdir(folderPath)

  folderData = readFolder(folderPath)
  existing = folderData[0]
  existingByName = folderData[1]
  pack_has_errors = folderData[2]

  existingReference = {}
  if isItems and referencePaths is not None:
    if isinstance(referencePaths, list):
      for referencePath in referencePaths:
        if not os.path.isdir(referencePath):
          os.mkdir(referencePath)
        existingReference = {**existingReference, **readFolder(referencePath)[0]}
    else:
      existingReference = readFolder(referencePaths)[0]

  # ========================
  # create or update entries
  # ========================
  for id in entries:
    source = entries[id]
    if not source:
      continue

    # build filename
    filename = "%s.htm" % id
    if source['type2']:
      filename = "%s-%s-%s" % (source['type1'], source['type2'], filename)
    elif source['type1']:
      filename = "%s-%s" % (source['type1'], filename)
    filepath = "%s/%s" % (folderPath, filename)

    # data exists in reference directory => ignore it
    if id in existingReference:
      print("%s already translated in reference compendiums => Skipping it" % (id))

    # data exists for id
    elif id in existing:

      # rename file if filepath not the same
      if existing[id]['filename'] != filename:

        pathFrom = "%s/%s" % (folderPath, existing[id]['filename'])
        pathTo = "%s/%s" % (folderPath, filename)
        os.rename(pathFrom, pathTo)

      # check status from existing file
      if not existing[id]["status"] in (ST_LIBRE, ST_OFICIAL, ST_DUPLICADA, ST_NINGUNA, ST_MODIFICADA, ST_AUTO_TRAD, ST_AUTO_GOOGTRAD, ST_VACIA):
        print_error("Status error for : %s status: %s" % (filepath, existing[id]["status"]));
        has_errors = True
        continue

      if (isItems and not equals(existing[id]['parentName'], source['parentName'])) \
              or (isPages and not equals(existing[id]['journal'], source['journal'])) \
              or not equals(existing[id]['nameEN'], source['name'])\
              or not equals(existing[id]['descrEN'], source['desc'])\
              or not equals(existing[id]['listsEN'], source['lists'])\
              or not equals(existing[id]['dataEN'], source['data']):
        if ('parentName' in source):
          existing[id]['parentName'] = source['parentName']
        if ('journal' in source):
          existing[id]['journal'] = source['journal']
        existing[id]['nameEN'] = source['name']
        existing[id]['descrEN'] = source['desc']
        existing[id]['listsEN'] = source['lists']
        existing[id]['dataEN'] = source['data']

        if existing[id]['status'] != ST_NINGUNA and existing[id]['status'] != ST_MODIFICADA:
          existing[id]['oldstatus'] = existing[id]['status']
          existing[id]['status'] = ST_MODIFICADA

        dataToFile(existing[id], filepath)

      elif 'oldstatus' in existing[id] and existing[id]['status'] != ST_MODIFICADA:
        del existing[id]['oldstatus']
        dataToFile(existing[id], filepath)

      if existing[id]["status"] == ST_AUTO_TRAD:
        trans_chars += len(existing[id]['descrES'])
      
      # Traducción automática
      # if existing[id]["status"] == ST_NINGUNA:
      #   if isItems\
      #         or isPages\
      #         or not existing[id]['nameEN'] in existingByName\
      #         or existing[id]['nameEN'] in ("Shattering Strike", "Chilling Spray"):
      #     name = existing[id]["nameEN"]
      #     if len(existing[id]['descrEN']) > 0 and len(existing[id]['descrEN']) < 5000:
      #       # Automatic translation
      #       logging.info("Translating %s" % name)
      #       descMD5 = hashlib.md5(existing[id]['descrEN'].encode('utf-8')).hexdigest()
      #       print("Translating %s %s" % (name, descMD5))
      #       translation_data = full_trad(driver, existing[id]['descrEN']) if descMD5 not in trans_cache else trans_cache[descMD5]
      #       trans_cache[descMD5] = translation_data
      #       tradDesc = translation_data.data
      #       status = translation_data.status
      #       if (trans_done > 9):
      #         if driver:
      #           driver.quit()
      #           driver = translator_driver()
      #           trans_done = 0
      #       trans_done += 1
      #       trans_chars += len(tradDesc)
      #       print("Translation done %s %s %d %d" % (tradDesc, status, trans_done, trans_chars))
      #       existing[id]['descrES'] = tradDesc
      #       existing[id]['status'] = status
      #       dataToFile(existing[id], filepath)

    # file doesn't exist
    else:

      # check if other entry exists with same name => means that ID has changed for the same element
      # not applicable to items (monsters have entries with the same name but different descriptions, we target on ID)
      if not isItems\
              and not isPages\
              and source['name'] in existingByName\
              and not source['name'] in ("Shattering Strike", "Chilling Spray"):
        oldEntry = existingByName[source['name']]
        # rename file
        pathFrom = "%s/%s" % (folderPath, oldEntry['filename'])
        pathTo = "%s/%s" % (folderPath, filename)

        if oldEntry['id'] in existing:
          del existing[oldEntry['id']]
        os.rename(pathFrom, pathTo)

      # create new
      else:
        name = source["name"]
        if len(source['desc']) > 0 and len(source['desc']) < 5000:
          # Automatic translation
          # logging.info("Translating %s" % name)
          # descMD5 = hashlib.md5(source['desc'].encode('utf-8')).hexdigest()
          # print("Translating %s %s" % (name, descMD5))
          # translation_data = full_trad(driver, source['desc']) if descMD5 not in trans_cache else trans_cache[descMD5]
          # trans_cache[descMD5] = translation_data
          # #translation_data = full_trad(driver, source['desc'])
          # tradDesc = translation_data.data
          # status = translation_data.status
          # if (trans_done > 9):
          #   if driver:
          #     driver.quit()
          #     driver = translator_driver()
          #     trans_done = 0
          # trans_done += 1
          # trans_chars += len(tradDesc)
          # print("Translation done %s %s %d %d" % (tradDesc, status, trans_done, trans_chars))
          # FIX : auto-trad demasiado largo para el bestiario 3
          status=ST_NINGUNA
          tradDesc = ""
        elif len(source['desc']) >= 5000:
          print("Skipped %s because of TOO LONG" % (name))
          tradDesc = ""
          status = ST_NINGUNA
        else:
          print("Skipped %s because of NO DESC" % (name))
          tradDesc = ""
          status = ST_VACIA

        data = {
          'parentName': source["parentName"] if isItems else None,
          'journal': source["journal"] if isPages else None,
          'nameEN': name,
          'nameES': "",
          'status': status,
          'descrEN': source['desc'],
          'descrES': tradDesc,
          'listsEN': source['lists'],
          'dataEN': source['data'],
          'listsES': {}
        }
        dataToFile(data, filepath)

  # si el paquete contiene al menos un error de lectura, dejamos de examinarlo
  if pack_has_errors == True:
    print_warning("Invalid data in pack %s, skipping" % (folderPath))
    return True

  # =======================
  # search deleted elements
  # =======================
  if not os.path.exists("%sarchive" % (ROOT)):
    os.makedirs("%sarchive" % (ROOT))
  if not os.path.exists("%sarchive/%s/" % (ROOT, pack_id)):
    os.makedirs("%sarchive/%s/" % (ROOT, pack_id))

  for id in existing:
    if not id in entries:
      filename = "%s/%s" % (folderPath, existing[id]['filename'])
      if existing[id]['status'] != 'aucune':
        print("Archiving %s" % (existing[id]['filename']))
        os.replace("%s/%s" % (folderPath, existing[id]['filename']),
                   "%sarchive/%s/%s" % (ROOT, pack_id, existing[id]['filename']))
      else:
        os.remove(filename)

  return False

# reactivar para autotrad
# # abrir la conexión con DeepL Translator
# print('Opening DeepL Translator connection')
# driver = translator_driver()

print('Loading packs...')
packs = getPacks()
has_errors = False

for p in packs:
  pack_id = p["id"]
  print('Preparing %s.db pack' % (pack_id))
  FILE=p["pack"] + "/" + pack_id + ".db" if "pack" in p else ROOT + "packs/" + pack_id + ".db"
  entries = {}

  # =================================
  # read pack files and generate dict
  # =================================
  with open(FILE, 'r', encoding='utf8') as f:
    content = f.readlines()

  obj_items = {}
  obj_pages = {}
  count = 0
  for line in content:
    count += 1
    try:
      obj = json.loads(line)
    except:
      print_error("Invalid json %s at line %d" % (FILE, count))
      continue
    
    if '$$deleted' in obj:
      continue
    
    entries[obj['_id']] = {
      'parentName': None,
      'name': getValue(obj, p['paths']['name']), 
      'desc': getValue(obj, p['paths']['desc'], False, "") if 'desc' in p['paths'] else "NO TRADUCIR",
      'type1': getValue(obj, p['paths']['type1']) if 'type1' in p['paths'] else None,
      'type2': getValue(obj, p['paths']['type2'], False) if 'type2' in p['paths'] else None,
      'lists': {},
      'data': {}
    }
    
    ## additional lists
    if "lists" in p:
      for key in p["lists"]:
        list = getList(obj, p["lists"][key], False)
        if len(list) == 0:
          list = getList(obj, p["lists"][key] + ".value", False)
        entries[obj['_id']]['lists'][key] = list
    
    ## other extractions
    if "extract" in p:
      for key in p["extract"]:
          value = getValue(obj, p["extract"][key], False, None, False)
          if value and len(value) > 0:
              entries[obj['_id']]['data'][key] = value

    ## items
    if "items" in p:
      items = getList(obj, "items", False)
      for item in items:
        # Ataques, Pasivas, Acciones
        if item['type'] in ["melee", "action", "spellcastingEntry"]:
          obj_items[item['_id']] = {
            "_id": getValue(item, '_id', False),
            "parentName": getValue(obj, p['paths']['name']),
            "name": getValue(item, p['items']['paths']['name'], False),
            "desc": getValue(item, p['items']['paths']['desc'], False, "") if 'desc' in p['items']['paths'] else "NO TRADUCIR",
            'type1': None,
            'type2': None,
            'lists': {},
            'data': {}
          }

    ## pages
    if "pages" in p:
      pages = getList(obj, "pages", False)
      journal = getValue(obj, p['paths']['name'])
      for page in pages:
        obj_pages[page['_id']] = {
          "_id": getValue(page, '_id', False),
          "journal": journal,
          "name": getValue(page, p['pages']['paths']['name'], False),
          "desc": getValue(page, p['pages']['paths']['desc'], False, "") if 'desc' in p['pages']['paths'] else "NO TRADUCIR",
          'type1': None,
          'type2': None,
          'lists': {},
          'data': {}
        }

  # ==============================
  # search for duplicates in names
  # ==============================
  duplic = {}
  for id in entries:
    if entries[id]['name'] in duplic:
      print_warning("Duplicated name: %s (%s)\e[0m" % (entries[id]['name'], id))
    else:
      duplic[entries[id]['name']] = id

  # ==========================
  # read all available entries
  # ==========================
  has_errors = checkItems(entries, pack_id)

  # =============
  # process items
  # =============
  if len(obj_items) > 0:
    folderPath = "%sdata/%s/" % (ROOT, pack_id+"-items")
    if "reference" in p:
      if isinstance(p["reference"], list):
        referencePaths = []
        for reference in p["reference"]:
          referencePaths.append("%sdata/%s/" % (ROOT, reference+"-items"))
      else:
          referencePaths = "%sdata/%s/" % (ROOT, p["reference"]+"-items")

    has_errors = checkItems(obj_items, pack_id+"-items", True, False)

  # =============
  # process pages
  # =============
  if len(obj_pages) > 0:
    has_errors = checkItems(obj_pages, pack_id+"-pages", False, True)

# reactivar para autotrad
# # cierre de la conexión con DeepL Translator
# if driver:
#   driver.quit()
if has_errors:
  print_error("At least one error during preparation, failure")
  exit(1)