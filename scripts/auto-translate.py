#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
from libdata import readFolder, dataToFile, getPacks
from libselenium import translator_driver, full_trad
from statuses import *
import hashlib
import json

ROOT = "../"

# driver = translator_driver()

logging.basicConfig(filename='translation.log', level=logging.INFO)

packs = getPacks()
trans_filepath = "%smd5_es.json" % ROOT
trans_data = json.load(open(trans_filepath, "r"))

def trans(data):
    global trans
    md5Id = hashlib.md5(data.encode('utf-8')).hexdigest()
    return trans_data[md5Id] if md5Id in trans_data else ""

def transL(data):
    global trans
    print("\t" + str(data))
    if data is dict:
        for k in data:
            data[k] = transL(data[k])
        return data
    elif isinstance(data, (tuple, list, set)):
        return map(trans, data)
    else:
        return trans(data)

# def addToTrans(data, prefix = ""):
#     global trans
#     if data is dict:
#         for k in data:
#             addToTrans(data[k], prefix + "\t")
#     elif isinstance(data, (tuple, list, set)):
#         for k in data:
#             addToTrans(k, prefix + "\t")
#     else:
#         print(prefix + data, data is list, data is dict, isinstance(data, (tuple, list, set)))
#         md5Id = hashlib.md5(data.encode('utf-8')).hexdigest()
#         trans[md5Id] = data

for p in packs:
    print (f'Generating {p["id"]}')
    folderData = readFolder("%sdata/%s/" % (ROOT, p["id"]))
    existing = folderData[0]
    existingByName = folderData[1]

    # ========================
    # create or update entries
    # ========================
    for id in existing:
        filename = existing[id]["filename"]
        filepath = "%sdata/%s/%s" % (ROOT, p["id"], filename)
        if existing[id]["status"] == ST_NINGUNA:
            print(existing[id])
            if ("nameEN" in existing[id] and existing[id]["nameES"] == ""):
                existing[id]["nameES"] = trans(existing[id]["nameEN"])
                if (existing[id]["nameES"] != ""): existing[id]["status"] = ST_AUTO_TRAD
            if ("descrEN" in existing[id] and existing[id]["descrES"] == ""):
                existing[id]["descrES"] = trans(existing[id]["descrEN"])
                if (existing[id]["descrES"] != ""): existing[id]["status"] = ST_AUTO_TRAD
            if "listsEN" in existing[id]:
                for k in existing[id]["listsEN"]:
                    temp = existing[id]["listsES"][k]
                    existing[id]["listsES"][k] = transL(existing[id]["listsEN"][k])
                    if (temp != existing[id]["listsES"][k]): existing[id]["status"] = ST_AUTO_TRAD
            if "dataEN" in existing[id]:
                for k in existing[id]["dataEN"]:
                    temp = existing[id]["dataES"][k]
                    existing[id]["dataES"][k] = transL(existing[id]["dataEN"][k])
                    if (temp != existing[id]["dataES"][k]): existing[id]["status"] = ST_AUTO_TRAD
            dataToFile(existing[id], filepath)
        
        if existing[id]["status"] == ST_AUTO_TRAD:
            print(existing[id])
            if ("nameEN" in existing[id] and existing[id]["nameES"] == ""):
                existing[id]["nameES"] = trans(existing[id]["nameEN"])
                existing[id]["status"] = ST_AUTO_TRAD
            if ("descrEN" in existing[id] and existing[id]["descrES"] == ""):
                existing[id]["descrES"] = trans(existing[id]["descrEN"])
                existing[id]["status"] = ST_AUTO_TRAD
            if "listsEN" in existing[id]:
                for k in existing[id]["listsEN"]:
                    existing[id]["listsES"][k] = transL(existing[id]["listsEN"][k])
                    existing[id]["status"] = ST_AUTO_TRAD
            if "dataEN" in existing[id]:
                for k in existing[id]["dataEN"]:
                     existing[id]["dataES"][k] = transL(existing[id]["dataEN"][k])
                     existing[id]["status"] = ST_AUTO_TRAD
            dataToFile(existing[id], filepath)

            
        # if existing[id]["status"] == ST_NINGUNA or (existing[id]["status"]== ST_MODIFICADA and len(existing[id]["descrES"])==0):
            # name = existing[id]["nameEN"]
            # logging.info("Translating "+name)
            # toTrad = existing[id]['descrEN']
            # translationAttempt = full_trad(driver, toTrad)
            # existing[id]['descrES'] = translationAttempt.data
            # existing[id]['status'] = translationAttempt.status
            # dataToFile(existing[id], filepath)
# driver.quit()

# json_object = json.dumps(trans, indent=2)
# trans_filepath = "%stotrans_ninguna.txt" % ROOT
        
# with open(trans_filepath, "w") as outfile:
#     outfile.write(json_object)

