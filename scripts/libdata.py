#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import re
import requests
import json
import html
import time
import logging
from dataclasses import dataclass

##########################################
# Packs
##########################################
#
# Lista de paquetes compatibles, con ajustes de traducción
#
#
# transl    Nombre del pack traducido al español
# paths     Diccionario que contiene la ruta de la información principal en el json inglés
#   name    Nombre de la propiedad que contiene el nombre de los datos
#   desc    Ruta de la propiedad que contiene la descripción a traducir
#   type1   Ruta de los datos que se utilizarán como primera parte del nombre del archivo. Si está ausente, sólo se utilizará el id para nombrar el archivo. El archivo se llamará type1-id.htm
#   type2   Ruta del segundo dato. Si el tipo2 está presente, el tipo1 también debe estarlo. El archivo se llamará type1-type2-id.htm
# extract   Diccionario que contiene la lista de campos adicionales que deben extraerse de la sección ------- Data y por traducir.
#           La clave será el nombre del campo con ES y EN añadidos
# lists     Diccionario que contiene la lista de campos adicionales a extraer en forma de lista, cuyos distintos valores se extraerán con un "|" como separador, y a traducir.
# items     (sólo Bestiario) Los objetos presentes en la criatura.

SUPPORTED = {
    #### Les ascendances et les capacités
    "ancestries": {'transl': "Ascendencias", "paths": {'name': "name", 'desc': "system.description.value"}},
    "heritages": {
        'transl': "Herencias de ascendencia",
        "paths": {'name': "name", 'desc': "system.description.value"},
    },
    "ancestryfeatures": {
        'transl': "Dotes de ascendencia",
        "paths": {
            'name': "name",
            'desc': "system.description.value",
            'type1': "type",
            'type2': "system.level.value"
        }
    },
    #### Los bagajes
    "backgrounds": {'transl': "Bagajes", "paths": {'name': "name", 'desc': "system.description.value"}},
    #### Las clases y sus dotes
    "classes": {'transl': "Clases", "paths": {'name': "name", 'desc': "system.description.value"}},
    "classfeatures": {
        'transl': "Dotes de clase",
        "paths": {
            'name': "name",
            'desc': "system.description.value",
            'type1': "system.traits.value",
            'type2': "system.level.value"
        }
    },
    #### Dotes (de clase, ancestrales, generales, de habilidad, arquetipos...)
    "feats": {
        'transl': "Dotes",
        "paths": {
            'name': "name",
            'desc': "system.description.value",
            'type1': "system.featType.value",
            'type2': "system.level.value"
        },
        "lists": {
            'Prereq': "system.prerequisites.value"
        }
    },
    #### Poderes de compañero y maestro
    "familiar-abilities": {'transl': "Habilidades de compañero", "paths": {'name': "name", 'desc': "system.description.value", 'type1': "system.level.value"}},
    #### Los arquetipos están de actualidad.
    #### "archetypes": {'transl': "Arquetipos","paths": {'name': "name",'desc': "content"}},
    #### Los conjuros
    "spells": {
        'transl': "Conjuros",
        "paths": {
            'name': "name",
            'desc': "system.description.value",
            'type1': "system.school.value",
            'type2': "system.level.value"
        },
        "extract": {
            'AreaDetails': "system.area.details",
            'Cost': "system.cost.value",
            'Duration': "system.duration.value",
            'Heightening1Target': "system.heightening.levels.1.target.value",
            'Heightening2Target': "system.heightening.levels.2.target.value",
            'Heightening3Target': "system.heightening.levels.3.target.value",
            'Heightening4Target': "system.heightening.levels.4.target.value",
            'Heightening5Target': "system.heightening.levels.5.target.value",
            'Heightening6Target': "system.heightening.levels.6.target.value",
            'Heightening7Target': "system.heightening.levels.7.target.value",
            'Heightening8Target': "system.heightening.levels.8.target.value",
            'Heightening9Target': "system.heightening.levels.9.target.value",
            'Heightening10Target': "system.heightening.levels.10.target.value",
            'Heightening1Range': "system.heightening.levels.1.range.value",
            'Heightening2Range': "system.heightening.levels.2.range.value",
            'Heightening3Range': "system.heightening.levels.3.range.value",
            'Heightening4Range': "system.heightening.levels.4.range.value",
            'Heightening5Range': "system.heightening.levels.5.range.value",
            'Heightening6Range': "system.heightening.levels.6.range.value",
            'Heightening7Range': "system.heightening.levels.7.range.value",
            'Heightening8Range': "system.heightening.levels.8.range.value",
            'Heightening9Range': "system.heightening.levels.9.range.value",
            'Heightening10Range': "system.heightening.levels.10.range.value",
            'Material': "system.materials.value",
            'Range': "system.range.value",
            'Target': "system.target.value",
            'Time': "system.time.value",
            'SecondaryCaster': "system.secondarycasters.value",
            'PrimaryCheck': "system.primarycheck.value",
            'SecondaryCheck': "system.secondarycheck.value",
        }
    },
    #### El equipo
    "equipment": {
        'transl': "Equipo",
        "paths": {
            'name': "name",
            'desc': "system.description.value",
            'type1': "type",
            'type2': "system.level.value"
        }
    },
    #### Los dioses
    "deities":    {'transl': "Dioses", "paths": { 'name': "name", 'desc': "system.description.value"}},
    #### Los dominios están de actualidad
    "domains": {
        'transl': "Dominios",
        "paths": {
            'name': "name",
            'desc': "content"
        }
    },
    #### Las reglas del juego: las acciones y las específicas de las aventuras
    "actions":    {'transl': "Acciones", "paths": {'name': "name", 'desc': "system.description.value"}},
    "adventure-specific-actions": {'transl': "Acciones específicas de aventura", "paths": {'name': "name", 'desc': "system.description.value"}},
    #### Los estados
    "conditions": {'transl': "Estados", "paths": {'name': "name", 'desc': "system.description.value"}},
    #### Los enemigos: bestiarios y peligros, habilidades de los monstruos
    "pathfinder-bestiary": {
        'transl': "Bestiario",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            }
        }
    },
    "pathfinder-bestiary-2": {
        'transl': "Bestiario 2",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "pathfinder-bestiary-3": {
        'transl': "Bestiario 3",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            }
        }
    },
    "npc-gallery": {
        'transl': "Galería de PNJs",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "book-of-the-dead-bestiary": {
        'transl': "Bestiario del Libro de los Muertos",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "hazards": {
      'transl': "Peligros",
      "paths": {
        'name': "name",
        'desc': "system.details.description"
      },
      "extract": {
        'Disable': "system.details.disable",
        'Reset': "system.details.reset",
        'Routine': "system.details.routine",
        'Stealth': "system.attributes.stealth.details",
        'Target': "system.target.value",
        'ItemsDescription': "items.system.description.value",
      },
      "items": {
        'paths': {
            'name': "name",
            'desc': "system.description.value"
        },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
      }
    },
    "vehicles": {
        'transl': "Vehículos",
        "paths": {
            'name': "name",
            'desc': "system.details.description"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'Crew': "system.details.crew",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'PilotingCheck': "system.details.pilotingCheck",
            'Speed': "system.details.speed",
            'Traits': "system.traits.traits.custom"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "bestiary-ability-glossary-srd": {
        'transl': "Aptitudes de monstruo",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    "bestiary-family-ability-glossary": {
        'transl': "Aptitudes de los compañeros de monstruo",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    #### Utilidades del DJ
    "criticaldeck": {
        'transl': "Mazo de críticos",
        "paths": {'name': "name", 'desc': "content"}
    },
    "gmg-srd": {'transl': "Guía del Director de juego", "paths": {'name': "name", 'desc': "content"}},
    "pathfinder-society-boons": {'transl': "Recompensas PFS", "paths": {'name': "name", 'desc': "system.description.value"}},
    "boons-and-curses": {'transl': "Bendiciones y maldiciones", "paths": {'name': "name", 'desc': "system.description.value"}},
    #### Macros de Foundry con sólo nombres traducidos
    "action-macros": {
        'transl': "Macros de acciones",
        "paths": { 'name': "name"}
    },
    "macros": {
        'transl': "Macros PF2e",
        "paths": { 'name': "name"}
    },
    #### Efectos de Foundry que pueden arrastrarse a las fichas, de los cuales sólo se traduce lo esencial.
    "bestiary-effects": {
        'transl': "Efectos de monstruos",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    "campaign-effects": {
        'transl': "Efectos de aventura",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    "equipment-effects": {
        'transl': "Efectos de equipo",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    "feat-effects": {
        'transl': "Efectos de dotes y aptitudes",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    "other-effects": {
        'transl': "Otros efectos",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    "spell-effects": {
        'transl': "Efectos de conjuros",
        "paths": {'name': "name", 'desc': "system.description.value"}
    },
    # Animal Companions
    "ac-advanced-maneuvers":      {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Maniobras avanzadas (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-ancestries-and-class":    {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Ascendencias y clases (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-construct-breakthroughs": {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Evolución de Contructos (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-construct-companions":    {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Constructos (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-eidolons":                {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Eidolons (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-evolution-feats":         {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Evoluciones (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-feats":                   {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Dotes (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-features":                {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Aptitudes (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},
    "ac-support-benefits":        {'pack': "../packs-animal", 'module': "pf2e-animal-companions", 'transl': "Beneficios (Compañeros Animales)", "paths": {'name': "name", 'desc': "system.description.value"}},

    # Adventure Paths
    "age-of-ashes-bestiary": {
        "transl": "La Era de las Cenizas",
        "reference": "pathfinder-bestiary",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value",
            'ItemsDescription': "items.system.description.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "extinction-curse-bestiary": {
        "transl": "La Maldición de la extinción",
        "reference": ["pathfinder-bestiary", "pathfinder-bestiary-2"],
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value",
            'ItemsDescription': "items.system.description.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "kingmaker-bestiary": {
        "transl": "Kingmaker",
        "reference": ["pathfinder-bestiary", "pathfinder-bestiary-2"],
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "menace-under-otari-bestiary": {
        "transl": "Amenaza bajo Otari",
        "reference": "pathfinder-bestiary",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value",
            'ItemsDescription': "items.system.description.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "troubles-in-otari-bestiary": {
        "transl": "Problems en Otari",
        "reference": "pathfinder-bestiary",
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value",
            'ItemsDescription': "items.system.description.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "agents-of-edgewatch-bestiary": {
        "transl": "Agentes de Edgewatch",
        "reference": ["pathfinder-bestiary","pathfinder-bestiary-2"],
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value",
            'ItemsDescription': "items.system.description.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },
    "abomination-vaults-bestiary": {
        "transl": "Criptas Aberrantes",
        "reference": ["pathfinder-bestiary","pathfinder-bestiary-2"],
        "paths": {
            'name': "name",
            'desc': "system.details.publicNotes"
        },
        "extract": {
            'CADetails': "system.attributes.ac.details",
            'CI': "system.traits.ci.custom",
            'DamageImmunity': "system.traits.di.custom",
            'DamageResistance': "system.traits.dr.custom",
            'DamageVulnerability': "system.traits.dv.custom",
            'HPDetails': "system.attributes.hp.details",
            'Languages': "system.traits.languages.custom",
            'Saves': "system.attributes.allSaves.value",
            'Senses': "system.traits.senses.value",
            'Speeds': "system.attributes.speed.details",
            'Traits': "system.traits.traits.custom",
            'Desc (hazard)': "system.details.description",
            'Stealth': "system.attributes.stealth.details",
            'Disable': "system.details.disable",
            'Reset': "system.details.reset",
            'Routine': "system.details.routine",
            'Target': "system.target.value",
            'ItemsDescription': "items.system.description.value"
        },
        "items": {
            'paths': {
                'name': "name",
                'desc': "system.description.value"
            },
            'extract': {
                'variant0': "system.variants.0.label",
                'variant1': "system.variants.1.label",
                'variant2': "system.variants.2.label"
            }
        }
    },

    ### Diarios
    "journals": {
        "transl": "Diarios",
        "paths": {
            'name': "name"
        },
        "pages": {
            'paths': {
                'name': "name",
                'desc': "text.content"
            }
        }
    }
}


class bcolors:
    OK = '\033[92m'  # GREEN
    WARNING = '\033[93m'  # YELLOW
    FAIL = '\033[91m'  # RED
    RESET = '\033[0m'  # RESET COLOR


def print_error(message):
    print(bcolors.FAIL + message + bcolors.RESET)


def print_warning(message):
    print(bcolors.WARNING + message + bcolors.RESET)

#
# esta función lee el archivo system.json y extrae información sobre los paquetes
#

def getPacks():
    response = json.loads(requests.get(
        "https://raw.githubusercontent.com/foundryvtt/pf2e/master/system.json").text)
    packs = []

    # PF2 system
    for p in response["packs"]:
        match = re.search('packs/([-\w]+)\.db', p['path'])
        if match:
            p['id'] = match.group(1).strip()
        else:
            print("Error parsing ID from %s" % p['path'])
            exit(1)

        if p['id'] in SUPPORTED:
            packs.append({**p, **SUPPORTED[p['id']]})

    # PF2 Animal Companion
    response = json.loads(requests.get(
        "https://raw.githubusercontent.com/TikaelSol/PF2e-Animal-Companions/main/module.json").text)

    for p in response["packs"]:
        match = re.search('packs/([-\w]+)\.db', p['path'])
        if match:
            p['id'] = match.group(1).strip()
        else:
            print("Error parsing ID from %s" % p['path'])
            exit(1)
        if p['id'] in SUPPORTED:
            packs.append({**p, **SUPPORTED[p['id']]})

    return packs

#
# esta función devuelve verdadero si los dos textos son idénticos
# (ignora los saltos de línea)
#

def equals(val1, val2):
    if isinstance(val1, dict) and isinstance(val2, dict):
        keys1 = list(val1.keys())
        keys2 = list(val2.keys())
        if len(keys1) != len(keys2):
            return False
        # comparar el contenido de cada clave
        for k in keys1:
            if not k in keys2:
                return False
            # el contenido es una lista
            # eliminar espacios en blanco de las listas
            list1 = []
            list2 = []
            try:
                list1 = [e.strip() for e in val1[k] if len(e.strip()) > 0]
            except AttributeError:
                for item in val1[k]:
                    for e in item.values():
                        if len(e.strip()) > 0:
                            list1 += [e.strip()]
            try:
                list2 = [e.strip() for e in val2[k] if len(e.strip()) > 0]
            except AttributeError:
                for item in val2[k]:
                    for e in item.values():
                        if len(e.strip()) > 0:
                            list2 += [e.strip()]
            if list1 != list2:
                return False
        return True
    else:
        return val1.replace('\n', '').replace('\r', '').strip() == val2.replace('\n', '').replace('\r', '').strip()

#
# esta función intenta extraer un valor de un objeto
# Ex: data.level.value => obj["data"]["level"]["value"]
#

def getObject(obj, path, exitOnError=True):
    element = obj
    for p in path.split('.'):
        if p in element and element[p] is not None:
            element = element[p]
        elif exitOnError:
            print_error("Error with path %s in %s" % (path, obj))
            exit(1)
        else:
            # print("Path %s not found for %s!" % (path, obj['name']))
            return None
    return element


def getValue(obj, path, exitOnError=True, defaultValue=None, digitFormat=True):
    element = getObject(obj, path, exitOnError)
    if element is None:
        return defaultValue
    elif isinstance(element, int) and digitFormat:
        return "%02d" % element
    elif isinstance(element, list):
        if len(element) == 0:
            return defaultValue
        if len(element) > 1:
            print_warning(
                "List has more than 1 element for '%s'! %s" % (element, path))
            return element[len(element) - 1]
        return element[0]
    elif element.isdigit() and digitFormat:
        return "%02d" % int(element)
    else:
        return element


def getList(obj, path, exitOnError=True):
    element = getObject(obj, path, exitOnError)
    if element is None:
        return []
    elif not isinstance(element, list):
        if exitOnError:
            print_error("Element at '%s' is not a list! %s" % (path, element))
            exit(1)
        return []
    else:
        return element

#
# Esta función extrae información de un archivo .htm en forma de una tabla que contiene los distintos atributos
# en el formato nombre: Valor
#
# Lista de valores devueltos :
#
# id            identificador único completo (por ejemplo, skill-15-Vk7BzAb3D9r226sI), obtenido a partir del nombre del archivo sin el .htm
# nameEN        nombre en inglés (Name)
# nameES        nombre en español (Nombre)
# status        estado de la trducción (Estado)
# oldstatus     estado original de la traducción (Estado original)
# benefitsEN    beneficios (de la dote?) en inglés (Benefits)
# benefitsES    beneficios (de la dote?) en español (Beneficios)
# spoilersEN    Etiqueta SpoilersEN
# spoilersES    Etiqueta SpoilersES
#
# descrES/EN    Descripción en español/inglés
# dataEN/ES     Tabla de los diferentes datos almacenados en la sección ------ Data
# listsEN/ES    Tabla de las diferentes listas
#               

def fileToData(filepath):
    data = {}
    if os.path.isfile(filepath):

        # read all lines in f
        with open(filepath, 'r', encoding='utf8') as f:
            content = f.readlines()

        descrEN = ""
        descrES = ""
        isDescEN = False
        isDescES = False
        isData = False
        listsEN = {}
        listsES = {}
        dataEN = {}
        dataES = {}

        match = re.search('(\w{16})\.htm', filepath)
        if not match:
            print_error("Invalid filename %s" % filepath)
            exit(1)

        data['id'] = match.group(1)
        data['misc'] = {}

        for line in content:
            if line.startswith("Parent Name:"):
                data['parentName'] = line[12:].strip()
            if line.startswith("Journal:"):
                data['journal'] = line[8:].strip()
            if line.startswith("Name:"):
                data['nameEN'] = line[5:].strip()
            elif line.startswith("Nombre:"):
                data['nameES'] = line[7:].strip()
            elif line.startswith("Estado:"):
                data['status'] = line[7:].strip()
            elif line.startswith("Estado original:"):
                data['oldstatus'] = line[16:].strip()

            # Campos codificados
            # Se requiere que el campo: no esté en Datos, termine en EN o ES y no sea una lista
            elif line.startswith("Benefits:"):
                data['benefitsEN'] = line[9:].strip()
            elif line.startswith("Beneficios:"):
                data['benefitsES'] = line[10:].strip()
            elif line.startswith("SpoilersEN:"):
                data['spoilersEN'] = line[11:].strip()
            elif line.startswith("SpoilersES:"):
                data['spoilersES'] = line[11:].strip()

            elif line.startswith("------ Benefits") or line.startswith("------ Spoilers"):
                isData = False
                continue
            elif line.startswith("------ Data"):
                isData = True
            elif line.startswith("------ Description (en) ------"):
                isData = False
                isDescEN = True
                isDescES = False
                continue
            elif line.startswith("------ Description (es) ------"):
                isData = False
                isDescES = True
                isDescEN = False
                continue
            elif not isDescEN and not isDescES and len(line.strip()) > 0:
                # intenta leer todas las propiedades restantes como traducciones ES/EN

                # empezamos buscando el ':' al final de la palabra
                sep = line.find(":")
                if sep < 0:
                    print(bcolors.FAIL + "Invalid data '%s' in file %s " %
                          (line, filepath) + bcolors.RESET)
                else:
                    key = line[0:sep]
                    value = line[sep+1:].strip()
                    # tomamos todos los atributos que terminan en ES o EN
                    if key.endswith("EN") or key.endswith("ES"):
                        key = key[0:-2]
                        lang = line[sep-2:sep]
                        if isData:
                            # si los datos están en la sección "------- Data
                            # se añade al diccionario de datos
                            # dataEN o dataES
                            if lang == "EN":
                                dataEN[key] = value.replace("\\n","\n")
                            elif lang == "ES":
                                dataES[key] = value.replace("\\n","\n")
                        else:
                            # en caso contrario, se considera una lista y los elementos de esta lista se añaden al diccionario de datos
                            # listsEN o listsES
                            liste = [e.strip() for e in value.split('|')]
                            liste = [e.strip()
                                     for e in liste if len(e.strip()) > 0]
                            if lang == "EN":
                                listsEN[key] = liste
                            elif lang == "ES":
                                listsES[key] = liste
                    else:
                        # todas las claves desconocidas que no terminan en EN o ES se almacenan en una propiedad 'misc' del resultado
                        data['misc'][key] = value

            if isDescEN:
                descrEN += line
            elif isDescES:
                descrES += line

        data['descrEN'] = descrEN.strip()
        data['descrES'] = descrES.strip()
        data['listsEN'] = listsEN
        data['listsES'] = listsES
        data['dataEN'] = dataEN
        data['dataES'] = dataES

    else:
        print("Invalid path: %s" % filepath)
        exit(1)

    if not 'nameEN' in data or not 'descrEN' in data:
        print_error("Invalid data: %s" % filepath)
        exit(1)

    return data

#
# esta función escribe los datos con los beneficios además
#

def dataToFile(data, filepath):
    with open(filepath, 'w', encoding='utf8') as df:
        if "parentName" in data and data['parentName'] is not None:
            df.write('Parent Name: ' + data['parentName'] + '\n')
        if "journal" in data and data['journal'] is not None:
            df.write('Journal: ' + data['journal'] + '\n')
        df.write('Name: ' + data['nameEN'] + '\n')
        df.write('Nombre: ' + data['nameES'] + '\n')

        if data['listsEN']:
            for key in data['listsEN']:
                try:
                    df.write("%sEN: %s\n" %
                             (key, "|".join(data['listsEN'][key]).replace("\n"," ")))
                except TypeError:
                    values = ""
                    for item in data['listsEN'][key]:
                        for e in item.values():
                            values += "|" + e
                    df.write("%sEN: %s\n" % (key, values.replace("\n"," ")))
                try:
                    df.write("%sES: %s\n" % (key, "|".join(
                        data['listsES'][key]) if key in data['listsES'] else ""))
                except TypeError:
                    values = ""
                    for item in data['listsES'][key]:
                        for e in item.values():
                            values += "|" + e
                    df.write("%sES: %s\n" % (key, values))

        df.write('Estado: ' + data['status'] + '\n')
        if 'oldstatus' in data:
            df.write('Estado original: ' + data['oldstatus'] + '\n')
        df.write('\n')
        if 'benefitsEN' in data or 'benefitsES' in data:
            df.write('------ Benefits ----' + '\n')
            if 'benefitsEN' in data:
                df.write("Benefits: %s\n" % data['benefitsEN'])
            if 'benefitsES' in data:
                df.write("Beneficios: %s\n" % data['benefitsES'])
        if 'spoilersEN' in data or 'spoilersES' in data:
            df.write('------ Spoilers ----' + '\n')
            if 'spoilersEN' in data:
                df.write("SpoilersEN: %s\n" % data['spoilersEN'])
            if 'spoilersES' in data:
                df.write("SpoilersES: %s\n" % data['spoilersES'])

        if data['dataEN']:
            df.write('------ Data ------' + '\n')
            for key in data['dataEN']:
                if data['dataEN'][key] and len(data['dataEN'][key]) > 0:
                    df.write("%sEN: %s\n" % (key, data['dataEN'][key].replace("\n","\\n")))
                    if 'dataES' in data and key in data['dataES'] and data['dataES'][key] and len(data['dataES'][key]) > 0:
                        df.write("%sES: %s\n" % (
                            key, data['dataES'][key].replace("\n","\\n") if key in data['dataES'] else ""))
                    else:
                        df.write("%sES: \n" % key)

        df.write('------ Description (en) ------' + '\n')
        df.write(data['descrEN'] + '\n')
        df.write('------ Description (es) ------' + '\n')
        if len(data['descrES']) > 0:
            df.write(data['descrES'] + '\n')

    return data

#
# devuelve true si la entrada es válida
#

def isValid(data):
    return data['nameES'] and len(data['nameES']) > 0

#
# esta función lee todos los ficheros de un directorio (datos)
# y genera un diccionario basado en los identificadores
#

def readFolder(path):
    resultById = {}
    resultByName = {}
    if not os.path.exists(path):
        return [resultById, resultByName, True]
    all_files = os.listdir(path)
    has_errors = False

    # read all files in folder
    for fpath in all_files:

        if fpath[0] != ".":
            data = fileToData(path + fpath)
            data['filename'] = fpath

            if data['id'] in resultById:
                print_error("Duplicate data %s and %s, please fix it manually!" % (
                    path + resultById[data['id']]['filename'], path + data['filename']))
                has_errors = True
            else:
                resultById[data['id']] = data
                resultByName[data['nameEN']] = data

    return [resultById, resultByName, has_errors]

#
# Comprueba si la cadena de texto está vacía, y devuelve None si lo está
#

def emptyAsNull(value, empty=None):
    if value is None:
        return None
    if empty is not None and value == empty:
        return None
    if isinstance(value, str) and len(value) == 0:
        return None
    if isinstance(value, list) and len(value) == 0:
        return None
    return value

#
# Intenta convertir el valor dado a un entero
# Si la conversión no es posible, devuelve el valor tal cual sin modificarlo
#

def tryIntOrNone(value):
    # si el valor es None, se devuelve None
    if value is None:
        return None

    # si el valor es una cadena correspondiente a un número con un signo - al principio y luego algunos dígitos
    # transformarlo en un int
    if isinstance(value, str) and re.match('^-?\d+$', value):
        return int(value)

    # de lo contrario no hacemos nada
    return value

#
# intenta cargar un elemento del diccionario anidado que coincida con las propiedades dadas.
# devuelve None en cuanto una clave no está presente en el diccionario.
#
# alternativa a los indexadores [''] para evitar colapsos
#
# ex: tryGetDict(dict, 'data', 'name', 'value')
#     equivale a que dict['data']['name']['value'] devuelva None si el diccionario no contiene ninguna de las claves
#

def tryGetDict(dict: dict, *args: str):
    i = 0
    node = dict
    while i < len(args) and node is not None and args[i] in node:
        node = node[args[i]]
        i = i + 1
    if i != len(args):
        return None
    return node

#
# añadir un elemento clave con valor al diccionario dict sólo si el valor no es None
#

def addIfNotNull(dict: dict, key: str, value: any):
    if value is None:
        return
    dict[key] = value

