#!/bin/bash
##
## Simple script para ejecutar después de build-manual.sh
## para poner todos los datos a cero
##
git reset ../data
rm -rf ../data
git checkout ../data

git reset ../babele*
rm -rf ../babele*
git checkout ../babele*

git reset ../module.json
git checkout ../module.json

git status
