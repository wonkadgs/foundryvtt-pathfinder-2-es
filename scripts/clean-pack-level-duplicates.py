#!/usr/bin/python3
# -*- coding: utf-8 -*-
##
## Este script permite limpiar un directorio de traducción eliminando elementos duplicados debido a la eliminación de
## niveles
##
import os
import re

from libdata import readFolder, dataToFile, getPacks, getValue, getList, equals, print_error, print_warning
# =================================
#  ---     PACK TO ARCHIVE      ---
# =================================
pack_to_archive = "spell-effects"

ROOT = "../"
print('Starting archiving on %s pack' % (pack_to_archive))

# =====================================
# create archive directory if necessary
# =====================================
if not os.path.exists("%sarchive" % (ROOT)):
  os.makedirs("%sarchive" % (ROOT))
if not os.path.exists("%sarchive/%s/" % (ROOT, pack_to_archive)):
  os.makedirs("%sarchive/%s/" % (ROOT, pack_to_archive))

# ==========================
# read translations
# ==========================
path = "%sdata/%s/" % (ROOT, pack_to_archive)
all_files = os.listdir(path)
filtered_files = list(filter(lambda f: re.search('^\d{2}-', f), all_files))

set_all_files = set(all_files)
for level_file in filtered_files:
  file_name = re.split("-", level_file)[1]
  if set([file_name]).issubset(set_all_files):
    os.replace("%sdata/%s/%s" % (ROOT, pack_to_archive, level_file),
                  "%sarchive/%s/%s" % (ROOT, pack_to_archive, level_file))
    print("Archiving %s" % level_file)