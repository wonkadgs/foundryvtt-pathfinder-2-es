#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import os            
import datetime

from libdata import *
from statuses import *

BABELE="../babele/es/"
BABELE_VE_VO="../babele-alt/ve-vo/es/"
BABELE_VO_VE="../babele-alt/vo-ve/es/"
BABELE_VO="../babele-alt/vo/es/"

packs = getPacks()
translations = {}

#
# esta función añade dinámicamente campos de lista
#
def addLists(pack, data, entry, mapping_prefix):
  if "lists" in pack and "listsES" in data:
    for k in pack["lists"]:
      if len(data["listsES"][k]) > 0:
        if k == "Prereq":
          json_array = []
          for item in data["listsES"][k]:
            json_array += [{ "value": item }]
          entry[mapping_prefix+k.lower()] = json_array
        else:
          entry[mapping_prefix+k.lower()] = data["listsES"][k]

  if "extract" in pack:
    for k in pack["extract"]:
      if k in data["dataES"] and len(data["dataES"][k].strip()) > 0:
        entry[mapping_prefix+k.lower()] = data["dataES"][k]


def handlePack(p, path, key, babele, babeleVeVo, babeleVoVe, babeleVo, isItems=False, isPages=False):
  preparedTranslations = []
  all_files = os.listdir(path)

  count = { ST_NINGUNA: 0, ST_LIBRE: 0, ST_OFICIAL: 0, ST_MODIFICADA: 0, ST_DUPLICADA: 0,
            ST_AUTO_TRAD: 0, ST_AUTO_GOOGTRAD: 0, ST_VACIA: 0 }

  # This in case of items, to prefix mappings
  mapping_prefix = "items-" if isItems else ""

  # add mappings (not for pages)
  if not isPages and 'desc' in p['paths']:
    babele['mapping'][mapping_prefix+"description"] = p['paths']['desc']
    babeleVeVo['mapping'][mapping_prefix+"description"] = p['paths']['desc']
    babeleVoVe['mapping'][mapping_prefix+"description"] = p['paths']['desc']
    babeleVo['mapping'][mapping_prefix+"description"] = p['paths']['desc']

  if "lists" in p:
    for k in p["lists"]:
      babele['mapping'][mapping_prefix+k.lower()] = p["lists"][k]
      babeleVeVo['mapping'][mapping_prefix+k.lower()] = p["lists"][k]
      babeleVoVe['mapping'][mapping_prefix+k.lower()] = p["lists"][k]
      babeleVo['mapping'][mapping_prefix+k.lower()] = p["lists"][k]

  if "extract" in p:
    for k in p["extract"]:
      babele['mapping'][mapping_prefix+k.lower()] = p["extract"][k]
      babeleVeVo['mapping'][mapping_prefix+k.lower()] = p["extract"][k]
      babeleVoVe['mapping'][mapping_prefix+k.lower()] = p["extract"][k]
      babeleVo['mapping'][mapping_prefix+k.lower()] = p["extract"][k]

  # read all files in folder
  for fpath in all_files:
    data = fileToData(path + fpath)
    if data['status'] == "auto-trad":
      data['descES'] = ""
    count[data['status']] += 1

    # prepare data
    match = re.search('/([^/]+/[^/]+\.htm)$', path + fpath)
    if not match:
      print("Invalid filename %s" % path + fpath)
      exit(1)

    preparedTranslations.append({
      'file': match.group(1),
      'name': data['nameEN'],
      'nombre': data['nameES'] if 'nameES' in data else "-",
      'link': "@UUID[Compendium.%s.%s]" % (key, data['id'])
    })

    if data['status'] == ST_NINGUNA \
            or data['status'] == ST_AUTO_GOOGTRAD  or data['status'] == ST_VACIA:
      continue
    elif not isValid(data):
      print("Skipping invalid entry %s" % path + fpath)
      continue

    # We use IDs for items
    if isItems:
      entry_id = data["id"]
    else:
      entry_id = data['nameEN']

    if(not isPages):
      entryVE = {'name': data['nameES'], mapping_prefix+"description": data['descrES']}
      entryVEVO = {'name': ("%s (%s)" % (data['nameES'], data['nameEN'])), mapping_prefix+"description": data['descrES']}
      entryVOVE = {'name': ("%s (%s)" % (data['nameEN'], data['nameES'])), mapping_prefix+"description": data['descrES']}
      entryVO = {'name': data['nameEN'], mapping_prefix+"description": data['descrES'] }
      # Initiatilse page dictionary if the compendium has pages (is a journal)
      if "pages" in p:
        entryVE['pages'] = {}
        entryVEVO['pages'] = {}
        entryVOVE['pages'] = {}
        entryVO['pages'] = {}
      addLists(p, data, entryVE, mapping_prefix)
      babele['entries'][entry_id] = entryVE
      addLists(p, data, entryVEVO, mapping_prefix)
      babeleVeVo['entries'][entry_id] = entryVEVO
      addLists(p, data, entryVOVE, mapping_prefix)
      babeleVoVe['entries'][entry_id] = entryVOVE
      addLists(p, data, entryVO, mapping_prefix)
      babeleVo['entries'][entry_id] = entryVO
    else:
      babele['entries'][data['journal']]["pages"][entry_id] = {"name": data['nameES'], "text": data["descrES"]}
      babeleVeVo['entries'][data['journal']]["pages"][entry_id] = {"name": ("%s (%s)" % (data['nameES'], data['nameEN'])), "text": data["descrES"]}
      babeleVoVe['entries'][data['journal']]["pages"][entry_id] = {"name": ("%s (%s)" % (data['nameEN'], data['nameES'])), "text": data["descrES"]}
      babeleVo['entries'][data['journal']]["pages"][entry_id] = {"name": data['nameEN'], "text": data["descrES"]}

  print("Estadísticas: " + path[8:][:-1])
  print(" - Traduciciones: %d (%s) %d (%s)" % (count[ST_OFICIAL], ST_OFICIAL, count[ST_LIBRE], ST_LIBRE))
  print(" - %s: %d" % (ST_MODIFICADA, count[ST_MODIFICADA]))
  print(" - No traducidas: %d - Auto-generadas: %d" % (count[ST_NINGUNA], count[ST_AUTO_TRAD]+count[ST_AUTO_GOOGTRAD]))
  print(" - %s: %d" % (ST_VACIA, count[ST_VACIA]))

  return preparedTranslations


for p in packs:
  module = p["module"] if "module" in p else "pf2e"
  key = "%s.%s" % (module,p["name"])
  path = "../data/%s/" % p["id"]
  packName = p["transl"]
  references = []
  if "reference" in p:
    if isinstance(p["reference"], list):
      for reference in p["reference"]:
        references.append("pf2e.%s" % reference)
    else:
      references = "pf2e.%s" % p["reference"]

  #Archivos de destino
  babele     = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}
  babeleVeVo = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}
  babeleVoVe = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}
  babeleVo   = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}

  # Generar JSON del paquete principal
  translations[p["id"]] = handlePack(p, path, "conditionitems" if key == "conditions" else key, babele, babeleVeVo, babeleVoVe, babeleVo)

  # Tratamiento de artículos (incorporación al archivo)
  if "items" in p:
    path = "../data/%s-items/" % p["id"]
    translations[p["id"]+"-items"] = handlePack(p["items"], path, key, babele, babeleVeVo, babeleVoVe, babeleVo, True)

  # Procesar páginas (añadir al archivo)
  if "pages" in p:
    path = "../data/%s-pages/" % p["id"]
    translations[p["id"]+"-pages"] = handlePack(p["pages"], path, key, babele, babeleVeVo, babeleVoVe, babeleVo, False, True)
  print(BABELE + key + ".json")
  with open(BABELE + key + ".json", 'w', encoding='utf-8') as f:
    json.dump(babele, f, ensure_ascii=False, indent=4)
  with open(BABELE_VE_VO + key + ".json", 'w', encoding='utf-8') as f:
    json.dump(babeleVeVo, f, ensure_ascii=False, indent=4)
  with open(BABELE_VO_VE + key + ".json", 'w', encoding='utf-8') as f:
    json.dump(babeleVoVe, f, ensure_ascii=False, indent=4)
  with open(BABELE_VO + key + ".json", 'w', encoding='utf-8') as f:
    json.dump(babeleVo, f, ensure_ascii=False, indent=4)

# ===============================
# Generación de diccionarios (EN)
# ===============================
content = "# Biblioteca\n\n"
content += "\n\nÚltima actualización: %s *(Hora de Canadá/Montreal)*" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
content += "\n\nEste archivo se genera automáticamente. NO MODIFICAR!\n\n"

packs = sorted(packs, key=lambda k: k['transl'])

for p in packs:
  packName = p["transl"]
  content += " * [%s](#%s)\n" % (packName, packName.lower().replace(' ', '-'))
  if 'items' in p:
    content += " * [%s](#%s)\n" % (packName + " (Items)", packName.lower().replace(' ', '-')+"-items")
  if 'pages' in p:
    content += " * [%s](#%s)\n" % (packName + " (Pages)", packName.lower().replace(' ', '-')+"-pages")
  
for p in packs: 
  packName = p["transl"]
  content += "\n\n## %s\n\n" % packName
  content += "| Nombre (EN)   | Nombre (ES)    | Enlace al compendio |\n"
  content += "|---------------|----------------|---------------------|\n"
  
  sortedList = sorted(translations[p["id"]], key=lambda k: k['name'])
  for el in sortedList:
    content += "|[%s](%s)|%s|`%s`|\n" % (el['name'], el['file'], el['nombre'], el['link'])

  if 'items' in p:
    content += "\n\n## %s (Items)\n\n" % packName
    content += "| Nombre (EN)   | Nombre (ES)    | Enlace al compendio |\n"
    content += "|---------------|----------------|---------------------|\n"

    sortedList = sorted(translations[p["id"]+"-items"], key=lambda k: k['name'])
    for el in sortedList:
      content += "|[%s](%s)|%s|`%s`|\n" % (el['name'], el['file'], el['nombre'], el['link'])
  
with open("../data/dictionnaire.md", 'w', encoding='utf-8') as f:
  f.write(content)
  

# ===============================
# generación de diccionarios (ES)
# ===============================

content = "# Biblioteca\n\n"
content += "\n\nÚltima actualización: %s *(Hora de Canadá/Montreal)*" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
content += "\n\nEste archivo se genera automáticamente. NO MODIFICAR!\n\n"

packs = sorted(packs, key=lambda k: k['transl'])

for p in packs:
  packName = p["transl"]
  content += " * [%s](#%s)\n" % (packName, packName.lower().replace(' ', '-'))
  if 'items' in p:
    content += " * [%s](#%s)\n" % (packName + " (Elementos)", packName.lower().replace(' ', '-')+"-items")
  if 'pages' in p:
    content += " * [%s](#%s)\n" % (packName + " (Páginas)", packName.lower().replace(' ', '-')+"-pages")

for p in packs:
  packName = p["transl"]
  content += "\n\n## %s\n\n" % packName
  content += "| Nombre (ES)   | Nombre (EN)    | Enlace al compendio |\n"
  content += "|---------------|----------------|---------------------|\n"

  sortedList = sorted(translations[p["id"]], key=lambda k: k['nombre'])
  for el in sortedList:
    content += "|[%s](%s)|%s|`%s`|\n" % (el['nombre'], el['file'], el['name'], el['link'])

  if 'items' in p:
    content += "\n\n## %s (Elementos)\n\n" % packName
    content += "| Nombre (ES)   | Nombre (EN)    | Enlace al compendio |\n"
    content += "|---------------|----------------|---------------------|\n"

    sortedList = sorted(translations[p["id"]+"-items"], key=lambda k: k['nombre'])
    for el in sortedList:
      content += "|[%s](%s)|%s|`%s`|\n" % (el['nombre'], el['file'], el['name'], el['link'])

with open("../data/dictionnaire-es.md", 'w', encoding='utf-8') as f:
  f.write(content)

