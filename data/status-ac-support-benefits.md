# Estado de la traducción (ac-support-benefits)

 * **auto-trad**: 45


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[2Jaz98WiQMNnqKCD.htm](ac-support-benefits/2Jaz98WiQMNnqKCD.htm)|Monitor Lizard Support Benefit|auto-trad|
|[3jZOVJsYwBQ7GPDi.htm](ac-support-benefits/3jZOVJsYwBQ7GPDi.htm)|Vampiric Animal Support Benefit|auto-trad|
|[4kkcfQdHAb4YTVNw.htm](ac-support-benefits/4kkcfQdHAb4YTVNw.htm)|Ghost Support Benefit|auto-trad|
|[5HnWXmOp8qNjfhdU.htm](ac-support-benefits/5HnWXmOp8qNjfhdU.htm)|Wolf Support Benefit|auto-trad|
|[7rhcqwMDt2xqnI1N.htm](ac-support-benefits/7rhcqwMDt2xqnI1N.htm)|Bird Support Benefit|auto-trad|
|[8b1G95eWa3DaTvw8.htm](ac-support-benefits/8b1G95eWa3DaTvw8.htm)|Cave Pterosaur Support Benefit|auto-trad|
|[8re1d4lM0Dn4sLto.htm](ac-support-benefits/8re1d4lM0Dn4sLto.htm)|Boar Support Benefit|auto-trad|
|[9Y5QKMvy0KsliE29.htm](ac-support-benefits/9Y5QKMvy0KsliE29.htm)|Moth Support Benefit|auto-trad|
|[Aj7JWFXIbF3qmvSX.htm](ac-support-benefits/Aj7JWFXIbF3qmvSX.htm)|Bat Support Benefit|auto-trad|
|[AvDlo1mgxXd7ZA8W.htm](ac-support-benefits/AvDlo1mgxXd7ZA8W.htm)|Bear Support Benefit|auto-trad|
|[bEkXle5FLNxnp3IE.htm](ac-support-benefits/bEkXle5FLNxnp3IE.htm)|Undead Bird of Prey Support Benefit|auto-trad|
|[bHaJZSm7SakAsk00.htm](ac-support-benefits/bHaJZSm7SakAsk00.htm)|Skeletal Servant Support Benefit|auto-trad|
|[bO92dKyk8V4aM0NP.htm](ac-support-benefits/bO92dKyk8V4aM0NP.htm)|Arboreal Sapling Support Benefit|auto-trad|
|[C41lCXjerYjMyOTI.htm](ac-support-benefits/C41lCXjerYjMyOTI.htm)|Riding Drake Support Benefit|auto-trad|
|[CKMQC3zu1XoGjeNd.htm](ac-support-benefits/CKMQC3zu1XoGjeNd.htm)|Skeletal Mount Support Benefit|auto-trad|
|[Du0wsxf7TitL6wSb.htm](ac-support-benefits/Du0wsxf7TitL6wSb.htm)|Dromaeosaur Support Benefit|auto-trad|
|[Fl1VEpYhmScEEon8.htm](ac-support-benefits/Fl1VEpYhmScEEon8.htm)|Capybara Support Benefit|auto-trad|
|[h65iGxPkfrT8DdPv.htm](ac-support-benefits/h65iGxPkfrT8DdPv.htm)|Shadow Hound Support Benefit|auto-trad|
|[HCQXxqMtH81jE1W5.htm](ac-support-benefits/HCQXxqMtH81jE1W5.htm)|Cat Support Benefit|auto-trad|
|[imNJpD4afn4UrLpu.htm](ac-support-benefits/imNJpD4afn4UrLpu.htm)|Undead Hand Support Benefit|auto-trad|
|[IUXX1lUd9kMzV4bq.htm](ac-support-benefits/IUXX1lUd9kMzV4bq.htm)|Water Wraith Support Benefit|auto-trad|
|[kecawjDhBFz5QA4f.htm](ac-support-benefits/kecawjDhBFz5QA4f.htm)|Rhinoceros Support Benefit|auto-trad|
|[kZvomyJobGrDs1MU.htm](ac-support-benefits/kZvomyJobGrDs1MU.htm)|Legchair Support Benefit|auto-trad|
|[lzaNwsSxjVPH1nzn.htm](ac-support-benefits/lzaNwsSxjVPH1nzn.htm)|Camel Support benefit|auto-trad|
|[mFTzDCqNL5EdwlGm.htm](ac-support-benefits/mFTzDCqNL5EdwlGm.htm)|Beetle Support Benefit|auto-trad|
|[N9fYaUuIgD6RQ7CY.htm](ac-support-benefits/N9fYaUuIgD6RQ7CY.htm)|Zombie Mount Support Benefit|auto-trad|
|[nDUwRm0TOtc2EX0O.htm](ac-support-benefits/nDUwRm0TOtc2EX0O.htm)|Cave Gecko Support Benefit|auto-trad|
|[nQwTbiyTHahsvmBi.htm](ac-support-benefits/nQwTbiyTHahsvmBi.htm)|Badger Support Benefit|auto-trad|
|[oLwBrrW33fIjSHLr.htm](ac-support-benefits/oLwBrrW33fIjSHLr.htm)|Terror Bird Support Benefit|auto-trad|
|[QEZUnvbdrgQJAUs0.htm](ac-support-benefits/QEZUnvbdrgQJAUs0.htm)|Vulture Support Benefit|auto-trad|
|[QiQvx7jtfrkGnf13.htm](ac-support-benefits/QiQvx7jtfrkGnf13.htm)|Pangolin Support Benefit|auto-trad|
|[Qm0iwIYyy2gbnYXe.htm](ac-support-benefits/Qm0iwIYyy2gbnYXe.htm)|Ape Support Benefit|auto-trad|
|[r7QN2lUVWXlcynMW.htm](ac-support-benefits/r7QN2lUVWXlcynMW.htm)|Elephant Support Benefit|auto-trad|
|[Si13B4VFANZN8mi1.htm](ac-support-benefits/Si13B4VFANZN8mi1.htm)|Skeletal Constrictor Support Benefit|auto-trad|
|[TDMhLC16cQOYBbmA.htm](ac-support-benefits/TDMhLC16cQOYBbmA.htm)|Triceratops Support Benefit|auto-trad|
|[trujzLuDqjQ7nJEO.htm](ac-support-benefits/trujzLuDqjQ7nJEO.htm)|Snake Support Benefit|auto-trad|
|[uZLtEOimbFnEDUKh.htm](ac-support-benefits/uZLtEOimbFnEDUKh.htm)|Ulgrem-Lurann Support Benefit|auto-trad|
|[wf20kx6pPGBWwUn2.htm](ac-support-benefits/wf20kx6pPGBWwUn2.htm)|Horse Support Benefit|auto-trad|
|[wlh1poSMLK9wk2Nw.htm](ac-support-benefits/wlh1poSMLK9wk2Nw.htm)|Hyena Support Benefit|auto-trad|
|[XrxIRaD1A5tpKm1t.htm](ac-support-benefits/XrxIRaD1A5tpKm1t.htm)|Tyrannosaurus Support Benefit|auto-trad|
|[xtXdDC5KrUqh7eCu.htm](ac-support-benefits/xtXdDC5KrUqh7eCu.htm)|Scorpion Support Benefit|auto-trad|
|[yAYlM3aIRYnqx6j8.htm](ac-support-benefits/yAYlM3aIRYnqx6j8.htm)|Zombie Support Benefit|auto-trad|
|[yfjeiYMC27YGgoGW.htm](ac-support-benefits/yfjeiYMC27YGgoGW.htm)|Crocodile Support Benefit|auto-trad|
|[YsLLV4J7L6fgzTXf.htm](ac-support-benefits/YsLLV4J7L6fgzTXf.htm)|Shark Support Benefit|auto-trad|
|[zE01vpjowhoKygig.htm](ac-support-benefits/zE01vpjowhoKygig.htm)|Zombie Carrion Bird Support Benefit|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
