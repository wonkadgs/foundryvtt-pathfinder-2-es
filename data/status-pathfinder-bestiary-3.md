# Estado de la traducción (pathfinder-bestiary-3)

 * **auto-trad**: 364
 * **modificada**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[01alXdlOUvAC6woS.htm](pathfinder-bestiary-3/01alXdlOUvAC6woS.htm)|Giant Vulture|auto-trad|
|[0A2XLkvOzDMOjC6Q.htm](pathfinder-bestiary-3/0A2XLkvOzDMOjC6Q.htm)|Clockwork Mage|auto-trad|
|[0HTju4vf8ADAAh2g.htm](pathfinder-bestiary-3/0HTju4vf8ADAAh2g.htm)|Air Wisp|auto-trad|
|[0JmtZzvzZU3HtYVp.htm](pathfinder-bestiary-3/0JmtZzvzZU3HtYVp.htm)|Squirrel Swarm|auto-trad|
|[0laxaxLySatd0Uii.htm](pathfinder-bestiary-3/0laxaxLySatd0Uii.htm)|Adult Sovereign Dragon|auto-trad|
|[0qqABck8p0lCe4xz.htm](pathfinder-bestiary-3/0qqABck8p0lCe4xz.htm)|Adult Sky Dragon|auto-trad|
|[0sQg5UM8dQY7fBhQ.htm](pathfinder-bestiary-3/0sQg5UM8dQY7fBhQ.htm)|Ganzi Martial Artist|auto-trad|
|[0yisb2wbIvfNqciD.htm](pathfinder-bestiary-3/0yisb2wbIvfNqciD.htm)|Elysian Titan|auto-trad|
|[1DzJjE7OnRDY5Pir.htm](pathfinder-bestiary-3/1DzJjE7OnRDY5Pir.htm)|Giant Opossum|auto-trad|
|[2DrerJYEJon5U6Fx.htm](pathfinder-bestiary-3/2DrerJYEJon5U6Fx.htm)|Slithering Pit|auto-trad|
|[2L2iQ9X8tYPH2K9s.htm](pathfinder-bestiary-3/2L2iQ9X8tYPH2K9s.htm)|Tidehawk|auto-trad|
|[2pQoqcUxyHsTLhjw.htm](pathfinder-bestiary-3/2pQoqcUxyHsTLhjw.htm)|Viper Swarm|auto-trad|
|[2vvPHlLhgDGr8fOF.htm](pathfinder-bestiary-3/2vvPHlLhgDGr8fOF.htm)|Shae|auto-trad|
|[324ZRtmDYulbw0CM.htm](pathfinder-bestiary-3/324ZRtmDYulbw0CM.htm)|Lifeleecher Brawler|auto-trad|
|[3OrdGXuPXSlrLlbf.htm](pathfinder-bestiary-3/3OrdGXuPXSlrLlbf.htm)|Wyrwood Sneak|auto-trad|
|[3SYFPEaAl4g5G3GK.htm](pathfinder-bestiary-3/3SYFPEaAl4g5G3GK.htm)|Kitsune Trickster|auto-trad|
|[3VTjHYQjp1aE27n4.htm](pathfinder-bestiary-3/3VTjHYQjp1aE27n4.htm)|Young Underworld Dragon|auto-trad|
|[4cfi0BksjHbFVY0A.htm](pathfinder-bestiary-3/4cfi0BksjHbFVY0A.htm)|Flumph|auto-trad|
|[4glVe36VTR8HTjcE.htm](pathfinder-bestiary-3/4glVe36VTR8HTjcE.htm)|Girtablilu Sentry|auto-trad|
|[4vwMHy39IQyb7I7p.htm](pathfinder-bestiary-3/4vwMHy39IQyb7I7p.htm)|Incutilis|auto-trad|
|[4WMThCqvHV1aaBwa.htm](pathfinder-bestiary-3/4WMThCqvHV1aaBwa.htm)|Animated Colossus|auto-trad|
|[5bBfMpcn0PuO1jTL.htm](pathfinder-bestiary-3/5bBfMpcn0PuO1jTL.htm)|Lava Worm Swarm|auto-trad|
|[5dZaPSltPYYIupeq.htm](pathfinder-bestiary-3/5dZaPSltPYYIupeq.htm)|Grioth Scout|auto-trad|
|[5qtnZRnS3WF5oTUq.htm](pathfinder-bestiary-3/5qtnZRnS3WF5oTUq.htm)|Thanatotic Titan|auto-trad|
|[5WAy9PYWu1PuQKXg.htm](pathfinder-bestiary-3/5WAy9PYWu1PuQKXg.htm)|Clacking Skull Swarm|auto-trad|
|[60bknqrpHs2lL4pt.htm](pathfinder-bestiary-3/60bknqrpHs2lL4pt.htm)|Rat Snake Swarm|auto-trad|
|[61atKNQVF73vWpqc.htm](pathfinder-bestiary-3/61atKNQVF73vWpqc.htm)|Divine Warden Of Nethys|auto-trad|
|[6A317pomsGPzW17M.htm](pathfinder-bestiary-3/6A317pomsGPzW17M.htm)|Munavri Spellblade|auto-trad|
|[6AvQH0XXccorLE6d.htm](pathfinder-bestiary-3/6AvQH0XXccorLE6d.htm)|Skinstitch|auto-trad|
|[6bkqOecc1n0PulCu.htm](pathfinder-bestiary-3/6bkqOecc1n0PulCu.htm)|Abrikandilu|auto-trad|
|[6OxiStysMq65xKgS.htm](pathfinder-bestiary-3/6OxiStysMq65xKgS.htm)|Kongamato|auto-trad|
|[6rtLd2rN1qd6eCqK.htm](pathfinder-bestiary-3/6rtLd2rN1qd6eCqK.htm)|Hieracosphinx|auto-trad|
|[6yc1exIcngUEvBQH.htm](pathfinder-bestiary-3/6yc1exIcngUEvBQH.htm)|Adhukait|auto-trad|
|[74Sw9n7c4sMaK9Rx.htm](pathfinder-bestiary-3/74Sw9n7c4sMaK9Rx.htm)|Stheno Harpist|auto-trad|
|[79pfivF3swvfsJE4.htm](pathfinder-bestiary-3/79pfivF3swvfsJE4.htm)|Caligni Vanguard|auto-trad|
|[7bTj2DC91yEdJiLq.htm](pathfinder-bestiary-3/7bTj2DC91yEdJiLq.htm)|Vilderavn|auto-trad|
|[7lSwznbhNb7THfZo.htm](pathfinder-bestiary-3/7lSwznbhNb7THfZo.htm)|Terror Shrike|auto-trad|
|[7M7mwhETGEJjYoiY.htm](pathfinder-bestiary-3/7M7mwhETGEJjYoiY.htm)|Kimenhul|auto-trad|
|[8Ds7BAEjgSSB32wS.htm](pathfinder-bestiary-3/8Ds7BAEjgSSB32wS.htm)|Deimavigga|auto-trad|
|[8HdbN5NwEolamidg.htm](pathfinder-bestiary-3/8HdbN5NwEolamidg.htm)|Vulpinal|auto-trad|
|[8LBJ0jKca3vLaUhx.htm](pathfinder-bestiary-3/8LBJ0jKca3vLaUhx.htm)|Festrog|auto-trad|
|[8lQf8PNcJvxwmqLd.htm](pathfinder-bestiary-3/8lQf8PNcJvxwmqLd.htm)|Kirin|auto-trad|
|[8M91u7Q3javRQVEY.htm](pathfinder-bestiary-3/8M91u7Q3javRQVEY.htm)|Raktavarna|auto-trad|
|[8S088wbZjUhx6IB7.htm](pathfinder-bestiary-3/8S088wbZjUhx6IB7.htm)|Khravgodon|auto-trad|
|[8zdRS4uemz3LCEzi.htm](pathfinder-bestiary-3/8zdRS4uemz3LCEzi.htm)|Ancient Sea Dragon (Spellcaster)|auto-trad|
|[92nVPdtlJR5uHzIl.htm](pathfinder-bestiary-3/92nVPdtlJR5uHzIl.htm)|Sabosan|auto-trad|
|[9HkUdRKoprwo27VN.htm](pathfinder-bestiary-3/9HkUdRKoprwo27VN.htm)|Clockwork Soldier|auto-trad|
|[9KZRWATLOGP7QGyM.htm](pathfinder-bestiary-3/9KZRWATLOGP7QGyM.htm)|Fire Wisp|auto-trad|
|[9PSCqGy7e2GDQpfU.htm](pathfinder-bestiary-3/9PSCqGy7e2GDQpfU.htm)|Love Siktempora|auto-trad|
|[9RNisBYwGOCQan1S.htm](pathfinder-bestiary-3/9RNisBYwGOCQan1S.htm)|Haniver|auto-trad|
|[9rugdliEg2udjROC.htm](pathfinder-bestiary-3/9rugdliEg2udjROC.htm)|Pufferfish|auto-trad|
|[9SW7NWlTisAjNwAu.htm](pathfinder-bestiary-3/9SW7NWlTisAjNwAu.htm)|Nyktera|auto-trad|
|[9uP6Dfv53H4Fa32c.htm](pathfinder-bestiary-3/9uP6Dfv53H4Fa32c.htm)|Consonite Choir|auto-trad|
|[A7TFAr1rBtKJyprn.htm](pathfinder-bestiary-3/A7TFAr1rBtKJyprn.htm)|Sulfur Zombie|auto-trad|
|[aAU3gHWd1a9DyQPH.htm](pathfinder-bestiary-3/aAU3gHWd1a9DyQPH.htm)|Nosferatu Overlord|auto-trad|
|[abSsuJM94EvvyQYw.htm](pathfinder-bestiary-3/abSsuJM94EvvyQYw.htm)|Giant Flying Squirrel|auto-trad|
|[ACTvXKXcw1RZQxrP.htm](pathfinder-bestiary-3/ACTvXKXcw1RZQxrP.htm)|Adult Forest Dragon|auto-trad|
|[aeLTz7wO9ajPaQ0V.htm](pathfinder-bestiary-3/aeLTz7wO9ajPaQ0V.htm)|Hermit Crab Swarm|auto-trad|
|[aepfNXXy2juozgzB.htm](pathfinder-bestiary-3/aepfNXXy2juozgzB.htm)|Weasel|auto-trad|
|[Al5OHM0hbWcqIplK.htm](pathfinder-bestiary-3/Al5OHM0hbWcqIplK.htm)|Wizard Sponge (Fiendish Temple)|auto-trad|
|[AlLBsYO3ax9OQzAK.htm](pathfinder-bestiary-3/AlLBsYO3ax9OQzAK.htm)|Tupilaq|auto-trad|
|[AmkhY7NErzNgbsN0.htm](pathfinder-bestiary-3/AmkhY7NErzNgbsN0.htm)|Narwhal|auto-trad|
|[an7tww93Y4pQ8HP6.htm](pathfinder-bestiary-3/an7tww93Y4pQ8HP6.htm)|Baykok|auto-trad|
|[aQusm2Uh1tw00IVb.htm](pathfinder-bestiary-3/aQusm2Uh1tw00IVb.htm)|Bore Worm Swarm|auto-trad|
|[aqvMwn6885CJEmCO.htm](pathfinder-bestiary-3/aqvMwn6885CJEmCO.htm)|Garuda|auto-trad|
|[aX0zhmJGzpnwCwMr.htm](pathfinder-bestiary-3/aX0zhmJGzpnwCwMr.htm)|Feral Skull Swarm|auto-trad|
|[AxfH7V8A38VVugYo.htm](pathfinder-bestiary-3/AxfH7V8A38VVugYo.htm)|Lampad|auto-trad|
|[AybyC0n2CWNbXAbK.htm](pathfinder-bestiary-3/AybyC0n2CWNbXAbK.htm)|Popobawa|auto-trad|
|[aZBp1m5C9nXRgxHA.htm](pathfinder-bestiary-3/aZBp1m5C9nXRgxHA.htm)|Trilobite|auto-trad|
|[BEzfxlrA0CNfh4Fr.htm](pathfinder-bestiary-3/BEzfxlrA0CNfh4Fr.htm)|Nosferatu Malefactor|auto-trad|
|[BMHkCVNiEbnmUVft.htm](pathfinder-bestiary-3/BMHkCVNiEbnmUVft.htm)|Toshigami|auto-trad|
|[BPmotFI9EoIqSatr.htm](pathfinder-bestiary-3/BPmotFI9EoIqSatr.htm)|Japalisura|auto-trad|
|[BrGvmcM6jl3xUs4d.htm](pathfinder-bestiary-3/BrGvmcM6jl3xUs4d.htm)|Cobbleswarm|auto-trad|
|[BZKNSyp1ATtn3JXj.htm](pathfinder-bestiary-3/BZKNSyp1ATtn3JXj.htm)|Adachros|auto-trad|
|[c7kP2W6zaZA9oxAd.htm](pathfinder-bestiary-3/c7kP2W6zaZA9oxAd.htm)|Giant Pangolin|auto-trad|
|[cbduxhlI7JbONLXF.htm](pathfinder-bestiary-3/cbduxhlI7JbONLXF.htm)|Shambler Troop|auto-trad|
|[CFzeAzMfGHkzCF7h.htm](pathfinder-bestiary-3/CFzeAzMfGHkzCF7h.htm)|Skunk|auto-trad|
|[ChzqT42N5waJZ9VS.htm](pathfinder-bestiary-3/ChzqT42N5waJZ9VS.htm)|Spiny Eurypterid|auto-trad|
|[CjAaXJDY4xpKqQEz.htm](pathfinder-bestiary-3/CjAaXJDY4xpKqQEz.htm)|Megalictis|auto-trad|
|[cKy95PZJt6lGCsJk.htm](pathfinder-bestiary-3/cKy95PZJt6lGCsJk.htm)|Kushtaka|auto-trad|
|[cKZtOsBlN3Qu8Kyq.htm](pathfinder-bestiary-3/cKZtOsBlN3Qu8Kyq.htm)|Clockwork Spy|auto-trad|
|[cMBXcfS0DuZ7O2vm.htm](pathfinder-bestiary-3/cMBXcfS0DuZ7O2vm.htm)|Pukwudgie|auto-trad|
|[cmzlnTgWcJjnISAK.htm](pathfinder-bestiary-3/cmzlnTgWcJjnISAK.htm)|Kokogiak|auto-trad|
|[CSPuBqtPITQt43Md.htm](pathfinder-bestiary-3/CSPuBqtPITQt43Md.htm)|Gathlain Wanderer|auto-trad|
|[cWlntwaa4HPFEf3u.htm](pathfinder-bestiary-3/cWlntwaa4HPFEf3u.htm)|Sturzstromer|auto-trad|
|[CXCdPqMRX58sBQ9G.htm](pathfinder-bestiary-3/CXCdPqMRX58sBQ9G.htm)|Hellknight Cavalry Brigade|auto-trad|
|[cXz5nWwlbRQ1g90y.htm](pathfinder-bestiary-3/cXz5nWwlbRQ1g90y.htm)|Young Sky Dragon|auto-trad|
|[CYzFHz8ZbU270z9N.htm](pathfinder-bestiary-3/CYzFHz8ZbU270z9N.htm)|Mokele-Mbembe|auto-trad|
|[D0IY6UW5I0R4GprS.htm](pathfinder-bestiary-3/D0IY6UW5I0R4GprS.htm)|Ancient Sky Dragon (Spellcaster)|auto-trad|
|[d1hv3x7syXaltxg4.htm](pathfinder-bestiary-3/d1hv3x7syXaltxg4.htm)|Young Sea Dragon (Spellcaster)|auto-trad|
|[d5UfBtz09fOXKSmr.htm](pathfinder-bestiary-3/d5UfBtz09fOXKSmr.htm)|Nemhaith|auto-trad|
|[dCJDmNm6WSKf0AY4.htm](pathfinder-bestiary-3/dCJDmNm6WSKf0AY4.htm)|Piranha Swarm|auto-trad|
|[DCzr8qOfSg2K7e3z.htm](pathfinder-bestiary-3/DCzr8qOfSg2K7e3z.htm)|Ancient Sea Dragon|auto-trad|
|[DIJQ1UvfDDnDP545.htm](pathfinder-bestiary-3/DIJQ1UvfDDnDP545.htm)|Cave Giant|auto-trad|
|[dkDbXG0boTkddHSG.htm](pathfinder-bestiary-3/dkDbXG0boTkddHSG.htm)|Melixie|auto-trad|
|[dKFzkdgTntTm8ydA.htm](pathfinder-bestiary-3/dKFzkdgTntTm8ydA.htm)|Wizard Sponge (Crypt)|auto-trad|
|[DmQM0QTSPJ7YtpMg.htm](pathfinder-bestiary-3/DmQM0QTSPJ7YtpMg.htm)|Girtablilu Seer|auto-trad|
|[dniiLeUhXaq5CElX.htm](pathfinder-bestiary-3/dniiLeUhXaq5CElX.htm)|Arboreal Reaper|auto-trad|
|[dtkq45qf18bENXBd.htm](pathfinder-bestiary-3/dtkq45qf18bENXBd.htm)|Kishi|auto-trad|
|[dVJkFPCqJcPjImdG.htm](pathfinder-bestiary-3/dVJkFPCqJcPjImdG.htm)|Mix Couatl|auto-trad|
|[e6r8AGxfp8PDXaZk.htm](pathfinder-bestiary-3/e6r8AGxfp8PDXaZk.htm)|Levaloch|auto-trad|
|[eGNpuEg60STItyGz.htm](pathfinder-bestiary-3/eGNpuEg60STItyGz.htm)|Cactus Leshy|auto-trad|
|[EGyCQDseM9FaCl78.htm](pathfinder-bestiary-3/EGyCQDseM9FaCl78.htm)|Chouchin-Obake|auto-trad|
|[EMT6L7RarzkSiEOq.htm](pathfinder-bestiary-3/EMT6L7RarzkSiEOq.htm)|Living Rune (Arcane)|auto-trad|
|[eO3hNubEw16BC8UJ.htm](pathfinder-bestiary-3/eO3hNubEw16BC8UJ.htm)|Adult Sky Dragon (Spellcaster)|auto-trad|
|[EP8xhzy46zPWvhQL.htm](pathfinder-bestiary-3/EP8xhzy46zPWvhQL.htm)|Squirming Swill|auto-trad|
|[epTO8fPDjyy2WhzD.htm](pathfinder-bestiary-3/epTO8fPDjyy2WhzD.htm)|Mithral Golem|auto-trad|
|[esw400AugH9XWq5p.htm](pathfinder-bestiary-3/esw400AugH9XWq5p.htm)|Hekatonkheires Titan|auto-trad|
|[EvLBhZSMrzVDk4mM.htm](pathfinder-bestiary-3/EvLBhZSMrzVDk4mM.htm)|Calikang|auto-trad|
|[ew52XP0hjUACnidH.htm](pathfinder-bestiary-3/ew52XP0hjUACnidH.htm)|Blood Hag|auto-trad|
|[eYKWJCYNqqp1rp2i.htm](pathfinder-bestiary-3/eYKWJCYNqqp1rp2i.htm)|Nightgaunt|auto-trad|
|[F4aCBm1lfPlRQzZ1.htm](pathfinder-bestiary-3/F4aCBm1lfPlRQzZ1.htm)|Ghoran Manipulator|auto-trad|
|[fcFQ2GDUZ9YAhiDC.htm](pathfinder-bestiary-3/fcFQ2GDUZ9YAhiDC.htm)|Animated Trebuchet|auto-trad|
|[fHczwj1B8ULKRSJk.htm](pathfinder-bestiary-3/fHczwj1B8ULKRSJk.htm)|Triumph Siktempora|auto-trad|
|[FHZPjbRmJCqnhdal.htm](pathfinder-bestiary-3/FHZPjbRmJCqnhdal.htm)|Mage-Eater Worm Swarm|auto-trad|
|[Fj3CXqR0Y6kgvcBE.htm](pathfinder-bestiary-3/Fj3CXqR0Y6kgvcBE.htm)|Kodama|auto-trad|
|[FjiovYhLSHDX0ODl.htm](pathfinder-bestiary-3/FjiovYhLSHDX0ODl.htm)|Tattoo Guardian|auto-trad|
|[FjZ737XIvSOSaYcA.htm](pathfinder-bestiary-3/FjZ737XIvSOSaYcA.htm)|Tyrannosaurus Skeleton|auto-trad|
|[fLyIWlTCW3cUPjvd.htm](pathfinder-bestiary-3/fLyIWlTCW3cUPjvd.htm)|Animated Furnace|auto-trad|
|[FOG8qK1bWVxp7vmE.htm](pathfinder-bestiary-3/FOG8qK1bWVxp7vmE.htm)|Empress Bore Worm|auto-trad|
|[fuCPoL2OVcQf5uT9.htm](pathfinder-bestiary-3/fuCPoL2OVcQf5uT9.htm)|Ovinnik|auto-trad|
|[fViWNHKjjL7fYbjW.htm](pathfinder-bestiary-3/fViWNHKjjL7fYbjW.htm)|Common Eurypterid|auto-trad|
|[FvywQFbFzwLV8mvW.htm](pathfinder-bestiary-3/FvywQFbFzwLV8mvW.htm)|Flaming Skull|auto-trad|
|[FXwgsLTRneGzclsw.htm](pathfinder-bestiary-3/FXwgsLTRneGzclsw.htm)|Ioton|auto-trad|
|[GD8yAL8R8oft23Ml.htm](pathfinder-bestiary-3/GD8yAL8R8oft23Ml.htm)|Fading Fox|auto-trad|
|[Gf3g6wQquSjJhFMC.htm](pathfinder-bestiary-3/Gf3g6wQquSjJhFMC.htm)|Samsaran Anchorite|auto-trad|
|[GGuT6YsRH4aXhpl3.htm](pathfinder-bestiary-3/GGuT6YsRH4aXhpl3.htm)|Zombie Dragon|auto-trad|
|[GHv9BN8JQEPJVp5n.htm](pathfinder-bestiary-3/GHv9BN8JQEPJVp5n.htm)|Guecubu|auto-trad|
|[GPP2YJd2CkXSlLok.htm](pathfinder-bestiary-3/GPP2YJd2CkXSlLok.htm)|Duende|auto-trad|
|[gQkqsdfkmws4oADW.htm](pathfinder-bestiary-3/gQkqsdfkmws4oADW.htm)|Werecrocodile|auto-trad|
|[H796hayJm3J7MYJg.htm](pathfinder-bestiary-3/H796hayJm3J7MYJg.htm)|Amphisbaena|auto-trad|
|[H7PDLOIbDG9zt4H1.htm](pathfinder-bestiary-3/H7PDLOIbDG9zt4H1.htm)|Yzobu|auto-trad|
|[H8PaAZanEOT85KjD.htm](pathfinder-bestiary-3/H8PaAZanEOT85KjD.htm)|Vine Leshy|auto-trad|
|[HEgGta4uVmwdkw3E.htm](pathfinder-bestiary-3/HEgGta4uVmwdkw3E.htm)|Nikaramsa|auto-trad|
|[hh7OuFB5BQIrfeRT.htm](pathfinder-bestiary-3/hh7OuFB5BQIrfeRT.htm)|Kasa-Obake|auto-trad|
|[HhuHFmaq69ekSgEl.htm](pathfinder-bestiary-3/HhuHFmaq69ekSgEl.htm)|Shantak|auto-trad|
|[HiazGJoPkJ3gQVAO.htm](pathfinder-bestiary-3/HiazGJoPkJ3gQVAO.htm)|Grioth Cultist|auto-trad|
|[hiDSC6gqneQTB106.htm](pathfinder-bestiary-3/hiDSC6gqneQTB106.htm)|Fuath|auto-trad|
|[hNW3X8MbQQ9pUMiR.htm](pathfinder-bestiary-3/hNW3X8MbQQ9pUMiR.htm)|Buso Farmer|auto-trad|
|[HoBmSopFM5TjlmBj.htm](pathfinder-bestiary-3/HoBmSopFM5TjlmBj.htm)|Young Sovereign Dragon (Spellcaster)|auto-trad|
|[HObVT8aJnsx5nnqu.htm](pathfinder-bestiary-3/HObVT8aJnsx5nnqu.htm)|Tikbalang|auto-trad|
|[hOgYpdscvGo4MHHo.htm](pathfinder-bestiary-3/hOgYpdscvGo4MHHo.htm)|Krampus|auto-trad|
|[HPVVewX9vqKH94xf.htm](pathfinder-bestiary-3/HPVVewX9vqKH94xf.htm)|Tooth Fairy|auto-trad|
|[i3Ui3hHIBZnHl0Le.htm](pathfinder-bestiary-3/i3Ui3hHIBZnHl0Le.htm)|Globster|auto-trad|
|[iD32uhsjUGLvC2q6.htm](pathfinder-bestiary-3/iD32uhsjUGLvC2q6.htm)|Bone Ship|auto-trad|
|[ie5MyxqTqGlxzgsH.htm](pathfinder-bestiary-3/ie5MyxqTqGlxzgsH.htm)|Ancient Forest Dragon|auto-trad|
|[IijUBFE1vhvgowhD.htm](pathfinder-bestiary-3/IijUBFE1vhvgowhD.htm)|Giant Porcupine|auto-trad|
|[iiXjQ1SchGiotpVp.htm](pathfinder-bestiary-3/iiXjQ1SchGiotpVp.htm)|Skeleton Infantry|auto-trad|
|[ilGMPBvjT9ovIiXB.htm](pathfinder-bestiary-3/ilGMPBvjT9ovIiXB.htm)|Gurgist Mauler|auto-trad|
|[iLoVkzve6Nu3gErr.htm](pathfinder-bestiary-3/iLoVkzve6Nu3gErr.htm)|Herexen|auto-trad|
|[IMrnOMr3GtUFKyuV.htm](pathfinder-bestiary-3/IMrnOMr3GtUFKyuV.htm)|Ice Worm Swarm|auto-trad|
|[iNgKGHzMOAHjWQeI.htm](pathfinder-bestiary-3/iNgKGHzMOAHjWQeI.htm)|Android Infiltrator|auto-trad|
|[Ir3N6RHfg6vXYkmN.htm](pathfinder-bestiary-3/Ir3N6RHfg6vXYkmN.htm)|Rosethorn Ram|auto-trad|
|[IsE3PvvjFfxzuQtE.htm](pathfinder-bestiary-3/IsE3PvvjFfxzuQtE.htm)|Sorcerous Sea Skull Swarm|auto-trad|
|[istUwJdW3Mlln2hb.htm](pathfinder-bestiary-3/istUwJdW3Mlln2hb.htm)|Giant Hermit Crab|auto-trad|
|[IyxWVWAi9BjKPxop.htm](pathfinder-bestiary-3/IyxWVWAi9BjKPxop.htm)|Living Graffiti (Chalk)|auto-trad|
|[JD5sD7vwwlU2DwJI.htm](pathfinder-bestiary-3/JD5sD7vwwlU2DwJI.htm)|Angazhani|auto-trad|
|[jdqCEhsHDs4ABh2X.htm](pathfinder-bestiary-3/jdqCEhsHDs4ABh2X.htm)|Jorogumo|auto-trad|
|[JGpz9B0QkkAcHT4e.htm](pathfinder-bestiary-3/JGpz9B0QkkAcHT4e.htm)|Empress Mage-Eater Worm|auto-trad|
|[JHrINFX7a7fYhP4w.htm](pathfinder-bestiary-3/JHrINFX7a7fYhP4w.htm)|Gliminal|auto-trad|
|[JKF2cMQEWkA5avCO.htm](pathfinder-bestiary-3/JKF2cMQEWkA5avCO.htm)|Shikigami|auto-trad|
|[JkJRKxuIGJ3DOD9L.htm](pathfinder-bestiary-3/JkJRKxuIGJ3DOD9L.htm)|Owb|auto-trad|
|[JlFBu8zobNq3daVF.htm](pathfinder-bestiary-3/JlFBu8zobNq3daVF.htm)|Necral Worm Swarm|auto-trad|
|[Jn35hqHlyzEyv0T7.htm](pathfinder-bestiary-3/Jn35hqHlyzEyv0T7.htm)|Ancient Forest Dragon (Spellcaster)|auto-trad|
|[JvT56DMG6vKiii0u.htm](pathfinder-bestiary-3/JvT56DMG6vKiii0u.htm)|Adult Forest Dragon (Spellcaster)|auto-trad|
|[JZMK7j7WGWlB3Jhw.htm](pathfinder-bestiary-3/JZMK7j7WGWlB3Jhw.htm)|Young Sea Dragon|auto-trad|
|[JZuQJnATcqljgGWn.htm](pathfinder-bestiary-3/JZuQJnATcqljgGWn.htm)|Adult Sea Dragon (Spellcaster)|auto-trad|
|[kBIl96jzJjOd0LkZ.htm](pathfinder-bestiary-3/kBIl96jzJjOd0LkZ.htm)|Namorrodor|auto-trad|
|[kCaEwjZyHXy7cJre.htm](pathfinder-bestiary-3/kCaEwjZyHXy7cJre.htm)|Trilobite Swarm|auto-trad|
|[KG1Zu3EK0wrNdsQC.htm](pathfinder-bestiary-3/KG1Zu3EK0wrNdsQC.htm)|Ittan-Momen|auto-trad|
|[KGAuzJaKfPAdn0It.htm](pathfinder-bestiary-3/KGAuzJaKfPAdn0It.htm)|Aghash|auto-trad|
|[KGiEFgiqiexNBfS3.htm](pathfinder-bestiary-3/KGiEFgiqiexNBfS3.htm)|Wihsaak|auto-trad|
|[krbmyD1SuPQb4QfF.htm](pathfinder-bestiary-3/krbmyD1SuPQb4QfF.htm)|Blood Painter|auto-trad|
|[kSk6QjH4wDGPPFrY.htm](pathfinder-bestiary-3/kSk6QjH4wDGPPFrY.htm)|Rokurokubi|auto-trad|
|[KSKettq5j3A7UsIh.htm](pathfinder-bestiary-3/KSKettq5j3A7UsIh.htm)|Phantom Beast|auto-trad|
|[KTzSpyphiJ78EnBd.htm](pathfinder-bestiary-3/KTzSpyphiJ78EnBd.htm)|Winter Hag|auto-trad|
|[kU0ZXzbHqHUIND6m.htm](pathfinder-bestiary-3/kU0ZXzbHqHUIND6m.htm)|Fortune Eater|auto-trad|
|[kuh9DOsFXybRZRlj.htm](pathfinder-bestiary-3/kuh9DOsFXybRZRlj.htm)|Tolokand|auto-trad|
|[kXc11R18rF28AgIf.htm](pathfinder-bestiary-3/kXc11R18rF28AgIf.htm)|Green Man|auto-trad|
|[L5cNazEKC5gASp41.htm](pathfinder-bestiary-3/L5cNazEKC5gASp41.htm)|Mothman|auto-trad|
|[lat5aZFMlWUR2Wbs.htm](pathfinder-bestiary-3/lat5aZFMlWUR2Wbs.htm)|Lampad Queen|auto-trad|
|[ldzHiLhLAHhwpVJe.htm](pathfinder-bestiary-3/ldzHiLhLAHhwpVJe.htm)|House Drake|auto-trad|
|[lhNceV0kAJozCUCI.htm](pathfinder-bestiary-3/lhNceV0kAJozCUCI.htm)|Feral Sea Skull Swarm|auto-trad|
|[LjpOnADaKe9ormfL.htm](pathfinder-bestiary-3/LjpOnADaKe9ormfL.htm)|Kuchisake-Onna|auto-trad|
|[lqDwO2xkBNNEZ57B.htm](pathfinder-bestiary-3/lqDwO2xkBNNEZ57B.htm)|Elder Wyrmwraith|auto-trad|
|[Lqm3acjjbLKuRUCf.htm](pathfinder-bestiary-3/Lqm3acjjbLKuRUCf.htm)|Peri|auto-trad|
|[lrSuRCrRjP3xBfRy.htm](pathfinder-bestiary-3/lrSuRCrRjP3xBfRy.htm)|Megatherium|auto-trad|
|[Ls2qYZDASu3VgXxo.htm](pathfinder-bestiary-3/Ls2qYZDASu3VgXxo.htm)|Ancient Sky Dragon|auto-trad|
|[LTSZ3LD1L7ZthoD2.htm](pathfinder-bestiary-3/LTSZ3LD1L7ZthoD2.htm)|Misery Siktempora|auto-trad|
|[lYOUeRxFhbTdOOGC.htm](pathfinder-bestiary-3/lYOUeRxFhbTdOOGC.htm)|Clacking Sea Skull Swarm|auto-trad|
|[m0w5VfUTRvRcGfba.htm](pathfinder-bestiary-3/m0w5VfUTRvRcGfba.htm)|Living Graffiti (Ink)|auto-trad|
|[mbu0D45HPRNAwgEU.htm](pathfinder-bestiary-3/mbu0D45HPRNAwgEU.htm)|Munagola|auto-trad|
|[MD6eXpxoSPO02fZY.htm](pathfinder-bestiary-3/MD6eXpxoSPO02fZY.htm)|Xiuh Couatl|auto-trad|
|[mgQSYE94vb2ICVjL.htm](pathfinder-bestiary-3/mgQSYE94vb2ICVjL.htm)|Locathah Hunter|auto-trad|
|[MJhb44wEqSMWtrfe.htm](pathfinder-bestiary-3/MJhb44wEqSMWtrfe.htm)|Angheuvore Flesh-Gnawer|auto-trad|
|[mK7FwVR1yyqeMlA4.htm](pathfinder-bestiary-3/mK7FwVR1yyqeMlA4.htm)|Seething Spirit|auto-trad|
|[MolnKBns2ePDFbAB.htm](pathfinder-bestiary-3/MolnKBns2ePDFbAB.htm)|Hellbound Attorney|auto-trad|
|[mRsiT9EtpbOQ7AeX.htm](pathfinder-bestiary-3/mRsiT9EtpbOQ7AeX.htm)|Draconal (Red)|auto-trad|
|[MtCiCanKa8EgZOm9.htm](pathfinder-bestiary-3/MtCiCanKa8EgZOm9.htm)|Three-Toed Sloth|auto-trad|
|[mupW1Mgec250lQiZ.htm](pathfinder-bestiary-3/mupW1Mgec250lQiZ.htm)|Umasi|auto-trad|
|[n1GmLT7b5Q579Tcf.htm](pathfinder-bestiary-3/n1GmLT7b5Q579Tcf.htm)|Trailgaunt|auto-trad|
|[N6nBHGfmlaeMiBMP.htm](pathfinder-bestiary-3/N6nBHGfmlaeMiBMP.htm)|Hellwasp Swarm|auto-trad|
|[n9xSjQA1YSlYc9p3.htm](pathfinder-bestiary-3/n9xSjQA1YSlYc9p3.htm)|Millindemalion|auto-trad|
|[NhHGDx6ChbrhAmbO.htm](pathfinder-bestiary-3/NhHGDx6ChbrhAmbO.htm)|Domovoi|auto-trad|
|[NikAalo85JWVE6d2.htm](pathfinder-bestiary-3/NikAalo85JWVE6d2.htm)|Nagaji Soldier|auto-trad|
|[nIXWanjtyklfwH7u.htm](pathfinder-bestiary-3/nIXWanjtyklfwH7u.htm)|Tiddalik|auto-trad|
|[NldgVUIHB3asozHm.htm](pathfinder-bestiary-3/NldgVUIHB3asozHm.htm)|Wyrmwraith|auto-trad|
|[nnI7oj1BcetLUTYo.htm](pathfinder-bestiary-3/nnI7oj1BcetLUTYo.htm)|Etioling Blightmage|auto-trad|
|[NoAyIhPpqJ1WE1pF.htm](pathfinder-bestiary-3/NoAyIhPpqJ1WE1pF.htm)|Kovintus Geomancer|auto-trad|
|[Nq0MR5YsuglgPi0m.htm](pathfinder-bestiary-3/Nq0MR5YsuglgPi0m.htm)|Ostovite|auto-trad|
|[NQzkW5D28zIGESBt.htm](pathfinder-bestiary-3/NQzkW5D28zIGESBt.htm)|Sumbreiva|auto-trad|
|[nr4E59Xu10nWYsyO.htm](pathfinder-bestiary-3/nr4E59Xu10nWYsyO.htm)|Giant Skunk|auto-trad|
|[nr8EXKLSejspbb3d.htm](pathfinder-bestiary-3/nr8EXKLSejspbb3d.htm)|Red Fox|auto-trad|
|[NSD8H0xzbSWOgt7x.htm](pathfinder-bestiary-3/NSD8H0xzbSWOgt7x.htm)|Stone Lion Cub|auto-trad|
|[nSpM8CroNk3J78SR.htm](pathfinder-bestiary-3/nSpM8CroNk3J78SR.htm)|Draconal (Yellow)|auto-trad|
|[NUWL7LHDqmP0c7OB.htm](pathfinder-bestiary-3/NUWL7LHDqmP0c7OB.htm)|Tooth Fairy Swarm|auto-trad|
|[nX3LIuvHqiKNrth9.htm](pathfinder-bestiary-3/nX3LIuvHqiKNrth9.htm)|Ancient Underworld Dragon|auto-trad|
|[nzF46X8zcUG0CvpV.htm](pathfinder-bestiary-3/nzF46X8zcUG0CvpV.htm)|Pakalchi|auto-trad|
|[o740a5FFLG834FkV.htm](pathfinder-bestiary-3/o740a5FFLG834FkV.htm)|Desert Giant|auto-trad|
|[O8iynsWnjObQ8TJl.htm](pathfinder-bestiary-3/O8iynsWnjObQ8TJl.htm)|Soul Skelm|auto-trad|
|[O9TQjp3scKfW8SZK.htm](pathfinder-bestiary-3/O9TQjp3scKfW8SZK.htm)|Shulsaga|auto-trad|
|[oE52gEbyrF4Dl3Go.htm](pathfinder-bestiary-3/oE52gEbyrF4Dl3Go.htm)|Yithian|auto-trad|
|[oIM21UCyiabwwHfo.htm](pathfinder-bestiary-3/oIM21UCyiabwwHfo.htm)|Elder Sphinx|auto-trad|
|[OiMWiQQuBYt6Yc73.htm](pathfinder-bestiary-3/OiMWiQQuBYt6Yc73.htm)|Adult Underworld Dragon (Spellcaster)|auto-trad|
|[ooyJuLQ3AivRwLpa.htm](pathfinder-bestiary-3/ooyJuLQ3AivRwLpa.htm)|Dretch|auto-trad|
|[OPeTxIUwkkAjC6T5.htm](pathfinder-bestiary-3/OPeTxIUwkkAjC6T5.htm)|Hyakume|auto-trad|
|[OPRuZ1cEuKkJGPMV.htm](pathfinder-bestiary-3/OPRuZ1cEuKkJGPMV.htm)|Earth Wisp|auto-trad|
|[oR8cm0Aj5FIDF67w.htm](pathfinder-bestiary-3/oR8cm0Aj5FIDF67w.htm)|Zetogeki|auto-trad|
|[OrO28a9h8kakTTj7.htm](pathfinder-bestiary-3/OrO28a9h8kakTTj7.htm)|Young Sovereign Dragon|auto-trad|
|[OsWFo87OQ4G67zMS.htm](pathfinder-bestiary-3/OsWFo87OQ4G67zMS.htm)|Kurobozu|auto-trad|
|[OTEJFZ03NDqtrhcj.htm](pathfinder-bestiary-3/OTEJFZ03NDqtrhcj.htm)|Hadrinnex|auto-trad|
|[pCDp2hdim12wonW5.htm](pathfinder-bestiary-3/pCDp2hdim12wonW5.htm)|Living Rune (Occult)|auto-trad|
|[pFUv7BzSjYTOo5mO.htm](pathfinder-bestiary-3/pFUv7BzSjYTOo5mO.htm)|Monkey|auto-trad|
|[phOYPM1OVAKPg68l.htm](pathfinder-bestiary-3/phOYPM1OVAKPg68l.htm)|Omox|auto-trad|
|[PJCeh8sj9Sm5Eqz8.htm](pathfinder-bestiary-3/PJCeh8sj9Sm5Eqz8.htm)|Draxie|auto-trad|
|[Pnw71fJ41j6Wx62M.htm](pathfinder-bestiary-3/Pnw71fJ41j6Wx62M.htm)|Harmona|auto-trad|
|[PVstJNeHeWLU2XoK.htm](pathfinder-bestiary-3/PVstJNeHeWLU2XoK.htm)|Cecaelia Trapper|auto-trad|
|[Pvuvyd4RKVyiVWlJ.htm](pathfinder-bestiary-3/Pvuvyd4RKVyiVWlJ.htm)|Mezlan|auto-trad|
|[PX8tlo804y4I1C8S.htm](pathfinder-bestiary-3/PX8tlo804y4I1C8S.htm)|Tylosaurus|auto-trad|
|[Q02Io3eFQpBad3vD.htm](pathfinder-bestiary-3/Q02Io3eFQpBad3vD.htm)|Stone Lion|auto-trad|
|[q2ja0fkdr4PeZOTE.htm](pathfinder-bestiary-3/q2ja0fkdr4PeZOTE.htm)|Ximtal|auto-trad|
|[qE0iZoeeOp7og5A5.htm](pathfinder-bestiary-3/qE0iZoeeOp7og5A5.htm)|Giant Seahorse|auto-trad|
|[QHl25x11pMTJ9SxN.htm](pathfinder-bestiary-3/QHl25x11pMTJ9SxN.htm)|Draconal (White)|auto-trad|
|[QJRi6WzEm0LbXjAc.htm](pathfinder-bestiary-3/QJRi6WzEm0LbXjAc.htm)|Doru|auto-trad|
|[QLxcPfaHfc1vmF1Y.htm](pathfinder-bestiary-3/QLxcPfaHfc1vmF1Y.htm)|Terra-Cotta Garrison|auto-trad|
|[qlYzd5vmhsFTNdbX.htm](pathfinder-bestiary-3/qlYzd5vmhsFTNdbX.htm)|Esipil|auto-trad|
|[qm0YqO9ik2U2Gjdx.htm](pathfinder-bestiary-3/qm0YqO9ik2U2Gjdx.htm)|Moon Hag|auto-trad|
|[qnFiUNUyH0zuG6hj.htm](pathfinder-bestiary-3/qnFiUNUyH0zuG6hj.htm)|Quintessivore|auto-trad|
|[QrD96KPUHGjjkLbL.htm](pathfinder-bestiary-3/QrD96KPUHGjjkLbL.htm)|Kappa|auto-trad|
|[QUzBzxRy6HLeK7ja.htm](pathfinder-bestiary-3/QUzBzxRy6HLeK7ja.htm)|Dybbuk|auto-trad|
|[qvVmYuERMib26Atc.htm](pathfinder-bestiary-3/qvVmYuERMib26Atc.htm)|Crossroads Guardian|auto-trad|
|[QWx9cIVUeP7dMOez.htm](pathfinder-bestiary-3/QWx9cIVUeP7dMOez.htm)|Roiling Incant (Evocation)|auto-trad|
|[QYkuUQNVml878cIy.htm](pathfinder-bestiary-3/QYkuUQNVml878cIy.htm)|Ledalusca|auto-trad|
|[QyuUubTtR38kafue.htm](pathfinder-bestiary-3/QyuUubTtR38kafue.htm)|Manticore Paaridar|auto-trad|
|[rBHAfYDWpEuzbzqV.htm](pathfinder-bestiary-3/rBHAfYDWpEuzbzqV.htm)|Street Skelm|auto-trad|
|[RCsFDD6nqNUX0gIa.htm](pathfinder-bestiary-3/RCsFDD6nqNUX0gIa.htm)|Chyzaedu|auto-trad|
|[rFpExrqWywVDPYdP.htm](pathfinder-bestiary-3/rFpExrqWywVDPYdP.htm)|Brainchild|auto-trad|
|[Rgy8OyRY5szgE6d0.htm](pathfinder-bestiary-3/Rgy8OyRY5szgE6d0.htm)|Empress Ice Worm|auto-trad|
|[RicPhk9hLC3dDjp5.htm](pathfinder-bestiary-3/RicPhk9hLC3dDjp5.htm)|Maharaja|auto-trad|
|[RjJzLQ1nuPtKvnXU.htm](pathfinder-bestiary-3/RjJzLQ1nuPtKvnXU.htm)|Shabti Redeemer|auto-trad|
|[rnxg09tUCFPGW8IS.htm](pathfinder-bestiary-3/rnxg09tUCFPGW8IS.htm)|Terror Bird|auto-trad|
|[rOBXOcxMcnFR2P9y.htm](pathfinder-bestiary-3/rOBXOcxMcnFR2P9y.htm)|Skull Peeler|auto-trad|
|[rOFpnEic0eJdaxiM.htm](pathfinder-bestiary-3/rOFpnEic0eJdaxiM.htm)|Kangaroo|auto-trad|
|[rqNNvJO0XEVHWwbW.htm](pathfinder-bestiary-3/rqNNvJO0XEVHWwbW.htm)|Wizard Sponge (Underwater)|auto-trad|
|[rr48cUJxGpII7jWz.htm](pathfinder-bestiary-3/rr48cUJxGpII7jWz.htm)|Feathered Bear|auto-trad|
|[rrkjmcNlODuIpbz6.htm](pathfinder-bestiary-3/rrkjmcNlODuIpbz6.htm)|Ouroboros|auto-trad|
|[RTTs4lvkcPz8u6IY.htm](pathfinder-bestiary-3/RTTs4lvkcPz8u6IY.htm)|Azarketi Explorer|auto-trad|
|[rVtBZrHnWM3lvSs7.htm](pathfinder-bestiary-3/rVtBZrHnWM3lvSs7.htm)|Brimorak|auto-trad|
|[rW6vTwkAEAH8AMGw.htm](pathfinder-bestiary-3/rW6vTwkAEAH8AMGw.htm)|Wayang Whisperblade|auto-trad|
|[RyFqNQ86931Y5tqO.htm](pathfinder-bestiary-3/RyFqNQ86931Y5tqO.htm)|Lovelorn|auto-trad|
|[RZ1SLG0gxrOYSKRs.htm](pathfinder-bestiary-3/RZ1SLG0gxrOYSKRs.htm)|Vanara Disciple|auto-trad|
|[S2DdLrq9V7M5WzM3.htm](pathfinder-bestiary-3/S2DdLrq9V7M5WzM3.htm)|Dvorovoi|auto-trad|
|[S6V3z6bE6G9J68tE.htm](pathfinder-bestiary-3/S6V3z6bE6G9J68tE.htm)|Ringhorn Ram|auto-trad|
|[SaNOrmVDvQGX5U1D.htm](pathfinder-bestiary-3/SaNOrmVDvQGX5U1D.htm)|Harpy Skeleton|auto-trad|
|[sCzgD99DO10Koovq.htm](pathfinder-bestiary-3/sCzgD99DO10Koovq.htm)|Silvanshee|auto-trad|
|[sESP4UwiJHeNUYEv.htm](pathfinder-bestiary-3/sESP4UwiJHeNUYEv.htm)|Monkey Swarm|auto-trad|
|[sFePZ5s3vMtC4YPx.htm](pathfinder-bestiary-3/sFePZ5s3vMtC4YPx.htm)|Draconal (Black)|auto-trad|
|[sgZGrUQrfH3QPdXF.htm](pathfinder-bestiary-3/sgZGrUQrfH3QPdXF.htm)|Caulborn|auto-trad|
|[ShcTh3dSwjxbtoGG.htm](pathfinder-bestiary-3/ShcTh3dSwjxbtoGG.htm)|Tzitzimitl|auto-trad|
|[SIE8CcEblKI9VwqO.htm](pathfinder-bestiary-3/SIE8CcEblKI9VwqO.htm)|Living Graffiti (Oil)|auto-trad|
|[SjvmOu8v9haaZE1p.htm](pathfinder-bestiary-3/SjvmOu8v9haaZE1p.htm)|Leng Ghoul|auto-trad|
|[sNeIVL8w7NPFtpK2.htm](pathfinder-bestiary-3/sNeIVL8w7NPFtpK2.htm)|Wizard Sponge (Fey Domain)|auto-trad|
|[sNEvW8qBxztZcI8p.htm](pathfinder-bestiary-3/sNEvW8qBxztZcI8p.htm)|Shrine Skelm|auto-trad|
|[SNOPmNj4ZEiFBAdv.htm](pathfinder-bestiary-3/SNOPmNj4ZEiFBAdv.htm)|Young Forest Dragon (Spellcaster)|auto-trad|
|[sp0CZ8B2IPw3bBVi.htm](pathfinder-bestiary-3/sp0CZ8B2IPw3bBVi.htm)|Huldra|auto-trad|
|[SQKdvPIhWkrHlkbn.htm](pathfinder-bestiary-3/SQKdvPIhWkrHlkbn.htm)|City Guard Squadron|auto-trad|
|[ss5f2tMeJWO3u8GU.htm](pathfinder-bestiary-3/ss5f2tMeJWO3u8GU.htm)|Sasquatch|auto-trad|
|[Su21mCjUxFJnoWGg.htm](pathfinder-bestiary-3/Su21mCjUxFJnoWGg.htm)|Azer|auto-trad|
|[SuI5sxy5cuc0lnsh.htm](pathfinder-bestiary-3/SuI5sxy5cuc0lnsh.htm)|Platecarpus|auto-trad|
|[szEyrpElcYBagqL2.htm](pathfinder-bestiary-3/szEyrpElcYBagqL2.htm)|Draconal (Green)|auto-trad|
|[tdSnY3lgYJnsvB8n.htm](pathfinder-bestiary-3/tdSnY3lgYJnsvB8n.htm)|Wizard Sponge|auto-trad|
|[tjtTHdIBP5QIAyS7.htm](pathfinder-bestiary-3/tjtTHdIBP5QIAyS7.htm)|Vishkanya Infiltrator|auto-trad|
|[tQBUoh5wLJXiFdX6.htm](pathfinder-bestiary-3/tQBUoh5wLJXiFdX6.htm)|Seaweed Leshy|auto-trad|
|[tQIYWBefvpemUVeJ.htm](pathfinder-bestiary-3/tQIYWBefvpemUVeJ.htm)|Nosferatu Thrall|auto-trad|
|[tr9bmyZ6CYl2FPnr.htm](pathfinder-bestiary-3/tr9bmyZ6CYl2FPnr.htm)|Valkyrie|auto-trad|
|[tvAlNMQluKDpfXMz.htm](pathfinder-bestiary-3/tvAlNMQluKDpfXMz.htm)|Living Rune (Divine)|auto-trad|
|[tWjY4BJMhawdqkD5.htm](pathfinder-bestiary-3/tWjY4BJMhawdqkD5.htm)|Ancient Sovereign Dragon (Spellcaster)|auto-trad|
|[TzltHdjikojp7Um7.htm](pathfinder-bestiary-3/TzltHdjikojp7Um7.htm)|Penanggalan|auto-trad|
|[U3rMc5sN05MempVX.htm](pathfinder-bestiary-3/U3rMc5sN05MempVX.htm)|Maftet Guardian|auto-trad|
|[uDebX6flGwrviGZK.htm](pathfinder-bestiary-3/uDebX6flGwrviGZK.htm)|Hesperid Queen|auto-trad|
|[uDNQyboLAiIxBatL.htm](pathfinder-bestiary-3/uDNQyboLAiIxBatL.htm)|Adult Sea Dragon|auto-trad|
|[ULfACJrDBnZLNBKj.htm](pathfinder-bestiary-3/ULfACJrDBnZLNBKj.htm)|Terra-Cotta Soldier|auto-trad|
|[uP6dE5adlWJ9DrFY.htm](pathfinder-bestiary-3/uP6dE5adlWJ9DrFY.htm)|Corrupted Relic|auto-trad|
|[UTqProdd8LA0X1BQ.htm](pathfinder-bestiary-3/UTqProdd8LA0X1BQ.htm)|Sepid|auto-trad|
|[UxiwQ2Nmvfk9Q9tC.htm](pathfinder-bestiary-3/UxiwQ2Nmvfk9Q9tC.htm)|Shaukeen|auto-trad|
|[v0bKmqnUHxPTFQu4.htm](pathfinder-bestiary-3/v0bKmqnUHxPTFQu4.htm)|Hesperid|auto-trad|
|[v0nvME08U3mZJWB3.htm](pathfinder-bestiary-3/v0nvME08U3mZJWB3.htm)|Betobeto-San|auto-trad|
|[v7nu3NMYTS0YyxH4.htm](pathfinder-bestiary-3/v7nu3NMYTS0YyxH4.htm)|Adlet|auto-trad|
|[V9SxfxUgljJR9xx5.htm](pathfinder-bestiary-3/V9SxfxUgljJR9xx5.htm)|Hatred Siktempora|auto-trad|
|[Vf4uzrQYTbENFFFF.htm](pathfinder-bestiary-3/Vf4uzrQYTbENFFFF.htm)|Young Sky Dragon (Spellcaster)|auto-trad|
|[VF81W91YRFgtBLli.htm](pathfinder-bestiary-3/VF81W91YRFgtBLli.htm)|Einherji|auto-trad|
|[Vi2p9VroteSHsSy9.htm](pathfinder-bestiary-3/Vi2p9VroteSHsSy9.htm)|Procyal|auto-trad|
|[vJAPvmWCjVGKr06E.htm](pathfinder-bestiary-3/vJAPvmWCjVGKr06E.htm)|Arboreal Archive|auto-trad|
|[VMZOQGY1x1of0XLm.htm](pathfinder-bestiary-3/VMZOQGY1x1of0XLm.htm)|Cunning Fox|auto-trad|
|[vOTFqODTDDC2BDLx.htm](pathfinder-bestiary-3/vOTFqODTDDC2BDLx.htm)|Rancorous Priesthood|auto-trad|
|[Vt28ucE0FUJYl1fD.htm](pathfinder-bestiary-3/Vt28ucE0FUJYl1fD.htm)|Coral Capuchin|auto-trad|
|[VUYg62jFjYB5Mxh0.htm](pathfinder-bestiary-3/VUYg62jFjYB5Mxh0.htm)|Scalescribe|auto-trad|
|[VVSTEyAnm9OSbfJ5.htm](pathfinder-bestiary-3/VVSTEyAnm9OSbfJ5.htm)|Werebat|auto-trad|
|[vWF79pVG3dbZIlId.htm](pathfinder-bestiary-3/vWF79pVG3dbZIlId.htm)|Young Forest Dragon|auto-trad|
|[vwzfmjR0Me6xPdTP.htm](pathfinder-bestiary-3/vwzfmjR0Me6xPdTP.htm)|Tomb Giant|auto-trad|
|[vzBsZqtGuj2FVLGj.htm](pathfinder-bestiary-3/vzBsZqtGuj2FVLGj.htm)|Nucol|auto-trad|
|[WfCLrFwwWSA7KRUu.htm](pathfinder-bestiary-3/WfCLrFwwWSA7KRUu.htm)|Living Rune (Primal)|auto-trad|
|[wLG0f6J8cgyCA0w4.htm](pathfinder-bestiary-3/wLG0f6J8cgyCA0w4.htm)|Zuishin|auto-trad|
|[wmKIB7cgWdAZ29mv.htm](pathfinder-bestiary-3/wmKIB7cgWdAZ29mv.htm)|Adult Underworld Dragon|auto-trad|
|[wnrgiB2PkaBC5gOQ.htm](pathfinder-bestiary-3/wnrgiB2PkaBC5gOQ.htm)|Animated Silverware Swarm|auto-trad|
|[WPRjynFmJ2p1MnT3.htm](pathfinder-bestiary-3/WPRjynFmJ2p1MnT3.htm)|Adult Sovereign Dragon (Spellcaster)|auto-trad|
|[Wq0Euk0RK6rhRDsN.htm](pathfinder-bestiary-3/Wq0Euk0RK6rhRDsN.htm)|Severed Head|auto-trad|
|[Wu6vmegKwR4bgLse.htm](pathfinder-bestiary-3/Wu6vmegKwR4bgLse.htm)|Dramofir|auto-trad|
|[wv0hiJIGQjTU1pnO.htm](pathfinder-bestiary-3/wv0hiJIGQjTU1pnO.htm)|Galvo|auto-trad|
|[wVMMl1jfxWyqU4yq.htm](pathfinder-bestiary-3/wVMMl1jfxWyqU4yq.htm)|Storm Hag|auto-trad|
|[XfHvnixZO9zVwZxC.htm](pathfinder-bestiary-3/XfHvnixZO9zVwZxC.htm)|Living Graffiti (Blood)|auto-trad|
|[XgCQnswAedPcwLck.htm](pathfinder-bestiary-3/XgCQnswAedPcwLck.htm)|Pairaka|auto-trad|
|[xHMiDdTkZA3HzVSJ.htm](pathfinder-bestiary-3/xHMiDdTkZA3HzVSJ.htm)|Eunemvro|auto-trad|
|[xIT2yHlwILLc5hgw.htm](pathfinder-bestiary-3/xIT2yHlwILLc5hgw.htm)|Amalgamite|auto-trad|
|[XmOYhscNHFw7M2G0.htm](pathfinder-bestiary-3/XmOYhscNHFw7M2G0.htm)|Rhu-Chalik|auto-trad|
|[XnHmGsR7bQCHTMdA.htm](pathfinder-bestiary-3/XnHmGsR7bQCHTMdA.htm)|Empress Necral Worm|auto-trad|
|[xnrXf66rFvAfyhE9.htm](pathfinder-bestiary-3/xnrXf66rFvAfyhE9.htm)|Palace Skelm|auto-trad|
|[XrSz2IIKbeYFGILW.htm](pathfinder-bestiary-3/XrSz2IIKbeYFGILW.htm)|Strix Kinmate|auto-trad|
|[xswHz64371Sb9Let.htm](pathfinder-bestiary-3/xswHz64371Sb9Let.htm)|Nightmarchers|auto-trad|
|[XwtH5kP5gkY42yWp.htm](pathfinder-bestiary-3/XwtH5kP5gkY42yWp.htm)|Clockwork Dragon|auto-trad|
|[xwTZZAEs1sf5RWCq.htm](pathfinder-bestiary-3/xwTZZAEs1sf5RWCq.htm)|Danava Titan|auto-trad|
|[XXsOK8ZUoqQATarG.htm](pathfinder-bestiary-3/XXsOK8ZUoqQATarG.htm)|Moose|auto-trad|
|[XyEvsURVDnJwb76F.htm](pathfinder-bestiary-3/XyEvsURVDnJwb76F.htm)|Bison|auto-trad|
|[Y8lQqtOgXYXDCPFg.htm](pathfinder-bestiary-3/Y8lQqtOgXYXDCPFg.htm)|Swordkeeper|auto-trad|
|[yCyZlDAaJ6cDYtB7.htm](pathfinder-bestiary-3/yCyZlDAaJ6cDYtB7.htm)|Wolliped|auto-trad|
|[YfosnJUwR0fSV7a8.htm](pathfinder-bestiary-3/YfosnJUwR0fSV7a8.htm)|Young Underworld Dragon (Spellcaster)|auto-trad|
|[Yg7R4UgB1FjF2Euu.htm](pathfinder-bestiary-3/Yg7R4UgB1FjF2Euu.htm)|Bauble Beast|auto-trad|
|[yi9g7D49uGvAdGDP.htm](pathfinder-bestiary-3/yi9g7D49uGvAdGDP.htm)|Mi-Go|auto-trad|
|[YLLyd6Jb4Zqmz0lo.htm](pathfinder-bestiary-3/YLLyd6Jb4Zqmz0lo.htm)|Aphorite Sharpshooter|auto-trad|
|[YO3aTYRNGXWiSRal.htm](pathfinder-bestiary-3/YO3aTYRNGXWiSRal.htm)|Sorcerous Skull Swarm|auto-trad|
|[Yq9TcUof5D117yns.htm](pathfinder-bestiary-3/Yq9TcUof5D117yns.htm)|Camel|auto-trad|
|[YsgpbtWVT3q0OLWv.htm](pathfinder-bestiary-3/YsgpbtWVT3q0OLWv.htm)|Empress Lava Worm|auto-trad|
|[ytYKPtPotjvsWoSl.htm](pathfinder-bestiary-3/ytYKPtPotjvsWoSl.htm)|Ancient Sovereign Dragon|auto-trad|
|[YX0CqhKKtRzoLkuP.htm](pathfinder-bestiary-3/YX0CqhKKtRzoLkuP.htm)|Water Wisp|auto-trad|
|[YzWAuyEVPLTyRoAy.htm](pathfinder-bestiary-3/YzWAuyEVPLTyRoAy.htm)|Ancient Underworld Dragon (Spellcaster)|auto-trad|
|[Z5RBfl8x39uFpDUn.htm](pathfinder-bestiary-3/Z5RBfl8x39uFpDUn.htm)|Myceloid|auto-trad|
|[ZDGYrJ68aTzZ2EtT.htm](pathfinder-bestiary-3/ZDGYrJ68aTzZ2EtT.htm)|Phantom Knight|auto-trad|
|[zdJgaVe6VRSfEE1n.htm](pathfinder-bestiary-3/zdJgaVe6VRSfEE1n.htm)|Caligni Caller|auto-trad|
|[zGco5QmokZpFgLes.htm](pathfinder-bestiary-3/zGco5QmokZpFgLes.htm)|Grimple|auto-trad|
|[zGtfiKku0td5E0VJ.htm](pathfinder-bestiary-3/zGtfiKku0td5E0VJ.htm)|Fossil Golem|auto-trad|
|[zMoJh88vXJQwSHsX.htm](pathfinder-bestiary-3/zMoJh88vXJQwSHsX.htm)|Abandoned Zealot|auto-trad|
|[ZMrydoEfgGUAJGNI.htm](pathfinder-bestiary-3/ZMrydoEfgGUAJGNI.htm)|Plague Giant|auto-trad|
|[ZOrG61DZ9aiv8poK.htm](pathfinder-bestiary-3/ZOrG61DZ9aiv8poK.htm)|Owb Prophet|auto-trad|
|[ZwtcCnW9CEs78WRC.htm](pathfinder-bestiary-3/ZwtcCnW9CEs78WRC.htm)|Mobogo|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[TeaF0WreNshQxbe8.htm](pathfinder-bestiary-3/TeaF0WreNshQxbe8.htm)|Wizard Sponge (Toxic Lair)|Esponja hechicera (Guarida tóxica)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
