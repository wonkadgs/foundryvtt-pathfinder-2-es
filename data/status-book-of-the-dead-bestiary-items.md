# Estado de la traducción (book-of-the-dead-bestiary-items)

 * **auto-trad**: 814
 * **modificada**: 9


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[048p1dvtj51r50vq.htm](book-of-the-dead-bestiary-items/048p1dvtj51r50vq.htm)|Lignify|auto-trad|
|[05F3xO5TlXZ9n8cg.htm](book-of-the-dead-bestiary-items/05F3xO5TlXZ9n8cg.htm)|Negative Healing|auto-trad|
|[06rJwQpNQ1H9tOn3.htm](book-of-the-dead-bestiary-items/06rJwQpNQ1H9tOn3.htm)|Darkvision|auto-trad|
|[09d8l407352ep4z4.htm](book-of-the-dead-bestiary-items/09d8l407352ep4z4.htm)|Claw|auto-trad|
|[0a0R5L9eHRL770lk.htm](book-of-the-dead-bestiary-items/0a0R5L9eHRL770lk.htm)|Breathsense (Precise) 60 feet|auto-trad|
|[0AkVpzKeetsxnaM0.htm](book-of-the-dead-bestiary-items/0AkVpzKeetsxnaM0.htm)|Negative Healing|auto-trad|
|[0aqvd9pAwLkOwZeo.htm](book-of-the-dead-bestiary-items/0aqvd9pAwLkOwZeo.htm)|Tortured Gaze|auto-trad|
|[0cJtxKvPeAJC8lOc.htm](book-of-the-dead-bestiary-items/0cJtxKvPeAJC8lOc.htm)|Improved Grab|auto-trad|
|[0cQO5TZGYHxwoqee.htm](book-of-the-dead-bestiary-items/0cQO5TZGYHxwoqee.htm)|Lifesense 30 feet|auto-trad|
|[0e0uF4QWaN68KIlV.htm](book-of-the-dead-bestiary-items/0e0uF4QWaN68KIlV.htm)|Paralyzing Claws|auto-trad|
|[0fo5e4nnx624my4t.htm](book-of-the-dead-bestiary-items/0fo5e4nnx624my4t.htm)|Weathering Aura|auto-trad|
|[0jGEzNSvC3mj1gAv.htm](book-of-the-dead-bestiary-items/0jGEzNSvC3mj1gAv.htm)|Swallow Whole|auto-trad|
|[0oBoPpJaPEder5Uj.htm](book-of-the-dead-bestiary-items/0oBoPpJaPEder5Uj.htm)|Scythe|auto-trad|
|[0VnHFn3wn3nNZ2Tp.htm](book-of-the-dead-bestiary-items/0VnHFn3wn3nNZ2Tp.htm)|Swallow Whole|auto-trad|
|[0xaci6r10kc1q6hz.htm](book-of-the-dead-bestiary-items/0xaci6r10kc1q6hz.htm)|Stunning Flurry|auto-trad|
|[0yqQhSvO3kn23ESU.htm](book-of-the-dead-bestiary-items/0yqQhSvO3kn23ESU.htm)|Devastating Blast|auto-trad|
|[0zj1icya9xc6t4m7.htm](book-of-the-dead-bestiary-items/0zj1icya9xc6t4m7.htm)|Consecration Vulnerability|auto-trad|
|[0zs0is6k2nndmc5h.htm](book-of-the-dead-bestiary-items/0zs0is6k2nndmc5h.htm)|Nerve Ending|auto-trad|
|[10pagf9v2vm39x4i.htm](book-of-the-dead-bestiary-items/10pagf9v2vm39x4i.htm)|Fist|auto-trad|
|[13h23z1r0zoaru7r.htm](book-of-the-dead-bestiary-items/13h23z1r0zoaru7r.htm)|Snatch|auto-trad|
|[14ufiu26cnai5yer.htm](book-of-the-dead-bestiary-items/14ufiu26cnai5yer.htm)|Demolition|auto-trad|
|[15ji0gz9dw3xxfog.htm](book-of-the-dead-bestiary-items/15ji0gz9dw3xxfog.htm)|Spray Black Bile|auto-trad|
|[15qn60fnmbbmi7qd.htm](book-of-the-dead-bestiary-items/15qn60fnmbbmi7qd.htm)|Bone|auto-trad|
|[164uke5yf2ifcf8e.htm](book-of-the-dead-bestiary-items/164uke5yf2ifcf8e.htm)|Reap|auto-trad|
|[1a7SbMUxtcojgqLl.htm](book-of-the-dead-bestiary-items/1a7SbMUxtcojgqLl.htm)|At-Will Spells|auto-trad|
|[1cs7olyf6afhvxbc.htm](book-of-the-dead-bestiary-items/1cs7olyf6afhvxbc.htm)|Onryo's Rancor|auto-trad|
|[1f5q331o2mt1wcdv.htm](book-of-the-dead-bestiary-items/1f5q331o2mt1wcdv.htm)|Soulbound Gallop|auto-trad|
|[1fa8oveae7es6ajn.htm](book-of-the-dead-bestiary-items/1fa8oveae7es6ajn.htm)|Fiddlestick|auto-trad|
|[1gmv4iqur5scrtnw.htm](book-of-the-dead-bestiary-items/1gmv4iqur5scrtnw.htm)|Soulscent (Imprecise) 200 feet|auto-trad|
|[1HdblgdIZkc4dN3e.htm](book-of-the-dead-bestiary-items/1HdblgdIZkc4dN3e.htm)|Vetalarana Vulnerabilities|auto-trad|
|[1lbqFqVnwC2oNkep.htm](book-of-the-dead-bestiary-items/1lbqFqVnwC2oNkep.htm)|Darkvision|auto-trad|
|[1mqppqclycua1e1x.htm](book-of-the-dead-bestiary-items/1mqppqclycua1e1x.htm)|Power of the Haunt|auto-trad|
|[1NWUcNdniZkhKfCb.htm](book-of-the-dead-bestiary-items/1NWUcNdniZkhKfCb.htm)|Darkvision|auto-trad|
|[1otpt2d2gyuzglcy.htm](book-of-the-dead-bestiary-items/1otpt2d2gyuzglcy.htm)|Claw|auto-trad|
|[1rzixvxa00esxhop.htm](book-of-the-dead-bestiary-items/1rzixvxa00esxhop.htm)|Filth Fever|auto-trad|
|[1T3U4mJBzZ7Nt8uF.htm](book-of-the-dead-bestiary-items/1T3U4mJBzZ7Nt8uF.htm)|Improved Grab|auto-trad|
|[1U8XEv359HH8seeK.htm](book-of-the-dead-bestiary-items/1U8XEv359HH8seeK.htm)|Divine Innate Spells|auto-trad|
|[1VGFADh8jJIfVH94.htm](book-of-the-dead-bestiary-items/1VGFADh8jJIfVH94.htm)|At-Will Spells|auto-trad|
|[1vq2jy6pqbxkwfde.htm](book-of-the-dead-bestiary-items/1vq2jy6pqbxkwfde.htm)|Servitor Realignment|auto-trad|
|[1y7qe4eff55izld8.htm](book-of-the-dead-bestiary-items/1y7qe4eff55izld8.htm)|Stinger|auto-trad|
|[1zQfinUzk9ky6EKF.htm](book-of-the-dead-bestiary-items/1zQfinUzk9ky6EKF.htm)|Weapon Master|auto-trad|
|[20w584tt1vgcah4n.htm](book-of-the-dead-bestiary-items/20w584tt1vgcah4n.htm)|Claw|auto-trad|
|[21j0rnxdp8gf5zsf.htm](book-of-the-dead-bestiary-items/21j0rnxdp8gf5zsf.htm)|Jaws|auto-trad|
|[21sp3kwgg8uh9p13.htm](book-of-the-dead-bestiary-items/21sp3kwgg8uh9p13.htm)|Dagger|auto-trad|
|[23gdXY5uSfl0Kp3I.htm](book-of-the-dead-bestiary-items/23gdXY5uSfl0Kp3I.htm)|Shifting Earth|auto-trad|
|[24C4VbeTjw3Ovvdn.htm](book-of-the-dead-bestiary-items/24C4VbeTjw3Ovvdn.htm)|Primal Innate Spells|auto-trad|
|[25v45nqumj44pd4b.htm](book-of-the-dead-bestiary-items/25v45nqumj44pd4b.htm)|Wear Skin|auto-trad|
|[266u1f6h9sli518d.htm](book-of-the-dead-bestiary-items/266u1f6h9sli518d.htm)|Urveth Venom|auto-trad|
|[26yzjYUTRsWSJIeP.htm](book-of-the-dead-bestiary-items/26yzjYUTRsWSJIeP.htm)|At-Will Spells|auto-trad|
|[26zb7felsd7fhdyg.htm](book-of-the-dead-bestiary-items/26zb7felsd7fhdyg.htm)|Float|auto-trad|
|[272bll9hrxa49hmf.htm](book-of-the-dead-bestiary-items/272bll9hrxa49hmf.htm)|Ghostly Swoop|auto-trad|
|[2ahqZc1dTw700QB2.htm](book-of-the-dead-bestiary-items/2ahqZc1dTw700QB2.htm)|Darkvision|auto-trad|
|[2aUUvMiYk0FR7Cfr.htm](book-of-the-dead-bestiary-items/2aUUvMiYk0FR7Cfr.htm)|Negative Healing|auto-trad|
|[2axamykyrsqyw3y1.htm](book-of-the-dead-bestiary-items/2axamykyrsqyw3y1.htm)|Undying Vendetta|auto-trad|
|[2ccxczu33z7f67dc.htm](book-of-the-dead-bestiary-items/2ccxczu33z7f67dc.htm)|Premonition of Death|auto-trad|
|[2d552nmuu8dx669n.htm](book-of-the-dead-bestiary-items/2d552nmuu8dx669n.htm)|Dagger|auto-trad|
|[2fv011dfxayfhevz.htm](book-of-the-dead-bestiary-items/2fv011dfxayfhevz.htm)|Water Vulnerability|auto-trad|
|[2h8eb5k9fufiveqm.htm](book-of-the-dead-bestiary-items/2h8eb5k9fufiveqm.htm)|Sand Rot|auto-trad|
|[2haw98dime2bu3qi.htm](book-of-the-dead-bestiary-items/2haw98dime2bu3qi.htm)|Shatter Block|auto-trad|
|[2mNv4HeeYz6pnwor.htm](book-of-the-dead-bestiary-items/2mNv4HeeYz6pnwor.htm)|Negative Healing|auto-trad|
|[2RyJ9XZZMEWMa6zK.htm](book-of-the-dead-bestiary-items/2RyJ9XZZMEWMa6zK.htm)|Darkvision|auto-trad|
|[2s8zOCsruWILWVA9.htm](book-of-the-dead-bestiary-items/2s8zOCsruWILWVA9.htm)|Darkvision|auto-trad|
|[2tfn09ud7vyhndbb.htm](book-of-the-dead-bestiary-items/2tfn09ud7vyhndbb.htm)|Fossil Fury|auto-trad|
|[2ujjbxpmiiodb9o5.htm](book-of-the-dead-bestiary-items/2ujjbxpmiiodb9o5.htm)|Slow|auto-trad|
|[2xqc5getycsdpc49.htm](book-of-the-dead-bestiary-items/2xqc5getycsdpc49.htm)|Talon|auto-trad|
|[2xvk1ovgd1lsv1ze.htm](book-of-the-dead-bestiary-items/2xvk1ovgd1lsv1ze.htm)|Vital Transfusion|auto-trad|
|[2ynj848nvp9c3igy.htm](book-of-the-dead-bestiary-items/2ynj848nvp9c3igy.htm)|Betray the Pack|auto-trad|
|[2yp36AhxUGVlWXgq.htm](book-of-the-dead-bestiary-items/2yp36AhxUGVlWXgq.htm)|Darkvision|auto-trad|
|[324ljVcWLv8U3PwG.htm](book-of-the-dead-bestiary-items/324ljVcWLv8U3PwG.htm)|Negative Healing|auto-trad|
|[32Oayck2ePyYYgli.htm](book-of-the-dead-bestiary-items/32Oayck2ePyYYgli.htm)|Negative Healing|auto-trad|
|[32onun6vxolii6qa.htm](book-of-the-dead-bestiary-items/32onun6vxolii6qa.htm)|Jaws|auto-trad|
|[35tm9k6kvybbcwkv.htm](book-of-the-dead-bestiary-items/35tm9k6kvybbcwkv.htm)|Aura of Whispers|auto-trad|
|[35ytvmm1cwqr4dbu.htm](book-of-the-dead-bestiary-items/35ytvmm1cwqr4dbu.htm)|Sudden Surge|auto-trad|
|[36cdvdo5yvfoxz7s.htm](book-of-the-dead-bestiary-items/36cdvdo5yvfoxz7s.htm)|Composite Longbow|auto-trad|
|[3BLhMeYajV3Rxi3L.htm](book-of-the-dead-bestiary-items/3BLhMeYajV3Rxi3L.htm)|Grab|auto-trad|
|[3G8FEWYsLeMf6cAv.htm](book-of-the-dead-bestiary-items/3G8FEWYsLeMf6cAv.htm)|Swallow Whole|auto-trad|
|[3gvb9wq37oq21be3.htm](book-of-the-dead-bestiary-items/3gvb9wq37oq21be3.htm)|Drain Life|auto-trad|
|[3hac6i5gkgu40340.htm](book-of-the-dead-bestiary-items/3hac6i5gkgu40340.htm)|Jaws|auto-trad|
|[3j8u75qe0rn2330n.htm](book-of-the-dead-bestiary-items/3j8u75qe0rn2330n.htm)|Flintlock Pistol|auto-trad|
|[3kzbeblthgly6zkx.htm](book-of-the-dead-bestiary-items/3kzbeblthgly6zkx.htm)|Time Shift|auto-trad|
|[3qz1msK40lJzzqhO.htm](book-of-the-dead-bestiary-items/3qz1msK40lJzzqhO.htm)|Grab|auto-trad|
|[3spjg2szu482ix18.htm](book-of-the-dead-bestiary-items/3spjg2szu482ix18.htm)|Drain Magic|auto-trad|
|[3t7zznqikrwn9cyb.htm](book-of-the-dead-bestiary-items/3t7zznqikrwn9cyb.htm)|Heavy Crossbow|auto-trad|
|[3whr45ahot8zh9nf.htm](book-of-the-dead-bestiary-items/3whr45ahot8zh9nf.htm)|Abdomen Cache|auto-trad|
|[3ybyexad76n3ge2g.htm](book-of-the-dead-bestiary-items/3ybyexad76n3ge2g.htm)|Servitor Assembly|auto-trad|
|[40McFjcRX4SifzsC.htm](book-of-the-dead-bestiary-items/40McFjcRX4SifzsC.htm)|Negative Healing|auto-trad|
|[40tw7u6c1rckcl0u.htm](book-of-the-dead-bestiary-items/40tw7u6c1rckcl0u.htm)|Bone Javelin|auto-trad|
|[41Dv5UzszJFoJotn.htm](book-of-the-dead-bestiary-items/41Dv5UzszJFoJotn.htm)|Negative Healing|auto-trad|
|[42bls3q0amg2osjy.htm](book-of-the-dead-bestiary-items/42bls3q0amg2osjy.htm)|Jaws|auto-trad|
|[42s7jyacejyvhl2u.htm](book-of-the-dead-bestiary-items/42s7jyacejyvhl2u.htm)|Infectious Visions|auto-trad|
|[43aba9wneEhm4kYI.htm](book-of-the-dead-bestiary-items/43aba9wneEhm4kYI.htm)|Darkvision|auto-trad|
|[43rtpaawc0hyjxs2.htm](book-of-the-dead-bestiary-items/43rtpaawc0hyjxs2.htm)|Sunlight Powerlessness|auto-trad|
|[44AaXfBWgXV0uMpL.htm](book-of-the-dead-bestiary-items/44AaXfBWgXV0uMpL.htm)|Darkvision|auto-trad|
|[479508pjo4uznsty.htm](book-of-the-dead-bestiary-items/479508pjo4uznsty.htm)|Final Snare|auto-trad|
|[47dj3m4gnycp9ovy.htm](book-of-the-dead-bestiary-items/47dj3m4gnycp9ovy.htm)|Vein Walker|auto-trad|
|[4fpg4t23cn5sl7dn.htm](book-of-the-dead-bestiary-items/4fpg4t23cn5sl7dn.htm)|Flail|auto-trad|
|[4KyzQNfgO9JMN6RH.htm](book-of-the-dead-bestiary-items/4KyzQNfgO9JMN6RH.htm)|Ghoul Fever|auto-trad|
|[4nn1859fvesmfedg.htm](book-of-the-dead-bestiary-items/4nn1859fvesmfedg.htm)|Shortsword|auto-trad|
|[4p83bgih48iq1inb.htm](book-of-the-dead-bestiary-items/4p83bgih48iq1inb.htm)|Claw|auto-trad|
|[4pt21hdckfee43y3.htm](book-of-the-dead-bestiary-items/4pt21hdckfee43y3.htm)|Mountain Sword|auto-trad|
|[4qk5llfpop3z00fj.htm](book-of-the-dead-bestiary-items/4qk5llfpop3z00fj.htm)|Fist|auto-trad|
|[4r9QDElzXGCRVfhh.htm](book-of-the-dead-bestiary-items/4r9QDElzXGCRVfhh.htm)|At-Will Spells|auto-trad|
|[4syq1lo2m1mx4330.htm](book-of-the-dead-bestiary-items/4syq1lo2m1mx4330.htm)|Agent of Despair|auto-trad|
|[4uc0r15oqcogaudt.htm](book-of-the-dead-bestiary-items/4uc0r15oqcogaudt.htm)|Voice Imitation|auto-trad|
|[4z31tccif8f7qaq7.htm](book-of-the-dead-bestiary-items/4z31tccif8f7qaq7.htm)|Spawn Hunter Wight|auto-trad|
|[4zbcbp0ersjqi242.htm](book-of-the-dead-bestiary-items/4zbcbp0ersjqi242.htm)|Curse Ye Scallywags!|auto-trad|
|[517tUMEgs6I4camL.htm](book-of-the-dead-bestiary-items/517tUMEgs6I4camL.htm)|Negative Healing|auto-trad|
|[53r0jhgz2tlvtcn9.htm](book-of-the-dead-bestiary-items/53r0jhgz2tlvtcn9.htm)|Accelerating Inquest|auto-trad|
|[55sqmv4kf6twrsh0.htm](book-of-the-dead-bestiary-items/55sqmv4kf6twrsh0.htm)|Foot|auto-trad|
|[56PK7jsheqUjQAKo.htm](book-of-the-dead-bestiary-items/56PK7jsheqUjQAKo.htm)|Sneak Attack|auto-trad|
|[56vkqrstijfjdh6c.htm](book-of-the-dead-bestiary-items/56vkqrstijfjdh6c.htm)|Ground Slam|auto-trad|
|[57b0x2wmtxjbx2ww.htm](book-of-the-dead-bestiary-items/57b0x2wmtxjbx2ww.htm)|Psychic Superiority|auto-trad|
|[581dy5lnt449p866.htm](book-of-the-dead-bestiary-items/581dy5lnt449p866.htm)|Spawn Wight Soldier|auto-trad|
|[58ox9o6ljire079b.htm](book-of-the-dead-bestiary-items/58ox9o6ljire079b.htm)|Wrath of the Haunt|auto-trad|
|[5aGqEha0KK3U7pFi.htm](book-of-the-dead-bestiary-items/5aGqEha0KK3U7pFi.htm)|-2 to All Saves vs. Curses|auto-trad|
|[5cP06WMcn0WgZIXu.htm](book-of-the-dead-bestiary-items/5cP06WMcn0WgZIXu.htm)|Darkvision|auto-trad|
|[5CQVRA4OFks4NZna.htm](book-of-the-dead-bestiary-items/5CQVRA4OFks4NZna.htm)|Negative Healing|auto-trad|
|[5dpwpqolmjl4a6sn.htm](book-of-the-dead-bestiary-items/5dpwpqolmjl4a6sn.htm)|Collect Soul|auto-trad|
|[5gjnv61c3nc5vbt6.htm](book-of-the-dead-bestiary-items/5gjnv61c3nc5vbt6.htm)|Spiked Chain|auto-trad|
|[5k5e5m4xanv71qep.htm](book-of-the-dead-bestiary-items/5k5e5m4xanv71qep.htm)|Smoldering Fist|auto-trad|
|[5l8z97yi0fc0gqoq.htm](book-of-the-dead-bestiary-items/5l8z97yi0fc0gqoq.htm)|Root|auto-trad|
|[5lklhb6x3xhg0lri.htm](book-of-the-dead-bestiary-items/5lklhb6x3xhg0lri.htm)|Fist|auto-trad|
|[5mxcdpdeibtg3cig.htm](book-of-the-dead-bestiary-items/5mxcdpdeibtg3cig.htm)|Ghostly Blades|auto-trad|
|[5n8sgtk9eiwslacu.htm](book-of-the-dead-bestiary-items/5n8sgtk9eiwslacu.htm)|Sense Visitors|auto-trad|
|[5q1vmg0m797bxfoz.htm](book-of-the-dead-bestiary-items/5q1vmg0m797bxfoz.htm)|Claw|auto-trad|
|[5q62z2h5hdchg2s2.htm](book-of-the-dead-bestiary-items/5q62z2h5hdchg2s2.htm)|Awful Approach|auto-trad|
|[5qil88xw16heonrg.htm](book-of-the-dead-bestiary-items/5qil88xw16heonrg.htm)|Smoke Vision|auto-trad|
|[5S4ZcAlTU2l4OVrx.htm](book-of-the-dead-bestiary-items/5S4ZcAlTU2l4OVrx.htm)|Wavesense (Imprecise) 100 feet|auto-trad|
|[5tla19582bslvej5.htm](book-of-the-dead-bestiary-items/5tla19582bslvej5.htm)|Death Grip|auto-trad|
|[6312irfg8l2y8wqs.htm](book-of-the-dead-bestiary-items/6312irfg8l2y8wqs.htm)|Foot|auto-trad|
|[68hcqX4h4YdgWr9o.htm](book-of-the-dead-bestiary-items/68hcqX4h4YdgWr9o.htm)|Fist|auto-trad|
|[6FVQCXo7u4wEwCcA.htm](book-of-the-dead-bestiary-items/6FVQCXo7u4wEwCcA.htm)|Constrict|auto-trad|
|[6h26o4rywcidme60.htm](book-of-the-dead-bestiary-items/6h26o4rywcidme60.htm)|Time-shifting Touch|auto-trad|
|[6Ho5G5qDA1od6BFM.htm](book-of-the-dead-bestiary-items/6Ho5G5qDA1od6BFM.htm)|Fast Healing 10|auto-trad|
|[6leqktn7qesbdoh1.htm](book-of-the-dead-bestiary-items/6leqktn7qesbdoh1.htm)|Desiccation Aura|auto-trad|
|[6MBpOAXA2v4U8Ojh.htm](book-of-the-dead-bestiary-items/6MBpOAXA2v4U8Ojh.htm)|Constrict|auto-trad|
|[6qp9o1qevi1znh6x.htm](book-of-the-dead-bestiary-items/6qp9o1qevi1znh6x.htm)|Twilight Spirit|auto-trad|
|[6sig7ubrfqb7uiei.htm](book-of-the-dead-bestiary-items/6sig7ubrfqb7uiei.htm)|Channel Rot|auto-trad|
|[6tism8ur5hq61x7v.htm](book-of-the-dead-bestiary-items/6tism8ur5hq61x7v.htm)|Steal Soul|auto-trad|
|[6U0JQZ8ztbguo44B.htm](book-of-the-dead-bestiary-items/6U0JQZ8ztbguo44B.htm)|Divine Innate Spells|auto-trad|
|[6UUahVCN4rUQxrW0.htm](book-of-the-dead-bestiary-items/6UUahVCN4rUQxrW0.htm)|At-Will Spells|auto-trad|
|[6yud3tv7zitkw9k9.htm](book-of-the-dead-bestiary-items/6yud3tv7zitkw9k9.htm)|Slithering Strike|auto-trad|
|[7499bjv6f655br8n.htm](book-of-the-dead-bestiary-items/7499bjv6f655br8n.htm)|Spawn Prowler Wight|auto-trad|
|[79lxx093izwrpuih.htm](book-of-the-dead-bestiary-items/79lxx093izwrpuih.htm)|Tusk|auto-trad|
|[7at7cp1369l1wlf5.htm](book-of-the-dead-bestiary-items/7at7cp1369l1wlf5.htm)|Warrior's Mask|auto-trad|
|[7avv9wov0dsg3srf.htm](book-of-the-dead-bestiary-items/7avv9wov0dsg3srf.htm)|Ravenous Void|auto-trad|
|[7hy3wqu9oefod2vm.htm](book-of-the-dead-bestiary-items/7hy3wqu9oefod2vm.htm)|Mountain Slam|auto-trad|
|[7Iep6kSfoCj1UNVm.htm](book-of-the-dead-bestiary-items/7Iep6kSfoCj1UNVm.htm)|Rejuvenation|auto-trad|
|[7ko9o8gxy2rnu262.htm](book-of-the-dead-bestiary-items/7ko9o8gxy2rnu262.htm)|Fist|auto-trad|
|[7lt4725ubnitaecg.htm](book-of-the-dead-bestiary-items/7lt4725ubnitaecg.htm)|Jaws|auto-trad|
|[7m7b3dwyednyhqtu.htm](book-of-the-dead-bestiary-items/7m7b3dwyednyhqtu.htm)|Denounce Heretic|auto-trad|
|[7qQmNwIMFsY4kIjN.htm](book-of-the-dead-bestiary-items/7qQmNwIMFsY4kIjN.htm)|Defend Territory|auto-trad|
|[7rpcLcKUwgjQVUST.htm](book-of-the-dead-bestiary-items/7rpcLcKUwgjQVUST.htm)|Sneak Attack|auto-trad|
|[7spg3bnxup4hapkm.htm](book-of-the-dead-bestiary-items/7spg3bnxup4hapkm.htm)|Branch|auto-trad|
|[7wp3tf6myiuh5zf6.htm](book-of-the-dead-bestiary-items/7wp3tf6myiuh5zf6.htm)|Mandible|auto-trad|
|[80uvreq1zdclqkeo.htm](book-of-the-dead-bestiary-items/80uvreq1zdclqkeo.htm)|Tumultuous Flash|auto-trad|
|[81Htx72275BlwBKF.htm](book-of-the-dead-bestiary-items/81Htx72275BlwBKF.htm)|Weep Blood|auto-trad|
|[83bRw8wnX0fjOtf2.htm](book-of-the-dead-bestiary-items/83bRw8wnX0fjOtf2.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[83mbwwys64b7tvby.htm](book-of-the-dead-bestiary-items/83mbwwys64b7tvby.htm)|Sense Companion|auto-trad|
|[88vGaWxuqoaedx3J.htm](book-of-the-dead-bestiary-items/88vGaWxuqoaedx3J.htm)|Arcane Innate Spells|auto-trad|
|[88ymu96vgm67d1yi.htm](book-of-the-dead-bestiary-items/88ymu96vgm67d1yi.htm)|Vengeful Suffocation|auto-trad|
|[8BdsrDdI55FRLHOf.htm](book-of-the-dead-bestiary-items/8BdsrDdI55FRLHOf.htm)|Primal Innate Spells|auto-trad|
|[8cgtf6p2b39hnee5.htm](book-of-the-dead-bestiary-items/8cgtf6p2b39hnee5.htm)|Fragile Wings|auto-trad|
|[8dmnhjn5baw0ic6c.htm](book-of-the-dead-bestiary-items/8dmnhjn5baw0ic6c.htm)|Breath of Sand|auto-trad|
|[8dn13bwe1ei77nhq.htm](book-of-the-dead-bestiary-items/8dn13bwe1ei77nhq.htm)|Shortbow|auto-trad|
|[8e4yMrXnRhkqRrxJ.htm](book-of-the-dead-bestiary-items/8e4yMrXnRhkqRrxJ.htm)|Darkvision|auto-trad|
|[8GAYsczHou1vWlDL.htm](book-of-the-dead-bestiary-items/8GAYsczHou1vWlDL.htm)|Negative Healing|auto-trad|
|[8h5yhlk6yupldtxp.htm](book-of-the-dead-bestiary-items/8h5yhlk6yupldtxp.htm)|Resurrection Vulnerability|auto-trad|
|[8jhirjg4ivj0abbm.htm](book-of-the-dead-bestiary-items/8jhirjg4ivj0abbm.htm)|Crumbling Form|auto-trad|
|[8ki7j1n996www1th.htm](book-of-the-dead-bestiary-items/8ki7j1n996www1th.htm)|Shadowless|auto-trad|
|[8lahe1p6o2mjwdip.htm](book-of-the-dead-bestiary-items/8lahe1p6o2mjwdip.htm)|Temporal Fracturing Ray|auto-trad|
|[8O9kWQZrwnaEV85K.htm](book-of-the-dead-bestiary-items/8O9kWQZrwnaEV85K.htm)|Improved Grab|auto-trad|
|[8paxHAcU5Do4oAI1.htm](book-of-the-dead-bestiary-items/8paxHAcU5Do4oAI1.htm)|At-Will Spells|auto-trad|
|[8q0pqsiwe57oc0lw.htm](book-of-the-dead-bestiary-items/8q0pqsiwe57oc0lw.htm)|Sandstorm|auto-trad|
|[8QQmzAl2OQsH6V6D.htm](book-of-the-dead-bestiary-items/8QQmzAl2OQsH6V6D.htm)|Constant Spells|auto-trad|
|[8t3kp7gfpp7qfl7q.htm](book-of-the-dead-bestiary-items/8t3kp7gfpp7qfl7q.htm)|Robe Tangle|auto-trad|
|[8ub8dfxzqtxj93eq.htm](book-of-the-dead-bestiary-items/8ub8dfxzqtxj93eq.htm)|Charge Chain|auto-trad|
|[90hmtfy65sjru41o.htm](book-of-the-dead-bestiary-items/90hmtfy65sjru41o.htm)|Dissonant Chord|auto-trad|
|[90j5smnm3ber9gfp.htm](book-of-the-dead-bestiary-items/90j5smnm3ber9gfp.htm)|Ghostly Hand|auto-trad|
|[92BrN8oQFaOLJ91I.htm](book-of-the-dead-bestiary-items/92BrN8oQFaOLJ91I.htm)|Darkvision|auto-trad|
|[942tW0398ldkqoyk.htm](book-of-the-dead-bestiary-items/942tW0398ldkqoyk.htm)|Fast Healing 10|auto-trad|
|[95en1txwwqbdjtdr.htm](book-of-the-dead-bestiary-items/95en1txwwqbdjtdr.htm)|Implacable Advance|auto-trad|
|[9br1r4rn2iiosza8.htm](book-of-the-dead-bestiary-items/9br1r4rn2iiosza8.htm)|Battlefield Bound|auto-trad|
|[9bucmn723g63yhg1.htm](book-of-the-dead-bestiary-items/9bucmn723g63yhg1.htm)|Jaws|auto-trad|
|[9BZlVPtKWPrqj2a3.htm](book-of-the-dead-bestiary-items/9BZlVPtKWPrqj2a3.htm)|Negative Healing|auto-trad|
|[9cot9pecrjfwekp6.htm](book-of-the-dead-bestiary-items/9cot9pecrjfwekp6.htm)|Vomit Blood|auto-trad|
|[9lqmxwvb8gf3pki2.htm](book-of-the-dead-bestiary-items/9lqmxwvb8gf3pki2.htm)|Siphon Vitality|auto-trad|
|[9nhauoark4hd73ic.htm](book-of-the-dead-bestiary-items/9nhauoark4hd73ic.htm)|Thoughtsense 60 feet|auto-trad|
|[9Osaf1jaTvEKFmUM.htm](book-of-the-dead-bestiary-items/9Osaf1jaTvEKFmUM.htm)|Ruinous Weapons|auto-trad|
|[9Qeobioo0LmSzjQg.htm](book-of-the-dead-bestiary-items/9Qeobioo0LmSzjQg.htm)|Darkvision|auto-trad|
|[9QFD9WobEytvbbov.htm](book-of-the-dead-bestiary-items/9QFD9WobEytvbbov.htm)|Negative Healing|auto-trad|
|[9RsQKHAZSLUD8OaG.htm](book-of-the-dead-bestiary-items/9RsQKHAZSLUD8OaG.htm)|Darkvision|auto-trad|
|[9uwteydd6ypzaa2e.htm](book-of-the-dead-bestiary-items/9uwteydd6ypzaa2e.htm)|Tactical Direction|auto-trad|
|[9X9LnWN67AI3Wyjr.htm](book-of-the-dead-bestiary-items/9X9LnWN67AI3Wyjr.htm)|Rejuvenation|auto-trad|
|[9xtciwtearfnw9g0.htm](book-of-the-dead-bestiary-items/9xtciwtearfnw9g0.htm)|Undying Vendetta|auto-trad|
|[9y280jhc7njt1nez.htm](book-of-the-dead-bestiary-items/9y280jhc7njt1nez.htm)|Lair Sense|auto-trad|
|[a0e3JLzKlammUKQN.htm](book-of-the-dead-bestiary-items/a0e3JLzKlammUKQN.htm)|Greater Darkvision|auto-trad|
|[a34aowwyzlmdtpg4.htm](book-of-the-dead-bestiary-items/a34aowwyzlmdtpg4.htm)|Ghostly Grasp|auto-trad|
|[a380z0ervv71womf.htm](book-of-the-dead-bestiary-items/a380z0ervv71womf.htm)|Wing|auto-trad|
|[a3wMuTNkVSHLbp4T.htm](book-of-the-dead-bestiary-items/a3wMuTNkVSHLbp4T.htm)|Darkvision|auto-trad|
|[a7gh1zpyblagcti4.htm](book-of-the-dead-bestiary-items/a7gh1zpyblagcti4.htm)|Death Gasp|auto-trad|
|[a7w43julclvnfw63.htm](book-of-the-dead-bestiary-items/a7w43julclvnfw63.htm)|Drain Spell Tome|auto-trad|
|[a9ir7xv426ririzq.htm](book-of-the-dead-bestiary-items/a9ir7xv426ririzq.htm)|Surge of Speed|auto-trad|
|[a9iRrDBwDQtFML4a.htm](book-of-the-dead-bestiary-items/a9iRrDBwDQtFML4a.htm)|Darkvision|auto-trad|
|[ad1bef6ocdexoep9.htm](book-of-the-dead-bestiary-items/ad1bef6ocdexoep9.htm)|Fangs|auto-trad|
|[aDtuEVLZvPSFSJTZ.htm](book-of-the-dead-bestiary-items/aDtuEVLZvPSFSJTZ.htm)|Occult Innate Spells|auto-trad|
|[ai6gr48jowwpnc0m.htm](book-of-the-dead-bestiary-items/ai6gr48jowwpnc0m.htm)|Ravenous Undoing|auto-trad|
|[AjjzMtoQOxAQvqf9.htm](book-of-the-dead-bestiary-items/AjjzMtoQOxAQvqf9.htm)|Negative Healing|auto-trad|
|[al020n2uibvv8exp.htm](book-of-the-dead-bestiary-items/al020n2uibvv8exp.htm)|Fist|auto-trad|
|[am4u24k7o7diqo0t.htm](book-of-the-dead-bestiary-items/am4u24k7o7diqo0t.htm)|Woodland Dependent|auto-trad|
|[AmOslrFKvYuv61w1.htm](book-of-the-dead-bestiary-items/AmOslrFKvYuv61w1.htm)|Control Comatose|auto-trad|
|[AnBTVVvzCtNwFDSJ.htm](book-of-the-dead-bestiary-items/AnBTVVvzCtNwFDSJ.htm)|Grab|auto-trad|
|[aph86y82rdqgcn4k.htm](book-of-the-dead-bestiary-items/aph86y82rdqgcn4k.htm)|Control Body|auto-trad|
|[apx5l6z5p09xanhv.htm](book-of-the-dead-bestiary-items/apx5l6z5p09xanhv.htm)|Feign Death|auto-trad|
|[apXBdiwzInrE6nhf.htm](book-of-the-dead-bestiary-items/apXBdiwzInrE6nhf.htm)|Negative Healing|auto-trad|
|[aqutya2asmrp8wxy.htm](book-of-the-dead-bestiary-items/aqutya2asmrp8wxy.htm)|Soul Theft|auto-trad|
|[ars8cn2115cvrmwo.htm](book-of-the-dead-bestiary-items/ars8cn2115cvrmwo.htm)|Ghostly Swoop|auto-trad|
|[b146le3z824z27kz.htm](book-of-the-dead-bestiary-items/b146le3z824z27kz.htm)|Jaws|auto-trad|
|[B2lJQF6Y6o4hnBeL.htm](book-of-the-dead-bestiary-items/B2lJQF6Y6o4hnBeL.htm)|Negative Healing|auto-trad|
|[b44nuiFLtAaurkMZ.htm](book-of-the-dead-bestiary-items/b44nuiFLtAaurkMZ.htm)|Divine Prepared Spells|auto-trad|
|[b4tdi1u14a8xwxkz.htm](book-of-the-dead-bestiary-items/b4tdi1u14a8xwxkz.htm)|Change of Luck|auto-trad|
|[b4ti2yqe9ipiuxz2.htm](book-of-the-dead-bestiary-items/b4ti2yqe9ipiuxz2.htm)|Sudden Surge|auto-trad|
|[B4uyQyWXUec4RybT.htm](book-of-the-dead-bestiary-items/B4uyQyWXUec4RybT.htm)|Ghostly Touch|auto-trad|
|[b9qjt3e0bvq6d5wx.htm](book-of-the-dead-bestiary-items/b9qjt3e0bvq6d5wx.htm)|Trunk|auto-trad|
|[BcSNcGhcDxvtI9k5.htm](book-of-the-dead-bestiary-items/BcSNcGhcDxvtI9k5.htm)|Divine Prepared Spells|auto-trad|
|[bCT8pcPOyNG51I6r.htm](book-of-the-dead-bestiary-items/bCT8pcPOyNG51I6r.htm)|Darkvision|auto-trad|
|[bd5fpgu3trmh7764.htm](book-of-the-dead-bestiary-items/bd5fpgu3trmh7764.htm)|Eat Soul|auto-trad|
|[bftdv71wnr4786h4.htm](book-of-the-dead-bestiary-items/bftdv71wnr4786h4.htm)|Call of the Damned|auto-trad|
|[bfVGbnQelJe4aMTO.htm](book-of-the-dead-bestiary-items/bfVGbnQelJe4aMTO.htm)|Lifesense 30 feet|auto-trad|
|[bh3hivovyu8ym9ay.htm](book-of-the-dead-bestiary-items/bh3hivovyu8ym9ay.htm)|Painful Touch|auto-trad|
|[bhmy3f5zgxplyfru.htm](book-of-the-dead-bestiary-items/bhmy3f5zgxplyfru.htm)|Demesne Confinement|auto-trad|
|[bj3PlHtgo2gfPTmO.htm](book-of-the-dead-bestiary-items/bj3PlHtgo2gfPTmO.htm)|Darkvision|auto-trad|
|[bKHEkuv4wRPctK0n.htm](book-of-the-dead-bestiary-items/bKHEkuv4wRPctK0n.htm)|+26 to Initiative|auto-trad|
|[BMogxn5MHTe3lvyK.htm](book-of-the-dead-bestiary-items/BMogxn5MHTe3lvyK.htm)|Darkvision|auto-trad|
|[bnaNGdsV5TrxiHNp.htm](book-of-the-dead-bestiary-items/bnaNGdsV5TrxiHNp.htm)|Constant Spells|auto-trad|
|[bnge6fye2orz7wv1.htm](book-of-the-dead-bestiary-items/bnge6fye2orz7wv1.htm)|Luring Laugh|auto-trad|
|[bnqnkgeuejbypq0u.htm](book-of-the-dead-bestiary-items/bnqnkgeuejbypq0u.htm)|Horned Rush|auto-trad|
|[bnQpqP6hIv1ISjen.htm](book-of-the-dead-bestiary-items/bnQpqP6hIv1ISjen.htm)|Negative Healing|auto-trad|
|[bpw486wu7j66tqyz.htm](book-of-the-dead-bestiary-items/bpw486wu7j66tqyz.htm)|Bloody Handprint|auto-trad|
|[brzonbw2amez88xj.htm](book-of-the-dead-bestiary-items/brzonbw2amez88xj.htm)|Possess Tree|auto-trad|
|[bT4K3qMjcsV7JMaV.htm](book-of-the-dead-bestiary-items/bT4K3qMjcsV7JMaV.htm)|Lifesense 60 feet|auto-trad|
|[bwdkr5yl7ye33lip.htm](book-of-the-dead-bestiary-items/bwdkr5yl7ye33lip.htm)|Dagger|auto-trad|
|[bx92ttr6lo474e66.htm](book-of-the-dead-bestiary-items/bx92ttr6lo474e66.htm)|Heretic's Armaments|auto-trad|
|[bXrBIlfFPcGYTa2l.htm](book-of-the-dead-bestiary-items/bXrBIlfFPcGYTa2l.htm)|Mental Rebirth|auto-trad|
|[bywAbicCfjMxO2MY.htm](book-of-the-dead-bestiary-items/bywAbicCfjMxO2MY.htm)|Negative Healing|auto-trad|
|[c0cED5WAyOCC6wwV.htm](book-of-the-dead-bestiary-items/c0cED5WAyOCC6wwV.htm)|Constant Spells|auto-trad|
|[c2iKZhgPad7G2R6c.htm](book-of-the-dead-bestiary-items/c2iKZhgPad7G2R6c.htm)|Darkvision|auto-trad|
|[c2kagtputtatmo83.htm](book-of-the-dead-bestiary-items/c2kagtputtatmo83.htm)|Longbow|auto-trad|
|[c4od3wbgqhmgyp28.htm](book-of-the-dead-bestiary-items/c4od3wbgqhmgyp28.htm)|Voice of the Soul|auto-trad|
|[C6FyNCCwhcZO6sQX.htm](book-of-the-dead-bestiary-items/C6FyNCCwhcZO6sQX.htm)|Sneak Attack|auto-trad|
|[cb4gc90xqcsm6n37.htm](book-of-the-dead-bestiary-items/cb4gc90xqcsm6n37.htm)|Sense Murderer|auto-trad|
|[cbmmwaxrz6a1z435.htm](book-of-the-dead-bestiary-items/cbmmwaxrz6a1z435.htm)|Divine Guardian|auto-trad|
|[CDMyFeuLiSZFWpoj.htm](book-of-the-dead-bestiary-items/CDMyFeuLiSZFWpoj.htm)|Darkvision|auto-trad|
|[cg2m9if702p5jhb2.htm](book-of-the-dead-bestiary-items/cg2m9if702p5jhb2.htm)|Totems of the Past|auto-trad|
|[cg7mnEAt7MBa8UwC.htm](book-of-the-dead-bestiary-items/cg7mnEAt7MBa8UwC.htm)|Vetalarana Vulnerabilities|auto-trad|
|[chmnd5AocqCFMCGT.htm](book-of-the-dead-bestiary-items/chmnd5AocqCFMCGT.htm)|Frightful Presence|auto-trad|
|[chqupAH7uMySkbAy.htm](book-of-the-dead-bestiary-items/chqupAH7uMySkbAy.htm)|Mental Rebirth|auto-trad|
|[cifv4nupbufci3x9.htm](book-of-the-dead-bestiary-items/cifv4nupbufci3x9.htm)|Fangs|auto-trad|
|[cIJp4XSjGySMlIFb.htm](book-of-the-dead-bestiary-items/cIJp4XSjGySMlIFb.htm)|Attack of Opportunity|auto-trad|
|[CItCjF3JMpPDPOGb.htm](book-of-the-dead-bestiary-items/CItCjF3JMpPDPOGb.htm)|Shamble Forth|auto-trad|
|[cMH2fN0W5f937Chd.htm](book-of-the-dead-bestiary-items/cMH2fN0W5f937Chd.htm)|Divine Innate Spells|auto-trad|
|[cPUJUW23ZXZ9W7Wr.htm](book-of-the-dead-bestiary-items/cPUJUW23ZXZ9W7Wr.htm)|Divine Innate Spells|auto-trad|
|[csccddk7b6n1491u.htm](book-of-the-dead-bestiary-items/csccddk7b6n1491u.htm)|Field of Undeath|auto-trad|
|[csqmqowngauhnll2.htm](book-of-the-dead-bestiary-items/csqmqowngauhnll2.htm)|Agonized Howl|auto-trad|
|[cuI5TrIQCtm5H6Bt.htm](book-of-the-dead-bestiary-items/cuI5TrIQCtm5H6Bt.htm)|Scythe|auto-trad|
|[cuvl6Xxa053PF1xX.htm](book-of-the-dead-bestiary-items/cuvl6Xxa053PF1xX.htm)|Negative Healing|auto-trad|
|[cVXdT14wxKimZClN.htm](book-of-the-dead-bestiary-items/cVXdT14wxKimZClN.htm)|Jiang-Shi Vulnerabilities|auto-trad|
|[CwXMyTDGPy4bVI0z.htm](book-of-the-dead-bestiary-items/CwXMyTDGPy4bVI0z.htm)|Negative Healing|auto-trad|
|[cxlq6wpb9in9z1s2.htm](book-of-the-dead-bestiary-items/cxlq6wpb9in9z1s2.htm)|Ghostly Hand Crossbow|auto-trad|
|[CZFYOk40GBWGe5U5.htm](book-of-the-dead-bestiary-items/CZFYOk40GBWGe5U5.htm)|Change Shape|auto-trad|
|[D0GXxbH8jAosuw4k.htm](book-of-the-dead-bestiary-items/D0GXxbH8jAosuw4k.htm)|Sneak Attack|auto-trad|
|[D1dtdbWicwawxBM3.htm](book-of-the-dead-bestiary-items/D1dtdbWicwawxBM3.htm)|Constrict|auto-trad|
|[d1u87ahqlsegvjml.htm](book-of-the-dead-bestiary-items/d1u87ahqlsegvjml.htm)|Javelin|auto-trad|
|[D2cPnadhWWwabreZ.htm](book-of-the-dead-bestiary-items/D2cPnadhWWwabreZ.htm)|Greater Constrict|auto-trad|
|[D5XHOv8oLEJdcY0i.htm](book-of-the-dead-bestiary-items/D5XHOv8oLEJdcY0i.htm)|Occult Innate Spells|auto-trad|
|[d607zui6g37uqefz.htm](book-of-the-dead-bestiary-items/d607zui6g37uqefz.htm)|Pounce|auto-trad|
|[da71u2jzo19jfmuf.htm](book-of-the-dead-bestiary-items/da71u2jzo19jfmuf.htm)|Channel Rot|auto-trad|
|[daAC9eNxuMlrDoZG.htm](book-of-the-dead-bestiary-items/daAC9eNxuMlrDoZG.htm)|Grab|auto-trad|
|[db4votcoqinbjj2s.htm](book-of-the-dead-bestiary-items/db4votcoqinbjj2s.htm)|Scythe|auto-trad|
|[dc6VyUkQp6pOJtSn.htm](book-of-the-dead-bestiary-items/dc6VyUkQp6pOJtSn.htm)|Constrict|auto-trad|
|[df2mf1pstfhw330a.htm](book-of-the-dead-bestiary-items/df2mf1pstfhw330a.htm)|Tear Skin|auto-trad|
|[df79dbzoljgbj0mq.htm](book-of-the-dead-bestiary-items/df79dbzoljgbj0mq.htm)|Self-Loathing|auto-trad|
|[dFiZIcxqM6qYIs6g.htm](book-of-the-dead-bestiary-items/dFiZIcxqM6qYIs6g.htm)|Darkvision|auto-trad|
|[DHn9EnlQKZ1hypSV.htm](book-of-the-dead-bestiary-items/DHn9EnlQKZ1hypSV.htm)|Darkvision|auto-trad|
|[dIjjf4Tqor8DWWHu.htm](book-of-the-dead-bestiary-items/dIjjf4Tqor8DWWHu.htm)|Swallow Whole|auto-trad|
|[dizkg5865hh5zv35.htm](book-of-the-dead-bestiary-items/dizkg5865hh5zv35.htm)|Wake the Dead|auto-trad|
|[Dj9dMn2OXHtfdyMz.htm](book-of-the-dead-bestiary-items/Dj9dMn2OXHtfdyMz.htm)|Whisper|auto-trad|
|[djU8N0rJuOlO9qqb.htm](book-of-the-dead-bestiary-items/djU8N0rJuOlO9qqb.htm)|Occult Innate Spells|auto-trad|
|[dKbCkV7uzuluzoo8.htm](book-of-the-dead-bestiary-items/dKbCkV7uzuluzoo8.htm)|Darkvision|auto-trad|
|[dkckp2z4bbmspgcy.htm](book-of-the-dead-bestiary-items/dkckp2z4bbmspgcy.htm)|Beak|auto-trad|
|[DLfsz7D9pLNSqIlT.htm](book-of-the-dead-bestiary-items/DLfsz7D9pLNSqIlT.htm)|Stony Shards|auto-trad|
|[dnu6ev9agkecmex6.htm](book-of-the-dead-bestiary-items/dnu6ev9agkecmex6.htm)|Share Blessings|auto-trad|
|[dOeCzneUCh2n8Vq5.htm](book-of-the-dead-bestiary-items/dOeCzneUCh2n8Vq5.htm)|Aura of Repose|auto-trad|
|[dp0m2eqjh44d7v4a.htm](book-of-the-dead-bestiary-items/dp0m2eqjh44d7v4a.htm)|Entropy's Shadow|auto-trad|
|[dqeg5ywheav84nwd.htm](book-of-the-dead-bestiary-items/dqeg5ywheav84nwd.htm)|Stench|auto-trad|
|[drin3tt0epvjjsr9.htm](book-of-the-dead-bestiary-items/drin3tt0epvjjsr9.htm)|Steady Spellcasting|auto-trad|
|[dSlveRiUSvMYcg8P.htm](book-of-the-dead-bestiary-items/dSlveRiUSvMYcg8P.htm)|Negative Healing|auto-trad|
|[DSp7AdbdrabM470N.htm](book-of-the-dead-bestiary-items/DSp7AdbdrabM470N.htm)|Surge Through|auto-trad|
|[dtbhcid8fzmfjtp7.htm](book-of-the-dead-bestiary-items/dtbhcid8fzmfjtp7.htm)|Paralyzing Spew|auto-trad|
|[duqnb28a7oppci1f.htm](book-of-the-dead-bestiary-items/duqnb28a7oppci1f.htm)|Tail|auto-trad|
|[dXqGPFgT8WHpl72i.htm](book-of-the-dead-bestiary-items/dXqGPFgT8WHpl72i.htm)|Divine Innate Spells|auto-trad|
|[dYKKjbhn2q7r853p.htm](book-of-the-dead-bestiary-items/dYKKjbhn2q7r853p.htm)|Negative Healing|auto-trad|
|[DYQmHncbK51f0Cup.htm](book-of-the-dead-bestiary-items/DYQmHncbK51f0Cup.htm)|Attack of Opportunity|auto-trad|
|[dz1w9j5ipev8z1v8.htm](book-of-the-dead-bestiary-items/dz1w9j5ipev8z1v8.htm)|Savvy Joinin' Me Crew?|auto-trad|
|[e0vfxgxi4fovzldn.htm](book-of-the-dead-bestiary-items/e0vfxgxi4fovzldn.htm)|Proboscis|auto-trad|
|[e68t84jixmehqnur.htm](book-of-the-dead-bestiary-items/e68t84jixmehqnur.htm)|Final Spite|auto-trad|
|[e7nXVy8HD6KkhPGf.htm](book-of-the-dead-bestiary-items/e7nXVy8HD6KkhPGf.htm)|One More Breath|auto-trad|
|[e7v5oimcd7h2wmj8.htm](book-of-the-dead-bestiary-items/e7v5oimcd7h2wmj8.htm)|Sand Form|auto-trad|
|[ecn8ua35xgwp7abu.htm](book-of-the-dead-bestiary-items/ecn8ua35xgwp7abu.htm)|Corpse Consumption|auto-trad|
|[EcZYs4KlcnQ09I0F.htm](book-of-the-dead-bestiary-items/EcZYs4KlcnQ09I0F.htm)|Darkvision|auto-trad|
|[Ee5DMlbADQLHFvnV.htm](book-of-the-dead-bestiary-items/Ee5DMlbADQLHFvnV.htm)|Darkvision|auto-trad|
|[eHad4TakoeYMeM8g.htm](book-of-the-dead-bestiary-items/eHad4TakoeYMeM8g.htm)|Sneak Attack|auto-trad|
|[eih755f7z0quy1bu.htm](book-of-the-dead-bestiary-items/eih755f7z0quy1bu.htm)|Scythe|auto-trad|
|[ej3i6scbur8axq00.htm](book-of-the-dead-bestiary-items/ej3i6scbur8axq00.htm)|Ghostly Cutlass|auto-trad|
|[ejx371hd10a4rhfs.htm](book-of-the-dead-bestiary-items/ejx371hd10a4rhfs.htm)|Jaws|auto-trad|
|[ekhnzl2vg8r8raia.htm](book-of-the-dead-bestiary-items/ekhnzl2vg8r8raia.htm)|Revenant Reload|auto-trad|
|[ekMIs1v4IBJG2qEg.htm](book-of-the-dead-bestiary-items/ekMIs1v4IBJG2qEg.htm)|Create Zombies|auto-trad|
|[enVGATk5fYLDSoEA.htm](book-of-the-dead-bestiary-items/enVGATk5fYLDSoEA.htm)|Negative Healing|auto-trad|
|[eO23hQJnoj5vQUSL.htm](book-of-the-dead-bestiary-items/eO23hQJnoj5vQUSL.htm)|Negative Healing|auto-trad|
|[EPaipnmixMdtaUeW.htm](book-of-the-dead-bestiary-items/EPaipnmixMdtaUeW.htm)|Negative Healing|auto-trad|
|[eq0chkkfcwtghv3v.htm](book-of-the-dead-bestiary-items/eq0chkkfcwtghv3v.htm)|Horn|auto-trad|
|[EqqrJxTxtfnQL9mm.htm](book-of-the-dead-bestiary-items/EqqrJxTxtfnQL9mm.htm)|Sudden Siphon|auto-trad|
|[equod2o7os2wz0wr.htm](book-of-the-dead-bestiary-items/equod2o7os2wz0wr.htm)|Claw|auto-trad|
|[es9ibq62fqcl30pn.htm](book-of-the-dead-bestiary-items/es9ibq62fqcl30pn.htm)|Jaws|auto-trad|
|[eu6xmtw27h5dt4bt.htm](book-of-the-dead-bestiary-items/eu6xmtw27h5dt4bt.htm)|Rapier|auto-trad|
|[EUiVBFw40fh2AfU0.htm](book-of-the-dead-bestiary-items/EUiVBFw40fh2AfU0.htm)|Negative Healing|auto-trad|
|[EuLz7D8KQPASHSbM.htm](book-of-the-dead-bestiary-items/EuLz7D8KQPASHSbM.htm)|Darkvision|auto-trad|
|[EXqKm6CdVt3ufYyT.htm](book-of-the-dead-bestiary-items/EXqKm6CdVt3ufYyT.htm)|Occult Innate Spells|auto-trad|
|[eymmc9qbqyp94ywu.htm](book-of-the-dead-bestiary-items/eymmc9qbqyp94ywu.htm)|Inspire the Faithless|auto-trad|
|[ezj7d0ajdqts6n00.htm](book-of-the-dead-bestiary-items/ezj7d0ajdqts6n00.htm)|Tail|auto-trad|
|[f006egqkkbtjuqgv.htm](book-of-the-dead-bestiary-items/f006egqkkbtjuqgv.htm)|Blinding Breath|auto-trad|
|[f303t130pkbopzk0.htm](book-of-the-dead-bestiary-items/f303t130pkbopzk0.htm)|Jaws|auto-trad|
|[f3HhM7YpmtEJwP4y.htm](book-of-the-dead-bestiary-items/f3HhM7YpmtEJwP4y.htm)|Negative Healing|auto-trad|
|[f55now7bu1g173vg.htm](book-of-the-dead-bestiary-items/f55now7bu1g173vg.htm)|Entropy's Shadow|auto-trad|
|[f84cxpe3e8us7653.htm](book-of-the-dead-bestiary-items/f84cxpe3e8us7653.htm)|Claw|auto-trad|
|[fba14HId1lw0Kex9.htm](book-of-the-dead-bestiary-items/fba14HId1lw0Kex9.htm)|Darkvision|auto-trad|
|[FByIi64kp8hJjz6z.htm](book-of-the-dead-bestiary-items/FByIi64kp8hJjz6z.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[fczuoyvm5wqij2to.htm](book-of-the-dead-bestiary-items/fczuoyvm5wqij2to.htm)|Intense Heat|auto-trad|
|[fet1zrdx8aapum04.htm](book-of-the-dead-bestiary-items/fet1zrdx8aapum04.htm)|Compression|auto-trad|
|[Fg4PN7JgMiI91eH5.htm](book-of-the-dead-bestiary-items/Fg4PN7JgMiI91eH5.htm)|Negative Healing|auto-trad|
|[FHXomEYheB5tF9Et.htm](book-of-the-dead-bestiary-items/FHXomEYheB5tF9Et.htm)|Aquatic Ambush|auto-trad|
|[fIhgzWWiqFCcA6nV.htm](book-of-the-dead-bestiary-items/fIhgzWWiqFCcA6nV.htm)|Conjure Instruments|auto-trad|
|[fk6qtcr37ue5mbxw.htm](book-of-the-dead-bestiary-items/fk6qtcr37ue5mbxw.htm)|Terrible Foresight|auto-trad|
|[FKLyRZ60trNAaU7a.htm](book-of-the-dead-bestiary-items/FKLyRZ60trNAaU7a.htm)|Negative Healing|auto-trad|
|[fn7qja3kw9dw1kox.htm](book-of-the-dead-bestiary-items/fn7qja3kw9dw1kox.htm)|Incorporeal Wheel|auto-trad|
|[fp4p44l0wgn5altu.htm](book-of-the-dead-bestiary-items/fp4p44l0wgn5altu.htm)|Consecration Vulnerability|auto-trad|
|[FRqXWcdzhlxU8sMh.htm](book-of-the-dead-bestiary-items/FRqXWcdzhlxU8sMh.htm)|Darkvision|auto-trad|
|[FRWKAh9vjYaNCqdT.htm](book-of-the-dead-bestiary-items/FRWKAh9vjYaNCqdT.htm)|Negative Healing|auto-trad|
|[fSXafbSBzAADlGcn.htm](book-of-the-dead-bestiary-items/fSXafbSBzAADlGcn.htm)|Cleric Domain Spells|auto-trad|
|[FTPKGpv0QRKEpmnY.htm](book-of-the-dead-bestiary-items/FTPKGpv0QRKEpmnY.htm)|Cleric Domain Spells|auto-trad|
|[FUCaE5JHQFv1Gc0a.htm](book-of-the-dead-bestiary-items/FUCaE5JHQFv1Gc0a.htm)|Cleric Domain Spells|auto-trad|
|[fv0690n3y8ghmvd9.htm](book-of-the-dead-bestiary-items/fv0690n3y8ghmvd9.htm)|Claw|auto-trad|
|[fxc64qicfzi9l9lx.htm](book-of-the-dead-bestiary-items/fxc64qicfzi9l9lx.htm)|Rotten Fruit|auto-trad|
|[FzDSsJ65EnkMhwSx.htm](book-of-the-dead-bestiary-items/FzDSsJ65EnkMhwSx.htm)|Negative Healing|auto-trad|
|[fzpmews5ufzi1k6e.htm](book-of-the-dead-bestiary-items/fzpmews5ufzi1k6e.htm)|Strangle|auto-trad|
|[g3UMdUkuFL5RdKPP.htm](book-of-the-dead-bestiary-items/g3UMdUkuFL5RdKPP.htm)|Grab|auto-trad|
|[g9hvfpvweahguxgh.htm](book-of-the-dead-bestiary-items/g9hvfpvweahguxgh.htm)|Create Zombies|auto-trad|
|[ga6hycubofuo1zyf.htm](book-of-the-dead-bestiary-items/ga6hycubofuo1zyf.htm)|Claw|auto-trad|
|[gB214qjiG2XEJPBR.htm](book-of-the-dead-bestiary-items/gB214qjiG2XEJPBR.htm)|Darkvision|auto-trad|
|[gB36aH5yDgmXH6xc.htm](book-of-the-dead-bestiary-items/gB36aH5yDgmXH6xc.htm)|Squeeze|auto-trad|
|[gDkkvEZZSC6HPnDm.htm](book-of-the-dead-bestiary-items/gDkkvEZZSC6HPnDm.htm)|Command Zombie|auto-trad|
|[gE5SOeGR6RJsEJJ1.htm](book-of-the-dead-bestiary-items/gE5SOeGR6RJsEJJ1.htm)|Divine Innate Spells|auto-trad|
|[GFbwZSSnTHj0YPuv.htm](book-of-the-dead-bestiary-items/GFbwZSSnTHj0YPuv.htm)|Darkvision|auto-trad|
|[ggz12biwbgr3mv1o.htm](book-of-the-dead-bestiary-items/ggz12biwbgr3mv1o.htm)|Lesser Mummy Rot|auto-trad|
|[ghtfq7ukz7gwxwsb.htm](book-of-the-dead-bestiary-items/ghtfq7ukz7gwxwsb.htm)|Draconic Frenzy|auto-trad|
|[gi4s0lkv3mfb9loi.htm](book-of-the-dead-bestiary-items/gi4s0lkv3mfb9loi.htm)|Fist|auto-trad|
|[girs49ehz5bzw3df.htm](book-of-the-dead-bestiary-items/girs49ehz5bzw3df.htm)|Anticipatory Attack|auto-trad|
|[gizkkpi42rqcq7rz.htm](book-of-the-dead-bestiary-items/gizkkpi42rqcq7rz.htm)|Jaws|auto-trad|
|[GJBlX4Lhocg26ELX.htm](book-of-the-dead-bestiary-items/GJBlX4Lhocg26ELX.htm)|Negative Healing|auto-trad|
|[gkW3gWYUJOzFKerr.htm](book-of-the-dead-bestiary-items/gkW3gWYUJOzFKerr.htm)|Lifesense 60 feet|auto-trad|
|[gocrv4xvdtl8n7hl.htm](book-of-the-dead-bestiary-items/gocrv4xvdtl8n7hl.htm)|Blazing Howl|auto-trad|
|[gomtgkod7rqvcpxq.htm](book-of-the-dead-bestiary-items/gomtgkod7rqvcpxq.htm)|Stamping Foot|auto-trad|
|[GrurNCdRZ7stOOB7.htm](book-of-the-dead-bestiary-items/GrurNCdRZ7stOOB7.htm)|Grab|auto-trad|
|[gsUuAStZr4pb7zis.htm](book-of-the-dead-bestiary-items/gsUuAStZr4pb7zis.htm)|Improved Grab|auto-trad|
|[GtDhQviwk4x4rmKZ.htm](book-of-the-dead-bestiary-items/GtDhQviwk4x4rmKZ.htm)|Divine Innate Spells|auto-trad|
|[gw4pcdfvx6elv0g3.htm](book-of-the-dead-bestiary-items/gw4pcdfvx6elv0g3.htm)|Siphon Faith|auto-trad|
|[gwzami23yywnrl99.htm](book-of-the-dead-bestiary-items/gwzami23yywnrl99.htm)|Slow|auto-trad|
|[GX7ZYuLk1vIcTVZQ.htm](book-of-the-dead-bestiary-items/GX7ZYuLk1vIcTVZQ.htm)|Divine Innate Spells|auto-trad|
|[gy0jf8etpgdnoddm.htm](book-of-the-dead-bestiary-items/gy0jf8etpgdnoddm.htm)|Natural Invisibility|auto-trad|
|[H0qYKDck9uqk3ZiC.htm](book-of-the-dead-bestiary-items/H0qYKDck9uqk3ZiC.htm)|Capture|auto-trad|
|[h189cx7txluockka.htm](book-of-the-dead-bestiary-items/h189cx7txluockka.htm)|Shadow Rapier|auto-trad|
|[h305evdhewgfdv8w.htm](book-of-the-dead-bestiary-items/h305evdhewgfdv8w.htm)|Field of Undeath|auto-trad|
|[H3cDChRjDRA71MNI.htm](book-of-the-dead-bestiary-items/H3cDChRjDRA71MNI.htm)|Lifesense (Imprecise) 60 feet|auto-trad|
|[h4i7zh8y17d35lou.htm](book-of-the-dead-bestiary-items/h4i7zh8y17d35lou.htm)|Jaws|auto-trad|
|[h4iw6qojb9gael8e.htm](book-of-the-dead-bestiary-items/h4iw6qojb9gael8e.htm)|Talon|auto-trad|
|[h6ms55ripngjxrjb.htm](book-of-the-dead-bestiary-items/h6ms55ripngjxrjb.htm)|Eroding Touch|auto-trad|
|[h7zcdyu7ovmxa2ya.htm](book-of-the-dead-bestiary-items/h7zcdyu7ovmxa2ya.htm)|Claw|auto-trad|
|[h9Qhc5vy2FgrUSrb.htm](book-of-the-dead-bestiary-items/h9Qhc5vy2FgrUSrb.htm)|Darkvision|auto-trad|
|[H9XeL3LQ06KgkEE1.htm](book-of-the-dead-bestiary-items/H9XeL3LQ06KgkEE1.htm)|Frenzy|auto-trad|
|[HeQVQ64EF8HjzyyO.htm](book-of-the-dead-bestiary-items/HeQVQ64EF8HjzyyO.htm)|Knockdown|auto-trad|
|[hG2ocnUMjZCucqB2.htm](book-of-the-dead-bestiary-items/hG2ocnUMjZCucqB2.htm)|Horn|auto-trad|
|[hGLTi7qRn9Hbn8VM.htm](book-of-the-dead-bestiary-items/hGLTi7qRn9Hbn8VM.htm)|Rigor Mortis|auto-trad|
|[HgX8EdBcuekHoaYB.htm](book-of-the-dead-bestiary-items/HgX8EdBcuekHoaYB.htm)|Constrict|auto-trad|
|[hJqwMBdBQecpuWU8.htm](book-of-the-dead-bestiary-items/hJqwMBdBQecpuWU8.htm)|Negative Healing|auto-trad|
|[hK1tPnyMOt0g3STo.htm](book-of-the-dead-bestiary-items/hK1tPnyMOt0g3STo.htm)|Lifesense 60 feet|auto-trad|
|[hma3kv808zk32z1k.htm](book-of-the-dead-bestiary-items/hma3kv808zk32z1k.htm)|Spear|auto-trad|
|[hndwksut71ohgybj.htm](book-of-the-dead-bestiary-items/hndwksut71ohgybj.htm)|Midnight Depths|auto-trad|
|[hrwH6CDrMAcUOAA4.htm](book-of-the-dead-bestiary-items/hrwH6CDrMAcUOAA4.htm)|Constant Spells|auto-trad|
|[htqhazfmsy64d5iz.htm](book-of-the-dead-bestiary-items/htqhazfmsy64d5iz.htm)|Entropy's Shadow|auto-trad|
|[HUgw20uZ51R7a9a4.htm](book-of-the-dead-bestiary-items/HUgw20uZ51R7a9a4.htm)|Topple Furniture|auto-trad|
|[HUIxZptlwIab6WRD.htm](book-of-the-dead-bestiary-items/HUIxZptlwIab6WRD.htm)|Darkvision|auto-trad|
|[hvmeWsfKjMqgYhBi.htm](book-of-the-dead-bestiary-items/hvmeWsfKjMqgYhBi.htm)|Negative Healing|auto-trad|
|[HWy6SULhipZ1fnRK.htm](book-of-the-dead-bestiary-items/HWy6SULhipZ1fnRK.htm)|Darkvision|auto-trad|
|[hz33scglipei6wyh.htm](book-of-the-dead-bestiary-items/hz33scglipei6wyh.htm)|Claw|auto-trad|
|[hz9E9TnkviNuCCnq.htm](book-of-the-dead-bestiary-items/hz9E9TnkviNuCCnq.htm)|Gasp|auto-trad|
|[HZCvfp07Tr6TQA6s.htm](book-of-the-dead-bestiary-items/HZCvfp07Tr6TQA6s.htm)|Darkvision|auto-trad|
|[i2zp6vcnxl3idh5a.htm](book-of-the-dead-bestiary-items/i2zp6vcnxl3idh5a.htm)|Tail|auto-trad|
|[i3b5R0drNc9jYAxQ.htm](book-of-the-dead-bestiary-items/i3b5R0drNc9jYAxQ.htm)|Monk Ki Spells|auto-trad|
|[i45nUr4U6ijZvV0X.htm](book-of-the-dead-bestiary-items/i45nUr4U6ijZvV0X.htm)|Negative Healing|auto-trad|
|[i5QEKnRQOxdbucFa.htm](book-of-the-dead-bestiary-items/i5QEKnRQOxdbucFa.htm)|Grab|auto-trad|
|[i7d6smiJzgZMN4eM.htm](book-of-the-dead-bestiary-items/i7d6smiJzgZMN4eM.htm)|Negative Healing|auto-trad|
|[IAkmYPAen89nAVOl.htm](book-of-the-dead-bestiary-items/IAkmYPAen89nAVOl.htm)|Constant Spells|auto-trad|
|[ibAx8T7dlGLGUNfN.htm](book-of-the-dead-bestiary-items/ibAx8T7dlGLGUNfN.htm)|Negative Healing|auto-trad|
|[Icnp6B80vyyxB58y.htm](book-of-the-dead-bestiary-items/Icnp6B80vyyxB58y.htm)|Lifesense 30 feet|auto-trad|
|[ie93zawg4aydi426.htm](book-of-the-dead-bestiary-items/ie93zawg4aydi426.htm)|Longsword|auto-trad|
|[IeIZXKmchA7BgVL3.htm](book-of-the-dead-bestiary-items/IeIZXKmchA7BgVL3.htm)|Lifesense 60 feet|auto-trad|
|[ifsi8iYNheUHJYSo.htm](book-of-the-dead-bestiary-items/ifsi8iYNheUHJYSo.htm)|Telepathy 100 feet|auto-trad|
|[igdqv17bti9ke6yv.htm](book-of-the-dead-bestiary-items/igdqv17bti9ke6yv.htm)|Dead Shot|auto-trad|
|[IgpRQqNu2wC1srux.htm](book-of-the-dead-bestiary-items/IgpRQqNu2wC1srux.htm)|Negative Healing|auto-trad|
|[igSuS5pwnNIqzoiZ.htm](book-of-the-dead-bestiary-items/igSuS5pwnNIqzoiZ.htm)|Negative Healing|auto-trad|
|[IhOPZKCB3vAG0fWj.htm](book-of-the-dead-bestiary-items/IhOPZKCB3vAG0fWj.htm)|Darkvision|auto-trad|
|[IiHW4mzCpj9P0zUh.htm](book-of-the-dead-bestiary-items/IiHW4mzCpj9P0zUh.htm)|Negative Healing|auto-trad|
|[ijsqkg9v5y89e7nf.htm](book-of-the-dead-bestiary-items/ijsqkg9v5y89e7nf.htm)|Scream in Agony|auto-trad|
|[IMlXx0dxH2pyVYkZ.htm](book-of-the-dead-bestiary-items/IMlXx0dxH2pyVYkZ.htm)|Negative Healing|auto-trad|
|[ioRiyfYOVSBwJU1I.htm](book-of-the-dead-bestiary-items/ioRiyfYOVSBwJU1I.htm)|Divine Prepared Spells|auto-trad|
|[ipzqyff0deghe3l9.htm](book-of-the-dead-bestiary-items/ipzqyff0deghe3l9.htm)|Funereal Touch|auto-trad|
|[iqh47015p1pztflo.htm](book-of-the-dead-bestiary-items/iqh47015p1pztflo.htm)|Force Feed|auto-trad|
|[irqrb4xjw4fid7ki.htm](book-of-the-dead-bestiary-items/irqrb4xjw4fid7ki.htm)|Fist|auto-trad|
|[isudkibiz4ctaaai.htm](book-of-the-dead-bestiary-items/isudkibiz4ctaaai.htm)|Mace|auto-trad|
|[IvNNdk1lrY4Xyd5o.htm](book-of-the-dead-bestiary-items/IvNNdk1lrY4Xyd5o.htm)|Troop Movement|auto-trad|
|[IWjuKGNq99Q9FpWw.htm](book-of-the-dead-bestiary-items/IWjuKGNq99Q9FpWw.htm)|Negative Healing|auto-trad|
|[j0godn9ynpr21ye8.htm](book-of-the-dead-bestiary-items/j0godn9ynpr21ye8.htm)|Possess Animal|auto-trad|
|[j0tNYHtR2k2ufUJL.htm](book-of-the-dead-bestiary-items/j0tNYHtR2k2ufUJL.htm)|Telepathy 100 feet|auto-trad|
|[jajq24ckigdi8yla.htm](book-of-the-dead-bestiary-items/jajq24ckigdi8yla.htm)|Rosebriar Lash|auto-trad|
|[jdhshd6p6earqvr9.htm](book-of-the-dead-bestiary-items/jdhshd6p6earqvr9.htm)|Waves of Sorrow|auto-trad|
|[je6z12w9bwlsz6kk.htm](book-of-the-dead-bestiary-items/je6z12w9bwlsz6kk.htm)|Incessant Goading|auto-trad|
|[jeo96qnICvlBhjJ0.htm](book-of-the-dead-bestiary-items/jeo96qnICvlBhjJ0.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[jfD7HOJyLqdUeAsO.htm](book-of-the-dead-bestiary-items/jfD7HOJyLqdUeAsO.htm)|Negative Healing|auto-trad|
|[jjm9aum1hxm0xf4c.htm](book-of-the-dead-bestiary-items/jjm9aum1hxm0xf4c.htm)|Command Zombie|auto-trad|
|[JLuKV6PpCsAJSsQD.htm](book-of-the-dead-bestiary-items/JLuKV6PpCsAJSsQD.htm)|Champion Devotion Spells|auto-trad|
|[jmhsum5l0sk7d9ru.htm](book-of-the-dead-bestiary-items/jmhsum5l0sk7d9ru.htm)|Polong Possession|auto-trad|
|[JNoGBCYpptNAJ1M0.htm](book-of-the-dead-bestiary-items/JNoGBCYpptNAJ1M0.htm)|Shatter|auto-trad|
|[jQEBpMjZVXEPDHQy.htm](book-of-the-dead-bestiary-items/jQEBpMjZVXEPDHQy.htm)|Drain Thoughts|auto-trad|
|[jVvenC8VtGesd99v.htm](book-of-the-dead-bestiary-items/jVvenC8VtGesd99v.htm)|Graveknight's Curse|auto-trad|
|[jWKQduPw868WuVOj.htm](book-of-the-dead-bestiary-items/jWKQduPw868WuVOj.htm)|Negative Healing|auto-trad|
|[JxEmM6BuyP2WDNYs.htm](book-of-the-dead-bestiary-items/JxEmM6BuyP2WDNYs.htm)|Grab|auto-trad|
|[k55F7rzQpnXtBEJV.htm](book-of-the-dead-bestiary-items/k55F7rzQpnXtBEJV.htm)|Rend|auto-trad|
|[k7dcbgnjv5bmyic2.htm](book-of-the-dead-bestiary-items/k7dcbgnjv5bmyic2.htm)|Targeted Collapse|auto-trad|
|[KB3dAuCJuP1fvJj1.htm](book-of-the-dead-bestiary-items/KB3dAuCJuP1fvJj1.htm)|Darkvision|auto-trad|
|[KfMljjVvrcltFSTJ.htm](book-of-the-dead-bestiary-items/KfMljjVvrcltFSTJ.htm)|Negative Healing|auto-trad|
|[kfufm3rf9agn7hj4.htm](book-of-the-dead-bestiary-items/kfufm3rf9agn7hj4.htm)|Hand|auto-trad|
|[KGi4YZQdbv764frU.htm](book-of-the-dead-bestiary-items/KGi4YZQdbv764frU.htm)|Trample|auto-trad|
|[khs8jsznmljq10no.htm](book-of-the-dead-bestiary-items/khs8jsznmljq10no.htm)|Talon|auto-trad|
|[ki62sc8x79ypbv1d.htm](book-of-the-dead-bestiary-items/ki62sc8x79ypbv1d.htm)|Foot|auto-trad|
|[kijygqC36kPqtu7H.htm](book-of-the-dead-bestiary-items/kijygqC36kPqtu7H.htm)|Darkvision|auto-trad|
|[KKDdzLVqG8oTmkVx.htm](book-of-the-dead-bestiary-items/KKDdzLVqG8oTmkVx.htm)|Shield Block|auto-trad|
|[kl1jb22cgwiuukv9.htm](book-of-the-dead-bestiary-items/kl1jb22cgwiuukv9.htm)|Draining Gaze|auto-trad|
|[kpogcinxfxdtnd5j.htm](book-of-the-dead-bestiary-items/kpogcinxfxdtnd5j.htm)|Gallow Curse|auto-trad|
|[kPpdhRglPpemLa6O.htm](book-of-the-dead-bestiary-items/kPpdhRglPpemLa6O.htm)|Divine Innate Spells|auto-trad|
|[krx5EeEB0stlZv3X.htm](book-of-the-dead-bestiary-items/krx5EeEB0stlZv3X.htm)|Lifesense 60 feet|auto-trad|
|[KY0t4zPgTeWUaAUM.htm](book-of-the-dead-bestiary-items/KY0t4zPgTeWUaAUM.htm)|Swift Leap|auto-trad|
|[L0BqA6ioP9NTqswd.htm](book-of-the-dead-bestiary-items/L0BqA6ioP9NTqswd.htm)|Negative Healing|auto-trad|
|[L4ewcxHDWUZozOIs.htm](book-of-the-dead-bestiary-items/L4ewcxHDWUZozOIs.htm)|Jiang-Shi Vulnerabilities|auto-trad|
|[l525qc20u09lsupe.htm](book-of-the-dead-bestiary-items/l525qc20u09lsupe.htm)|Rhapsodic Flourish|auto-trad|
|[l7w2peurzgbblvcv.htm](book-of-the-dead-bestiary-items/l7w2peurzgbblvcv.htm)|Bottle Bound|auto-trad|
|[L95LvvfAZVKzDqz8.htm](book-of-the-dead-bestiary-items/L95LvvfAZVKzDqz8.htm)|Occult Innate Spells|auto-trad|
|[ldn7og162ohnumw1.htm](book-of-the-dead-bestiary-items/ldn7og162ohnumw1.htm)|Lignifying Hand|auto-trad|
|[LiEmzWVKW20OJoEm.htm](book-of-the-dead-bestiary-items/LiEmzWVKW20OJoEm.htm)|Darkvision|auto-trad|
|[LigXwWUfSvwcEKm4.htm](book-of-the-dead-bestiary-items/LigXwWUfSvwcEKm4.htm)|Thoughtsense (Precise) 100 feet|auto-trad|
|[lj7qJagCUYgoGe76.htm](book-of-the-dead-bestiary-items/lj7qJagCUYgoGe76.htm)|Divine Prepared Spells|auto-trad|
|[lLCa9xf3PhXLdbdq.htm](book-of-the-dead-bestiary-items/lLCa9xf3PhXLdbdq.htm)|Darkvision|auto-trad|
|[llsd8tyjckikjmsy.htm](book-of-the-dead-bestiary-items/llsd8tyjckikjmsy.htm)|Fist|auto-trad|
|[lo0krhkeahlcxl4k.htm](book-of-the-dead-bestiary-items/lo0krhkeahlcxl4k.htm)|Servitor Lunge|auto-trad|
|[loe7wunkt2aw95fw.htm](book-of-the-dead-bestiary-items/loe7wunkt2aw95fw.htm)|Blight|auto-trad|
|[ls7l1xs99s4qiknb.htm](book-of-the-dead-bestiary-items/ls7l1xs99s4qiknb.htm)|Pallid Touch|auto-trad|
|[lsjXlNKiGE8mRABo.htm](book-of-the-dead-bestiary-items/lsjXlNKiGE8mRABo.htm)|Darkvision|auto-trad|
|[m3t78jldn1s4cro7.htm](book-of-the-dead-bestiary-items/m3t78jldn1s4cro7.htm)|Brain Rot|auto-trad|
|[M5lSM9eRGm6vSvNd.htm](book-of-the-dead-bestiary-items/M5lSM9eRGm6vSvNd.htm)|Negative Healing|auto-trad|
|[M8YAKZOpPsPwvANR.htm](book-of-the-dead-bestiary-items/M8YAKZOpPsPwvANR.htm)|Negative Healing|auto-trad|
|[mAgSJlfDWhy4ZhCH.htm](book-of-the-dead-bestiary-items/mAgSJlfDWhy4ZhCH.htm)|Drain Thoughts|auto-trad|
|[mc334q1nfka7sngl.htm](book-of-the-dead-bestiary-items/mc334q1nfka7sngl.htm)|Fire Fist|auto-trad|
|[mCgB8TMnu1WQOtQJ.htm](book-of-the-dead-bestiary-items/mCgB8TMnu1WQOtQJ.htm)|Darkvision|auto-trad|
|[md6wPL6GWR80z2Z6.htm](book-of-the-dead-bestiary-items/md6wPL6GWR80z2Z6.htm)|Darkvision|auto-trad|
|[ME1twBqe6m8JvEtv.htm](book-of-the-dead-bestiary-items/ME1twBqe6m8JvEtv.htm)|Darkvision|auto-trad|
|[MFbENdEsj2e5TGOd.htm](book-of-the-dead-bestiary-items/MFbENdEsj2e5TGOd.htm)|Darkvision|auto-trad|
|[MFUrmw36LIlgw5UX.htm](book-of-the-dead-bestiary-items/MFUrmw36LIlgw5UX.htm)|Lifesense 60 feet|auto-trad|
|[mgzsxl5d5chh9f17.htm](book-of-the-dead-bestiary-items/mgzsxl5d5chh9f17.htm)|Brew Tomb Juice|auto-trad|
|[mHzpQhYPnyDQgnCL.htm](book-of-the-dead-bestiary-items/mHzpQhYPnyDQgnCL.htm)|Chains of the Dead|auto-trad|
|[MIBK76pXVm7hAU66.htm](book-of-the-dead-bestiary-items/MIBK76pXVm7hAU66.htm)|Thoughtsense (Precise) 100 feet|auto-trad|
|[mjju0x3kql87c8d3.htm](book-of-the-dead-bestiary-items/mjju0x3kql87c8d3.htm)|Sacrilegious Aura|auto-trad|
|[mjX6AVWz9h31QQlj.htm](book-of-the-dead-bestiary-items/mjX6AVWz9h31QQlj.htm)|Darkvision|auto-trad|
|[mkg6oo3hwvllmvw3.htm](book-of-the-dead-bestiary-items/mkg6oo3hwvllmvw3.htm)|Victory Celebration|auto-trad|
|[mkicpd4bhua50de0.htm](book-of-the-dead-bestiary-items/mkicpd4bhua50de0.htm)|Claw|auto-trad|
|[mlnuhck97ts6m986.htm](book-of-the-dead-bestiary-items/mlnuhck97ts6m986.htm)|Tail|auto-trad|
|[mlytlp3vwdiv4xev.htm](book-of-the-dead-bestiary-items/mlytlp3vwdiv4xev.htm)|Rise Again|auto-trad|
|[mmLtzGT7Xi5ll4Cd.htm](book-of-the-dead-bestiary-items/mmLtzGT7Xi5ll4Cd.htm)|Attack of Opportunity|auto-trad|
|[mn56u513jsfshjfz.htm](book-of-the-dead-bestiary-items/mn56u513jsfshjfz.htm)|Machete|auto-trad|
|[mo4zs1e4rvsueimw.htm](book-of-the-dead-bestiary-items/mo4zs1e4rvsueimw.htm)|Jaws|auto-trad|
|[mp4tj1xv12frp13e.htm](book-of-the-dead-bestiary-items/mp4tj1xv12frp13e.htm)|Final Blasphemy|auto-trad|
|[MRiU0bmGLDherRXE.htm](book-of-the-dead-bestiary-items/MRiU0bmGLDherRXE.htm)|Grab|auto-trad|
|[msrjzpke2pxhz0yk.htm](book-of-the-dead-bestiary-items/msrjzpke2pxhz0yk.htm)|Sand Vision|auto-trad|
|[msy9ijw3hmhu6ppi.htm](book-of-the-dead-bestiary-items/msy9ijw3hmhu6ppi.htm)|Staff|auto-trad|
|[MuhMVZGSGDdlWbDo.htm](book-of-the-dead-bestiary-items/MuhMVZGSGDdlWbDo.htm)|Shove|auto-trad|
|[mvCicXdmNRrAJwTJ.htm](book-of-the-dead-bestiary-items/mvCicXdmNRrAJwTJ.htm)|Rigor Mortis|auto-trad|
|[mW6gZ5jbhxJRUo7Y.htm](book-of-the-dead-bestiary-items/mW6gZ5jbhxJRUo7Y.htm)|Negative Healing|auto-trad|
|[mx6sscnvhtaiutid.htm](book-of-the-dead-bestiary-items/mx6sscnvhtaiutid.htm)|Meant to Live|auto-trad|
|[MYPGBIYRPHho61zG.htm](book-of-the-dead-bestiary-items/MYPGBIYRPHho61zG.htm)|Ghoul Fever|auto-trad|
|[n2cq3t3kbwnbsjkm.htm](book-of-the-dead-bestiary-items/n2cq3t3kbwnbsjkm.htm)|Sunlight Powerlessness|auto-trad|
|[n3kan97tvbssofit.htm](book-of-the-dead-bestiary-items/n3kan97tvbssofit.htm)|Servitor Attack|auto-trad|
|[n468FPlhvooypdYU.htm](book-of-the-dead-bestiary-items/n468FPlhvooypdYU.htm)|Darkvision|auto-trad|
|[n53sw596l0uo2mmy.htm](book-of-the-dead-bestiary-items/n53sw596l0uo2mmy.htm)|Sunlight Powerlessness|auto-trad|
|[n5oh9urk0vokvmfv.htm](book-of-the-dead-bestiary-items/n5oh9urk0vokvmfv.htm)|Claw|auto-trad|
|[N5PKTuC1nPKbFmU9.htm](book-of-the-dead-bestiary-items/N5PKTuC1nPKbFmU9.htm)|Primal Prepared Spells|auto-trad|
|[n8a2azRJSqQCGbOL.htm](book-of-the-dead-bestiary-items/n8a2azRJSqQCGbOL.htm)|Negative Healing|auto-trad|
|[Nca0UI8dL6FmQNm5.htm](book-of-the-dead-bestiary-items/Nca0UI8dL6FmQNm5.htm)|Darkvision|auto-trad|
|[ncmh5gpidrqgeqj5.htm](book-of-the-dead-bestiary-items/ncmh5gpidrqgeqj5.htm)|Negative Ray|auto-trad|
|[NcuughOSy9EI249D.htm](book-of-the-dead-bestiary-items/NcuughOSy9EI249D.htm)|Darkvision|auto-trad|
|[ndmnLmpNGYMdIz2H.htm](book-of-the-dead-bestiary-items/ndmnLmpNGYMdIz2H.htm)|Jaws|auto-trad|
|[ngnwinz7rvkh1cvw.htm](book-of-the-dead-bestiary-items/ngnwinz7rvkh1cvw.htm)|Bound|auto-trad|
|[NIIRwklz4cR5uYjX.htm](book-of-the-dead-bestiary-items/NIIRwklz4cR5uYjX.htm)|Negative Healing|auto-trad|
|[NjGL58OXlG8alhDr.htm](book-of-the-dead-bestiary-items/NjGL58OXlG8alhDr.htm)|Fast Healing 10|auto-trad|
|[NkZfjGZXJvvzsjuj.htm](book-of-the-dead-bestiary-items/NkZfjGZXJvvzsjuj.htm)|Primal Focus Spells|auto-trad|
|[nltFl2TPZ3VCCV0z.htm](book-of-the-dead-bestiary-items/nltFl2TPZ3VCCV0z.htm)|Darkvision|auto-trad|
|[NrspwQc7xJGzHSfc.htm](book-of-the-dead-bestiary-items/NrspwQc7xJGzHSfc.htm)|Phantom Mount|auto-trad|
|[nua4eplxdmlfifpv.htm](book-of-the-dead-bestiary-items/nua4eplxdmlfifpv.htm)|Dance with Death|auto-trad|
|[nwwxqa7o7t95veoj.htm](book-of-the-dead-bestiary-items/nwwxqa7o7t95veoj.htm)|Living Visage|auto-trad|
|[nXUOVxkr2qzcOOP6.htm](book-of-the-dead-bestiary-items/nXUOVxkr2qzcOOP6.htm)|Negative Healing|auto-trad|
|[nYA9w4d89kX8zkyS.htm](book-of-the-dead-bestiary-items/nYA9w4d89kX8zkyS.htm)|Swallow Whole|auto-trad|
|[nYgcNR6KtQIwzTTP.htm](book-of-the-dead-bestiary-items/nYgcNR6KtQIwzTTP.htm)|Shield Block|auto-trad|
|[O1MGe9IusxQ8m5IK.htm](book-of-the-dead-bestiary-items/O1MGe9IusxQ8m5IK.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[o31uzfafqxg6retc.htm](book-of-the-dead-bestiary-items/o31uzfafqxg6retc.htm)|Jaws|auto-trad|
|[o50ct5atowqid2nk.htm](book-of-the-dead-bestiary-items/o50ct5atowqid2nk.htm)|Claw|auto-trad|
|[o5vtcl40ekp9qv6n.htm](book-of-the-dead-bestiary-items/o5vtcl40ekp9qv6n.htm)|Starvation Aura|auto-trad|
|[o6I8Uh0qPM5ozyMW.htm](book-of-the-dead-bestiary-items/o6I8Uh0qPM5ozyMW.htm)|Primal Innate Spells|auto-trad|
|[o7u2o263mumslgsb.htm](book-of-the-dead-bestiary-items/o7u2o263mumslgsb.htm)|War Flail|auto-trad|
|[o82ozylu3ctjujip.htm](book-of-the-dead-bestiary-items/o82ozylu3ctjujip.htm)|Jaws|auto-trad|
|[o8cbkspx01eskehx.htm](book-of-the-dead-bestiary-items/o8cbkspx01eskehx.htm)|Terrifying Laugh|auto-trad|
|[objWEl4wPiQkNnho.htm](book-of-the-dead-bestiary-items/objWEl4wPiQkNnho.htm)|Negative Healing|auto-trad|
|[oc46s540bthxaecp.htm](book-of-the-dead-bestiary-items/oc46s540bthxaecp.htm)|Combat Current|auto-trad|
|[oer9h35wciqfjj68.htm](book-of-the-dead-bestiary-items/oer9h35wciqfjj68.htm)|Claw|auto-trad|
|[OFQw1wCzWACEpFPh.htm](book-of-the-dead-bestiary-items/OFQw1wCzWACEpFPh.htm)|Negative Healing|auto-trad|
|[ohpwpe4hf1byw99z.htm](book-of-the-dead-bestiary-items/ohpwpe4hf1byw99z.htm)|Rejuvenation|auto-trad|
|[olrz72ebabiexxj8.htm](book-of-the-dead-bestiary-items/olrz72ebabiexxj8.htm)|Synaptic Overload|auto-trad|
|[om967vglnfob32of.htm](book-of-the-dead-bestiary-items/om967vglnfob32of.htm)|Frightful Battle Cry|auto-trad|
|[ombjfkyxjmwbb9xc.htm](book-of-the-dead-bestiary-items/ombjfkyxjmwbb9xc.htm)|Drain Life|auto-trad|
|[onemB0KzXTHPiaTQ.htm](book-of-the-dead-bestiary-items/onemB0KzXTHPiaTQ.htm)|Darkvision|auto-trad|
|[onsl5qalhafgwyhe.htm](book-of-the-dead-bestiary-items/onsl5qalhafgwyhe.htm)|Compression|auto-trad|
|[oramesvya495nwdm.htm](book-of-the-dead-bestiary-items/oramesvya495nwdm.htm)|Shuriken|auto-trad|
|[OrGqVpvK9Ri1osng.htm](book-of-the-dead-bestiary-items/OrGqVpvK9Ri1osng.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[ovW00fb5RQ0fss2D.htm](book-of-the-dead-bestiary-items/ovW00fb5RQ0fss2D.htm)|Darkvision|auto-trad|
|[p1j4vk8x9pqsovd6.htm](book-of-the-dead-bestiary-items/p1j4vk8x9pqsovd6.htm)|Sense Companion|auto-trad|
|[p2homhgdk4xiy7ve.htm](book-of-the-dead-bestiary-items/p2homhgdk4xiy7ve.htm)|Breath Weapon|auto-trad|
|[p2rb04uhqbleqtpy.htm](book-of-the-dead-bestiary-items/p2rb04uhqbleqtpy.htm)|Claw|auto-trad|
|[p3fyg7e66y8gq61g.htm](book-of-the-dead-bestiary-items/p3fyg7e66y8gq61g.htm)|Claw|auto-trad|
|[p44HhukqlJ7IVvti.htm](book-of-the-dead-bestiary-items/p44HhukqlJ7IVvti.htm)|Negative Healing|auto-trad|
|[P5HJ6L1BVQCeHoJr.htm](book-of-the-dead-bestiary-items/P5HJ6L1BVQCeHoJr.htm)|+18 to Initiative|auto-trad|
|[p5ohcfvydik0i2mz.htm](book-of-the-dead-bestiary-items/p5ohcfvydik0i2mz.htm)|Hungry Armor|auto-trad|
|[p7BEvZc5WuJeZIJf.htm](book-of-the-dead-bestiary-items/p7BEvZc5WuJeZIJf.htm)|Constrict|auto-trad|
|[P7FUCg0CYhhrSYXp.htm](book-of-the-dead-bestiary-items/P7FUCg0CYhhrSYXp.htm)|Negative Healing|auto-trad|
|[P9uhzDYGuPtmPcTt.htm](book-of-the-dead-bestiary-items/P9uhzDYGuPtmPcTt.htm)|Darkvision 60 feet|auto-trad|
|[pbwnkebF8Azr64pn.htm](book-of-the-dead-bestiary-items/pbwnkebF8Azr64pn.htm)|Grab|auto-trad|
|[pcv2wjgq3skfdwr1.htm](book-of-the-dead-bestiary-items/pcv2wjgq3skfdwr1.htm)|Stench|auto-trad|
|[Pd2a0LCF9zAkHizr.htm](book-of-the-dead-bestiary-items/Pd2a0LCF9zAkHizr.htm)|Divine Innate Spells|auto-trad|
|[pdt8bn3rsl7rxryb.htm](book-of-the-dead-bestiary-items/pdt8bn3rsl7rxryb.htm)|Summon Weapon|auto-trad|
|[pdtNmk8Zc7lN4BFF.htm](book-of-the-dead-bestiary-items/pdtNmk8Zc7lN4BFF.htm)|Grab|auto-trad|
|[piqtoxtcbhe4b1a1.htm](book-of-the-dead-bestiary-items/piqtoxtcbhe4b1a1.htm)|Great Despair|auto-trad|
|[pJdmn8co20iXokx1.htm](book-of-the-dead-bestiary-items/pJdmn8co20iXokx1.htm)|Negative Healing|auto-trad|
|[pKH0djjdIbsZYSyj.htm](book-of-the-dead-bestiary-items/pKH0djjdIbsZYSyj.htm)|Darkvision|auto-trad|
|[plczq6dno7lmsveb.htm](book-of-the-dead-bestiary-items/plczq6dno7lmsveb.htm)|Adopt Guise|auto-trad|
|[pm509slp2godzutv.htm](book-of-the-dead-bestiary-items/pm509slp2godzutv.htm)|Roll the Bones|auto-trad|
|[pQh6aSqjZQoy6EAa.htm](book-of-the-dead-bestiary-items/pQh6aSqjZQoy6EAa.htm)|The Upper Hand|auto-trad|
|[PQnmOAwTpUMykmR6.htm](book-of-the-dead-bestiary-items/PQnmOAwTpUMykmR6.htm)|Darkvision|auto-trad|
|[pst72akzo45b7a0z.htm](book-of-the-dead-bestiary-items/pst72akzo45b7a0z.htm)|Parry Dance|auto-trad|
|[pvl5y6qz89i11vc5.htm](book-of-the-dead-bestiary-items/pvl5y6qz89i11vc5.htm)|Sense Murderer|auto-trad|
|[pYCh0bsqXGiKu08p.htm](book-of-the-dead-bestiary-items/pYCh0bsqXGiKu08p.htm)|Darkvision|auto-trad|
|[q6iakq314is9gj9c.htm](book-of-the-dead-bestiary-items/q6iakq314is9gj9c.htm)|Ecstatic Ululation|auto-trad|
|[qbupjn2tlljiraow.htm](book-of-the-dead-bestiary-items/qbupjn2tlljiraow.htm)|Stone Antler|auto-trad|
|[qDKNtEMBCrIlKW6A.htm](book-of-the-dead-bestiary-items/qDKNtEMBCrIlKW6A.htm)|Blight Mastery|auto-trad|
|[qmkVNjGJoAddHkl7.htm](book-of-the-dead-bestiary-items/qmkVNjGJoAddHkl7.htm)|Greater Darkvision|auto-trad|
|[qmXjR6UjkPsa5Yrc.htm](book-of-the-dead-bestiary-items/qmXjR6UjkPsa5Yrc.htm)|Improved Grab|auto-trad|
|[qo8ae1c96drn6owz.htm](book-of-the-dead-bestiary-items/qo8ae1c96drn6owz.htm)|Claw|auto-trad|
|[qqbbtn1cc3yyrpdv.htm](book-of-the-dead-bestiary-items/qqbbtn1cc3yyrpdv.htm)|Slow|auto-trad|
|[QQXKnrwMluEorlEb.htm](book-of-the-dead-bestiary-items/QQXKnrwMluEorlEb.htm)|Darkvision|auto-trad|
|[qrob277923ccvj5f.htm](book-of-the-dead-bestiary-items/qrob277923ccvj5f.htm)|Flicker|auto-trad|
|[QRR9beKx0noS0RfN.htm](book-of-the-dead-bestiary-items/QRR9beKx0noS0RfN.htm)|Lifesense 60 feet|auto-trad|
|[QTvQkFjvczOYx7t8.htm](book-of-the-dead-bestiary-items/QTvQkFjvczOYx7t8.htm)|One More Breath|auto-trad|
|[qvgyve9ubbhtjhlb.htm](book-of-the-dead-bestiary-items/qvgyve9ubbhtjhlb.htm)|Consecration Vulnerability|auto-trad|
|[qViybdc6hq6JwJNP.htm](book-of-the-dead-bestiary-items/qViybdc6hq6JwJNP.htm)|Warped Fulu|auto-trad|
|[qVmgf9hKv2Wt0x9f.htm](book-of-the-dead-bestiary-items/qVmgf9hKv2Wt0x9f.htm)|Telepathy 100 feet|auto-trad|
|[qWb2hnLxcNgrwqy7.htm](book-of-the-dead-bestiary-items/qWb2hnLxcNgrwqy7.htm)|Darkvision|auto-trad|
|[qxv9k072qjlqqrmv.htm](book-of-the-dead-bestiary-items/qxv9k072qjlqqrmv.htm)|Bite Back|auto-trad|
|[r1hdbf8k93avvj7u.htm](book-of-the-dead-bestiary-items/r1hdbf8k93avvj7u.htm)|Forest Guardian|auto-trad|
|[r2m3ybfe440pmnlu.htm](book-of-the-dead-bestiary-items/r2m3ybfe440pmnlu.htm)|Terrain Advantage|auto-trad|
|[r5xddqaz12nw9vmw.htm](book-of-the-dead-bestiary-items/r5xddqaz12nw9vmw.htm)|Muscular Leap|auto-trad|
|[R7D68innrOxLHJAm.htm](book-of-the-dead-bestiary-items/R7D68innrOxLHJAm.htm)|Musical Assault|auto-trad|
|[r7kfGxLnvo6gvY8J.htm](book-of-the-dead-bestiary-items/r7kfGxLnvo6gvY8J.htm)|Rejuvenation|auto-trad|
|[r867EAQMTLMUJiHC.htm](book-of-the-dead-bestiary-items/r867EAQMTLMUJiHC.htm)|Greater Darkvision|auto-trad|
|[r9tJVChsQaE6jEtS.htm](book-of-the-dead-bestiary-items/r9tJVChsQaE6jEtS.htm)|Darkvision|auto-trad|
|[RaJiotddHOyL70ol.htm](book-of-the-dead-bestiary-items/RaJiotddHOyL70ol.htm)|Negative Healing|auto-trad|
|[rat6z2dx8o2ocr0x.htm](book-of-the-dead-bestiary-items/rat6z2dx8o2ocr0x.htm)|Aura of Peace|auto-trad|
|[rfeIYZBHpVkv6qJZ.htm](book-of-the-dead-bestiary-items/rfeIYZBHpVkv6qJZ.htm)|Negative Healing|auto-trad|
|[rfqiqYvSTPZWOWVq.htm](book-of-the-dead-bestiary-items/rfqiqYvSTPZWOWVq.htm)|Negative Healing|auto-trad|
|[rfs2498hafjirg6i.htm](book-of-the-dead-bestiary-items/rfs2498hafjirg6i.htm)|Claw|auto-trad|
|[rh6570cau7s1l659.htm](book-of-the-dead-bestiary-items/rh6570cau7s1l659.htm)|Arm Spike|auto-trad|
|[ri32o76yg6cyuq05.htm](book-of-the-dead-bestiary-items/ri32o76yg6cyuq05.htm)|Glaive|auto-trad|
|[rIOKUxnAXsQC39t6.htm](book-of-the-dead-bestiary-items/rIOKUxnAXsQC39t6.htm)|Darkvision|auto-trad|
|[Rk6vaUzdBA05Ipr4.htm](book-of-the-dead-bestiary-items/Rk6vaUzdBA05Ipr4.htm)|Negative Healing|auto-trad|
|[RKJRHH3TlsIfcjRw.htm](book-of-the-dead-bestiary-items/RKJRHH3TlsIfcjRw.htm)|Negative Healing|auto-trad|
|[rNWhoVEyE4tbJ7UF.htm](book-of-the-dead-bestiary-items/rNWhoVEyE4tbJ7UF.htm)|Darkvision|auto-trad|
|[ROMNXYjyp9b5wU7t.htm](book-of-the-dead-bestiary-items/ROMNXYjyp9b5wU7t.htm)|Darkvision|auto-trad|
|[RpSDL9pRSQiu3lu9.htm](book-of-the-dead-bestiary-items/RpSDL9pRSQiu3lu9.htm)|Cleric Domain Spells|auto-trad|
|[rpzk7dfngnq6h4fj.htm](book-of-the-dead-bestiary-items/rpzk7dfngnq6h4fj.htm)|Crossbow|auto-trad|
|[rsHuZ4AV9kbemHYN.htm](book-of-the-dead-bestiary-items/rsHuZ4AV9kbemHYN.htm)|Negative Healing|auto-trad|
|[RsT9PJggBJ9E7yXV.htm](book-of-the-dead-bestiary-items/RsT9PJggBJ9E7yXV.htm)|Shut In|auto-trad|
|[rt3cft7ln3rvinzi.htm](book-of-the-dead-bestiary-items/rt3cft7ln3rvinzi.htm)|Teleporting Clothesline|auto-trad|
|[RvTQTNsY192UMAWY.htm](book-of-the-dead-bestiary-items/RvTQTNsY192UMAWY.htm)|Drain Qi|auto-trad|
|[RZ8nmKkTFCoNYpw1.htm](book-of-the-dead-bestiary-items/RZ8nmKkTFCoNYpw1.htm)|Negative Healing|auto-trad|
|[RZU7C9rxkfFtWjAK.htm](book-of-the-dead-bestiary-items/RZU7C9rxkfFtWjAK.htm)|Negative Healing|auto-trad|
|[S03qKeYKeO3g8bMB.htm](book-of-the-dead-bestiary-items/S03qKeYKeO3g8bMB.htm)|Darkvision|auto-trad|
|[s0ilcnpw3egzb7jx.htm](book-of-the-dead-bestiary-items/s0ilcnpw3egzb7jx.htm)|Jaws|auto-trad|
|[S5HKepuRcLZyCgzt.htm](book-of-the-dead-bestiary-items/S5HKepuRcLZyCgzt.htm)|Paralysis|auto-trad|
|[s6celq1ixxucp8ro.htm](book-of-the-dead-bestiary-items/s6celq1ixxucp8ro.htm)|Death Gasp|auto-trad|
|[SAnGJYM6pCkTSTFE.htm](book-of-the-dead-bestiary-items/SAnGJYM6pCkTSTFE.htm)|Chomp|auto-trad|
|[sbuu4mk2ssfmalco.htm](book-of-the-dead-bestiary-items/sbuu4mk2ssfmalco.htm)|Claw|auto-trad|
|[sbw8eejalnwiv9pt.htm](book-of-the-dead-bestiary-items/sbw8eejalnwiv9pt.htm)|Soulscent (Imprecise) 100 feet|auto-trad|
|[sE93R44GXNoE5W3Y.htm](book-of-the-dead-bestiary-items/sE93R44GXNoE5W3Y.htm)|Drain Qi|auto-trad|
|[SeDr8IsuA0cd6pKi.htm](book-of-the-dead-bestiary-items/SeDr8IsuA0cd6pKi.htm)|Rejuvenation|auto-trad|
|[sfOhU9Ljw1nJ9Vw8.htm](book-of-the-dead-bestiary-items/sfOhU9Ljw1nJ9Vw8.htm)|Negative Healing|auto-trad|
|[SfwWBcVieSEQcUwE.htm](book-of-the-dead-bestiary-items/SfwWBcVieSEQcUwE.htm)|Divine Innate Spells|auto-trad|
|[sg28ok3v73dn8dvi.htm](book-of-the-dead-bestiary-items/sg28ok3v73dn8dvi.htm)|Snatch|auto-trad|
|[sgrD5tLKLJMdGbYH.htm](book-of-the-dead-bestiary-items/sgrD5tLKLJMdGbYH.htm)|Improved Grab|auto-trad|
|[ShZ2CAIzf05F41B2.htm](book-of-the-dead-bestiary-items/ShZ2CAIzf05F41B2.htm)|Stop Heart|auto-trad|
|[sjrL9SViifyZvj1G.htm](book-of-the-dead-bestiary-items/sjrL9SViifyZvj1G.htm)|Negative Healing|auto-trad|
|[so0db4mc0q95h2lm.htm](book-of-the-dead-bestiary-items/so0db4mc0q95h2lm.htm)|Enshroud|auto-trad|
|[SQt7rET9Z2MgoK8E.htm](book-of-the-dead-bestiary-items/SQt7rET9Z2MgoK8E.htm)|Rejuvenation|auto-trad|
|[ssmbr5eqwybx9xne.htm](book-of-the-dead-bestiary-items/ssmbr5eqwybx9xne.htm)|Endless Suffering|auto-trad|
|[ssocwemmugpprhuc.htm](book-of-the-dead-bestiary-items/ssocwemmugpprhuc.htm)|Sunlight Powerlessness|auto-trad|
|[suMVXLkMmzCGv2fJ.htm](book-of-the-dead-bestiary-items/suMVXLkMmzCGv2fJ.htm)|Fist|auto-trad|
|[supp5wizpe79eqi9.htm](book-of-the-dead-bestiary-items/supp5wizpe79eqi9.htm)|Bloody Spew|auto-trad|
|[sUsdrW2eEA7nzp6P.htm](book-of-the-dead-bestiary-items/sUsdrW2eEA7nzp6P.htm)|Divine Innate Spells|auto-trad|
|[SwCl0ytYvoRbPKKM.htm](book-of-the-dead-bestiary-items/SwCl0ytYvoRbPKKM.htm)|Darkvision|auto-trad|
|[sWtjwSeVA2Yi9URn.htm](book-of-the-dead-bestiary-items/sWtjwSeVA2Yi9URn.htm)|Jaws|auto-trad|
|[SXOUzgOVcsJXmJtF.htm](book-of-the-dead-bestiary-items/SXOUzgOVcsJXmJtF.htm)|Gather Spirit|auto-trad|
|[syR8yFxw3sNEGkUO.htm](book-of-the-dead-bestiary-items/syR8yFxw3sNEGkUO.htm)|Negative Healing|auto-trad|
|[SZNvFrnofD8dddxU.htm](book-of-the-dead-bestiary-items/SZNvFrnofD8dddxU.htm)|Divine Prepared Spells|auto-trad|
|[t1x1x6iwe8eqpsxc.htm](book-of-the-dead-bestiary-items/t1x1x6iwe8eqpsxc.htm)|Staff|auto-trad|
|[t2vkqgxwqps1lc2c.htm](book-of-the-dead-bestiary-items/t2vkqgxwqps1lc2c.htm)|Plant|auto-trad|
|[t31wSSg27UyZRToz.htm](book-of-the-dead-bestiary-items/t31wSSg27UyZRToz.htm)|Divine Innate Spells|auto-trad|
|[t57awjzu5thorizx.htm](book-of-the-dead-bestiary-items/t57awjzu5thorizx.htm)|Pistol Whip|auto-trad|
|[t6tofj5bxmm53nld.htm](book-of-the-dead-bestiary-items/t6tofj5bxmm53nld.htm)|Death Gasp|auto-trad|
|[TA1sQhusAvQaJjnY.htm](book-of-the-dead-bestiary-items/TA1sQhusAvQaJjnY.htm)|Change Shape|auto-trad|
|[tabg0vo3elkeri2r.htm](book-of-the-dead-bestiary-items/tabg0vo3elkeri2r.htm)|Horrifying Screech|auto-trad|
|[TCn4mhscYuvLcXc1.htm](book-of-the-dead-bestiary-items/TCn4mhscYuvLcXc1.htm)|Darkvision|auto-trad|
|[TDyZOrJxLY1hcPKJ.htm](book-of-the-dead-bestiary-items/TDyZOrJxLY1hcPKJ.htm)|Drain Life|auto-trad|
|[thmdq28mehsfvjtu.htm](book-of-the-dead-bestiary-items/thmdq28mehsfvjtu.htm)|Severed Trunk|auto-trad|
|[thmuyoj3asul9w8q.htm](book-of-the-dead-bestiary-items/thmuyoj3asul9w8q.htm)|Claw|auto-trad|
|[TkNnlQ5Y9tP89lcL.htm](book-of-the-dead-bestiary-items/TkNnlQ5Y9tP89lcL.htm)|Desperate Meal|auto-trad|
|[TN9YVnHaKRk3uwTv.htm](book-of-the-dead-bestiary-items/TN9YVnHaKRk3uwTv.htm)|Negative Healing|auto-trad|
|[tOSvo39Ofu2f5ufu.htm](book-of-the-dead-bestiary-items/tOSvo39Ofu2f5ufu.htm)|Rejuvenation|auto-trad|
|[tskaoz4x2uhz0ss3.htm](book-of-the-dead-bestiary-items/tskaoz4x2uhz0ss3.htm)|Bone Debris|auto-trad|
|[tult7kj40ygqn8tz.htm](book-of-the-dead-bestiary-items/tult7kj40ygqn8tz.htm)|Ghostly Grasp|auto-trad|
|[U0g3auuSZ4pwJU82.htm](book-of-the-dead-bestiary-items/U0g3auuSZ4pwJU82.htm)|Greater Darkvision|auto-trad|
|[U34PxMHZ0fS0k12A.htm](book-of-the-dead-bestiary-items/U34PxMHZ0fS0k12A.htm)|Form Up|auto-trad|
|[u35onovz82ogkm7o.htm](book-of-the-dead-bestiary-items/u35onovz82ogkm7o.htm)|Take Root|auto-trad|
|[u3b2rv8mv9k9rb23.htm](book-of-the-dead-bestiary-items/u3b2rv8mv9k9rb23.htm)|Great Despair|auto-trad|
|[u5fvma01emd3ped8.htm](book-of-the-dead-bestiary-items/u5fvma01emd3ped8.htm)|Jaws|auto-trad|
|[u5q8d8r0yfjwcdoj.htm](book-of-the-dead-bestiary-items/u5q8d8r0yfjwcdoj.htm)|Ship Bound|auto-trad|
|[u7gqkauw2hdfde3u.htm](book-of-the-dead-bestiary-items/u7gqkauw2hdfde3u.htm)|Involuntary Reaction|auto-trad|
|[uB6EJzJpTE3PRvS8.htm](book-of-the-dead-bestiary-items/uB6EJzJpTE3PRvS8.htm)|Negative Healing|auto-trad|
|[ubqhrgnv2m0l1100.htm](book-of-the-dead-bestiary-items/ubqhrgnv2m0l1100.htm)|Claw|auto-trad|
|[ubsx377x3tktofot.htm](book-of-the-dead-bestiary-items/ubsx377x3tktofot.htm)|Claw|auto-trad|
|[UBzBGEYk2sLnPlbQ.htm](book-of-the-dead-bestiary-items/UBzBGEYk2sLnPlbQ.htm)|Occult Spontaneous Spells|auto-trad|
|[Uc4DijFU9ApvpWFZ.htm](book-of-the-dead-bestiary-items/Uc4DijFU9ApvpWFZ.htm)|Consume Flesh|auto-trad|
|[ucz1Sz93jpBvRWbp.htm](book-of-the-dead-bestiary-items/ucz1Sz93jpBvRWbp.htm)|Darkvision|auto-trad|
|[Ud7fmRAenQnd8Zxa.htm](book-of-the-dead-bestiary-items/Ud7fmRAenQnd8Zxa.htm)|Negative Healing|auto-trad|
|[udo3xq7mjc590h1k.htm](book-of-the-dead-bestiary-items/udo3xq7mjc590h1k.htm)|Fire Mote|auto-trad|
|[ugPuUFMgsKh55RQn.htm](book-of-the-dead-bestiary-items/ugPuUFMgsKh55RQn.htm)|Divine Focus Spells|auto-trad|
|[uh3gw9uok4g0fuzy.htm](book-of-the-dead-bestiary-items/uh3gw9uok4g0fuzy.htm)|Reap Faith|auto-trad|
|[UHjOHKWLHa1xT8ID.htm](book-of-the-dead-bestiary-items/UHjOHKWLHa1xT8ID.htm)|Slow|auto-trad|
|[uhTWUFxW0j3Yev1T.htm](book-of-the-dead-bestiary-items/uhTWUFxW0j3Yev1T.htm)|Negative Healing|auto-trad|
|[UK03LEy2NrVO5Ir3.htm](book-of-the-dead-bestiary-items/UK03LEy2NrVO5Ir3.htm)|Telepathy 100 feet|auto-trad|
|[ukiq9bqccfjpoc72.htm](book-of-the-dead-bestiary-items/ukiq9bqccfjpoc72.htm)|Submission Lock|auto-trad|
|[un97s9dr8tua2f9t.htm](book-of-the-dead-bestiary-items/un97s9dr8tua2f9t.htm)|Fangs|auto-trad|
|[uovpjaklzm8wgqbt.htm](book-of-the-dead-bestiary-items/uovpjaklzm8wgqbt.htm)|Spectral Charge|auto-trad|
|[uphuqzm8b7b57k4d.htm](book-of-the-dead-bestiary-items/uphuqzm8b7b57k4d.htm)|Final Blasphemy|auto-trad|
|[urw7slqqg18c2bqr.htm](book-of-the-dead-bestiary-items/urw7slqqg18c2bqr.htm)|Change Posture|auto-trad|
|[UTArnl71MY7BVLQc.htm](book-of-the-dead-bestiary-items/UTArnl71MY7BVLQc.htm)|Negative Healing|auto-trad|
|[UuMVTRKxoumdKVH4.htm](book-of-the-dead-bestiary-items/UuMVTRKxoumdKVH4.htm)|At-Will Spells|auto-trad|
|[UVtZ0kwBh5CQGxkT.htm](book-of-the-dead-bestiary-items/UVtZ0kwBh5CQGxkT.htm)|Stalk|auto-trad|
|[UwqJzuhMBvtDiE1L.htm](book-of-the-dead-bestiary-items/UwqJzuhMBvtDiE1L.htm)|Darkvision|auto-trad|
|[uyk9qJPvkuv0JU5M.htm](book-of-the-dead-bestiary-items/uyk9qJPvkuv0JU5M.htm)|Mental Bind|auto-trad|
|[uyym1abw28sm0iuq.htm](book-of-the-dead-bestiary-items/uyym1abw28sm0iuq.htm)|Crush Item|auto-trad|
|[v006gzbsc924nhz4.htm](book-of-the-dead-bestiary-items/v006gzbsc924nhz4.htm)|Exhale|auto-trad|
|[v0q9b81a0anjrpfc.htm](book-of-the-dead-bestiary-items/v0q9b81a0anjrpfc.htm)|Chastise Heretic|auto-trad|
|[v1cjchmhqo6lvdjy.htm](book-of-the-dead-bestiary-items/v1cjchmhqo6lvdjy.htm)|Coordinated Strike|auto-trad|
|[v1KNai0kJiubpLMO.htm](book-of-the-dead-bestiary-items/v1KNai0kJiubpLMO.htm)|Darkvision|auto-trad|
|[v3e78dwshaq5ch57.htm](book-of-the-dead-bestiary-items/v3e78dwshaq5ch57.htm)|Consult the Text|auto-trad|
|[V5QNa5hnkOnSMJVv.htm](book-of-the-dead-bestiary-items/V5QNa5hnkOnSMJVv.htm)|Attack of Opportunity (Special)|auto-trad|
|[v6OYQ9mA3KrJqI4Q.htm](book-of-the-dead-bestiary-items/v6OYQ9mA3KrJqI4Q.htm)|Negative Healing|auto-trad|
|[v7iYGiJ8yz2GeTHT.htm](book-of-the-dead-bestiary-items/v7iYGiJ8yz2GeTHT.htm)|Darkvision|auto-trad|
|[VaJj5noJcvvTyDKp.htm](book-of-the-dead-bestiary-items/VaJj5noJcvvTyDKp.htm)|Grab|auto-trad|
|[vb6es4dznn8764o4.htm](book-of-the-dead-bestiary-items/vb6es4dznn8764o4.htm)|Fist|auto-trad|
|[vc4ulmssbysj8wam.htm](book-of-the-dead-bestiary-items/vc4ulmssbysj8wam.htm)|Catching Bite|auto-trad|
|[vd5bzz1j83bmas2b.htm](book-of-the-dead-bestiary-items/vd5bzz1j83bmas2b.htm)|Spear|auto-trad|
|[VdFWJZh6U3QHXfn7.htm](book-of-the-dead-bestiary-items/VdFWJZh6U3QHXfn7.htm)|Negative Healing|auto-trad|
|[vghi4lo4jsex6xvg.htm](book-of-the-dead-bestiary-items/vghi4lo4jsex6xvg.htm)|Spellstealing Counter|auto-trad|
|[vgpzwfs5atbe56al.htm](book-of-the-dead-bestiary-items/vgpzwfs5atbe56al.htm)|Cold Rot|auto-trad|
|[vj9p9alcrxuahtfn.htm](book-of-the-dead-bestiary-items/vj9p9alcrxuahtfn.htm)|Scythe Claw|auto-trad|
|[VJtbJPj39575cPho.htm](book-of-the-dead-bestiary-items/VJtbJPj39575cPho.htm)|Change Shape|auto-trad|
|[vkuk04wj4sfihq3b.htm](book-of-the-dead-bestiary-items/vkuk04wj4sfihq3b.htm)|Corpse Scent (Imprecise) 1 mile|auto-trad|
|[vlcr44dvupfosqoq.htm](book-of-the-dead-bestiary-items/vlcr44dvupfosqoq.htm)|Steady Spellcasting|auto-trad|
|[vlnnscd18jfqcdq9.htm](book-of-the-dead-bestiary-items/vlnnscd18jfqcdq9.htm)|Scything Blade|auto-trad|
|[vM9qUCcmjXuCKUvw.htm](book-of-the-dead-bestiary-items/vM9qUCcmjXuCKUvw.htm)|Divine Innate Spells|auto-trad|
|[VnobmUcJZ8e6awlN.htm](book-of-the-dead-bestiary-items/VnobmUcJZ8e6awlN.htm)|Rituals|auto-trad|
|[VqcQnwyahELeDBwK.htm](book-of-the-dead-bestiary-items/VqcQnwyahELeDBwK.htm)|Greater Darkvision|auto-trad|
|[vqtr9yyxp561y34u.htm](book-of-the-dead-bestiary-items/vqtr9yyxp561y34u.htm)|Ghostly Hand|auto-trad|
|[vrbzxalz7mler6oi.htm](book-of-the-dead-bestiary-items/vrbzxalz7mler6oi.htm)|Snow Vision|auto-trad|
|[VuchmKXLlqk2wO9c.htm](book-of-the-dead-bestiary-items/VuchmKXLlqk2wO9c.htm)|Grab|auto-trad|
|[vvxq8pxshmt9yd29.htm](book-of-the-dead-bestiary-items/vvxq8pxshmt9yd29.htm)|Frozen Breath|auto-trad|
|[vW68sTmazYmk9G6Q.htm](book-of-the-dead-bestiary-items/vW68sTmazYmk9G6Q.htm)|Feral Leap|auto-trad|
|[vX9cmoHPlrRAHeYs.htm](book-of-the-dead-bestiary-items/vX9cmoHPlrRAHeYs.htm)|Bullets of Vengeance|auto-trad|
|[VZR6D452W8R3BDqd.htm](book-of-the-dead-bestiary-items/VZR6D452W8R3BDqd.htm)|Darkvision|auto-trad|
|[W0TicentuZL4V7qS.htm](book-of-the-dead-bestiary-items/W0TicentuZL4V7qS.htm)|Darkvision|auto-trad|
|[w4cvlv6bdzp5wcxx.htm](book-of-the-dead-bestiary-items/w4cvlv6bdzp5wcxx.htm)|Hand|auto-trad|
|[wauesfz491hsnkk4.htm](book-of-the-dead-bestiary-items/wauesfz491hsnkk4.htm)|Destructive Finale|auto-trad|
|[wbmhtkddtggv5bgk.htm](book-of-the-dead-bestiary-items/wbmhtkddtggv5bgk.htm)|Fist|auto-trad|
|[wbqipaniohqje45s.htm](book-of-the-dead-bestiary-items/wbqipaniohqje45s.htm)|Devour Flesh|auto-trad|
|[wjdkro5cizi5vre7.htm](book-of-the-dead-bestiary-items/wjdkro5cizi5vre7.htm)|Eyes of the Tyrant|auto-trad|
|[WkoSPIMoXbCHFRZj.htm](book-of-the-dead-bestiary-items/WkoSPIMoXbCHFRZj.htm)|Grab|auto-trad|
|[wln6jtro52i3xd9c.htm](book-of-the-dead-bestiary-items/wln6jtro52i3xd9c.htm)|Exhume|auto-trad|
|[WnQDVEnbKG81zOIf.htm](book-of-the-dead-bestiary-items/WnQDVEnbKG81zOIf.htm)|Occult Prepared Spells|auto-trad|
|[wo29s0y8jcz4isnc.htm](book-of-the-dead-bestiary-items/wo29s0y8jcz4isnc.htm)|Claw|auto-trad|
|[woLgXjw2p6sRigIz.htm](book-of-the-dead-bestiary-items/woLgXjw2p6sRigIz.htm)|Grab|auto-trad|
|[woUqNS1copPPro5x.htm](book-of-the-dead-bestiary-items/woUqNS1copPPro5x.htm)|Negative Healing|auto-trad|
|[WPhT2IXNFPoANk1z.htm](book-of-the-dead-bestiary-items/WPhT2IXNFPoANk1z.htm)|Grab|auto-trad|
|[WPV9eNOze72jDJV4.htm](book-of-the-dead-bestiary-items/WPV9eNOze72jDJV4.htm)|Frightful Presence|auto-trad|
|[wq8dsdznit8qtxcv.htm](book-of-the-dead-bestiary-items/wq8dsdznit8qtxcv.htm)|Fist|auto-trad|
|[wRVqT1QO1O1ru1SY.htm](book-of-the-dead-bestiary-items/wRVqT1QO1O1ru1SY.htm)|Trample|auto-trad|
|[wspi8nMzAlqEekgs.htm](book-of-the-dead-bestiary-items/wspi8nMzAlqEekgs.htm)|Telepathy 100 feet|auto-trad|
|[WthNDt9DjtNzlPes.htm](book-of-the-dead-bestiary-items/WthNDt9DjtNzlPes.htm)|Negative Healing|auto-trad|
|[Wunpn2wRMKV7JLZr.htm](book-of-the-dead-bestiary-items/Wunpn2wRMKV7JLZr.htm)|Darkvision|auto-trad|
|[WUYJjrYb5HbQgPNA.htm](book-of-the-dead-bestiary-items/WUYJjrYb5HbQgPNA.htm)|Negative Healing|auto-trad|
|[wvs0vnvwndnjqq6u.htm](book-of-the-dead-bestiary-items/wvs0vnvwndnjqq6u.htm)|Chain Capture|auto-trad|
|[ww6g31ronygtiit4.htm](book-of-the-dead-bestiary-items/ww6g31ronygtiit4.htm)|Primal Corruption|auto-trad|
|[X1pEJ8DPmm00nq2l.htm](book-of-the-dead-bestiary-items/X1pEJ8DPmm00nq2l.htm)|Constant Spells|auto-trad|
|[X22kke8lGbonOyuE.htm](book-of-the-dead-bestiary-items/X22kke8lGbonOyuE.htm)|Lifesense 60 feet|auto-trad|
|[x9iTyqF9YiSbKaz7.htm](book-of-the-dead-bestiary-items/x9iTyqF9YiSbKaz7.htm)|Frightful Presence|auto-trad|
|[xat38aa9u2gh8xpz.htm](book-of-the-dead-bestiary-items/xat38aa9u2gh8xpz.htm)|Curse of Eternal Sleep|auto-trad|
|[xcdtl5hp12cdn0au.htm](book-of-the-dead-bestiary-items/xcdtl5hp12cdn0au.htm)|Necrotic Runoff|auto-trad|
|[xcxndcu0v1o1sayq.htm](book-of-the-dead-bestiary-items/xcxndcu0v1o1sayq.htm)|Fist|auto-trad|
|[xf1472qm9rkgerh8.htm](book-of-the-dead-bestiary-items/xf1472qm9rkgerh8.htm)|Aura of Doom|auto-trad|
|[xicsjt4e3glrfz9g.htm](book-of-the-dead-bestiary-items/xicsjt4e3glrfz9g.htm)|Final Spite|auto-trad|
|[XlVJgIVvBqc5DGGe.htm](book-of-the-dead-bestiary-items/XlVJgIVvBqc5DGGe.htm)|Darkvision|auto-trad|
|[Xoqie6AhgsKZ7wJ3.htm](book-of-the-dead-bestiary-items/Xoqie6AhgsKZ7wJ3.htm)|Sudden Chill|auto-trad|
|[xrhhvmqzd4qp9cx9.htm](book-of-the-dead-bestiary-items/xrhhvmqzd4qp9cx9.htm)|Steady Spellcasting|auto-trad|
|[xt70n62im3owz0y2.htm](book-of-the-dead-bestiary-items/xt70n62im3owz0y2.htm)|Crumble|auto-trad|
|[xukqweotozm6rcvt.htm](book-of-the-dead-bestiary-items/xukqweotozm6rcvt.htm)|Claw|auto-trad|
|[xw3lqxe7mwht4qcu.htm](book-of-the-dead-bestiary-items/xw3lqxe7mwht4qcu.htm)|Claw|auto-trad|
|[xw3tnwsyuflmy8kr.htm](book-of-the-dead-bestiary-items/xw3tnwsyuflmy8kr.htm)|Stored Items|auto-trad|
|[XxTC7GYQhX1vxMLs.htm](book-of-the-dead-bestiary-items/XxTC7GYQhX1vxMLs.htm)|At-Will Spells|auto-trad|
|[XyO3dvZ3nK3xea9x.htm](book-of-the-dead-bestiary-items/XyO3dvZ3nK3xea9x.htm)|Breathsense  (Precise) 60 feet|auto-trad|
|[XyqShEf9KyXNmA9C.htm](book-of-the-dead-bestiary-items/XyqShEf9KyXNmA9C.htm)|Rejuvenation|auto-trad|
|[y2p2f22o6cstzezq.htm](book-of-the-dead-bestiary-items/y2p2f22o6cstzezq.htm)|Wail|auto-trad|
|[Y4U4NccPky46RHKa.htm](book-of-the-dead-bestiary-items/Y4U4NccPky46RHKa.htm)|Darkvision|auto-trad|
|[Y5HEEWSpdfvhVsRo.htm](book-of-the-dead-bestiary-items/Y5HEEWSpdfvhVsRo.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[y75gjuac91xio97i.htm](book-of-the-dead-bestiary-items/y75gjuac91xio97i.htm)|Shortsword|auto-trad|
|[y7o3rimaf8qdtr7r.htm](book-of-the-dead-bestiary-items/y7o3rimaf8qdtr7r.htm)|Mystery Ingredients|auto-trad|
|[yarojxhc6r3jba98.htm](book-of-the-dead-bestiary-items/yarojxhc6r3jba98.htm)|Pallid Plague|auto-trad|
|[ydi6eab8julbghyg.htm](book-of-the-dead-bestiary-items/ydi6eab8julbghyg.htm)|Heretic's Smite|auto-trad|
|[YG1rDVP0SPT5NVZR.htm](book-of-the-dead-bestiary-items/YG1rDVP0SPT5NVZR.htm)|Negative Healing|auto-trad|
|[YgBTeOvZE7Aq4UvH.htm](book-of-the-dead-bestiary-items/YgBTeOvZE7Aq4UvH.htm)|Shambling Trample|auto-trad|
|[yl83ga21pc0ii43n.htm](book-of-the-dead-bestiary-items/yl83ga21pc0ii43n.htm)|Fist|auto-trad|
|[yM9uk6VXyMHmaP4c.htm](book-of-the-dead-bestiary-items/yM9uk6VXyMHmaP4c.htm)|Seep Blood|auto-trad|
|[yn0676ooolga6aov.htm](book-of-the-dead-bestiary-items/yn0676ooolga6aov.htm)|Claw|auto-trad|
|[yof80btzc6ifgcl4.htm](book-of-the-dead-bestiary-items/yof80btzc6ifgcl4.htm)|Battle Axe|auto-trad|
|[ypbtlxics2321ox1.htm](book-of-the-dead-bestiary-items/ypbtlxics2321ox1.htm)|Regenerating Bond|auto-trad|
|[yqze7ouayizxybn1.htm](book-of-the-dead-bestiary-items/yqze7ouayizxybn1.htm)|Aura of Charm|auto-trad|
|[yRj5K09fzH4SzWgC.htm](book-of-the-dead-bestiary-items/yRj5K09fzH4SzWgC.htm)|Darkvision|auto-trad|
|[yrk0vsss2csfbcxn.htm](book-of-the-dead-bestiary-items/yrk0vsss2csfbcxn.htm)|Self-Loathing|auto-trad|
|[Yv49fZ7FOxFBIwPP.htm](book-of-the-dead-bestiary-items/Yv49fZ7FOxFBIwPP.htm)|Divine Innate Spells|auto-trad|
|[yWkcNDsv41WwuTaJ.htm](book-of-the-dead-bestiary-items/yWkcNDsv41WwuTaJ.htm)|Primal Innate Spells|auto-trad|
|[YWKIM8D6A5hTxCGj.htm](book-of-the-dead-bestiary-items/YWKIM8D6A5hTxCGj.htm)|Darkvision|auto-trad|
|[yXCq63vokzp9doFR.htm](book-of-the-dead-bestiary-items/yXCq63vokzp9doFR.htm)|Negative Healing|auto-trad|
|[YXtsCv8FMKUxjjDY.htm](book-of-the-dead-bestiary-items/YXtsCv8FMKUxjjDY.htm)|Telepathy 100 feet|auto-trad|
|[YykK3Rlmno0gXfji.htm](book-of-the-dead-bestiary-items/YykK3Rlmno0gXfji.htm)|Darkvision|auto-trad|
|[yZSthxhBNrgEZuJQ.htm](book-of-the-dead-bestiary-items/yZSthxhBNrgEZuJQ.htm)|Grab|auto-trad|
|[z0m4exlsxdjib6e2.htm](book-of-the-dead-bestiary-items/z0m4exlsxdjib6e2.htm)|Shamble Forth!|auto-trad|
|[z3ckvu9q74atbpee.htm](book-of-the-dead-bestiary-items/z3ckvu9q74atbpee.htm)|Feed on the Living|auto-trad|
|[Z47yWqdpdND95LJp.htm](book-of-the-dead-bestiary-items/Z47yWqdpdND95LJp.htm)|Regeneration 10 (Deactivated by Cold)|auto-trad|
|[z4v0g409xiuvao4u.htm](book-of-the-dead-bestiary-items/z4v0g409xiuvao4u.htm)|Baleful Gaze|auto-trad|
|[z7svy61gizn3hcsn.htm](book-of-the-dead-bestiary-items/z7svy61gizn3hcsn.htm)|Jaws|auto-trad|
|[Zb4v92oZHr85fHnO.htm](book-of-the-dead-bestiary-items/Zb4v92oZHr85fHnO.htm)|Darkvision|auto-trad|
|[zboaidBufdR4dpax.htm](book-of-the-dead-bestiary-items/zboaidBufdR4dpax.htm)|Whispering Scythe|auto-trad|
|[zdy9kx6yjmJEYk7V.htm](book-of-the-dead-bestiary-items/zdy9kx6yjmJEYk7V.htm)|Arcane Prepared Spells|auto-trad|
|[zFCU1ww4cUWHDUWa.htm](book-of-the-dead-bestiary-items/zFCU1ww4cUWHDUWa.htm)|Constant Spells|auto-trad|
|[zg9njsvehofdrf4r.htm](book-of-the-dead-bestiary-items/zg9njsvehofdrf4r.htm)|Claw|auto-trad|
|[zhuzs5628vicodv5.htm](book-of-the-dead-bestiary-items/zhuzs5628vicodv5.htm)|Set Defense|auto-trad|
|[ZIt2oLTwffPTkqT4.htm](book-of-the-dead-bestiary-items/ZIt2oLTwffPTkqT4.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[ZmcRFvC9pG00P8gI.htm](book-of-the-dead-bestiary-items/ZmcRFvC9pG00P8gI.htm)|Greater Darkvision|auto-trad|
|[ZMfdgAIrZqi3yhIl.htm](book-of-the-dead-bestiary-items/ZMfdgAIrZqi3yhIl.htm)|Darkvision|auto-trad|
|[zmsoavgcearhti47.htm](book-of-the-dead-bestiary-items/zmsoavgcearhti47.htm)|Slow|auto-trad|
|[znu8ybij44epq6vh.htm](book-of-the-dead-bestiary-items/znu8ybij44epq6vh.htm)|Exemplar of Violence|auto-trad|
|[ZoUf0bRzsx7T7fTw.htm](book-of-the-dead-bestiary-items/ZoUf0bRzsx7T7fTw.htm)|Negative Healing|auto-trad|
|[zp0eidljzesefa24.htm](book-of-the-dead-bestiary-items/zp0eidljzesefa24.htm)|Repartee Riposte|auto-trad|
|[zpLChqdAZDg9x9Lg.htm](book-of-the-dead-bestiary-items/zpLChqdAZDg9x9Lg.htm)|Grab|auto-trad|
|[ZPZh59h8oJbpPBr5.htm](book-of-the-dead-bestiary-items/ZPZh59h8oJbpPBr5.htm)|Darkvision|auto-trad|
|[zqe60ky12wasdnz6.htm](book-of-the-dead-bestiary-items/zqe60ky12wasdnz6.htm)|Drowning Grasp|auto-trad|
|[ZRadUe9v3qyF3o2k.htm](book-of-the-dead-bestiary-items/ZRadUe9v3qyF3o2k.htm)|Troop Defenses|auto-trad|
|[zrycs1fp75o2l8h0.htm](book-of-the-dead-bestiary-items/zrycs1fp75o2l8h0.htm)|Bite|auto-trad|
|[zu5kwhnrxafjmru7.htm](book-of-the-dead-bestiary-items/zu5kwhnrxafjmru7.htm)|Axe Vulnerability|auto-trad|
|[ZuLQYlg9AglGAJp6.htm](book-of-the-dead-bestiary-items/ZuLQYlg9AglGAJp6.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[zwnm8cclbjl2zo8c.htm](book-of-the-dead-bestiary-items/zwnm8cclbjl2zo8c.htm)|Heretic's Smite|auto-trad|
|[zy1MsGurtjxz9LuB.htm](book-of-the-dead-bestiary-items/zy1MsGurtjxz9LuB.htm)|Warped Fulu|auto-trad|
|[Zyy2H5wK3HtqNuuy.htm](book-of-the-dead-bestiary-items/Zyy2H5wK3HtqNuuy.htm)|Negative Healing|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[6iygpHZZveJtpY3U.htm](book-of-the-dead-bestiary-items/6iygpHZZveJtpY3U.htm)|Feast|Festín|modificada|
|[eyfrpq3bvxnh075k.htm](book-of-the-dead-bestiary-items/eyfrpq3bvxnh075k.htm)|Silent Aura|Aura silenciosa|modificada|
|[n7PNu3mHe1gYDzMO.htm](book-of-the-dead-bestiary-items/n7PNu3mHe1gYDzMO.htm)|Fast Healing 10|Curación rápida|modificada|
|[obhnbsaze5pkt16e.htm](book-of-the-dead-bestiary-items/obhnbsaze5pkt16e.htm)|Osseous Defense|Defensa Ósea|modificada|
|[sDG67LzJ4UKhFyfv.htm](book-of-the-dead-bestiary-items/sDG67LzJ4UKhFyfv.htm)|Carve in Flesh|Tallar en Carne|modificada|
|[t4dd2bxdturlg45u.htm](book-of-the-dead-bestiary-items/t4dd2bxdturlg45u.htm)|Flaying Flurry|Ráfaga Desolladora|modificada|
|[tvvsxwr3fxjxf5ho.htm](book-of-the-dead-bestiary-items/tvvsxwr3fxjxf5ho.htm)|Broken Barb|Broken Barb|modificada|
|[w8nbobeosq4s3a35.htm](book-of-the-dead-bestiary-items/w8nbobeosq4s3a35.htm)|Stance of Death|Posición de Muerte|modificada|
|[wv50pg4p8i3xb12d.htm](book-of-the-dead-bestiary-items/wv50pg4p8i3xb12d.htm)|Putrid Assault|Putrid Assault|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
