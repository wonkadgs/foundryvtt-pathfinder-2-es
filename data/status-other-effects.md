# Estado de la traducción (other-effects)

 * **auto-trad**: 33


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0r8MznJe5UugViee.htm](other-effects/0r8MznJe5UugViee.htm)|Effect: -15-foot status penalty to your land Speed|auto-trad|
|[5gX3PZztMecmWxX9.htm](other-effects/5gX3PZztMecmWxX9.htm)|Effect: -2 circumstance penalty to attack rolls|auto-trad|
|[9c93NfZpENofiGUp.htm](other-effects/9c93NfZpENofiGUp.htm)|Effect: Mounted|auto-trad|
|[AHMUpMbaVkZ5A1KX.htm](other-effects/AHMUpMbaVkZ5A1KX.htm)|Effect: Aid|auto-trad|
|[BjkhK1nFUw3vNKg2.htm](other-effects/BjkhK1nFUw3vNKg2.htm)|Effect: -2 circumstance penalty to spell attack rolls and spell DC's|auto-trad|
|[c6cwOc4Zri6QiqsR.htm](other-effects/c6cwOc4Zri6QiqsR.htm)|Effect: -2 circumstance penalty to checks and saving throws until healed|auto-trad|
|[Ck0MQP9og75AWix7.htm](other-effects/Ck0MQP9og75AWix7.htm)|Effect: -2 circumstance penalty to attack rolls until healed|auto-trad|
|[EMqGwUi3VMhCjTlF.htm](other-effects/EMqGwUi3VMhCjTlF.htm)|Effect: Scouting|auto-trad|
|[fqk0ESqJACXqz21k.htm](other-effects/fqk0ESqJACXqz21k.htm)|Effect: -1 circumstance penalty to attack rolls until you score a critical hit|auto-trad|
|[HLBhINtyRcHk6d7h.htm](other-effects/HLBhINtyRcHk6d7h.htm)|Effect: +2 circumstance bonus to attack rolls|auto-trad|
|[HTwG0F0LDu5ihBUm.htm](other-effects/HTwG0F0LDu5ihBUm.htm)|Effect: -5-foot status penalty to your land Speed|auto-trad|
|[i5WccD9u62QPRZI8.htm](other-effects/i5WccD9u62QPRZI8.htm)|Effect: -2 circumstance penalty to attack rolls made with this attack until healed|auto-trad|
|[I9lfZUiCwMiGogVi.htm](other-effects/I9lfZUiCwMiGogVi.htm)|Effect: Cover|auto-trad|
|[IUvO6CatanKAgmNr.htm](other-effects/IUvO6CatanKAgmNr.htm)|Effect: +1 circumstance bonus to attack rolls for 3 rounds|auto-trad|
|[la8rWwUtReElgTS6.htm](other-effects/la8rWwUtReElgTS6.htm)|Effect: Scouting (Incredible Scout)|auto-trad|
|[lTwF7aCtQ6napqYu.htm](other-effects/lTwF7aCtQ6napqYu.htm)|Effect: +2 status bonus to attack rolls|auto-trad|
|[NU5wAbJLZO6T86eB.htm](other-effects/NU5wAbJLZO6T86eB.htm)|Effect: -10-foot circumstance penalty to your land Speed|auto-trad|
|[PaS2kwv4vueI4PC7.htm](other-effects/PaS2kwv4vueI4PC7.htm)|Effect: +2 status bonus to saving throws against spells for 1 min|auto-trad|
|[pbKvPM4cnmGTkDiZ.htm](other-effects/pbKvPM4cnmGTkDiZ.htm)|Effect: +4 circumstance bonus to AC against your ranged attacks|auto-trad|
|[QGTMOcJ1Dq5bk0GG.htm](other-effects/QGTMOcJ1Dq5bk0GG.htm)|Effect: +2 circumstance bonus to AC|auto-trad|
|[RcMZr0wSjrir1AnS.htm](other-effects/RcMZr0wSjrir1AnS.htm)|Effect: -5-foot circumstance penalty to your land Speed|auto-trad|
|[ScZGg91ri19H4IIc.htm](other-effects/ScZGg91ri19H4IIc.htm)|Effect: -10-foot circumstance penalty to all Speeds|auto-trad|
|[sq8lhVfn0pl5WJ4t.htm](other-effects/sq8lhVfn0pl5WJ4t.htm)|Effect: +2 status bonus to AC and all saving throws|auto-trad|
|[TPbr1kErAAJKBi3V.htm](other-effects/TPbr1kErAAJKBi3V.htm)|Effect: Aquatic Combat|auto-trad|
|[uwYGEeqvt8pwjhXa.htm](other-effects/uwYGEeqvt8pwjhXa.htm)|Effect: Dazzled until end of your next turn|auto-trad|
|[UZabiyynJWuUblMk.htm](other-effects/UZabiyynJWuUblMk.htm)|Effect: -2 circumstance penalty to attack rolls with this weapon|auto-trad|
|[VCSpuc3Tf3XWMkd3.htm](other-effects/VCSpuc3Tf3XWMkd3.htm)|Effect: Follow The Expert|auto-trad|
|[W2OF7VeLHqc7p3DO.htm](other-effects/W2OF7VeLHqc7p3DO.htm)|Effect: Deafened until end of your next turn|auto-trad|
|[wHWWHkjDXmJl4Ia6.htm](other-effects/wHWWHkjDXmJl4Ia6.htm)|Effect: Adverse Subsist Situation|auto-trad|
|[WqgtMWgM9nUjwQiI.htm](other-effects/WqgtMWgM9nUjwQiI.htm)|Effect: Resistance 5 to all damage|auto-trad|
|[wVGSlZEGxsg1s8AZ.htm](other-effects/wVGSlZEGxsg1s8AZ.htm)|Effect: -2 circumstance penalty to ranged attacks|auto-trad|
|[y1GwyXv7iOf8DhBg.htm](other-effects/y1GwyXv7iOf8DhBg.htm)|Effect: Flat-Footed until end of your next turn|auto-trad|
|[zDV1wo2ytNTbyTB0.htm](other-effects/zDV1wo2ytNTbyTB0.htm)|Effect: -10-foot status penalty to your land Speed|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
