# Estado de la traducción (conditions)

 * **auto-trad**: 42


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[1wQY3JYyhMYeeV2G.htm](conditions/1wQY3JYyhMYeeV2G.htm)|Observed|auto-trad|
|[3uh1r86TzbQvosxv.htm](conditions/3uh1r86TzbQvosxv.htm)|Doomed|auto-trad|
|[4D2KBtexWXa6oUMR.htm](conditions/4D2KBtexWXa6oUMR.htm)|Drained|auto-trad|
|[6dNUvdb1dhToNDj3.htm](conditions/6dNUvdb1dhToNDj3.htm)|Broken|auto-trad|
|[6uEgoh53GbXuHpTF.htm](conditions/6uEgoh53GbXuHpTF.htm)|Paralyzed|auto-trad|
|[9evPzg9E6muFcoSk.htm](conditions/9evPzg9E6muFcoSk.htm)|Unnoticed|auto-trad|
|[9PR9y0bi4JPKnHPR.htm](conditions/9PR9y0bi4JPKnHPR.htm)|Deafened|auto-trad|
|[9qGBRpbX9NEwtAAr.htm](conditions/9qGBRpbX9NEwtAAr.htm)|Controlled|auto-trad|
|[AdPVz7rbaVSRxHFg.htm](conditions/AdPVz7rbaVSRxHFg.htm)|Fascinated|auto-trad|
|[AJh5ex99aV6VTggg.htm](conditions/AJh5ex99aV6VTggg.htm)|Flat-Footed|auto-trad|
|[D5mg6Tc7Jzrj6ro7.htm](conditions/D5mg6Tc7Jzrj6ro7.htm)|Encumbered|auto-trad|
|[dfCMdR4wnpbYNTix.htm](conditions/dfCMdR4wnpbYNTix.htm)|Stunned|auto-trad|
|[DmAIPqOBomZ7H95W.htm](conditions/DmAIPqOBomZ7H95W.htm)|Concealed|auto-trad|
|[dTwPJuKgBQCMxixg.htm](conditions/dTwPJuKgBQCMxixg.htm)|Petrified|auto-trad|
|[e1XGnhKNSQIm5IXg.htm](conditions/e1XGnhKNSQIm5IXg.htm)|Stupefied|auto-trad|
|[eIcWbB5o3pP6OIMe.htm](conditions/eIcWbB5o3pP6OIMe.htm)|Immobilized|auto-trad|
|[fBnFDH2MTzgFijKf.htm](conditions/fBnFDH2MTzgFijKf.htm)|Unconscious|auto-trad|
|[fesd1n5eVhpCSS18.htm](conditions/fesd1n5eVhpCSS18.htm)|Sickened|auto-trad|
|[fuG8dgthlDWfWjIA.htm](conditions/fuG8dgthlDWfWjIA.htm)|Indifferent|auto-trad|
|[HL2l2VRSaQHu9lUw.htm](conditions/HL2l2VRSaQHu9lUw.htm)|Fatigued|auto-trad|
|[I1ffBVISxLr2gC4u.htm](conditions/I1ffBVISxLr2gC4u.htm)|Unfriendly|auto-trad|
|[i3OJZU2nk64Df3xm.htm](conditions/i3OJZU2nk64Df3xm.htm)|Clumsy|auto-trad|
|[iU0fEDdBp3rXpTMC.htm](conditions/iU0fEDdBp3rXpTMC.htm)|Hidden|auto-trad|
|[j91X7x0XSomq8d60.htm](conditions/j91X7x0XSomq8d60.htm)|Prone|auto-trad|
|[kWc1fhmv9LBiTuei.htm](conditions/kWc1fhmv9LBiTuei.htm)|Grabbed|auto-trad|
|[lDVqvLKA6eF3Df60.htm](conditions/lDVqvLKA6eF3Df60.htm)|Persistent Damage|auto-trad|
|[MIRkyAjyBeXivMa7.htm](conditions/MIRkyAjyBeXivMa7.htm)|Enfeebled|auto-trad|
|[nlCjDvLMf2EkV2dl.htm](conditions/nlCjDvLMf2EkV2dl.htm)|Quickened|auto-trad|
|[sDPxOjQ9kx2RZE8D.htm](conditions/sDPxOjQ9kx2RZE8D.htm)|Fleeing|auto-trad|
|[TBSHQspnbcqxsmjL.htm](conditions/TBSHQspnbcqxsmjL.htm)|Frightened|auto-trad|
|[TkIyaNPgTZFBCCuh.htm](conditions/TkIyaNPgTZFBCCuh.htm)|Dazzled|auto-trad|
|[ud7gTLwPeklzYSXG.htm](conditions/ud7gTLwPeklzYSXG.htm)|Hostile|auto-trad|
|[v44P3WUcU1j0115l.htm](conditions/v44P3WUcU1j0115l.htm)|Helpful|auto-trad|
|[v66R7FdOf11l94im.htm](conditions/v66R7FdOf11l94im.htm)|Friendly|auto-trad|
|[VcDeM8A5oI6VqhbM.htm](conditions/VcDeM8A5oI6VqhbM.htm)|Restrained|auto-trad|
|[VRSef5y1LmL2Hkjf.htm](conditions/VRSef5y1LmL2Hkjf.htm)|Undetected|auto-trad|
|[XgEqL1kFApUbl5Z2.htm](conditions/XgEqL1kFApUbl5Z2.htm)|Blinded|auto-trad|
|[xYTAsEpcJE1Ccni3.htm](conditions/xYTAsEpcJE1Ccni3.htm)|Slowed|auto-trad|
|[yblD8fOR1J8rDwEQ.htm](conditions/yblD8fOR1J8rDwEQ.htm)|Confused|auto-trad|
|[Yl48xTdMh3aeQYL2.htm](conditions/Yl48xTdMh3aeQYL2.htm)|Wounded|auto-trad|
|[yZRUzMqrMmfLu0V1.htm](conditions/yZRUzMqrMmfLu0V1.htm)|Dying|auto-trad|
|[zJxUflt9np0q4yML.htm](conditions/zJxUflt9np0q4yML.htm)|Invisible|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
