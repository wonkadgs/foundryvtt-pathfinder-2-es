# Estado de la traducción (classes)

 * **auto-trad**: 17
 * **modificada**: 3
 * **oficial**: 2


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[15Yc1r6s9CEhSTMe.htm](classes/15Yc1r6s9CEhSTMe.htm)|Sorcerer|auto-trad|
|[30qVs46dVNflgQNx.htm](classes/30qVs46dVNflgQNx.htm)|Inventor|auto-trad|
|[3gweRQ5gn7szIWAv.htm](classes/3gweRQ5gn7szIWAv.htm)|Bard|auto-trad|
|[4wrSCyX6akmyo7Wj.htm](classes/4wrSCyX6akmyo7Wj.htm)|Investigator|auto-trad|
|[7s57JDCaiYYCAdFx.htm](classes/7s57JDCaiYYCAdFx.htm)|Druid|auto-trad|
|[8zn3cD6GSmoo1LW4.htm](classes/8zn3cD6GSmoo1LW4.htm)|Fighter|auto-trad|
|[EizrWvUPMS67Pahd.htm](classes/EizrWvUPMS67Pahd.htm)|Cleric|auto-trad|
|[HQBA9Yx2s8ycvz3C.htm](classes/HQBA9Yx2s8ycvz3C.htm)|Magus|auto-trad|
|[Inq4gH3P5PYjSQbD.htm](classes/Inq4gH3P5PYjSQbD.htm)|Psychic|auto-trad|
|[LO9STvskJemPkiAI.htm](classes/LO9STvskJemPkiAI.htm)|Rogue|auto-trad|
|[RwjIZzIxzPpUglnK.htm](classes/RwjIZzIxzPpUglnK.htm)|Wizard|auto-trad|
|[x8iwnpdLbfcoZkHA.htm](classes/x8iwnpdLbfcoZkHA.htm)|Champion|auto-trad|
|[Y5GsHqzCzJlKka6x.htm](classes/Y5GsHqzCzJlKka6x.htm)|Thaumaturge|auto-trad|
|[YDRiP7uVvr9WRhOI.htm](classes/YDRiP7uVvr9WRhOI.htm)|Barbarian|auto-trad|
|[YPxpk9JbMnKjbNLc.htm](classes/YPxpk9JbMnKjbNLc.htm)|Monk|auto-trad|
|[YtOm245r8GFSFYeD.htm](classes/YtOm245r8GFSFYeD.htm)|Summoner|auto-trad|
|[Z9li154CPNmun29Q.htm](classes/Z9li154CPNmun29Q.htm)|Gunslinger|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[bYDXk9HUMKOuym9h.htm](classes/bYDXk9HUMKOuym9h.htm)|Witch|Bruja|modificada|
|[pWHx4SXcft9O2udP.htm](classes/pWHx4SXcft9O2udP.htm)|Oracle|Oráculo|modificada|
|[uJ5aCzlw34GGdWjp.htm](classes/uJ5aCzlw34GGdWjp.htm)|Swashbuckler|Espadachín|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[XwfcJuskrhI9GIjX.htm](classes/XwfcJuskrhI9GIjX.htm)|Alchemist|Alquimista|oficial|
|[Yix76sfxrIlltSTJ.htm](classes/Yix76sfxrIlltSTJ.htm)|Ranger|Explorador|oficial|
