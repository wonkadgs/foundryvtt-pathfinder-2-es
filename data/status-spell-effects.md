# Estado de la traducción (spell-effects)

 * **auto-trad**: 363
 * **ninguna**: 5
 * **modificada**: 7


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[4ag0OHKfjROmR4Pm.htm](spell-effects/4ag0OHKfjROmR4Pm.htm)|Spell Effect: Anticipate Peril|
|[5p3bKvWsJgo83FS1.htm](spell-effects/5p3bKvWsJgo83FS1.htm)|Aura: Protective Ward|
|[EScdpppYsf9KhG4D.htm](spell-effects/EScdpppYsf9KhG4D.htm)|Spell Effect: Ghostly Weapon|
|[F1APSdrw5uv672hf.htm](spell-effects/F1APSdrw5uv672hf.htm)|Spell Effect: Battlefield Persistence|
|[Y6aNYnGVXdAMvL7Y.htm](spell-effects/Y6aNYnGVXdAMvL7Y.htm)|Spell Effect: Thermal Stasis|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[06zdFoxzuTpPPGyJ.htm](spell-effects/06zdFoxzuTpPPGyJ.htm)|Spell Effect: Rejuvenating Flames|auto-trad|
|[0bfqYkNaWsdTmtrc.htm](spell-effects/0bfqYkNaWsdTmtrc.htm)|Spell Effect: Juvenile Companion|auto-trad|
|[0Cyf07wboRp4CmcQ.htm](spell-effects/0Cyf07wboRp4CmcQ.htm)|Spell Effect: Dinosaur Form (Ankylosaurus)|auto-trad|
|[0gv9D5RlrF5cKA3I.htm](spell-effects/0gv9D5RlrF5cKA3I.htm)|Spell Effect: Adapt Self (Darkvision)|auto-trad|
|[0o984LjzIFXxeXIF.htm](spell-effects/0o984LjzIFXxeXIF.htm)|Spell Effect: Evolution Surge (Amphibious)|auto-trad|
|[0OC945wcZ4H4akLz.htm](spell-effects/0OC945wcZ4H4akLz.htm)|Spell Effect: Summoner's Visage|auto-trad|
|[0q2716S34XL1y9Hh.htm](spell-effects/0q2716S34XL1y9Hh.htm)|Spell Effect: Rapid Adaptation (Underground)|auto-trad|
|[0QVufU5o3xIxiHmP.htm](spell-effects/0QVufU5o3xIxiHmP.htm)|Spell Effect: Aerial Form (Bird)|auto-trad|
|[0R42NyuEZMVALjQs.htm](spell-effects/0R42NyuEZMVALjQs.htm)|Spell Effect: Traveler's Transit (Swim)|auto-trad|
|[0s6YaL3IjqECmjab.htm](spell-effects/0s6YaL3IjqECmjab.htm)|Spell Effect: Roar of the Wyrm|auto-trad|
|[0W87OkYi3qCwNGSj.htm](spell-effects/0W87OkYi3qCwNGSj.htm)|Spell Effect: Daemon Form (Piscodaemon)|auto-trad|
|[0yy4t4UY1HqrEo70.htm](spell-effects/0yy4t4UY1HqrEo70.htm)|Spell Effect: Devil Form (Barbazu)|auto-trad|
|[14AFzcwkN019dzcl.htm](spell-effects/14AFzcwkN019dzcl.htm)|Spell Effect: Shifting Form (Claws)|auto-trad|
|[14m4s0FeRSqRlHwL.htm](spell-effects/14m4s0FeRSqRlHwL.htm)|Spell Effect: Arcane Countermeasure|auto-trad|
|[16QrxIYal7PJFL2W.htm](spell-effects/16QrxIYal7PJFL2W.htm)|Spell Effect: Euphoric Renewal|auto-trad|
|[1gTnFmfLUQnCo3TK.htm](spell-effects/1gTnFmfLUQnCo3TK.htm)|Spell Effect: Dread Ambience (Success)|auto-trad|
|[1kelGCsoXyGRqMd9.htm](spell-effects/1kelGCsoXyGRqMd9.htm)|Spell Effect: Diabolic Edict|auto-trad|
|[1n84AqLtsdT8I64W.htm](spell-effects/1n84AqLtsdT8I64W.htm)|Spell Effect: Daemon Form (Ceustodaemon)|auto-trad|
|[1RsScTvNdGD9zGWe.htm](spell-effects/1RsScTvNdGD9zGWe.htm)|Spell Effect: Fire Shield|auto-trad|
|[1VuHjj32wge2gPOr.htm](spell-effects/1VuHjj32wge2gPOr.htm)|Spell Effect: Animal Feature (Wings)|auto-trad|
|[20egTKICPMhibqgn.htm](spell-effects/20egTKICPMhibqgn.htm)|Spell Effect: Demon Form (Vrock)|auto-trad|
|[28NvrpZmELvyrHUt.htm](spell-effects/28NvrpZmELvyrHUt.htm)|Spell Effect: Variable Gravity (High Gravity)|auto-trad|
|[2KQSsrzUqAxSXOdd.htm](spell-effects/2KQSsrzUqAxSXOdd.htm)|Spell Effect: Dancing Shield|auto-trad|
|[2sMXAGZfdqiy10kk.htm](spell-effects/2sMXAGZfdqiy10kk.htm)|Spell Effect: Clinging Ice|auto-trad|
|[2Ss5VblfZNHg1HjN.htm](spell-effects/2Ss5VblfZNHg1HjN.htm)|Spell Effect: Cosmic Form (Sun)|auto-trad|
|[2SWUzp4JuNK5EX0J.htm](spell-effects/2SWUzp4JuNK5EX0J.htm)|Spell Effect: Adapt Self (Swim)|auto-trad|
|[2wfrhRLmmgPSKbAZ.htm](spell-effects/2wfrhRLmmgPSKbAZ.htm)|Spell Effect: Animal Feature (Claws)|auto-trad|
|[3HEiYVhqypfc4IsP.htm](spell-effects/3HEiYVhqypfc4IsP.htm)|Spell Effect: Safeguard Secret|auto-trad|
|[3Ktyd5F9lOPo4myk.htm](spell-effects/3Ktyd5F9lOPo4myk.htm)|Spell Effect: Illusory Disguise|auto-trad|
|[3LyOkV25p7wA181H.htm](spell-effects/3LyOkV25p7wA181H.htm)|Effect: Guidance Immunity|auto-trad|
|[3qHKBDF7lrHw8jFK.htm](spell-effects/3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|auto-trad|
|[3vWfew0TIrcGRjLZ.htm](spell-effects/3vWfew0TIrcGRjLZ.htm)|Spell Effect: Angel Form (Monadic Deva)|auto-trad|
|[3zdBGENpmaze1bpq.htm](spell-effects/3zdBGENpmaze1bpq.htm)|Spell Effect: Ooze Form (Gelatinous Cube)|auto-trad|
|[41WThj17MZBXTO2X.htm](spell-effects/41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|auto-trad|
|[46vCC77mBNBWtmx3.htm](spell-effects/46vCC77mBNBWtmx3.htm)|Spell Effect: Litany of Righteousness|auto-trad|
|[4FD4vJqVpuuJjk9Q.htm](spell-effects/4FD4vJqVpuuJjk9Q.htm)|Spell Effect: Mind Swap (Critical Success)|auto-trad|
|[4iakL7fDcZ8RT6Tu.htm](spell-effects/4iakL7fDcZ8RT6Tu.htm)|Spell Effect: Face in the Crowd|auto-trad|
|[4ktNx3cVz5GkcGJa.htm](spell-effects/4ktNx3cVz5GkcGJa.htm)|Spell Effect: Untwisting Iron Augmentation|auto-trad|
|[4Lo2qb5PmavSsLNk.htm](spell-effects/4Lo2qb5PmavSsLNk.htm)|Spell Effect: Energy Aegis|auto-trad|
|[4Lt69zSeYElEfDrZ.htm](spell-effects/4Lt69zSeYElEfDrZ.htm)|Spell Effect: Dread Ambience (Critical Failure)|auto-trad|
|[4VOZP2ArmS12nvz8.htm](spell-effects/4VOZP2ArmS12nvz8.htm)|Spell Effect: Draw Ire (Critical Failure)|auto-trad|
|[542Keo6txtq7uvqe.htm](spell-effects/542Keo6txtq7uvqe.htm)|Spell Effect: Dinosaur Form (Tyrannosaurus)|auto-trad|
|[57lnrCzGUcNUBP2O.htm](spell-effects/57lnrCzGUcNUBP2O.htm)|Spell Effect: Athletic Rush|auto-trad|
|[5MI2c9IgxfSeGZQo.htm](spell-effects/5MI2c9IgxfSeGZQo.htm)|Spell Effect: Wind Jump|auto-trad|
|[5R3ewWLFkgqTvZsc.htm](spell-effects/5R3ewWLFkgqTvZsc.htm)|Spell Effect: Elemental Gift (Fire)|auto-trad|
|[5xCheSMgtQhQZm00.htm](spell-effects/5xCheSMgtQhQZm00.htm)|Spell Effect: Garden of Death (Critical Success)|auto-trad|
|[5yCL7InrJDHpaQjz.htm](spell-effects/5yCL7InrJDHpaQjz.htm)|Spell Effect: Ant Haul|auto-trad|
|[6BjslHgY01cNbKp5.htm](spell-effects/6BjslHgY01cNbKp5.htm)|Spell Effect: Armor of Bones|auto-trad|
|[6embuvXCpS3YOD5u.htm](spell-effects/6embuvXCpS3YOD5u.htm)|Spell Effect: Resilient Touch|auto-trad|
|[6Ev2ytJZfr2t33iy.htm](spell-effects/6Ev2ytJZfr2t33iy.htm)|Spell Effect: Evolution Surge (Sight)|auto-trad|
|[6GAztnHuQSwAp1k1.htm](spell-effects/6GAztnHuQSwAp1k1.htm)|Spell Effect: Adaptive Ablation|auto-trad|
|[6IvTWcispcDaw88N.htm](spell-effects/6IvTWcispcDaw88N.htm)|Spell Effect: Insect Form (Ant)|auto-trad|
|[6qvLnIkWAoGvTIWy.htm](spell-effects/6qvLnIkWAoGvTIWy.htm)|Spell Effect: Community Repair (Critical Failure)|auto-trad|
|[6SG8wVmppv4oXZtx.htm](spell-effects/6SG8wVmppv4oXZtx.htm)|Spell Effect: Mantle of the Magma Heart (Fiery Grasp)|auto-trad|
|[70qdCBokXBvKIUIQ.htm](spell-effects/70qdCBokXBvKIUIQ.htm)|Spell Effect: Vision of Weakness|auto-trad|
|[7tfF8ifVvOKNud8t.htm](spell-effects/7tfF8ifVvOKNud8t.htm)|Spell Effect: Ooze Form (Gray Ooze)|auto-trad|
|[7tYv9lY3ksSUny2h.htm](spell-effects/7tYv9lY3ksSUny2h.htm)|Spell Effect: Gray Shadow|auto-trad|
|[7vIUF5zbvHzVcJA0.htm](spell-effects/7vIUF5zbvHzVcJA0.htm)|Spell Effect: Longstrider (8 hours)|auto-trad|
|[7zJPd2BsFl82qFRV.htm](spell-effects/7zJPd2BsFl82qFRV.htm)|Spell Effect: Warding Aggression (+3)|auto-trad|
|[7zy4W2RXQiMEr6cp.htm](spell-effects/7zy4W2RXQiMEr6cp.htm)|Spell Effect: Dragon Claws|auto-trad|
|[81TfqzTfIqkQA4Dy.htm](spell-effects/81TfqzTfIqkQA4Dy.htm)|Spell Effect: Thundering Dominance|auto-trad|
|[8adLKKzJy49USYJt.htm](spell-effects/8adLKKzJy49USYJt.htm)|Spell Effect: Song of Strength|auto-trad|
|[8aNZhlkzRTRKlKag.htm](spell-effects/8aNZhlkzRTRKlKag.htm)|Spell Effect: Dragon Form (Gold)|auto-trad|
|[8ecGfjmxnBY3WWao.htm](spell-effects/8ecGfjmxnBY3WWao.htm)|Spell Effect: Thicket of Knives|auto-trad|
|[8eWLR0WCf5258z8X.htm](spell-effects/8eWLR0WCf5258z8X.htm)|Spell Effect: Elemental Form (Earth)|auto-trad|
|[8gqb5FMTaArsKdWB.htm](spell-effects/8gqb5FMTaArsKdWB.htm)|Spell Effect: Prismatic Armor|auto-trad|
|[8GUkKvCeI0xljCOk.htm](spell-effects/8GUkKvCeI0xljCOk.htm)|Spell Effect: Stormwind Flight|auto-trad|
|[8olfnTmWh0GGPDqX.htm](spell-effects/8olfnTmWh0GGPDqX.htm)|Spell Effect: Ki Strike|auto-trad|
|[8wCVSzWYcURWewbd.htm](spell-effects/8wCVSzWYcURWewbd.htm)|Spell Effect: Bestial Curse (Failure)|auto-trad|
|[8XaSpienzVXLmcfp.htm](spell-effects/8XaSpienzVXLmcfp.htm)|Spell Effect: Inspire Heroics (Strength, +3)|auto-trad|
|[9yzlmYUdvdQshTDF.htm](spell-effects/9yzlmYUdvdQshTDF.htm)|Spell Effect: Bullhorn|auto-trad|
|[9ZIP6gWSp9OTEu8i.htm](spell-effects/9ZIP6gWSp9OTEu8i.htm)|Spell Effect: Pocket Library|auto-trad|
|[a3uZckqOY9zQWzZ2.htm](spell-effects/a3uZckqOY9zQWzZ2.htm)|Spell Effect: Read the Air|auto-trad|
|[A48jNUOAmCljx8Ru.htm](spell-effects/A48jNUOAmCljx8Ru.htm)|Spell Effect: Shifting Form (Darkvision)|auto-trad|
|[a5rWrWwuevTzs9Io.htm](spell-effects/a5rWrWwuevTzs9Io.htm)|Spell Effect: Wild Shape|auto-trad|
|[A61eVVVyUuaUl3tz.htm](spell-effects/A61eVVVyUuaUl3tz.htm)|Spell Effect: Celestial Brand|auto-trad|
|[aDOL3OAEWf3ka9oT.htm](spell-effects/aDOL3OAEWf3ka9oT.htm)|Spell Effect: Blood Ward|auto-trad|
|[afJCG4vC5WF5h5IB.htm](spell-effects/afJCG4vC5WF5h5IB.htm)|Spell Effect: Clawsong (Damage Increase D8)|auto-trad|
|[AJkRUIdYLnt4QOOg.htm](spell-effects/AJkRUIdYLnt4QOOg.htm)|Spell Effect: Tempt Fate|auto-trad|
|[alyNtkHLNnt98Ewz.htm](spell-effects/alyNtkHLNnt98Ewz.htm)|Spell Effect: Accelerating Touch|auto-trad|
|[AM49w68oKykc2fHI.htm](spell-effects/AM49w68oKykc2fHI.htm)|Spell Effect: Rapid Adaptation (Plains)|auto-trad|
|[amTa9jSml9ioKduN.htm](spell-effects/amTa9jSml9ioKduN.htm)|Spell Effect: Insect Form (Beetle)|auto-trad|
|[an4yZ6dyIDOFa1wa.htm](spell-effects/an4yZ6dyIDOFa1wa.htm)|Spell Effect: Soothing Words|auto-trad|
|[AnawxScxqUiRuGTm.htm](spell-effects/AnawxScxqUiRuGTm.htm)|Spell Effect: Divine Vessel 9th level (Evil)|auto-trad|
|[AnCRD7kDcG0DDGKn.htm](spell-effects/AnCRD7kDcG0DDGKn.htm)|Spell Effect: Fungal Infestation (Failure)|auto-trad|
|[b5OyBdc0bolgWZZT.htm](spell-effects/b5OyBdc0bolgWZZT.htm)|Spell Effect: Tempest Form (Air)|auto-trad|
|[b8bfWIICHOsGVzjp.htm](spell-effects/b8bfWIICHOsGVzjp.htm)|Spell Effect: Monstrosity Form (Phoenix)|auto-trad|
|[Bc2Bwuan3716eAyY.htm](spell-effects/Bc2Bwuan3716eAyY.htm)|Spell Effect: Font of Serenity|auto-trad|
|[Bd86oAvK3RLN076H.htm](spell-effects/Bd86oAvK3RLN076H.htm)|Spell Effect: Angel Form (Movanic Deva)|auto-trad|
|[BDMEqBsumguTrMXa.htm](spell-effects/BDMEqBsumguTrMXa.htm)|Spell Effect: Devil Form (Erinys)|auto-trad|
|[beReeFroAx24hj83.htm](spell-effects/beReeFroAx24hj83.htm)|Spell Effect: Inspire Courage|auto-trad|
|[BfaFe1cI9IkpvmmY.htm](spell-effects/BfaFe1cI9IkpvmmY.htm)|Spell Effect: Countless Eyes|auto-trad|
|[BKam63zT98iWMJH7.htm](spell-effects/BKam63zT98iWMJH7.htm)|Spell Effect: Inspire Heroics (Defense, +3)|auto-trad|
|[blBXnWb1Y8q8YYMh.htm](spell-effects/blBXnWb1Y8q8YYMh.htm)|Spell Effect: Primal Summons (Fire)|auto-trad|
|[BsGZdgiEElNlgZVv.htm](spell-effects/BsGZdgiEElNlgZVv.htm)|Spell Effect: Draw Ire (Failure)|auto-trad|
|[BT1ofB6RvRocQOWO.htm](spell-effects/BT1ofB6RvRocQOWO.htm)|Spell Effect: Animal Form (Bull)|auto-trad|
|[buXx8Azr4BYWPtFg.htm](spell-effects/buXx8Azr4BYWPtFg.htm)|Spell Effect: Blood Vendetta (Failure)|auto-trad|
|[byXkHIKFwuKrZ55M.htm](spell-effects/byXkHIKFwuKrZ55M.htm)|Spell Effect: Shifting Form (Scent)|auto-trad|
|[C3RdbEQTvawqKAhw.htm](spell-effects/C3RdbEQTvawqKAhw.htm)|Spell Effect: Tempest Form (Water)|auto-trad|
|[c4cIfS2974nUJDPt.htm](spell-effects/c4cIfS2974nUJDPt.htm)|Spell Effect: Fey Form (Dryad)|auto-trad|
|[ceEA7nBGNmoR8Sjj.htm](spell-effects/ceEA7nBGNmoR8Sjj.htm)|Spell Effect: Litany of Self-Interest|auto-trad|
|[Chol7ExtoN2T36mP.htm](spell-effects/Chol7ExtoN2T36mP.htm)|Spell Effect: Inspire Heroics (Defense, +2)|auto-trad|
|[con2Hzt47JjpuUej.htm](spell-effects/con2Hzt47JjpuUej.htm)|Spell Effect: Resist Energy|auto-trad|
|[cSoL5aMy3PCzM4Yv.htm](spell-effects/cSoL5aMy3PCzM4Yv.htm)|Spell Effect: Return the Favor|auto-trad|
|[cTBYHfiXDOA09G4b.htm](spell-effects/cTBYHfiXDOA09G4b.htm)|Spell Effect: Traveler's Transit (Fly)|auto-trad|
|[CTdEsMIwVYqqkH50.htm](spell-effects/CTdEsMIwVYqqkH50.htm)|Spell Effect: Litany of Depravity|auto-trad|
|[ctMxYPGEpstvhW9C.htm](spell-effects/ctMxYPGEpstvhW9C.htm)|Spell Effect: Forbidding Ward|auto-trad|
|[cVVZXNbV0nElVOPZ.htm](spell-effects/cVVZXNbV0nElVOPZ.htm)|Spell Effect: Light|auto-trad|
|[CWC2fPmlgixoIKy5.htm](spell-effects/CWC2fPmlgixoIKy5.htm)|Spell Effect: Clawsong (Damage Increase D6)|auto-trad|
|[cwetyC5o4dRyFWJZ.htm](spell-effects/cwetyC5o4dRyFWJZ.htm)|Spell Effect: Necromancer's Generosity|auto-trad|
|[czteoX2cggQzfkK9.htm](spell-effects/czteoX2cggQzfkK9.htm)|Spell Effect: Evolution Surge (Climb)|auto-trad|
|[D0Qj5tC1hGUjzQc4.htm](spell-effects/D0Qj5tC1hGUjzQc4.htm)|Spell Effect: Elemental Motion (Water)|auto-trad|
|[DBaMtFHRPEg1JeLs.htm](spell-effects/DBaMtFHRPEg1JeLs.htm)|Spell Effect: Mind Blank|auto-trad|
|[dCQCzapIk53xmDo5.htm](spell-effects/dCQCzapIk53xmDo5.htm)|Spell Effect: Animal Feature (Cat Eyes)|auto-trad|
|[deG1dtfuQph03Kkg.htm](spell-effects/deG1dtfuQph03Kkg.htm)|Spell Effect: Shillelagh|auto-trad|
|[dEsaufFnfYihu5Ex.htm](spell-effects/dEsaufFnfYihu5Ex.htm)|Spell Effect: Discern Secrets (Sense Motive)|auto-trad|
|[DHYWmMGmKOpRSqza.htm](spell-effects/DHYWmMGmKOpRSqza.htm)|Spell Effect: Chromatic Armor|auto-trad|
|[dIftJU6Ki2QSLCOD.htm](spell-effects/dIftJU6Ki2QSLCOD.htm)|Spell Effect: Divine Vessel 9th level (Chaotic)|auto-trad|
|[DliizYpHcmBG130w.htm](spell-effects/DliizYpHcmBG130w.htm)|Spell Effect: Elemental Form (Air)|auto-trad|
|[DLwTvjjnqs2sNGuG.htm](spell-effects/DLwTvjjnqs2sNGuG.htm)|Spell Effect: Inspire Defense|auto-trad|
|[DrNpuMj14wVj4bWF.htm](spell-effects/DrNpuMj14wVj4bWF.htm)|Spell Effect: Dragon Form (Copper)|auto-trad|
|[dWbg2gACxMkSnZag.htm](spell-effects/dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Ward|auto-trad|
|[DwM5qcFp4JgKhXrY.htm](spell-effects/DwM5qcFp4JgKhXrY.htm)|Spell Effect: Fey Form (Unicorn)|auto-trad|
|[dXq7z633ve4E0nlX.htm](spell-effects/dXq7z633ve4E0nlX.htm)|Spell Effect: Regenerate|auto-trad|
|[ei9MIyZbIaP4AZmh.htm](spell-effects/ei9MIyZbIaP4AZmh.htm)|Spell Effect: Flame Wisp|auto-trad|
|[Eik8Fj8nGo2GLcbn.htm](spell-effects/Eik8Fj8nGo2GLcbn.htm)|Spell Effect: Monstrosity Form (Sea Serpent)|auto-trad|
|[EKdqKCuyWSkpXpyJ.htm](spell-effects/EKdqKCuyWSkpXpyJ.htm)|Spell Effect: Ooze Form (Black Pudding)|auto-trad|
|[eotqxEWIgaK7nMpD.htm](spell-effects/eotqxEWIgaK7nMpD.htm)|Spell Effect: Blunt the Final Blade (Critical Success)|auto-trad|
|[ETgzIIv3M2zvclAR.htm](spell-effects/ETgzIIv3M2zvclAR.htm)|Spell Effect: Dragon Form (Blue)|auto-trad|
|[EUxTav62IXTz5CxW.htm](spell-effects/EUxTav62IXTz5CxW.htm)|Spell Effect: Nature Incarnate (Kaiju)|auto-trad|
|[evK8JR3j2iWGWaug.htm](spell-effects/evK8JR3j2iWGWaug.htm)|Spell Effect: Divine Vessel (Evil)|auto-trad|
|[F10ofwC0k1ELIaV4.htm](spell-effects/F10ofwC0k1ELIaV4.htm)|Spell Effect: Impeccable Flow (Critical Failure Effect)|auto-trad|
|[F4DTpDXNu5IliyhJ.htm](spell-effects/F4DTpDXNu5IliyhJ.htm)|Spell Effect: Animal Form (Deer)|auto-trad|
|[fCIT9YgGUwIc3Z9G.htm](spell-effects/fCIT9YgGUwIc3Z9G.htm)|Spell Effect: Draw the Lightning|auto-trad|
|[FD9Ce5pqcZYstcMI.htm](spell-effects/FD9Ce5pqcZYstcMI.htm)|Spell Effect: Blessing of Defiance|auto-trad|
|[fEhCbATDNlt6c1Ug.htm](spell-effects/fEhCbATDNlt6c1Ug.htm)|Spell Effect: Extract Poison|auto-trad|
|[fGK6zJ7mWz9D5QYo.htm](spell-effects/fGK6zJ7mWz9D5QYo.htm)|Spell Effect: Rapid Adaptation (Aquatic Base Swim Speed)|auto-trad|
|[fIloZhZVH1xTnX4B.htm](spell-effects/fIloZhZVH1xTnX4B.htm)|Spell Effect: Plant Form (Shambler)|auto-trad|
|[Fjnm1l59KH5YJ7G9.htm](spell-effects/Fjnm1l59KH5YJ7G9.htm)|Spell Effect: Inspire Heroics (Strength, +2)|auto-trad|
|[fKeZDm8kpDFK5HWp.htm](spell-effects/fKeZDm8kpDFK5HWp.htm)|Spell Effect: Devil Form (Sarglagon)|auto-trad|
|[fLOQMycP5tmXgPv9.htm](spell-effects/fLOQMycP5tmXgPv9.htm)|Spell Effect: Elemental Gift (Earth)|auto-trad|
|[fpGDAz2v5PG0zUSl.htm](spell-effects/fpGDAz2v5PG0zUSl.htm)|Spell Effect: True Strike|auto-trad|
|[FR4ucNi2ceHZdrpB.htm](spell-effects/FR4ucNi2ceHZdrpB.htm)|Spell Effect: Warding Aggression (+1)|auto-trad|
|[FT5Tt2DKBRutDqbV.htm](spell-effects/FT5Tt2DKBRutDqbV.htm)|Spell Effect: Dread Ambience (Critical Success)|auto-trad|
|[fvIlSZPwojixVvyZ.htm](spell-effects/fvIlSZPwojixVvyZ.htm)|Spell Effect: Lucky Number|auto-trad|
|[fwaAe71qfnK7SiOB.htm](spell-effects/fwaAe71qfnK7SiOB.htm)|Spell Effect: Primal Summons (Air)|auto-trad|
|[g4E9l4uA62LcRBJS.htm](spell-effects/g4E9l4uA62LcRBJS.htm)|Spell Effect: Clawsong (Versatile Piercing)|auto-trad|
|[GDzn5DToE62ZOTrP.htm](spell-effects/GDzn5DToE62ZOTrP.htm)|Spell Effect: Divine Vessel (Chaotic)|auto-trad|
|[GhGoZdAZtzZTYCzj.htm](spell-effects/GhGoZdAZtzZTYCzj.htm)|Spell Effect: Animal Feature (Jaws)|auto-trad|
|[GhNVAYtoF5hK3AlD.htm](spell-effects/GhNVAYtoF5hK3AlD.htm)|Spell Effect: Touch of Corruption|auto-trad|
|[gKGErrsS1WoAyWub.htm](spell-effects/gKGErrsS1WoAyWub.htm)|Spell Effect: Aberrant Form (Gogiteth)|auto-trad|
|[GlggmEqkGVj1noOD.htm](spell-effects/GlggmEqkGVj1noOD.htm)|Spell Effect: Bottle the Storm|auto-trad|
|[GnWkI3T3LYRlm3X8.htm](spell-effects/GnWkI3T3LYRlm3X8.htm)|Spell Effect: Magic Weapon|auto-trad|
|[gQnDKDeBTtjwOWAk.htm](spell-effects/gQnDKDeBTtjwOWAk.htm)|Spell Effect: Animal Form (Bear)|auto-trad|
|[Gqy7K6FnbLtwGpud.htm](spell-effects/Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|auto-trad|
|[gX8O0ArQXbEVDUbW.htm](spell-effects/gX8O0ArQXbEVDUbW.htm)|Spell Effect: Embrace the Pit|auto-trad|
|[h0CKGrgjGNSg21BW.htm](spell-effects/h0CKGrgjGNSg21BW.htm)|Spell Effect: Boost Eidolon|auto-trad|
|[h2JzNunzO8hXiNV3.htm](spell-effects/h2JzNunzO8hXiNV3.htm)|Spell Effect: Lifelink Surge|auto-trad|
|[H6ndYYYlADWwqVQb.htm](spell-effects/H6ndYYYlADWwqVQb.htm)|Spell Effect: Dragon Form (White)|auto-trad|
|[HDKJAUXMbtxnBdgR.htm](spell-effects/HDKJAUXMbtxnBdgR.htm)|Spell Effect: Tempest Form (Mist)|auto-trad|
|[hdOb5Iu6Zd3pHoGI.htm](spell-effects/hdOb5Iu6Zd3pHoGI.htm)|Spell Effect: Discern Secrets (Recall Knowledge)|auto-trad|
|[heAj9paC8ZRh7QEj.htm](spell-effects/heAj9paC8ZRh7QEj.htm)|Spell Effect: Fey Form (Redcap)|auto-trad|
|[HEbbxKtBzsLhFead.htm](spell-effects/HEbbxKtBzsLhFead.htm)|Spell Effect: Devil Form (Osyluth)|auto-trad|
|[hkLhZsH3T6jc9S1y.htm](spell-effects/hkLhZsH3T6jc9S1y.htm)|Spell Effect: Veil of Dreams|auto-trad|
|[hnfQyf05IIa7WPBB.htm](spell-effects/hnfQyf05IIa7WPBB.htm)|Spell Effect: Demon Form (Nabasu)|auto-trad|
|[HoCUCi2jL1OLfXWR.htm](spell-effects/HoCUCi2jL1OLfXWR.htm)|Spell Effect: Unblinking Flame Aura|auto-trad|
|[HoOujAdQWCN4E6sQ.htm](spell-effects/HoOujAdQWCN4E6sQ.htm)|Spell Effect: Barkskin|auto-trad|
|[HtaDbgTIzdiTiKLX.htm](spell-effects/HtaDbgTIzdiTiKLX.htm)|Spell Effect: Triple Time|auto-trad|
|[hXtK08bTnDBSzGTJ.htm](spell-effects/hXtK08bTnDBSzGTJ.htm)|Spell Effect: Iron Gut|auto-trad|
|[hya8NfBB1GJofTXm.htm](spell-effects/hya8NfBB1GJofTXm.htm)|Spell Effect: Unblinking Flame Ignition|auto-trad|
|[I4PsUAaYSUJ8pwKC.htm](spell-effects/I4PsUAaYSUJ8pwKC.htm)|Spell Effect: Ray of Frost|auto-trad|
|[i9YITDcrq1nKjV5l.htm](spell-effects/i9YITDcrq1nKjV5l.htm)|Spell Effect: Infectious Melody|auto-trad|
|[ib58LaffEUIypuzL.htm](spell-effects/ib58LaffEUIypuzL.htm)|Spell Effect: Evolution Surge (Huge)|auto-trad|
|[iiV80Kexj6vPmzqU.htm](spell-effects/iiV80Kexj6vPmzqU.htm)|Spell Effect: Rapid Adaptation (Arctic)|auto-trad|
|[iJ7TVW5tDnZG9DG8.htm](spell-effects/iJ7TVW5tDnZG9DG8.htm)|Spell Effect: Competitive Edge|auto-trad|
|[inNfTmtWpsxeGBI9.htm](spell-effects/inNfTmtWpsxeGBI9.htm)|Spell Effect: Darkvision (24 hours)|auto-trad|
|[iqtjMVl6rGQhX2k8.htm](spell-effects/iqtjMVl6rGQhX2k8.htm)|Spell Effect: Elemental Motion (Air)|auto-trad|
|[itmiGioGNuVvt4QE.htm](spell-effects/itmiGioGNuVvt4QE.htm)|Spell Effect: Rapid Adaptation (Aquatic Speed Bonus)|auto-trad|
|[IWD5RehCxZVfgrX9.htm](spell-effects/IWD5RehCxZVfgrX9.htm)|Spell Effect: Elephant Form|auto-trad|
|[IXS15IQXYCZ8vsmX.htm](spell-effects/IXS15IQXYCZ8vsmX.htm)|Spell Effect: Darkvision|auto-trad|
|[iZYjxY0qYvg5yPP3.htm](spell-effects/iZYjxY0qYvg5yPP3.htm)|Spell Effect: Angelic Wings|auto-trad|
|[j2LhQ7kEQhq3J3zZ.htm](spell-effects/j2LhQ7kEQhq3J3zZ.htm)|Spell Effect: Animal Form (Frog)|auto-trad|
|[J60rN48XzBGHmR6m.htm](spell-effects/J60rN48XzBGHmR6m.htm)|Spell Effect: Element Embodied (Air)|auto-trad|
|[j6po934p4jcUVC6l.htm](spell-effects/j6po934p4jcUVC6l.htm)|Spell Effect: Shifting Form (Speed)|auto-trad|
|[j9l4LDnAwg9xzYsy.htm](spell-effects/j9l4LDnAwg9xzYsy.htm)|Spell Effect: Life Connection|auto-trad|
|[Jemq5UknGdMO7b73.htm](spell-effects/Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|auto-trad|
|[JhihziXQuoteftdd.htm](spell-effects/JhihziXQuoteftdd.htm)|Spell Effect: Lay on Hands (Vs. Undead)|auto-trad|
|[jj0P4eGVpmdwZjlA.htm](spell-effects/jj0P4eGVpmdwZjlA.htm)|Spell Effect: Instant Armor|auto-trad|
|[jp88SCE3VCRAyE6x.htm](spell-effects/jp88SCE3VCRAyE6x.htm)|Spell Effect: Element Embodied (Earth)|auto-trad|
|[JqrTrvwV7pYStMXz.htm](spell-effects/JqrTrvwV7pYStMXz.htm)|Spell Effect: Levitate|auto-trad|
|[JrNHFNxJayevlv2G.htm](spell-effects/JrNHFNxJayevlv2G.htm)|Spell Effect: Plant Form (Flytrap)|auto-trad|
|[jtMo6qS47hPx6EbR.htm](spell-effects/jtMo6qS47hPx6EbR.htm)|Spell Effect: Rapid Adaptation (Forest)|auto-trad|
|[jtW3VfI5Kktuy3GH.htm](spell-effects/jtW3VfI5Kktuy3GH.htm)|Spell Effect: Dragon Form (Bronze)|auto-trad|
|[jvwKRHtOiPAm4uAP.htm](spell-effects/jvwKRHtOiPAm4uAP.htm)|Spell Effect: Aerial Form (Bat)|auto-trad|
|[jy4edd6pvJvJgOSP.htm](spell-effects/jy4edd6pvJvJgOSP.htm)|Spell Effect: Dragon Wings (Own Speed)|auto-trad|
|[KcBqo33ekJHxZLHo.htm](spell-effects/KcBqo33ekJHxZLHo.htm)|Spell Effect: Fey Form (Naiad)|auto-trad|
|[KkDRRDuycXwKPa6n.htm](spell-effects/KkDRRDuycXwKPa6n.htm)|Spell Effect: Dinosaur Form (Brontosaurus)|auto-trad|
|[KMF2t3qqzyFP0rxL.htm](spell-effects/KMF2t3qqzyFP0rxL.htm)|Spell Effect: Divine Vessel (Good)|auto-trad|
|[kMoOWWBqDYmPcYyS.htm](spell-effects/kMoOWWBqDYmPcYyS.htm)|Spell Effect: Bathe in Blood|auto-trad|
|[KtAJN4Qr2poTL6BB.htm](spell-effects/KtAJN4Qr2poTL6BB.htm)|Spell Effect: Bestial Curse (Critical Failure)|auto-trad|
|[kxMBdANwCcF841uA.htm](spell-effects/kxMBdANwCcF841uA.htm)|Spell Effect: Elemental Form (Water)|auto-trad|
|[kZ39XWJA3RBDTnqG.htm](spell-effects/kZ39XWJA3RBDTnqG.htm)|Spell Effect: Inspire Heroics (Courage, +2)|auto-trad|
|[kz3mlFwb9tV9bFwu.htm](spell-effects/kz3mlFwb9tV9bFwu.htm)|Spell Effect: Animal Form (Snake)|auto-trad|
|[l8HkOKfiUqd3BUwT.htm](spell-effects/l8HkOKfiUqd3BUwT.htm)|Spell Effect: Ancestral Form|auto-trad|
|[l9HRQggofFGIxEse.htm](spell-effects/l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|auto-trad|
|[lEU3DH1tGjAigpEt.htm](spell-effects/lEU3DH1tGjAigpEt.htm)|Spell Effect: Energy Absorption|auto-trad|
|[LHREWCGPkWsc4GGJ.htm](spell-effects/LHREWCGPkWsc4GGJ.htm)|Spell Effect: Faerie Dust (Failure)|auto-trad|
|[lIl0yYdS9zojOZhe.htm](spell-effects/lIl0yYdS9zojOZhe.htm)|Spell Effect: Life-Giving Form|auto-trad|
|[LldX5hnNhKzGtOS0.htm](spell-effects/LldX5hnNhKzGtOS0.htm)|Spell Effect: Elemental Absorption|auto-trad|
|[llrOM8rPP9nxIuEN.htm](spell-effects/llrOM8rPP9nxIuEN.htm)|Spell Effect: Insect Form (Mantis)|auto-trad|
|[lmAwCy7isFvLYdGd.htm](spell-effects/lmAwCy7isFvLYdGd.htm)|Spell Effect: Element Embodied (Fire)|auto-trad|
|[LMXxICrByo7XZ3Q3.htm](spell-effects/LMXxICrByo7XZ3Q3.htm)|Spell Effect: Downpour|auto-trad|
|[LMzFBnOEPzDGzHg4.htm](spell-effects/LMzFBnOEPzDGzHg4.htm)|Spell Effect: Unusual Anatomy|auto-trad|
|[LT5AV9vSN3T9x3J9.htm](spell-effects/LT5AV9vSN3T9x3J9.htm)|Spell Effect: Corrosive Body|auto-trad|
|[lTL5VwNrZ5xiitGV.htm](spell-effects/lTL5VwNrZ5xiitGV.htm)|Spell effect: Nudge the Odds|auto-trad|
|[LXf1Cqi1zyo4DaLv.htm](spell-effects/LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|auto-trad|
|[lyLMiauxIVUM3oF1.htm](spell-effects/lyLMiauxIVUM3oF1.htm)|Spell Effect: Lay on Hands|auto-trad|
|[lywUJljQy2SxRZQt.htm](spell-effects/lywUJljQy2SxRZQt.htm)|Spell Effect: Nature Incarnate (Green Man)|auto-trad|
|[m1tQTBrolf7uZBW0.htm](spell-effects/m1tQTBrolf7uZBW0.htm)|Spell Effect: Discern Secrets (Seek)|auto-trad|
|[m6x0IvoeX0a0bZiQ.htm](spell-effects/m6x0IvoeX0a0bZiQ.htm)|Spell Effect: Unbreaking Wave Vapor|auto-trad|
|[mAofA4oy3cRdT71K.htm](spell-effects/mAofA4oy3cRdT71K.htm)|Spell Effect: Penumbral Disguise|auto-trad|
|[mCb9mWAmgWPQrkTY.htm](spell-effects/mCb9mWAmgWPQrkTY.htm)|Spell Effect: Barkskin (Arboreal's Revenge)|auto-trad|
|[Me470HI6inX3Bovh.htm](spell-effects/Me470HI6inX3Bovh.htm)|Spell Effect: Guided Introspection|auto-trad|
|[mhklZ6wjfty0bF44.htm](spell-effects/mhklZ6wjfty0bF44.htm)|Spell Effect: Speaking Sky|auto-trad|
|[MJSoRFfEdM4j5mNG.htm](spell-effects/MJSoRFfEdM4j5mNG.htm)|Spell Effect: Sweet Dream (Glamour)|auto-trad|
|[MjtPtndJx31q2N9R.htm](spell-effects/MjtPtndJx31q2N9R.htm)|Spell Effect: Amplifying Touch|auto-trad|
|[Mp7252yAsSA8lCEA.htm](spell-effects/Mp7252yAsSA8lCEA.htm)|Spell Effect: One With the Land|auto-trad|
|[MqZ6FScbfGtXB8tt.htm](spell-effects/MqZ6FScbfGtXB8tt.htm)|Spell Effect: Magic Fang|auto-trad|
|[mr6mlkUMeStdChxi.htm](spell-effects/mr6mlkUMeStdChxi.htm)|Spell Effect: Animal Feature (Owl Eyes)|auto-trad|
|[mrSulUdNbwzGSwfu.htm](spell-effects/mrSulUdNbwzGSwfu.htm)|Spell Effect: Glutton's Jaw|auto-trad|
|[MuRBCiZn5IKeaoxi.htm](spell-effects/MuRBCiZn5IKeaoxi.htm)|Spell Effect: Fly|auto-trad|
|[mzDgsuuo5wCgqyxR.htm](spell-effects/mzDgsuuo5wCgqyxR.htm)|Spell Effect: Mirecloak|auto-trad|
|[N1EM3jRyT8PCG1Py.htm](spell-effects/N1EM3jRyT8PCG1Py.htm)|Spell Effect: Traveler's Transit (Climb)|auto-trad|
|[n6NK7wqhTxWr3ij8.htm](spell-effects/n6NK7wqhTxWr3ij8.htm)|Spell Effect: Warding Aggression (+2)|auto-trad|
|[nbW4udOUTrCGL3Gf.htm](spell-effects/nbW4udOUTrCGL3Gf.htm)|Spell Effect: Shifting Form (Climb Speed)|auto-trad|
|[ndj0TpLxyzbyzcm4.htm](spell-effects/ndj0TpLxyzbyzcm4.htm)|Spell Effect: Necrotize (Legs)|auto-trad|
|[nemThuhp3praALY6.htm](spell-effects/nemThuhp3praALY6.htm)|Spell Effect: Zealous Conviction|auto-trad|
|[nHXKK4pRXAzrLdEP.htm](spell-effects/nHXKK4pRXAzrLdEP.htm)|Spell Effect: Take Its Course (Affliction, Help)|auto-trad|
|[nIryhRgeiacQw1Em.htm](spell-effects/nIryhRgeiacQw1Em.htm)|Spell Effect: Soothing Blossoms|auto-trad|
|[nkk4O5fyzrC0057i.htm](spell-effects/nkk4O5fyzrC0057i.htm)|Spell Effect: Soothe|auto-trad|
|[npFFTAxN44WWrGnM.htm](spell-effects/npFFTAxN44WWrGnM.htm)|Spell Effect: Wash Your Luck|auto-trad|
|[NQZ88IoKeMBsfjp7.htm](spell-effects/NQZ88IoKeMBsfjp7.htm)|Spell Effect: Life Boost|auto-trad|
|[nU4SxAk6XreHUi5h.htm](spell-effects/nU4SxAk6XreHUi5h.htm)|Spell Effect: Infectious Enthusiasm|auto-trad|
|[nUJSdm4fy6fcwsvv.htm](spell-effects/nUJSdm4fy6fcwsvv.htm)|Spell Effect: Lashunta's Life Bubble|auto-trad|
|[nWEx5kpkE8YlBZvy.htm](spell-effects/nWEx5kpkE8YlBZvy.htm)|Spell Effect: Dragon Form (Green)|auto-trad|
|[NXzo2kdgVixIZ2T1.htm](spell-effects/NXzo2kdgVixIZ2T1.htm)|Spell Effect: Apex Companion|auto-trad|
|[oaRt210JV4GZIHmJ.htm](spell-effects/oaRt210JV4GZIHmJ.htm)|Spell Effect: Rejuvenating Touch|auto-trad|
|[OeCn76SB92GPOZwr.htm](spell-effects/OeCn76SB92GPOZwr.htm)|Spell Effect: Dragon Form (Brass)|auto-trad|
|[oJbcmpBSHwmx6FD4.htm](spell-effects/oJbcmpBSHwmx6FD4.htm)|Spell Effect: Dinosaur Form (Deinonychus)|auto-trad|
|[otK6eG3b3FV7n2xP.htm](spell-effects/otK6eG3b3FV7n2xP.htm)|Spell Effect: Swampcall (Critical Failure)|auto-trad|
|[OxJEUhim6xzsHIyi.htm](spell-effects/OxJEUhim6xzsHIyi.htm)|Spell Effect: Divine Vessel 9th level (Good)|auto-trad|
|[p8F3MVUkGmpsUDOn.htm](spell-effects/p8F3MVUkGmpsUDOn.htm)|Spell Effect: Untwisting Iron Pillar|auto-trad|
|[pcK88HqL6LjBNH2h.htm](spell-effects/pcK88HqL6LjBNH2h.htm)|Spell Effect: Faerie Dust (Critical Failure)|auto-trad|
|[PDoTV4EhJp63FEaG.htm](spell-effects/PDoTV4EhJp63FEaG.htm)|Spell Effect: Draw Ire (Success)|auto-trad|
|[Pfllo68qdQjC4Qv6.htm](spell-effects/Pfllo68qdQjC4Qv6.htm)|Spell Effect: Prismatic Shield|auto-trad|
|[PhBrHvBwvq8rni9C.htm](spell-effects/PhBrHvBwvq8rni9C.htm)|Spell Effect: Evolution Surge (Large)|auto-trad|
|[phIoucsDa3iplMm2.htm](spell-effects/phIoucsDa3iplMm2.htm)|Spell Effect: Elemental Form (Fire)|auto-trad|
|[PNEGSVYhMKf6kQZ6.htm](spell-effects/PNEGSVYhMKf6kQZ6.htm)|Spell Effect: Call to Arms|auto-trad|
|[pocsoEi84Mr2buOc.htm](spell-effects/pocsoEi84Mr2buOc.htm)|Spell Effect: Evolution Surge (Scent)|auto-trad|
|[PpkOZVoHkBZUmddx.htm](spell-effects/PpkOZVoHkBZUmddx.htm)|Spell Effect: Ooze Form (Ochre Jelly)|auto-trad|
|[pPMldkAbPVOSOPIF.htm](spell-effects/pPMldkAbPVOSOPIF.htm)|Spell Effect: Protect Companion|auto-trad|
|[ppVKJY6AYggn2Fma.htm](spell-effects/ppVKJY6AYggn2Fma.htm)|Spell Effect: Goodberry|auto-trad|
|[PQHP7Oph3BQX1GhF.htm](spell-effects/PQHP7Oph3BQX1GhF.htm)|Spell Effect: Longstrider|auto-trad|
|[ptOqsN5FS0nQh7RW.htm](spell-effects/ptOqsN5FS0nQh7RW.htm)|Spell Effect: Animal Form (Cat)|auto-trad|
|[q4EEYltjqpRGiLsP.htm](spell-effects/q4EEYltjqpRGiLsP.htm)|Spell Effect: Elemental Motion (Fire)|auto-trad|
|[qbOpis7pIkXJbM2B.htm](spell-effects/qbOpis7pIkXJbM2B.htm)|Spell Effect: Elemental Motion (Earth)|auto-trad|
|[QF6RDlCoTvkVHRo4.htm](spell-effects/QF6RDlCoTvkVHRo4.htm)|Effect: Shield Immunity|auto-trad|
|[qhNUfwpkD8BRw4zj.htm](spell-effects/qhNUfwpkD8BRw4zj.htm)|Spell Effect: Magic Hide|auto-trad|
|[QJRaVbulmpOzWi6w.htm](spell-effects/QJRaVbulmpOzWi6w.htm)|Spell Effect: Girzanje's March|auto-trad|
|[qkwb5DD3zmKwvbk0.htm](spell-effects/qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mage Armor|auto-trad|
|[qlz0sJIvqc0FdUdr.htm](spell-effects/qlz0sJIvqc0FdUdr.htm)|Spell Effect: Weapon Surge|auto-trad|
|[qo7DoF11Xl9gqmFc.htm](spell-effects/qo7DoF11Xl9gqmFc.htm)|Spell Effect: Rapid Adaptation (Desert)|auto-trad|
|[qPaEEhczUWCQo6ux.htm](spell-effects/qPaEEhczUWCQo6ux.htm)|Spell Effect: Animal Form (Shark)|auto-trad|
|[qQLHPbUFASKFky1W.htm](spell-effects/qQLHPbUFASKFky1W.htm)|Spell Effect: Hyperfocus|auto-trad|
|[Qr5rgoZvI4KmFY0N.htm](spell-effects/Qr5rgoZvI4KmFY0N.htm)|Spell Effect: Calm Emotions|auto-trad|
|[qRyynMOflGajgAR3.htm](spell-effects/qRyynMOflGajgAR3.htm)|Spell Effect: Mantle of the Magma Heart (Enlarging Eruption)|auto-trad|
|[R27azQfzeFuFc48G.htm](spell-effects/R27azQfzeFuFc48G.htm)|Spell Effect: Take Its Course (Affliction, Hinder)|auto-trad|
|[R3j6song8sYLY5vG.htm](spell-effects/R3j6song8sYLY5vG.htm)|Spell Effect: Community Repair (Critical Success)|auto-trad|
|[RawLEPwyT5CtCZ4D.htm](spell-effects/RawLEPwyT5CtCZ4D.htm)|Spell Effect: Protection|auto-trad|
|[rHXOZAFBdRXIlxt5.htm](spell-effects/rHXOZAFBdRXIlxt5.htm)|Spell Effect: Dragon Form (Black)|auto-trad|
|[RLUhIyqH84Dle4vo.htm](spell-effects/RLUhIyqH84Dle4vo.htm)|Spell Effect: Fungal Infestation (Critical Failure)|auto-trad|
|[ROuGwXEvFznzGE9k.htm](spell-effects/ROuGwXEvFznzGE9k.htm)|Spell Effect: Swampcall (Failure)|auto-trad|
|[rQaltMIEi2bn1Z4k.htm](spell-effects/rQaltMIEi2bn1Z4k.htm)|Spell Effect: Ki Form|auto-trad|
|[rTVZ0zwiKeslRw6p.htm](spell-effects/rTVZ0zwiKeslRw6p.htm)|Spell Effect: Wild Morph|auto-trad|
|[s6CwkSsMDGfUmotn.htm](spell-effects/s6CwkSsMDGfUmotn.htm)|Spell Effect: Death Ward|auto-trad|
|[S75DOLjKaSJGMc0D.htm](spell-effects/S75DOLjKaSJGMc0D.htm)|Spell Effect: Fey Form (Elananx)|auto-trad|
|[sccNh8j1PKLHCKh1.htm](spell-effects/sccNh8j1PKLHCKh1.htm)|Spell Effect: Angel Form (Choral)|auto-trad|
|[ScF0ECWnfXMHYLDL.htm](spell-effects/ScF0ECWnfXMHYLDL.htm)|Spell Effect: Daemon Form (Leukodaemon)|auto-trad|
|[sDN9b4bjCGH2nQnG.htm](spell-effects/sDN9b4bjCGH2nQnG.htm)|Spell Effect: Rapid Adaptation (Mountain)|auto-trad|
|[sE2txm68yZSFMV3v.htm](spell-effects/sE2txm68yZSFMV3v.htm)|Spell Effect: Sweet Dream (Voyaging)|auto-trad|
|[sfJyQKmoxSRo6FyP.htm](spell-effects/sfJyQKmoxSRo6FyP.htm)|Spell Effect: Aberrant Form (Gug)|auto-trad|
|[sILRkGTwoBywy0BU.htm](spell-effects/sILRkGTwoBywy0BU.htm)|Spell Effect: Gaseous Form|auto-trad|
|[SjfDoeymtnYKoGUD.htm](spell-effects/SjfDoeymtnYKoGUD.htm)|Spell Effect: Aberrant Form (Otyugh)|auto-trad|
|[slI9P4jUp3ERPCqX.htm](spell-effects/slI9P4jUp3ERPCqX.htm)|Spell Effect: Impeccable Flow|auto-trad|
|[sN3mQ7YrPBogEJRn.htm](spell-effects/sN3mQ7YrPBogEJRn.htm)|Spell Effect: Animal Form (Canine)|auto-trad|
|[sPCWrhUHqlbGhYSD.htm](spell-effects/sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|auto-trad|
|[sXe7cPazOJbX41GU.htm](spell-effects/sXe7cPazOJbX41GU.htm)|Spell Effect: Demon Form (Hezrou)|auto-trad|
|[T3t9776ataHzrmTs.htm](spell-effects/T3t9776ataHzrmTs.htm)|Spell Effect: Inside Ropes (3rd level)|auto-trad|
|[T5bk6UH7yuYog1Fp.htm](spell-effects/T5bk6UH7yuYog1Fp.htm)|Spell Effect: See Invisibility|auto-trad|
|[T6XnxvsgvvOrpien.htm](spell-effects/T6XnxvsgvvOrpien.htm)|Spell Effect: Dinosaur Form (Stegosaurus)|auto-trad|
|[TAAWbJgfESltn2we.htm](spell-effects/TAAWbJgfESltn2we.htm)|Spell Effect: Primal Summons (Water)|auto-trad|
|[tBgwWblDp1xdxN4D.htm](spell-effects/tBgwWblDp1xdxN4D.htm)|Spell Effect: Divine Vessel (Lawful)|auto-trad|
|[tfdDpf9xSWgQer5g.htm](spell-effects/tfdDpf9xSWgQer5g.htm)|Spell Effect: Cosmic Form (Moon)|auto-trad|
|[ThFug45WHkQQXcoF.htm](spell-effects/ThFug45WHkQQXcoF.htm)|Spell Effect: Fleet Step|auto-trad|
|[tjC6JeZgLDPIMHjG.htm](spell-effects/tjC6JeZgLDPIMHjG.htm)|Spell Effect: Malignant Sustenance|auto-trad|
|[TjGHxli0edXI6rAg.htm](spell-effects/TjGHxli0edXI6rAg.htm)|Spell Effect: Schadenfreude (Success)|auto-trad|
|[tk3go5Cl6Qt130Dk.htm](spell-effects/tk3go5Cl6Qt130Dk.htm)|Spell Effect: Animal Form (Ape)|auto-trad|
|[tNjimcyUwn8afeH6.htm](spell-effects/tNjimcyUwn8afeH6.htm)|Spell Effect: Gravity Weapon|auto-trad|
|[TpVkVALUBrBQjULn.htm](spell-effects/TpVkVALUBrBQjULn.htm)|Spell Effect: Stoke the Heart|auto-trad|
|[tu8FyCtmL3YYR2jL.htm](spell-effects/tu8FyCtmL3YYR2jL.htm)|Spell Effect: Plant Form (Arboreal)|auto-trad|
|[TUyEeLyqdJL6PwbH.htm](spell-effects/TUyEeLyqdJL6PwbH.htm)|Spell Effect: Dragon Form (Silver)|auto-trad|
|[TwtUIEyenrtAbeiX.htm](spell-effects/TwtUIEyenrtAbeiX.htm)|Spell Effect: Tanglefoot|auto-trad|
|[u0AznkjTZAVnCMNp.htm](spell-effects/u0AznkjTZAVnCMNp.htm)|Spell Effect: Evolution Surge (Fly)|auto-trad|
|[U2eD42cGwdvQMdN0.htm](spell-effects/U2eD42cGwdvQMdN0.htm)|Spell Effect: Fiery Body (9th Level)|auto-trad|
|[uDOxq24S7IT2EcXv.htm](spell-effects/uDOxq24S7IT2EcXv.htm)|Spell Effect: Object Memory (Weapon)|auto-trad|
|[UH2sT6eW5e31Xytd.htm](spell-effects/UH2sT6eW5e31Xytd.htm)|Spell Effect: Dutiful Challenge|auto-trad|
|[uIMaMzd6pcKmMNPJ.htm](spell-effects/uIMaMzd6pcKmMNPJ.htm)|Spell Effect: Fiery Body|auto-trad|
|[Uj9VFXoVMH0mTTdt.htm](spell-effects/Uj9VFXoVMH0mTTdt.htm)|Spell Effect: Organsight|auto-trad|
|[UjoNm3lrhlg4ctAQ.htm](spell-effects/UjoNm3lrhlg4ctAQ.htm)|Spell Effect: Aerial Form (Pterosaur)|auto-trad|
|[Um25D1qLtZWOSBny.htm](spell-effects/Um25D1qLtZWOSBny.htm)|Spell Effect: Shifting Form (Swim Speed)|auto-trad|
|[UtIOWubq7akdHMOh.htm](spell-effects/UtIOWubq7akdHMOh.htm)|Spell Effect: Augment Summoning|auto-trad|
|[UTLp7omqsiC36bso.htm](spell-effects/UTLp7omqsiC36bso.htm)|Spell Effect: Bane|auto-trad|
|[uUlFic5lnr2FpNiG.htm](spell-effects/uUlFic5lnr2FpNiG.htm)|Spell Effect: Lashunta's Life Bubble (Heightened)|auto-trad|
|[UVrEe0nukiSmiwfF.htm](spell-effects/UVrEe0nukiSmiwfF.htm)|Spell Effect: Reinforce Eidolon|auto-trad|
|[UXdt1WVA66oZOoZS.htm](spell-effects/UXdt1WVA66oZOoZS.htm)|Spell Effect: Flame Barrier|auto-trad|
|[v051JKN0Dj3ve5cF.htm](spell-effects/v051JKN0Dj3ve5cF.htm)|Spell Effect: Sweet Dream (Insight)|auto-trad|
|[v09uwq1eHEAy2bgh.htm](spell-effects/v09uwq1eHEAy2bgh.htm)|Spell Effect: Unbreaking Wave Barrier|auto-trad|
|[V4a9pZHNUlddAwTA.htm](spell-effects/V4a9pZHNUlddAwTA.htm)|Spell Effect: Dragon Form (Red)|auto-trad|
|[V7jAnItnVqtfCAKt.htm](spell-effects/V7jAnItnVqtfCAKt.htm)|Spell Effect: Dragon Wings (60 Feet)|auto-trad|
|[VFereWC1agrwgzPL.htm](spell-effects/VFereWC1agrwgzPL.htm)|Spell Effect: Inspire Heroics (Courage, +3)|auto-trad|
|[ViBlOrd6hno3DiPP.htm](spell-effects/ViBlOrd6hno3DiPP.htm)|Spell Effect: Stumbling Curse|auto-trad|
|[W0PjCMyGOpKAuyKX.htm](spell-effects/W0PjCMyGOpKAuyKX.htm)|Spell Effect: Weapon Surge (Major Striking)|auto-trad|
|[w1HwO7huxJoK0gHY.htm](spell-effects/w1HwO7huxJoK0gHY.htm)|Spell Effect: Element Embodied (Water)|auto-trad|
|[W4lb3417rNDd9tCq.htm](spell-effects/W4lb3417rNDd9tCq.htm)|Spell Effect: Righteous Might|auto-trad|
|[w9HpSwBLByNyRXvi.htm](spell-effects/w9HpSwBLByNyRXvi.htm)|Spell Effect: Blessing of Defiance (2 Action)|auto-trad|
|[WEpgIGFwtRb3ef1x.htm](spell-effects/WEpgIGFwtRb3ef1x.htm)|Spell Effect: Angel Form (Balisse)|auto-trad|
|[wKdIf5x7zztiFHpL.htm](spell-effects/wKdIf5x7zztiFHpL.htm)|Spell Effect: Divine Vessel 9th level (Lawful)|auto-trad|
|[wqh8D9kHGItZBvtQ.htm](spell-effects/wqh8D9kHGItZBvtQ.htm)|Spell Effect: Mantle of the Frozen Heart (Icy Claws)|auto-trad|
|[WWtSEJGwKY4bQpUn.htm](spell-effects/WWtSEJGwKY4bQpUn.htm)|Spell Effect: Vital Beacon|auto-trad|
|[X1kkbRrh4zJuDGjl.htm](spell-effects/X1kkbRrh4zJuDGjl.htm)|Spell Effect: Demon Form (Babau)|auto-trad|
|[X7RD0JRxhJV9u2LC.htm](spell-effects/X7RD0JRxhJV9u2LC.htm)|Spell Effect: Disrupting Weapons|auto-trad|
|[xgZxYqjDPNtsQ3Qp.htm](spell-effects/xgZxYqjDPNtsQ3Qp.htm)|Spell Effect: Aerial Form (Wasp)|auto-trad|
|[xKJVqN1ETnNH3tFg.htm](spell-effects/xKJVqN1ETnNH3tFg.htm)|Spell Effect: Corrosive Body (Heightened 9th)|auto-trad|
|[Xl48OsJ47oDVZAVQ.htm](spell-effects/Xl48OsJ47oDVZAVQ.htm)|Spell Effect: Primal Summons (Earth)|auto-trad|
|[Xlwt1wpjEKWBLUjK.htm](spell-effects/Xlwt1wpjEKWBLUjK.htm)|Spell Effect: Animal Feature (Fish Tail)|auto-trad|
|[XMBoKRRyooKnGkHk.htm](spell-effects/XMBoKRRyooKnGkHk.htm)|Spell Effect: Practice Makes Perfect|auto-trad|
|[XMOWgJV8UXWmpgk0.htm](spell-effects/XMOWgJV8UXWmpgk0.htm)|Spell Effect: Evolution Surge (Speed)|auto-trad|
|[xPVOvWNJORvm8EwP.htm](spell-effects/xPVOvWNJORvm8EwP.htm)|Spell Effect: Mimic Undead|auto-trad|
|[xsy1yaCj0SVsn502.htm](spell-effects/xsy1yaCj0SVsn502.htm)|Spell Effect: Aberrant Form (Chuul)|auto-trad|
|[XT3AyRfx4xeXfAjP.htm](spell-effects/XT3AyRfx4xeXfAjP.htm)|Spell Effect: Physical Boost|auto-trad|
|[XTgxkQkhlap66e54.htm](spell-effects/XTgxkQkhlap66e54.htm)|Spell Effect: Iron Gut (3rd Level)|auto-trad|
|[y9PJdDYFemhk6Z5o.htm](spell-effects/y9PJdDYFemhk6Z5o.htm)|Spell Effect: Agile Feet|auto-trad|
|[YCno88Te0nfwFgVo.htm](spell-effects/YCno88Te0nfwFgVo.htm)|Spell Effect: Mantle of the Frozen Heart (Ice Glide)|auto-trad|
|[ydsLEGjY89Akc4oZ.htm](spell-effects/ydsLEGjY89Akc4oZ.htm)|Spell Effect: Pest Form|auto-trad|
|[yke7fAUDxUNzouQc.htm](spell-effects/yke7fAUDxUNzouQc.htm)|Spell Effect: Elemental Gift (Air)|auto-trad|
|[yq6iu0Qxg3YEbb6s.htm](spell-effects/yq6iu0Qxg3YEbb6s.htm)|Spell Effect: Elemental Gift (Water)|auto-trad|
|[zdpTPf157rXl3tEJ.htm](spell-effects/zdpTPf157rXl3tEJ.htm)|Spell Effect: Clawsong (Deadly D8)|auto-trad|
|[ZHVtJKnur9PAF5TO.htm](spell-effects/ZHVtJKnur9PAF5TO.htm)|Spell Effect: Enduring Might|auto-trad|
|[zIRnnuj4lARq43DA.htm](spell-effects/zIRnnuj4lARq43DA.htm)|Spell Effect: Daemon Form (Meladaemon)|auto-trad|
|[zjFN1cJEl3AMKiVs.htm](spell-effects/zjFN1cJEl3AMKiVs.htm)|Spell Effect: Nymph's Token|auto-trad|
|[ZlsuhS9J0S3PuvCO.htm](spell-effects/ZlsuhS9J0S3PuvCO.htm)|Spell Effect: Blink|auto-trad|
|[zPGVOLz6xhsQN35C.htm](spell-effects/zPGVOLz6xhsQN35C.htm)|Spell Effect: Envenom Companion|auto-trad|
|[zpxIwEjnLUSO1B4z.htm](spell-effects/zpxIwEjnLUSO1B4z.htm)|Spell Effect: Magic's Vessel|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[782NyomkDHyfsUn6.htm](spell-effects/782NyomkDHyfsUn6.htm)|Spell Effect: Insect Form (Spider)|Efecto de Hechizo: Forma de insecto (Araña)|modificada|
|[bOjuEX3qj7XAOoDF.htm](spell-effects/bOjuEX3qj7XAOoDF.htm)|Spell Effect: Insect Form (Scorpion)|Efecto del hechizo: forma de insecto (escorpión).|modificada|
|[DENMzySYANjUBs4O.htm](spell-effects/DENMzySYANjUBs4O.htm)|Spell Effect: Insect Form (Centipede)|Efecto del hechizo: Forma de insecto (Ciempiés).|modificada|
|[iOKhr2El8R6cz6YI.htm](spell-effects/iOKhr2El8R6cz6YI.htm)|Spell Effect: Dinosaur Form (Triceratops)|Efecto de Hechizo: Forma de dinosaurio (Triceratops)|modificada|
|[mmJNE57hC7G3SPae.htm](spell-effects/mmJNE57hC7G3SPae.htm)|Spell Effect: Silence|Efecto de Hechizo: Silencio|modificada|
|[PkofF4bxkDUgeIoE.htm](spell-effects/PkofF4bxkDUgeIoE.htm)|Spell Effect: Touch of Corruption (Healing)|Efecto de Hechizo: Toque de Corrupción (Curar)|modificada|
|[rEsgDhunQ5Yx8KZx.htm](spell-effects/rEsgDhunQ5Yx8KZx.htm)|Spell Effect: Monstrosity Form (Purple Worm)|Efecto de Hechizo: Forma monstruosa (Gusano morado)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
