# Estado de la traducción (equipment-effects)

 * **auto-trad**: 337
 * **ninguna**: 87
 * **vacía**: 6


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[5mQ51m1lqQlvfi8n.htm](equipment-effects/5mQ51m1lqQlvfi8n.htm)|Effect: Phantasmal Doorknob - Weapon|
|[9mIS76oZkxXQ4g3T.htm](equipment-effects/9mIS76oZkxXQ4g3T.htm)|Effect: Jolt Coil - Armor|
|[AwpSNXaKloq2KQNy.htm](equipment-effects/AwpSNXaKloq2KQNy.htm)|Effect: Sanguine Fang - Armor (Greater)|
|[bKEx53h6lrFOYvpu.htm](equipment-effects/bKEx53h6lrFOYvpu.htm)|Effect: Sanguine Fang - Weapon|
|[bMZ6Gw58sK8sFp5n.htm](equipment-effects/bMZ6Gw58sK8sFp5n.htm)|Effect: Crackling Bubble Gum (Failure)|
|[Bwh3kiNX8nxEwVZ5.htm](equipment-effects/Bwh3kiNX8nxEwVZ5.htm)|Effect: Sanguine Fang - Weapon (Greater)|
|[ceIvpmxWqBJpBHIn.htm](equipment-effects/ceIvpmxWqBJpBHIn.htm)|Effect: Jolt Coil - Weapon (Major)|
|[cOcHWeogJFIkEI0d.htm](equipment-effects/cOcHWeogJFIkEI0d.htm)|Effect: Energizing Lattice|
|[D8cI0uhI3YFFQssp.htm](equipment-effects/D8cI0uhI3YFFQssp.htm)|Effect: Phantasmal Doorknob - Armor (Major)|
|[DaN3N9bOzqDhOng0.htm](equipment-effects/DaN3N9bOzqDhOng0.htm)|Effect: Pickled Demon Tongue - Weapon (Major)|
|[dfnhwI5pJgLtXh2k.htm](equipment-effects/dfnhwI5pJgLtXh2k.htm)|Effect: Silversheen|
|[dMbRTs2YvnDwtUEj.htm](equipment-effects/dMbRTs2YvnDwtUEj.htm)|Effect: Phantasmal Doorknob - Weapon (Major)|
|[ECGvrM0eAaJlm1VC.htm](equipment-effects/ECGvrM0eAaJlm1VC.htm)|Effect: Desolation Locket - Armor (Greater)|
|[EjLVjt3GMeHM0Ai3.htm](equipment-effects/EjLVjt3GMeHM0Ai3.htm)|Effect: Ghostcaller's Planchette - Armor|
|[eLQABqabYp41Mw1R.htm](equipment-effects/eLQABqabYp41Mw1R.htm)|Effect: Immortal Bastion|
|[EPqnA5OlNpwr41Os.htm](equipment-effects/EPqnA5OlNpwr41Os.htm)|Effect: Beastmaster's Sigil - Ranged Weapon|
|[EwHufLQI1z1QzqZU.htm](equipment-effects/EwHufLQI1z1QzqZU.htm)|Effect: Jolt Coil - Weapon (Greater)|
|[Fb4QI4zlmoqfwHY0.htm](equipment-effects/Fb4QI4zlmoqfwHY0.htm)|Effect: Wyrm Claw - Armor (Major)|
|[ft5LjQSa8mZkklhM.htm](equipment-effects/ft5LjQSa8mZkklhM.htm)|Effect: Polished Demon Horn - Armor|
|[gaEXUewHgPpM3zfW.htm](equipment-effects/gaEXUewHgPpM3zfW.htm)|Effect: Jyoti's Feather - Armor (Greater)|
|[gAQaizpMbZLDbzg7.htm](equipment-effects/gAQaizpMbZLDbzg7.htm)|Effect: Rime Crystal - Armor|
|[grXFmNl8Zy3VRVpR.htm](equipment-effects/grXFmNl8Zy3VRVpR.htm)|Effect: Ghostcaller's Planchette - Weapon|
|[gU3uZE2ihLnpQN0b.htm](equipment-effects/gU3uZE2ihLnpQN0b.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Greater)|
|[GUHNFlNYiR38sTDE.htm](equipment-effects/GUHNFlNYiR38sTDE.htm)|Effect: Crackling Bubble Gum|
|[gZOED4T3o6giterN.htm](equipment-effects/gZOED4T3o6giterN.htm)|Effect: Beastmaster's Sigil - Armor (Greater)|
|[hozXQvKqp62DnawX.htm](equipment-effects/hozXQvKqp62DnawX.htm)|Effect: Jyoti's Feather - Armor|
|[i5agc4lBE6GfeCXq.htm](equipment-effects/i5agc4lBE6GfeCXq.htm)|Effect: Cold Iron Blanch (Lesser)|
|[IiDpW99zrh7zHxmQ.htm](equipment-effects/IiDpW99zrh7zHxmQ.htm)|Effect: Rime Crystal - Weapon|
|[IiPqlP4C7YTjkE9w.htm](equipment-effects/IiPqlP4C7YTjkE9w.htm)|Effect: Phantasmal Doorknob - Weapon (Greater)|
|[IlNjAwsIZShlVsCT.htm](equipment-effects/IlNjAwsIZShlVsCT.htm)|Effect: Architect's Pattern Book|
|[inqXnzrqYzbUBuOj.htm](equipment-effects/inqXnzrqYzbUBuOj.htm)|Effect: Diplomat's Charcuterie|
|[iOY4XqVZNiQ5esdu.htm](equipment-effects/iOY4XqVZNiQ5esdu.htm)|Effect: Pickled Demon Tongue - Armor (Major)|
|[JvwzM4rJWwtB9HAP.htm](equipment-effects/JvwzM4rJWwtB9HAP.htm)|Effect: Wolfjaw Armor|
|[KJXNLvJAl0mNnGvn.htm](equipment-effects/KJXNLvJAl0mNnGvn.htm)|Effect: Jyoti's Feather - Weapon|
|[LbaYzs0dQuFj8FXJ.htm](equipment-effects/LbaYzs0dQuFj8FXJ.htm)|Effect: Pickled Demon Tongue - Weapon|
|[LjaEu7gAGO77uVs2.htm](equipment-effects/LjaEu7gAGO77uVs2.htm)|Effect: Hexing Jar|
|[M1HKNPpqkjFI9A4q.htm](equipment-effects/M1HKNPpqkjFI9A4q.htm)|Effect: Beastmaster's Sigil - Armor|
|[mHIdEC7RX6isILiM.htm](equipment-effects/mHIdEC7RX6isILiM.htm)|Effect: Jolt Coil - Weapon|
|[mQgA7XmjVI6WG6oq.htm](equipment-effects/mQgA7XmjVI6WG6oq.htm)|Effect: Sanguine Fang - Armor|
|[NBxTbdCCqmilAxqA.htm](equipment-effects/NBxTbdCCqmilAxqA.htm)|Effect: Whispering Staff (Enemy)|
|[NddLhLIQYgZYrPTR.htm](equipment-effects/NddLhLIQYgZYrPTR.htm)|Effect: Pickled Demon Tongue - Weapon (Greater)|
|[NwEVRZmLbM9QKoIH.htm](equipment-effects/NwEVRZmLbM9QKoIH.htm)|Effect: Desolation Locket - Weapon|
|[OFJVaPxdafc4ezWB.htm](equipment-effects/OFJVaPxdafc4ezWB.htm)|Effect: Cold Iron Blanch (Greater)|
|[omyZyfTnx3uYVgiP.htm](equipment-effects/omyZyfTnx3uYVgiP.htm)|Effect: Arachnid Harness|
|[owA1eQU6LTP3A3of.htm](equipment-effects/owA1eQU6LTP3A3of.htm)|Effect: Wyrm Claw - Armor (Greater)|
|[P882YXPpESinSvrJ.htm](equipment-effects/P882YXPpESinSvrJ.htm)|Effect: Polished Demon Horn - Weapon (Major)|
|[PBvLrztlLIfr2dlV.htm](equipment-effects/PBvLrztlLIfr2dlV.htm)|Effect: Phantasmal Doorknob - Armor|
|[pgWhwSGZd8JT5IlF.htm](equipment-effects/pgWhwSGZd8JT5IlF.htm)|Effect: Ghostcaller's Planchette - Weapon (Greater)|
|[pnBdSjOtQb9T1ajL.htm](equipment-effects/pnBdSjOtQb9T1ajL.htm)|Effect: Jolt Coil - Armor (Major)|
|[pr12dSHV4nIyVG5n.htm](equipment-effects/pr12dSHV4nIyVG5n.htm)|Effect: Polished Demon Horn - Armor (Greater)|
|[QCqYiSIR9DVPAHgR.htm](equipment-effects/QCqYiSIR9DVPAHgR.htm)|Effect: Assassin Vine Wine|
|[qFT6TJU5EObpoixe.htm](equipment-effects/qFT6TJU5EObpoixe.htm)|Effect: Ghostcaller's Planchette - Armor (Greater)|
|[qLl1jwybXY6EbOoI.htm](equipment-effects/qLl1jwybXY6EbOoI.htm)|Effect: Bewitching Bloom (Magnolia)|
|[qoV03Fk6HSzZUCmv.htm](equipment-effects/qoV03Fk6HSzZUCmv.htm)|Effect: Sanguine Fang - Weapon (Major)|
|[Qvjw5RYhglGcVRhF.htm](equipment-effects/Qvjw5RYhglGcVRhF.htm)|Effect: Rime Crystal - Weapon (Major)|
|[rdHzCYZEWpy2rTfI.htm](equipment-effects/rdHzCYZEWpy2rTfI.htm)|Effect: Beastmaster's Sigil - Melee Weapon|
|[Rfw1T5NXIoeUbJzt.htm](equipment-effects/Rfw1T5NXIoeUbJzt.htm)|Effect: Polished Demon Horn - Armor (Major)|
|[ri5qxyVViva60ilN.htm](equipment-effects/ri5qxyVViva60ilN.htm)|Effect: Bewitching Bloom (Lotus)|
|[rQV8Azb3FeUJJ3fG.htm](equipment-effects/rQV8Azb3FeUJJ3fG.htm)|Effect: Delve Scale|
|[RxtpVyOywdrt29Q6.htm](equipment-effects/RxtpVyOywdrt29Q6.htm)|Effect: Desolation Locket - Armor (Major)|
|[T38SHe842S43a8bB.htm](equipment-effects/T38SHe842S43a8bB.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Major)|
|[Thd0XXhunYNk6jD7.htm](equipment-effects/Thd0XXhunYNk6jD7.htm)|Effect: Cinnamon Seers|
|[TM0LFTy30FG2wwI2.htm](equipment-effects/TM0LFTy30FG2wwI2.htm)|Effect: Star of Cynosure|
|[tNaFPSbNkcyHS50y.htm](equipment-effects/tNaFPSbNkcyHS50y.htm)|Effect: Polished Demon Horn - Weapon (Greater)|
|[TU67AK08CUsP7pl4.htm](equipment-effects/TU67AK08CUsP7pl4.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Greater)|
|[uHZ23fBG9HIdK5ht.htm](equipment-effects/uHZ23fBG9HIdK5ht.htm)|Effect: Tremorsensors|
|[UihgHdEj0GsaRaAL.htm](equipment-effects/UihgHdEj0GsaRaAL.htm)|Effect: Phantasmal Doorknob - Armor (Greater)|
|[uijpoXaiKXcCYrSD.htm](equipment-effects/uijpoXaiKXcCYrSD.htm)|Effect: Auric Noodles|
|[uK2vXk4WnleihqYI.htm](equipment-effects/uK2vXk4WnleihqYI.htm)|Effect: Cold Iron Blanch (Moderate)|
|[UPMZe0oKVpUgDaOE.htm](equipment-effects/UPMZe0oKVpUgDaOE.htm)|Effect: Pickled Demon Tongue - Armor (Greater)|
|[VKdiRnhrsgQTFSCM.htm](equipment-effects/VKdiRnhrsgQTFSCM.htm)|Effect: Whispering Staff (Ally)|
|[VVXjPCummVHQp7hG.htm](equipment-effects/VVXjPCummVHQp7hG.htm)|Effect: Bloodhound Olfactory Stimulators|
|[vw6BbXYsEgCR3dPt.htm](equipment-effects/vw6BbXYsEgCR3dPt.htm)|Effect: Rime Crystal - Armor (Greater)|
|[WARLTi8unmPgmnNw.htm](equipment-effects/WARLTi8unmPgmnNw.htm)|Effect: Polished Demon Horn - Weapon|
|[wcjEjFKLcPisk4jK.htm](equipment-effects/wcjEjFKLcPisk4jK.htm)|Effect: Jyoti's Feather - Weapon (Major)|
|[WGmrfhdQzlNzyMrq.htm](equipment-effects/WGmrfhdQzlNzyMrq.htm)|Effect: Jyoti's Feather - Weapon (Greater)|
|[WJ9L6rgUTZVV7vEE.htm](equipment-effects/WJ9L6rgUTZVV7vEE.htm)|Effect: Desolation Locket - Armor|
|[WLvFC2eE80SEZpUg.htm](equipment-effects/WLvFC2eE80SEZpUg.htm)|Effect: Wyrm Claw - Armor|
|[WRvZ2Nq3wquisD4Y.htm](equipment-effects/WRvZ2Nq3wquisD4Y.htm)|Effect: Pickled Demon Tongue - Armor|
|[Wylo8ttAkExaX6Gs.htm](equipment-effects/Wylo8ttAkExaX6Gs.htm)|Effect: Rime Crystal - Armor (Major)|
|[xiG3kmPJBpX2KA7l.htm](equipment-effects/xiG3kmPJBpX2KA7l.htm)|Effect: Oil of Corpse Restoration|
|[xMG5PrT6NvCFYGqI.htm](equipment-effects/xMG5PrT6NvCFYGqI.htm)|Effect: Sanguine Fang - Armor (Major)|
|[xSw7cTboMvP8sJAq.htm](equipment-effects/xSw7cTboMvP8sJAq.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Major)|
|[YAZ1iri403S8XcrH.htm](equipment-effects/YAZ1iri403S8XcrH.htm)|Effect: Jyoti's Feather - Armor (Major)|
|[YflZZ7EG7JJkdX0d.htm](equipment-effects/YflZZ7EG7JJkdX0d.htm)|Effect: Jolt Coil - Armor (Greater)|
|[YsV7tB15XrSCKNnB.htm](equipment-effects/YsV7tB15XrSCKNnB.htm)|Effect: Rime Crystal - Weapon (Greater)|
|[yzENPvcYIxegPflt.htm](equipment-effects/yzENPvcYIxegPflt.htm)|Effect: Beastmaster's Sigil - Armor (Major)|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0qJEtpXWPb7JJBbY.htm](equipment-effects/0qJEtpXWPb7JJBbY.htm)|Effect: Aromatic Ammunition|auto-trad|
|[0YbNzbW0HSKtgStQ.htm](equipment-effects/0YbNzbW0HSKtgStQ.htm)|Effect: Dragon's Blood Pudding (Major)|auto-trad|
|[16tOZk4qy329s2aK.htm](equipment-effects/16tOZk4qy329s2aK.htm)|Effect: Shielding Salve|auto-trad|
|[19ECULG5Zp593jQX.htm](equipment-effects/19ECULG5Zp593jQX.htm)|Effect: Dragon's Blood Pudding (Moderate)|auto-trad|
|[1ihy7Jvw5PY4WYbP.htm](equipment-effects/1ihy7Jvw5PY4WYbP.htm)|Effect: Eye of the Unseen (Greater)|auto-trad|
|[1l139A2Qik4lBHKO.htm](equipment-effects/1l139A2Qik4lBHKO.htm)|Effect: Juggernaut Mutagen (Lesser)|auto-trad|
|[1mKjaWC65KWPuFR4.htm](equipment-effects/1mKjaWC65KWPuFR4.htm)|Effect: Antidote (Major)|auto-trad|
|[1N28rGPbAl2IkGUf.htm](equipment-effects/1N28rGPbAl2IkGUf.htm)|Effect: Slime Whip|auto-trad|
|[1ouUo8lLK6H79Rqh.htm](equipment-effects/1ouUo8lLK6H79Rqh.htm)|Effect: Bestial Mutagen (Major)|auto-trad|
|[1S51uIRb9bnZtpFU.htm](equipment-effects/1S51uIRb9bnZtpFU.htm)|Effect: Winged Boots|auto-trad|
|[1tweTwYQuQUV45wJ.htm](equipment-effects/1tweTwYQuQUV45wJ.htm)|Effect: Rowan Rifle (Cold)|auto-trad|
|[1xHHvQlW4pRR89qj.htm](equipment-effects/1xHHvQlW4pRR89qj.htm)|Effect: Stone Body Mutagen (Moderate)|auto-trad|
|[2Bds6d4UGQZqYSZM.htm](equipment-effects/2Bds6d4UGQZqYSZM.htm)|Effect: Quicksilver Mutagen (Greater)|auto-trad|
|[2C1HuKDQDGFZuv7l.htm](equipment-effects/2C1HuKDQDGFZuv7l.htm)|Effect: Boulderhead Bock|auto-trad|
|[2iR5uP6vgPzgKKNO.htm](equipment-effects/2iR5uP6vgPzgKKNO.htm)|Effect: Red-Rib Gill Mask (Moderate)|auto-trad|
|[2PNo8u4wxSbz5WEs.htm](equipment-effects/2PNo8u4wxSbz5WEs.htm)|Effect: Juggernaut Mutagen (Major)|auto-trad|
|[2YgXoHvJfrDHucMr.htm](equipment-effects/2YgXoHvJfrDHucMr.htm)|Effect: Raise a Shield|auto-trad|
|[2ytxPqhGyLtEjYxW.htm](equipment-effects/2ytxPqhGyLtEjYxW.htm)|Effect: Static Snare|auto-trad|
|[3LhreroLRmI4atE6.htm](equipment-effects/3LhreroLRmI4atE6.htm)|Effect: Clockwork Cloak|auto-trad|
|[3O5lvuX4VHqtpCkU.htm](equipment-effects/3O5lvuX4VHqtpCkU.htm)|Effect: Lover's Gloves|auto-trad|
|[4aSqtBgvQr2TI3XT.htm](equipment-effects/4aSqtBgvQr2TI3XT.htm)|Effect: Grit (Stage 2)|auto-trad|
|[4G9qnI0oRyL6eKFQ.htm](equipment-effects/4G9qnI0oRyL6eKFQ.htm)|Effect: Frost Vial (Major)|auto-trad|
|[4JULykNCgQoypsu8.htm](equipment-effects/4JULykNCgQoypsu8.htm)|Effect: Spiderfoot Brew (Major)|auto-trad|
|[4RnEUeYEzC919GZR.htm](equipment-effects/4RnEUeYEzC919GZR.htm)|Effect: Energizing Rune (Sonic)|auto-trad|
|[4tepFOJLhZSelPoa.htm](equipment-effects/4tepFOJLhZSelPoa.htm)|Effect: Dragon Turtle Scale|auto-trad|
|[4uy4Ygf5KD2WrtGW.htm](equipment-effects/4uy4Ygf5KD2WrtGW.htm)|Effect: Nosoi Charm (Diplomacy)|auto-trad|
|[5dhm66yN0LQTOePw.htm](equipment-effects/5dhm66yN0LQTOePw.htm)|Effect: Holy Steam Ball|auto-trad|
|[5Gof60StUppR2Xn9.htm](equipment-effects/5Gof60StUppR2Xn9.htm)|Effect: Skeptic's Elixir (Lesser)|auto-trad|
|[5JYchreCttBg7RcD.htm](equipment-effects/5JYchreCttBg7RcD.htm)|Effect: Goo Grenade|auto-trad|
|[5KXsyN9J78glG25I.htm](equipment-effects/5KXsyN9J78glG25I.htm)|Effect: Ochre Fulcrum Lens|auto-trad|
|[5lZWAvm0oGxvF4bm.htm](equipment-effects/5lZWAvm0oGxvF4bm.htm)|Effect: Exsanguinating Ammunition (Greater)|auto-trad|
|[5o33sch67Z8j5Vom.htm](equipment-effects/5o33sch67Z8j5Vom.htm)|Effect: Lastwall Soup (Improved)|auto-trad|
|[5OABp099y6w3didN.htm](equipment-effects/5OABp099y6w3didN.htm)|Effect: Soulspark Candle|auto-trad|
|[5oYKYXAexr0vhx84.htm](equipment-effects/5oYKYXAexr0vhx84.htm)|Effect: Treat Disease (Critical Failure)|auto-trad|
|[5uK3fmGlfJrbWQz4.htm](equipment-effects/5uK3fmGlfJrbWQz4.htm)|Effect: Stalwart's Ring|auto-trad|
|[5WLda1tGUiKoSj1K.htm](equipment-effects/5WLda1tGUiKoSj1K.htm)|Effect: Stalk Goggles (Major)|auto-trad|
|[5xgapIXn5DwbXHKh.htm](equipment-effects/5xgapIXn5DwbXHKh.htm)|Effect: Serene Mutagen (Lesser)|auto-trad|
|[68xcDyxsNgD3JddD.htm](equipment-effects/68xcDyxsNgD3JddD.htm)|Effect: Energizing Rune (Cold)|auto-trad|
|[6A8jsLR7upLGuRiv.htm](equipment-effects/6A8jsLR7upLGuRiv.htm)|Effect: Lastwall Soup|auto-trad|
|[6alqXfVq0qWQC359.htm](equipment-effects/6alqXfVq0qWQC359.htm)|Effect: Energy Robe of Fire|auto-trad|
|[6dsPjRKjCPd9BWPt.htm](equipment-effects/6dsPjRKjCPd9BWPt.htm)|Effect: Greater Healer's Gel|auto-trad|
|[6p2Sjl7XxCc55ft4.htm](equipment-effects/6p2Sjl7XxCc55ft4.htm)|Effect: Mudrock Snare (Success)|auto-trad|
|[6PNLBIdlqqWNCFMy.htm](equipment-effects/6PNLBIdlqqWNCFMy.htm)|Effect: Quicksilver Mutagen (Lesser)|auto-trad|
|[7dLsA9PAb5ij7Bc6.htm](equipment-effects/7dLsA9PAb5ij7Bc6.htm)|Effect: Dueling Cape|auto-trad|
|[7MgpgF8tOXOiDEwv.htm](equipment-effects/7MgpgF8tOXOiDEwv.htm)|Effect: Vaultbreaker's Harness|auto-trad|
|[7UL8belWmo7U5YGM.htm](equipment-effects/7UL8belWmo7U5YGM.htm)|Effect: Darkvision Elixir (Lesser)|auto-trad|
|[7vCenP9j6FuHRv5C.htm](equipment-effects/7vCenP9j6FuHRv5C.htm)|Effect: Darkvision Elixir (Greater)|auto-trad|
|[7z1iY4AaNEAIKuAU.htm](equipment-effects/7z1iY4AaNEAIKuAU.htm)|Effect: Antidote (Lesser)|auto-trad|
|[88kqcDmsoAEddzUt.htm](equipment-effects/88kqcDmsoAEddzUt.htm)|Effect: Boots of Elvenkind|auto-trad|
|[8ersuvNJXX00XaIQ.htm](equipment-effects/8ersuvNJXX00XaIQ.htm)|Effect: Euryale (Curse) Card|auto-trad|
|[8kfSF8P4NOh09YvZ.htm](equipment-effects/8kfSF8P4NOh09YvZ.htm)|Effect: Grim Sandglass - Weapon (Greater)|auto-trad|
|[8RNPIAuV7ixaXeq5.htm](equipment-effects/8RNPIAuV7ixaXeq5.htm)|Effect: War Blood Mutagen (Greater)|auto-trad|
|[988f6NpOo4YzFzIr.htm](equipment-effects/988f6NpOo4YzFzIr.htm)|Effect: Quicksilver Mutagen (Major)|auto-trad|
|[9BsFdrEc7hkPWgSd.htm](equipment-effects/9BsFdrEc7hkPWgSd.htm)|Effect: Energizing Rune (Electricity)|auto-trad|
|[9e6iVkPpGqJYwMyb.htm](equipment-effects/9e6iVkPpGqJYwMyb.htm)|Effect: Brewer's Regret|auto-trad|
|[9FfFhu2kl2wMTsiI.htm](equipment-effects/9FfFhu2kl2wMTsiI.htm)|Effect: Silvertongue Mutagen (Major)|auto-trad|
|[9j1uTGBGAc7GIhjm.htm](equipment-effects/9j1uTGBGAc7GIhjm.htm)|Effect: Dragonfly Potion|auto-trad|
|[9keegq0GdS1eSrNr.htm](equipment-effects/9keegq0GdS1eSrNr.htm)|Effect: Sea Touch Elixir (Moderate)|auto-trad|
|[9kOgG7BPEfIyWyqm.htm](equipment-effects/9kOgG7BPEfIyWyqm.htm)|Effect: Rowan Rifle (Electricity)|auto-trad|
|[9MeHc072G4L8AJkp.htm](equipment-effects/9MeHc072G4L8AJkp.htm)|Effect: Elixir of Life (True)|auto-trad|
|[9PASRixhNM0ogqmG.htm](equipment-effects/9PASRixhNM0ogqmG.htm)|Effect: Triton's Conch|auto-trad|
|[agDVcRyoS4NTHkht.htm](equipment-effects/agDVcRyoS4NTHkht.htm)|Effect: Trinity Geode - Armor (Major)|auto-trad|
|[ah41XCrV4LFsVyzl.htm](equipment-effects/ah41XCrV4LFsVyzl.htm)|Effect: Shield of the Unified Legion|auto-trad|
|[aIZsC56OdotiGb9M.htm](equipment-effects/aIZsC56OdotiGb9M.htm)|Effect: War Blood Mutagen (Lesser)|auto-trad|
|[AJx8i8QX35vsG5Q4.htm](equipment-effects/AJx8i8QX35vsG5Q4.htm)|Effect: Stonethroat Ammunition (Success)|auto-trad|
|[AMhUb42NAJ1aisZp.htm](equipment-effects/AMhUb42NAJ1aisZp.htm)|Effect: Stone Fist Elixir|auto-trad|
|[ApGnHnZEK7nv3IqL.htm](equipment-effects/ApGnHnZEK7nv3IqL.htm)|Effect: Greater Codex of Unimpeded Sight|auto-trad|
|[ascxqSlMEN9R6OOy.htm](equipment-effects/ascxqSlMEN9R6OOy.htm)|Effect: Energizing Rune (Fire)|auto-trad|
|[AUoiLqENVZlZohsn.htm](equipment-effects/AUoiLqENVZlZohsn.htm)|Effect: Spined Shield Spines|auto-trad|
|[AvXNZ9I6s1H8C4wd.htm](equipment-effects/AvXNZ9I6s1H8C4wd.htm)|Effect: War Blood Mutagen (Moderate)|auto-trad|
|[aXDtl9vMp1vIznya.htm](equipment-effects/aXDtl9vMp1vIznya.htm)|Effect: Eye of the Unseen|auto-trad|
|[b9DTIJyBT8kvIBpj.htm](equipment-effects/b9DTIJyBT8kvIBpj.htm)|Effect: Stone Body Mutagen (Greater)|auto-trad|
|[bcxVvIbuZWOvsKcA.htm](equipment-effects/bcxVvIbuZWOvsKcA.htm)|Effect: Darkvision Elixir (Moderate)|auto-trad|
|[Bg4hNMqBx0yqmWYJ.htm](equipment-effects/Bg4hNMqBx0yqmWYJ.htm)|Effect: Clockwork Goggles (Major)|auto-trad|
|[bP40jr6wE6MCsRvY.htm](equipment-effects/bP40jr6wE6MCsRvY.htm)|Effect: Golden Legion Epaulet|auto-trad|
|[bri7UVNCfHhCIvXN.htm](equipment-effects/bri7UVNCfHhCIvXN.htm)|Effect: Immolation Clan Pistol|auto-trad|
|[buJnkFBzL4e22ASp.htm](equipment-effects/buJnkFBzL4e22ASp.htm)|Effect: Gecko Potion|auto-trad|
|[BV8RPntjc9FUzD3g.htm](equipment-effects/BV8RPntjc9FUzD3g.htm)|Effect: Drakeheart Mutagen (Moderate)|auto-trad|
|[c0URo81HpSmCkuQc.htm](equipment-effects/c0URo81HpSmCkuQc.htm)|Effect: Energy Robe of Electricity|auto-trad|
|[C9Tnl6Q7Z5Sbw5EY.htm](equipment-effects/C9Tnl6Q7Z5Sbw5EY.htm)|Effect: Energy Mutagen (Lesser)|auto-trad|
|[ccMa75bqXo3ZnlHM.htm](equipment-effects/ccMa75bqXo3ZnlHM.htm)|Effect: Five-Feather Wreath - Armor (Major)|auto-trad|
|[cg5qyeMJUh6b4fta.htm](equipment-effects/cg5qyeMJUh6b4fta.htm)|Effect: Belt of the Five Kings (Wearer)|auto-trad|
|[CIfqUEC0mITBjwmL.htm](equipment-effects/CIfqUEC0mITBjwmL.htm)|Effect: Sarkorian God-Caller Garb|auto-trad|
|[cjQHrvoXDCGOsptN.htm](equipment-effects/cjQHrvoXDCGOsptN.htm)|Effect: Flask of Fellowship|auto-trad|
|[ClsVhp5baFRjZQ23.htm](equipment-effects/ClsVhp5baFRjZQ23.htm)|Effect: Energizing Rune (Acid)|auto-trad|
|[cozi2kUELY40Dcv3.htm](equipment-effects/cozi2kUELY40Dcv3.htm)|Effect: Malleable Mixture (Lesser)|auto-trad|
|[csA4UAD2tQq7RjT8.htm](equipment-effects/csA4UAD2tQq7RjT8.htm)|Effect: Tanglefoot Bag (Greater)|auto-trad|
|[Cxa7MdgMCUoMqbKm.htm](equipment-effects/Cxa7MdgMCUoMqbKm.htm)|Effect: Bronze Bull Pendant|auto-trad|
|[cy42NXgx1vjYzSxN.htm](equipment-effects/cy42NXgx1vjYzSxN.htm)|Effect: Suit of Armoire Frustration|auto-trad|
|[d7BDxmsnM1BUoEeT.htm](equipment-effects/d7BDxmsnM1BUoEeT.htm)|Effect: Goggles of Night (Greater)|auto-trad|
|[D7teqZ68L21aZCpd.htm](equipment-effects/D7teqZ68L21aZCpd.htm)|Effect: Glittering Snare (Failure)|auto-trad|
|[dchlrZqQ2oEmgNlN.htm](equipment-effects/dchlrZqQ2oEmgNlN.htm)|Effect: Silkspinner's Shield (Animated Strike)|auto-trad|
|[DfAyZW2vkhTygZVC.htm](equipment-effects/DfAyZW2vkhTygZVC.htm)|Effect: Prepared Camouflage Suit (Superb)|auto-trad|
|[DlqcczhwjfaEf7G1.htm](equipment-effects/DlqcczhwjfaEf7G1.htm)|Effect: Ablative Armor Plating (Greater)|auto-trad|
|[dpIrjd1UPY7EnWUD.htm](equipment-effects/dpIrjd1UPY7EnWUD.htm)|Effect: Silvertongue Mutagen (Lesser)|auto-trad|
|[dv0IKm5syOdP759w.htm](equipment-effects/dv0IKm5syOdP759w.htm)|Effect: Frost Vial (Moderate)|auto-trad|
|[E2uy6gqOXi1HRVBU.htm](equipment-effects/E2uy6gqOXi1HRVBU.htm)|Effect: Clockwork Goggles (Greater)|auto-trad|
|[e3RzlURndODzBnMt.htm](equipment-effects/e3RzlURndODzBnMt.htm)|Effect: Grit (Stage 1)|auto-trad|
|[E4B02mJmNexQLa8F.htm](equipment-effects/E4B02mJmNexQLa8F.htm)|Effect: Inspiring Spotlight|auto-trad|
|[e6dXfbKzv5sNr1zh.htm](equipment-effects/e6dXfbKzv5sNr1zh.htm)|Effect: Vermin Repellent Agent (Major)|auto-trad|
|[Ee2xfKX1yyqGIDZj.htm](equipment-effects/Ee2xfKX1yyqGIDZj.htm)|Effect: Treat Disease (Success)|auto-trad|
|[eeGWTG9ZAha4IIOY.htm](equipment-effects/eeGWTG9ZAha4IIOY.htm)|Effect: Cloak of Elvenkind|auto-trad|
|[eh7EqmDBDW30ShCu.htm](equipment-effects/eh7EqmDBDW30ShCu.htm)|Effect: Bravo's Brew (Lesser)|auto-trad|
|[ehYmO1rFBt35zoOw.htm](equipment-effects/ehYmO1rFBt35zoOw.htm)|Effect: Server's Stew|auto-trad|
|[eNVSBXuOiAaN152C.htm](equipment-effects/eNVSBXuOiAaN152C.htm)|Effect: Energized Cartridge (Electricity)|auto-trad|
|[EpB7yJPEuG6ez4z3.htm](equipment-effects/EpB7yJPEuG6ez4z3.htm)|Effect: Elixir of Life (Lesser)|auto-trad|
|[EpNflrkmWzQ0lEb4.htm](equipment-effects/EpNflrkmWzQ0lEb4.htm)|Effect: Glaive of the Artist|auto-trad|
|[eQqi3tWSHwV4SHqK.htm](equipment-effects/eQqi3tWSHwV4SHqK.htm)|Effect: South Wind's Scorch Song (Speed Boost)|auto-trad|
|[eSIYyxi6uTKiP6W5.htm](equipment-effects/eSIYyxi6uTKiP6W5.htm)|Effect: Improvised Weapon|auto-trad|
|[ESuBosh3t1pXEcBj.htm](equipment-effects/ESuBosh3t1pXEcBj.htm)|Effect: Treat Poison (Critical Failure)|auto-trad|
|[etJW0w4CiSFgMrWP.htm](equipment-effects/etJW0w4CiSFgMrWP.htm)|Effect: Aeon Stone (Orange Prism) (Nature)|auto-trad|
|[exwQF6E1FWmuxwBc.htm](equipment-effects/exwQF6E1FWmuxwBc.htm)|Effect: Protective Barrier|auto-trad|
|[f2U0pvTwqrLYyOlC.htm](equipment-effects/f2U0pvTwqrLYyOlC.htm)|Effect: Azarim|auto-trad|
|[F8nQOLVWmpp9G5hZ.htm](equipment-effects/F8nQOLVWmpp9G5hZ.htm)|Effect: Dragon's Blood Pudding (Greater)|auto-trad|
|[FbFl95WRpzrrijh3.htm](equipment-effects/FbFl95WRpzrrijh3.htm)|Effect: Aeon Stone (Orange Prism) (Religion)|auto-trad|
|[fbSFwwp60AuDDKpK.htm](equipment-effects/fbSFwwp60AuDDKpK.htm)|Effect: Belt of the Five Kings (Allies)|auto-trad|
|[fIpzDpuwLdIS4tW5.htm](equipment-effects/fIpzDpuwLdIS4tW5.htm)|Effect: Bestial Mutagen (Lesser)|auto-trad|
|[Fngb79C1VDGLJ1EQ.htm](equipment-effects/Fngb79C1VDGLJ1EQ.htm)|Effect: Feyfoul (Lesser)|auto-trad|
|[FOZXp7QQDnny1600.htm](equipment-effects/FOZXp7QQDnny1600.htm)|Effect: Fire and Iceberg|auto-trad|
|[fRlvmul3LbLo2xvR.htm](equipment-effects/fRlvmul3LbLo2xvR.htm)|Effect: Parry|auto-trad|
|[fuQVJiPPUsvL6fi5.htm](equipment-effects/fuQVJiPPUsvL6fi5.htm)|Effect: Sulfur Bomb (Failure)|auto-trad|
|[fUrZ4xcMJz0CfTyG.htm](equipment-effects/fUrZ4xcMJz0CfTyG.htm)|Effect: Juggernaut Mutagen (Moderate)|auto-trad|
|[fYe48HmFgfmcqbvL.htm](equipment-effects/fYe48HmFgfmcqbvL.htm)|Effect: Taljjae's Mask (The Hero)|auto-trad|
|[fYjvLx9DHIdCHdDx.htm](equipment-effects/fYjvLx9DHIdCHdDx.htm)|Effect: Applereed Mutagen (Moderate)|auto-trad|
|[fYZIanbYu0Vc4JEL.htm](equipment-effects/fYZIanbYu0Vc4JEL.htm)|Effect: Tanglefoot Bag (Lesser)|auto-trad|
|[Fz3cSffzDAxhCh2D.htm](equipment-effects/Fz3cSffzDAxhCh2D.htm)|Effect: Exsanguinating Ammunition|auto-trad|
|[G0lG7IIZnCZtYi6v.htm](equipment-effects/G0lG7IIZnCZtYi6v.htm)|Effect: Breastplate of Command|auto-trad|
|[g8JS6wsw5sRWOJLg.htm](equipment-effects/g8JS6wsw5sRWOJLg.htm)|Effect: Stalk Goggles|auto-trad|
|[GBBjw61g4ekJymT0.htm](equipment-effects/GBBjw61g4ekJymT0.htm)|Effect: Drakeheart Mutagen (Lesser)|auto-trad|
|[gDefAEEMXVVZgqXH.htm](equipment-effects/gDefAEEMXVVZgqXH.htm)|Effect: Celestial Armor|auto-trad|
|[Gj6u2Za5okFlsTvT.htm](equipment-effects/Gj6u2Za5okFlsTvT.htm)|Effect: Deadweight Snare (Failure/Critical Failure)|auto-trad|
|[GNFNDyx8nfNXrgV6.htm](equipment-effects/GNFNDyx8nfNXrgV6.htm)|Effect: Glittering Snare (Critical Failure)|auto-trad|
|[h0Zh8tDF9zJBHZXA.htm](equipment-effects/h0Zh8tDF9zJBHZXA.htm)|Effect: Flaming Star - Weapon (Major)|auto-trad|
|[H29JukjrSpHe5DXR.htm](equipment-effects/H29JukjrSpHe5DXR.htm)|Effect: Impossible Cake|auto-trad|
|[haywlcUtG6hV1LAy.htm](equipment-effects/haywlcUtG6hV1LAy.htm)|Effect: Trinity Geode - Armor (Greater)|auto-trad|
|[HaZ5LB1wh1LY5wUy.htm](equipment-effects/HaZ5LB1wh1LY5wUy.htm)|Effect: Potion of Minute Echoes|auto-trad|
|[hD0dUWYKM8FrVDZY.htm](equipment-effects/hD0dUWYKM8FrVDZY.htm)|Effect: Crown of the Kobold King|auto-trad|
|[HeRHBo2NaKy5IxhU.htm](equipment-effects/HeRHBo2NaKy5IxhU.htm)|Effect: Antiplague (Moderate)|auto-trad|
|[Hnt3Trd7TiFICB06.htm](equipment-effects/Hnt3Trd7TiFICB06.htm)|Effect: Vermin Repellent Agent (Moderate)|auto-trad|
|[HoZWmT4yvGso7pHM.htm](equipment-effects/HoZWmT4yvGso7pHM.htm)|Effect: Flaming Star - Weapon (Greater)|auto-trad|
|[hPxrIpuL54XRlA2h.htm](equipment-effects/hPxrIpuL54XRlA2h.htm)|Effect: Earplugs|auto-trad|
|[Hx4MOTujp5z6SlQu.htm](equipment-effects/Hx4MOTujp5z6SlQu.htm)|Effect: Arboreal's Revenge (Speed Penalty)|auto-trad|
|[hy6LAC13QIJNDYXm.htm](equipment-effects/hy6LAC13QIJNDYXm.htm)|Effect: South Wind's Scorch Song (Damage)|auto-trad|
|[i0tm2ZHekp7rGGR3.htm](equipment-effects/i0tm2ZHekp7rGGR3.htm)|Effect: Stole of Civility|auto-trad|
|[id20P4pj7zDKeLmy.htm](equipment-effects/id20P4pj7zDKeLmy.htm)|Effect: Treat Disease (Critical Success)|auto-trad|
|[iEkH8BKLMUa2wxLX.htm](equipment-effects/iEkH8BKLMUa2wxLX.htm)|Effect: Glamorous Buckler|auto-trad|
|[iK6JeCsZwm5Vakks.htm](equipment-effects/iK6JeCsZwm5Vakks.htm)|Effect: Anklets of Alacrity|auto-trad|
|[IlTS2LTwYTyGXY49.htm](equipment-effects/IlTS2LTwYTyGXY49.htm)|Effect: Feyfoul (Moderate)|auto-trad|
|[ioGzmVSmMGXWWBYb.htm](equipment-effects/ioGzmVSmMGXWWBYb.htm)|Effect: Cloak of the Bat|auto-trad|
|[ITAFsW3dQPupJ3DW.htm](equipment-effects/ITAFsW3dQPupJ3DW.htm)|Effect: Tanglefoot Bag (Major)|auto-trad|
|[IZkHdaqWBJIIWO7F.htm](equipment-effects/IZkHdaqWBJIIWO7F.htm)|Effect: Ebon Fulcrum Lens (Reaction)|auto-trad|
|[J0YS8mQsQ1BmT6Xv.htm](equipment-effects/J0YS8mQsQ1BmT6Xv.htm)|Effect: Emberheart|auto-trad|
|[j9zVZwRBVAcnpEkE.htm](equipment-effects/j9zVZwRBVAcnpEkE.htm)|Effect: Cheetah's Elixir (Moderate)|auto-trad|
|[Ja3PlqLuD9aSaPNZ.htm](equipment-effects/Ja3PlqLuD9aSaPNZ.htm)|Effect: Brewer's Regret (Greater)|auto-trad|
|[jaBMZKdoywOTrQvP.htm](equipment-effects/jaBMZKdoywOTrQvP.htm)|Effect: Cognitive Mutagen (Lesser)|auto-trad|
|[JbJykktAYMR4BRav.htm](equipment-effects/JbJykktAYMR4BRav.htm)|Effect: Energy Mutagen (Greater)|auto-trad|
|[jgaDboqENQJaS1sW.htm](equipment-effects/jgaDboqENQJaS1sW.htm)|Effect: Prepared Camouflage Suit|auto-trad|
|[jlVYoiPVRRVGBj5G.htm](equipment-effects/jlVYoiPVRRVGBj5G.htm)|Effect: Ablative Armor Plating (Lesser)|auto-trad|
|[JnnyamqQrAEcyI6F.htm](equipment-effects/JnnyamqQrAEcyI6F.htm)|Effect: Grim Sandglass - Armor (Greater)|auto-trad|
|[jw6Tr9FbErjLAFLQ.htm](equipment-effects/jw6Tr9FbErjLAFLQ.htm)|Effect: Serene Mutagen (Greater)|auto-trad|
|[K21XQMoDVSPqzRla.htm](equipment-effects/K21XQMoDVSPqzRla.htm)|Effect: Sage's Lash (Turquoise)|auto-trad|
|[KAEWiyE8TQwofNj9.htm](equipment-effects/KAEWiyE8TQwofNj9.htm)|Effect: Impossible Cake (Greater)|auto-trad|
|[KFnOWk5e7nwXT8IE.htm](equipment-effects/KFnOWk5e7nwXT8IE.htm)|Effect: Feyfoul (Greater)|auto-trad|
|[kgEOxqF1q4Sy6r97.htm](equipment-effects/kgEOxqF1q4Sy6r97.htm)|Effect: Flaming Star - Armor (Major)|auto-trad|
|[kgotU0sFmtAHYySB.htm](equipment-effects/kgotU0sFmtAHYySB.htm)|Effect: Eagle Eye Elixir (Greater)|auto-trad|
|[KiurLemTV8GV7OyM.htm](equipment-effects/KiurLemTV8GV7OyM.htm)|Effect: Rowan Rifle (Sonic)|auto-trad|
|[kkDbalYEavzRpYlp.htm](equipment-effects/kkDbalYEavzRpYlp.htm)|Effect: Antiplague (Lesser)|auto-trad|
|[kMPPl4AqFb6GclOL.htm](equipment-effects/kMPPl4AqFb6GclOL.htm)|Effect: Malleable Mixture (Greater)|auto-trad|
|[KSvkfMqMQ8mlGLiz.htm](equipment-effects/KSvkfMqMQ8mlGLiz.htm)|Effect: Goggles of Night|auto-trad|
|[kwD0wuW5Ndkc9YXB.htm](equipment-effects/kwD0wuW5Ndkc9YXB.htm)|Effect: Bestial Mutagen (Greater)|auto-trad|
|[kwOtHtmlH69ctK0O.htm](equipment-effects/kwOtHtmlH69ctK0O.htm)|Effect: Sun Orchid Poultice|auto-trad|
|[kyLLXUQ9zSEvC4py.htm](equipment-effects/kyLLXUQ9zSEvC4py.htm)|Effect: Stalk Goggles (Greater)|auto-trad|
|[lBMhT2W2raYMa8JS.htm](equipment-effects/lBMhT2W2raYMa8JS.htm)|Effect: Spellguard Shield|auto-trad|
|[lgvjbbQiHBGKR3C6.htm](equipment-effects/lgvjbbQiHBGKR3C6.htm)|Effect: Rhino Shot|auto-trad|
|[LH0IDLLF4RsT3KvM.htm](equipment-effects/LH0IDLLF4RsT3KvM.htm)|Effect: Energized Cartridge (Fire)|auto-trad|
|[lLP56tbG689TNKW5.htm](equipment-effects/lLP56tbG689TNKW5.htm)|Effect: Bracelet of Dashing|auto-trad|
|[lNWACCNe9RYgaFxb.htm](equipment-effects/lNWACCNe9RYgaFxb.htm)|Effect: Cheetah's Elixir (Lesser)|auto-trad|
|[lO95TwgihBdTilAB.htm](equipment-effects/lO95TwgihBdTilAB.htm)|Effect: Thurible of Revelation|auto-trad|
|[lPRuIRbu0rHBkoKY.htm](equipment-effects/lPRuIRbu0rHBkoKY.htm)|Effect: Elixir of Life (Minor)|auto-trad|
|[LVy8SfUF8Jrd6X18.htm](equipment-effects/LVy8SfUF8Jrd6X18.htm)|Effect: Leopard's Armor|auto-trad|
|[M0hhLRC86sASVOk7.htm](equipment-effects/M0hhLRC86sASVOk7.htm)|Effect: Tteokguk of Time Advancement|auto-trad|
|[M3EFomnN5xArdQmV.htm](equipment-effects/M3EFomnN5xArdQmV.htm)|Effect: Moderate Healer's Gel|auto-trad|
|[m4WpxepWRV1u1Kcw.htm](equipment-effects/m4WpxepWRV1u1Kcw.htm)|Effect: Grim Sandglass - Weapon|auto-trad|
|[MCny5ohCGf09a7Wl.htm](equipment-effects/MCny5ohCGf09a7Wl.htm)|Effect: Salve of Slipperiness|auto-trad|
|[MEreOgnjoRiXPEuq.htm](equipment-effects/MEreOgnjoRiXPEuq.htm)|Effect: Tanglefoot Bag (Moderate)|auto-trad|
|[Mf9EBLhYmZerf0nS.htm](equipment-effects/Mf9EBLhYmZerf0nS.htm)|Effect: Potion of Flying (Standard)|auto-trad|
|[mG6S6zm6hxaF7Tla.htm](equipment-effects/mG6S6zm6hxaF7Tla.htm)|Effect: Skeptic's Elixir (Moderate)|auto-trad|
|[mi4Md1fB2XThCand.htm](equipment-effects/mi4Md1fB2XThCand.htm)|Effect: Antidote (Moderate)|auto-trad|
|[MI5OCkF9IXmD2lPF.htm](equipment-effects/MI5OCkF9IXmD2lPF.htm)|Effect: Bloodhound Mask (Greater)|auto-trad|
|[mkjcgwDBeaOUolVe.htm](equipment-effects/mkjcgwDBeaOUolVe.htm)|Effect: Major Fanged Rune Animal Form|auto-trad|
|[mn39aML7EWKbttKT.htm](equipment-effects/mn39aML7EWKbttKT.htm)|Effect: Ablative Armor Plating (Moderate)|auto-trad|
|[ModBoFdCi7YQU4gP.htm](equipment-effects/ModBoFdCi7YQU4gP.htm)|Effect: Potion of Swimming (Greater)|auto-trad|
|[mQJk8R0vHzvpTz0e.htm](equipment-effects/mQJk8R0vHzvpTz0e.htm)|Effect: Chatterer of Follies|auto-trad|
|[mrwg2XftLtSLj197.htm](equipment-effects/mrwg2XftLtSLj197.htm)|Effect: Exsanguinating Ammunition (Major)|auto-trad|
|[na2gf5mSkilFoHXk.htm](equipment-effects/na2gf5mSkilFoHXk.htm)|Effect: Energy Mutagen (Moderate)|auto-trad|
|[Nbgf8zvHimdQqIu6.htm](equipment-effects/Nbgf8zvHimdQqIu6.htm)|Effect: Skyrider Sword|auto-trad|
|[NdfhpKCjSS80LiUz.htm](equipment-effects/NdfhpKCjSS80LiUz.htm)|Effect: Nosoi Charm (Greater) (Diplomacy)|auto-trad|
|[NE7Fm5YnUhD4ySX3.htm](equipment-effects/NE7Fm5YnUhD4ySX3.htm)|Effect: Earplugs (PFS Guide)|auto-trad|
|[neZPoQF4hW3A31dd.htm](equipment-effects/neZPoQF4hW3A31dd.htm)|Effect: Lastwall Soup (Greater)|auto-trad|
|[nJRoiSyd67eQ1dYj.htm](equipment-effects/nJRoiSyd67eQ1dYj.htm)|Effect: Frost Vial (Greater)|auto-trad|
|[no7vnIiNBwWjh3w8.htm](equipment-effects/no7vnIiNBwWjh3w8.htm)|Effect: Grasp of Droskar|auto-trad|
|[nQ6vM1CRLyvQdGLG.htm](equipment-effects/nQ6vM1CRLyvQdGLG.htm)|Effect: Five-Feather Wreath - Armor|auto-trad|
|[NYOi1F9cW3axHrdc.htm](equipment-effects/NYOi1F9cW3axHrdc.htm)|Effect: Deadweight Snare (Success)|auto-trad|
|[oAewXfq9c0ecaSfw.htm](equipment-effects/oAewXfq9c0ecaSfw.htm)|Effect: Silvertongue Mutagen (Greater)|auto-trad|
|[OAN5Fj21PJPhIqRU.htm](equipment-effects/OAN5Fj21PJPhIqRU.htm)|Effect: Vermin Repellent Agent (Lesser)|auto-trad|
|[oaR6YGiZKg8a2971.htm](equipment-effects/oaR6YGiZKg8a2971.htm)|Effect: Clockwork Goggles|auto-trad|
|[ohMdE8BmQHuLs40b.htm](equipment-effects/ohMdE8BmQHuLs40b.htm)|Effect: Impossible Cake (Major)|auto-trad|
|[oiO3cQfqp8MuxR82.htm](equipment-effects/oiO3cQfqp8MuxR82.htm)|Effect: Blast Boots (Major)|auto-trad|
|[OMW71UJzYCUr4ubh.htm](equipment-effects/OMW71UJzYCUr4ubh.htm)|Effect: Ablative Armor Plating (Major)|auto-trad|
|[oqwrw6XztVlS9tEG.htm](equipment-effects/oqwrw6XztVlS9tEG.htm)|Effect: Trinity Geode - Armor|auto-trad|
|[ORdhj3IAvYACNGkJ.htm](equipment-effects/ORdhj3IAvYACNGkJ.htm)|Effect: Shrine Inarizushi|auto-trad|
|[OxCVZSvWVJsOGAZN.htm](equipment-effects/OxCVZSvWVJsOGAZN.htm)|Effect: Flaming Star - Weapon|auto-trad|
|[p2aGtovaY1feytws.htm](equipment-effects/p2aGtovaY1feytws.htm)|Effect: Aeon Stone (Black Pearl)|auto-trad|
|[P7Y7pO2ulZ5wBgxU.htm](equipment-effects/P7Y7pO2ulZ5wBgxU.htm)|Effect: Barding of the Zephyr|auto-trad|
|[p9jROgkqozXB52UJ.htm](equipment-effects/p9jROgkqozXB52UJ.htm)|Effect: Trinity Geode - Weapon (Greater)|auto-trad|
|[pAMyEbJzWBoYoGhs.htm](equipment-effects/pAMyEbJzWBoYoGhs.htm)|Effect: Diplomat's Badge|auto-trad|
|[PeiuJ951kkBPTCSM.htm](equipment-effects/PeiuJ951kkBPTCSM.htm)|Effect: Bracers of Missile Deflection|auto-trad|
|[PEPOd38VfVzQMKG5.htm](equipment-effects/PEPOd38VfVzQMKG5.htm)|Effect: Stone Body Mutagen (Lesser)|auto-trad|
|[PeuUz7JaabCgl6Yh.htm](equipment-effects/PeuUz7JaabCgl6Yh.htm)|Effect: Cheetah's Elixir (Greater)|auto-trad|
|[Pkk8m79MoT1RgtfW.htm](equipment-effects/Pkk8m79MoT1RgtfW.htm)|Effect: Succubus Kiss (Stage 1)|auto-trad|
|[pmWJxDjz3gqL29OM.htm](equipment-effects/pmWJxDjz3gqL29OM.htm)|Effect: Bottled Omen|auto-trad|
|[PpLxndUSgzgs6dd0.htm](equipment-effects/PpLxndUSgzgs6dd0.htm)|Effect: Elixir of Life (Major)|auto-trad|
|[PuWZyFzJCkbq1Inj.htm](equipment-effects/PuWZyFzJCkbq1Inj.htm)|Effect: Flaming Star - Armor|auto-trad|
|[q1EhQ716bPSgJVnC.htm](equipment-effects/q1EhQ716bPSgJVnC.htm)|Effect: Bravo's Brew (Greater)|auto-trad|
|[q58ahUEjUzTXffRN.htm](equipment-effects/q58ahUEjUzTXffRN.htm)|Effect: Perfect Droplet - Armor (Greater)|auto-trad|
|[QapoFh0tbUgMwSIB.htm](equipment-effects/QapoFh0tbUgMwSIB.htm)|Effect: Thurible of Revelation (Greater)|auto-trad|
|[qit1mLbJUyRTYcPU.htm](equipment-effects/qit1mLbJUyRTYcPU.htm)|Effect: Cognitive Mutagen (Greater)|auto-trad|
|[qOBdeZ4FXYc5qHsm.htm](equipment-effects/qOBdeZ4FXYc5qHsm.htm)|Effect: Private Workshop (Using for Crafting)|auto-trad|
|[QrsPKOFuo3qzgxw5.htm](equipment-effects/QrsPKOFuo3qzgxw5.htm)|Effect: Red-Rib Gill Mask (Greater)|auto-trad|
|[QuZ5frBMJF3gi7RY.htm](equipment-effects/QuZ5frBMJF3gi7RY.htm)|Effect: Antidote (Greater)|auto-trad|
|[qVKrrKpTghgMIuGH.htm](equipment-effects/qVKrrKpTghgMIuGH.htm)|Effect: Antiplague (Major)|auto-trad|
|[qwoLV4awdezlEJ60.htm](equipment-effects/qwoLV4awdezlEJ60.htm)|Effect: Drakeheart Mutagen (Greater)|auto-trad|
|[QXJLvL2k3WqlF0SN.htm](equipment-effects/QXJLvL2k3WqlF0SN.htm)|Effect: Grim Sandglass - Armor (Major)|auto-trad|
|[qzRcSQ0HTTp58hV2.htm](equipment-effects/qzRcSQ0HTTp58hV2.htm)|Effect: Sixfingers Elixir (Moderate)|auto-trad|
|[R106i7WCXvHLGMTu.htm](equipment-effects/R106i7WCXvHLGMTu.htm)|Effect: Antiplague (Greater)|auto-trad|
|[R1kZsBMZdGZ3ATkA.htm](equipment-effects/R1kZsBMZdGZ3ATkA.htm)|Effect: Flaming Star - Armor (Greater)|auto-trad|
|[R5ugeFK3MPwkbv0s.htm](equipment-effects/R5ugeFK3MPwkbv0s.htm)|Effect: Potency Crystal|auto-trad|
|[r6hDgfVLod0AmU7J.htm](equipment-effects/r6hDgfVLod0AmU7J.htm)|Effect: Heartripper Blade|auto-trad|
|[rH6RHxy6sNTLusKX.htm](equipment-effects/rH6RHxy6sNTLusKX.htm)|Effect: Emerald Fulcrum Lens (Saving Throw)|auto-trad|
|[RIozNOntRJok5ZJt.htm](equipment-effects/RIozNOntRJok5ZJt.htm)|Effect: Energy Mutagen (Major)|auto-trad|
|[RLsdvhmTh64Mmty9.htm](equipment-effects/RLsdvhmTh64Mmty9.htm)|Effect: Frost Vial (Lesser)|auto-trad|
|[RRusoN3HEGnDO1Dg.htm](equipment-effects/RRusoN3HEGnDO1Dg.htm)|Effect: Sea Touch Elixir (Greater)|auto-trad|
|[RT1BxXrbbGgk40Ti.htm](equipment-effects/RT1BxXrbbGgk40Ti.htm)|Effect: Cognitive Mutagen (Major)|auto-trad|
|[rVFDLzYrJVYLiQBL.htm](equipment-effects/rVFDLzYrJVYLiQBL.htm)|Effect: Qat (Stage 1)|auto-trad|
|[rXM6njevpwqSMNRt.htm](equipment-effects/rXM6njevpwqSMNRt.htm)|Effect: Tallowheart Mass|auto-trad|
|[S3Sv7SYwxozbG554.htm](equipment-effects/S3Sv7SYwxozbG554.htm)|Effect: War Blood Mutagen (Major)|auto-trad|
|[S4MZzALqFoXJsr6o.htm](equipment-effects/S4MZzALqFoXJsr6o.htm)|Effect: Bloodhound Mask (Lesser)|auto-trad|
|[s95P3L72BDKvzYhn.htm](equipment-effects/s95P3L72BDKvzYhn.htm)|Effect: Curse of Potent Poison|auto-trad|
|[SbYcOry1cxbndSve.htm](equipment-effects/SbYcOry1cxbndSve.htm)|Effect: Silkspinner's Shield (Climb)|auto-trad|
|[sOwAqyQ6MaoSqaY1.htm](equipment-effects/sOwAqyQ6MaoSqaY1.htm)|Effect: Lesser Healer's Gel|auto-trad|
|[SqN1FGSgdNlyvRu9.htm](equipment-effects/SqN1FGSgdNlyvRu9.htm)|Effect: Containment Contraption|auto-trad|
|[T27uDXdMVc5ZFwKw.htm](equipment-effects/T27uDXdMVc5ZFwKw.htm)|Effect: Enhanced Hearing Aids|auto-trad|
|[t7VUJHSUT6bkVUjg.htm](equipment-effects/t7VUJHSUT6bkVUjg.htm)|Effect: Serene Mutagen (Major)|auto-trad|
|[tcHG8NlsYmHdziko.htm](equipment-effects/tcHG8NlsYmHdziko.htm)|Effect: Grim Sandglass - Weapon (Major)|auto-trad|
|[tGeMT4iHJcsjVbl3.htm](equipment-effects/tGeMT4iHJcsjVbl3.htm)|Effect: Metuak's Pendant|auto-trad|
|[thOpQunbQr77XWdF.htm](equipment-effects/thOpQunbQr77XWdF.htm)|Effect: Sea Touch Elixir (Lesser)|auto-trad|
|[Tioloj3bTlFnQDqa.htm](equipment-effects/Tioloj3bTlFnQDqa.htm)|Effect: Perfect Droplet - Armor (Major)|auto-trad|
|[TkRuKKYyPHTGPfgf.htm](equipment-effects/TkRuKKYyPHTGPfgf.htm)|Effect: Sixfingers Elixir (Greater)|auto-trad|
|[tLGzSCcfxflLSqzw.htm](equipment-effects/tLGzSCcfxflLSqzw.htm)|Effect: Energized Cartridge (Cold)|auto-trad|
|[TsWUTODTVi487SEz.htm](equipment-effects/TsWUTODTVi487SEz.htm)|Effect: Skeptic's Elixir (Greater)|auto-trad|
|[tTBJ33UGtzXjWOJp.htm](equipment-effects/tTBJ33UGtzXjWOJp.htm)|Effect: Applereed Mutagen (Greater)|auto-trad|
|[u35Qzft0c84UySq2.htm](equipment-effects/u35Qzft0c84UySq2.htm)|Effect: Ebon Fulcrum Lens (2 Action)|auto-trad|
|[u7200u7lh40am0jb.htm](equipment-effects/u7200u7lh40am0jb.htm)|Effect: Trinity Geode - Weapon (Major)|auto-trad|
|[Uadsb25G18pKdZ2e.htm](equipment-effects/Uadsb25G18pKdZ2e.htm)|Effect: Clandestine Cloak|auto-trad|
|[uC6KjfiWrTBXYtP8.htm](equipment-effects/uC6KjfiWrTBXYtP8.htm)|Effect: Storm Chair|auto-trad|
|[UDfVCATxdLdSzJYJ.htm](equipment-effects/UDfVCATxdLdSzJYJ.htm)|Effect: Red-Rib Gill Mask (Lesser)|auto-trad|
|[UlalLihKzDxcOdXL.htm](equipment-effects/UlalLihKzDxcOdXL.htm)|Effect: Thurible of Revelation (Moderate)|auto-trad|
|[UTtX0xLGYci6P43I.htm](equipment-effects/UTtX0xLGYci6P43I.htm)|Effect: Mudrock Snare (Failure)|auto-trad|
|[uVxs1qFMQsGWXNs6.htm](equipment-effects/uVxs1qFMQsGWXNs6.htm)|Effect: Potion of Stable Form|auto-trad|
|[uXEp1rPU5fY4OiBf.htm](equipment-effects/uXEp1rPU5fY4OiBf.htm)|Effect: Clandestine Cloak (Greater)|auto-trad|
|[UzSrsR9S2pgMDbbp.htm](equipment-effects/UzSrsR9S2pgMDbbp.htm)|Effect: Cape of the Open Sky|auto-trad|
|[V4JoVnOfKze8cRan.htm](equipment-effects/V4JoVnOfKze8cRan.htm)|Effect: Grim Sandglass - Armor|auto-trad|
|[v5Ht1V4MZvRKRBjL.htm](equipment-effects/v5Ht1V4MZvRKRBjL.htm)|Effect: Silvertongue Mutagen (Moderate)|auto-trad|
|[VCypzSu659eC6jNi.htm](equipment-effects/VCypzSu659eC6jNi.htm)|Effect: Eagle Eye Elixir (Lesser)|auto-trad|
|[vFOr2JAJxiVvvn2E.htm](equipment-effects/vFOr2JAJxiVvvn2E.htm)|Effect: Drakeheart Mutagen (Major)|auto-trad|
|[vH4bEu3EnAhNpKEQ.htm](equipment-effects/vH4bEu3EnAhNpKEQ.htm)|Effect: Dragonscale Amulet|auto-trad|
|[viCX9fZzTWGuoO85.htm](equipment-effects/viCX9fZzTWGuoO85.htm)|Effect: Cloak of Elvenkind (Greater)|auto-trad|
|[VIzeuA9tQEQ7V1Ib.htm](equipment-effects/VIzeuA9tQEQ7V1Ib.htm)|Effect: Wand of Fey Flames|auto-trad|
|[vj2hkcSbwwRYNLk5.htm](equipment-effects/vj2hkcSbwwRYNLk5.htm)|Effect: Jack's Tattered Cape|auto-trad|
|[vKooOkXHvtqCgZYg.htm](equipment-effects/vKooOkXHvtqCgZYg.htm)|Effect: Energized Cartridge (Acid)|auto-trad|
|[VlfuBfWkygsG8u5h.htm](equipment-effects/VlfuBfWkygsG8u5h.htm)|Effect: Blaze|auto-trad|
|[vOgD9wfStLX1utte.htm](equipment-effects/vOgD9wfStLX1utte.htm)|Effect: Skyrider Sword (Greater)|auto-trad|
|[vOU4Yv2MyAfYBbmF.htm](equipment-effects/vOU4Yv2MyAfYBbmF.htm)|Effect: Aeon Stone (Orange Prism) (Occultism)|auto-trad|
|[VPtsrpbP0AE642al.htm](equipment-effects/VPtsrpbP0AE642al.htm)|Effect: Quicksilver Mutagen (Moderate)|auto-trad|
|[VrYfR2WuyA15zFhq.htm](equipment-effects/VrYfR2WuyA15zFhq.htm)|Effect: Vermin Repellent Agent (Greater)|auto-trad|
|[VsHhBBLApZsOCJRL.htm](equipment-effects/VsHhBBLApZsOCJRL.htm)|Effect: Fire and Iceberg (Greater)|auto-trad|
|[VVWvXiNudYYGV9sJ.htm](equipment-effects/VVWvXiNudYYGV9sJ.htm)|Effect: Nosoi Charm (Lifesense)|auto-trad|
|[VZCcjwsQX1wnYlTn.htm](equipment-effects/VZCcjwsQX1wnYlTn.htm)|Effect: Perfect Droplet - Armor|auto-trad|
|[vZPyQAt5T2L0Dfmq.htm](equipment-effects/vZPyQAt5T2L0Dfmq.htm)|Effect: Topology Protoplasm|auto-trad|
|[w0LUnfS2whVhDBUF.htm](equipment-effects/w0LUnfS2whVhDBUF.htm)|Effect: Glittering Snare (Success)|auto-trad|
|[W3BCLbX6j1IqL0uB.htm](equipment-effects/W3BCLbX6j1IqL0uB.htm)|Effect: Slippers of Spider Climbing|auto-trad|
|[W3xQBLj5hLOtb6Tj.htm](equipment-effects/W3xQBLj5hLOtb6Tj.htm)|Effect: Potion of Swimming (Moderate)|auto-trad|
|[W9tKQlA7tVIcAuzw.htm](equipment-effects/W9tKQlA7tVIcAuzw.htm)|Effect: Greater Potion of Stable Form|auto-trad|
|[Wa4317cqU4lJ8vAQ.htm](equipment-effects/Wa4317cqU4lJ8vAQ.htm)|Effect: Eagle Eye Elixir (Moderate)|auto-trad|
|[wacGBDbbQ1HaNZbX.htm](equipment-effects/wacGBDbbQ1HaNZbX.htm)|Effect: Hyldarf's Fang|auto-trad|
|[wFF0SZs1Hcf87Kk1.htm](equipment-effects/wFF0SZs1Hcf87Kk1.htm)|Effect: Bloodhound Mask (Moderate)|auto-trad|
|[wFP3SqPoO0bCPmyK.htm](equipment-effects/wFP3SqPoO0bCPmyK.htm)|Effect: Kraken's Guard|auto-trad|
|[WHAp9cDOqnJ1VCcg.htm](equipment-effects/WHAp9cDOqnJ1VCcg.htm)|Effect: Orchestral Brooch|auto-trad|
|[WMKjWH4gyUrky4Hy.htm](equipment-effects/WMKjWH4gyUrky4Hy.htm)|Effect: Demon Dust (Stage 1)|auto-trad|
|[wNCxSxruzLVGtLE4.htm](equipment-effects/wNCxSxruzLVGtLE4.htm)|Effect: Spiderfoot Brew (Lesser)|auto-trad|
|[WQ0DxUzMgAvo0zy9.htm](equipment-effects/WQ0DxUzMgAvo0zy9.htm)|Effect: Apricot of Bestial Might|auto-trad|
|[WRV0XjiEHdlBpduS.htm](equipment-effects/WRV0XjiEHdlBpduS.htm)|Effect: Trinity Geode - Weapon|auto-trad|
|[wTZnKkT0K4Tdy8mD.htm](equipment-effects/wTZnKkT0K4Tdy8mD.htm)|Effect: Bravo's Brew (Moderate)|auto-trad|
|[WXrqEuLT4uP48Bvo.htm](equipment-effects/WXrqEuLT4uP48Bvo.htm)|Effect: Goggles of Night (Major)|auto-trad|
|[WXsWkFosSGBrptwF.htm](equipment-effects/WXsWkFosSGBrptwF.htm)|Effect: Ivory Baton|auto-trad|
|[wyLEew86nhNUXASu.htm](equipment-effects/wyLEew86nhNUXASu.htm)|Effect: Eagle Eye Elixir (Major)|auto-trad|
|[X2ZZgTqanpoCMDmd.htm](equipment-effects/X2ZZgTqanpoCMDmd.htm)|Effect: Taljjae's Mask (The General)|auto-trad|
|[xFQRiVU6h8EA6Lw9.htm](equipment-effects/xFQRiVU6h8EA6Lw9.htm)|Effect: Bestial Mutagen (Moderate)|auto-trad|
|[XKsqQrabRlg9klGp.htm](equipment-effects/XKsqQrabRlg9klGp.htm)|Effect: Draft of Stellar Radiance|auto-trad|
|[XlHbUOTbK6PfBfCv.htm](equipment-effects/XlHbUOTbK6PfBfCv.htm)|Effect: Demon Dust (Stage 2)|auto-trad|
|[xLilBqqf34ZJYO9i.htm](equipment-effects/xLilBqqf34ZJYO9i.htm)|Effect: Juggernaut Mutagen (Greater)|auto-trad|
|[XrlChFETfe8avLsX.htm](equipment-effects/XrlChFETfe8avLsX.htm)|Effect: Sixfingers Elixir (Lesser)|auto-trad|
|[xVAdPzFaSvJXPMKv.htm](equipment-effects/xVAdPzFaSvJXPMKv.htm)|Effect: Applereed Mutagen (Lesser)|auto-trad|
|[XwCBalKJf3CiEiFa.htm](equipment-effects/XwCBalKJf3CiEiFa.htm)|Effect: Treat Poison (Critical Success)|auto-trad|
|[XWenziR7J3mwKV4W.htm](equipment-effects/XWenziR7J3mwKV4W.htm)|Effect: Treat Poison (Success)|auto-trad|
|[XzxADtNpUlRff972.htm](equipment-effects/XzxADtNpUlRff972.htm)|Effect: Greater Fanged Rune Animal Form|auto-trad|
|[YI7QQqXO6nosaAKr.htm](equipment-effects/YI7QQqXO6nosaAKr.htm)|Effect: Spiderfoot Brew (Greater)|auto-trad|
|[yP45Rqu4jvCfXBkp.htm](equipment-effects/yP45Rqu4jvCfXBkp.htm)|Effect: Fire and Iceberg (Major)|auto-trad|
|[yrbz0rZzp8aZEqbv.htm](equipment-effects/yrbz0rZzp8aZEqbv.htm)|Effect: Serene Mutagen (Moderate)|auto-trad|
|[yt8meGTS7wLa6Fg2.htm](equipment-effects/yt8meGTS7wLa6Fg2.htm)|Effect: Ablative Armor Plating (True)|auto-trad|
|[ytqvHyF0oMKbo65P.htm](equipment-effects/ytqvHyF0oMKbo65P.htm)|Effect: Crimson Fulcrum Lens|auto-trad|
|[yvabfuAO74pvH8hh.htm](equipment-effects/yvabfuAO74pvH8hh.htm)|Effect: Aeon Stone (Orange Prism) (Arcana)|auto-trad|
|[Yxssrnh9UZJAM0V7.htm](equipment-effects/Yxssrnh9UZJAM0V7.htm)|Effect: Elixir of Life (Moderate)|auto-trad|
|[z3ATL8DcRVrT0Uzt.htm](equipment-effects/z3ATL8DcRVrT0Uzt.htm)|Effect: Disarm (Success)|auto-trad|
|[z8FvdsKEY4lB2L8b.htm](equipment-effects/z8FvdsKEY4lB2L8b.htm)|Effect: Phoenix Flask|auto-trad|
|[Z9oPh462q82IYIZ6.htm](equipment-effects/Z9oPh462q82IYIZ6.htm)|Effect: Elixir of Life (Greater)|auto-trad|
|[Zb8RYgmzCI6fQE0o.htm](equipment-effects/Zb8RYgmzCI6fQE0o.htm)|Effect: Throne Card|auto-trad|
|[zd85Ny1RS46OL0TD.htm](equipment-effects/zd85Ny1RS46OL0TD.htm)|Effect: Shrinking Potion (Greater)|auto-trad|
|[Zdh2uO1vVYJmaqld.htm](equipment-effects/Zdh2uO1vVYJmaqld.htm)|Effect: Potion of Flying (Greater)|auto-trad|
|[zDuuHVeHgd175pGf.htm](equipment-effects/zDuuHVeHgd175pGf.htm)|Effect: Succubus Kiss (Stage 2)|auto-trad|
|[zlSNbMDIlTOpcO8R.htm](equipment-effects/zlSNbMDIlTOpcO8R.htm)|Effect: Skinstitch Salve|auto-trad|
|[ZP9Uq4PVTgzJ3wEi.htm](equipment-effects/ZP9Uq4PVTgzJ3wEi.htm)|Effect: Five-Feather Wreath - Armor (Greater)|auto-trad|
|[zqKzWGLODgIvtiKf.htm](equipment-effects/zqKzWGLODgIvtiKf.htm)|Effect: Spellguard Blade|auto-trad|
|[ztxW3lBPRcesF7wK.htm](equipment-effects/ztxW3lBPRcesF7wK.htm)|Effect: Cognitive Mutagen (Moderate)|auto-trad|
|[ZV9rtVOD1eDTwcY4.htm](equipment-effects/ZV9rtVOD1eDTwcY4.htm)|Effect: Grit (Stage 3)|auto-trad|
|[zY7cemRcFD2zAVbC.htm](equipment-effects/zY7cemRcFD2zAVbC.htm)|Effect: Oath of the Devoted|auto-trad|
|[zZsdex5orF5Odpus.htm](equipment-effects/zZsdex5orF5Odpus.htm)|Effect: Mask of Allure|auto-trad|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[8YZX34sJOIH32VwI.htm](equipment-effects/8YZX34sJOIH32VwI.htm)|Effect: Illuminated Folio|vacía|
|[GqXIV46JqB8x8eEN.htm](equipment-effects/GqXIV46JqB8x8eEN.htm)|Effect: Book of Warding Prayers|vacía|
|[jlYPMOHplgkvzLa9.htm](equipment-effects/jlYPMOHplgkvzLa9.htm)|Effect: Standard of the Primeval Howl|vacía|
|[N54jx6GEz2NpGobK.htm](equipment-effects/N54jx6GEz2NpGobK.htm)|Effect: Breastplate of the Mountain|vacía|
|[PT1g0Ar47FVo2O4D.htm](equipment-effects/PT1g0Ar47FVo2O4D.htm)|Effect: Batsbreath Cane|vacía|
|[yykiQBIGqwxIDRZq.htm](equipment-effects/yykiQBIGqwxIDRZq.htm)|Effect: Viper Rapier|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
