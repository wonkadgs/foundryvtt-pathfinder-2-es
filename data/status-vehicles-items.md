# Estado de la traducción (vehicles-items)

 * **auto-trad**: 75


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0CcYSQlAiZcoMkF0.htm](vehicles-items/0CcYSQlAiZcoMkF0.htm)|Skewering Step|auto-trad|
|[0pWllu1H0c2ZTJbM.htm](vehicles-items/0pWllu1H0c2ZTJbM.htm)|Hauler|auto-trad|
|[2UISHdYECuDVkedh.htm](vehicles-items/2UISHdYECuDVkedh.htm)|Sluggish|auto-trad|
|[38Zgv0sAfLXH2dfB.htm](vehicles-items/38Zgv0sAfLXH2dfB.htm)|Wind-Up|auto-trad|
|[3Vt9tFjjJmSsJ99w.htm](vehicles-items/3Vt9tFjjJmSsJ99w.htm)|Wind-Up|auto-trad|
|[3WQ9vWhVa2yyJCcV.htm](vehicles-items/3WQ9vWhVa2yyJCcV.htm)|Luxurious Accommodations|auto-trad|
|[79YskaB7D1bAmIDD.htm](vehicles-items/79YskaB7D1bAmIDD.htm)|Volatile Flamethrower|auto-trad|
|[7LOtkjkF6S8p6CRA.htm](vehicles-items/7LOtkjkF6S8p6CRA.htm)|Tether Buoy|auto-trad|
|[7OqKezdHChnhLvmc.htm](vehicles-items/7OqKezdHChnhLvmc.htm)|Sluggish|auto-trad|
|[9UcPAPPrzEhaoWgY.htm](vehicles-items/9UcPAPPrzEhaoWgY.htm)|Portaged|auto-trad|
|[b3OQJLYv4hEX7yhn.htm](vehicles-items/b3OQJLYv4hEX7yhn.htm)|Long Reach|auto-trad|
|[Buxx8NC3JQzeLsjs.htm](vehicles-items/Buxx8NC3JQzeLsjs.htm)|Sluggish|auto-trad|
|[cFwKXyV5W3o1v3KK.htm](vehicles-items/cFwKXyV5W3o1v3KK.htm)|Adhesive Secretions|auto-trad|
|[ChjOaa11CkBJeVJ8.htm](vehicles-items/ChjOaa11CkBJeVJ8.htm)|Glow|auto-trad|
|[dabct2Mb2XcbWKwb.htm](vehicles-items/dabct2Mb2XcbWKwb.htm)|Maneuverable|auto-trad|
|[DoCusD26UNQbmFNI.htm](vehicles-items/DoCusD26UNQbmFNI.htm)|Continous Track|auto-trad|
|[eMf9xcynjFjF5ma8.htm](vehicles-items/eMf9xcynjFjF5ma8.htm)|Wind-Up|auto-trad|
|[eXdF4IskaccqgTpS.htm](vehicles-items/eXdF4IskaccqgTpS.htm)|Survey|auto-trad|
|[fG2IE4D8GEe8e98D.htm](vehicles-items/fG2IE4D8GEe8e98D.htm)|Hover|auto-trad|
|[gafl9oZCmfSGtfxj.htm](vehicles-items/gafl9oZCmfSGtfxj.htm)|Hopper|auto-trad|
|[Gy4AuNFAGBzfjRFq.htm](vehicles-items/Gy4AuNFAGBzfjRFq.htm)|Smog|auto-trad|
|[hCwx6Q30DbNVZa88.htm](vehicles-items/hCwx6Q30DbNVZa88.htm)|Sluggish|auto-trad|
|[iU2xj2sIMM3UdezU.htm](vehicles-items/iU2xj2sIMM3UdezU.htm)|Speed Boost|auto-trad|
|[Ix4GHIqSUqHawiG8.htm](vehicles-items/Ix4GHIqSUqHawiG8.htm)|Starting Drop|auto-trad|
|[izZwMB8zgdsWje9o.htm](vehicles-items/izZwMB8zgdsWje9o.htm)|Prismatic Defense|auto-trad|
|[jUfFzSzu0PIwCSgB.htm](vehicles-items/jUfFzSzu0PIwCSgB.htm)|Mountain Traverser|auto-trad|
|[jv3XUW2upC7aFRtR.htm](vehicles-items/jv3XUW2upC7aFRtR.htm)|Open Eyes|auto-trad|
|[kbVUEpQsOKNyw0BS.htm](vehicles-items/kbVUEpQsOKNyw0BS.htm)|Steam Cloud|auto-trad|
|[KFSpJiu6uoHoZl1b.htm](vehicles-items/KFSpJiu6uoHoZl1b.htm)|Manipulate Hands|auto-trad|
|[kQbyDtF75cOAr2Lc.htm](vehicles-items/kQbyDtF75cOAr2Lc.htm)|Wind-Up|auto-trad|
|[kr1leAaQlGXDTwn6.htm](vehicles-items/kr1leAaQlGXDTwn6.htm)|Wind-Up|auto-trad|
|[kt9xSEJnhJNdCBEy.htm](vehicles-items/kt9xSEJnhJNdCBEy.htm)|Captivating Wealth|auto-trad|
|[L0S2jaWemV8ndKIE.htm](vehicles-items/L0S2jaWemV8ndKIE.htm)|Sidecars|auto-trad|
|[L5LOLKdKxPBT5LCB.htm](vehicles-items/L5LOLKdKxPBT5LCB.htm)|Iron Rim|auto-trad|
|[lctMsuE0KxKUZgnH.htm](vehicles-items/lctMsuE0KxKUZgnH.htm)|Wind-Up|auto-trad|
|[lTX8qPMdwaThuCnc.htm](vehicles-items/lTX8qPMdwaThuCnc.htm)|Quaking Step|auto-trad|
|[MBdI0c7xv3ufaJM8.htm](vehicles-items/MBdI0c7xv3ufaJM8.htm)|Environmental Protections|auto-trad|
|[mTjyYbkFbVeKoGO8.htm](vehicles-items/mTjyYbkFbVeKoGO8.htm)|Submersible|auto-trad|
|[O5JvDSJWNWG2VtpR.htm](vehicles-items/O5JvDSJWNWG2VtpR.htm)|Electrify|auto-trad|
|[oWlyE6XYOLX2gBB6.htm](vehicles-items/oWlyE6XYOLX2gBB6.htm)|Weapon Mount|auto-trad|
|[PcXltLxBnG3FD8dH.htm](vehicles-items/PcXltLxBnG3FD8dH.htm)|Portable|auto-trad|
|[PqsmKTcXOM0aNYo8.htm](vehicles-items/PqsmKTcXOM0aNYo8.htm)|Shard Trail|auto-trad|
|[QUHIpCfam8YMHy6U.htm](vehicles-items/QUHIpCfam8YMHy6U.htm)|Electrical Absorption|auto-trad|
|[QwXzg5zGAZg4h78t.htm](vehicles-items/QwXzg5zGAZg4h78t.htm)|Fragile|auto-trad|
|[Qx3tJuimyRKT7l2W.htm](vehicles-items/Qx3tJuimyRKT7l2W.htm)|Cable|auto-trad|
|[R2SaS8szVUWwiHQr.htm](vehicles-items/R2SaS8szVUWwiHQr.htm)|Streamlined|auto-trad|
|[R4Co971n6x5ZNJHd.htm](vehicles-items/R4Co971n6x5ZNJHd.htm)|Wind-Up|auto-trad|
|[Rb4MyOLlodPUZdJX.htm](vehicles-items/Rb4MyOLlodPUZdJX.htm)|Flame Jet|auto-trad|
|[rDwfQJPnK5YyRx7v.htm](vehicles-items/rDwfQJPnK5YyRx7v.htm)|Sluggish|auto-trad|
|[RJf32Bczb9zQaFqs.htm](vehicles-items/RJf32Bczb9zQaFqs.htm)|Sluggish|auto-trad|
|[rvLRBCsFQMdUKtar.htm](vehicles-items/rvLRBCsFQMdUKtar.htm)|Sand Skimmer|auto-trad|
|[scXXuno9YNm1GWXK.htm](vehicles-items/scXXuno9YNm1GWXK.htm)|Pivoting Seats|auto-trad|
|[tFb8nV3bxHVLeU8Z.htm](vehicles-items/tFb8nV3bxHVLeU8Z.htm)|Maneuverable|auto-trad|
|[TMYxeMEQ1ilLoAlJ.htm](vehicles-items/TMYxeMEQ1ilLoAlJ.htm)|Wind Up|auto-trad|
|[TPj4AFDezXubgaJm.htm](vehicles-items/TPj4AFDezXubgaJm.htm)|Adamantine Drill|auto-trad|
|[tS9MnEsewMslgzhi.htm](vehicles-items/tS9MnEsewMslgzhi.htm)|Wind-Up|auto-trad|
|[uoev6dRtG1YFWPrn.htm](vehicles-items/uoev6dRtG1YFWPrn.htm)|Open Portholes|auto-trad|
|[uP38cyn9BsX8ni0O.htm](vehicles-items/uP38cyn9BsX8ni0O.htm)|Massive Jump|auto-trad|
|[upS8Tctv0gf8rQOC.htm](vehicles-items/upS8Tctv0gf8rQOC.htm)|Ballast Release|auto-trad|
|[UQr6y2RUCvhlcvZ3.htm](vehicles-items/UQr6y2RUCvhlcvZ3.htm)|Protective Barrier|auto-trad|
|[UZrxxtljdLoLewB9.htm](vehicles-items/UZrxxtljdLoLewB9.htm)|Deploy Wheels|auto-trad|
|[V0HecEvj92RlltT5.htm](vehicles-items/V0HecEvj92RlltT5.htm)|Sluggish|auto-trad|
|[V0Iw3ZOvbTqoxlvz.htm](vehicles-items/V0Iw3ZOvbTqoxlvz.htm)|Jump Jet|auto-trad|
|[V86H5yeH71jmelzY.htm](vehicles-items/V86H5yeH71jmelzY.htm)|Submersible|auto-trad|
|[vDMm6mf0S2i9JMzd.htm](vehicles-items/vDMm6mf0S2i9JMzd.htm)|Bolt Blast|auto-trad|
|[VqS08x8SzApqcmVy.htm](vehicles-items/VqS08x8SzApqcmVy.htm)|Weapon Mounts|auto-trad|
|[W01aasqmTJL5TpkW.htm](vehicles-items/W01aasqmTJL5TpkW.htm)|Unstable Launch|auto-trad|
|[xB0A9RW1Zi6TC7g0.htm](vehicles-items/xB0A9RW1Zi6TC7g0.htm)|Starting Drop|auto-trad|
|[XLttadhtgYoOq6f1.htm](vehicles-items/XLttadhtgYoOq6f1.htm)|Flash|auto-trad|
|[XvCWEJlIIDSfz2EA.htm](vehicles-items/XvCWEJlIIDSfz2EA.htm)|Ice Traverser|auto-trad|
|[yKd1wGx6zmlBwk0u.htm](vehicles-items/yKd1wGx6zmlBwk0u.htm)|Fall Apart|auto-trad|
|[Yr2rxvC8Hzmb70KZ.htm](vehicles-items/Yr2rxvC8Hzmb70KZ.htm)|Sluggish|auto-trad|
|[zEf7XS6RlEjM0VSO.htm](vehicles-items/zEf7XS6RlEjM0VSO.htm)|Wind-Up|auto-trad|
|[zGMODH6r1X3ZbwB1.htm](vehicles-items/zGMODH6r1X3ZbwB1.htm)|Sluggish|auto-trad|
|[zni42kvDwzT6lzuW.htm](vehicles-items/zni42kvDwzT6lzuW.htm)|Wind-Up|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
