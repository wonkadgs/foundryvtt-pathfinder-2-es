# Estado de la traducción (npc-gallery-items)

 * **auto-trad**: 502
 * **modificada**: 2


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[06QUpYDzeWLuHjVF.htm](npc-gallery-items/06QUpYDzeWLuHjVF.htm)|Shortsword|auto-trad|
|[087PGXgVjLNGs7lG.htm](npc-gallery-items/087PGXgVjLNGs7lG.htm)|Javelin|auto-trad|
|[0fWZn3xodpxwco8r.htm](npc-gallery-items/0fWZn3xodpxwco8r.htm)|Swear Vengeance|auto-trad|
|[0mY6UXKCPuoJcVKz.htm](npc-gallery-items/0mY6UXKCPuoJcVKz.htm)|Rapier|auto-trad|
|[0osYGMTj1AfagtgG.htm](npc-gallery-items/0osYGMTj1AfagtgG.htm)|Hand Crossbow|auto-trad|
|[0OzSGNJ8fmj9Z2dh.htm](npc-gallery-items/0OzSGNJ8fmj9Z2dh.htm)|Scimitar|auto-trad|
|[0PlCgdUO4JNfJMKr.htm](npc-gallery-items/0PlCgdUO4JNfJMKr.htm)|Hand Crossbow|auto-trad|
|[0WoLHxcEDfHig6LB.htm](npc-gallery-items/0WoLHxcEDfHig6LB.htm)|Scoundrel's Feint|auto-trad|
|[0yKbSsTBHJzUb4J9.htm](npc-gallery-items/0yKbSsTBHJzUb4J9.htm)|Pitchfork|auto-trad|
|[0Z0tfKCoRaKagvVT.htm](npc-gallery-items/0Z0tfKCoRaKagvVT.htm)|Pike and Strike|auto-trad|
|[1dkm6dgNqE06TZD0.htm](npc-gallery-items/1dkm6dgNqE06TZD0.htm)|Pewter Mug|auto-trad|
|[1gLrx7OTMtRPbK9N.htm](npc-gallery-items/1gLrx7OTMtRPbK9N.htm)|Brutal Beating|auto-trad|
|[1VRZfyweUQjfqW3F.htm](npc-gallery-items/1VRZfyweUQjfqW3F.htm)|Steady Spellcasting|auto-trad|
|[1Z1VM6NR01OCRpFf.htm](npc-gallery-items/1Z1VM6NR01OCRpFf.htm)|Cleric Domain Spells|auto-trad|
|[22gFqjuhKcKMgJKN.htm](npc-gallery-items/22gFqjuhKcKMgJKN.htm)|Warden's Protection|auto-trad|
|[22PsMMGp8JINhVoi.htm](npc-gallery-items/22PsMMGp8JINhVoi.htm)|Rapier|auto-trad|
|[28SnHCyal79ByXz4.htm](npc-gallery-items/28SnHCyal79ByXz4.htm)|Composite Longbow|auto-trad|
|[2HONqJ3KSR0h25We.htm](npc-gallery-items/2HONqJ3KSR0h25We.htm)|Occult Spontaneous Spells|auto-trad|
|[2LYwFejQFhQ3id2V.htm](npc-gallery-items/2LYwFejQFhQ3id2V.htm)|Attack of Opportunity|auto-trad|
|[2pNLHufgvxymiXGj.htm](npc-gallery-items/2pNLHufgvxymiXGj.htm)|Rock|auto-trad|
|[2PrqruymHWvMXHjI.htm](npc-gallery-items/2PrqruymHWvMXHjI.htm)|Air of Authority|auto-trad|
|[2uSSFi0lqFoWgzYI.htm](npc-gallery-items/2uSSFi0lqFoWgzYI.htm)|Home Turf|auto-trad|
|[2w1WQr635JVnsORb.htm](npc-gallery-items/2w1WQr635JVnsORb.htm)|Swift Sneak|auto-trad|
|[2xwVAGUh92PRESt7.htm](npc-gallery-items/2xwVAGUh92PRESt7.htm)|Wizard School Spell|auto-trad|
|[3CHnQSHfFAroFmAn.htm](npc-gallery-items/3CHnQSHfFAroFmAn.htm)|Light Hammer|auto-trad|
|[3Cmc9ZLwNvRhmJBB.htm](npc-gallery-items/3Cmc9ZLwNvRhmJBB.htm)|Mobility|auto-trad|
|[3CtPYZLeE2j7fIME.htm](npc-gallery-items/3CtPYZLeE2j7fIME.htm)|Shiv|auto-trad|
|[3FUfdj3U5vEftIPg.htm](npc-gallery-items/3FUfdj3U5vEftIPg.htm)|Primal Prepared Spells|auto-trad|
|[3Gsk5abTPsMK7L90.htm](npc-gallery-items/3Gsk5abTPsMK7L90.htm)|Dagger|auto-trad|
|[3Hp4zP3MQ5RzRFcu.htm](npc-gallery-items/3Hp4zP3MQ5RzRFcu.htm)|Fist|auto-trad|
|[3i4w75rSKQiwZzwD.htm](npc-gallery-items/3i4w75rSKQiwZzwD.htm)|Fated Doom|auto-trad|
|[3k43BvFaLIDzoL5a.htm](npc-gallery-items/3k43BvFaLIDzoL5a.htm)|Crossbow|auto-trad|
|[3LcqeSNm2BGnGC1G.htm](npc-gallery-items/3LcqeSNm2BGnGC1G.htm)|Placate|auto-trad|
|[3lmMOkUuWRYkYOfO.htm](npc-gallery-items/3lmMOkUuWRYkYOfO.htm)|Divine Prepared Spells|auto-trad|
|[486JeF49AVqoD8d0.htm](npc-gallery-items/486JeF49AVqoD8d0.htm)|Composite Longbow|auto-trad|
|[49sfbmwEvN94dGhS.htm](npc-gallery-items/49sfbmwEvN94dGhS.htm)|Hand Crossbow|auto-trad|
|[4baqnRP77X4lxpxU.htm](npc-gallery-items/4baqnRP77X4lxpxU.htm)|Wild Empathy|auto-trad|
|[4E5jd0bmls7oDWHu.htm](npc-gallery-items/4E5jd0bmls7oDWHu.htm)|Trident|auto-trad|
|[4l9I2HyaE5ptxZSF.htm](npc-gallery-items/4l9I2HyaE5ptxZSF.htm)|Drain Bonded Item|auto-trad|
|[4QWoVlc5btvkhSWU.htm](npc-gallery-items/4QWoVlc5btvkhSWU.htm)|Divine Rituals|auto-trad|
|[4VbQq13CfCCwqfYt.htm](npc-gallery-items/4VbQq13CfCCwqfYt.htm)|Foot|auto-trad|
|[598qUP8bkS1TdMEY.htm](npc-gallery-items/598qUP8bkS1TdMEY.htm)|Fist|auto-trad|
|[5as8LbntWBRwhdy8.htm](npc-gallery-items/5as8LbntWBRwhdy8.htm)|Sentry's Aim|auto-trad|
|[5DPI512dyzb4L4kR.htm](npc-gallery-items/5DPI512dyzb4L4kR.htm)|Shield Block|auto-trad|
|[5h9TgMqtTUK56q1p.htm](npc-gallery-items/5h9TgMqtTUK56q1p.htm)|Divine Focus Spells|auto-trad|
|[5iTqDt4fDSnYuJIg.htm](npc-gallery-items/5iTqDt4fDSnYuJIg.htm)|Dual Disarm|auto-trad|
|[5qtQA0th9HWkfecU.htm](npc-gallery-items/5qtQA0th9HWkfecU.htm)|Hunt Prey|auto-trad|
|[6048zx9AI7OaIsjX.htm](npc-gallery-items/6048zx9AI7OaIsjX.htm)|Reach Spell|auto-trad|
|[6bZGX49gZGcVxwBY.htm](npc-gallery-items/6bZGX49gZGcVxwBY.htm)|Primal Focus Spells|auto-trad|
|[6KJ0ua534rpJ3q5Q.htm](npc-gallery-items/6KJ0ua534rpJ3q5Q.htm)|Shortsword|auto-trad|
|[6kOiyngYv38Ec0lB.htm](npc-gallery-items/6kOiyngYv38Ec0lB.htm)|Divine Prepared Spells|auto-trad|
|[6SS8rVfwSj7Oy7WY.htm](npc-gallery-items/6SS8rVfwSj7Oy7WY.htm)|Healing Hands|auto-trad|
|[6UQShM6aleyfYDgH.htm](npc-gallery-items/6UQShM6aleyfYDgH.htm)|Big Swing|auto-trad|
|[78xUeN4lAiJy7HB8.htm](npc-gallery-items/78xUeN4lAiJy7HB8.htm)|Paragon's Guard|auto-trad|
|[7bwgpaZGG2Bs6DSx.htm](npc-gallery-items/7bwgpaZGG2Bs6DSx.htm)|Apple|auto-trad|
|[7JKronzMIeoVdmyI.htm](npc-gallery-items/7JKronzMIeoVdmyI.htm)|Bard Composition Spells|auto-trad|
|[7STN0PrV0AblMmmM.htm](npc-gallery-items/7STN0PrV0AblMmmM.htm)|Primal Spontaneous Spells|auto-trad|
|[7uuo0yXpPiywEFNQ.htm](npc-gallery-items/7uuo0yXpPiywEFNQ.htm)|Counterspell|auto-trad|
|[7uXdmkkS4JVZOIyN.htm](npc-gallery-items/7uXdmkkS4JVZOIyN.htm)|Composite Shortbow|auto-trad|
|[7ydMyqCO1oh7cHUR.htm](npc-gallery-items/7ydMyqCO1oh7cHUR.htm)|Fist|auto-trad|
|[80iH1mCHZeW5rWYE.htm](npc-gallery-items/80iH1mCHZeW5rWYE.htm)|Nimble Dodge|auto-trad|
|[85x1pisleQwObtMf.htm](npc-gallery-items/85x1pisleQwObtMf.htm)|Mark for Death|auto-trad|
|[86ELyZJGF5f8M7ew.htm](npc-gallery-items/86ELyZJGF5f8M7ew.htm)|Bravery|auto-trad|
|[86tScORfgOX4zuix.htm](npc-gallery-items/86tScORfgOX4zuix.htm)|Sneak Attack|auto-trad|
|[8812wH4PteCqa89F.htm](npc-gallery-items/8812wH4PteCqa89F.htm)|Dagger|auto-trad|
|[8EfthxjczAzYkpvD.htm](npc-gallery-items/8EfthxjczAzYkpvD.htm)|Dagger|auto-trad|
|[8fAOSxgE0FgWpvsE.htm](npc-gallery-items/8fAOSxgE0FgWpvsE.htm)|Composite Shortbow|auto-trad|
|[8l8fkQuORmfdmuFO.htm](npc-gallery-items/8l8fkQuORmfdmuFO.htm)|Divine Spontaneous Spells|auto-trad|
|[8sKYzAJzGo9HTaC9.htm](npc-gallery-items/8sKYzAJzGo9HTaC9.htm)|Group Impression|auto-trad|
|[9CtaEDgxqQK3AGrO.htm](npc-gallery-items/9CtaEDgxqQK3AGrO.htm)|Dagger|auto-trad|
|[9hxxkHZZGdkhbZvg.htm](npc-gallery-items/9hxxkHZZGdkhbZvg.htm)|Shield Warden|auto-trad|
|[9KFKSYQInYw7RRFw.htm](npc-gallery-items/9KFKSYQInYw7RRFw.htm)|Greataxe|auto-trad|
|[9OxsjzXYOnydXvcb.htm](npc-gallery-items/9OxsjzXYOnydXvcb.htm)|Fist|auto-trad|
|[9PJJ2Aqeorbh7BnP.htm](npc-gallery-items/9PJJ2Aqeorbh7BnP.htm)|Throw Rock|auto-trad|
|[9UbRDyf3EovxiABp.htm](npc-gallery-items/9UbRDyf3EovxiABp.htm)|Fickle Prophecy|auto-trad|
|[9xXxlNwXM90sw8zk.htm](npc-gallery-items/9xXxlNwXM90sw8zk.htm)|Doctor's Hand|auto-trad|
|[9xyeARIhxCpAj8BU.htm](npc-gallery-items/9xyeARIhxCpAj8BU.htm)|Greatclub|auto-trad|
|[a6VT956imq0UeyhM.htm](npc-gallery-items/a6VT956imq0UeyhM.htm)|Fist|auto-trad|
|[aAgpROGXTToyiZfx.htm](npc-gallery-items/aAgpROGXTToyiZfx.htm)|Foot|auto-trad|
|[agaJs5LEhgmy0sPO.htm](npc-gallery-items/agaJs5LEhgmy0sPO.htm)|Scout's Warning|auto-trad|
|[aMJGSTnE6Wgp9MBr.htm](npc-gallery-items/aMJGSTnE6Wgp9MBr.htm)|Guiding Words|auto-trad|
|[ammC8wH7C165yxrp.htm](npc-gallery-items/ammC8wH7C165yxrp.htm)|Light in the Dark|auto-trad|
|[Ant0lStzSYo01Zhd.htm](npc-gallery-items/Ant0lStzSYo01Zhd.htm)|Halberd|auto-trad|
|[AtMtAwu4qZIQZq8E.htm](npc-gallery-items/AtMtAwu4qZIQZq8E.htm)|Dagger|auto-trad|
|[AuocSDVewXxiFzuz.htm](npc-gallery-items/AuocSDVewXxiFzuz.htm)|Fist|auto-trad|
|[AWKTmRlR5ZVNFq1A.htm](npc-gallery-items/AWKTmRlR5ZVNFq1A.htm)|Dagger|auto-trad|
|[axdkeSbnyjnpjO2h.htm](npc-gallery-items/axdkeSbnyjnpjO2h.htm)|Speaker of the Oceans|auto-trad|
|[ayPb7Lg7AFYqIfzk.htm](npc-gallery-items/ayPb7Lg7AFYqIfzk.htm)|Sneak Attack|auto-trad|
|[aYsvRHgZ6n5NX68c.htm](npc-gallery-items/aYsvRHgZ6n5NX68c.htm)|Trap Finder|auto-trad|
|[Azm5VrtnHNvlTD1i.htm](npc-gallery-items/Azm5VrtnHNvlTD1i.htm)|Inspirational Presence|auto-trad|
|[b65fCKLd7xxZXc5w.htm](npc-gallery-items/b65fCKLd7xxZXc5w.htm)|Aquatic Predator|auto-trad|
|[b8lQp0O8fNE08x0h.htm](npc-gallery-items/b8lQp0O8fNE08x0h.htm)|Kukri|auto-trad|
|[BbtsWaLbyjDHtboq.htm](npc-gallery-items/BbtsWaLbyjDHtboq.htm)|Cleric Domain Spells|auto-trad|
|[BDewONWVQqYhfwRL.htm](npc-gallery-items/BDewONWVQqYhfwRL.htm)|Gavel|auto-trad|
|[Bg7DMaLAy2rtjxkb.htm](npc-gallery-items/Bg7DMaLAy2rtjxkb.htm)|Composite Shortbow|auto-trad|
|[BHcpl6Nlf6nVff26.htm](npc-gallery-items/BHcpl6Nlf6nVff26.htm)|Deadly Simplicity|auto-trad|
|[blPj44XSQdjiu1MS.htm](npc-gallery-items/blPj44XSQdjiu1MS.htm)|Fence's Eye|auto-trad|
|[BpD2byfrbMItSgfy.htm](npc-gallery-items/BpD2byfrbMItSgfy.htm)|+15 to Sense Motive|auto-trad|
|[bqpvez0u4Rw93KXo.htm](npc-gallery-items/bqpvez0u4Rw93KXo.htm)|Font of Knowledge|auto-trad|
|[BR4ZECJ9kO4oph8g.htm](npc-gallery-items/BR4ZECJ9kO4oph8g.htm)|Bloodline Magic|auto-trad|
|[Bre79vI4roX7eGSB.htm](npc-gallery-items/Bre79vI4roX7eGSB.htm)|Fist|auto-trad|
|[Bz5e5nzwVDHVigQ0.htm](npc-gallery-items/Bz5e5nzwVDHVigQ0.htm)|Dagger|auto-trad|
|[C7PdppuFM1J5AfZc.htm](npc-gallery-items/C7PdppuFM1J5AfZc.htm)|Shiv|auto-trad|
|[C9BvM6sI4AN0SssQ.htm](npc-gallery-items/C9BvM6sI4AN0SssQ.htm)|Shovel|auto-trad|
|[cah7DtpiMX62b2rT.htm](npc-gallery-items/cah7DtpiMX62b2rT.htm)|Longspear|auto-trad|
|[cHWzyBoiaRRRBq99.htm](npc-gallery-items/cHWzyBoiaRRRBq99.htm)|-2 to Will Saves vs. Higher Ranking Cult Members|auto-trad|
|[CJp7VcMyI9Wp4QX0.htm](npc-gallery-items/CJp7VcMyI9Wp4QX0.htm)|Attack of Opportunity|auto-trad|
|[cjVbggOxvEzXmIQm.htm](npc-gallery-items/cjVbggOxvEzXmIQm.htm)|Piton Pin|auto-trad|
|[CKzyNTs3y8kKsSFY.htm](npc-gallery-items/CKzyNTs3y8kKsSFY.htm)|Efficient Capture|auto-trad|
|[Cmu4PgaVIciVRKDC.htm](npc-gallery-items/Cmu4PgaVIciVRKDC.htm)|Dagger|auto-trad|
|[CmUq3q1BL7VoIvyM.htm](npc-gallery-items/CmUq3q1BL7VoIvyM.htm)|Cutlass|auto-trad|
|[CmXRYDHVFD9zOTaa.htm](npc-gallery-items/CmXRYDHVFD9zOTaa.htm)|Dagger|auto-trad|
|[cMz56RGF3H0a0ZQi.htm](npc-gallery-items/cMz56RGF3H0a0ZQi.htm)|Quick Rummage|auto-trad|
|[CN4GwNgOryN6LNFo.htm](npc-gallery-items/CN4GwNgOryN6LNFo.htm)|Fist|auto-trad|
|[cnmmUfSvnBHbPJ3N.htm](npc-gallery-items/cnmmUfSvnBHbPJ3N.htm)|Bard Composition Spells|auto-trad|
|[coq6krhuSNTUKuyB.htm](npc-gallery-items/coq6krhuSNTUKuyB.htm)|Deny Advantage|auto-trad|
|[CqwxqwQxgNIp6UdX.htm](npc-gallery-items/CqwxqwQxgNIp6UdX.htm)|Staff Of Abjuration|auto-trad|
|[CRo6e5ADHqXxRyBr.htm](npc-gallery-items/CRo6e5ADHqXxRyBr.htm)|Occult Spells Known|auto-trad|
|[CUkJIaXcAVXlBYoH.htm](npc-gallery-items/CUkJIaXcAVXlBYoH.htm)|Smith's Fury|auto-trad|
|[cviTvCcO5isNFaU7.htm](npc-gallery-items/cviTvCcO5isNFaU7.htm)|Sickle|auto-trad|
|[cxWmbPgxDBajNjqz.htm](npc-gallery-items/cxWmbPgxDBajNjqz.htm)|Scimitar|auto-trad|
|[CY3VwzlwEsLmLp7q.htm](npc-gallery-items/CY3VwzlwEsLmLp7q.htm)|Dagger|auto-trad|
|[cYpzRpaINHeW2XxC.htm](npc-gallery-items/cYpzRpaINHeW2XxC.htm)|Poison Weapon|auto-trad|
|[cz2QOg6QSmJNA3YL.htm](npc-gallery-items/cz2QOg6QSmJNA3YL.htm)|Sway the Judge and Jury|auto-trad|
|[D91SDgzufetsXtHT.htm](npc-gallery-items/D91SDgzufetsXtHT.htm)|Acid Flask|auto-trad|
|[d9TOZvyquMo6o18K.htm](npc-gallery-items/d9TOZvyquMo6o18K.htm)|Quick Bomber|auto-trad|
|[db0QJhDLMLOerNa6.htm](npc-gallery-items/db0QJhDLMLOerNa6.htm)|Channel Smite|auto-trad|
|[dbDMr7uWS78nPenZ.htm](npc-gallery-items/dbDMr7uWS78nPenZ.htm)|Dagger|auto-trad|
|[dCBZN8wCmO7ipEHO.htm](npc-gallery-items/dCBZN8wCmO7ipEHO.htm)|Dagger|auto-trad|
|[ddMOXx1g84tLNThI.htm](npc-gallery-items/ddMOXx1g84tLNThI.htm)|+1 Status to All Saves vs. Poison|auto-trad|
|[DDRbAS5r8U0XMma7.htm](npc-gallery-items/DDRbAS5r8U0XMma7.htm)|Greataxe|auto-trad|
|[DgGWQZ2UfWzKedET.htm](npc-gallery-items/DgGWQZ2UfWzKedET.htm)|Composite Longbow|auto-trad|
|[DipxFEdiddYHWirp.htm](npc-gallery-items/DipxFEdiddYHWirp.htm)|Bard Composition Spells|auto-trad|
|[DjeP7DBDv1Ue87eW.htm](npc-gallery-items/DjeP7DBDv1Ue87eW.htm)|Whip|auto-trad|
|[dlhm2yJ4EUGym0CG.htm](npc-gallery-items/dlhm2yJ4EUGym0CG.htm)|Gang Up|auto-trad|
|[drNAUPLqWo7QSh3l.htm](npc-gallery-items/drNAUPLqWo7QSh3l.htm)|Shielded Advance|auto-trad|
|[dSa8OQfOz63lYKQ2.htm](npc-gallery-items/dSa8OQfOz63lYKQ2.htm)|Versatile Performance|auto-trad|
|[dTwii5UOFOwMyY0C.htm](npc-gallery-items/dTwii5UOFOwMyY0C.htm)|Font of Gossip|auto-trad|
|[DV9NQLrI4FouRCpe.htm](npc-gallery-items/DV9NQLrI4FouRCpe.htm)|Fist|auto-trad|
|[dYLk3rxvErYoaP9G.htm](npc-gallery-items/dYLk3rxvErYoaP9G.htm)|Attack of Opportunity|auto-trad|
|[dYlvFC5F1daX6NF8.htm](npc-gallery-items/dYlvFC5F1daX6NF8.htm)|Shortsword|auto-trad|
|[DzrwPVv4yZGfqMNf.htm](npc-gallery-items/DzrwPVv4yZGfqMNf.htm)|Improved Communal Healing|auto-trad|
|[eAaIdI0UJUrcmPzC.htm](npc-gallery-items/eAaIdI0UJUrcmPzC.htm)|Bar Brawler|auto-trad|
|[EbHva548irAoyT4c.htm](npc-gallery-items/EbHva548irAoyT4c.htm)|Attack of Opportunity|auto-trad|
|[ebZ4RtDXFWOGqY2i.htm](npc-gallery-items/ebZ4RtDXFWOGqY2i.htm)|Rapier|auto-trad|
|[ECBmark63yV59X2C.htm](npc-gallery-items/ECBmark63yV59X2C.htm)|Occult Spontaneous Spells|auto-trad|
|[edMKJZQVDeLF48ZV.htm](npc-gallery-items/edMKJZQVDeLF48ZV.htm)|Reach Spell|auto-trad|
|[EHw3qV7vYGDQdfPB.htm](npc-gallery-items/EHw3qV7vYGDQdfPB.htm)|Sneak Attack|auto-trad|
|[EMerfEpCLFxD8gAF.htm](npc-gallery-items/EMerfEpCLFxD8gAF.htm)|Staff|auto-trad|
|[emTBNypG9xLkrGmv.htm](npc-gallery-items/emTBNypG9xLkrGmv.htm)|Greataxe|auto-trad|
|[eplixHfDeknEF97x.htm](npc-gallery-items/eplixHfDeknEF97x.htm)|Chameleon Step|auto-trad|
|[Eq3KajvZR0XWgw2w.htm](npc-gallery-items/Eq3KajvZR0XWgw2w.htm)|Rapier|auto-trad|
|[erMgbyNlTUBjOhqo.htm](npc-gallery-items/erMgbyNlTUBjOhqo.htm)|Falchion|auto-trad|
|[EtuysfUGpQnBFAaU.htm](npc-gallery-items/EtuysfUGpQnBFAaU.htm)|Rapier|auto-trad|
|[Ew9YdRtlJy7JUSHa.htm](npc-gallery-items/Ew9YdRtlJy7JUSHa.htm)|Fist|auto-trad|
|[ezgpbQSl4o3vFJaU.htm](npc-gallery-items/ezgpbQSl4o3vFJaU.htm)|Arcane Prepared Spells|auto-trad|
|[F0Yrpyr0NczIzhGY.htm](npc-gallery-items/F0Yrpyr0NczIzhGY.htm)|Steady Balance|auto-trad|
|[f6BZlUvuAkJjyx2T.htm](npc-gallery-items/f6BZlUvuAkJjyx2T.htm)|Vengeful Edge|auto-trad|
|[F7IHKiavOdLoatuo.htm](npc-gallery-items/F7IHKiavOdLoatuo.htm)|Doctor's Hand|auto-trad|
|[f9317vgvLL2FaL7N.htm](npc-gallery-items/f9317vgvLL2FaL7N.htm)|Fortitude Saves|auto-trad|
|[F9lM0H0vX25rlQiB.htm](npc-gallery-items/F9lM0H0vX25rlQiB.htm)|Sworn Duty|auto-trad|
|[FDgG3VBojXvvTpd3.htm](npc-gallery-items/FDgG3VBojXvvTpd3.htm)|Darkvision|auto-trad|
|[fGw7sipW1eI40byQ.htm](npc-gallery-items/fGw7sipW1eI40byQ.htm)|Dagger|auto-trad|
|[FkNhevhHp4HfH05a.htm](npc-gallery-items/FkNhevhHp4HfH05a.htm)|Low-Light Vision|auto-trad|
|[flLLoUGHSVJire4L.htm](npc-gallery-items/flLLoUGHSVJire4L.htm)|Sorcerer Bloodline Spells|auto-trad|
|[FMRFRcuXn2QHFX2v.htm](npc-gallery-items/FMRFRcuXn2QHFX2v.htm)|Bravery|auto-trad|
|[FmriI9fZfj3I2QS1.htm](npc-gallery-items/FmriI9fZfj3I2QS1.htm)|Swig|auto-trad|
|[FMXvHmfCarIbLAk5.htm](npc-gallery-items/FMXvHmfCarIbLAk5.htm)|Broom|auto-trad|
|[fp3XmyB0pcpjCBex.htm](npc-gallery-items/fp3XmyB0pcpjCBex.htm)|Nimble Dodge|auto-trad|
|[FQO3E18nwnNURI9p.htm](npc-gallery-items/FQO3E18nwnNURI9p.htm)|Universal Obedience|auto-trad|
|[fSa7yGc6AlAoFW23.htm](npc-gallery-items/fSa7yGc6AlAoFW23.htm)|Composite Longbow|auto-trad|
|[FuOqFNDYZSFUVtPi.htm](npc-gallery-items/FuOqFNDYZSFUVtPi.htm)|Cutlery|auto-trad|
|[fWy0XIMLXO8QjFJn.htm](npc-gallery-items/fWy0XIMLXO8QjFJn.htm)|Divine Prepared Spells|auto-trad|
|[FXduVQD4iGJxgXX2.htm](npc-gallery-items/FXduVQD4iGJxgXX2.htm)|Naval Pike|auto-trad|
|[G4RkhwmVzzHUmIqk.htm](npc-gallery-items/G4RkhwmVzzHUmIqk.htm)|Club|auto-trad|
|[G5iBcxxzOecJApLi.htm](npc-gallery-items/G5iBcxxzOecJApLi.htm)|Crossbow|auto-trad|
|[geD4pfraSGQRXXvE.htm](npc-gallery-items/geD4pfraSGQRXXvE.htm)|+1 Status to All Saves vs. Fear|auto-trad|
|[Gf4bCH3UInm7pjOu.htm](npc-gallery-items/Gf4bCH3UInm7pjOu.htm)|Intimidating Strike|auto-trad|
|[ggCUTswEGOjGvjj0.htm](npc-gallery-items/ggCUTswEGOjGvjj0.htm)|Sudden Charge|auto-trad|
|[GhbdiBXGiH9AN1pw.htm](npc-gallery-items/GhbdiBXGiH9AN1pw.htm)|Staff|auto-trad|
|[ghFGvItVlZL7x9SC.htm](npc-gallery-items/ghFGvItVlZL7x9SC.htm)|Hand Crossbow|auto-trad|
|[gHkZ5EkRCbXagaxl.htm](npc-gallery-items/gHkZ5EkRCbXagaxl.htm)|Drunken Rage|auto-trad|
|[GICVztYSUDv0IIkz.htm](npc-gallery-items/GICVztYSUDv0IIkz.htm)|Cat Fall|auto-trad|
|[GmvgnOPx8qubo9xm.htm](npc-gallery-items/GmvgnOPx8qubo9xm.htm)|Advancing Flourish|auto-trad|
|[groFZoIcMCSJcNZO.htm](npc-gallery-items/groFZoIcMCSJcNZO.htm)|+2 Circumstance to All Saves vs. Dream and Sleep|auto-trad|
|[GvFkJmDJIJRneI79.htm](npc-gallery-items/GvFkJmDJIJRneI79.htm)|Primal Prepared Spells|auto-trad|
|[GxVV9OrtL5eoSx2b.htm](npc-gallery-items/GxVV9OrtL5eoSx2b.htm)|Forager|auto-trad|
|[gzBR3N7gO6KPegiv.htm](npc-gallery-items/gzBR3N7gO6KPegiv.htm)|Mask Bond|auto-trad|
|[h0oO1nc9v9yl5OgB.htm](npc-gallery-items/h0oO1nc9v9yl5OgB.htm)|Lip Reader|auto-trad|
|[h1SgfPkIaJazPwYz.htm](npc-gallery-items/h1SgfPkIaJazPwYz.htm)|Glittering Distraction|auto-trad|
|[H8PMiZFsy2MQfLN2.htm](npc-gallery-items/H8PMiZFsy2MQfLN2.htm)|Gather Converts|auto-trad|
|[HBgAXRPFxVlqiDZ8.htm](npc-gallery-items/HBgAXRPFxVlqiDZ8.htm)|Shortbow|auto-trad|
|[hc2576PgevK5i1qc.htm](npc-gallery-items/hc2576PgevK5i1qc.htm)|Pitch Bale|auto-trad|
|[HDAF4Nls9WJAWOeR.htm](npc-gallery-items/HDAF4Nls9WJAWOeR.htm)|Submerged Stealth|auto-trad|
|[hHng07DmszKk5pNt.htm](npc-gallery-items/hHng07DmszKk5pNt.htm)|Crossbow|auto-trad|
|[hjekwptdXb14cWSj.htm](npc-gallery-items/hjekwptdXb14cWSj.htm)|Bravery|auto-trad|
|[HnDaKsWEBEBi5eJX.htm](npc-gallery-items/HnDaKsWEBEBi5eJX.htm)|Sneak Attack|auto-trad|
|[hoNnL8BsPzPuGERZ.htm](npc-gallery-items/hoNnL8BsPzPuGERZ.htm)|Swinging Strike|auto-trad|
|[HpLKljvtfXk5Em41.htm](npc-gallery-items/HpLKljvtfXk5Em41.htm)|Book|auto-trad|
|[hpPQrfATIi4fXq3L.htm](npc-gallery-items/hpPQrfATIi4fXq3L.htm)|Cane|auto-trad|
|[Hrp3jkhyAGyCrwfW.htm](npc-gallery-items/Hrp3jkhyAGyCrwfW.htm)|Force Body|auto-trad|
|[Ht7knXtXeyTkKi2n.htm](npc-gallery-items/Ht7knXtXeyTkKi2n.htm)|Evasion|auto-trad|
|[Hw1KYT6hIaAHcXkI.htm](npc-gallery-items/Hw1KYT6hIaAHcXkI.htm)|Navigator's Edge|auto-trad|
|[HXCEPfKkb2TP6NVy.htm](npc-gallery-items/HXCEPfKkb2TP6NVy.htm)|Bravery|auto-trad|
|[hYh1jMda40LahOdv.htm](npc-gallery-items/hYh1jMda40LahOdv.htm)|Sickle|auto-trad|
|[hyLN8l8Z2WSxFAXC.htm](npc-gallery-items/hyLN8l8Z2WSxFAXC.htm)|Running Reload|auto-trad|
|[i3KBgRUcT11VAT2c.htm](npc-gallery-items/i3KBgRUcT11VAT2c.htm)|Attack of Opportunity|auto-trad|
|[i6RGiBt0UOTBg3go.htm](npc-gallery-items/i6RGiBt0UOTBg3go.htm)|Club|auto-trad|
|[IAFE4UVaYTfZgp3J.htm](npc-gallery-items/IAFE4UVaYTfZgp3J.htm)|Fist|auto-trad|
|[icyjQtX7NGtgTdJj.htm](npc-gallery-items/icyjQtX7NGtgTdJj.htm)|Claw|auto-trad|
|[IEibCXh97z4dVqH9.htm](npc-gallery-items/IEibCXh97z4dVqH9.htm)|Bandit's Ambush|auto-trad|
|[ImzpAPq1SgMcf7t3.htm](npc-gallery-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|auto-trad|
|[iQWAwtvFxRVGqW0G.htm](npc-gallery-items/iQWAwtvFxRVGqW0G.htm)|Journal|auto-trad|
|[ItJWnelnPHXRTb86.htm](npc-gallery-items/ItJWnelnPHXRTb86.htm)|Cleric Domain Spells|auto-trad|
|[iUTM9WOzUZIkVt1D.htm](npc-gallery-items/iUTM9WOzUZIkVt1D.htm)|Hand Crossbow|auto-trad|
|[IWfm1LwDDnDCkgUi.htm](npc-gallery-items/IWfm1LwDDnDCkgUi.htm)|Fist|auto-trad|
|[iWiQjH6UZGPhap8G.htm](npc-gallery-items/iWiQjH6UZGPhap8G.htm)|Intimidating Strike|auto-trad|
|[Iwy3TBA4Dhrb3256.htm](npc-gallery-items/Iwy3TBA4Dhrb3256.htm)|Composite Longbow|auto-trad|
|[IWzZhxjRQbZJM9bU.htm](npc-gallery-items/IWzZhxjRQbZJM9bU.htm)|+3 Status to Reflex vs. Damaging Effects|auto-trad|
|[J0gZtGInMyuCyCuB.htm](npc-gallery-items/J0gZtGInMyuCyCuB.htm)|Longsword|auto-trad|
|[j1wf4dSXkGypJjRs.htm](npc-gallery-items/j1wf4dSXkGypJjRs.htm)|Sneak Attack|auto-trad|
|[J2jz17JAQtHVQs5I.htm](npc-gallery-items/J2jz17JAQtHVQs5I.htm)|Sneak Attack|auto-trad|
|[J6LhLBjnU1kyUhwK.htm](npc-gallery-items/J6LhLBjnU1kyUhwK.htm)|Shield Ally|auto-trad|
|[Ja5QiBYnwlN8lKXg.htm](npc-gallery-items/Ja5QiBYnwlN8lKXg.htm)|Composite Shortbow|auto-trad|
|[jbBEFIXHqLEFkuxj.htm](npc-gallery-items/jbBEFIXHqLEFkuxj.htm)|Sneak Attack|auto-trad|
|[JbbyThxk68mjKgkw.htm](npc-gallery-items/JbbyThxk68mjKgkw.htm)|Fist|auto-trad|
|[JbPgkPBw6I7dZFcQ.htm](npc-gallery-items/JbPgkPBw6I7dZFcQ.htm)|Scalpel|auto-trad|
|[JCFQaCMK8sKMFgL2.htm](npc-gallery-items/JCFQaCMK8sKMFgL2.htm)|Cleric Domain Spells|auto-trad|
|[jHMJPUtmDcCC20ND.htm](npc-gallery-items/jHMJPUtmDcCC20ND.htm)|Crossbow|auto-trad|
|[jhzTyAeLv0K1sK7c.htm](npc-gallery-items/jhzTyAeLv0K1sK7c.htm)|Book|auto-trad|
|[JLeDsdQFNsR2XCIX.htm](npc-gallery-items/JLeDsdQFNsR2XCIX.htm)|Torch|auto-trad|
|[jr5nhXsMkIwlhAOa.htm](npc-gallery-items/jr5nhXsMkIwlhAOa.htm)|Moderate Frost Vial|auto-trad|
|[JUbm5Xf0IURQGSTX.htm](npc-gallery-items/JUbm5Xf0IURQGSTX.htm)|Ledger|auto-trad|
|[JVQnk3tWMv7fuQax.htm](npc-gallery-items/JVQnk3tWMv7fuQax.htm)|Fist|auto-trad|
|[KbYGZddUNQGhrhEf.htm](npc-gallery-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|auto-trad|
|[kcbRLFttvvktolJn.htm](npc-gallery-items/kcbRLFttvvktolJn.htm)|Hydration|auto-trad|
|[kcXoIhefcYSfr7CL.htm](npc-gallery-items/kcXoIhefcYSfr7CL.htm)|Guide's Warning|auto-trad|
|[KdjtCPQTvuJzzmNN.htm](npc-gallery-items/KdjtCPQTvuJzzmNN.htm)|Crossbow|auto-trad|
|[KDV6e31ms8EioLrR.htm](npc-gallery-items/KDV6e31ms8EioLrR.htm)|Dagger|auto-trad|
|[keHqnY2IJOfiVKRj.htm](npc-gallery-items/keHqnY2IJOfiVKRj.htm)|Rapier|auto-trad|
|[KhEHpoNP1iXOpths.htm](npc-gallery-items/KhEHpoNP1iXOpths.htm)|Club|auto-trad|
|[kotwyraYf9vVKqEY.htm](npc-gallery-items/kotwyraYf9vVKqEY.htm)|Fascinating Dance|auto-trad|
|[kPjRjJyV9Xs4FsAB.htm](npc-gallery-items/kPjRjJyV9Xs4FsAB.htm)|Arcane Prepared Spells|auto-trad|
|[KRLpKVZ3AZwkE4cj.htm](npc-gallery-items/KRLpKVZ3AZwkE4cj.htm)|+2 Status to Perception to Find Traps|auto-trad|
|[kSd8rIlnXiVczYg0.htm](npc-gallery-items/kSd8rIlnXiVczYg0.htm)|Deny Advantage|auto-trad|
|[kx6tL1KbenrQWtCB.htm](npc-gallery-items/kx6tL1KbenrQWtCB.htm)|Composite Longbow|auto-trad|
|[kzNiR7bLBxL4aWfR.htm](npc-gallery-items/kzNiR7bLBxL4aWfR.htm)|Quick Draw|auto-trad|
|[L2NMWuwie5fk4t5D.htm](npc-gallery-items/L2NMWuwie5fk4t5D.htm)|Beat a Retreat|auto-trad|
|[Laex9xerP0tAva7M.htm](npc-gallery-items/Laex9xerP0tAva7M.htm)|Surprise Attack|auto-trad|
|[LbKXY3iqGS2l93BZ.htm](npc-gallery-items/LbKXY3iqGS2l93BZ.htm)|Fist|auto-trad|
|[lC7KFntiTwd1FRo7.htm](npc-gallery-items/lC7KFntiTwd1FRo7.htm)|Pickpocket|auto-trad|
|[ld6AdRlIeDnctV9m.htm](npc-gallery-items/ld6AdRlIeDnctV9m.htm)|Sneak Attack|auto-trad|
|[LEi8MMikbUZD1avz.htm](npc-gallery-items/LEi8MMikbUZD1avz.htm)|Shield Block|auto-trad|
|[LGdgJYiYY4gmLKOx.htm](npc-gallery-items/LGdgJYiYY4gmLKOx.htm)|Surprise Attack|auto-trad|
|[lKG5nFZzi5NYvRU3.htm](npc-gallery-items/lKG5nFZzi5NYvRU3.htm)|Dagger|auto-trad|
|[ll3LaxvkACnszsxH.htm](npc-gallery-items/ll3LaxvkACnszsxH.htm)|Live to Tell the Tale|auto-trad|
|[LMckIZmrAQt7HWW3.htm](npc-gallery-items/LMckIZmrAQt7HWW3.htm)|Unstable Compounds|auto-trad|
|[lpMiICSGm7J1NIMh.htm](npc-gallery-items/lpMiICSGm7J1NIMh.htm)|Snare Crafting|auto-trad|
|[lrHNxw2aet6y4AAg.htm](npc-gallery-items/lrHNxw2aet6y4AAg.htm)|Attack of Opportunity|auto-trad|
|[LtLpVRWe7Qsb7Y8A.htm](npc-gallery-items/LtLpVRWe7Qsb7Y8A.htm)|Bodyguard's Defense|auto-trad|
|[lTt5I810cFQrBGBC.htm](npc-gallery-items/lTt5I810cFQrBGBC.htm)|Arcane Focus Spells|auto-trad|
|[LvkfpNccs65oqyIV.htm](npc-gallery-items/LvkfpNccs65oqyIV.htm)|Precision Edge|auto-trad|
|[LzDRsXeV3aAZe1AF.htm](npc-gallery-items/LzDRsXeV3aAZe1AF.htm)|Cleric Domain Spells|auto-trad|
|[M6mU2n4cS5QZRHvV.htm](npc-gallery-items/M6mU2n4cS5QZRHvV.htm)|Steady Spellcasting|auto-trad|
|[m9he0rG2ytGNLtE9.htm](npc-gallery-items/m9he0rG2ytGNLtE9.htm)|Apprentice's Ambition|auto-trad|
|[ma0kNlN1eN2YPKXO.htm](npc-gallery-items/ma0kNlN1eN2YPKXO.htm)|Heft Crate|auto-trad|
|[McH1gJqQMQRVSagF.htm](npc-gallery-items/McH1gJqQMQRVSagF.htm)|Pewter Mug|auto-trad|
|[mCWRIeeB6PFExTmR.htm](npc-gallery-items/mCWRIeeB6PFExTmR.htm)|Hunt Prey|auto-trad|
|[MePUvMSpXtRD7gNJ.htm](npc-gallery-items/MePUvMSpXtRD7gNJ.htm)|Flail|auto-trad|
|[mgVHJtmU3oAVM8wL.htm](npc-gallery-items/mgVHJtmU3oAVM8wL.htm)|Wizard School Spells|auto-trad|
|[MKdjcAmWOXZkk2Pu.htm](npc-gallery-items/MKdjcAmWOXZkk2Pu.htm)|Catch Rock|auto-trad|
|[MLCrUvDlugE5BKqe.htm](npc-gallery-items/MLCrUvDlugE5BKqe.htm)|Staff|auto-trad|
|[Mlr3n6w8aB9t1a5K.htm](npc-gallery-items/Mlr3n6w8aB9t1a5K.htm)|Infused Items|auto-trad|
|[MlZNOt3aiHAeXqiD.htm](npc-gallery-items/MlZNOt3aiHAeXqiD.htm)|Sap|auto-trad|
|[mOaqXByjpJOQroyH.htm](npc-gallery-items/mOaqXByjpJOQroyH.htm)|Timely Advice|auto-trad|
|[MpZVk32vk8f54dFz.htm](npc-gallery-items/MpZVk32vk8f54dFz.htm)|Crossbow|auto-trad|
|[mtGgMNRGtkuPRFCN.htm](npc-gallery-items/mtGgMNRGtkuPRFCN.htm)|Longsword|auto-trad|
|[muJs58oNvKAXYuXV.htm](npc-gallery-items/muJs58oNvKAXYuXV.htm)|Hatchet|auto-trad|
|[N2HV6ZqTWrwK3PxJ.htm](npc-gallery-items/N2HV6ZqTWrwK3PxJ.htm)|Gavel|auto-trad|
|[N85njvyWLxpngIQ4.htm](npc-gallery-items/N85njvyWLxpngIQ4.htm)|Druid Order Spells|auto-trad|
|[naPxWIBQxbkOgD8M.htm](npc-gallery-items/naPxWIBQxbkOgD8M.htm)|Breach the Abyss|auto-trad|
|[NaQSwHuqBJmhgld8.htm](npc-gallery-items/NaQSwHuqBJmhgld8.htm)|Primal Prepared Spells|auto-trad|
|[nFxNDxHPYmc47Mlx.htm](npc-gallery-items/nFxNDxHPYmc47Mlx.htm)|Dagger|auto-trad|
|[nFYahNWZUkU0gnJ9.htm](npc-gallery-items/nFYahNWZUkU0gnJ9.htm)|Hatchet|auto-trad|
|[nhdJ9mXQ9NXj6n7V.htm](npc-gallery-items/nhdJ9mXQ9NXj6n7V.htm)|+2 Status to Reflex Saves vs. Traps|auto-trad|
|[NjMo70hHW4elTqEW.htm](npc-gallery-items/NjMo70hHW4elTqEW.htm)|Rapier|auto-trad|
|[NKmPaEo138UaRnmO.htm](npc-gallery-items/NKmPaEo138UaRnmO.htm)|Experienced Hand|auto-trad|
|[NLeDblkhTEvLGjwy.htm](npc-gallery-items/NLeDblkhTEvLGjwy.htm)|Reflex Saves|auto-trad|
|[nN82jRXcz7hgkXiE.htm](npc-gallery-items/nN82jRXcz7hgkXiE.htm)|Dagger|auto-trad|
|[NQR9pSqVCQqJO0b7.htm](npc-gallery-items/NQR9pSqVCQqJO0b7.htm)|Bottle|auto-trad|
|[nu42JIeBWdi2MnrN.htm](npc-gallery-items/nu42JIeBWdi2MnrN.htm)|Divine Prepared Spells|auto-trad|
|[NUzbNVXkKMop7XIE.htm](npc-gallery-items/NUzbNVXkKMop7XIE.htm)|Dagger|auto-trad|
|[nV2KEb0hxYozbDOL.htm](npc-gallery-items/nV2KEb0hxYozbDOL.htm)|Weapon Mastery|auto-trad|
|[O4GUtfCmOYOhNgf2.htm](npc-gallery-items/O4GUtfCmOYOhNgf2.htm)|Dagger|auto-trad|
|[o4oz661UQlAOVWiz.htm](npc-gallery-items/o4oz661UQlAOVWiz.htm)|Bottled Lightning|auto-trad|
|[o5K5Jo1qZTIda54i.htm](npc-gallery-items/o5K5Jo1qZTIda54i.htm)|Wizard Prepared Spells|auto-trad|
|[o8GJ3AVTDFtS1dFG.htm](npc-gallery-items/o8GJ3AVTDFtS1dFG.htm)|Sickle|auto-trad|
|[O8PG0gBxUuoMxwH2.htm](npc-gallery-items/O8PG0gBxUuoMxwH2.htm)|Retributive Strike|auto-trad|
|[oAhc8baMBkq1f865.htm](npc-gallery-items/oAhc8baMBkq1f865.htm)|Medical Textbook|auto-trad|
|[oc10jg3Rm2uXz5eg.htm](npc-gallery-items/oc10jg3Rm2uXz5eg.htm)|Crossbow|auto-trad|
|[oc2k5FQjYXACg6rP.htm](npc-gallery-items/oc2k5FQjYXACg6rP.htm)|Stone Pestle|auto-trad|
|[OcO2C4FT46HfzihZ.htm](npc-gallery-items/OcO2C4FT46HfzihZ.htm)|Shovel|auto-trad|
|[OjePIuXOLgAEb4RG.htm](npc-gallery-items/OjePIuXOLgAEb4RG.htm)|No Quarter!|auto-trad|
|[OkdkQX03m4gr7M3e.htm](npc-gallery-items/OkdkQX03m4gr7M3e.htm)|Shield Block|auto-trad|
|[Oq7EJED40zeLYJUJ.htm](npc-gallery-items/Oq7EJED40zeLYJUJ.htm)|Deceiver's Surprise|auto-trad|
|[OqsCqhHLnN6naAWU.htm](npc-gallery-items/OqsCqhHLnN6naAWU.htm)|Scroll Mastery|auto-trad|
|[OSqvPT6Kb8Od7g2m.htm](npc-gallery-items/OSqvPT6Kb8Od7g2m.htm)|Shortsword|auto-trad|
|[oSXltTfrdndgGVfN.htm](npc-gallery-items/oSXltTfrdndgGVfN.htm)|Swinging Strike|auto-trad|
|[OTCwjYQZzd5T0upd.htm](npc-gallery-items/OTCwjYQZzd5T0upd.htm)|Arcane Prepared Spells|auto-trad|
|[owBd74YHegCqxDhR.htm](npc-gallery-items/owBd74YHegCqxDhR.htm)|Occult Spontaneous Spells|auto-trad|
|[owq1vQqeFtTypbuU.htm](npc-gallery-items/owq1vQqeFtTypbuU.htm)|Attack of Opportunity|auto-trad|
|[OzTxpPbEz9oZ7Oxz.htm](npc-gallery-items/OzTxpPbEz9oZ7Oxz.htm)|Mobility|auto-trad|
|[p0JGAWaRJxrH24fB.htm](npc-gallery-items/p0JGAWaRJxrH24fB.htm)|Bardic Lore|auto-trad|
|[p0JJ5Z6nze1w1CXi.htm](npc-gallery-items/p0JJ5Z6nze1w1CXi.htm)|Trained Animal|auto-trad|
|[P0WSBDjQC4AcYXXj.htm](npc-gallery-items/P0WSBDjQC4AcYXXj.htm)|Attack of Opportunity|auto-trad|
|[P2ky9f2YijdmoVk9.htm](npc-gallery-items/P2ky9f2YijdmoVk9.htm)|Sap|auto-trad|
|[P3HCHCil1R8QALJv.htm](npc-gallery-items/P3HCHCil1R8QALJv.htm)|Fist|auto-trad|
|[Pa0KJtbEU8gMH1Qh.htm](npc-gallery-items/Pa0KJtbEU8gMH1Qh.htm)|Torch Combatant|auto-trad|
|[pCgvFzMbb4y6tLfW.htm](npc-gallery-items/pCgvFzMbb4y6tLfW.htm)|Sage's Analysis|auto-trad|
|[pcW9etyXWo6vgBDy.htm](npc-gallery-items/pcW9etyXWo6vgBDy.htm)|Shortsword|auto-trad|
|[pCZpELfahQYNki30.htm](npc-gallery-items/pCZpELfahQYNki30.htm)|Cutlery|auto-trad|
|[PDidp7qMSw5qRoXU.htm](npc-gallery-items/PDidp7qMSw5qRoXU.htm)|Quick Draw|auto-trad|
|[phnggMzXOyEB9A1S.htm](npc-gallery-items/phnggMzXOyEB9A1S.htm)|Club|auto-trad|
|[PlvlB09wLGdgmyo3.htm](npc-gallery-items/PlvlB09wLGdgmyo3.htm)|Attack of Opportunity|auto-trad|
|[PqeleLSAo4wtVlzd.htm](npc-gallery-items/PqeleLSAo4wtVlzd.htm)|Composite Longbow|auto-trad|
|[pvLUpob2OzcBpXiY.htm](npc-gallery-items/pvLUpob2OzcBpXiY.htm)|Cite Precedent|auto-trad|
|[pZ0qTSs1z35vx57T.htm](npc-gallery-items/pZ0qTSs1z35vx57T.htm)|Hand Crossbow|auto-trad|
|[pZfnSGB52sgqtEKM.htm](npc-gallery-items/pZfnSGB52sgqtEKM.htm)|Crossbow|auto-trad|
|[q0BbteyMJiNAGthF.htm](npc-gallery-items/q0BbteyMJiNAGthF.htm)|Frost Vial|auto-trad|
|[q2DuiV2Er7pxJDvH.htm](npc-gallery-items/q2DuiV2Er7pxJDvH.htm)|Sneak Attack|auto-trad|
|[Q3RfoNXTp5dIw2Mx.htm](npc-gallery-items/Q3RfoNXTp5dIw2Mx.htm)|Behead|auto-trad|
|[q8RnxJPpBW9f5Ie5.htm](npc-gallery-items/q8RnxJPpBW9f5Ie5.htm)|Forager|auto-trad|
|[Q95qJwP5SCa0IlGu.htm](npc-gallery-items/Q95qJwP5SCa0IlGu.htm)|Medical Malpractice|auto-trad|
|[Q9h6X4WFBsWo4KlV.htm](npc-gallery-items/Q9h6X4WFBsWo4KlV.htm)|Rock|auto-trad|
|[qfzrHQkkZcEFrhFT.htm](npc-gallery-items/qfzrHQkkZcEFrhFT.htm)|You're Next|auto-trad|
|[qIRZdfRLhqBnYMWt.htm](npc-gallery-items/qIRZdfRLhqBnYMWt.htm)|Chart a Course|auto-trad|
|[qmCssarLjtwLY11r.htm](npc-gallery-items/qmCssarLjtwLY11r.htm)|Darkvision|auto-trad|
|[qNEPB3RKC3SlV4YE.htm](npc-gallery-items/qNEPB3RKC3SlV4YE.htm)|Surprise Attack|auto-trad|
|[qOhRxpDdOg4c6aNs.htm](npc-gallery-items/qOhRxpDdOg4c6aNs.htm)|Bedside Manner|auto-trad|
|[qQfETAyjFjGSYQV3.htm](npc-gallery-items/qQfETAyjFjGSYQV3.htm)|Shield Block|auto-trad|
|[qu7Gla2a7DxXInkc.htm](npc-gallery-items/qu7Gla2a7DxXInkc.htm)|Warhammer|auto-trad|
|[qw0aIOiOvQLo7bUz.htm](npc-gallery-items/qw0aIOiOvQLo7bUz.htm)|Crossbow|auto-trad|
|[Qw88JvXNINA3GNOT.htm](npc-gallery-items/Qw88JvXNINA3GNOT.htm)|Moderate Alchemist's Fire|auto-trad|
|[QW9oExZljGxszfI0.htm](npc-gallery-items/QW9oExZljGxszfI0.htm)|Crossbow|auto-trad|
|[qy60SQR1jiyW4CXc.htm](npc-gallery-items/qy60SQR1jiyW4CXc.htm)|Snagging Strike|auto-trad|
|[r19Dz88pBsPS4lU2.htm](npc-gallery-items/r19Dz88pBsPS4lU2.htm)|Medical Wisdom|auto-trad|
|[R3Qe1BmMhgp4UHSo.htm](npc-gallery-items/R3Qe1BmMhgp4UHSo.htm)|Moderate Acid Flask|auto-trad|
|[R4Ai5HX7MjgLGEyG.htm](npc-gallery-items/R4Ai5HX7MjgLGEyG.htm)|Dangerous Sorcery|auto-trad|
|[R5pKOSIyZ8tVn26G.htm](npc-gallery-items/R5pKOSIyZ8tVn26G.htm)|Dagger|auto-trad|
|[Rgd5z8pZXoiA9cop.htm](npc-gallery-items/Rgd5z8pZXoiA9cop.htm)|Staff|auto-trad|
|[RnJieV2KgJwl8PgM.htm](npc-gallery-items/RnJieV2KgJwl8PgM.htm)|Intimidating Strike|auto-trad|
|[RoENPeDeCIhLDS5H.htm](npc-gallery-items/RoENPeDeCIhLDS5H.htm)|Trident|auto-trad|
|[rTeH6wIQdfQpy5CH.htm](npc-gallery-items/rTeH6wIQdfQpy5CH.htm)|Hunt Prey|auto-trad|
|[rWb3x7u63QvEkLfQ.htm](npc-gallery-items/rWb3x7u63QvEkLfQ.htm)|Divine Prepared Spells|auto-trad|
|[rwsjmSGbF6Nuxl2R.htm](npc-gallery-items/rwsjmSGbF6Nuxl2R.htm)|Darkvision|auto-trad|
|[rxhsNPSPQ1q9Zs6z.htm](npc-gallery-items/rxhsNPSPQ1q9Zs6z.htm)|Living Sextant|auto-trad|
|[RzSdb5m56ixW5tfw.htm](npc-gallery-items/RzSdb5m56ixW5tfw.htm)|Mace|auto-trad|
|[s0IuVvw3zTYVeGYV.htm](npc-gallery-items/s0IuVvw3zTYVeGYV.htm)|Bastard Sword|auto-trad|
|[S0MOf3jyFwTZYtAP.htm](npc-gallery-items/S0MOf3jyFwTZYtAP.htm)|Naval Pike|auto-trad|
|[s4BkXBiBAgyWmBrU.htm](npc-gallery-items/s4BkXBiBAgyWmBrU.htm)|Deny Advantage|auto-trad|
|[S4syqUe0hjyn6BQn.htm](npc-gallery-items/S4syqUe0hjyn6BQn.htm)|Crossbow|auto-trad|
|[sDwLsrlMceRxlzIo.htm](npc-gallery-items/sDwLsrlMceRxlzIo.htm)|Deny Advantage|auto-trad|
|[sDz2pTgrdn7OJC6y.htm](npc-gallery-items/sDz2pTgrdn7OJC6y.htm)|Occult Spontaneous Spells|auto-trad|
|[ShvU5c3eNvpu7NSs.htm](npc-gallery-items/ShvU5c3eNvpu7NSs.htm)|Shortsword|auto-trad|
|[SjfWr7t0YSZQdt8c.htm](npc-gallery-items/SjfWr7t0YSZQdt8c.htm)|Snare Crafting|auto-trad|
|[skjl8IwD4XG0LeLw.htm](npc-gallery-items/skjl8IwD4XG0LeLw.htm)|Sink or Swim|auto-trad|
|[SPtYCE6lLVtQjkar.htm](npc-gallery-items/SPtYCE6lLVtQjkar.htm)|Divine Spontaneous Spells|auto-trad|
|[SvCtowtaw8hmMZKz.htm](npc-gallery-items/SvCtowtaw8hmMZKz.htm)|Hatchet|auto-trad|
|[SvKEuXIi5eUaqkLA.htm](npc-gallery-items/SvKEuXIi5eUaqkLA.htm)|Warhammer|auto-trad|
|[sw2mWVFaQb93J9el.htm](npc-gallery-items/sw2mWVFaQb93J9el.htm)|Forager|auto-trad|
|[sWmESzQJUt1c3Xuu.htm](npc-gallery-items/sWmESzQJUt1c3Xuu.htm)|Sea Legs|auto-trad|
|[T0cpCJqKGkWPXW4L.htm](npc-gallery-items/T0cpCJqKGkWPXW4L.htm)|Royal Defender|auto-trad|
|[T3ZpHsIT64RGyhT0.htm](npc-gallery-items/T3ZpHsIT64RGyhT0.htm)|Power of the Mob|auto-trad|
|[t5skSqnTS4aKuuan.htm](npc-gallery-items/t5skSqnTS4aKuuan.htm)|Surprise Attack|auto-trad|
|[t7aKwycZ4ehsFtFB.htm](npc-gallery-items/t7aKwycZ4ehsFtFB.htm)|Hatchet|auto-trad|
|[T7oZcH7OKDm1O4cw.htm](npc-gallery-items/T7oZcH7OKDm1O4cw.htm)|Aura of Command|auto-trad|
|[t8D9jbYaoXXUxjCe.htm](npc-gallery-items/t8D9jbYaoXXUxjCe.htm)|Versatile Performance|auto-trad|
|[taX3Q0r1h37NNYFO.htm](npc-gallery-items/taX3Q0r1h37NNYFO.htm)|Hazard Spotter|auto-trad|
|[tCqd5uhyOKVs0yco.htm](npc-gallery-items/tCqd5uhyOKVs0yco.htm)|Protect the Master!|auto-trad|
|[tDia9PqZk9lPblrF.htm](npc-gallery-items/tDia9PqZk9lPblrF.htm)|Appraising Eye|auto-trad|
|[TEKRQ6lf1xZxEPIz.htm](npc-gallery-items/TEKRQ6lf1xZxEPIz.htm)|Fist|auto-trad|
|[tMYrLjnqdp2h4CWk.htm](npc-gallery-items/tMYrLjnqdp2h4CWk.htm)|Swim Away|auto-trad|
|[ttkiwONkYUn5piHI.htm](npc-gallery-items/ttkiwONkYUn5piHI.htm)|War Flail|auto-trad|
|[ttSdjINAYV0VxPZk.htm](npc-gallery-items/ttSdjINAYV0VxPZk.htm)|Sneak Attack|auto-trad|
|[TvnwhUBwaljk26uo.htm](npc-gallery-items/TvnwhUBwaljk26uo.htm)|Dagger|auto-trad|
|[tWDvrIy7tMRedOAa.htm](npc-gallery-items/tWDvrIy7tMRedOAa.htm)|Master Tracker|auto-trad|
|[tYBrwLh84hOMoQb4.htm](npc-gallery-items/tYBrwLh84hOMoQb4.htm)|Bastard Sword|auto-trad|
|[TztjddoEZphVsSDa.htm](npc-gallery-items/TztjddoEZphVsSDa.htm)|Focused Thinker|auto-trad|
|[u3RFdo6uA3yPenzC.htm](npc-gallery-items/u3RFdo6uA3yPenzC.htm)|Light Hammer|auto-trad|
|[u6QEft9DxfittKI2.htm](npc-gallery-items/u6QEft9DxfittKI2.htm)|Bosun's Command|auto-trad|
|[u7AfocCKAnj4LGQO.htm](npc-gallery-items/u7AfocCKAnj4LGQO.htm)|Staff|auto-trad|
|[ue4Y2G3CCizn6XEG.htm](npc-gallery-items/ue4Y2G3CCizn6XEG.htm)|Sneak Attack|auto-trad|
|[uE9nqIQImKOG8Vr0.htm](npc-gallery-items/uE9nqIQImKOG8Vr0.htm)|Hidden Blade|auto-trad|
|[Uee48lu9JRn91tRp.htm](npc-gallery-items/Uee48lu9JRn91tRp.htm)|Pick|auto-trad|
|[Ufe5H0f9R4O2VBNV.htm](npc-gallery-items/Ufe5H0f9R4O2VBNV.htm)|Nature's Edge|auto-trad|
|[ufJVnIjBde8zNbAj.htm](npc-gallery-items/ufJVnIjBde8zNbAj.htm)|Dagger|auto-trad|
|[ug2TQxIeBOWQren2.htm](npc-gallery-items/ug2TQxIeBOWQren2.htm)|Sorcerer Bloodline Spells|auto-trad|
|[uglCIhXIhI66IGsk.htm](npc-gallery-items/uglCIhXIhI66IGsk.htm)|Crimson Vengeance|auto-trad|
|[UhDwKtMaA99YE2vC.htm](npc-gallery-items/UhDwKtMaA99YE2vC.htm)|Sudden Charge|auto-trad|
|[uMvy7NgRGRADz6zB.htm](npc-gallery-items/uMvy7NgRGRADz6zB.htm)|Staff|auto-trad|
|[uozTPO1slJcKBEax.htm](npc-gallery-items/uozTPO1slJcKBEax.htm)|Dagger|auto-trad|
|[UQoNtmlZuh2NnKd0.htm](npc-gallery-items/UQoNtmlZuh2NnKd0.htm)|Noble's Ruse|auto-trad|
|[UticIspMVAd6QGkp.htm](npc-gallery-items/UticIspMVAd6QGkp.htm)|Divine Focus Spells|auto-trad|
|[UTr5XgqL7eUu8pyH.htm](npc-gallery-items/UTr5XgqL7eUu8pyH.htm)|Low-Light Vision|auto-trad|
|[uUISfTBeVRNhnN9u.htm](npc-gallery-items/uUISfTBeVRNhnN9u.htm)|Fist|auto-trad|
|[UWagoDkGXzULZ0i4.htm](npc-gallery-items/UWagoDkGXzULZ0i4.htm)|Sap|auto-trad|
|[UwdvjmLBy5IaWIWn.htm](npc-gallery-items/UwdvjmLBy5IaWIWn.htm)|Sneak Attack|auto-trad|
|[UZZJYmev69dOUgKu.htm](npc-gallery-items/UZZJYmev69dOUgKu.htm)|Composite Longbow|auto-trad|
|[v3VQfpzwMQ3vAQCU.htm](npc-gallery-items/v3VQfpzwMQ3vAQCU.htm)|Scoundrel's Feint|auto-trad|
|[V3WALYE32ZPtfiRt.htm](npc-gallery-items/V3WALYE32ZPtfiRt.htm)|Sap|auto-trad|
|[VCHM4U1i2BqAQJdz.htm](npc-gallery-items/VCHM4U1i2BqAQJdz.htm)|Staff|auto-trad|
|[vfhKr3Ly9XjE7PhV.htm](npc-gallery-items/vfhKr3Ly9XjE7PhV.htm)|Occult Focus Spells|auto-trad|
|[VfIDgw7C87841Ehg.htm](npc-gallery-items/VfIDgw7C87841Ehg.htm)|Unshakable|auto-trad|
|[vgPpoUOgBxWLd4Ne.htm](npc-gallery-items/vgPpoUOgBxWLd4Ne.htm)|Steady Balance|auto-trad|
|[VJxxeLCAgKz1s9HI.htm](npc-gallery-items/VJxxeLCAgKz1s9HI.htm)|Abyssal Temptation|auto-trad|
|[VKwW4ylsv5KU7Nhr.htm](npc-gallery-items/VKwW4ylsv5KU7Nhr.htm)|Dread Stalker|auto-trad|
|[vmjC8rwYO5CA0CjY.htm](npc-gallery-items/vmjC8rwYO5CA0CjY.htm)|Bardic Lore|auto-trad|
|[vOk7ftvRMJXLkNWY.htm](npc-gallery-items/vOk7ftvRMJXLkNWY.htm)|Hand Crossbow|auto-trad|
|[vprpkjJKcOBq6m7X.htm](npc-gallery-items/vprpkjJKcOBq6m7X.htm)|Rock|auto-trad|
|[Vqjb5Zoa5sSTgnDg.htm](npc-gallery-items/Vqjb5Zoa5sSTgnDg.htm)|Composite Longbow|auto-trad|
|[VQPPHF2M9xHf5XD3.htm](npc-gallery-items/VQPPHF2M9xHf5XD3.htm)|Sneak Attack|auto-trad|
|[vrubZ4BfdgD4AmTp.htm](npc-gallery-items/vrubZ4BfdgD4AmTp.htm)|Shortsword|auto-trad|
|[vtgkJ23YrCFVTyTP.htm](npc-gallery-items/vtgkJ23YrCFVTyTP.htm)|Warding Strike|auto-trad|
|[VtGXx6gZ4Ezl8Mcp.htm](npc-gallery-items/VtGXx6gZ4Ezl8Mcp.htm)|Pewter Mug|auto-trad|
|[vyJ3imlXVxUHGVq3.htm](npc-gallery-items/vyJ3imlXVxUHGVq3.htm)|Weapon Mastery (Hammer)|auto-trad|
|[Vz4DYAJUl9kelFni.htm](npc-gallery-items/Vz4DYAJUl9kelFni.htm)|Staff|auto-trad|
|[vZlRJzB7o08xfLuI.htm](npc-gallery-items/vZlRJzB7o08xfLuI.htm)|Light Hammer|auto-trad|
|[VzPHszVrqprSLuY7.htm](npc-gallery-items/VzPHszVrqprSLuY7.htm)|Primal Focus Spells|auto-trad|
|[w021SRzRisqhAnKU.htm](npc-gallery-items/w021SRzRisqhAnKU.htm)|Darkvision|auto-trad|
|[W3jbnf0RJFBtiRto.htm](npc-gallery-items/W3jbnf0RJFBtiRto.htm)|Halberd|auto-trad|
|[wDgSQGJhEyvpYeZE.htm](npc-gallery-items/wDgSQGJhEyvpYeZE.htm)|Persistent Lies|auto-trad|
|[we5UuMRtG5niRCBP.htm](npc-gallery-items/we5UuMRtG5niRCBP.htm)|Thunderstone|auto-trad|
|[WFByucZQz0Q9slLk.htm](npc-gallery-items/WFByucZQz0Q9slLk.htm)|Sling|auto-trad|
|[wfNSTwrVHkHF5YLS.htm](npc-gallery-items/wfNSTwrVHkHF5YLS.htm)|Hand Crossbow|auto-trad|
|[WGBYwG2C4eElCrPd.htm](npc-gallery-items/WGBYwG2C4eElCrPd.htm)|Collaborative Thievery|auto-trad|
|[WHOLkEuYaCAMaFl1.htm](npc-gallery-items/WHOLkEuYaCAMaFl1.htm)|Noble's Ally|auto-trad|
|[wiKF84BoxIlLdggN.htm](npc-gallery-items/wiKF84BoxIlLdggN.htm)|Surprise Attack|auto-trad|
|[wLxqjBiRMADtMrlV.htm](npc-gallery-items/wLxqjBiRMADtMrlV.htm)|Light Hammer|auto-trad|
|[wnR3RwCNys0DRqxB.htm](npc-gallery-items/wnR3RwCNys0DRqxB.htm)|Sap|auto-trad|
|[wPucfhAZe8Uqamyu.htm](npc-gallery-items/wPucfhAZe8Uqamyu.htm)|Crossbow|auto-trad|
|[WRQQun80G8om7SIp.htm](npc-gallery-items/WRQQun80G8om7SIp.htm)|Morningstar|auto-trad|
|[WrTvPVOo3UOYpMWC.htm](npc-gallery-items/WrTvPVOo3UOYpMWC.htm)|Boarding Action|auto-trad|
|[wvyx4nEskJ9Imojt.htm](npc-gallery-items/wvyx4nEskJ9Imojt.htm)|Barkeep's Advice|auto-trad|
|[wwLWKH0F1mtPQ2lE.htm](npc-gallery-items/wwLWKH0F1mtPQ2lE.htm)|+1 to Sense Motive|auto-trad|
|[wXgvnztT7r9jO7Px.htm](npc-gallery-items/wXgvnztT7r9jO7Px.htm)|Brutal Rally|auto-trad|
|[wxpvY7YmCPTSk3p9.htm](npc-gallery-items/wxpvY7YmCPTSk3p9.htm)|Raise a Shield|auto-trad|
|[wYUI9xidJaJLYynz.htm](npc-gallery-items/wYUI9xidJaJLYynz.htm)|The Jig Is Up|auto-trad|
|[WZzoPHc7f4dTn3iC.htm](npc-gallery-items/WZzoPHc7f4dTn3iC.htm)|Shortsword|auto-trad|
|[X7QR8UnnfPH6vOWK.htm](npc-gallery-items/X7QR8UnnfPH6vOWK.htm)|Subdue Prisoners|auto-trad|
|[X8NQcsCgEiQHiZj6.htm](npc-gallery-items/X8NQcsCgEiQHiZj6.htm)|Arcane Prepared Spells|auto-trad|
|[x8s1JWo1Jcm4elFK.htm](npc-gallery-items/x8s1JWo1Jcm4elFK.htm)|+1 Status to All Saves vs. Poison|auto-trad|
|[xBb6evQkhB3uYAvn.htm](npc-gallery-items/xBb6evQkhB3uYAvn.htm)|Rage|auto-trad|
|[Xbpr8x90JdxlOqrV.htm](npc-gallery-items/Xbpr8x90JdxlOqrV.htm)|Occult Spontaneous Spells|auto-trad|
|[XCCNdWgAwDmYJNUr.htm](npc-gallery-items/XCCNdWgAwDmYJNUr.htm)|Occult Spontaneous Spells|auto-trad|
|[XCtQzAvxzCKT6NIl.htm](npc-gallery-items/XCtQzAvxzCKT6NIl.htm)|Stench of Decay|auto-trad|
|[XdNbqGPaLBs6RCOX.htm](npc-gallery-items/XdNbqGPaLBs6RCOX.htm)|Dagger|auto-trad|
|[xeEI4dqDE4PmxOeA.htm](npc-gallery-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|auto-trad|
|[XFo44PTU8F1rJam8.htm](npc-gallery-items/XFo44PTU8F1rJam8.htm)|Destructive Vengeance|auto-trad|
|[XHVHeWTU0zqmjJWZ.htm](npc-gallery-items/XHVHeWTU0zqmjJWZ.htm)|Whip|auto-trad|
|[xhZKk4oQRgs08i0T.htm](npc-gallery-items/xhZKk4oQRgs08i0T.htm)|Nimble Dodge|auto-trad|
|[xI5EGbyi2XbV0z6e.htm](npc-gallery-items/xI5EGbyi2XbV0z6e.htm)|Fist|auto-trad|
|[xkwTvTqSPNbk86Un.htm](npc-gallery-items/xkwTvTqSPNbk86Un.htm)|Crossbow|auto-trad|
|[Xo7vz0G7GjTFg4yo.htm](npc-gallery-items/Xo7vz0G7GjTFg4yo.htm)|Light Mace|auto-trad|
|[XPYr9hMESgVNjbVp.htm](npc-gallery-items/XPYr9hMESgVNjbVp.htm)|Trick Attack|auto-trad|
|[XqNEuTQGsReodN8h.htm](npc-gallery-items/XqNEuTQGsReodN8h.htm)|Sap|auto-trad|
|[xS8f6u8VDcBepVGM.htm](npc-gallery-items/xS8f6u8VDcBepVGM.htm)|Demon Summoning|auto-trad|
|[xuyjTEDoDAmb5acN.htm](npc-gallery-items/xuyjTEDoDAmb5acN.htm)|Sap|auto-trad|
|[XVoLXwXlo8BPWO8P.htm](npc-gallery-items/XVoLXwXlo8BPWO8P.htm)|Spiked Gauntlet|auto-trad|
|[XxRBSksVZSF2n79w.htm](npc-gallery-items/XxRBSksVZSF2n79w.htm)|Dagger|auto-trad|
|[XXu57gmSKMHk38fa.htm](npc-gallery-items/XXu57gmSKMHk38fa.htm)|Rock|auto-trad|
|[Y4ZPmVCbnUAexfGS.htm](npc-gallery-items/Y4ZPmVCbnUAexfGS.htm)|+1 Circumstance to All Saves vs. Traps|auto-trad|
|[y5n9AfgPzGvGtS69.htm](npc-gallery-items/y5n9AfgPzGvGtS69.htm)|Rock|auto-trad|
|[y77GcQxQjHDAiISd.htm](npc-gallery-items/y77GcQxQjHDAiISd.htm)|Sling|auto-trad|
|[y9nL7kklEoVqLV06.htm](npc-gallery-items/y9nL7kklEoVqLV06.htm)|Favored Terrain|auto-trad|
|[yAk7aiu08rTizR5c.htm](npc-gallery-items/yAk7aiu08rTizR5c.htm)|Rapier|auto-trad|
|[YB9T72k74oWO9L6c.htm](npc-gallery-items/YB9T72k74oWO9L6c.htm)|Low-Light Vision|auto-trad|
|[YCf4WMESlKy1zF6l.htm](npc-gallery-items/YCf4WMESlKy1zF6l.htm)|Sneak Attack|auto-trad|
|[Ye6jF7F3YdRigibJ.htm](npc-gallery-items/Ye6jF7F3YdRigibJ.htm)|Methodical Research|auto-trad|
|[yEcvRwueCdGqs9GC.htm](npc-gallery-items/yEcvRwueCdGqs9GC.htm)|Dagger|auto-trad|
|[YEsqZ1pmCAbdEHfI.htm](npc-gallery-items/YEsqZ1pmCAbdEHfI.htm)|Champion Devotion Spells|auto-trad|
|[YgMj9CaUadOgAwPl.htm](npc-gallery-items/YgMj9CaUadOgAwPl.htm)|Scalpel|auto-trad|
|[yJqGrkv3kpN6caAd.htm](npc-gallery-items/yJqGrkv3kpN6caAd.htm)|Weapon Mastery|auto-trad|
|[yM3snMjrKoj8gXaq.htm](npc-gallery-items/yM3snMjrKoj8gXaq.htm)|Club|auto-trad|
|[YMK6yeIqUwKCrXpF.htm](npc-gallery-items/YMK6yeIqUwKCrXpF.htm)|Composite Shortbow|auto-trad|
|[YT49F2uCWIqKs6Ct.htm](npc-gallery-items/YT49F2uCWIqKs6Ct.htm)|Divine Spontaneous Spells|auto-trad|
|[YTWYrskTTtVm4yze.htm](npc-gallery-items/YTWYrskTTtVm4yze.htm)|Composite Longbow|auto-trad|
|[Yxoxo2Zh5olZHceb.htm](npc-gallery-items/Yxoxo2Zh5olZHceb.htm)|Shield Block|auto-trad|
|[Yzt7iAgbGNFAn1NW.htm](npc-gallery-items/Yzt7iAgbGNFAn1NW.htm)|+1 Circumstance Bonus on Saves vs. Poisons|auto-trad|
|[ZcVfcVHFVcJZhIQI.htm](npc-gallery-items/ZcVfcVHFVcJZhIQI.htm)|Greataxe|auto-trad|
|[zecbW6qIhXuKa8Yu.htm](npc-gallery-items/zecbW6qIhXuKa8Yu.htm)|Fist|auto-trad|
|[zeH3OrBLioIRoYb5.htm](npc-gallery-items/zeH3OrBLioIRoYb5.htm)|Deny Advantage|auto-trad|
|[zg7ntU3VEOypwaws.htm](npc-gallery-items/zg7ntU3VEOypwaws.htm)|Attack of Opportunity|auto-trad|
|[zICeN9ExAbhXEjzA.htm](npc-gallery-items/zICeN9ExAbhXEjzA.htm)|Infused Items|auto-trad|
|[ziPz3z2a6Db63N9r.htm](npc-gallery-items/ziPz3z2a6Db63N9r.htm)|Reckless Alchemy|auto-trad|
|[zKVUU94IzSXLEDLT.htm](npc-gallery-items/zKVUU94IzSXLEDLT.htm)|Crossbow|auto-trad|
|[zmdPdUEf9SjhHnTL.htm](npc-gallery-items/zmdPdUEf9SjhHnTL.htm)|Sneak Attack|auto-trad|
|[ZmfzXnEaPSxGh2J4.htm](npc-gallery-items/ZmfzXnEaPSxGh2J4.htm)|Quick Catch|auto-trad|
|[zMkrKhyfRWFrFfuv.htm](npc-gallery-items/zMkrKhyfRWFrFfuv.htm)|Fanatical Frenzy|auto-trad|
|[zS04tb23VRw9X9kX.htm](npc-gallery-items/zS04tb23VRw9X9kX.htm)|Nimble Dodge|auto-trad|
|[ZtFKTs16yHUY48WF.htm](npc-gallery-items/ZtFKTs16yHUY48WF.htm)|Healing Hands|auto-trad|
|[ZuDGBQWUf1cSV69U.htm](npc-gallery-items/ZuDGBQWUf1cSV69U.htm)|Call to Action|auto-trad|
|[zYZvGGQfgJZyoNMQ.htm](npc-gallery-items/zYZvGGQfgJZyoNMQ.htm)|Dagger|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[3EX2JaX17gPKVhcb.htm](npc-gallery-items/3EX2JaX17gPKVhcb.htm)|Main-Gauche|Main-gauche|modificada|
|[wOWNuciIMk1EpXis.htm](npc-gallery-items/wOWNuciIMk1EpXis.htm)|Holy Water|Agua bendita|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
