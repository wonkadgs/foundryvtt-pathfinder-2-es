# Estado de la traducción (adventure-specific-actions)

 * **auto-trad**: 91
 * **modificada**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0nyMrziUqqRcSxXD.htm](adventure-specific-actions/0nyMrziUqqRcSxXD.htm)|Fight Fires|auto-trad|
|[0z53rJzCIGysXGTy.htm](adventure-specific-actions/0z53rJzCIGysXGTy.htm)|Fire the Cannons! (5-6)|auto-trad|
|[236E6kEEayy8h2CF.htm](adventure-specific-actions/236E6kEEayy8h2CF.htm)|Position the Hunters|auto-trad|
|[2Kz5whXqnRsL4fEl.htm](adventure-specific-actions/2Kz5whXqnRsL4fEl.htm)|Practical Research|auto-trad|
|[2Vrg2RkEdOteyp5O.htm](adventure-specific-actions/2Vrg2RkEdOteyp5O.htm)|Perform a Trick|auto-trad|
|[3E5TMUDCSEI5x2bv.htm](adventure-specific-actions/3E5TMUDCSEI5x2bv.htm)|Smooth the Path|auto-trad|
|[3P0rSASN69wXV6Fw.htm](adventure-specific-actions/3P0rSASN69wXV6Fw.htm)|Approach Duneshadow|auto-trad|
|[3Pr6Jk6AobttQRqN.htm](adventure-specific-actions/3Pr6Jk6AobttQRqN.htm)|Forge Documents|auto-trad|
|[43vVOSRi8Lnk1Ril.htm](adventure-specific-actions/43vVOSRi8Lnk1Ril.htm)|Investigate Chamber|auto-trad|
|[52zcawpATnCLh4J9.htm](adventure-specific-actions/52zcawpATnCLh4J9.htm)|Send in the Clowns|auto-trad|
|[56ajIlNy3SEvP9Ud.htm](adventure-specific-actions/56ajIlNy3SEvP9Ud.htm)|Costar|auto-trad|
|[5DVnTrN2xt3NINGZ.htm](adventure-specific-actions/5DVnTrN2xt3NINGZ.htm)|Make General Repairs|auto-trad|
|[5QQqv1A3aG6G2Sui.htm](adventure-specific-actions/5QQqv1A3aG6G2Sui.htm)|Rebuild Collapsed Stairs|auto-trad|
|[6hrDW8tF6CDIaZl7.htm](adventure-specific-actions/6hrDW8tF6CDIaZl7.htm)|Dream Research|auto-trad|
|[764EVGVADrDSbdqu.htm](adventure-specific-actions/764EVGVADrDSbdqu.htm)|Forgive Foe|auto-trad|
|[799p70dXF3UYkTih.htm](adventure-specific-actions/799p70dXF3UYkTih.htm)|Communicate From Beyond|auto-trad|
|[7WczIXwjOs9raMja.htm](adventure-specific-actions/7WczIXwjOs9raMja.htm)|Fey Luck|auto-trad|
|[9NWMchSkF940L0SW.htm](adventure-specific-actions/9NWMchSkF940L0SW.htm)|Invoke Eiseth|auto-trad|
|[aCFKA59YgGDNnSJ8.htm](adventure-specific-actions/aCFKA59YgGDNnSJ8.htm)|Exhale Poison|auto-trad|
|[aUgAKW3SWhU9ATl8.htm](adventure-specific-actions/aUgAKW3SWhU9ATl8.htm)|Repair Crumbled Walls|auto-trad|
|[AvpKHblov4xjANaH.htm](adventure-specific-actions/AvpKHblov4xjANaH.htm)|Build Training Facility|auto-trad|
|[aw2JzLBAsmO8dD3r.htm](adventure-specific-actions/aw2JzLBAsmO8dD3r.htm)|Fire the Cannons! (3-4)|auto-trad|
|[awJY2ylrsVEJNTFi.htm](adventure-specific-actions/awJY2ylrsVEJNTFi.htm)|Pander to the Crowd|auto-trad|
|[Bh1Qnh8JP4CMysRK.htm](adventure-specific-actions/Bh1Qnh8JP4CMysRK.htm)|Shootist's Draw|auto-trad|
|[BnKNrkcM6F3v0p7s.htm](adventure-specific-actions/BnKNrkcM6F3v0p7s.htm)|Haul Supplies|auto-trad|
|[Bw2L4guiRFphTpF1.htm](adventure-specific-actions/Bw2L4guiRFphTpF1.htm)|Appeal to Shadowy Intruders|auto-trad|
|[cGgeAlsnsSQPSPDQ.htm](adventure-specific-actions/cGgeAlsnsSQPSPDQ.htm)|Upgrade Defenses|auto-trad|
|[cp8eFBCr1n7xIaxq.htm](adventure-specific-actions/cp8eFBCr1n7xIaxq.htm)|Rebuild Battlements|auto-trad|
|[CRiItJtc8N9Hc0X0.htm](adventure-specific-actions/CRiItJtc8N9Hc0X0.htm)|Explore the Vault of Boundless Wonder|auto-trad|
|[Dir7OyCss7H1XQGX.htm](adventure-specific-actions/Dir7OyCss7H1XQGX.htm)|Organize Labor|auto-trad|
|[dqE9pP9Pv0JG9d8X.htm](adventure-specific-actions/dqE9pP9Pv0JG9d8X.htm)|Smuggled|auto-trad|
|[EUU6zUeCzYm9jIhT.htm](adventure-specific-actions/EUU6zUeCzYm9jIhT.htm)|Find the Cells|auto-trad|
|[FLz8SEF0Y4UEavvD.htm](adventure-specific-actions/FLz8SEF0Y4UEavvD.htm)|Hunt the Animals|auto-trad|
|[fUNCyoyLgpIYFLe1.htm](adventure-specific-actions/fUNCyoyLgpIYFLe1.htm)|Influence Guild|auto-trad|
|[FyT7VwMCJjjHDSgO.htm](adventure-specific-actions/FyT7VwMCJjjHDSgO.htm)|Contact Steel Falcons|auto-trad|
|[GhahZSxNPSrabaA6.htm](adventure-specific-actions/GhahZSxNPSrabaA6.htm)|Scout the Facility|auto-trad|
|[HcASfeYcE8QXayfk.htm](adventure-specific-actions/HcASfeYcE8QXayfk.htm)|Check the Walls|auto-trad|
|[hQ5BAIjAKpp2dYhR.htm](adventure-specific-actions/hQ5BAIjAKpp2dYhR.htm)|Build Connections|auto-trad|
|[hq5KwJRPbTVcoD3k.htm](adventure-specific-actions/hq5KwJRPbTVcoD3k.htm)|Dispel a Disguise|auto-trad|
|[hvvzc86tW5MgElMB.htm](adventure-specific-actions/hvvzc86tW5MgElMB.htm)|Assessing Tatzlford's Defenses|auto-trad|
|[I19VNyhXYaFCpsxl.htm](adventure-specific-actions/I19VNyhXYaFCpsxl.htm)|Scout Duneshadow|auto-trad|
|[IQewgylxmkmBYcoY.htm](adventure-specific-actions/IQewgylxmkmBYcoY.htm)|Secure Invitation|auto-trad|
|[iQfHTjg9dNLbuzr8.htm](adventure-specific-actions/iQfHTjg9dNLbuzr8.htm)|Clear Courtyard|auto-trad|
|[IuD5u9tSTabZ0KD1.htm](adventure-specific-actions/IuD5u9tSTabZ0KD1.htm)|Build Library|auto-trad|
|[jwo5CvftA5puYp7i.htm](adventure-specific-actions/jwo5CvftA5puYp7i.htm)|Mesmerizing Performance|auto-trad|
|[KIU5eDZP9VyQIfas.htm](adventure-specific-actions/KIU5eDZP9VyQIfas.htm)|Protector's Interdiction|auto-trad|
|[KmpPAOjNP980NuCY.htm](adventure-specific-actions/KmpPAOjNP980NuCY.htm)|Repair Huntergate|auto-trad|
|[KsYvgnBNvdC23gnC.htm](adventure-specific-actions/KsYvgnBNvdC23gnC.htm)|Erect Barricades|auto-trad|
|[L8UbCtOYzgEutput.htm](adventure-specific-actions/L8UbCtOYzgEutput.htm)|Fight the Fire|auto-trad|
|[lD2RA75awEu4cG7e.htm](adventure-specific-actions/lD2RA75awEu4cG7e.htm)|Promote the Circus|auto-trad|
|[LRuwz61jNmIfQYby.htm](adventure-specific-actions/LRuwz61jNmIfQYby.htm)|Shed Time|auto-trad|
|[lySoX0VbIaEEEnDZ.htm](adventure-specific-actions/lySoX0VbIaEEEnDZ.htm)|Clean|auto-trad|
|[m9Si1ygkv9ISjKVN.htm](adventure-specific-actions/m9Si1ygkv9ISjKVN.htm)|Navigate Steamgrotto|auto-trad|
|[MgSwLes5lp3TE1ZV.htm](adventure-specific-actions/MgSwLes5lp3TE1ZV.htm)|Mental Ward|auto-trad|
|[mluc8JLd20HjGrqu.htm](adventure-specific-actions/mluc8JLd20HjGrqu.htm)|Convince Mengkare|auto-trad|
|[nLLgAxo4IHebsyg1.htm](adventure-specific-actions/nLLgAxo4IHebsyg1.htm)|Deadly Traps|auto-trad|
|[nmgmPqExUZt5u5Wr.htm](adventure-specific-actions/nmgmPqExUZt5u5Wr.htm)|Rotate the Wheel|auto-trad|
|[nVdoKUIWaJ47xMuB.htm](adventure-specific-actions/nVdoKUIWaJ47xMuB.htm)|Distract Guards|auto-trad|
|[o3u4snDwjBDcNlG3.htm](adventure-specific-actions/o3u4snDwjBDcNlG3.htm)|Topple Crates|auto-trad|
|[O4MWAxAYxNkndVnt.htm](adventure-specific-actions/O4MWAxAYxNkndVnt.htm)|Shortcut Through the Wastes|auto-trad|
|[o6hu1my40jfcLHqD.htm](adventure-specific-actions/o6hu1my40jfcLHqD.htm)|Host Event|auto-trad|
|[OSFi8oH5ndLgnksD.htm](adventure-specific-actions/OSFi8oH5ndLgnksD.htm)|Search the Laughing Jungle|auto-trad|
|[OV77buFW6zHl4Smo.htm](adventure-specific-actions/OV77buFW6zHl4Smo.htm)|Breaking and Entering|auto-trad|
|[OxRT8qhujKG9Rhb2.htm](adventure-specific-actions/OxRT8qhujKG9Rhb2.htm)|Build Infirmary|auto-trad|
|[pQIgAdZucEEWCMfL.htm](adventure-specific-actions/pQIgAdZucEEWCMfL.htm)|Gather Information|auto-trad|
|[pWxRtCeAw4XnyzoM.htm](adventure-specific-actions/pWxRtCeAw4XnyzoM.htm)|Post Snipers|auto-trad|
|[Qkm7jcKPA3elk9Nx.htm](adventure-specific-actions/Qkm7jcKPA3elk9Nx.htm)|Administer|auto-trad|
|[QW2gYIKce3W31xXf.htm](adventure-specific-actions/QW2gYIKce3W31xXf.htm)|Prove Peace|auto-trad|
|[R90HXdiRwl0Fa4wb.htm](adventure-specific-actions/R90HXdiRwl0Fa4wb.htm)|Loot the Vaults|auto-trad|
|[rI9qKyONeMPtajZ8.htm](adventure-specific-actions/rI9qKyONeMPtajZ8.htm)|Build Workshop (Crafting)|auto-trad|
|[rKWfjflS15KEB3Yt.htm](adventure-specific-actions/rKWfjflS15KEB3Yt.htm)|De-Animating Gestures (False)|auto-trad|
|[rR8UYHHpo2Rffi1p.htm](adventure-specific-actions/rR8UYHHpo2Rffi1p.htm)|De-Animating Gestures (True)|auto-trad|
|[RX62MAyEUtuHMNBm.htm](adventure-specific-actions/RX62MAyEUtuHMNBm.htm)|Guild Investigation|auto-trad|
|[sT8EpCnySUSiBqBp.htm](adventure-specific-actions/sT8EpCnySUSiBqBp.htm)|Locked Doors|auto-trad|
|[sWTvJahHpdz4CC6E.htm](adventure-specific-actions/sWTvJahHpdz4CC6E.htm)|Recruit Wildlife|auto-trad|
|[tay92zbn04IR40qv.htm](adventure-specific-actions/tay92zbn04IR40qv.htm)|Prepare Firepots|auto-trad|
|[TM4pOPSM9r7XEM64.htm](adventure-specific-actions/TM4pOPSM9r7XEM64.htm)|Soul Ward|auto-trad|
|[Vt6CuD83hPiyoiOZ.htm](adventure-specific-actions/Vt6CuD83hPiyoiOZ.htm)|Study|auto-trad|
|[WKFZlmmuZGnucRen.htm](adventure-specific-actions/WKFZlmmuZGnucRen.htm)|Breaking the Chains|auto-trad|
|[WlXmO2kwyWGgAuOv.htm](adventure-specific-actions/WlXmO2kwyWGgAuOv.htm)|Deduce Traditions|auto-trad|
|[x5hIMfjmsDlpQWyt.htm](adventure-specific-actions/x5hIMfjmsDlpQWyt.htm)|Blend In|auto-trad|
|[xklnt1hmdFnix64F.htm](adventure-specific-actions/xklnt1hmdFnix64F.htm)|Rescue Citizens|auto-trad|
|[XTDd73QzhETeXY3g.htm](adventure-specific-actions/XTDd73QzhETeXY3g.htm)|Secure Disguises|auto-trad|
|[XUNM9eqfhnSaPVov.htm](adventure-specific-actions/XUNM9eqfhnSaPVov.htm)|Steal Keys|auto-trad|
|[XyfC1zPgSTP01ZUr.htm](adventure-specific-actions/XyfC1zPgSTP01ZUr.htm)|Cram|auto-trad|
|[Ys6rFLuyxocQy2hA.htm](adventure-specific-actions/Ys6rFLuyxocQy2hA.htm)|Seek the Hidden Forge|auto-trad|
|[YvSjpOAI9bCDQU5h.htm](adventure-specific-actions/YvSjpOAI9bCDQU5h.htm)|Seek the Animals|auto-trad|
|[yy5PeDyV7kIWlPOU.htm](adventure-specific-actions/yy5PeDyV7kIWlPOU.htm)|Influence Regent|auto-trad|
|[z1noqZiavhnqQL50.htm](adventure-specific-actions/z1noqZiavhnqQL50.htm)|Set Traps|auto-trad|
|[zF3q1nTANa5NYYJu.htm](adventure-specific-actions/zF3q1nTANa5NYYJu.htm)|Cunning Disguise|auto-trad|
|[zIwbbth7qyKraiWV.htm](adventure-specific-actions/zIwbbth7qyKraiWV.htm)|Issue Challenge|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[G8xZPhzoLF1SGyV9.htm](adventure-specific-actions/G8xZPhzoLF1SGyV9.htm)|Diviner on Duty|Adivinación de servicio|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
