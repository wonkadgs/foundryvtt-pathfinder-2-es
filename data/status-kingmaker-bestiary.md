# Estado de la traducción (kingmaker-bestiary)

 * **auto-trad**: 234
 * **modificada**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0EO5vP2gCliAxLrF.htm](kingmaker-bestiary/0EO5vP2gCliAxLrF.htm)|Nok-Nok (Level 5)|auto-trad|
|[0EQySWHT7D68yrrx.htm](kingmaker-bestiary/0EQySWHT7D68yrrx.htm)|Jubilost (Level 8)|auto-trad|
|[0qc6h3jgLFNhX1tG.htm](kingmaker-bestiary/0qc6h3jgLFNhX1tG.htm)|Ballista Defense|auto-trad|
|[1SEyDYO9l6mcFhoy.htm](kingmaker-bestiary/1SEyDYO9l6mcFhoy.htm)|Ekundayo (Level 1)|auto-trad|
|[1upVC05DY7YzxNsr.htm](kingmaker-bestiary/1upVC05DY7YzxNsr.htm)|Chew Spider|auto-trad|
|[1UWbR2WkeP0kl1nQ.htm](kingmaker-bestiary/1UWbR2WkeP0kl1nQ.htm)|Giant Trapdoor Spider|auto-trad|
|[1vsQlCVwa9kVmgRi.htm](kingmaker-bestiary/1vsQlCVwa9kVmgRi.htm)|Avatar of the Lantern King|auto-trad|
|[1ZKOTqqzeorIv7BB.htm](kingmaker-bestiary/1ZKOTqqzeorIv7BB.htm)|Trapdoor Ogre Spider|auto-trad|
|[2deePNLiJcnSZQhd.htm](kingmaker-bestiary/2deePNLiJcnSZQhd.htm)|Test of Strength|auto-trad|
|[2mkfF43tP8IGVfBz.htm](kingmaker-bestiary/2mkfF43tP8IGVfBz.htm)|Foras|auto-trad|
|[3gtQv6Mkr7CUlG7W.htm](kingmaker-bestiary/3gtQv6Mkr7CUlG7W.htm)|Wild Hunt Archer|auto-trad|
|[3HnIluPWsKm3eEYB.htm](kingmaker-bestiary/3HnIluPWsKm3eEYB.htm)|Flooding Room|auto-trad|
|[3kyS4KzEmG8Eyl4N.htm](kingmaker-bestiary/3kyS4KzEmG8Eyl4N.htm)|Thresholder Hermeticist|auto-trad|
|[3QkgiJJ0IS6oTT0t.htm](kingmaker-bestiary/3QkgiJJ0IS6oTT0t.htm)|River Elasmosaurus|auto-trad|
|[3Tb42wMuwjtP3iYN.htm](kingmaker-bestiary/3Tb42wMuwjtP3iYN.htm)|The Lonely Warrior|auto-trad|
|[3xHHKI7PGQN218aL.htm](kingmaker-bestiary/3xHHKI7PGQN218aL.htm)|Eobald|auto-trad|
|[48o307dry03xazvd.htm](kingmaker-bestiary/48o307dry03xazvd.htm)|Corax|auto-trad|
|[554zNFkbYXBdmgAy.htm](kingmaker-bestiary/554zNFkbYXBdmgAy.htm)|Sister of the Bloodshot Eye|auto-trad|
|[5npzwOJSQLSPhmx2.htm](kingmaker-bestiary/5npzwOJSQLSPhmx2.htm)|Pavetta Stroon-Drelev|auto-trad|
|[5Xslhvyh3MSzgIXN.htm](kingmaker-bestiary/5Xslhvyh3MSzgIXN.htm)|Volodmyra|auto-trad|
|[6BFUxkEBKPr89SNt.htm](kingmaker-bestiary/6BFUxkEBKPr89SNt.htm)|Mandragora Swarm|auto-trad|
|[6zWcjvPya0wWDZv2.htm](kingmaker-bestiary/6zWcjvPya0wWDZv2.htm)|Risen Fetch|auto-trad|
|[7bWjM5e9sKxaYbOw.htm](kingmaker-bestiary/7bWjM5e9sKxaYbOw.htm)|Thorn River Bandit|auto-trad|
|[7os3GIj69tIqXTGl.htm](kingmaker-bestiary/7os3GIj69tIqXTGl.htm)|Wild Hunt Horse|auto-trad|
|[7PkMXEkBrxhTCw4s.htm](kingmaker-bestiary/7PkMXEkBrxhTCw4s.htm)|Horagnamon|auto-trad|
|[8cRXmQ9vd93T0m2N.htm](kingmaker-bestiary/8cRXmQ9vd93T0m2N.htm)|Cyclops Zombie|auto-trad|
|[8eTfnYg0xmcgmDVQ.htm](kingmaker-bestiary/8eTfnYg0xmcgmDVQ.htm)|Irovetti's Fetch|auto-trad|
|[8IYynuqZxDpXZwBE.htm](kingmaker-bestiary/8IYynuqZxDpXZwBE.htm)|Tartuccio|auto-trad|
|[8YJiyhWSWxMdGJV6.htm](kingmaker-bestiary/8YJiyhWSWxMdGJV6.htm)|The Power of Faith|auto-trad|
|[9nGhRYarE5KkwOwc.htm](kingmaker-bestiary/9nGhRYarE5KkwOwc.htm)|Nilak|auto-trad|
|[AA3hdVlJhALthghl.htm](kingmaker-bestiary/AA3hdVlJhALthghl.htm)|Enormous Flame Drake|auto-trad|
|[aEz68WBHT9F5YlXz.htm](kingmaker-bestiary/aEz68WBHT9F5YlXz.htm)|Castruccio Irovetti|auto-trad|
|[AGNRcnrftglAtlbN.htm](kingmaker-bestiary/AGNRcnrftglAtlbN.htm)|Gromog|auto-trad|
|[AjpJNvt4nwzDAhqT.htm](kingmaker-bestiary/AjpJNvt4nwzDAhqT.htm)|Villamor Koth|auto-trad|
|[aoICPc2KqxIP19m2.htm](kingmaker-bestiary/aoICPc2KqxIP19m2.htm)|Valerie (Level 9)|auto-trad|
|[Awlsx9BtgkQ39x6N.htm](kingmaker-bestiary/Awlsx9BtgkQ39x6N.htm)|Aldori Sister|auto-trad|
|[AXmQ8rUKsJFZUKb6.htm](kingmaker-bestiary/AXmQ8rUKsJFZUKb6.htm)|Collapsing Bridge|auto-trad|
|[B0EZMtvXsIE3SYiu.htm](kingmaker-bestiary/B0EZMtvXsIE3SYiu.htm)|Locking Alarm|auto-trad|
|[BEo6xNEhEKPsUzIn.htm](kingmaker-bestiary/BEo6xNEhEKPsUzIn.htm)|Hooktongue|auto-trad|
|[bF0FHdZMWl1OuRae.htm](kingmaker-bestiary/bF0FHdZMWl1OuRae.htm)|Hill Giant Butcher|auto-trad|
|[bHnGsHHymPUC1XGc.htm](kingmaker-bestiary/bHnGsHHymPUC1XGc.htm)|Auchs|auto-trad|
|[BJbe6BzLSIx8Jsix.htm](kingmaker-bestiary/BJbe6BzLSIx8Jsix.htm)|Storm-Struck Arboreal|auto-trad|
|[bjMn6cRNFK9jgfe0.htm](kingmaker-bestiary/bjMn6cRNFK9jgfe0.htm)|Bog Mummy Cultist|auto-trad|
|[Bq2eST0Ke4MnKXgR.htm](kingmaker-bestiary/Bq2eST0Ke4MnKXgR.htm)|Grigori|auto-trad|
|[bwEYizrdHs8lmvxE.htm](kingmaker-bestiary/bwEYizrdHs8lmvxE.htm)|Leng Envoy|auto-trad|
|[BWI2CbtRo2lUcraX.htm](kingmaker-bestiary/BWI2CbtRo2lUcraX.htm)|Bloom Cultist|auto-trad|
|[ce5vC049lfuXnPSy.htm](kingmaker-bestiary/ce5vC049lfuXnPSy.htm)|Orb Blast Trap|auto-trad|
|[Cj2zn7JazTB9ZnIq.htm](kingmaker-bestiary/Cj2zn7JazTB9ZnIq.htm)|Tiger Lord Hill Giant (TL2)|auto-trad|
|[cjQVmbjqONihI9SR.htm](kingmaker-bestiary/cjQVmbjqONihI9SR.htm)|Fen Pudding|auto-trad|
|[cPcregujkG0LUHiG.htm](kingmaker-bestiary/cPcregujkG0LUHiG.htm)|Evindra|auto-trad|
|[defXhBIK4TtoZXGK.htm](kingmaker-bestiary/defXhBIK4TtoZXGK.htm)|The Stag Lord|auto-trad|
|[Dxn6t9aoWUwPn6CE.htm](kingmaker-bestiary/Dxn6t9aoWUwPn6CE.htm)|Virthad|auto-trad|
|[dZEl1W8zV3rj5D9O.htm](kingmaker-bestiary/dZEl1W8zV3rj5D9O.htm)|Old Crackjaw|auto-trad|
|[E70Drr6CmSaJQ01v.htm](kingmaker-bestiary/E70Drr6CmSaJQ01v.htm)|Spiral Seal|auto-trad|
|[Ea0Edd9XNA17yj9n.htm](kingmaker-bestiary/Ea0Edd9XNA17yj9n.htm)|Stag Lord Bandit|auto-trad|
|[eAu4SOvMaNISD3RZ.htm](kingmaker-bestiary/eAu4SOvMaNISD3RZ.htm)|Vordakai|auto-trad|
|[EI8wQc9kzooDHQoJ.htm](kingmaker-bestiary/EI8wQc9kzooDHQoJ.htm)|Weakened Floor|auto-trad|
|[EM2mwJzZeu5rWIQS.htm](kingmaker-bestiary/EM2mwJzZeu5rWIQS.htm)|Ekundayo (Level 6)|auto-trad|
|[etIP2Mdv3Xnr0wto.htm](kingmaker-bestiary/etIP2Mdv3Xnr0wto.htm)|Ancient Wisp|auto-trad|
|[etrlNmJRUWDKbKwG.htm](kingmaker-bestiary/etrlNmJRUWDKbKwG.htm)|Terrion Numesti|auto-trad|
|[eVHkWtrdGJMVMob7.htm](kingmaker-bestiary/eVHkWtrdGJMVMob7.htm)|Dovan from Nisroch|auto-trad|
|[EvNCuVyY5sEGM4ZL.htm](kingmaker-bestiary/EvNCuVyY5sEGM4ZL.htm)|Hannis Drelev|auto-trad|
|[F0uz9YV8ILAM8fIg.htm](kingmaker-bestiary/F0uz9YV8ILAM8fIg.htm)|Windchaser|auto-trad|
|[fbsy6zPV4HjHiis7.htm](kingmaker-bestiary/fbsy6zPV4HjHiis7.htm)|Bloomborn Athach|auto-trad|
|[fiBhPqM2lomswplt.htm](kingmaker-bestiary/fiBhPqM2lomswplt.htm)|Kereek|auto-trad|
|[FkSxn7QSbVqA3dMy.htm](kingmaker-bestiary/FkSxn7QSbVqA3dMy.htm)|Test of Endurance|auto-trad|
|[fQ9FuovHuRt6vtcq.htm](kingmaker-bestiary/fQ9FuovHuRt6vtcq.htm)|Wild Hunt Scout|auto-trad|
|[fs1iFoZmJF1iUWwX.htm](kingmaker-bestiary/fs1iFoZmJF1iUWwX.htm)|Davik Nettles|auto-trad|
|[GDBEHLicn4kKggis.htm](kingmaker-bestiary/GDBEHLicn4kKggis.htm)|Murder of Crows|auto-trad|
|[gdTJOwXUwwhKAzlR.htm](kingmaker-bestiary/gdTJOwXUwwhKAzlR.htm)|Nishkiv the Knife|auto-trad|
|[GiRckUiMamrdgjXQ.htm](kingmaker-bestiary/GiRckUiMamrdgjXQ.htm)|Cutthroat Haunt|auto-trad|
|[gNtXGquzueNJLvFJ.htm](kingmaker-bestiary/gNtXGquzueNJLvFJ.htm)|Dread Aura|auto-trad|
|[GviFe34FuTpo8AT0.htm](kingmaker-bestiary/GviFe34FuTpo8AT0.htm)|Scalding Tar Lake|auto-trad|
|[gXbDaY9ci2u22ptT.htm](kingmaker-bestiary/gXbDaY9ci2u22ptT.htm)|Ghostly Guard|auto-trad|
|[h73Up6EQZqtgh6gP.htm](kingmaker-bestiary/h73Up6EQZqtgh6gP.htm)|Jamandi Aldori|auto-trad|
|[hdFT5WIarw2Do3Sy.htm](kingmaker-bestiary/hdFT5WIarw2Do3Sy.htm)|Tristian (Level 1)|auto-trad|
|[hFHdGOMRuhXIuAJo.htm](kingmaker-bestiary/hFHdGOMRuhXIuAJo.htm)|Test of Agility|auto-trad|
|[hGLp8mkvx8J8DDL8.htm](kingmaker-bestiary/hGLp8mkvx8J8DDL8.htm)|Malgorzata Niska|auto-trad|
|[hGQ4uxhxwtrnGfj0.htm](kingmaker-bestiary/hGQ4uxhxwtrnGfj0.htm)|Rickety Bridge|auto-trad|
|[HkdsqQLNb9XwzYIH.htm](kingmaker-bestiary/HkdsqQLNb9XwzYIH.htm)|Tiger Lord|auto-trad|
|[hLBHFloWuXLjCQYH.htm](kingmaker-bestiary/hLBHFloWuXLjCQYH.htm)|Primal Bandersnatch|auto-trad|
|[hLvxvLere6DruDLJ.htm](kingmaker-bestiary/hLvxvLere6DruDLJ.htm)|Annamede Belavarah|auto-trad|
|[hQ0aR4oXug0yoTbT.htm](kingmaker-bestiary/hQ0aR4oXug0yoTbT.htm)|Unstable Pit|auto-trad|
|[HRLmXOCMwyw5HAfw.htm](kingmaker-bestiary/HRLmXOCMwyw5HAfw.htm)|Camouflaged Spiked Pit|auto-trad|
|[htHgsx1COWOfhE3D.htm](kingmaker-bestiary/htHgsx1COWOfhE3D.htm)|Thresholder Mystic|auto-trad|
|[hW40C78kV4MBDs4v.htm](kingmaker-bestiary/hW40C78kV4MBDs4v.htm)|Teleport Trap|auto-trad|
|[hX3uMf6KxgObJ9ec.htm](kingmaker-bestiary/hX3uMf6KxgObJ9ec.htm)|Valerie (Level 1)|auto-trad|
|[hywUu3wVDuXePx3e.htm](kingmaker-bestiary/hywUu3wVDuXePx3e.htm)|Centaur Scout|auto-trad|
|[I8IPTHEU1zF5KmAB.htm](kingmaker-bestiary/I8IPTHEU1zF5KmAB.htm)|Boggard Warden|auto-trad|
|[I8pvGB7SiXQ6SnGn.htm](kingmaker-bestiary/I8pvGB7SiXQ6SnGn.htm)|Stygian Fires|auto-trad|
|[IGVqtFsNIyghuVsD.htm](kingmaker-bestiary/IGVqtFsNIyghuVsD.htm)|Agai|auto-trad|
|[j7fPCy71EfQL1KmU.htm](kingmaker-bestiary/j7fPCy71EfQL1KmU.htm)|Nyrissa|auto-trad|
|[jFA0q3g3MsFxS3xO.htm](kingmaker-bestiary/jFA0q3g3MsFxS3xO.htm)|Goblin Bat-Dog|auto-trad|
|[JLfy3tVKEhLqT2j5.htm](kingmaker-bestiary/JLfy3tVKEhLqT2j5.htm)|Shelyn's Shame|auto-trad|
|[jmH2nbWRJSYgzx5z.htm](kingmaker-bestiary/jmH2nbWRJSYgzx5z.htm)|Jin Durwhimmer|auto-trad|
|[jUNnWUuiVueOTvHt.htm](kingmaker-bestiary/jUNnWUuiVueOTvHt.htm)|The Misbegotten Troll|auto-trad|
|[K61HTqmdHRPHQz1x.htm](kingmaker-bestiary/K61HTqmdHRPHQz1x.htm)|Tulvak|auto-trad|
|[k9eR5UwCSrfpPPAv.htm](kingmaker-bestiary/k9eR5UwCSrfpPPAv.htm)|Hateful Hermit|auto-trad|
|[kiUlJn4FzWMzgkbW.htm](kingmaker-bestiary/kiUlJn4FzWMzgkbW.htm)|Talon Peak Roc|auto-trad|
|[Kmr8s4sEMn365d5M.htm](kingmaker-bestiary/Kmr8s4sEMn365d5M.htm)|Prank Workshop Mitflit|auto-trad|
|[KP7uf1E8CQgzTuQy.htm](kingmaker-bestiary/KP7uf1E8CQgzTuQy.htm)|Nugrah|auto-trad|
|[KqWZZBucIAA1MzjF.htm](kingmaker-bestiary/KqWZZBucIAA1MzjF.htm)|Jubilost (Level 1)|auto-trad|
|[kSR0D8dLXTlw09NT.htm](kingmaker-bestiary/kSR0D8dLXTlw09NT.htm)|Gedovius|auto-trad|
|[L3q7yQ0jKqH2IWy7.htm](kingmaker-bestiary/L3q7yQ0jKqH2IWy7.htm)|Nightmare Rook|auto-trad|
|[L6ANJQoCyk5dWcdH.htm](kingmaker-bestiary/L6ANJQoCyk5dWcdH.htm)|Elder Elemental Tsunami|auto-trad|
|[LdIVntI4ho9eKTVt.htm](kingmaker-bestiary/LdIVntI4ho9eKTVt.htm)|The Dancing Lady|auto-trad|
|[LF67gTrSqSb7h4KZ.htm](kingmaker-bestiary/LF67gTrSqSb7h4KZ.htm)|Boggard Cultist|auto-trad|
|[Lm30gxMpkRJ2Y43d.htm](kingmaker-bestiary/Lm30gxMpkRJ2Y43d.htm)|Satinder Morne|auto-trad|
|[lRxISnNmcfUm7AfG.htm](kingmaker-bestiary/lRxISnNmcfUm7AfG.htm)|Grabbles|auto-trad|
|[lutSSPcXOzDDqIGj.htm](kingmaker-bestiary/lutSSPcXOzDDqIGj.htm)|Gurija|auto-trad|
|[lZtkMlyix3kaTO0j.htm](kingmaker-bestiary/lZtkMlyix3kaTO0j.htm)|The Beast|auto-trad|
|[M7hMPdEbaC1RNwfY.htm](kingmaker-bestiary/M7hMPdEbaC1RNwfY.htm)|Kob Moleg|auto-trad|
|[m8kwG6NskYDlBSCy.htm](kingmaker-bestiary/m8kwG6NskYDlBSCy.htm)|Ankou Assassin|auto-trad|
|[MABh0eh1VKh3izdf.htm](kingmaker-bestiary/MABh0eh1VKh3izdf.htm)|Eldritch Echoes|auto-trad|
|[mbAhAq5OyZAv6lq5.htm](kingmaker-bestiary/mbAhAq5OyZAv6lq5.htm)|Darivan|auto-trad|
|[mgh7E2Mh0ZRaniCc.htm](kingmaker-bestiary/mgh7E2Mh0ZRaniCc.htm)|King Vesket|auto-trad|
|[MgUBst46K9Hv0qsJ.htm](kingmaker-bestiary/MgUBst46K9Hv0qsJ.htm)|Amiri (Level 1, Kingmaker)|auto-trad|
|[MlWXttLN4MjyzTGr.htm](kingmaker-bestiary/MlWXttLN4MjyzTGr.htm)|Dropping Web Trap|auto-trad|
|[mMfMs5PlNYkwe55s.htm](kingmaker-bestiary/mMfMs5PlNYkwe55s.htm)|Alasen|auto-trad|
|[mNKAaSBWbZHQRdo9.htm](kingmaker-bestiary/mNKAaSBWbZHQRdo9.htm)|Endless Struggle|auto-trad|
|[MqxA7COGMXc5CNsZ.htm](kingmaker-bestiary/MqxA7COGMXc5CNsZ.htm)|Black Tear Cutthroat|auto-trad|
|[MUuXMpUGEnqmElgT.htm](kingmaker-bestiary/MUuXMpUGEnqmElgT.htm)|Vicious Army Ant Swarm|auto-trad|
|[myNEeBzXVmWbHk2X.htm](kingmaker-bestiary/myNEeBzXVmWbHk2X.htm)|Azure Lilies|auto-trad|
|[n82GZhM6joceE91v.htm](kingmaker-bestiary/n82GZhM6joceE91v.htm)|Ilthuliak|auto-trad|
|[nDFogS2qQJomjfmR.htm](kingmaker-bestiary/nDFogS2qQJomjfmR.htm)|Fetch Stalker|auto-trad|
|[ndoXVn6MPPxSJvcC.htm](kingmaker-bestiary/ndoXVn6MPPxSJvcC.htm)|Black Smilodon|auto-trad|
|[ni0RSuVeUgs5WmlY.htm](kingmaker-bestiary/ni0RSuVeUgs5WmlY.htm)|Hillstomper|auto-trad|
|[NMCee869vhJjP5Ri.htm](kingmaker-bestiary/NMCee869vhJjP5Ri.htm)|The Wriggling Man|auto-trad|
|[NSY6VAGs9VrKzyRX.htm](kingmaker-bestiary/NSY6VAGs9VrKzyRX.htm)|Armag Twice-Born|auto-trad|
|[Nx6vcagWXhYToIdC.htm](kingmaker-bestiary/Nx6vcagWXhYToIdC.htm)|Jurgrindor|auto-trad|
|[NxVi4ot2bzNOk6Zj.htm](kingmaker-bestiary/NxVi4ot2bzNOk6Zj.htm)|Aecora Silverfire|auto-trad|
|[O5EEnXdrKPcODuwh.htm](kingmaker-bestiary/O5EEnXdrKPcODuwh.htm)|Sir Fredero Sinnet|auto-trad|
|[oBPdL0icCG5zmknB.htm](kingmaker-bestiary/oBPdL0icCG5zmknB.htm)|Glyph of Warding (Kingmaker)|auto-trad|
|[obT2KQ8YYlRxvSWr.htm](kingmaker-bestiary/obT2KQ8YYlRxvSWr.htm)|Tree that Weeps|auto-trad|
|[OhuPV1g5LfejhtAz.htm](kingmaker-bestiary/OhuPV1g5LfejhtAz.htm)|The Horned Hunter|auto-trad|
|[oM1AvORITfhzwrDk.htm](kingmaker-bestiary/oM1AvORITfhzwrDk.htm)|Barbtongued Wyvern|auto-trad|
|[OnHIutiVLt1czwWL.htm](kingmaker-bestiary/OnHIutiVLt1czwWL.htm)|Wild Hunt Hound|auto-trad|
|[oOPf7VG4tuMvzrgA.htm](kingmaker-bestiary/oOPf7VG4tuMvzrgA.htm)|Dog (Ekundayo's Companion)|auto-trad|
|[oqlZNnsV8XMGn7JN.htm](kingmaker-bestiary/oqlZNnsV8XMGn7JN.htm)|Praise of Yog-Sothoth|auto-trad|
|[ovjnD3aiPgRi2C7u.htm](kingmaker-bestiary/ovjnD3aiPgRi2C7u.htm)|The Gardener|auto-trad|
|[P1kWLRlEPTcZ3uzD.htm](kingmaker-bestiary/P1kWLRlEPTcZ3uzD.htm)|Kellid Graveknight|auto-trad|
|[pcct13qdrriJf3OL.htm](kingmaker-bestiary/pcct13qdrriJf3OL.htm)|Akuzhail|auto-trad|
|[pD5Y7gJtqlr2A4a2.htm](kingmaker-bestiary/pD5Y7gJtqlr2A4a2.htm)|Hargulka|auto-trad|
|[pfvUhfy0VHNlTyvN.htm](kingmaker-bestiary/pfvUhfy0VHNlTyvN.htm)|Immense Mandragora|auto-trad|
|[PK8yBANFyMqFZ3IY.htm](kingmaker-bestiary/PK8yBANFyMqFZ3IY.htm)|Zorek|auto-trad|
|[pT5hfxcsG7eV5oxh.htm](kingmaker-bestiary/pT5hfxcsG7eV5oxh.htm)|Niodrhast|auto-trad|
|[pZosztihhMtCLinT.htm](kingmaker-bestiary/pZosztihhMtCLinT.htm)|Wild Hunt Monarch|auto-trad|
|[QF2AIby1vQRq5b9E.htm](kingmaker-bestiary/QF2AIby1vQRq5b9E.htm)|Ameon Trask|auto-trad|
|[QkGk4GMq3pCtBbLS.htm](kingmaker-bestiary/QkGk4GMq3pCtBbLS.htm)|Troll Guard|auto-trad|
|[qLfHY6uNUQ99NZei.htm](kingmaker-bestiary/qLfHY6uNUQ99NZei.htm)|Werendegar|auto-trad|
|[qpkpPFlN0dSKJxaR.htm](kingmaker-bestiary/qpkpPFlN0dSKJxaR.htm)|Breeg's Traps|auto-trad|
|[qS7JwIPqsjNKKALK.htm](kingmaker-bestiary/qS7JwIPqsjNKKALK.htm)|The First Faithful|auto-trad|
|[RfIipAkVucpR0f0x.htm](kingmaker-bestiary/RfIipAkVucpR0f0x.htm)|Oleg|auto-trad|
|[RL6cxasbeQMtCDvV.htm](kingmaker-bestiary/RL6cxasbeQMtCDvV.htm)|Chief Sootscale|auto-trad|
|[RN3Fiz9AZzUuqb9z.htm](kingmaker-bestiary/RN3Fiz9AZzUuqb9z.htm)|Smoke-Filled Hallway|auto-trad|
|[rnFjLc6xYYPtXS6a.htm](kingmaker-bestiary/rnFjLc6xYYPtXS6a.htm)|Defaced Naiad Queen|auto-trad|
|[rsm5ZSX6oKJWQRvf.htm](kingmaker-bestiary/rsm5ZSX6oKJWQRvf.htm)|Nyrissa's Tempest|auto-trad|
|[RTd4FwqGq8gBjdAO.htm](kingmaker-bestiary/RTd4FwqGq8gBjdAO.htm)|Melianse|auto-trad|
|[rUmPNDqvptyp5Ob4.htm](kingmaker-bestiary/rUmPNDqvptyp5Ob4.htm)|Test of Tactics|auto-trad|
|[s0BfmFWAhLQkQEbg.htm](kingmaker-bestiary/s0BfmFWAhLQkQEbg.htm)|Kargstaad's Giant|auto-trad|
|[S3jESLRaGeoaHG7t.htm](kingmaker-bestiary/S3jESLRaGeoaHG7t.htm)|Exploding Bloom Pods|auto-trad|
|[S4oIMaVPzQuRaTpK.htm](kingmaker-bestiary/S4oIMaVPzQuRaTpK.htm)|Phomandala|auto-trad|
|[SaSXYUpSWIFHIzET.htm](kingmaker-bestiary/SaSXYUpSWIFHIzET.htm)|Pitax Warden|auto-trad|
|[sedubjznhIbVfCkD.htm](kingmaker-bestiary/sedubjznhIbVfCkD.htm)|Sepoko|auto-trad|
|[sF7JAULkMpi1QMz2.htm](kingmaker-bestiary/sF7JAULkMpi1QMz2.htm)|Amiri (Level 11, Kingmaker)|auto-trad|
|[SfFMqKTUQ1Dwu5lT.htm](kingmaker-bestiary/SfFMqKTUQ1Dwu5lT.htm)|Whimwyrm|auto-trad|
|[SjU0oB6pOk0XY8VN.htm](kingmaker-bestiary/SjU0oB6pOk0XY8VN.htm)|Minognos-Ushad|auto-trad|
|[sMCEMlNngFINMX8y.htm](kingmaker-bestiary/sMCEMlNngFINMX8y.htm)|Elga Verniex|auto-trad|
|[sNqAajzeDA9BUkfa.htm](kingmaker-bestiary/sNqAajzeDA9BUkfa.htm)|Explosion Bear|auto-trad|
|[so1XCkNLe2tuNbzW.htm](kingmaker-bestiary/so1XCkNLe2tuNbzW.htm)|Tristian (Level 10)|auto-trad|
|[SVUjDSZHYmwbQgnq.htm](kingmaker-bestiary/SVUjDSZHYmwbQgnq.htm)|The Knurly Witch|auto-trad|
|[sw32QdZlsWnmWaVY.htm](kingmaker-bestiary/sw32QdZlsWnmWaVY.htm)|Mastiff Of Tindalos|auto-trad|
|[sY8owbk9TFeygFL9.htm](kingmaker-bestiary/sY8owbk9TFeygFL9.htm)|Nok-Nok (Level 1)|auto-trad|
|[SZVTHAwTVXDwIOqC.htm](kingmaker-bestiary/SZVTHAwTVXDwIOqC.htm)|Gaetane|auto-trad|
|[TAEgPgivuUOyuEU5.htm](kingmaker-bestiary/TAEgPgivuUOyuEU5.htm)|Falling Portcullis|auto-trad|
|[TfwvPnETjUhEUQ82.htm](kingmaker-bestiary/TfwvPnETjUhEUQ82.htm)|Trapped Portcullis|auto-trad|
|[tjq87ghubOcPAXjj.htm](kingmaker-bestiary/tjq87ghubOcPAXjj.htm)|Fetch Behemoth|auto-trad|
|[TLoNfIIhS7YGdV54.htm](kingmaker-bestiary/TLoNfIIhS7YGdV54.htm)|Thylacine|auto-trad|
|[TOW9azHYIoaNSavI.htm](kingmaker-bestiary/TOW9azHYIoaNSavI.htm)|Hidden Pressure Plate|auto-trad|
|[tq87VRZjkGBmW8kf.htm](kingmaker-bestiary/tq87VRZjkGBmW8kf.htm)|Rezatha|auto-trad|
|[UAlHSl6Cpujld1dx.htm](kingmaker-bestiary/UAlHSl6Cpujld1dx.htm)|Logger|auto-trad|
|[UBwmJpIyIV65U7R2.htm](kingmaker-bestiary/UBwmJpIyIV65U7R2.htm)|Kressle|auto-trad|
|[UfB3NfSgZIkN5Rjx.htm](kingmaker-bestiary/UfB3NfSgZIkN5Rjx.htm)|Cleansed Cultist|auto-trad|
|[ugzdSsP9U0gGLZ3v.htm](kingmaker-bestiary/ugzdSsP9U0gGLZ3v.htm)|Rigg Gargadilly|auto-trad|
|[uLzD70CB7Bh2XxQf.htm](kingmaker-bestiary/uLzD70CB7Bh2XxQf.htm)|Shambler|auto-trad|
|[UPESZZbXchcuqI1r.htm](kingmaker-bestiary/UPESZZbXchcuqI1r.htm)|Engelidis|auto-trad|
|[uQbzVX7DWDbxLONd.htm](kingmaker-bestiary/uQbzVX7DWDbxLONd.htm)|Paranoia Well|auto-trad|
|[UXXEOnvp2MDaS9Sc.htm](kingmaker-bestiary/UXXEOnvp2MDaS9Sc.htm)|Vilderavn Herald|auto-trad|
|[v2cJC9tdjRHexMwa.htm](kingmaker-bestiary/v2cJC9tdjRHexMwa.htm)|Irahkatu|auto-trad|
|[V7FoP8iIcehuiF20.htm](kingmaker-bestiary/V7FoP8iIcehuiF20.htm)|Cursed Guardian|auto-trad|
|[vBugpZnpxQcrrWoo.htm](kingmaker-bestiary/vBugpZnpxQcrrWoo.htm)|Bloom of Lamashtu|auto-trad|
|[VEWrS5u71szMrhs4.htm](kingmaker-bestiary/VEWrS5u71szMrhs4.htm)|Quintessa Maray|auto-trad|
|[vff5VzjJpRMmg4Hx.htm](kingmaker-bestiary/vff5VzjJpRMmg4Hx.htm)|Cephal Lorentus|auto-trad|
|[vONZlReozVCabXhq.htm](kingmaker-bestiary/vONZlReozVCabXhq.htm)|Collapsing Floor|auto-trad|
|[vrZrha0Gz14Zd4tA.htm](kingmaker-bestiary/vrZrha0Gz14Zd4tA.htm)|Korog|auto-trad|
|[vs8QT4LYEcQfA6Us.htm](kingmaker-bestiary/vs8QT4LYEcQfA6Us.htm)|Skeletal Tiger Lord|auto-trad|
|[VSffsyt5RONB4k2U.htm](kingmaker-bestiary/VSffsyt5RONB4k2U.htm)|General Avinash Jurrg|auto-trad|
|[vTzbfxtvhhmS7KWr.htm](kingmaker-bestiary/vTzbfxtvhhmS7KWr.htm)|Bloom Wyvern|auto-trad|
|[w8jUzPPGLQECT4j7.htm](kingmaker-bestiary/w8jUzPPGLQECT4j7.htm)|Overgrown Viper Vine|auto-trad|
|[wD6nctHffSaMdyag.htm](kingmaker-bestiary/wD6nctHffSaMdyag.htm)|Lizardfolk Warrior|auto-trad|
|[wdoRpYImTQuVGZSQ.htm](kingmaker-bestiary/wdoRpYImTQuVGZSQ.htm)|Fionn|auto-trad|
|[WGHV95WkNnlY70Sn.htm](kingmaker-bestiary/WGHV95WkNnlY70Sn.htm)|Lintwerth|auto-trad|
|[wHQmyXnG4Yax4KcK.htm](kingmaker-bestiary/wHQmyXnG4Yax4KcK.htm)|Enormous Dragonfly|auto-trad|
|[WJ0bMCZUHJVwKYG1.htm](kingmaker-bestiary/WJ0bMCZUHJVwKYG1.htm)|Freshly Bloomed Basilisk|auto-trad|
|[wJDtBtvRtyxtyqHS.htm](kingmaker-bestiary/wJDtBtvRtyxtyqHS.htm)|Kargstaad|auto-trad|
|[wMfnwJoLvxdZZAwr.htm](kingmaker-bestiary/wMfnwJoLvxdZZAwr.htm)|Drelev Guards|auto-trad|
|[WmSZELnHCZ9g9Nq2.htm](kingmaker-bestiary/WmSZELnHCZ9g9Nq2.htm)|Svetlana|auto-trad|
|[Wyqsf3qDt7PqQ8OM.htm](kingmaker-bestiary/Wyqsf3qDt7PqQ8OM.htm)|Brush Thylacine|auto-trad|
|[xMuLJmx51eBv9FcE.htm](kingmaker-bestiary/xMuLJmx51eBv9FcE.htm)|Jewel|auto-trad|
|[XOiNuunTFGbDYeu2.htm](kingmaker-bestiary/XOiNuunTFGbDYeu2.htm)|Lights of the Lost|auto-trad|
|[xqTVUqUs5UsGcSuH.htm](kingmaker-bestiary/xqTVUqUs5UsGcSuH.htm)|Winged Owlbear|auto-trad|
|[XQyXoOe7FCpZilaF.htm](kingmaker-bestiary/XQyXoOe7FCpZilaF.htm)|Lesser Jabberwock|auto-trad|
|[XydYK1C7RFDqKine.htm](kingmaker-bestiary/XydYK1C7RFDqKine.htm)|Imeckus Stroon|auto-trad|
|[XzFifthQr0V5nEJe.htm](kingmaker-bestiary/XzFifthQr0V5nEJe.htm)|Ilora Nuski|auto-trad|
|[y5W2rrHzQmeSE6LU.htm](kingmaker-bestiary/y5W2rrHzQmeSE6LU.htm)|Happs Bydon|auto-trad|
|[y68kqNXmr1BnjZtc.htm](kingmaker-bestiary/y68kqNXmr1BnjZtc.htm)|Oversized Chimera|auto-trad|
|[Y9N8MaTYupFhCUuN.htm](kingmaker-bestiary/Y9N8MaTYupFhCUuN.htm)|Jaggedbriar Hag|auto-trad|
|[YFnWOu9edWhj6vV6.htm](kingmaker-bestiary/YFnWOu9edWhj6vV6.htm)|Akiros Ismort|auto-trad|
|[ylMXDs2f7C0YKdZY.htm](kingmaker-bestiary/ylMXDs2f7C0YKdZY.htm)|Xae|auto-trad|
|[Ypxj2FdUPQqpWPf3.htm](kingmaker-bestiary/Ypxj2FdUPQqpWPf3.htm)|Ntavi|auto-trad|
|[YyUVzTucO99JFDnm.htm](kingmaker-bestiary/YyUVzTucO99JFDnm.htm)|Rigged Climbing Loops|auto-trad|
|[yzlu5YX7Oxo7TVvK.htm](kingmaker-bestiary/yzlu5YX7Oxo7TVvK.htm)|Prazil|auto-trad|
|[z1Hk6z6RU4sF9aJU.htm](kingmaker-bestiary/z1Hk6z6RU4sF9aJU.htm)|Thresholder Disciple|auto-trad|
|[ZaA9oQXOWne0IXSG.htm](kingmaker-bestiary/ZaA9oQXOWne0IXSG.htm)|Linzi (Level 1)|auto-trad|
|[ZbgFOoDCOXqhJWhz.htm](kingmaker-bestiary/ZbgFOoDCOXqhJWhz.htm)|Ngara|auto-trad|
|[ZDt1rLh1VUFnHj5S.htm](kingmaker-bestiary/ZDt1rLh1VUFnHj5S.htm)|Lickweed|auto-trad|
|[ZEpByav3dMCyvJJu.htm](kingmaker-bestiary/ZEpByav3dMCyvJJu.htm)|Void Pit|auto-trad|
|[ZGVQcWl03kBIStS0.htm](kingmaker-bestiary/ZGVQcWl03kBIStS0.htm)|Kundal|auto-trad|
|[ZiNVsXL5DJ4Ekd5v.htm](kingmaker-bestiary/ZiNVsXL5DJ4Ekd5v.htm)|Breath of Despair|auto-trad|
|[ZlEhOqdwPDpU3jvO.htm](kingmaker-bestiary/ZlEhOqdwPDpU3jvO.htm)|Stinging Nettles|auto-trad|
|[ZoUIXzygFDuHKebr.htm](kingmaker-bestiary/ZoUIXzygFDuHKebr.htm)|Spirit of Stisshak|auto-trad|
|[ZPm7WqgQTjysuTiT.htm](kingmaker-bestiary/ZPm7WqgQTjysuTiT.htm)|False Priestess|auto-trad|
|[zvhw5Qx6gU7e39he.htm](kingmaker-bestiary/zvhw5Qx6gU7e39he.htm)|Linzi (Level 7)|auto-trad|
|[zZrKk6wh7av4nU1z.htm](kingmaker-bestiary/zZrKk6wh7av4nU1z.htm)|Phantasmagoric Fog Trap|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[8D3xvMiuOLJ3p6FU.htm](kingmaker-bestiary/8D3xvMiuOLJ3p6FU.htm)|Hooktongue Hydra|Hooktongue Hydra|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
