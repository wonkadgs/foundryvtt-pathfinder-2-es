# Estado de la traducción (abomination-vaults-bestiary-items)

 * **auto-trad**: 852
 * **modificada**: 8


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[01CKBc5uwpZchw7t.htm](abomination-vaults-bestiary-items/01CKBc5uwpZchw7t.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[06lGWrOe3DOYvTgV.htm](abomination-vaults-bestiary-items/06lGWrOe3DOYvTgV.htm)|Bright Release|auto-trad|
|[09R0B0sIkwFfNoZ5.htm](abomination-vaults-bestiary-items/09R0B0sIkwFfNoZ5.htm)|Shuriken|auto-trad|
|[0agBOEbezDUSkYUQ.htm](abomination-vaults-bestiary-items/0agBOEbezDUSkYUQ.htm)|Greater Darkvision|auto-trad|
|[0dO1DU5gt5v8bFwc.htm](abomination-vaults-bestiary-items/0dO1DU5gt5v8bFwc.htm)|Swallow Whole|auto-trad|
|[0Pe1uBGVehRLhBim.htm](abomination-vaults-bestiary-items/0Pe1uBGVehRLhBim.htm)|Attack of Opportunity|auto-trad|
|[0PqUwOqIbxXioOPh.htm](abomination-vaults-bestiary-items/0PqUwOqIbxXioOPh.htm)|Negative Healing|auto-trad|
|[0QCPtkVtKzuuVVaK.htm](abomination-vaults-bestiary-items/0QCPtkVtKzuuVVaK.htm)|Bladed Limb|auto-trad|
|[0QhLcUbXJtWWVCmp.htm](abomination-vaults-bestiary-items/0QhLcUbXJtWWVCmp.htm)|Paralysis|auto-trad|
|[0UgwXoyN2VLdj2sc.htm](abomination-vaults-bestiary-items/0UgwXoyN2VLdj2sc.htm)|Darkvision|auto-trad|
|[11ioR4E9tTmB1Wak.htm](abomination-vaults-bestiary-items/11ioR4E9tTmB1Wak.htm)|Filth Fever|auto-trad|
|[122ICOgqYvLmpu9U.htm](abomination-vaults-bestiary-items/122ICOgqYvLmpu9U.htm)|Sneak Attack|auto-trad|
|[12LD6QHUzml95C9n.htm](abomination-vaults-bestiary-items/12LD6QHUzml95C9n.htm)|Frost Greatsword|auto-trad|
|[18GcGCMe0ekCnLyC.htm](abomination-vaults-bestiary-items/18GcGCMe0ekCnLyC.htm)|Dagger|auto-trad|
|[19toTT0P9kxuQAmz.htm](abomination-vaults-bestiary-items/19toTT0P9kxuQAmz.htm)|At-Will Spells|auto-trad|
|[1B109bqthYI0DGyC.htm](abomination-vaults-bestiary-items/1B109bqthYI0DGyC.htm)|Innocuous|auto-trad|
|[1CumgFDHAf5o5Yfs.htm](abomination-vaults-bestiary-items/1CumgFDHAf5o5Yfs.htm)|Breath Weapon|auto-trad|
|[1FEKSwqCrM8p8ho7.htm](abomination-vaults-bestiary-items/1FEKSwqCrM8p8ho7.htm)|Negative Healing|auto-trad|
|[1G0kEOTWyivIisjY.htm](abomination-vaults-bestiary-items/1G0kEOTWyivIisjY.htm)|Psychokinetic Honing|auto-trad|
|[1IcPKJ8QCEzRvPPY.htm](abomination-vaults-bestiary-items/1IcPKJ8QCEzRvPPY.htm)|Light Hammer|auto-trad|
|[1lJU8hzSml2TMM3M.htm](abomination-vaults-bestiary-items/1lJU8hzSml2TMM3M.htm)|Telepathy 100 feet|auto-trad|
|[1SovHiGHboDM9yGe.htm](abomination-vaults-bestiary-items/1SovHiGHboDM9yGe.htm)|Light Aura|auto-trad|
|[1tb7DMhGgvu0yKi2.htm](abomination-vaults-bestiary-items/1tb7DMhGgvu0yKi2.htm)|Seugathi Venom|auto-trad|
|[1ubYo2q1E42ysH5K.htm](abomination-vaults-bestiary-items/1ubYo2q1E42ysH5K.htm)|Darkvision|auto-trad|
|[1UmNfowZ4HWxS9rx.htm](abomination-vaults-bestiary-items/1UmNfowZ4HWxS9rx.htm)|Devastating Blast|auto-trad|
|[1zCZasUbrbz7SMAh.htm](abomination-vaults-bestiary-items/1zCZasUbrbz7SMAh.htm)|Flicker|auto-trad|
|[29EzhqCR2u7R7K4N.htm](abomination-vaults-bestiary-items/29EzhqCR2u7R7K4N.htm)|At-Will Spells|auto-trad|
|[2a2eONlkTBanM6W0.htm](abomination-vaults-bestiary-items/2a2eONlkTBanM6W0.htm)|Drain Life|auto-trad|
|[2dHPDZu3BYN1GauB.htm](abomination-vaults-bestiary-items/2dHPDZu3BYN1GauB.htm)|No Breath|auto-trad|
|[2lRJXkUSUIu9w7Vg.htm](abomination-vaults-bestiary-items/2lRJXkUSUIu9w7Vg.htm)|Elven Curve Blade|auto-trad|
|[2NEvfMuBqdlReHe7.htm](abomination-vaults-bestiary-items/2NEvfMuBqdlReHe7.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[2pz6VpKOe8cRxqUY.htm](abomination-vaults-bestiary-items/2pz6VpKOe8cRxqUY.htm)|Guiding Rhythm|auto-trad|
|[2rAKighB6Ywhk5s3.htm](abomination-vaults-bestiary-items/2rAKighB6Ywhk5s3.htm)|Flare|auto-trad|
|[2rGZ3A8JPafZZZur.htm](abomination-vaults-bestiary-items/2rGZ3A8JPafZZZur.htm)|Terrifying Charge|auto-trad|
|[2Ufw7cZauI4kkDrY.htm](abomination-vaults-bestiary-items/2Ufw7cZauI4kkDrY.htm)|Starknife|auto-trad|
|[37DJ9kew2cByiJDV.htm](abomination-vaults-bestiary-items/37DJ9kew2cByiJDV.htm)|Self-Loathing|auto-trad|
|[3AqKPdexSndZxd5X.htm](abomination-vaults-bestiary-items/3AqKPdexSndZxd5X.htm)|Rend|auto-trad|
|[3b3sEGxcz23toRTi.htm](abomination-vaults-bestiary-items/3b3sEGxcz23toRTi.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[3Eksrmh390T4SrP7.htm](abomination-vaults-bestiary-items/3Eksrmh390T4SrP7.htm)|Magic Immunity|auto-trad|
|[3mqnU8Lr3Tl3zgd2.htm](abomination-vaults-bestiary-items/3mqnU8Lr3Tl3zgd2.htm)|Claw|auto-trad|
|[3RI2spwyiEWuXdgF.htm](abomination-vaults-bestiary-items/3RI2spwyiEWuXdgF.htm)|Negative Healing|auto-trad|
|[3sSrZrwFGWNyzaIj.htm](abomination-vaults-bestiary-items/3sSrZrwFGWNyzaIj.htm)|Light Blindness|auto-trad|
|[3vycv9ThXOQbgNBf.htm](abomination-vaults-bestiary-items/3vycv9ThXOQbgNBf.htm)|Instinctual Tinker|auto-trad|
|[3Y7de8nXARx61nEk.htm](abomination-vaults-bestiary-items/3Y7de8nXARx61nEk.htm)|Spray Toxic Oil|auto-trad|
|[41jyyqtM7TidzhJx.htm](abomination-vaults-bestiary-items/41jyyqtM7TidzhJx.htm)|Greater Darkvision|auto-trad|
|[43FJrgdgr667MXKo.htm](abomination-vaults-bestiary-items/43FJrgdgr667MXKo.htm)|Darkvision|auto-trad|
|[45yP2nFzQUcp3Zvm.htm](abomination-vaults-bestiary-items/45yP2nFzQUcp3Zvm.htm)|Feed on Fear|auto-trad|
|[4adEu2Sh2OYsvkLV.htm](abomination-vaults-bestiary-items/4adEu2Sh2OYsvkLV.htm)|Fist|auto-trad|
|[4cuSJxJLUg7icXmj.htm](abomination-vaults-bestiary-items/4cuSJxJLUg7icXmj.htm)|Vulnerable to Shatter|auto-trad|
|[4GEUTovCKhSoT0PD.htm](abomination-vaults-bestiary-items/4GEUTovCKhSoT0PD.htm)|Stupor Poison|auto-trad|
|[4k6yZiC3ggdlq57F.htm](abomination-vaults-bestiary-items/4k6yZiC3ggdlq57F.htm)|Ghostly Hand|auto-trad|
|[4mS9YqseWjLaRiQq.htm](abomination-vaults-bestiary-items/4mS9YqseWjLaRiQq.htm)|Skirmishing Dash|auto-trad|
|[4pjPyWF2dnob8Tlm.htm](abomination-vaults-bestiary-items/4pjPyWF2dnob8Tlm.htm)|Necrotic Decay|auto-trad|
|[4UocioViS4VkF9mM.htm](abomination-vaults-bestiary-items/4UocioViS4VkF9mM.htm)|Scythe Shuffle|auto-trad|
|[51r9xhJpaBdZxkK3.htm](abomination-vaults-bestiary-items/51r9xhJpaBdZxkK3.htm)|Darkvision|auto-trad|
|[5CnYknDSEBbdP7iX.htm](abomination-vaults-bestiary-items/5CnYknDSEBbdP7iX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[5dcaJKOKohQRyeQx.htm](abomination-vaults-bestiary-items/5dcaJKOKohQRyeQx.htm)|Glow|auto-trad|
|[5dn4uxBWvzHmjJF9.htm](abomination-vaults-bestiary-items/5dn4uxBWvzHmjJF9.htm)|Mummy Rot|auto-trad|
|[5e5FND78feSKXP99.htm](abomination-vaults-bestiary-items/5e5FND78feSKXP99.htm)|Impaling Chain|auto-trad|
|[5htUbZNH2cz3d5sv.htm](abomination-vaults-bestiary-items/5htUbZNH2cz3d5sv.htm)|Club|auto-trad|
|[5NK2vxZn4QPORBlo.htm](abomination-vaults-bestiary-items/5NK2vxZn4QPORBlo.htm)|Scalathrax Venom|auto-trad|
|[5OrtHpltPsigs3A8.htm](abomination-vaults-bestiary-items/5OrtHpltPsigs3A8.htm)|Jaws|auto-trad|
|[5pnWBWoyvDchoWWQ.htm](abomination-vaults-bestiary-items/5pnWBWoyvDchoWWQ.htm)|Divine Innate Spells|auto-trad|
|[5rrKtIIzYEri3htn.htm](abomination-vaults-bestiary-items/5rrKtIIzYEri3htn.htm)|Wolf Coordination|auto-trad|
|[5s3WbfpPg11BNAkO.htm](abomination-vaults-bestiary-items/5s3WbfpPg11BNAkO.htm)|Apocalypse Beam (From Tsunami)|auto-trad|
|[5Teyg9iopHoZ6xTN.htm](abomination-vaults-bestiary-items/5Teyg9iopHoZ6xTN.htm)|Death's Grace|auto-trad|
|[5TV0w8GnwrjMX6pX.htm](abomination-vaults-bestiary-items/5TV0w8GnwrjMX6pX.htm)|Apocalypse Beam (from Earthquake)|auto-trad|
|[5Vw9VTB3I1sie1g1.htm](abomination-vaults-bestiary-items/5Vw9VTB3I1sie1g1.htm)|Sarglagon Venom|auto-trad|
|[5YFP1XKwVqEkmjZV.htm](abomination-vaults-bestiary-items/5YFP1XKwVqEkmjZV.htm)|Swift Leap|auto-trad|
|[5YNto5CTjVF4xtDp.htm](abomination-vaults-bestiary-items/5YNto5CTjVF4xtDp.htm)|At-Will Spells|auto-trad|
|[60GCM4eYgqqlPJau.htm](abomination-vaults-bestiary-items/60GCM4eYgqqlPJau.htm)|Light Blindness|auto-trad|
|[65o1CNgkOxBuaZU6.htm](abomination-vaults-bestiary-items/65o1CNgkOxBuaZU6.htm)|At-Will Spells|auto-trad|
|[6BBZEA8kkI2GaLNH.htm](abomination-vaults-bestiary-items/6BBZEA8kkI2GaLNH.htm)|Shauth Lash|auto-trad|
|[6EdSmIm5HogvdHr3.htm](abomination-vaults-bestiary-items/6EdSmIm5HogvdHr3.htm)|Jaws|auto-trad|
|[6F6RBqJmz0JUvLc3.htm](abomination-vaults-bestiary-items/6F6RBqJmz0JUvLc3.htm)|Divine Innate Spells|auto-trad|
|[6FmfnLeOcKMh8v5n.htm](abomination-vaults-bestiary-items/6FmfnLeOcKMh8v5n.htm)|Occult Spontaneous Spells|auto-trad|
|[6glfioS5uXVucJjF.htm](abomination-vaults-bestiary-items/6glfioS5uXVucJjF.htm)|Greater Darkvision|auto-trad|
|[6j3vC2OBRq8nReei.htm](abomination-vaults-bestiary-items/6j3vC2OBRq8nReei.htm)|Divine Innate Spells|auto-trad|
|[6P6uGjuZLcSJBoME.htm](abomination-vaults-bestiary-items/6P6uGjuZLcSJBoME.htm)|Light Blindness|auto-trad|
|[6pIoss4a281w0Fed.htm](abomination-vaults-bestiary-items/6pIoss4a281w0Fed.htm)|Light Flare|auto-trad|
|[6sW128BEd2N5fP96.htm](abomination-vaults-bestiary-items/6sW128BEd2N5fP96.htm)|At-Will Spells|auto-trad|
|[6TMEJEZsiK6x4E68.htm](abomination-vaults-bestiary-items/6TMEJEZsiK6x4E68.htm)|Necrotic Decay|auto-trad|
|[6XRN01EUnaz1YCgJ.htm](abomination-vaults-bestiary-items/6XRN01EUnaz1YCgJ.htm)|Hand Crossbow|auto-trad|
|[6zbiPHhWaAV5pJd6.htm](abomination-vaults-bestiary-items/6zbiPHhWaAV5pJd6.htm)|Wriggling Beard|auto-trad|
|[72UcADd2WTnoNikm.htm](abomination-vaults-bestiary-items/72UcADd2WTnoNikm.htm)|Occult Prepared Spells|auto-trad|
|[78oc41E029XUkMoS.htm](abomination-vaults-bestiary-items/78oc41E029XUkMoS.htm)|Attack of Opportunity|auto-trad|
|[7hyO9Fv7Tl6MS4L3.htm](abomination-vaults-bestiary-items/7hyO9Fv7Tl6MS4L3.htm)|Skull Rot|auto-trad|
|[7mOx4vdvxyfHNF71.htm](abomination-vaults-bestiary-items/7mOx4vdvxyfHNF71.htm)|Blood Magic|auto-trad|
|[7NjsgQKMg0FvxDmJ.htm](abomination-vaults-bestiary-items/7NjsgQKMg0FvxDmJ.htm)|Telepathy 100 feet|auto-trad|
|[7q1bZINWOLceAJQO.htm](abomination-vaults-bestiary-items/7q1bZINWOLceAJQO.htm)|Rituals|auto-trad|
|[7QmN8VFF7J1MzXXO.htm](abomination-vaults-bestiary-items/7QmN8VFF7J1MzXXO.htm)|Darkvision|auto-trad|
|[7QQL3bbmfxHIb7Tz.htm](abomination-vaults-bestiary-items/7QQL3bbmfxHIb7Tz.htm)|Greater Darkvision|auto-trad|
|[7sBSRQrV9kFrhQxu.htm](abomination-vaults-bestiary-items/7sBSRQrV9kFrhQxu.htm)|Bite|auto-trad|
|[7Scw9mC9n5d0Jon5.htm](abomination-vaults-bestiary-items/7Scw9mC9n5d0Jon5.htm)|Spear Frog Poison|auto-trad|
|[7tCu8SPq0TpF1lJX.htm](abomination-vaults-bestiary-items/7tCu8SPq0TpF1lJX.htm)|Mindfog Aura|auto-trad|
|[7VBvaubtWnbIVsZ9.htm](abomination-vaults-bestiary-items/7VBvaubtWnbIVsZ9.htm)|At-Will Spells|auto-trad|
|[7wW5Mpc0dnnQnoko.htm](abomination-vaults-bestiary-items/7wW5Mpc0dnnQnoko.htm)|Wicked Bite|auto-trad|
|[7Y7vl4vBRBKsb6wB.htm](abomination-vaults-bestiary-items/7Y7vl4vBRBKsb6wB.htm)|Jaws|auto-trad|
|[800HkICSLpenBkq9.htm](abomination-vaults-bestiary-items/800HkICSLpenBkq9.htm)|Occult Spontaneous Spells|auto-trad|
|[83QmDBGigZxaHbbd.htm](abomination-vaults-bestiary-items/83QmDBGigZxaHbbd.htm)|Darkvision|auto-trad|
|[87c42mEQ6XMyoNBu.htm](abomination-vaults-bestiary-items/87c42mEQ6XMyoNBu.htm)|Read the Stars|auto-trad|
|[8aYfDhAzJOdFBLKs.htm](abomination-vaults-bestiary-items/8aYfDhAzJOdFBLKs.htm)|At-Will Spells|auto-trad|
|[8g0Gpku3xbzwpSTx.htm](abomination-vaults-bestiary-items/8g0Gpku3xbzwpSTx.htm)|Go Dark|auto-trad|
|[8l05kDJb53W6NV0L.htm](abomination-vaults-bestiary-items/8l05kDJb53W6NV0L.htm)|Jaws|auto-trad|
|[8nidqgL5pawfDVZa.htm](abomination-vaults-bestiary-items/8nidqgL5pawfDVZa.htm)|Darkvision|auto-trad|
|[8QZHl09gya9bBM7r.htm](abomination-vaults-bestiary-items/8QZHl09gya9bBM7r.htm)|Darkvision|auto-trad|
|[8R3jmOcyIfgNNdKr.htm](abomination-vaults-bestiary-items/8R3jmOcyIfgNNdKr.htm)|Negative Healing|auto-trad|
|[8rL4XxfF1Fb3WMBG.htm](abomination-vaults-bestiary-items/8rL4XxfF1Fb3WMBG.htm)|Occult Innate Spells|auto-trad|
|[8SCH000Zv6X9BFPf.htm](abomination-vaults-bestiary-items/8SCH000Zv6X9BFPf.htm)|Fist|auto-trad|
|[8SE9FJ5v5zXgilOT.htm](abomination-vaults-bestiary-items/8SE9FJ5v5zXgilOT.htm)|Attack of Opportunity|auto-trad|
|[8vdtmNasjIJH8Bi3.htm](abomination-vaults-bestiary-items/8vdtmNasjIJH8Bi3.htm)|Light Hammer|auto-trad|
|[8xM2M4RGdKhM7ex3.htm](abomination-vaults-bestiary-items/8xM2M4RGdKhM7ex3.htm)|Divine Innate Spells|auto-trad|
|[8z4D5pllZhfFA85D.htm](abomination-vaults-bestiary-items/8z4D5pllZhfFA85D.htm)|Stench Suppression|auto-trad|
|[8zJvcrLeqTjH3Eup.htm](abomination-vaults-bestiary-items/8zJvcrLeqTjH3Eup.htm)|Primal Spontaneous Spells|auto-trad|
|[94yfc2eSiwRZfq49.htm](abomination-vaults-bestiary-items/94yfc2eSiwRZfq49.htm)|Needle|auto-trad|
|[99jBJtnLmD48VOSS.htm](abomination-vaults-bestiary-items/99jBJtnLmD48VOSS.htm)|Claw|auto-trad|
|[9C4djpvjI0fNA6Fa.htm](abomination-vaults-bestiary-items/9C4djpvjI0fNA6Fa.htm)|Darkvision|auto-trad|
|[9DkYcHRtL1rgB4Ob.htm](abomination-vaults-bestiary-items/9DkYcHRtL1rgB4Ob.htm)|Constrict|auto-trad|
|[9Eh0XQSnJ9rPgQ3d.htm](abomination-vaults-bestiary-items/9Eh0XQSnJ9rPgQ3d.htm)|Push|auto-trad|
|[9eONvr5by0xfPFcf.htm](abomination-vaults-bestiary-items/9eONvr5by0xfPFcf.htm)|Witchflame Caress|auto-trad|
|[9i3UjVYq4lp3MqZJ.htm](abomination-vaults-bestiary-items/9i3UjVYq4lp3MqZJ.htm)|Burning Lash|auto-trad|
|[9J2GBQp8uuYCXtLo.htm](abomination-vaults-bestiary-items/9J2GBQp8uuYCXtLo.htm)|Attack of Opportunity|auto-trad|
|[9xQRUNNxUssnoy4j.htm](abomination-vaults-bestiary-items/9xQRUNNxUssnoy4j.htm)|Chameleon Skin|auto-trad|
|[9yk5MBYML8Z0OmtD.htm](abomination-vaults-bestiary-items/9yk5MBYML8Z0OmtD.htm)|Telepathy (with spectral thralls only)|auto-trad|
|[a4lUBuJPU86WP0Gq.htm](abomination-vaults-bestiary-items/a4lUBuJPU86WP0Gq.htm)|Mindfog Aura|auto-trad|
|[a4vxarsPYtIYy5Br.htm](abomination-vaults-bestiary-items/a4vxarsPYtIYy5Br.htm)|At-Will Spells|auto-trad|
|[a6xLeWb5L726R1lN.htm](abomination-vaults-bestiary-items/a6xLeWb5L726R1lN.htm)|Push|auto-trad|
|[AAFWcmRlGxb76Pps.htm](abomination-vaults-bestiary-items/AAFWcmRlGxb76Pps.htm)|Occult Innate Spells|auto-trad|
|[ACtqtCeexz93EgK9.htm](abomination-vaults-bestiary-items/ACtqtCeexz93EgK9.htm)|Light Blindness|auto-trad|
|[agqHW8i0ToF0OIyF.htm](abomination-vaults-bestiary-items/agqHW8i0ToF0OIyF.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[AGshXsiKjGrOkqWk.htm](abomination-vaults-bestiary-items/AGshXsiKjGrOkqWk.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[AHdJDOIrRS3IZSrc.htm](abomination-vaults-bestiary-items/AHdJDOIrRS3IZSrc.htm)|Greater Darkvision|auto-trad|
|[aK6ekR5eDVtsBkFZ.htm](abomination-vaults-bestiary-items/aK6ekR5eDVtsBkFZ.htm)|Occult Attack|auto-trad|
|[AkS8oNJyeHZyFiyP.htm](abomination-vaults-bestiary-items/AkS8oNJyeHZyFiyP.htm)|Pitfall|auto-trad|
|[alpHOBrHeF3AD5cW.htm](abomination-vaults-bestiary-items/alpHOBrHeF3AD5cW.htm)|At-Will Spells|auto-trad|
|[aSbHK3DUVAsb8eW6.htm](abomination-vaults-bestiary-items/aSbHK3DUVAsb8eW6.htm)|Jaws|auto-trad|
|[aUiw4yrSZnbmjXeR.htm](abomination-vaults-bestiary-items/aUiw4yrSZnbmjXeR.htm)|Sneak Attack|auto-trad|
|[AWNveQn0EQ1aYORU.htm](abomination-vaults-bestiary-items/AWNveQn0EQ1aYORU.htm)|Rise Up|auto-trad|
|[AyfJyi6gCHBgaToD.htm](abomination-vaults-bestiary-items/AyfJyi6gCHBgaToD.htm)|Darkvision|auto-trad|
|[ayqWkUKHp3vU6wiY.htm](abomination-vaults-bestiary-items/ayqWkUKHp3vU6wiY.htm)|Blood Offering|auto-trad|
|[azGXpMTZu0Ft4DaJ.htm](abomination-vaults-bestiary-items/azGXpMTZu0Ft4DaJ.htm)|Horn|auto-trad|
|[b1w5wps2XBnNPOls.htm](abomination-vaults-bestiary-items/b1w5wps2XBnNPOls.htm)|Furious Claws|auto-trad|
|[b6BZWa1ISKLflSTs.htm](abomination-vaults-bestiary-items/b6BZWa1ISKLflSTs.htm)|Light Blindness|auto-trad|
|[b6FIdRGaxea1KX6k.htm](abomination-vaults-bestiary-items/b6FIdRGaxea1KX6k.htm)|Claw|auto-trad|
|[B8jNXhGz4H4CYK3k.htm](abomination-vaults-bestiary-items/B8jNXhGz4H4CYK3k.htm)|Negative Healing|auto-trad|
|[BBdrCuAIiJoPRLOJ.htm](abomination-vaults-bestiary-items/BBdrCuAIiJoPRLOJ.htm)|Breath of the Bog|auto-trad|
|[BCdQbDHjNRy8IfGp.htm](abomination-vaults-bestiary-items/BCdQbDHjNRy8IfGp.htm)|Unwilling Teleportation|auto-trad|
|[BDMTyu1gp7B6b9XF.htm](abomination-vaults-bestiary-items/BDMTyu1gp7B6b9XF.htm)|Rituals|auto-trad|
|[BeNcnupTKc7WS9e5.htm](abomination-vaults-bestiary-items/BeNcnupTKc7WS9e5.htm)|Dance Moves|auto-trad|
|[BFiMEADLB2Qw0lL1.htm](abomination-vaults-bestiary-items/BFiMEADLB2Qw0lL1.htm)|Spike|auto-trad|
|[BhqjfA3rcAJRma6F.htm](abomination-vaults-bestiary-items/BhqjfA3rcAJRma6F.htm)|Claw|auto-trad|
|[bIhvOKLmWJLAIfDK.htm](abomination-vaults-bestiary-items/bIhvOKLmWJLAIfDK.htm)|Immobilizing Blow|auto-trad|
|[BkxqdQTCmF312Eby.htm](abomination-vaults-bestiary-items/BkxqdQTCmF312Eby.htm)|At-Will Spells|auto-trad|
|[BldzzBvbyFPySPXc.htm](abomination-vaults-bestiary-items/BldzzBvbyFPySPXc.htm)|Occult Innate Spells|auto-trad|
|[BMv1jkvT4dSbEL8X.htm](abomination-vaults-bestiary-items/BMv1jkvT4dSbEL8X.htm)|Greater Darkvision|auto-trad|
|[bMX8ZvBxeu0Ty5cl.htm](abomination-vaults-bestiary-items/bMX8ZvBxeu0Ty5cl.htm)|Devour Soul|auto-trad|
|[boE5h9E3qlCaVd4L.htm](abomination-vaults-bestiary-items/boE5h9E3qlCaVd4L.htm)|Darkvision|auto-trad|
|[BPJS4QzIXUC3Zmwf.htm](abomination-vaults-bestiary-items/BPJS4QzIXUC3Zmwf.htm)|Frightful Presence|auto-trad|
|[BPvAPhpd3Nzs8pvv.htm](abomination-vaults-bestiary-items/BPvAPhpd3Nzs8pvv.htm)|Needle Spray|auto-trad|
|[buzf372gE5wTjjeH.htm](abomination-vaults-bestiary-items/buzf372gE5wTjjeH.htm)|Fist|auto-trad|
|[bxjYZ892TLi9AizW.htm](abomination-vaults-bestiary-items/bxjYZ892TLi9AizW.htm)|Attack of Opportunity|auto-trad|
|[BZL7n09A5aQfGHjz.htm](abomination-vaults-bestiary-items/BZL7n09A5aQfGHjz.htm)|Occult Innate Spells|auto-trad|
|[C2q1zvPuwqgQMvwX.htm](abomination-vaults-bestiary-items/C2q1zvPuwqgQMvwX.htm)|Change Shape|auto-trad|
|[C2yMhpgqYU1HM1NR.htm](abomination-vaults-bestiary-items/C2yMhpgqYU1HM1NR.htm)|Claw|auto-trad|
|[C46qe4d4dQ5DqKp4.htm](abomination-vaults-bestiary-items/C46qe4d4dQ5DqKp4.htm)|Vile Blowgun|auto-trad|
|[c8DR2M4wTFOhiB7j.htm](abomination-vaults-bestiary-items/c8DR2M4wTFOhiB7j.htm)|Fast Healing 7|auto-trad|
|[C91ggah5Ye65LrMH.htm](abomination-vaults-bestiary-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|auto-trad|
|[CAWJlqwSDN3w5gWy.htm](abomination-vaults-bestiary-items/CAWJlqwSDN3w5gWy.htm)|Overpowering Jaws|auto-trad|
|[cb5fpVrLbNcM4VUi.htm](abomination-vaults-bestiary-items/cb5fpVrLbNcM4VUi.htm)|Suppress Aura|auto-trad|
|[CcTlJ52vSeJqkx5b.htm](abomination-vaults-bestiary-items/CcTlJ52vSeJqkx5b.htm)|Light Blindness|auto-trad|
|[cdTaJad7jDM2QE73.htm](abomination-vaults-bestiary-items/cdTaJad7jDM2QE73.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[cfM8s4VVlCf81rzl.htm](abomination-vaults-bestiary-items/cfM8s4VVlCf81rzl.htm)|Repeating Hand Crossbow|auto-trad|
|[CH1wifIGckLBQDfe.htm](abomination-vaults-bestiary-items/CH1wifIGckLBQDfe.htm)|All-Around Vision|auto-trad|
|[ch9nYPl6I99TJLu9.htm](abomination-vaults-bestiary-items/ch9nYPl6I99TJLu9.htm)|Swarming Stance|auto-trad|
|[CH9sJBUN2HtEAgtt.htm](abomination-vaults-bestiary-items/CH9sJBUN2HtEAgtt.htm)|Vocal Warm-Up|auto-trad|
|[Ci03R6SeE2d256my.htm](abomination-vaults-bestiary-items/Ci03R6SeE2d256my.htm)|Aquatic Ambush|auto-trad|
|[cj3wll0YeOYjYWaL.htm](abomination-vaults-bestiary-items/cj3wll0YeOYjYWaL.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[cKCHCzZNwEgF3rgQ.htm](abomination-vaults-bestiary-items/cKCHCzZNwEgF3rgQ.htm)|Negative Healing|auto-trad|
|[ckEhVhjZnjMqhSjZ.htm](abomination-vaults-bestiary-items/ckEhVhjZnjMqhSjZ.htm)|Shootist's Draw|auto-trad|
|[CL3iEdGmI7RDCvqu.htm](abomination-vaults-bestiary-items/CL3iEdGmI7RDCvqu.htm)|Divine Innate Spells|auto-trad|
|[cNnj1mdRc6n47fUX.htm](abomination-vaults-bestiary-items/cNnj1mdRc6n47fUX.htm)|Darkvision|auto-trad|
|[coOSYslrfzzpX9Qf.htm](abomination-vaults-bestiary-items/coOSYslrfzzpX9Qf.htm)|Darkvision|auto-trad|
|[COyeCB6tCf47WbAu.htm](abomination-vaults-bestiary-items/COyeCB6tCf47WbAu.htm)|Jaws|auto-trad|
|[CpXxvpsrf7rcrJM4.htm](abomination-vaults-bestiary-items/CpXxvpsrf7rcrJM4.htm)|Darkvision|auto-trad|
|[cQrkrn9uX9hV860V.htm](abomination-vaults-bestiary-items/cQrkrn9uX9hV860V.htm)|Greater Darkvision|auto-trad|
|[cRzs39Hx788xFMKH.htm](abomination-vaults-bestiary-items/cRzs39Hx788xFMKH.htm)|Rituals|auto-trad|
|[Cth0VtGbtTcGpsAR.htm](abomination-vaults-bestiary-items/Cth0VtGbtTcGpsAR.htm)|Red Claw|auto-trad|
|[CtYG2Idf6Es4FDCt.htm](abomination-vaults-bestiary-items/CtYG2Idf6Es4FDCt.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[cU4WaqnerXGtIpEM.htm](abomination-vaults-bestiary-items/cU4WaqnerXGtIpEM.htm)|Jaws|auto-trad|
|[cu7cx46lq0JVi00X.htm](abomination-vaults-bestiary-items/cu7cx46lq0JVi00X.htm)|Occult Spontaneous Spells|auto-trad|
|[cxiSU1lrmfq49Cag.htm](abomination-vaults-bestiary-items/cxiSU1lrmfq49Cag.htm)|Grab|auto-trad|
|[cYzNZC3Tju4useHD.htm](abomination-vaults-bestiary-items/cYzNZC3Tju4useHD.htm)|Phantom Mount|auto-trad|
|[cZSuVe7TToENdQod.htm](abomination-vaults-bestiary-items/cZSuVe7TToENdQod.htm)|Spore Jet|auto-trad|
|[D0n53VOiJk5g1YnP.htm](abomination-vaults-bestiary-items/D0n53VOiJk5g1YnP.htm)|Necrotic Bomb|auto-trad|
|[D6oLgwNpejO3m2LR.htm](abomination-vaults-bestiary-items/D6oLgwNpejO3m2LR.htm)|Darkvision|auto-trad|
|[D8o83tvKi46maS3U.htm](abomination-vaults-bestiary-items/D8o83tvKi46maS3U.htm)|Sneak Attack|auto-trad|
|[d9oLx0w5SZt7KSEv.htm](abomination-vaults-bestiary-items/d9oLx0w5SZt7KSEv.htm)|Divine Innate Spells|auto-trad|
|[dBpRjWleNemuJ0CH.htm](abomination-vaults-bestiary-items/dBpRjWleNemuJ0CH.htm)|Occult Innate Spells|auto-trad|
|[dd53Mcobz1fpxVhN.htm](abomination-vaults-bestiary-items/dd53Mcobz1fpxVhN.htm)|Claw|auto-trad|
|[deTqYUQHSSEOChwZ.htm](abomination-vaults-bestiary-items/deTqYUQHSSEOChwZ.htm)|Tentacle|auto-trad|
|[DfDsOQ6I6MwVdnPd.htm](abomination-vaults-bestiary-items/DfDsOQ6I6MwVdnPd.htm)|Fleshy Slap|auto-trad|
|[dFe2hAb3rrhlAVsx.htm](abomination-vaults-bestiary-items/dFe2hAb3rrhlAVsx.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[dFO4VUU2D6u32L0j.htm](abomination-vaults-bestiary-items/dFO4VUU2D6u32L0j.htm)|Wicked Bite|auto-trad|
|[DGCZumwAduW04eMJ.htm](abomination-vaults-bestiary-items/DGCZumwAduW04eMJ.htm)|Darkvision|auto-trad|
|[dH7eKw8pDJQDkR3P.htm](abomination-vaults-bestiary-items/dH7eKw8pDJQDkR3P.htm)|Constrict|auto-trad|
|[DHJskQgx4Ucg690b.htm](abomination-vaults-bestiary-items/DHJskQgx4Ucg690b.htm)|Darkvision|auto-trad|
|[dHv4zBnoFd4KSWBz.htm](abomination-vaults-bestiary-items/dHv4zBnoFd4KSWBz.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[DI6Nw0XKEZZJLY8D.htm](abomination-vaults-bestiary-items/DI6Nw0XKEZZJLY8D.htm)|Motion Sense 60 feet|auto-trad|
|[DI73oBXVmI2VUsnm.htm](abomination-vaults-bestiary-items/DI73oBXVmI2VUsnm.htm)|Light Blindness|auto-trad|
|[dJQlNu9f34qL4mEx.htm](abomination-vaults-bestiary-items/dJQlNu9f34qL4mEx.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[DKikA8jlUkgQxScq.htm](abomination-vaults-bestiary-items/DKikA8jlUkgQxScq.htm)|Constant Spells|auto-trad|
|[dl0aE5MkOn0AmMJs.htm](abomination-vaults-bestiary-items/dl0aE5MkOn0AmMJs.htm)|Jaws|auto-trad|
|[DlQoJTMEjgmsWgCa.htm](abomination-vaults-bestiary-items/DlQoJTMEjgmsWgCa.htm)|Mandibles|auto-trad|
|[DLzdlzxfprHMmnS5.htm](abomination-vaults-bestiary-items/DLzdlzxfprHMmnS5.htm)|Greater Darkvision|auto-trad|
|[dNigd85c2ZydQi1Z.htm](abomination-vaults-bestiary-items/dNigd85c2ZydQi1Z.htm)|Swarm Mind|auto-trad|
|[DriJGrzzm1Qq0NNX.htm](abomination-vaults-bestiary-items/DriJGrzzm1Qq0NNX.htm)|Light Blindness|auto-trad|
|[dsBZk8GZYws5VlE1.htm](abomination-vaults-bestiary-items/dsBZk8GZYws5VlE1.htm)|Claustrophobia|auto-trad|
|[DSrUNlWJjUN1fnWL.htm](abomination-vaults-bestiary-items/DSrUNlWJjUN1fnWL.htm)|Poisoned Breath|auto-trad|
|[DSW55AZz6cwRj1rc.htm](abomination-vaults-bestiary-items/DSW55AZz6cwRj1rc.htm)|Jaws|auto-trad|
|[dTlqxVR7MGmNsHHm.htm](abomination-vaults-bestiary-items/dTlqxVR7MGmNsHHm.htm)|Kukri|auto-trad|
|[dwzqPqd8eTnASsxZ.htm](abomination-vaults-bestiary-items/dwzqPqd8eTnASsxZ.htm)|Stone Defense|auto-trad|
|[DYoEwMCwfSHk2Og2.htm](abomination-vaults-bestiary-items/DYoEwMCwfSHk2Og2.htm)|Kukri|auto-trad|
|[DZ1joQ2LpXlkz2Jm.htm](abomination-vaults-bestiary-items/DZ1joQ2LpXlkz2Jm.htm)|Dagger|auto-trad|
|[DzJcNWKMqkWqtgAX.htm](abomination-vaults-bestiary-items/DzJcNWKMqkWqtgAX.htm)|Flaming Hellforged Glaive|auto-trad|
|[E1j9pEdjLX1NuzdV.htm](abomination-vaults-bestiary-items/E1j9pEdjLX1NuzdV.htm)|Staff|auto-trad|
|[e2TItLJ7Ia5jTsir.htm](abomination-vaults-bestiary-items/e2TItLJ7Ia5jTsir.htm)|+2 Status To All Saves vs. Mental|auto-trad|
|[eAyilI89Ya4AzPjX.htm](abomination-vaults-bestiary-items/eAyilI89Ya4AzPjX.htm)|Bounce|auto-trad|
|[Eb8EhrTUrXxsrLz3.htm](abomination-vaults-bestiary-items/Eb8EhrTUrXxsrLz3.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[EBgiFSyYf0W5Fxcr.htm](abomination-vaults-bestiary-items/EBgiFSyYf0W5Fxcr.htm)|Jaws|auto-trad|
|[EDPFYDhj0ZOTpRmX.htm](abomination-vaults-bestiary-items/EDPFYDhj0ZOTpRmX.htm)|Animal Order Spells|auto-trad|
|[eebzEKEUQ1IX3JhM.htm](abomination-vaults-bestiary-items/eebzEKEUQ1IX3JhM.htm)|Bottled Lightning|auto-trad|
|[EEwIe3zMikRnRWg6.htm](abomination-vaults-bestiary-items/EEwIe3zMikRnRWg6.htm)|Warp Reality|auto-trad|
|[eGvWOYTY1N6i6QpT.htm](abomination-vaults-bestiary-items/eGvWOYTY1N6i6QpT.htm)|Blood Drain|auto-trad|
|[EHjIsHVK3BSAzKZW.htm](abomination-vaults-bestiary-items/EHjIsHVK3BSAzKZW.htm)|Focus Gaze|auto-trad|
|[eIBT42KNs5UfO41v.htm](abomination-vaults-bestiary-items/eIBT42KNs5UfO41v.htm)|Darkvision|auto-trad|
|[EiWEO80FhEG270MQ.htm](abomination-vaults-bestiary-items/EiWEO80FhEG270MQ.htm)|Paralysis|auto-trad|
|[Elphpw5BdTHWoPps.htm](abomination-vaults-bestiary-items/Elphpw5BdTHWoPps.htm)|Occult Innate Spells|auto-trad|
|[eltHO3xRIKJCfFcF.htm](abomination-vaults-bestiary-items/eltHO3xRIKJCfFcF.htm)|Claw|auto-trad|
|[eMT2uTNjSReRvfV8.htm](abomination-vaults-bestiary-items/eMT2uTNjSReRvfV8.htm)|Sneak Attack|auto-trad|
|[eP5tBspqEqP18oTY.htm](abomination-vaults-bestiary-items/eP5tBspqEqP18oTY.htm)|Site Bound|auto-trad|
|[eRq3BYWv8B8rLpXL.htm](abomination-vaults-bestiary-items/eRq3BYWv8B8rLpXL.htm)|Shadow Flitter|auto-trad|
|[ErwsWW6oip6zma6i.htm](abomination-vaults-bestiary-items/ErwsWW6oip6zma6i.htm)|Shauth Seize|auto-trad|
|[ESemIP7QRiArXvT4.htm](abomination-vaults-bestiary-items/ESemIP7QRiArXvT4.htm)|Darkvision|auto-trad|
|[eugKdHe7dmAkjXP0.htm](abomination-vaults-bestiary-items/eugKdHe7dmAkjXP0.htm)|Witchflame Bolt|auto-trad|
|[EvzzltzygJO4ess9.htm](abomination-vaults-bestiary-items/EvzzltzygJO4ess9.htm)|Negative Healing|auto-trad|
|[EXyt7FC2RKPKjWCX.htm](abomination-vaults-bestiary-items/EXyt7FC2RKPKjWCX.htm)|Despair|auto-trad|
|[EyBIlDKJTvz0g8ka.htm](abomination-vaults-bestiary-items/EyBIlDKJTvz0g8ka.htm)|Echoes of Defeat|auto-trad|
|[EzSKWtWM2tSTtZwA.htm](abomination-vaults-bestiary-items/EzSKWtWM2tSTtZwA.htm)|Claw|auto-trad|
|[eZt77JcDJLXdqusp.htm](abomination-vaults-bestiary-items/eZt77JcDJLXdqusp.htm)|Telepathy 100 feet|auto-trad|
|[eZxgYfIRD41tf3F9.htm](abomination-vaults-bestiary-items/eZxgYfIRD41tf3F9.htm)|Radiant Ray|auto-trad|
|[F1MZjO36rRuFQXYR.htm](abomination-vaults-bestiary-items/F1MZjO36rRuFQXYR.htm)|Envenom Weapon|auto-trad|
|[f2TrT4l8m1DRgTZ4.htm](abomination-vaults-bestiary-items/f2TrT4l8m1DRgTZ4.htm)|Darkvision|auto-trad|
|[F40AJYqQ0BCC2TF0.htm](abomination-vaults-bestiary-items/F40AJYqQ0BCC2TF0.htm)|Chain|auto-trad|
|[f50ST8T8gY3pQJfx.htm](abomination-vaults-bestiary-items/f50ST8T8gY3pQJfx.htm)|Evasion|auto-trad|
|[f7rkC7qodqH9NbVI.htm](abomination-vaults-bestiary-items/f7rkC7qodqH9NbVI.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[F7zqmFxLH2v3hOAA.htm](abomination-vaults-bestiary-items/F7zqmFxLH2v3hOAA.htm)|Telepathy 100 feet|auto-trad|
|[F8VzPOjdtS2Gmx87.htm](abomination-vaults-bestiary-items/F8VzPOjdtS2Gmx87.htm)|Mind Feeding|auto-trad|
|[F8wXzsUNliJVU8bv.htm](abomination-vaults-bestiary-items/F8wXzsUNliJVU8bv.htm)|Composite Shortbow|auto-trad|
|[F8xpT7h5a5ZviXU5.htm](abomination-vaults-bestiary-items/F8xpT7h5a5ZviXU5.htm)|Feed on Fear|auto-trad|
|[Fap5Oo5cGmdLjzes.htm](abomination-vaults-bestiary-items/Fap5Oo5cGmdLjzes.htm)|Reloading Trick|auto-trad|
|[fBrZMabfv55LX5bY.htm](abomination-vaults-bestiary-items/fBrZMabfv55LX5bY.htm)|Show the Looming Moon|auto-trad|
|[fdAMCESh7OW3fKWY.htm](abomination-vaults-bestiary-items/fdAMCESh7OW3fKWY.htm)|Consume Confusion|auto-trad|
|[fDft2vRYcFM3JXBe.htm](abomination-vaults-bestiary-items/fDft2vRYcFM3JXBe.htm)|Intense Performer|auto-trad|
|[fDmw9feXNMKuAci4.htm](abomination-vaults-bestiary-items/fDmw9feXNMKuAci4.htm)|Telepathy 100 feet|auto-trad|
|[fGmd67WxTzSxDIpe.htm](abomination-vaults-bestiary-items/fGmd67WxTzSxDIpe.htm)|Tendril|auto-trad|
|[fIssfjOuDo0yfwLc.htm](abomination-vaults-bestiary-items/fIssfjOuDo0yfwLc.htm)|Eerie Flexibility|auto-trad|
|[FLe1chjATs0hNKPw.htm](abomination-vaults-bestiary-items/FLe1chjATs0hNKPw.htm)|Explosive Decay|auto-trad|
|[fMZfXQMCUD23X5H9.htm](abomination-vaults-bestiary-items/fMZfXQMCUD23X5H9.htm)|Greater Darkvision|auto-trad|
|[FN5QndPNQPuzc0V3.htm](abomination-vaults-bestiary-items/FN5QndPNQPuzc0V3.htm)|Coven Spells|auto-trad|
|[fNQ2aHpM6vTQ7iqr.htm](abomination-vaults-bestiary-items/fNQ2aHpM6vTQ7iqr.htm)|Command Confusion|auto-trad|
|[fOXEVyfWiHQRSyiC.htm](abomination-vaults-bestiary-items/fOXEVyfWiHQRSyiC.htm)|Greater Darkvision|auto-trad|
|[FsHTfpYkyQ2CCGzZ.htm](abomination-vaults-bestiary-items/FsHTfpYkyQ2CCGzZ.htm)|Light Blindness|auto-trad|
|[FteyjzEwACt18LfC.htm](abomination-vaults-bestiary-items/FteyjzEwACt18LfC.htm)|Gnawing Fog|auto-trad|
|[FuDdgAP7wwq6LCFO.htm](abomination-vaults-bestiary-items/FuDdgAP7wwq6LCFO.htm)|Serpent Venom|auto-trad|
|[fueyaPehWakc93nh.htm](abomination-vaults-bestiary-items/fueyaPehWakc93nh.htm)|Swarming Stance|auto-trad|
|[FZlSN0F2oSrYwSf4.htm](abomination-vaults-bestiary-items/FZlSN0F2oSrYwSf4.htm)|Attack of Opportunity|auto-trad|
|[g2QS3ryiZhgirGmj.htm](abomination-vaults-bestiary-items/g2QS3ryiZhgirGmj.htm)|Bog Rot|auto-trad|
|[G3bgPlOpXpztjTdf.htm](abomination-vaults-bestiary-items/G3bgPlOpXpztjTdf.htm)|Katar|auto-trad|
|[G412UtVloe7PIpa4.htm](abomination-vaults-bestiary-items/G412UtVloe7PIpa4.htm)|+2 Status to All Saves vs. Disease and Poison|auto-trad|
|[G4UvThy3GwXSiOIo.htm](abomination-vaults-bestiary-items/G4UvThy3GwXSiOIo.htm)|Low-Light Vision|auto-trad|
|[GA3sOIQc3gtd1dyN.htm](abomination-vaults-bestiary-items/GA3sOIQc3gtd1dyN.htm)|Shadow Jump|auto-trad|
|[gAui54rDueAP4nNh.htm](abomination-vaults-bestiary-items/gAui54rDueAP4nNh.htm)|Sneak Attack|auto-trad|
|[GCu6OFgRjem6F0sI.htm](abomination-vaults-bestiary-items/GCu6OFgRjem6F0sI.htm)|Witchflame|auto-trad|
|[GDIw4d1nppVwAcDM.htm](abomination-vaults-bestiary-items/GDIw4d1nppVwAcDM.htm)|Blood Magic|auto-trad|
|[gdWSFRbbf21JhOa9.htm](abomination-vaults-bestiary-items/gdWSFRbbf21JhOa9.htm)|Ectoplasmic Web Trap|auto-trad|
|[gH06P6rp2bswuGya.htm](abomination-vaults-bestiary-items/gH06P6rp2bswuGya.htm)|Jaws|auto-trad|
|[gHMs8jNdIsdBy2OZ.htm](abomination-vaults-bestiary-items/gHMs8jNdIsdBy2OZ.htm)|Shock|auto-trad|
|[Gix3fftNAeuJS7zt.htm](abomination-vaults-bestiary-items/Gix3fftNAeuJS7zt.htm)|Rituals|auto-trad|
|[gkIxUkhYZg4oHj1t.htm](abomination-vaults-bestiary-items/gkIxUkhYZg4oHj1t.htm)|+2 Status to All Saves vs. Bleed, Death Effects, Disease, Doomed, Fatigued, Paralyzed, Poison, Sickened|auto-trad|
|[gl7lB276y1xS70s3.htm](abomination-vaults-bestiary-items/gl7lB276y1xS70s3.htm)|Jaul Coordination|auto-trad|
|[GOUc826yPXfyrWNW.htm](abomination-vaults-bestiary-items/GOUc826yPXfyrWNW.htm)|Shootist's Luck|auto-trad|
|[GoYV1mj4XKxDiBOM.htm](abomination-vaults-bestiary-items/GoYV1mj4XKxDiBOM.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GpoVeh1bdg8JH2SG.htm](abomination-vaults-bestiary-items/GpoVeh1bdg8JH2SG.htm)|Deny Advantage|auto-trad|
|[GsXsiIe2IQXx1C8I.htm](abomination-vaults-bestiary-items/GsXsiIe2IQXx1C8I.htm)|At-Will Spells|auto-trad|
|[gTviRIemgZXPfEW1.htm](abomination-vaults-bestiary-items/gTviRIemgZXPfEW1.htm)|Draining Venom|auto-trad|
|[gyyRt5jyTNORzIpr.htm](abomination-vaults-bestiary-items/gyyRt5jyTNORzIpr.htm)|Defensive Needle|auto-trad|
|[GZ55vWFhwaHyhnfM.htm](abomination-vaults-bestiary-items/GZ55vWFhwaHyhnfM.htm)|Avernal Fever|auto-trad|
|[gZDMpjUjAgRTjrcL.htm](abomination-vaults-bestiary-items/gZDMpjUjAgRTjrcL.htm)|Spike|auto-trad|
|[GzMhkd0CBpUeUX63.htm](abomination-vaults-bestiary-items/GzMhkd0CBpUeUX63.htm)|Projectile Launcher|auto-trad|
|[h2qxWUvI0fbg4RxB.htm](abomination-vaults-bestiary-items/h2qxWUvI0fbg4RxB.htm)|Divine Innate Spells|auto-trad|
|[H2rT9wdiejx3F1b0.htm](abomination-vaults-bestiary-items/H2rT9wdiejx3F1b0.htm)|Light Blindness|auto-trad|
|[H34w70sTO05v4xsB.htm](abomination-vaults-bestiary-items/H34w70sTO05v4xsB.htm)|Attack of Opportunity|auto-trad|
|[H3dPRQwlEWXQFPUe.htm](abomination-vaults-bestiary-items/H3dPRQwlEWXQFPUe.htm)|At-Will Spells|auto-trad|
|[H61mcURmEhYdmt9L.htm](abomination-vaults-bestiary-items/H61mcURmEhYdmt9L.htm)|Ghoul Fever|auto-trad|
|[H6sBL4aimiKQcdtJ.htm](abomination-vaults-bestiary-items/H6sBL4aimiKQcdtJ.htm)|Snout|auto-trad|
|[HaYQ8Cd4C0hYSFdc.htm](abomination-vaults-bestiary-items/HaYQ8Cd4C0hYSFdc.htm)|Leap Attack|auto-trad|
|[HcrsT0ltPpWRSZG1.htm](abomination-vaults-bestiary-items/HcrsT0ltPpWRSZG1.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Hczu635vDDik3NIJ.htm](abomination-vaults-bestiary-items/Hczu635vDDik3NIJ.htm)|Grab|auto-trad|
|[hfZmDX2fsCvlGR4v.htm](abomination-vaults-bestiary-items/hfZmDX2fsCvlGR4v.htm)|Occult Innate Spells|auto-trad|
|[HHbU5fdUwrxiWVG3.htm](abomination-vaults-bestiary-items/HHbU5fdUwrxiWVG3.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[hIYUGmOfZ6ZD3cNt.htm](abomination-vaults-bestiary-items/hIYUGmOfZ6ZD3cNt.htm)|Darkvision|auto-trad|
|[Hl7cINV4KNf7cW0s.htm](abomination-vaults-bestiary-items/Hl7cINV4KNf7cW0s.htm)|Darkvision|auto-trad|
|[HLcMADTgT1nixxce.htm](abomination-vaults-bestiary-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|auto-trad|
|[HLLk7YCS6Ad1ibE5.htm](abomination-vaults-bestiary-items/HLLk7YCS6Ad1ibE5.htm)|Staff|auto-trad|
|[hNe75I8nMGqBfaaP.htm](abomination-vaults-bestiary-items/hNe75I8nMGqBfaaP.htm)|Rise Up|auto-trad|
|[Hor4EOQfUGAkFak1.htm](abomination-vaults-bestiary-items/Hor4EOQfUGAkFak1.htm)|Flaming Composite Longbow|auto-trad|
|[hpMBDF24fDXi90pI.htm](abomination-vaults-bestiary-items/hpMBDF24fDXi90pI.htm)|Divine Innate Spells|auto-trad|
|[hr8UQcvJzWfTNGoH.htm](abomination-vaults-bestiary-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 5|auto-trad|
|[hSFAedu25PZDak7p.htm](abomination-vaults-bestiary-items/hSFAedu25PZDak7p.htm)|Stench|auto-trad|
|[hSGaU1UT8mzmPFWl.htm](abomination-vaults-bestiary-items/hSGaU1UT8mzmPFWl.htm)|Petrifying Gaze|auto-trad|
|[HSgw6P66g1tETSK5.htm](abomination-vaults-bestiary-items/HSgw6P66g1tETSK5.htm)|Improvised Projectile|auto-trad|
|[HSib85eXEGK4xoXf.htm](abomination-vaults-bestiary-items/HSib85eXEGK4xoXf.htm)|Diabolic Quill|auto-trad|
|[HtINDVVIov9yMEhV.htm](abomination-vaults-bestiary-items/HtINDVVIov9yMEhV.htm)|Site Bound|auto-trad|
|[htwDT7obXSlNmWpU.htm](abomination-vaults-bestiary-items/htwDT7obXSlNmWpU.htm)|Longsword|auto-trad|
|[Hu1SdyNVG4WJmNcx.htm](abomination-vaults-bestiary-items/Hu1SdyNVG4WJmNcx.htm)|Soul Spells|auto-trad|
|[huUPSBc3tD6r0B4N.htm](abomination-vaults-bestiary-items/huUPSBc3tD6r0B4N.htm)|Darkvision|auto-trad|
|[hvENSxQhO8lwlD1Y.htm](abomination-vaults-bestiary-items/hvENSxQhO8lwlD1Y.htm)|Commanding Aura|auto-trad|
|[hw9SLn99zC7mqeTR.htm](abomination-vaults-bestiary-items/hw9SLn99zC7mqeTR.htm)|Stupor Poison|auto-trad|
|[hwoQ4oeElvvImKgQ.htm](abomination-vaults-bestiary-items/hwoQ4oeElvvImKgQ.htm)|Jaws|auto-trad|
|[hwqvhAAeZ5lZGags.htm](abomination-vaults-bestiary-items/hwqvhAAeZ5lZGags.htm)|Landbound|auto-trad|
|[HytGc8m6N3nhaxiO.htm](abomination-vaults-bestiary-items/HytGc8m6N3nhaxiO.htm)|Divine Innate Spells|auto-trad|
|[hZ7GydaiKm8Egcxd.htm](abomination-vaults-bestiary-items/hZ7GydaiKm8Egcxd.htm)|Quickened Casting|auto-trad|
|[I41NEY2RdcZSR1ft.htm](abomination-vaults-bestiary-items/I41NEY2RdcZSR1ft.htm)|Apocalypse Beam (from Undead Uprising)|auto-trad|
|[i4Z53PbHjjlFshli.htm](abomination-vaults-bestiary-items/i4Z53PbHjjlFshli.htm)|Primal Prepared Spells|auto-trad|
|[I4ZxiQRjcJ5rCCvp.htm](abomination-vaults-bestiary-items/I4ZxiQRjcJ5rCCvp.htm)|Magic Immunity|auto-trad|
|[i68AZkuUsYHLKLWd.htm](abomination-vaults-bestiary-items/i68AZkuUsYHLKLWd.htm)|Jaws|auto-trad|
|[i7kJARfn6bo7YXTy.htm](abomination-vaults-bestiary-items/i7kJARfn6bo7YXTy.htm)|Staff|auto-trad|
|[I7Zg9nDgRBezMT5l.htm](abomination-vaults-bestiary-items/I7Zg9nDgRBezMT5l.htm)|Darkvision|auto-trad|
|[iaTfEZ50lrTlV5Ku.htm](abomination-vaults-bestiary-items/iaTfEZ50lrTlV5Ku.htm)|Personality Fragments|auto-trad|
|[IcalXla4INxKnCPk.htm](abomination-vaults-bestiary-items/IcalXla4INxKnCPk.htm)|Occult Innate Spells|auto-trad|
|[IDg4tkvpzcQVtLok.htm](abomination-vaults-bestiary-items/IDg4tkvpzcQVtLok.htm)|Bloodline Spells|auto-trad|
|[IFp4VK6X80nXiTJu.htm](abomination-vaults-bestiary-items/IFp4VK6X80nXiTJu.htm)|Darkvision|auto-trad|
|[igyVJ2r1r0X9HNP8.htm](abomination-vaults-bestiary-items/igyVJ2r1r0X9HNP8.htm)|Moon Frenzy|auto-trad|
|[IHcY0GaB5rFEpIJT.htm](abomination-vaults-bestiary-items/IHcY0GaB5rFEpIJT.htm)|Site Bound|auto-trad|
|[IiFLZ6cZ0dZBAcee.htm](abomination-vaults-bestiary-items/IiFLZ6cZ0dZBAcee.htm)|Squirming Embrace|auto-trad|
|[iIggw0JAX98xzjTY.htm](abomination-vaults-bestiary-items/iIggw0JAX98xzjTY.htm)|Suction|auto-trad|
|[ijACdC2w6CsLO9Ks.htm](abomination-vaults-bestiary-items/ijACdC2w6CsLO9Ks.htm)|Telepathy 100 feet|auto-trad|
|[iJcL8jitqKxQqnx8.htm](abomination-vaults-bestiary-items/iJcL8jitqKxQqnx8.htm)|Grab|auto-trad|
|[IJTHmP61FLx42vVO.htm](abomination-vaults-bestiary-items/IJTHmP61FLx42vVO.htm)|Sneak Attack|auto-trad|
|[ikaHblTttb4ck8mr.htm](abomination-vaults-bestiary-items/ikaHblTttb4ck8mr.htm)|Negative Healing|auto-trad|
|[ikOLQArHJ3ox5p0j.htm](abomination-vaults-bestiary-items/ikOLQArHJ3ox5p0j.htm)|Light Blindness|auto-trad|
|[ilL5pHw5IAPVwhHU.htm](abomination-vaults-bestiary-items/ilL5pHw5IAPVwhHU.htm)|Darkvision|auto-trad|
|[IM2z6ZQvAhXIgpsu.htm](abomination-vaults-bestiary-items/IM2z6ZQvAhXIgpsu.htm)|Confusing Confrontation|auto-trad|
|[IopEu45YRMrLuNzn.htm](abomination-vaults-bestiary-items/IopEu45YRMrLuNzn.htm)|Attack of Opportunity|auto-trad|
|[ioqicnfviMKvXg9T.htm](abomination-vaults-bestiary-items/ioqicnfviMKvXg9T.htm)|Rituals|auto-trad|
|[iOvO0dFhneixzLvw.htm](abomination-vaults-bestiary-items/iOvO0dFhneixzLvw.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[Ipe3znrfNfiAvRCl.htm](abomination-vaults-bestiary-items/Ipe3znrfNfiAvRCl.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[IPH7UJLRufEnfg32.htm](abomination-vaults-bestiary-items/IPH7UJLRufEnfg32.htm)|Necrotic Decay|auto-trad|
|[iPIBtZVZ5TaPEFp6.htm](abomination-vaults-bestiary-items/iPIBtZVZ5TaPEFp6.htm)|Darkvision|auto-trad|
|[IR6YUO9Gdy6kxYo5.htm](abomination-vaults-bestiary-items/IR6YUO9Gdy6kxYo5.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Irss8fPxNC91YnDe.htm](abomination-vaults-bestiary-items/Irss8fPxNC91YnDe.htm)|Greater Darkvision|auto-trad|
|[IS350crGK3U3r6SG.htm](abomination-vaults-bestiary-items/IS350crGK3U3r6SG.htm)|Darkvision|auto-trad|
|[IuwOAyVqNq1LTtkA.htm](abomination-vaults-bestiary-items/IuwOAyVqNq1LTtkA.htm)|Arcane Prepared Spells|auto-trad|
|[iWHE4a4vmetksWd7.htm](abomination-vaults-bestiary-items/iWHE4a4vmetksWd7.htm)|Aquatic Ambush|auto-trad|
|[iwppReF9hEA7igPO.htm](abomination-vaults-bestiary-items/iwppReF9hEA7igPO.htm)|Jaws|auto-trad|
|[iY5D7YKAlye71zpy.htm](abomination-vaults-bestiary-items/iY5D7YKAlye71zpy.htm)|Scythe|auto-trad|
|[iZFB4xCQhaumJQTC.htm](abomination-vaults-bestiary-items/iZFB4xCQhaumJQTC.htm)|Death Motes|auto-trad|
|[j0uUpSfzz62wPB0o.htm](abomination-vaults-bestiary-items/j0uUpSfzz62wPB0o.htm)|Web Trap|auto-trad|
|[j1CfaIqiOES2OC3Y.htm](abomination-vaults-bestiary-items/j1CfaIqiOES2OC3Y.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[J1YjLfTIk9vqgvRy.htm](abomination-vaults-bestiary-items/J1YjLfTIk9vqgvRy.htm)|Malevolent Possession|auto-trad|
|[J3s8xl5VZ1K6APbE.htm](abomination-vaults-bestiary-items/J3s8xl5VZ1K6APbE.htm)|Envenom Weapon|auto-trad|
|[J7wgD2uQamiPxQFV.htm](abomination-vaults-bestiary-items/J7wgD2uQamiPxQFV.htm)|Darkvision|auto-trad|
|[J82EHqf8yKe7pMEq.htm](abomination-vaults-bestiary-items/J82EHqf8yKe7pMEq.htm)|Tremorsense 30 feet|auto-trad|
|[J9CjxTTQRdZozTW4.htm](abomination-vaults-bestiary-items/J9CjxTTQRdZozTW4.htm)|Right of Inspection|auto-trad|
|[j9vgTM71zw7kdn4T.htm](abomination-vaults-bestiary-items/j9vgTM71zw7kdn4T.htm)|+2 Status to All Saves vs. Disease and Poison|auto-trad|
|[JEA54JYtf51IHnRH.htm](abomination-vaults-bestiary-items/JEA54JYtf51IHnRH.htm)|Kukri|auto-trad|
|[jEONedyoxPV0Uymr.htm](abomination-vaults-bestiary-items/jEONedyoxPV0Uymr.htm)|Motion Sense 60 feet|auto-trad|
|[Jf35tOxuG3lkWtlq.htm](abomination-vaults-bestiary-items/Jf35tOxuG3lkWtlq.htm)|Sneak Attack|auto-trad|
|[JfQnN4zwkzfUcPK4.htm](abomination-vaults-bestiary-items/JfQnN4zwkzfUcPK4.htm)|Dread Flickering|auto-trad|
|[jG0R8rvY0cpJadHK.htm](abomination-vaults-bestiary-items/jG0R8rvY0cpJadHK.htm)|Fist|auto-trad|
|[JgGnEERmYAIJ8Hbc.htm](abomination-vaults-bestiary-items/JgGnEERmYAIJ8Hbc.htm)|Primal Innate Spells|auto-trad|
|[jIbBcQcAKDXMVJzj.htm](abomination-vaults-bestiary-items/jIbBcQcAKDXMVJzj.htm)|Greater Darkvision|auto-trad|
|[jl8TRoHirSz5Q9Mt.htm](abomination-vaults-bestiary-items/jl8TRoHirSz5Q9Mt.htm)|Sorcerer Bloodline Spells|auto-trad|
|[JlHuStw8Ji1vTXMi.htm](abomination-vaults-bestiary-items/JlHuStw8Ji1vTXMi.htm)|Shoulder to Shoulder|auto-trad|
|[JMc7RSkWQTx1DQOo.htm](abomination-vaults-bestiary-items/JMc7RSkWQTx1DQOo.htm)|Magic Immunity|auto-trad|
|[JMhh3dDSE8195tKc.htm](abomination-vaults-bestiary-items/JMhh3dDSE8195tKc.htm)|Occult Spontaneous Spells|auto-trad|
|[Jn4tXrVKX66VzX4O.htm](abomination-vaults-bestiary-items/Jn4tXrVKX66VzX4O.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[JPj4ayUtkVtkvYCy.htm](abomination-vaults-bestiary-items/JPj4ayUtkVtkvYCy.htm)|Consume Light|auto-trad|
|[JQ3FZtUvvt6RRRXF.htm](abomination-vaults-bestiary-items/JQ3FZtUvvt6RRRXF.htm)|Darkvision|auto-trad|
|[jqJroJQSIk3eYFSA.htm](abomination-vaults-bestiary-items/jqJroJQSIk3eYFSA.htm)|Low-Light Vision|auto-trad|
|[jr6lRjG0ZeZgW116.htm](abomination-vaults-bestiary-items/jr6lRjG0ZeZgW116.htm)|Soul Scream|auto-trad|
|[jS78EFSX9XswID7b.htm](abomination-vaults-bestiary-items/jS78EFSX9XswID7b.htm)|Light Blindness|auto-trad|
|[JSPBoCm4ZF48k1Qw.htm](abomination-vaults-bestiary-items/JSPBoCm4ZF48k1Qw.htm)|Quick Consumption|auto-trad|
|[JuEXPvUywaRE7BnJ.htm](abomination-vaults-bestiary-items/JuEXPvUywaRE7BnJ.htm)|Light Blindness|auto-trad|
|[JxMkMIPvp1ErxI39.htm](abomination-vaults-bestiary-items/JxMkMIPvp1ErxI39.htm)|Negative Healing|auto-trad|
|[k0izMsqD3Kbrn6oD.htm](abomination-vaults-bestiary-items/k0izMsqD3Kbrn6oD.htm)|Magic Immunity|auto-trad|
|[k1oAvKH4tGhwjVr5.htm](abomination-vaults-bestiary-items/k1oAvKH4tGhwjVr5.htm)|Occult Spontaneous Spells|auto-trad|
|[k2PMRhPXO7eus3sg.htm](abomination-vaults-bestiary-items/k2PMRhPXO7eus3sg.htm)|Necrotic Decay|auto-trad|
|[kaDLtkZWXkQssjdZ.htm](abomination-vaults-bestiary-items/kaDLtkZWXkQssjdZ.htm)|Adjust Shape|auto-trad|
|[kbtVRkQHlP5ap1MJ.htm](abomination-vaults-bestiary-items/kbtVRkQHlP5ap1MJ.htm)|Blowgun|auto-trad|
|[kCh7O0sugL7oCnHv.htm](abomination-vaults-bestiary-items/kCh7O0sugL7oCnHv.htm)|Darkvision|auto-trad|
|[kcKV8At9w2cFZz1T.htm](abomination-vaults-bestiary-items/kcKV8At9w2cFZz1T.htm)|Darkvision|auto-trad|
|[kcRvfKLlgq9sdmsj.htm](abomination-vaults-bestiary-items/kcRvfKLlgq9sdmsj.htm)|Golem Antimagic|auto-trad|
|[kFeAtCbycBHyIGcR.htm](abomination-vaults-bestiary-items/kFeAtCbycBHyIGcR.htm)|Attack of Opportunity|auto-trad|
|[khhjOlnkRw4wzweC.htm](abomination-vaults-bestiary-items/khhjOlnkRw4wzweC.htm)|Sunlight Powerlessness|auto-trad|
|[khwh43CDMROkIfgf.htm](abomination-vaults-bestiary-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[kJAxB9HM1zsPkPDC.htm](abomination-vaults-bestiary-items/kJAxB9HM1zsPkPDC.htm)|Tentacle|auto-trad|
|[KkhGaZRJP4cnQSOE.htm](abomination-vaults-bestiary-items/KkhGaZRJP4cnQSOE.htm)|Heavy Crossbow|auto-trad|
|[koHANYfuvrmBm3Bc.htm](abomination-vaults-bestiary-items/koHANYfuvrmBm3Bc.htm)|Light Blindness|auto-trad|
|[kppTcx5TylWkpfF1.htm](abomination-vaults-bestiary-items/kppTcx5TylWkpfF1.htm)|Light Vulnerability|auto-trad|
|[kUtQvCE4ePTqjluS.htm](abomination-vaults-bestiary-items/kUtQvCE4ePTqjluS.htm)|At-Will Spells|auto-trad|
|[Kvdwxj4PR8US0RUi.htm](abomination-vaults-bestiary-items/Kvdwxj4PR8US0RUi.htm)|Shortbow|auto-trad|
|[kwMV2uj1jCdvIm7J.htm](abomination-vaults-bestiary-items/kwMV2uj1jCdvIm7J.htm)|Beard|auto-trad|
|[kzKe7Q6umOWzUJdN.htm](abomination-vaults-bestiary-items/kzKe7Q6umOWzUJdN.htm)|At-Will Spells|auto-trad|
|[l18iYVUpNZjCrvzG.htm](abomination-vaults-bestiary-items/l18iYVUpNZjCrvzG.htm)|Necrotic Decay|auto-trad|
|[l2sX5eZSrv6H5EQ4.htm](abomination-vaults-bestiary-items/l2sX5eZSrv6H5EQ4.htm)|Sibilant Whispers|auto-trad|
|[L8z6SDNKm57PB639.htm](abomination-vaults-bestiary-items/L8z6SDNKm57PB639.htm)|Claw|auto-trad|
|[LAMXu0pzRQpS1K2p.htm](abomination-vaults-bestiary-items/LAMXu0pzRQpS1K2p.htm)|Infused Items|auto-trad|
|[LbkW4zjvlyreuGX1.htm](abomination-vaults-bestiary-items/LbkW4zjvlyreuGX1.htm)|Fangs|auto-trad|
|[LBtHMsDMDaIqyB7z.htm](abomination-vaults-bestiary-items/LBtHMsDMDaIqyB7z.htm)|Radiant Touch|auto-trad|
|[LcG0o3PUyemEv5oK.htm](abomination-vaults-bestiary-items/LcG0o3PUyemEv5oK.htm)|Darkvision|auto-trad|
|[lclcMLIuitUA02XQ.htm](abomination-vaults-bestiary-items/lclcMLIuitUA02XQ.htm)|Rituals|auto-trad|
|[ley6LXSrjfxUxe7T.htm](abomination-vaults-bestiary-items/ley6LXSrjfxUxe7T.htm)|Ward Contract|auto-trad|
|[LfqV1vPTJG0eLpId.htm](abomination-vaults-bestiary-items/LfqV1vPTJG0eLpId.htm)|+2 Status to All Saves vs. Disease and Poison|auto-trad|
|[LGBzL3vv4QNzTFJ1.htm](abomination-vaults-bestiary-items/LGBzL3vv4QNzTFJ1.htm)|Telepathy 100 feet|auto-trad|
|[LGGpAgf32ukoqCFX.htm](abomination-vaults-bestiary-items/LGGpAgf32ukoqCFX.htm)|Light Blindness|auto-trad|
|[lGZkyb82Eeg7M96l.htm](abomination-vaults-bestiary-items/lGZkyb82Eeg7M96l.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[lIpwTEFoDElIQYqN.htm](abomination-vaults-bestiary-items/lIpwTEFoDElIQYqN.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[lj2wYgitpa7YQNCt.htm](abomination-vaults-bestiary-items/lj2wYgitpa7YQNCt.htm)|Bouncing Slam|auto-trad|
|[LkqFbyPhl9cNZFyq.htm](abomination-vaults-bestiary-items/LkqFbyPhl9cNZFyq.htm)|Constant Spells|auto-trad|
|[LLnHvtR9xniSgkOM.htm](abomination-vaults-bestiary-items/LLnHvtR9xniSgkOM.htm)|Tentacle|auto-trad|
|[LNbjPm5a6CTl4lko.htm](abomination-vaults-bestiary-items/LNbjPm5a6CTl4lko.htm)|Infernal Eye|auto-trad|
|[Lnc74mXrMbKNeEpe.htm](abomination-vaults-bestiary-items/Lnc74mXrMbKNeEpe.htm)|Attack of Opportunity|auto-trad|
|[LOeBCSVBauL8spor.htm](abomination-vaults-bestiary-items/LOeBCSVBauL8spor.htm)|Black Smear Poison|auto-trad|
|[Loj0LD68R46zC1Tx.htm](abomination-vaults-bestiary-items/Loj0LD68R46zC1Tx.htm)|Rejuvenation|auto-trad|
|[lQZGx8hWnzcG2R6l.htm](abomination-vaults-bestiary-items/lQZGx8hWnzcG2R6l.htm)|Dazzling Brilliance|auto-trad|
|[LrviNgUYKAYQl4i9.htm](abomination-vaults-bestiary-items/LrviNgUYKAYQl4i9.htm)|At-Will Spells|auto-trad|
|[LRVsZzLZZ4VCKy1h.htm](abomination-vaults-bestiary-items/LRVsZzLZZ4VCKy1h.htm)|Darkvision|auto-trad|
|[LS15ASdQlOSeQgPH.htm](abomination-vaults-bestiary-items/LS15ASdQlOSeQgPH.htm)|Jaws|auto-trad|
|[lTaNtOa2ywL54YZJ.htm](abomination-vaults-bestiary-items/lTaNtOa2ywL54YZJ.htm)|Darkvision|auto-trad|
|[ltvMN7K0itu2l3WZ.htm](abomination-vaults-bestiary-items/ltvMN7K0itu2l3WZ.htm)|Snake Fangs|auto-trad|
|[lVTp2LJQAZzKNaIY.htm](abomination-vaults-bestiary-items/lVTp2LJQAZzKNaIY.htm)|Storm of Blades|auto-trad|
|[LwBTFYgYLRf8gXzt.htm](abomination-vaults-bestiary-items/LwBTFYgYLRf8gXzt.htm)|Spell Reflection|auto-trad|
|[LwePStc0eHhM9L5l.htm](abomination-vaults-bestiary-items/LwePStc0eHhM9L5l.htm)|Jaws|auto-trad|
|[lZIMAORXwdcjvy4H.htm](abomination-vaults-bestiary-items/lZIMAORXwdcjvy4H.htm)|Distracting Shot|auto-trad|
|[LZjWlCLb24UlRVSo.htm](abomination-vaults-bestiary-items/LZjWlCLb24UlRVSo.htm)|Divine Innate Spells|auto-trad|
|[m25t326n1Ovt0t5U.htm](abomination-vaults-bestiary-items/m25t326n1Ovt0t5U.htm)|Negative Healing|auto-trad|
|[m2rnilFYTvOg589S.htm](abomination-vaults-bestiary-items/m2rnilFYTvOg589S.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[M7sZToYvFNrig8TH.htm](abomination-vaults-bestiary-items/M7sZToYvFNrig8TH.htm)|Constant Spells|auto-trad|
|[M7VqYd1K43RsOInY.htm](abomination-vaults-bestiary-items/M7VqYd1K43RsOInY.htm)|Grim Glimmering|auto-trad|
|[mA7HIkvCJVI5uu1m.htm](abomination-vaults-bestiary-items/mA7HIkvCJVI5uu1m.htm)|Death Light|auto-trad|
|[mC6jPQk1bRvlBdvh.htm](abomination-vaults-bestiary-items/mC6jPQk1bRvlBdvh.htm)|Repeating Hand Crossbow|auto-trad|
|[McjPEAjdnUEE3tx6.htm](abomination-vaults-bestiary-items/McjPEAjdnUEE3tx6.htm)|Shauth Blade|auto-trad|
|[MDttImPA5a6D9APk.htm](abomination-vaults-bestiary-items/MDttImPA5a6D9APk.htm)|Feed on Magic|auto-trad|
|[mEGMPmEBPm6Apx3y.htm](abomination-vaults-bestiary-items/mEGMPmEBPm6Apx3y.htm)|Wearying Touch|auto-trad|
|[melBrcTFYaugSkrF.htm](abomination-vaults-bestiary-items/melBrcTFYaugSkrF.htm)|At-Will Spells|auto-trad|
|[MeT5lDeI05fBDoW3.htm](abomination-vaults-bestiary-items/MeT5lDeI05fBDoW3.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[mgluD1d7bZoq5JBg.htm](abomination-vaults-bestiary-items/mgluD1d7bZoq5JBg.htm)|Fangs|auto-trad|
|[MhPq4EOlBLppwQeu.htm](abomination-vaults-bestiary-items/MhPq4EOlBLppwQeu.htm)|Cocytan Filth|auto-trad|
|[MJktJ33e2ejK5I1o.htm](abomination-vaults-bestiary-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|auto-trad|
|[ml69ZMPwkVsbCTC7.htm](abomination-vaults-bestiary-items/ml69ZMPwkVsbCTC7.htm)|Storm of Tentacles|auto-trad|
|[mlcDOwUA9F64jE1Y.htm](abomination-vaults-bestiary-items/mlcDOwUA9F64jE1Y.htm)|Mouth|auto-trad|
|[mlcNFWnWIzRngDv0.htm](abomination-vaults-bestiary-items/mlcNFWnWIzRngDv0.htm)|Attack of Opportunity|auto-trad|
|[mmfsGHgB3hcCPClW.htm](abomination-vaults-bestiary-items/mmfsGHgB3hcCPClW.htm)|Apocalypse Beam (from Monster)|auto-trad|
|[MnAW3prtfTZxnv7T.htm](abomination-vaults-bestiary-items/MnAW3prtfTZxnv7T.htm)|Magic Item Mastery|auto-trad|
|[mQfS4yOFYXyHwwas.htm](abomination-vaults-bestiary-items/mQfS4yOFYXyHwwas.htm)|Surprise Attacker|auto-trad|
|[mS0Mzbjj7mazrPnr.htm](abomination-vaults-bestiary-items/mS0Mzbjj7mazrPnr.htm)|Death Flame|auto-trad|
|[mSAztmtFp4kF0SdP.htm](abomination-vaults-bestiary-items/mSAztmtFp4kF0SdP.htm)|Occult Innate Spells|auto-trad|
|[MsljHqXO1LMWSpK4.htm](abomination-vaults-bestiary-items/MsljHqXO1LMWSpK4.htm)|Spit|auto-trad|
|[mSS5Nol0cifKQmxM.htm](abomination-vaults-bestiary-items/mSS5Nol0cifKQmxM.htm)|Knockdown|auto-trad|
|[mUJY9mXKNFT1G9Ri.htm](abomination-vaults-bestiary-items/mUJY9mXKNFT1G9Ri.htm)|Negative Healing|auto-trad|
|[myXt3SwrT2L8VMW4.htm](abomination-vaults-bestiary-items/myXt3SwrT2L8VMW4.htm)|Stygian Guardian|auto-trad|
|[MZelOTUvBCjq2zbz.htm](abomination-vaults-bestiary-items/MZelOTUvBCjq2zbz.htm)|Draft Contract|auto-trad|
|[MZkr0UEdl93FQyzP.htm](abomination-vaults-bestiary-items/MZkr0UEdl93FQyzP.htm)|Bloom|auto-trad|
|[N0G4vzuAThb89i17.htm](abomination-vaults-bestiary-items/N0G4vzuAThb89i17.htm)|Rejuvenation|auto-trad|
|[N1zdZTTLXEp3x5eJ.htm](abomination-vaults-bestiary-items/N1zdZTTLXEp3x5eJ.htm)|Wicked Bite|auto-trad|
|[n2BHwPEdFYp07VvG.htm](abomination-vaults-bestiary-items/n2BHwPEdFYp07VvG.htm)|Pseudopod|auto-trad|
|[n2w1C4HwH3QTV7dw.htm](abomination-vaults-bestiary-items/n2w1C4HwH3QTV7dw.htm)|Swift Leap|auto-trad|
|[N7x5OQAAhVdxvKnV.htm](abomination-vaults-bestiary-items/N7x5OQAAhVdxvKnV.htm)|Death Shadows|auto-trad|
|[N9qNd1NMgEeyNwcF.htm](abomination-vaults-bestiary-items/N9qNd1NMgEeyNwcF.htm)|Spell Deflection|auto-trad|
|[NAX30nf7nlVx5vTo.htm](abomination-vaults-bestiary-items/NAX30nf7nlVx5vTo.htm)|Cheek Pouches|auto-trad|
|[nBbFutvNlcdf1cgZ.htm](abomination-vaults-bestiary-items/nBbFutvNlcdf1cgZ.htm)|Drown|auto-trad|
|[NBuJOqDORlJbI3m9.htm](abomination-vaults-bestiary-items/NBuJOqDORlJbI3m9.htm)|Wisp Form|auto-trad|
|[NcZMn3pf8nbMeY8x.htm](abomination-vaults-bestiary-items/NcZMn3pf8nbMeY8x.htm)|Darkvision|auto-trad|
|[nETM5TEYK8ufocn0.htm](abomination-vaults-bestiary-items/nETM5TEYK8ufocn0.htm)|Low-Light Vision|auto-trad|
|[NflnAPRnfgBSgygz.htm](abomination-vaults-bestiary-items/NflnAPRnfgBSgygz.htm)|Divine Innate Spells|auto-trad|
|[NiDXROLnOZXSEpNC.htm](abomination-vaults-bestiary-items/NiDXROLnOZXSEpNC.htm)|Jaws|auto-trad|
|[NJwXFlfXwTfxdk7U.htm](abomination-vaults-bestiary-items/NJwXFlfXwTfxdk7U.htm)|Sneak Attack|auto-trad|
|[nkgGXuCapE0gTVxH.htm](abomination-vaults-bestiary-items/nkgGXuCapE0gTVxH.htm)|Negative Healing|auto-trad|
|[nLKHrHv3rcodZfOf.htm](abomination-vaults-bestiary-items/nLKHrHv3rcodZfOf.htm)|Seugathi Venom|auto-trad|
|[nNGqk0aQljFVuL8g.htm](abomination-vaults-bestiary-items/nNGqk0aQljFVuL8g.htm)|Rituals|auto-trad|
|[nQoYz6B0Wx0OjS4e.htm](abomination-vaults-bestiary-items/nQoYz6B0Wx0OjS4e.htm)|Undulating Step|auto-trad|
|[NrJvXEDlrKMp5Eh0.htm](abomination-vaults-bestiary-items/NrJvXEDlrKMp5Eh0.htm)|Fist|auto-trad|
|[nSxXMYPDUloleNgn.htm](abomination-vaults-bestiary-items/nSxXMYPDUloleNgn.htm)|Warding Shove|auto-trad|
|[NTAt5R3Zz1e8OAHf.htm](abomination-vaults-bestiary-items/NTAt5R3Zz1e8OAHf.htm)|War Flail|auto-trad|
|[NTmAGY84p18KWTa8.htm](abomination-vaults-bestiary-items/NTmAGY84p18KWTa8.htm)|+2 Status To All Saves vs. Mental|auto-trad|
|[NuYsILwFjvqlwWPp.htm](abomination-vaults-bestiary-items/NuYsILwFjvqlwWPp.htm)|Darkvision|auto-trad|
|[nvaQh9xjojojnL27.htm](abomination-vaults-bestiary-items/nvaQh9xjojojnL27.htm)|Attack Now!|auto-trad|
|[NVdOFyf2EscpiksE.htm](abomination-vaults-bestiary-items/NVdOFyf2EscpiksE.htm)|Battle Axe|auto-trad|
|[Nwtd1G94Ikab4Ejf.htm](abomination-vaults-bestiary-items/Nwtd1G94Ikab4Ejf.htm)|Scuttling Attack|auto-trad|
|[NwuVG96DJON1Txro.htm](abomination-vaults-bestiary-items/NwuVG96DJON1Txro.htm)|Constant Spells|auto-trad|
|[NXk6QjCfO8eW6Txp.htm](abomination-vaults-bestiary-items/NXk6QjCfO8eW6Txp.htm)|Sneak Attack|auto-trad|
|[NyXCVjhHwWZtpfOI.htm](abomination-vaults-bestiary-items/NyXCVjhHwWZtpfOI.htm)|Consume Flesh|auto-trad|
|[NzFY8G1K3Mfksj2F.htm](abomination-vaults-bestiary-items/NzFY8G1K3Mfksj2F.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[NzV2MTJJsvUDXOUv.htm](abomination-vaults-bestiary-items/NzV2MTJJsvUDXOUv.htm)|Coven Ritual|auto-trad|
|[nzWpj7yo8PguGBFG.htm](abomination-vaults-bestiary-items/nzWpj7yo8PguGBFG.htm)|Bite|auto-trad|
|[O2CT9C7oM6YOLZNX.htm](abomination-vaults-bestiary-items/O2CT9C7oM6YOLZNX.htm)|Glow|auto-trad|
|[o40ddeHAtx8cJ0uY.htm](abomination-vaults-bestiary-items/o40ddeHAtx8cJ0uY.htm)|Mind Lash|auto-trad|
|[oAOkRgFO0POryMdo.htm](abomination-vaults-bestiary-items/oAOkRgFO0POryMdo.htm)|Percussive Reverberation|auto-trad|
|[obfPUaVoDmOFchMR.htm](abomination-vaults-bestiary-items/obfPUaVoDmOFchMR.htm)|Jaws|auto-trad|
|[od2Tpdqgyx5vuTIw.htm](abomination-vaults-bestiary-items/od2Tpdqgyx5vuTIw.htm)|Poison Weapon|auto-trad|
|[Oevi96zftiaBjAOH.htm](abomination-vaults-bestiary-items/Oevi96zftiaBjAOH.htm)|Occult Innate Spells|auto-trad|
|[oeWKbVvK3lP66J9I.htm](abomination-vaults-bestiary-items/oeWKbVvK3lP66J9I.htm)|Rejuvenation|auto-trad|
|[OHEkKMalMOyLMyRM.htm](abomination-vaults-bestiary-items/OHEkKMalMOyLMyRM.htm)|Draining Touch|auto-trad|
|[ohL0OcnbucaAroHs.htm](abomination-vaults-bestiary-items/ohL0OcnbucaAroHs.htm)|Deft Evasion|auto-trad|
|[OIcVAsJUARlaypEm.htm](abomination-vaults-bestiary-items/OIcVAsJUARlaypEm.htm)|At-Will Spells|auto-trad|
|[Okdzr1mvuSTh4c4K.htm](abomination-vaults-bestiary-items/Okdzr1mvuSTh4c4K.htm)|Battle Lute|auto-trad|
|[oKefVQZfmh0iqIhc.htm](abomination-vaults-bestiary-items/oKefVQZfmh0iqIhc.htm)|No MAP|auto-trad|
|[OKsFbAOCubRBtq1O.htm](abomination-vaults-bestiary-items/OKsFbAOCubRBtq1O.htm)|Occult Ward|auto-trad|
|[OksJxFiBFjS3xboM.htm](abomination-vaults-bestiary-items/OksJxFiBFjS3xboM.htm)|Mark for Death|auto-trad|
|[oKuYToU9Nv9ALIIv.htm](abomination-vaults-bestiary-items/oKuYToU9Nv9ALIIv.htm)|Rejuvenation|auto-trad|
|[OlnYDBwZvO3CeHG3.htm](abomination-vaults-bestiary-items/OlnYDBwZvO3CeHG3.htm)|Force Blast|auto-trad|
|[oLOpCzPI98yXuw1u.htm](abomination-vaults-bestiary-items/oLOpCzPI98yXuw1u.htm)|Consume Flesh|auto-trad|
|[om8dBOucNQt5iWeZ.htm](abomination-vaults-bestiary-items/om8dBOucNQt5iWeZ.htm)|Domain Spells|auto-trad|
|[oMcchH9BcuFxkNKA.htm](abomination-vaults-bestiary-items/oMcchH9BcuFxkNKA.htm)|Haunted Lighthouse|auto-trad|
|[OMfPkRtQLgK6lUE8.htm](abomination-vaults-bestiary-items/OMfPkRtQLgK6lUE8.htm)|Gatekeeper's Will|auto-trad|
|[onlqduhJCOKTLTGj.htm](abomination-vaults-bestiary-items/onlqduhJCOKTLTGj.htm)|At-Will Spells|auto-trad|
|[OOe8PxQr9xXaWkvc.htm](abomination-vaults-bestiary-items/OOe8PxQr9xXaWkvc.htm)|Hand Crossbow|auto-trad|
|[OOpmi6uvkCP25AwD.htm](abomination-vaults-bestiary-items/OOpmi6uvkCP25AwD.htm)|Bravery|auto-trad|
|[OpUEqqVcx7trG4mu.htm](abomination-vaults-bestiary-items/OpUEqqVcx7trG4mu.htm)|Kukri|auto-trad|
|[OqYpi6JullFs4WMU.htm](abomination-vaults-bestiary-items/OqYpi6JullFs4WMU.htm)|Negative Healing|auto-trad|
|[OSiq1CXChzTT7udW.htm](abomination-vaults-bestiary-items/OSiq1CXChzTT7udW.htm)|Spectral Corruption|auto-trad|
|[osvhHIWUUPwyQszY.htm](abomination-vaults-bestiary-items/osvhHIWUUPwyQszY.htm)|Tremorsense 30 feet|auto-trad|
|[oviUeLIMpj5EZvoL.htm](abomination-vaults-bestiary-items/oviUeLIMpj5EZvoL.htm)|Negative Healing|auto-trad|
|[OvlSSz4vVMTtlzue.htm](abomination-vaults-bestiary-items/OvlSSz4vVMTtlzue.htm)|Telepathy 100 feet|auto-trad|
|[OxVsLGBZ8k9rqQxY.htm](abomination-vaults-bestiary-items/OxVsLGBZ8k9rqQxY.htm)|Jaws|auto-trad|
|[P0CkVE8kq3xALSWx.htm](abomination-vaults-bestiary-items/P0CkVE8kq3xALSWx.htm)|Negative Healing|auto-trad|
|[P1ggE8aYgCQOaac1.htm](abomination-vaults-bestiary-items/P1ggE8aYgCQOaac1.htm)|Death Burst|auto-trad|
|[P2nBJI904M5iiMWb.htm](abomination-vaults-bestiary-items/P2nBJI904M5iiMWb.htm)|Shortsword|auto-trad|
|[P7RUoae9G01AaAHd.htm](abomination-vaults-bestiary-items/P7RUoae9G01AaAHd.htm)|Negative Healing|auto-trad|
|[p9kCNA8Mq9cXm23t.htm](abomination-vaults-bestiary-items/p9kCNA8Mq9cXm23t.htm)|Darkvision|auto-trad|
|[pb8PZlMfMR2NS3Vt.htm](abomination-vaults-bestiary-items/pb8PZlMfMR2NS3Vt.htm)|War Leader|auto-trad|
|[PBrXRWLJMqALmC7c.htm](abomination-vaults-bestiary-items/PBrXRWLJMqALmC7c.htm)|Greater Darkvision|auto-trad|
|[PCXa7Z3d10U79Quj.htm](abomination-vaults-bestiary-items/PCXa7Z3d10U79Quj.htm)|Vengeful Anger|auto-trad|
|[PDBItJftGnhDq2dg.htm](abomination-vaults-bestiary-items/PDBItJftGnhDq2dg.htm)|At-Will Spells|auto-trad|
|[peS8oEAOCjaeRDhC.htm](abomination-vaults-bestiary-items/peS8oEAOCjaeRDhC.htm)|Rituals|auto-trad|
|[PFH04FMn9c52nnhf.htm](abomination-vaults-bestiary-items/PFH04FMn9c52nnhf.htm)|Sneak Attack|auto-trad|
|[Pgdez88Zqu0dqgGI.htm](abomination-vaults-bestiary-items/Pgdez88Zqu0dqgGI.htm)|Despairing Cry|auto-trad|
|[pi0jucwlFYIo5mn8.htm](abomination-vaults-bestiary-items/pi0jucwlFYIo5mn8.htm)|Discorporate|auto-trad|
|[pJ6eBDUVUixzX9UL.htm](abomination-vaults-bestiary-items/pJ6eBDUVUixzX9UL.htm)|Animate Chains|auto-trad|
|[PJPuhtPtTcNoLZS4.htm](abomination-vaults-bestiary-items/PJPuhtPtTcNoLZS4.htm)|Tamchal Chakram|auto-trad|
|[PkDvZBCHiGCocxUn.htm](abomination-vaults-bestiary-items/PkDvZBCHiGCocxUn.htm)|Primal Innate Spells|auto-trad|
|[pKPJ4u9g0HQyU7b6.htm](abomination-vaults-bestiary-items/pKPJ4u9g0HQyU7b6.htm)|Furious Fusillade|auto-trad|
|[PM9U1Ca1DQ2ZA7ZJ.htm](abomination-vaults-bestiary-items/PM9U1Ca1DQ2ZA7ZJ.htm)|Negative Healing|auto-trad|
|[POLjXIhCSeQBrvcV.htm](abomination-vaults-bestiary-items/POLjXIhCSeQBrvcV.htm)|Greater Darkvision|auto-trad|
|[pozH1aOlixvNcvU6.htm](abomination-vaults-bestiary-items/pozH1aOlixvNcvU6.htm)|Burn Knowledge|auto-trad|
|[PpFajj7OukFhYId7.htm](abomination-vaults-bestiary-items/PpFajj7OukFhYId7.htm)|Darkvision|auto-trad|
|[Pr87C99kuBwyUE2x.htm](abomination-vaults-bestiary-items/Pr87C99kuBwyUE2x.htm)|Spirit Sight (Precise) 30 feet|auto-trad|
|[pRRtkR99srCWUO1j.htm](abomination-vaults-bestiary-items/pRRtkR99srCWUO1j.htm)|Corrosive Mass|auto-trad|
|[PrsRCxi6j7SHJeMu.htm](abomination-vaults-bestiary-items/PrsRCxi6j7SHJeMu.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[pViUeTjTLhTubzOw.htm](abomination-vaults-bestiary-items/pViUeTjTLhTubzOw.htm)|Coven|auto-trad|
|[PVrWJHEarOi9LFbD.htm](abomination-vaults-bestiary-items/PVrWJHEarOi9LFbD.htm)|Rope Snare|auto-trad|
|[PXA2uFcT0VRiQkBO.htm](abomination-vaults-bestiary-items/PXA2uFcT0VRiQkBO.htm)|Fearful Curse|auto-trad|
|[pyBAuiHwkCVhaTe8.htm](abomination-vaults-bestiary-items/pyBAuiHwkCVhaTe8.htm)|Shock|auto-trad|
|[pZ4b9ZPNlMtoF8P7.htm](abomination-vaults-bestiary-items/pZ4b9ZPNlMtoF8P7.htm)|Bite|auto-trad|
|[q0kSVZllWCY1CwD2.htm](abomination-vaults-bestiary-items/q0kSVZllWCY1CwD2.htm)|Darkvision|auto-trad|
|[Q43gFqCqgbv9y5VO.htm](abomination-vaults-bestiary-items/Q43gFqCqgbv9y5VO.htm)|Skirmish Strike|auto-trad|
|[Q4ib52AxEVe6nHUx.htm](abomination-vaults-bestiary-items/Q4ib52AxEVe6nHUx.htm)|Gauntlight Beam|auto-trad|
|[Q4NBsMEwBKrHlvNK.htm](abomination-vaults-bestiary-items/Q4NBsMEwBKrHlvNK.htm)|Oily Scales|auto-trad|
|[Q4RY5CXdMVoB93xD.htm](abomination-vaults-bestiary-items/Q4RY5CXdMVoB93xD.htm)|Underground Stride|auto-trad|
|[Q8iuI7tt5UHoTXnP.htm](abomination-vaults-bestiary-items/Q8iuI7tt5UHoTXnP.htm)|Swift Leap|auto-trad|
|[QaqXp54jr3aKQjWN.htm](abomination-vaults-bestiary-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|auto-trad|
|[Qc2AY3uXl9fGeDBe.htm](abomination-vaults-bestiary-items/Qc2AY3uXl9fGeDBe.htm)|Hampering Slash|auto-trad|
|[Qc5SGmGRuhj2vMYt.htm](abomination-vaults-bestiary-items/Qc5SGmGRuhj2vMYt.htm)|Loose Bones|auto-trad|
|[QcW3MprdYsKVeRT7.htm](abomination-vaults-bestiary-items/QcW3MprdYsKVeRT7.htm)|Flood of Despair|auto-trad|
|[QdYWcouo4wSAer3W.htm](abomination-vaults-bestiary-items/QdYWcouo4wSAer3W.htm)|Occult Prepared Spells|auto-trad|
|[qFopdg6xXJDmE4k7.htm](abomination-vaults-bestiary-items/qFopdg6xXJDmE4k7.htm)|Cavern Distortion|auto-trad|
|[qh8LFXVLWjILfZZx.htm](abomination-vaults-bestiary-items/qh8LFXVLWjILfZZx.htm)|Sunlight Powerlessness|auto-trad|
|[QHc3i64UJqwenXAH.htm](abomination-vaults-bestiary-items/QHc3i64UJqwenXAH.htm)|Darkvision|auto-trad|
|[Qhmy1EmaoDYJuVdZ.htm](abomination-vaults-bestiary-items/Qhmy1EmaoDYJuVdZ.htm)|Grab|auto-trad|
|[QJJWPGE3n8ZduC9D.htm](abomination-vaults-bestiary-items/QJJWPGE3n8ZduC9D.htm)|Light the Living Wick|auto-trad|
|[Qkqj9KzvAaoHkxjH.htm](abomination-vaults-bestiary-items/Qkqj9KzvAaoHkxjH.htm)|Darkvision|auto-trad|
|[Qkr3ZHA5WjD7gYCY.htm](abomination-vaults-bestiary-items/Qkr3ZHA5WjD7gYCY.htm)|Warding Script|auto-trad|
|[QlygOvoO8v1q3qZ1.htm](abomination-vaults-bestiary-items/QlygOvoO8v1q3qZ1.htm)|Swarming Stance|auto-trad|
|[Qmj5gqP1YgGLr1W3.htm](abomination-vaults-bestiary-items/Qmj5gqP1YgGLr1W3.htm)|Rhoka Sword|auto-trad|
|[QOG4x5d8jOfeGb6l.htm](abomination-vaults-bestiary-items/QOG4x5d8jOfeGb6l.htm)|Negative Healing|auto-trad|
|[QPbTNI3BxNTdGPuH.htm](abomination-vaults-bestiary-items/QPbTNI3BxNTdGPuH.htm)|Divine Prepared Spells|auto-trad|
|[QrVRCf0qabKVREzS.htm](abomination-vaults-bestiary-items/QrVRCf0qabKVREzS.htm)|Darkvision|auto-trad|
|[QSEVW6fObs2AVajU.htm](abomination-vaults-bestiary-items/QSEVW6fObs2AVajU.htm)|Corrupting Gaze|auto-trad|
|[qtILk42sO65XzDmW.htm](abomination-vaults-bestiary-items/qtILk42sO65XzDmW.htm)|Consume Flesh|auto-trad|
|[qTIxm70Zd6ETxfgu.htm](abomination-vaults-bestiary-items/qTIxm70Zd6ETxfgu.htm)|Insightful Swing|auto-trad|
|[qtqSaGbHHzYr1Zvx.htm](abomination-vaults-bestiary-items/qtqSaGbHHzYr1Zvx.htm)|Rusty Chains|auto-trad|
|[Qw2zvLnJQFja72D2.htm](abomination-vaults-bestiary-items/Qw2zvLnJQFja72D2.htm)|Darkvision|auto-trad|
|[qwwxBH4SCVdtmTb2.htm](abomination-vaults-bestiary-items/qwwxBH4SCVdtmTb2.htm)|Sneak Attack|auto-trad|
|[qykfSUOQXBbtu4gM.htm](abomination-vaults-bestiary-items/qykfSUOQXBbtu4gM.htm)|Frenzied Attack|auto-trad|
|[qyWEf4ehAOLXTLjf.htm](abomination-vaults-bestiary-items/qyWEf4ehAOLXTLjf.htm)|Seugathi Venom|auto-trad|
|[QZup3O3zcKH6TFZd.htm](abomination-vaults-bestiary-items/QZup3O3zcKH6TFZd.htm)|Sneak Attack|auto-trad|
|[R0x59cKzEcWsxbbV.htm](abomination-vaults-bestiary-items/R0x59cKzEcWsxbbV.htm)|Longsword|auto-trad|
|[R2IrxhTRU85GEr92.htm](abomination-vaults-bestiary-items/R2IrxhTRU85GEr92.htm)|Swallow Whole|auto-trad|
|[R6nrltKyDBZG2PNf.htm](abomination-vaults-bestiary-items/R6nrltKyDBZG2PNf.htm)|All-Around Vision|auto-trad|
|[RatED2kNCjAiVv2a.htm](abomination-vaults-bestiary-items/RatED2kNCjAiVv2a.htm)|Fist|auto-trad|
|[RBjfyeeaXd5gEMNe.htm](abomination-vaults-bestiary-items/RBjfyeeaXd5gEMNe.htm)|Curse of the Werewolf|auto-trad|
|[Rc6ZoOmMG3mlyrrV.htm](abomination-vaults-bestiary-items/Rc6ZoOmMG3mlyrrV.htm)|Spore Explosion|auto-trad|
|[rdEPYGmtMUuJ7I3P.htm](abomination-vaults-bestiary-items/rdEPYGmtMUuJ7I3P.htm)|Fearful Strike|auto-trad|
|[re67Wr8G5ybq0iEY.htm](abomination-vaults-bestiary-items/re67Wr8G5ybq0iEY.htm)|Survivor's Nourishment|auto-trad|
|[RGaUdIjwMpgnNYGg.htm](abomination-vaults-bestiary-items/RGaUdIjwMpgnNYGg.htm)|Pain Starvation|auto-trad|
|[rGX35xgDAZROYYQF.htm](abomination-vaults-bestiary-items/rGX35xgDAZROYYQF.htm)|Rejuvenation|auto-trad|
|[RhLJUtGDkZup2Y2e.htm](abomination-vaults-bestiary-items/RhLJUtGDkZup2Y2e.htm)|Fleshy Slap|auto-trad|
|[Ri0eYhcAj6M7CX7f.htm](abomination-vaults-bestiary-items/Ri0eYhcAj6M7CX7f.htm)|All-Around Vision|auto-trad|
|[rJLYYTfUR7ew1R0A.htm](abomination-vaults-bestiary-items/rJLYYTfUR7ew1R0A.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[rLUQOqHBgpcUnpXn.htm](abomination-vaults-bestiary-items/rLUQOqHBgpcUnpXn.htm)|Occult Spontaneous Spells|auto-trad|
|[rq016ZULtzeq5Jdo.htm](abomination-vaults-bestiary-items/rq016ZULtzeq5Jdo.htm)|Greater Darkvision|auto-trad|
|[RQSPQLwfW9A5Ijew.htm](abomination-vaults-bestiary-items/RQSPQLwfW9A5Ijew.htm)|Apocalypse Beam (from Burning City)|auto-trad|
|[Rt1kYgHWRK0u7WPS.htm](abomination-vaults-bestiary-items/Rt1kYgHWRK0u7WPS.htm)|Darkvision|auto-trad|
|[rV1k0lGcAvoytfnj.htm](abomination-vaults-bestiary-items/rV1k0lGcAvoytfnj.htm)|Tendril|auto-trad|
|[RwS0OFipezRfdrZG.htm](abomination-vaults-bestiary-items/RwS0OFipezRfdrZG.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[rzTC3Hz3hT9xd3W9.htm](abomination-vaults-bestiary-items/rzTC3Hz3hT9xd3W9.htm)|Consume Tattooed Flesh|auto-trad|
|[S0eUokfR9NCiHTkD.htm](abomination-vaults-bestiary-items/S0eUokfR9NCiHTkD.htm)|Wolf Empathy|auto-trad|
|[S0PyywlB6YgiEU7L.htm](abomination-vaults-bestiary-items/S0PyywlB6YgiEU7L.htm)|Light Blindness|auto-trad|
|[S31ljAQqgmPf1CMj.htm](abomination-vaults-bestiary-items/S31ljAQqgmPf1CMj.htm)|Constrict (Grabbed by Claws only)|auto-trad|
|[s48Fu4ttbbhaVZUd.htm](abomination-vaults-bestiary-items/s48Fu4ttbbhaVZUd.htm)|Jaws|auto-trad|
|[S4gQomqdPJUG2IPl.htm](abomination-vaults-bestiary-items/S4gQomqdPJUG2IPl.htm)|Acid Flask|auto-trad|
|[s57i0xdwt99HF3Tp.htm](abomination-vaults-bestiary-items/s57i0xdwt99HF3Tp.htm)|Trident|auto-trad|
|[S9nPU36oRplQB6mn.htm](abomination-vaults-bestiary-items/S9nPU36oRplQB6mn.htm)|Opportune Step|auto-trad|
|[SaMUZistsZa0Bskd.htm](abomination-vaults-bestiary-items/SaMUZistsZa0Bskd.htm)|All-Around Vision|auto-trad|
|[sAuhwgcHiN60xIJT.htm](abomination-vaults-bestiary-items/sAuhwgcHiN60xIJT.htm)|Swarming|auto-trad|
|[SawmEH90X7bn6dTY.htm](abomination-vaults-bestiary-items/SawmEH90X7bn6dTY.htm)|Darkvision|auto-trad|
|[SC9Vfc2U7mtfdimN.htm](abomination-vaults-bestiary-items/SC9Vfc2U7mtfdimN.htm)|Death Flame|auto-trad|
|[sdsJsKNzbvVK8R4I.htm](abomination-vaults-bestiary-items/sdsJsKNzbvVK8R4I.htm)|Wooden Chair|auto-trad|
|[sEiZ1blIoaIiKSvz.htm](abomination-vaults-bestiary-items/sEiZ1blIoaIiKSvz.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[SeJXe222cuooTJUx.htm](abomination-vaults-bestiary-items/SeJXe222cuooTJUx.htm)|Split|auto-trad|
|[SHNbQciOGHNt8z1c.htm](abomination-vaults-bestiary-items/SHNbQciOGHNt8z1c.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[sIBET4O7QFkxNkyF.htm](abomination-vaults-bestiary-items/sIBET4O7QFkxNkyF.htm)|Swamp Stride|auto-trad|
|[SiVeimRpwflBeUiq.htm](abomination-vaults-bestiary-items/SiVeimRpwflBeUiq.htm)|Consume Masterpiece|auto-trad|
|[sJW19BG0UI1qi3ct.htm](abomination-vaults-bestiary-items/sJW19BG0UI1qi3ct.htm)|Divine Innate Spells|auto-trad|
|[Ska9kA0i2bCxOPKc.htm](abomination-vaults-bestiary-items/Ska9kA0i2bCxOPKc.htm)|Hateful Memories|auto-trad|
|[smJkeUn705exUfuJ.htm](abomination-vaults-bestiary-items/smJkeUn705exUfuJ.htm)|Negative Healing|auto-trad|
|[SNJ0RSCREyP9YYKY.htm](abomination-vaults-bestiary-items/SNJ0RSCREyP9YYKY.htm)|Infernal Wound|auto-trad|
|[snOrUVs5f8v8gtuP.htm](abomination-vaults-bestiary-items/snOrUVs5f8v8gtuP.htm)|Graveknight's Curse|auto-trad|
|[sslSR9PEr8cWdVop.htm](abomination-vaults-bestiary-items/sslSR9PEr8cWdVop.htm)|Steal Shadow|auto-trad|
|[STdfpuDxhppjlflP.htm](abomination-vaults-bestiary-items/STdfpuDxhppjlflP.htm)|Skillful Catch|auto-trad|
|[SuNMXHWTyC2lF2Pa.htm](abomination-vaults-bestiary-items/SuNMXHWTyC2lF2Pa.htm)|Claw|auto-trad|
|[sVvkzILqsBCfcw03.htm](abomination-vaults-bestiary-items/sVvkzILqsBCfcw03.htm)|Ghostly Hand|auto-trad|
|[sXswvc2qcns84U92.htm](abomination-vaults-bestiary-items/sXswvc2qcns84U92.htm)|Dagger|auto-trad|
|[T108hwN1OhACYQdQ.htm](abomination-vaults-bestiary-items/T108hwN1OhACYQdQ.htm)|Attack of Opportunity|auto-trad|
|[t1m1XIVqXFEnCTKv.htm](abomination-vaults-bestiary-items/t1m1XIVqXFEnCTKv.htm)|Mindfog Aura|auto-trad|
|[t4jPFFTR7Eur2Nml.htm](abomination-vaults-bestiary-items/t4jPFFTR7Eur2Nml.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[t4QXi08ulbzmSfHt.htm](abomination-vaults-bestiary-items/t4QXi08ulbzmSfHt.htm)|Divine Prepared Spells|auto-trad|
|[t6KHJCiZXdNJnqxB.htm](abomination-vaults-bestiary-items/t6KHJCiZXdNJnqxB.htm)|Weapon Master|auto-trad|
|[T8KOcWxiqo3StVBo.htm](abomination-vaults-bestiary-items/T8KOcWxiqo3StVBo.htm)|Spit|auto-trad|
|[t9ReCb126y5KXuru.htm](abomination-vaults-bestiary-items/t9ReCb126y5KXuru.htm)|Opportune Witchflame|auto-trad|
|[T9tAbwnyo4ZmiWkk.htm](abomination-vaults-bestiary-items/T9tAbwnyo4ZmiWkk.htm)|Fervent Command|auto-trad|
|[TBlIXTNQSQ1e3bEo.htm](abomination-vaults-bestiary-items/TBlIXTNQSQ1e3bEo.htm)|Snout|auto-trad|
|[TdzSpCHa1QM7Auhy.htm](abomination-vaults-bestiary-items/TdzSpCHa1QM7Auhy.htm)|Distracting Declaration|auto-trad|
|[TfHuj8LvtfwsXfXQ.htm](abomination-vaults-bestiary-items/TfHuj8LvtfwsXfXQ.htm)|Tentacle Arm|auto-trad|
|[thNxqBSaHQwe1kqj.htm](abomination-vaults-bestiary-items/thNxqBSaHQwe1kqj.htm)|Primal Prepared Spells|auto-trad|
|[tIvtO7TbZgSNlr3F.htm](abomination-vaults-bestiary-items/tIvtO7TbZgSNlr3F.htm)|Constant Spells|auto-trad|
|[tjQCRx8UN4LQPnwJ.htm](abomination-vaults-bestiary-items/tjQCRx8UN4LQPnwJ.htm)|Witchflame Kindling|auto-trad|
|[tJRkLCUMgqynhzQ9.htm](abomination-vaults-bestiary-items/tJRkLCUMgqynhzQ9.htm)|Infested Shadow|auto-trad|
|[tjRomYaatwAgMUvv.htm](abomination-vaults-bestiary-items/tjRomYaatwAgMUvv.htm)|Partially Technological|auto-trad|
|[TKCoF7GXS2ldg658.htm](abomination-vaults-bestiary-items/TKCoF7GXS2ldg658.htm)|Jaws|auto-trad|
|[TM2S9CZ5bTyxKkXp.htm](abomination-vaults-bestiary-items/TM2S9CZ5bTyxKkXp.htm)|Unnerving Gaze|auto-trad|
|[tNfhf1JnOxOHfY86.htm](abomination-vaults-bestiary-items/tNfhf1JnOxOHfY86.htm)|Envenom Weapon|auto-trad|
|[TPGcPPTzpn0NMPGl.htm](abomination-vaults-bestiary-items/TPGcPPTzpn0NMPGl.htm)|Sneak Attack|auto-trad|
|[Tq0WVPPxrkpFbZn4.htm](abomination-vaults-bestiary-items/Tq0WVPPxrkpFbZn4.htm)|Fist|auto-trad|
|[tqSmdxFVDmY2CLns.htm](abomination-vaults-bestiary-items/tqSmdxFVDmY2CLns.htm)|Heavy Aura|auto-trad|
|[tW1jcJOAZ3y2pThe.htm](abomination-vaults-bestiary-items/tW1jcJOAZ3y2pThe.htm)|Starknife|auto-trad|
|[tWnV5NxscBllgsSs.htm](abomination-vaults-bestiary-items/tWnV5NxscBllgsSs.htm)|Magic Sense|auto-trad|
|[Tx79k8XzQHlqc3ew.htm](abomination-vaults-bestiary-items/Tx79k8XzQHlqc3ew.htm)|Dagger|auto-trad|
|[tZ3KCPTZhq8XGVMM.htm](abomination-vaults-bestiary-items/tZ3KCPTZhq8XGVMM.htm)|Rituals|auto-trad|
|[TzpdMinu4WoEyAY9.htm](abomination-vaults-bestiary-items/TzpdMinu4WoEyAY9.htm)|Occult Innate Spells|auto-trad|
|[U0pfy4tbvu2Y9KYj.htm](abomination-vaults-bestiary-items/U0pfy4tbvu2Y9KYj.htm)|Sneak Attack|auto-trad|
|[U83VZh2S8gyX5Kvp.htm](abomination-vaults-bestiary-items/U83VZh2S8gyX5Kvp.htm)|Sneak Attack|auto-trad|
|[u8Tqev49a0Iwqy80.htm](abomination-vaults-bestiary-items/u8Tqev49a0Iwqy80.htm)|At-Will Spells|auto-trad|
|[u9UlDj5Q35Cp8lmR.htm](abomination-vaults-bestiary-items/u9UlDj5Q35Cp8lmR.htm)|All-Around Vision|auto-trad|
|[UAaoMzfNeh44Gojl.htm](abomination-vaults-bestiary-items/UAaoMzfNeh44Gojl.htm)|Bone Shard|auto-trad|
|[UakPnn8jYjY9gCdo.htm](abomination-vaults-bestiary-items/UakPnn8jYjY9gCdo.htm)|Rope|auto-trad|
|[UbtJOXzzsAmXD5rI.htm](abomination-vaults-bestiary-items/UbtJOXzzsAmXD5rI.htm)|Swarm Shape|auto-trad|
|[ufvpAL31F9ZaHsEo.htm](abomination-vaults-bestiary-items/ufvpAL31F9ZaHsEo.htm)|Bounding Swarm|auto-trad|
|[UI4pCPb4yqFcryJ6.htm](abomination-vaults-bestiary-items/UI4pCPb4yqFcryJ6.htm)|+2 Status To All Saves vs. Mental|auto-trad|
|[UIrt3wtjXEhvQtEL.htm](abomination-vaults-bestiary-items/UIrt3wtjXEhvQtEL.htm)|Powerful Stench|auto-trad|
|[uItSOCTzoCrocBon.htm](abomination-vaults-bestiary-items/uItSOCTzoCrocBon.htm)|Corpse Sense (Precise) 30 feet|auto-trad|
|[UK5IIgsnGnQ6Oh6K.htm](abomination-vaults-bestiary-items/UK5IIgsnGnQ6Oh6K.htm)|Negative Healing|auto-trad|
|[uKvKe5cNgTA0yIxl.htm](abomination-vaults-bestiary-items/uKvKe5cNgTA0yIxl.htm)|Overpowering Jaws|auto-trad|
|[UluC5rRvQl0yriAj.htm](abomination-vaults-bestiary-items/UluC5rRvQl0yriAj.htm)|Divine Innate Spells|auto-trad|
|[UP3MBQg1Px5Bkp7a.htm](abomination-vaults-bestiary-items/UP3MBQg1Px5Bkp7a.htm)|Dagger|auto-trad|
|[upJaEvHTKGpsW57H.htm](abomination-vaults-bestiary-items/upJaEvHTKGpsW57H.htm)|Flaming Longsword|auto-trad|
|[UQ4kfO1kRCsEtz0l.htm](abomination-vaults-bestiary-items/UQ4kfO1kRCsEtz0l.htm)|Divine Innate Spells|auto-trad|
|[USCoVo5LJNs12zB4.htm](abomination-vaults-bestiary-items/USCoVo5LJNs12zB4.htm)|Go Dark|auto-trad|
|[Usk5KyF9A2RQaJt2.htm](abomination-vaults-bestiary-items/Usk5KyF9A2RQaJt2.htm)|Flurry of Blows|auto-trad|
|[uuED3LvzlH3CHS3S.htm](abomination-vaults-bestiary-items/uuED3LvzlH3CHS3S.htm)|Sudden Throw|auto-trad|
|[UV3vQl5jrXwr1wQh.htm](abomination-vaults-bestiary-items/UV3vQl5jrXwr1wQh.htm)|Undulating Step|auto-trad|
|[uWu9iZkKF8LUjUoe.htm](abomination-vaults-bestiary-items/uWu9iZkKF8LUjUoe.htm)|+2 Status to All Saves vs. Disease and Poison|auto-trad|
|[UYMJ2nbr7f8EgOHc.htm](abomination-vaults-bestiary-items/UYMJ2nbr7f8EgOHc.htm)|Negative Recovery|auto-trad|
|[V0Zof9p6jaQUrOl0.htm](abomination-vaults-bestiary-items/V0Zof9p6jaQUrOl0.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[v1N2fY4anfmaOUr3.htm](abomination-vaults-bestiary-items/v1N2fY4anfmaOUr3.htm)|Jaws|auto-trad|
|[V5Ok21wotF3DQNBZ.htm](abomination-vaults-bestiary-items/V5Ok21wotF3DQNBZ.htm)|Someone is Watching|auto-trad|
|[VB81WcGgMngyyh3E.htm](abomination-vaults-bestiary-items/VB81WcGgMngyyh3E.htm)|Slink in Shadows|auto-trad|
|[vBp53lYo9D4gq1jh.htm](abomination-vaults-bestiary-items/vBp53lYo9D4gq1jh.htm)|All-Around Vision|auto-trad|
|[VDa8nmlZXUFf1Y7z.htm](abomination-vaults-bestiary-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|auto-trad|
|[vDdGrFDKu7JIcuaR.htm](abomination-vaults-bestiary-items/vDdGrFDKu7JIcuaR.htm)|Rituals|auto-trad|
|[vdPooFytAas2OzRw.htm](abomination-vaults-bestiary-items/vdPooFytAas2OzRw.htm)|Reloading Trick|auto-trad|
|[VgEW660oBhpCgGPZ.htm](abomination-vaults-bestiary-items/VgEW660oBhpCgGPZ.htm)|Claw|auto-trad|
|[VGPs2FUB6OC8igcY.htm](abomination-vaults-bestiary-items/VGPs2FUB6OC8igcY.htm)|Web|auto-trad|
|[vH2uvgGZ6JCaWrIl.htm](abomination-vaults-bestiary-items/vH2uvgGZ6JCaWrIl.htm)|Ghostly Hand|auto-trad|
|[VH3uvuMCquGbT6Wx.htm](abomination-vaults-bestiary-items/VH3uvuMCquGbT6Wx.htm)|Regeneration 10 (Deactivated by Good or Silver)|auto-trad|
|[vhcL0ctFDySpLekK.htm](abomination-vaults-bestiary-items/vhcL0ctFDySpLekK.htm)|Negative Healing|auto-trad|
|[VJFdV0JcVaaqVlyL.htm](abomination-vaults-bestiary-items/VJFdV0JcVaaqVlyL.htm)|Divine Innate Spells|auto-trad|
|[vKdhNyXTKHDMUfAc.htm](abomination-vaults-bestiary-items/vKdhNyXTKHDMUfAc.htm)|Flames of Fury|auto-trad|
|[VkmU3zCSF1tm61N6.htm](abomination-vaults-bestiary-items/VkmU3zCSF1tm61N6.htm)|Command Confusion|auto-trad|
|[VkXxpkRx6E7dyCI4.htm](abomination-vaults-bestiary-items/VkXxpkRx6E7dyCI4.htm)|Vermin Empathy|auto-trad|
|[vMlj7QxxkX61zODC.htm](abomination-vaults-bestiary-items/vMlj7QxxkX61zODC.htm)|Sneak Attack|auto-trad|
|[VNqQVxfzCgmliiPq.htm](abomination-vaults-bestiary-items/VNqQVxfzCgmliiPq.htm)|Tail|auto-trad|
|[Vo6AHVWsAnxqkwLW.htm](abomination-vaults-bestiary-items/Vo6AHVWsAnxqkwLW.htm)|Drover's Band|auto-trad|
|[vO90s2UOixYIPRSk.htm](abomination-vaults-bestiary-items/vO90s2UOixYIPRSk.htm)|Jaws|auto-trad|
|[VOrtrf3kSz1oIQkq.htm](abomination-vaults-bestiary-items/VOrtrf3kSz1oIQkq.htm)|Darkvision|auto-trad|
|[VPyIMfFo4j5NMPBG.htm](abomination-vaults-bestiary-items/VPyIMfFo4j5NMPBG.htm)|Dagger|auto-trad|
|[Vq09pyw5K2mb3YeT.htm](abomination-vaults-bestiary-items/Vq09pyw5K2mb3YeT.htm)|Longsword|auto-trad|
|[VqTDcQNmGBPYQ6d3.htm](abomination-vaults-bestiary-items/VqTDcQNmGBPYQ6d3.htm)|Greater Darkvision|auto-trad|
|[VQXGCkNvdRLDuyH1.htm](abomination-vaults-bestiary-items/VQXGCkNvdRLDuyH1.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Vr79KdOVhzih6EMo.htm](abomination-vaults-bestiary-items/Vr79KdOVhzih6EMo.htm)|Rapier|auto-trad|
|[vTyv5W3SkeNgrprr.htm](abomination-vaults-bestiary-items/vTyv5W3SkeNgrprr.htm)|Occult Innate Spells|auto-trad|
|[VUmbLmAZWhr6cx2b.htm](abomination-vaults-bestiary-items/VUmbLmAZWhr6cx2b.htm)|Longsword|auto-trad|
|[VwIv1y01lqATWPRT.htm](abomination-vaults-bestiary-items/VwIv1y01lqATWPRT.htm)|Necrotic Decay|auto-trad|
|[VWWosu7Wvs4yAKZy.htm](abomination-vaults-bestiary-items/VWWosu7Wvs4yAKZy.htm)|Grab|auto-trad|
|[VXZ8QpZ1EGC0hg4D.htm](abomination-vaults-bestiary-items/VXZ8QpZ1EGC0hg4D.htm)|Magic Immunity|auto-trad|
|[Vy2pPgrGJ3SguoOn.htm](abomination-vaults-bestiary-items/Vy2pPgrGJ3SguoOn.htm)|Shadow Hand|auto-trad|
|[VzobMtkoa82xICPm.htm](abomination-vaults-bestiary-items/VzobMtkoa82xICPm.htm)|Darkvision|auto-trad|
|[W0VQcpug1ZFVNZox.htm](abomination-vaults-bestiary-items/W0VQcpug1ZFVNZox.htm)|Occult Innate Spells|auto-trad|
|[W2nnShEx9f5pWTjq.htm](abomination-vaults-bestiary-items/W2nnShEx9f5pWTjq.htm)|Pewter Mug|auto-trad|
|[W4yMtkEVcpKOM2tE.htm](abomination-vaults-bestiary-items/W4yMtkEVcpKOM2tE.htm)|Broad Swipe|auto-trad|
|[W7NJfdI36Wt2pLfo.htm](abomination-vaults-bestiary-items/W7NJfdI36Wt2pLfo.htm)|Camouflaged Step|auto-trad|
|[w8TbxNNTVBH6j4jP.htm](abomination-vaults-bestiary-items/w8TbxNNTVBH6j4jP.htm)|Shootist's Draw|auto-trad|
|[WA3qxd5nl6en4BPF.htm](abomination-vaults-bestiary-items/WA3qxd5nl6en4BPF.htm)|Spore Cloud|auto-trad|
|[WAo4NMhw8o3YpQGq.htm](abomination-vaults-bestiary-items/WAo4NMhw8o3YpQGq.htm)|Wicked Bite|auto-trad|
|[WCcurB9iEOCcU58H.htm](abomination-vaults-bestiary-items/WCcurB9iEOCcU58H.htm)|Paralysis|auto-trad|
|[wChn2PEw9GYziq2n.htm](abomination-vaults-bestiary-items/wChn2PEw9GYziq2n.htm)|Glow|auto-trad|
|[wcJ7xyyDuDpmKxyt.htm](abomination-vaults-bestiary-items/wcJ7xyyDuDpmKxyt.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[WersQyXKosWIiXXl.htm](abomination-vaults-bestiary-items/WersQyXKosWIiXXl.htm)|Ghostly Hand|auto-trad|
|[wImzz2Ucpz9gqh2J.htm](abomination-vaults-bestiary-items/wImzz2Ucpz9gqh2J.htm)|Claw|auto-trad|
|[wjbGJGSRPWZ49qZd.htm](abomination-vaults-bestiary-items/wjbGJGSRPWZ49qZd.htm)|Spittle|auto-trad|
|[wKmUr6VixtVPXpXR.htm](abomination-vaults-bestiary-items/wKmUr6VixtVPXpXR.htm)|Tamchal Chakram|auto-trad|
|[WKywaKpRvPLFLVvG.htm](abomination-vaults-bestiary-items/WKywaKpRvPLFLVvG.htm)|Mosh|auto-trad|
|[wnV3crCWLBRnytqk.htm](abomination-vaults-bestiary-items/wnV3crCWLBRnytqk.htm)|Death Flame|auto-trad|
|[WsGIMLD86LgMNUWg.htm](abomination-vaults-bestiary-items/WsGIMLD86LgMNUWg.htm)|Claw|auto-trad|
|[WU1jExSnDYhncwTy.htm](abomination-vaults-bestiary-items/WU1jExSnDYhncwTy.htm)|Darkvision|auto-trad|
|[WVPQDcIaUdkMfawy.htm](abomination-vaults-bestiary-items/WVPQDcIaUdkMfawy.htm)|Rapier|auto-trad|
|[wWrvIQM5fOYBSMRQ.htm](abomination-vaults-bestiary-items/wWrvIQM5fOYBSMRQ.htm)|At-Will Spells|auto-trad|
|[wWufyBbAG3JXOfoE.htm](abomination-vaults-bestiary-items/wWufyBbAG3JXOfoE.htm)|Darkvision|auto-trad|
|[wyQSk60Np0UavJiY.htm](abomination-vaults-bestiary-items/wyQSk60Np0UavJiY.htm)|Stasis Field|auto-trad|
|[x13RVTp5gtE3XOTa.htm](abomination-vaults-bestiary-items/x13RVTp5gtE3XOTa.htm)|Defensive Shooter|auto-trad|
|[x2mDaJtGWiO1eS56.htm](abomination-vaults-bestiary-items/x2mDaJtGWiO1eS56.htm)|Counterfeit Haunting|auto-trad|
|[x3yCpybINerCQY3Q.htm](abomination-vaults-bestiary-items/x3yCpybINerCQY3Q.htm)|Corrosive Touch|auto-trad|
|[x97vWevUTEMqeOCJ.htm](abomination-vaults-bestiary-items/x97vWevUTEMqeOCJ.htm)|Painsight|auto-trad|
|[xA9OK9ybw4WLdQ8o.htm](abomination-vaults-bestiary-items/xA9OK9ybw4WLdQ8o.htm)|Darkvision|auto-trad|
|[xCCM8tyhLkWqYUpL.htm](abomination-vaults-bestiary-items/xCCM8tyhLkWqYUpL.htm)|Motion Sense 60 feet|auto-trad|
|[XCfMmPXvB7LJznlU.htm](abomination-vaults-bestiary-items/XCfMmPXvB7LJznlU.htm)|Frost Composite Longbow|auto-trad|
|[XETiXceuUZEG0el1.htm](abomination-vaults-bestiary-items/XETiXceuUZEG0el1.htm)|Rejuvenation|auto-trad|
|[XezgRZTc5EmXVys0.htm](abomination-vaults-bestiary-items/XezgRZTc5EmXVys0.htm)|Fist|auto-trad|
|[XfR063rnMuiow4bN.htm](abomination-vaults-bestiary-items/XfR063rnMuiow4bN.htm)|Opportune Step|auto-trad|
|[xH6NrBw4bYEHU7e3.htm](abomination-vaults-bestiary-items/xH6NrBw4bYEHU7e3.htm)|Sacrilegious Aura|auto-trad|
|[xiTvpYtL10U6KNkT.htm](abomination-vaults-bestiary-items/xiTvpYtL10U6KNkT.htm)|Darkvision|auto-trad|
|[XJ7A5MPGGNFZpxXP.htm](abomination-vaults-bestiary-items/XJ7A5MPGGNFZpxXP.htm)|Negative Healing|auto-trad|
|[XjvtH7IilCnzJpIN.htm](abomination-vaults-bestiary-items/XjvtH7IilCnzJpIN.htm)|Darkvision|auto-trad|
|[xk8dbRocKTe3GSWC.htm](abomination-vaults-bestiary-items/xk8dbRocKTe3GSWC.htm)|Spore Explosion|auto-trad|
|[XKiDDCClxzUDYSO8.htm](abomination-vaults-bestiary-items/XKiDDCClxzUDYSO8.htm)|Ghoul Fever|auto-trad|
|[XmKUjz23s0dNFOYW.htm](abomination-vaults-bestiary-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|auto-trad|
|[XNJnpNGQCkQD00v3.htm](abomination-vaults-bestiary-items/XNJnpNGQCkQD00v3.htm)|Rejuvenation|auto-trad|
|[xoFC8JZ1B0deUW2k.htm](abomination-vaults-bestiary-items/xoFC8JZ1B0deUW2k.htm)|Magic Item Mastery|auto-trad|
|[xpJLgdCTRRKsEqi3.htm](abomination-vaults-bestiary-items/xpJLgdCTRRKsEqi3.htm)|Magic Item Mastery|auto-trad|
|[XplLVQ3DKSYJCuYL.htm](abomination-vaults-bestiary-items/XplLVQ3DKSYJCuYL.htm)|At-Will Spells|auto-trad|
|[XrEqDkgs6eaCAzhM.htm](abomination-vaults-bestiary-items/XrEqDkgs6eaCAzhM.htm)|Wicked Bite|auto-trad|
|[XsfO4kZKUmGSNSI0.htm](abomination-vaults-bestiary-items/XsfO4kZKUmGSNSI0.htm)|+1 Status to All Saves vs. Poison|auto-trad|
|[xTCkHuwFUsvdVThS.htm](abomination-vaults-bestiary-items/xTCkHuwFUsvdVThS.htm)|Aquatic Ambush|auto-trad|
|[XvsfsnZoPvQNu6Be.htm](abomination-vaults-bestiary-items/XvsfsnZoPvQNu6Be.htm)|Negative Healing|auto-trad|
|[xVxy6iARZLipqA8h.htm](abomination-vaults-bestiary-items/xVxy6iARZLipqA8h.htm)|Command Confusion|auto-trad|
|[xWD7oLqr7RfHGQVH.htm](abomination-vaults-bestiary-items/xWD7oLqr7RfHGQVH.htm)|Bone Shard|auto-trad|
|[xxK8rXRDWK5Rwj9i.htm](abomination-vaults-bestiary-items/xxK8rXRDWK5Rwj9i.htm)|Divine Innate Spells|auto-trad|
|[xze5oFK48fMv2xBU.htm](abomination-vaults-bestiary-items/xze5oFK48fMv2xBU.htm)|Tentacle Transfer|auto-trad|
|[XzUUZ1FoJXIkVqZu.htm](abomination-vaults-bestiary-items/XzUUZ1FoJXIkVqZu.htm)|Uncanny Tinker|auto-trad|
|[Y0gOm1Uz3dMu6Q4W.htm](abomination-vaults-bestiary-items/Y0gOm1Uz3dMu6Q4W.htm)|Claim Corpse|auto-trad|
|[Y0zIOXUUeYuBTRst.htm](abomination-vaults-bestiary-items/Y0zIOXUUeYuBTRst.htm)|Occult Spontaneous Spells|auto-trad|
|[y1F32N9fSld9NydJ.htm](abomination-vaults-bestiary-items/y1F32N9fSld9NydJ.htm)|Negative Healing|auto-trad|
|[y4g9ZcRWSHKAxsUe.htm](abomination-vaults-bestiary-items/y4g9ZcRWSHKAxsUe.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[y4um9lgRgEVJYHeJ.htm](abomination-vaults-bestiary-items/y4um9lgRgEVJYHeJ.htm)|Paralysis|auto-trad|
|[Y6Rqq2ahFM07WKRm.htm](abomination-vaults-bestiary-items/Y6Rqq2ahFM07WKRm.htm)|Swarming Stance|auto-trad|
|[Y7vFj13afzJKpHws.htm](abomination-vaults-bestiary-items/Y7vFj13afzJKpHws.htm)|Pinning Chomp|auto-trad|
|[y87UDyuJmwKsI7Tq.htm](abomination-vaults-bestiary-items/y87UDyuJmwKsI7Tq.htm)|At-Will Spells|auto-trad|
|[y8rBowLYE52m6Vb7.htm](abomination-vaults-bestiary-items/y8rBowLYE52m6Vb7.htm)|Go Dark|auto-trad|
|[Y8ZISA3GaUO4MtHh.htm](abomination-vaults-bestiary-items/Y8ZISA3GaUO4MtHh.htm)|Jaws|auto-trad|
|[Y9Z7x3aVBCSbKMxB.htm](abomination-vaults-bestiary-items/Y9Z7x3aVBCSbKMxB.htm)|Grab|auto-trad|
|[yafzmxKBfx68zshb.htm](abomination-vaults-bestiary-items/yafzmxKBfx68zshb.htm)|Hand Crossbow|auto-trad|
|[yajcaYxlIfR8IeG6.htm](abomination-vaults-bestiary-items/yajcaYxlIfR8IeG6.htm)|Arcane Prepared Spells|auto-trad|
|[Ybx41yIdQO59hKjK.htm](abomination-vaults-bestiary-items/Ybx41yIdQO59hKjK.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[yCKSVMhk8eLrvD0h.htm](abomination-vaults-bestiary-items/yCKSVMhk8eLrvD0h.htm)|Darkvision|auto-trad|
|[yCvj5N1H04hmAYuS.htm](abomination-vaults-bestiary-items/yCvj5N1H04hmAYuS.htm)|Gas Release|auto-trad|
|[yD0yYDGEVE1rfNJJ.htm](abomination-vaults-bestiary-items/yD0yYDGEVE1rfNJJ.htm)|Ghoul Fever|auto-trad|
|[yDdnC4S3nrhfHYil.htm](abomination-vaults-bestiary-items/yDdnC4S3nrhfHYil.htm)|Heat Mirage|auto-trad|
|[YE5eTfCKX1ANEEP7.htm](abomination-vaults-bestiary-items/YE5eTfCKX1ANEEP7.htm)|Light Blindness|auto-trad|
|[YEnM0GJZ6D2RdXBg.htm](abomination-vaults-bestiary-items/YEnM0GJZ6D2RdXBg.htm)|Composite Longbow|auto-trad|
|[ygCGxbBzOI9gN0fC.htm](abomination-vaults-bestiary-items/ygCGxbBzOI9gN0fC.htm)|Knock It Away|auto-trad|
|[yI8fil9Hp8Ob0BcY.htm](abomination-vaults-bestiary-items/yI8fil9Hp8Ob0BcY.htm)|Occult Innate Spells|auto-trad|
|[yJdnNORoa1KtMM7W.htm](abomination-vaults-bestiary-items/yJdnNORoa1KtMM7W.htm)|Site Bound|auto-trad|
|[yJVTcQFJpy86gPDG.htm](abomination-vaults-bestiary-items/yJVTcQFJpy86gPDG.htm)|Claw|auto-trad|
|[YK1tWx8RaI3guzkW.htm](abomination-vaults-bestiary-items/YK1tWx8RaI3guzkW.htm)|Warhammer|auto-trad|
|[ykVZX3vwjXXl9WoS.htm](abomination-vaults-bestiary-items/ykVZX3vwjXXl9WoS.htm)|At-Will Spells|auto-trad|
|[YnER4NJf6wi05tio.htm](abomination-vaults-bestiary-items/YnER4NJf6wi05tio.htm)|Claw|auto-trad|
|[yNjPKC3nPRiaD957.htm](abomination-vaults-bestiary-items/yNjPKC3nPRiaD957.htm)|Dicing Scythes|auto-trad|
|[yp7FDjO7715BHNPw.htm](abomination-vaults-bestiary-items/yp7FDjO7715BHNPw.htm)|Darkvision|auto-trad|
|[YsiUbsAWRxZMjaWA.htm](abomination-vaults-bestiary-items/YsiUbsAWRxZMjaWA.htm)|Ghoul Fever|auto-trad|
|[yTXiHzmRavI1BmKJ.htm](abomination-vaults-bestiary-items/yTXiHzmRavI1BmKJ.htm)|Rend|auto-trad|
|[YuDlX3Kr7cepFAW4.htm](abomination-vaults-bestiary-items/YuDlX3Kr7cepFAW4.htm)|Feed on Despair|auto-trad|
|[YUgXFNB4xn08S3Cf.htm](abomination-vaults-bestiary-items/YUgXFNB4xn08S3Cf.htm)|Club|auto-trad|
|[yUMwDlPA7DiVRv5u.htm](abomination-vaults-bestiary-items/yUMwDlPA7DiVRv5u.htm)|Negative Healing|auto-trad|
|[YuXrcTfctRYHmw90.htm](abomination-vaults-bestiary-items/YuXrcTfctRYHmw90.htm)|Glow|auto-trad|
|[yvKEo91RnhVFPZ5L.htm](abomination-vaults-bestiary-items/yvKEo91RnhVFPZ5L.htm)|Negative Healing|auto-trad|
|[YvmZQz32k7hFcqzM.htm](abomination-vaults-bestiary-items/YvmZQz32k7hFcqzM.htm)|Darkvision|auto-trad|
|[YwUDfdiKYGDgdz8Q.htm](abomination-vaults-bestiary-items/YwUDfdiKYGDgdz8Q.htm)|Attack of Opportunity|auto-trad|
|[yxXEQaBGVF13c0rW.htm](abomination-vaults-bestiary-items/yxXEQaBGVF13c0rW.htm)|Dagger|auto-trad|
|[z2quk2nw5Lv8ueRG.htm](abomination-vaults-bestiary-items/z2quk2nw5Lv8ueRG.htm)|Rituals|auto-trad|
|[Z60YSknQUrMbR4mB.htm](abomination-vaults-bestiary-items/Z60YSknQUrMbR4mB.htm)|Divine Innate Spells|auto-trad|
|[z6U85IjlJZGBsdcA.htm](abomination-vaults-bestiary-items/z6U85IjlJZGBsdcA.htm)|Occult Innate Spells|auto-trad|
|[z9bTah65DoASEDhP.htm](abomination-vaults-bestiary-items/z9bTah65DoASEDhP.htm)|Shortsword|auto-trad|
|[za8ix1WE9HbNfHvg.htm](abomination-vaults-bestiary-items/za8ix1WE9HbNfHvg.htm)|Grab|auto-trad|
|[zadQdUq4eeiDSHSq.htm](abomination-vaults-bestiary-items/zadQdUq4eeiDSHSq.htm)|Shadow Spawn|auto-trad|
|[zALaGtcb55Te1IcS.htm](abomination-vaults-bestiary-items/zALaGtcb55Te1IcS.htm)|Darkvision|auto-trad|
|[Zb3D43B4UbusNqAe.htm](abomination-vaults-bestiary-items/Zb3D43B4UbusNqAe.htm)|Light Blindness|auto-trad|
|[zb4lD1qa1tSyLe20.htm](abomination-vaults-bestiary-items/zb4lD1qa1tSyLe20.htm)|Claw|auto-trad|
|[ZCynz5sJ6bYK6fY6.htm](abomination-vaults-bestiary-items/ZCynz5sJ6bYK6fY6.htm)|Low-Light Vision|auto-trad|
|[ZfdrndtEZTKXbrq0.htm](abomination-vaults-bestiary-items/ZfdrndtEZTKXbrq0.htm)|Red Ruin Stance|auto-trad|
|[zGg5a7ALqF7zVFCa.htm](abomination-vaults-bestiary-items/zGg5a7ALqF7zVFCa.htm)|Ectoplasmic Web|auto-trad|
|[zGXZKhk95VPsSdKN.htm](abomination-vaults-bestiary-items/zGXZKhk95VPsSdKN.htm)|Biting Snakes|auto-trad|
|[zjVbARImBBAx6EKv.htm](abomination-vaults-bestiary-items/zjVbARImBBAx6EKv.htm)|Vile Touch|auto-trad|
|[ZkF8meq1sB3uUIcI.htm](abomination-vaults-bestiary-items/ZkF8meq1sB3uUIcI.htm)|Darkvision|auto-trad|
|[zKQUDZzcDDJHuOiB.htm](abomination-vaults-bestiary-items/zKQUDZzcDDJHuOiB.htm)|Paralysis|auto-trad|
|[zm8POWGccsjyfuPN.htm](abomination-vaults-bestiary-items/zm8POWGccsjyfuPN.htm)|Bouncing Crush|auto-trad|
|[ZnFe1AJx67Diyerp.htm](abomination-vaults-bestiary-items/ZnFe1AJx67Diyerp.htm)|Occult Innate Spells|auto-trad|
|[zNFSrrQNB1kgV8Go.htm](abomination-vaults-bestiary-items/zNFSrrQNB1kgV8Go.htm)|Stay in the Fight|auto-trad|
|[ZqBQutg0Cvs5YcxR.htm](abomination-vaults-bestiary-items/ZqBQutg0Cvs5YcxR.htm)|Negative Healing|auto-trad|
|[zQUhdB7kvudfGv5Z.htm](abomination-vaults-bestiary-items/zQUhdB7kvudfGv5Z.htm)|Reposition|auto-trad|
|[ZrjdCvEH7E5tQb4n.htm](abomination-vaults-bestiary-items/ZrjdCvEH7E5tQb4n.htm)|Frightful Presence|auto-trad|
|[zSR3MB9KNpQPcxA0.htm](abomination-vaults-bestiary-items/zSR3MB9KNpQPcxA0.htm)|Drumstick|auto-trad|
|[ZtEncTg58fTcUXvv.htm](abomination-vaults-bestiary-items/ZtEncTg58fTcUXvv.htm)|Apocalypse Beam|auto-trad|
|[ZU5jisN3KgoM2kPq.htm](abomination-vaults-bestiary-items/ZU5jisN3KgoM2kPq.htm)|Darkvision|auto-trad|
|[zuOWrPyeR35yI1r7.htm](abomination-vaults-bestiary-items/zuOWrPyeR35yI1r7.htm)|Leg Quill|auto-trad|
|[ZVpl1dcKnK3GldQS.htm](abomination-vaults-bestiary-items/ZVpl1dcKnK3GldQS.htm)|Trident|auto-trad|
|[zWjx9xYQFt0WhRW5.htm](abomination-vaults-bestiary-items/zWjx9xYQFt0WhRW5.htm)|Divine Innate Spells|auto-trad|
|[zXmPJ4IvqhUl8bad.htm](abomination-vaults-bestiary-items/zXmPJ4IvqhUl8bad.htm)|Grab|auto-trad|
|[zyawAgjl2eHH085S.htm](abomination-vaults-bestiary-items/zyawAgjl2eHH085S.htm)|Drumstick|auto-trad|
|[zYMKuVB2VBXOtrfe.htm](abomination-vaults-bestiary-items/zYMKuVB2VBXOtrfe.htm)|Divine Innate Spells|auto-trad|
|[zzmMQYbPGJRgOH7E.htm](abomination-vaults-bestiary-items/zzmMQYbPGJRgOH7E.htm)|Ghostly Assault|auto-trad|
|[zZQXw3WG7NLAQrux.htm](abomination-vaults-bestiary-items/zZQXw3WG7NLAQrux.htm)|Swift Leap|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[2l5Df8352WwEuAXJ.htm](abomination-vaults-bestiary-items/2l5Df8352WwEuAXJ.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[6uvSpdy8ezlFUIIo.htm](abomination-vaults-bestiary-items/6uvSpdy8ezlFUIIo.htm)|Shred Flesh|Destrozar Carne|modificada|
|[CncVGQus926xFSl2.htm](abomination-vaults-bestiary-items/CncVGQus926xFSl2.htm)|Shauth Bite|Muerdemuerde|modificada|
|[DIqlApNIdzR6Eh6H.htm](abomination-vaults-bestiary-items/DIqlApNIdzR6Eh6H.htm)|Hunter's Wound|Herida del cazador|modificada|
|[owQaoWxiXKdJdh64.htm](abomination-vaults-bestiary-items/owQaoWxiXKdJdh64.htm)|Infernal Wound|Herida infernal|modificada|
|[QaDq4G70qrufNlS5.htm](abomination-vaults-bestiary-items/QaDq4G70qrufNlS5.htm)|Sapping Squeeze|Estrujamiento agotador|modificada|
|[taKy6ih4jlHzjBsw.htm](abomination-vaults-bestiary-items/taKy6ih4jlHzjBsw.htm)|Sapping Squeeze|Estrujamiento agotador|modificada|
|[tce3ovXnnblqwD77.htm](abomination-vaults-bestiary-items/tce3ovXnnblqwD77.htm)|Shred Flesh|Destrozar Carne|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
