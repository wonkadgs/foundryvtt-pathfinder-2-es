# Estado de la traducción (pathfinder-bestiary-2)

 * **auto-trad**: 371
 * **modificada**: 2


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0rhltfBkQtYkCwXd.htm](pathfinder-bestiary-2/0rhltfBkQtYkCwXd.htm)|Yellow Musk Thrall|auto-trad|
|[10JbkieWjqKA5Hzc.htm](pathfinder-bestiary-2/10JbkieWjqKA5Hzc.htm)|Irlgaunt|auto-trad|
|[12vyw160K7p3M3Mt.htm](pathfinder-bestiary-2/12vyw160K7p3M3Mt.htm)|Quetz Couatl|auto-trad|
|[18NInoyRLIrWE0nt.htm](pathfinder-bestiary-2/18NInoyRLIrWE0nt.htm)|Chernobue|auto-trad|
|[1QWZDaBoefL5rWkz.htm](pathfinder-bestiary-2/1QWZDaBoefL5rWkz.htm)|Hodag|auto-trad|
|[27R4ntwgch7eIQM6.htm](pathfinder-bestiary-2/27R4ntwgch7eIQM6.htm)|Akizendri|auto-trad|
|[2Kw49I6EZbKKlTtK.htm](pathfinder-bestiary-2/2Kw49I6EZbKKlTtK.htm)|Blizzardborn|auto-trad|
|[32HHvah2Ji6FjJnM.htm](pathfinder-bestiary-2/32HHvah2Ji6FjJnM.htm)|Giant Squid|auto-trad|
|[340AwQpRXGblw1kF.htm](pathfinder-bestiary-2/340AwQpRXGblw1kF.htm)|Sarglagon|auto-trad|
|[37FAi1y5S8snofws.htm](pathfinder-bestiary-2/37FAi1y5S8snofws.htm)|Olethrodaemon|auto-trad|
|[3dlupMwDA9qv1xWS.htm](pathfinder-bestiary-2/3dlupMwDA9qv1xWS.htm)|Suli Dune Dancer|auto-trad|
|[3DSqfobadad3pA6O.htm](pathfinder-bestiary-2/3DSqfobadad3pA6O.htm)|Giant Fly|auto-trad|
|[3MDlKPlPhHBo1DId.htm](pathfinder-bestiary-2/3MDlKPlPhHBo1DId.htm)|Vampire Squid|auto-trad|
|[3u4toki9EeSRvcs2.htm](pathfinder-bestiary-2/3u4toki9EeSRvcs2.htm)|Quoppopak|auto-trad|
|[3VFHZyjSjNA1jbiO.htm](pathfinder-bestiary-2/3VFHZyjSjNA1jbiO.htm)|Sportlebore Swarm|auto-trad|
|[3YcsuATyahEMygNy.htm](pathfinder-bestiary-2/3YcsuATyahEMygNy.htm)|Revenant|auto-trad|
|[48agy7vl9fQW1LLA.htm](pathfinder-bestiary-2/48agy7vl9fQW1LLA.htm)|Draugr|auto-trad|
|[49y0g521xHESClzk.htm](pathfinder-bestiary-2/49y0g521xHESClzk.htm)|Young Magma Dragon|auto-trad|
|[4x9trDSABU5dqq9w.htm](pathfinder-bestiary-2/4x9trDSABU5dqq9w.htm)|Blue Dracolisk|auto-trad|
|[4Y8UO8NloCoPxJ1E.htm](pathfinder-bestiary-2/4Y8UO8NloCoPxJ1E.htm)|Umonlee|auto-trad|
|[57pzXwM9qFiRTwq9.htm](pathfinder-bestiary-2/57pzXwM9qFiRTwq9.htm)|Dig-Widget|auto-trad|
|[5CixyyBkCLj2VI8q.htm](pathfinder-bestiary-2/5CixyyBkCLj2VI8q.htm)|Hippopotamus|auto-trad|
|[5FcYbQxHehjO4w1d.htm](pathfinder-bestiary-2/5FcYbQxHehjO4w1d.htm)|Ooze Mephit|auto-trad|
|[5FGuXfg6Ub2hCBG7.htm](pathfinder-bestiary-2/5FGuXfg6Ub2hCBG7.htm)|Yellow Musk Creeper|auto-trad|
|[5l0b80kz53Ie26gq.htm](pathfinder-bestiary-2/5l0b80kz53Ie26gq.htm)|Wood Giant|auto-trad|
|[5l3surWoPHTgPoWY.htm](pathfinder-bestiary-2/5l3surWoPHTgPoWY.htm)|Petitioner (Abaddon)|auto-trad|
|[5QmDXmgNGBmEhQd9.htm](pathfinder-bestiary-2/5QmDXmgNGBmEhQd9.htm)|Adult Crystal Dragon (Spellcaster)|auto-trad|
|[64sdDzjLWu2W2R2e.htm](pathfinder-bestiary-2/64sdDzjLWu2W2R2e.htm)|Water Orm|auto-trad|
|[6EX8nxH3zRotLRJG.htm](pathfinder-bestiary-2/6EX8nxH3zRotLRJG.htm)|Sea Snake|auto-trad|
|[6FltuGxvUoNH9b17.htm](pathfinder-bestiary-2/6FltuGxvUoNH9b17.htm)|Petitioner (Plane of Air)|auto-trad|
|[6ic6oNCpWjkVAsIN.htm](pathfinder-bestiary-2/6ic6oNCpWjkVAsIN.htm)|Ember Fox|auto-trad|
|[6jqnHzjmJ190G8Zj.htm](pathfinder-bestiary-2/6jqnHzjmJ190G8Zj.htm)|Yeth Hound|auto-trad|
|[6Me4Q1JDupyCCnGg.htm](pathfinder-bestiary-2/6Me4Q1JDupyCCnGg.htm)|Ice Mephit|auto-trad|
|[6tKVMKDdvbp0VhZD.htm](pathfinder-bestiary-2/6tKVMKDdvbp0VhZD.htm)|Slime Mold|auto-trad|
|[6wpHJXze0RLxl780.htm](pathfinder-bestiary-2/6wpHJXze0RLxl780.htm)|Behir|auto-trad|
|[6xioRweZ0T4o4X9e.htm](pathfinder-bestiary-2/6xioRweZ0T4o4X9e.htm)|Witchfire|auto-trad|
|[6YOyTHRGqkYl2Prb.htm](pathfinder-bestiary-2/6YOyTHRGqkYl2Prb.htm)|Wolverine|auto-trad|
|[6ZLs4H4gjVyb3yV7.htm](pathfinder-bestiary-2/6ZLs4H4gjVyb3yV7.htm)|Petitioner (Elysium)|auto-trad|
|[73qJxJK8ES4Tbp2k.htm](pathfinder-bestiary-2/73qJxJK8ES4Tbp2k.htm)|Lurker In Light|auto-trad|
|[7EppoGdIcmd0AV9B.htm](pathfinder-bestiary-2/7EppoGdIcmd0AV9B.htm)|Onidoshi|auto-trad|
|[7lL7APeHQgkVxU1B.htm](pathfinder-bestiary-2/7lL7APeHQgkVxU1B.htm)|Fire Jellyfish Swarm|auto-trad|
|[7oiIDnLBDpC2r2AA.htm](pathfinder-bestiary-2/7oiIDnLBDpC2r2AA.htm)|Sacristan|auto-trad|
|[7SVhYtnBn967Hy8O.htm](pathfinder-bestiary-2/7SVhYtnBn967Hy8O.htm)|Bone Prophet|auto-trad|
|[7UBCht1tQfeba2Ih.htm](pathfinder-bestiary-2/7UBCht1tQfeba2Ih.htm)|Wood Golem|auto-trad|
|[89isDKkB1UCbsSvU.htm](pathfinder-bestiary-2/89isDKkB1UCbsSvU.htm)|Ostiarius|auto-trad|
|[8a9N2iPA5HLpkWaF.htm](pathfinder-bestiary-2/8a9N2iPA5HLpkWaF.htm)|Army Ant Swarm|auto-trad|
|[8HJNH3aDTJyJi3pU.htm](pathfinder-bestiary-2/8HJNH3aDTJyJi3pU.htm)|Cockroach Swarm|auto-trad|
|[8IyfadYYpYXBvlXN.htm](pathfinder-bestiary-2/8IyfadYYpYXBvlXN.htm)|Vrykolakas Spawn|auto-trad|
|[8MQXM56Rm0rmzaxY.htm](pathfinder-bestiary-2/8MQXM56Rm0rmzaxY.htm)|Ancient Cloud Dragon (Spellcaster)|auto-trad|
|[8usfZlFqdD9cchPJ.htm](pathfinder-bestiary-2/8usfZlFqdD9cchPJ.htm)|Bottlenose Dolphin|auto-trad|
|[8WAc7n2M4Ks00yvj.htm](pathfinder-bestiary-2/8WAc7n2M4Ks00yvj.htm)|Cavern Troll|auto-trad|
|[9sUx2sV4E7Sy3vJT.htm](pathfinder-bestiary-2/9sUx2sV4E7Sy3vJT.htm)|Zelekhut|auto-trad|
|[a6TY4fgTz5MbpkRR.htm](pathfinder-bestiary-2/a6TY4fgTz5MbpkRR.htm)|Young Magma Dragon (Spellcaster)|auto-trad|
|[a8Ih1RIr4UUV4QCL.htm](pathfinder-bestiary-2/a8Ih1RIr4UUV4QCL.htm)|Fire Yai|auto-trad|
|[Aa652cDdKTani3DY.htm](pathfinder-bestiary-2/Aa652cDdKTani3DY.htm)|Scythe Tree|auto-trad|
|[aATQbGnSA0KOWG53.htm](pathfinder-bestiary-2/aATQbGnSA0KOWG53.htm)|Anancus|auto-trad|
|[ABMn623izSLHA0sx.htm](pathfinder-bestiary-2/ABMn623izSLHA0sx.htm)|Glass Golem|auto-trad|
|[ABrzFoOqQohQqU6C.htm](pathfinder-bestiary-2/ABrzFoOqQohQqU6C.htm)|Yamaraj|auto-trad|
|[ACIqdW6VsFA1TCpN.htm](pathfinder-bestiary-2/ACIqdW6VsFA1TCpN.htm)|Manta Ray|auto-trad|
|[aFRGgQWWCDIiBlUl.htm](pathfinder-bestiary-2/aFRGgQWWCDIiBlUl.htm)|D'ziriak|auto-trad|
|[aMIb3Xm64NzRj8YP.htm](pathfinder-bestiary-2/aMIb3Xm64NzRj8YP.htm)|Mandragora|auto-trad|
|[ARw4pyNmuQxoZDke.htm](pathfinder-bestiary-2/ARw4pyNmuQxoZDke.htm)|Steam Mephit|auto-trad|
|[aVf2IojxU9ZZ64M5.htm](pathfinder-bestiary-2/aVf2IojxU9ZZ64M5.htm)|Fetchling Scout|auto-trad|
|[b0jMGY65XVyuKUjf.htm](pathfinder-bestiary-2/b0jMGY65XVyuKUjf.htm)|Blink Dog|auto-trad|
|[B4TJHP0yX8SXHQ4x.htm](pathfinder-bestiary-2/B4TJHP0yX8SXHQ4x.htm)|Trumpet Archon|auto-trad|
|[b5b6J3TqbcXnmKyy.htm](pathfinder-bestiary-2/b5b6J3TqbcXnmKyy.htm)|Giant Dragonfly|auto-trad|
|[B8dOh27iarR7rZI3.htm](pathfinder-bestiary-2/B8dOh27iarR7rZI3.htm)|Blindheim|auto-trad|
|[bAaVfrdNLfFh7iHv.htm](pathfinder-bestiary-2/bAaVfrdNLfFh7iHv.htm)|Basidirond|auto-trad|
|[BbIyNJDPkGKLK0Rt.htm](pathfinder-bestiary-2/BbIyNJDPkGKLK0Rt.htm)|Vexgit|auto-trad|
|[BE5GH4dwZ76qV7rH.htm](pathfinder-bestiary-2/BE5GH4dwZ76qV7rH.htm)|Petitioner (Maelstrom)|auto-trad|
|[BJoM8LAVP8MA5o8v.htm](pathfinder-bestiary-2/BJoM8LAVP8MA5o8v.htm)|Catoblepas|auto-trad|
|[BNizhfieKEditLGb.htm](pathfinder-bestiary-2/BNizhfieKEditLGb.htm)|Petitioner (Plane of Fire)|auto-trad|
|[bpwaqRoO1wYo15T5.htm](pathfinder-bestiary-2/bpwaqRoO1wYo15T5.htm)|Nuglub|auto-trad|
|[BSlvgDdLkTXU9hRF.htm](pathfinder-bestiary-2/BSlvgDdLkTXU9hRF.htm)|Ancient Umbral Dragon|auto-trad|
|[BTQ2ARKulg5Egueg.htm](pathfinder-bestiary-2/BTQ2ARKulg5Egueg.htm)|Augur|auto-trad|
|[Bz5wgKEVsHAyaYEx.htm](pathfinder-bestiary-2/Bz5wgKEVsHAyaYEx.htm)|Sunflower Leshy|auto-trad|
|[c4uQKZaRWv9n9IHo.htm](pathfinder-bestiary-2/c4uQKZaRWv9n9IHo.htm)|Solar|auto-trad|
|[C7LvY2Xxzm2xRL1M.htm](pathfinder-bestiary-2/C7LvY2Xxzm2xRL1M.htm)|Giant Solifugid|auto-trad|
|[cd2IaNGpdmHHZnrN.htm](pathfinder-bestiary-2/cd2IaNGpdmHHZnrN.htm)|Xill|auto-trad|
|[cijMuXBk8YUw2RoB.htm](pathfinder-bestiary-2/cijMuXBk8YUw2RoB.htm)|Caligni Slayer|auto-trad|
|[cKu0rrZtEAFCosej.htm](pathfinder-bestiary-2/cKu0rrZtEAFCosej.htm)|Void Zombie|auto-trad|
|[cmuCFkXdn1JuVTK4.htm](pathfinder-bestiary-2/cmuCFkXdn1JuVTK4.htm)|Gorgon|auto-trad|
|[coNTMrHerjBIf2ig.htm](pathfinder-bestiary-2/coNTMrHerjBIf2ig.htm)|Culdewen|auto-trad|
|[cPX0NmB242FqSRxa.htm](pathfinder-bestiary-2/cPX0NmB242FqSRxa.htm)|Grendel|auto-trad|
|[CrAsEzLl3F2vejd3.htm](pathfinder-bestiary-2/CrAsEzLl3F2vejd3.htm)|Earthen Destrier|auto-trad|
|[cu6Bl5pFByrpinxd.htm](pathfinder-bestiary-2/cu6Bl5pFByrpinxd.htm)|Planetar|auto-trad|
|[CvO6MAoxDFO0J5qm.htm](pathfinder-bestiary-2/CvO6MAoxDFO0J5qm.htm)|Witchwyrd|auto-trad|
|[CwrVQsRAeqlr1Vh0.htm](pathfinder-bestiary-2/CwrVQsRAeqlr1Vh0.htm)|Athach|auto-trad|
|[dBUiB3Hyh0hN8Exc.htm](pathfinder-bestiary-2/dBUiB3Hyh0hN8Exc.htm)|Woolly Rhinoceros|auto-trad|
|[DC9b8mG1KdyXZkLp.htm](pathfinder-bestiary-2/DC9b8mG1KdyXZkLp.htm)|Calathgar|auto-trad|
|[DEo5AEY1i9s0kWYZ.htm](pathfinder-bestiary-2/DEo5AEY1i9s0kWYZ.htm)|Lunar Naga|auto-trad|
|[DM5js1XSiuiPAf3t.htm](pathfinder-bestiary-2/DM5js1XSiuiPAf3t.htm)|White Dracolisk|auto-trad|
|[drtCZDDoESPSUqH7.htm](pathfinder-bestiary-2/drtCZDDoESPSUqH7.htm)|Peluda|auto-trad|
|[duaHanGHvRGhUtB5.htm](pathfinder-bestiary-2/duaHanGHvRGhUtB5.htm)|Viper Vine|auto-trad|
|[dvZM7hGNDu8olV7A.htm](pathfinder-bestiary-2/dvZM7hGNDu8olV7A.htm)|Brood Leech Swarm|auto-trad|
|[DWEk6i42qAhckgqF.htm](pathfinder-bestiary-2/DWEk6i42qAhckgqF.htm)|Ancient Crystal Dragon (Spellcaster)|auto-trad|
|[DWQzgPL1WjXfu4rV.htm](pathfinder-bestiary-2/DWQzgPL1WjXfu4rV.htm)|Assassin Vine|auto-trad|
|[dwyt7e1EZPjw9KBB.htm](pathfinder-bestiary-2/dwyt7e1EZPjw9KBB.htm)|Giant Hippocampus|auto-trad|
|[DxV2vH17H789bJ6y.htm](pathfinder-bestiary-2/DxV2vH17H789bJ6y.htm)|Ankou|auto-trad|
|[DYBZniwR1Ip4inVr.htm](pathfinder-bestiary-2/DYBZniwR1Ip4inVr.htm)|Morlock|auto-trad|
|[e0PXAtnDnJo4ggFX.htm](pathfinder-bestiary-2/e0PXAtnDnJo4ggFX.htm)|Vrykolakas Ancient|auto-trad|
|[e0YvEWJ5zpU946G4.htm](pathfinder-bestiary-2/e0YvEWJ5zpU946G4.htm)|Cave Scorpion|auto-trad|
|[E4aJhHWyFDxgJcy9.htm](pathfinder-bestiary-2/E4aJhHWyFDxgJcy9.htm)|Peryton|auto-trad|
|[e5yE4RkwLQojRYHm.htm](pathfinder-bestiary-2/e5yE4RkwLQojRYHm.htm)|Urdefhan Warrior|auto-trad|
|[eD1kydftMIp4CL2K.htm](pathfinder-bestiary-2/eD1kydftMIp4CL2K.htm)|Sard|auto-trad|
|[EKbnknpDZEMkZSF4.htm](pathfinder-bestiary-2/EKbnknpDZEMkZSF4.htm)|Petitioner (Positive Energy Plane)|auto-trad|
|[elb0mL1cfknHxskr.htm](pathfinder-bestiary-2/elb0mL1cfknHxskr.htm)|Petitioner (Nirvana)|auto-trad|
|[EOiOqs1vHCxZAj3T.htm](pathfinder-bestiary-2/EOiOqs1vHCxZAj3T.htm)|Scarecrow|auto-trad|
|[ePUTKSr45H7S5dQs.htm](pathfinder-bestiary-2/ePUTKSr45H7S5dQs.htm)|Veranallia|auto-trad|
|[Et2784TVZOXIbtM1.htm](pathfinder-bestiary-2/Et2784TVZOXIbtM1.htm)|Interlocutor|auto-trad|
|[EWoJAp5LJeL1Qfye.htm](pathfinder-bestiary-2/EWoJAp5LJeL1Qfye.htm)|Giant Frog|auto-trad|
|[EXeR2HC4xsvHcf6K.htm](pathfinder-bestiary-2/EXeR2HC4xsvHcf6K.htm)|Monadic Deva|auto-trad|
|[F5QibbbxUeO8A3Kj.htm](pathfinder-bestiary-2/F5QibbbxUeO8A3Kj.htm)|Grimstalker|auto-trad|
|[FERUCyMwvy4Nejou.htm](pathfinder-bestiary-2/FERUCyMwvy4Nejou.htm)|Hellcat|auto-trad|
|[FHnWzK7DsniqZWzG.htm](pathfinder-bestiary-2/FHnWzK7DsniqZWzG.htm)|Nereid|auto-trad|
|[fizcFuAe2WPauA2U.htm](pathfinder-bestiary-2/fizcFuAe2WPauA2U.htm)|Petitioner (Boneyard)|auto-trad|
|[FJr7Rc9MQEwxjAcz.htm](pathfinder-bestiary-2/FJr7Rc9MQEwxjAcz.htm)|Urdefhan Tormentor|auto-trad|
|[FMU6L5vbiUxa1ZF7.htm](pathfinder-bestiary-2/FMU6L5vbiUxa1ZF7.htm)|Kelpie|auto-trad|
|[FnLuqlzLl4zCE9pJ.htm](pathfinder-bestiary-2/FnLuqlzLl4zCE9pJ.htm)|Adult Magma Dragon|auto-trad|
|[fpBJiCOoHDlefgpO.htm](pathfinder-bestiary-2/fpBJiCOoHDlefgpO.htm)|Thunderbird|auto-trad|
|[fRLrlY25qXkOMBNG.htm](pathfinder-bestiary-2/fRLrlY25qXkOMBNG.htm)|Nuckelavee|auto-trad|
|[FRTpT9rLxUBKGdin.htm](pathfinder-bestiary-2/FRTpT9rLxUBKGdin.htm)|Star Archon|auto-trad|
|[fv91xoQJlogVbruW.htm](pathfinder-bestiary-2/fv91xoQJlogVbruW.htm)|Aapoph Serpentfolk|auto-trad|
|[fVvRASaKa6SkQkYG.htm](pathfinder-bestiary-2/fVvRASaKa6SkQkYG.htm)|Giant Snapping Turtle|auto-trad|
|[fwjDIGzmuEJN81Qq.htm](pathfinder-bestiary-2/fwjDIGzmuEJN81Qq.htm)|Trollhound|auto-trad|
|[g1htYgARxNKxc7MV.htm](pathfinder-bestiary-2/g1htYgARxNKxc7MV.htm)|Giant Slug|auto-trad|
|[g41X4b3q2iDMLRt3.htm](pathfinder-bestiary-2/g41X4b3q2iDMLRt3.htm)|Ravener|auto-trad|
|[GBBVBEAsznP8A5gv.htm](pathfinder-bestiary-2/GBBVBEAsznP8A5gv.htm)|Leydroth|auto-trad|
|[Ge5Q5I7TTksf7QyN.htm](pathfinder-bestiary-2/Ge5Q5I7TTksf7QyN.htm)|Zomok|auto-trad|
|[GfJLotTcrZmn6tay.htm](pathfinder-bestiary-2/GfJLotTcrZmn6tay.htm)|Purrodaemon|auto-trad|
|[GnKay44MFMZkqXRi.htm](pathfinder-bestiary-2/GnKay44MFMZkqXRi.htm)|Hadrosaurid|auto-trad|
|[gOjrie9YndQPomU8.htm](pathfinder-bestiary-2/gOjrie9YndQPomU8.htm)|Sand Sentry|auto-trad|
|[GsrqCDf0Dc9PisEt.htm](pathfinder-bestiary-2/GsrqCDf0Dc9PisEt.htm)|Violet Fungus|auto-trad|
|[GvjCh1Q6NEXnBHcD.htm](pathfinder-bestiary-2/GvjCh1Q6NEXnBHcD.htm)|Green Dracolisk|auto-trad|
|[gVNKimslRe1k2hdV.htm](pathfinder-bestiary-2/gVNKimslRe1k2hdV.htm)|Vaspercham|auto-trad|
|[H37tfTiLiUhpYxaS.htm](pathfinder-bestiary-2/H37tfTiLiUhpYxaS.htm)|Bastion Archon|auto-trad|
|[h4SZ3U9at7aT0ASv.htm](pathfinder-bestiary-2/h4SZ3U9at7aT0ASv.htm)|Spirit Naga|auto-trad|
|[H8KNSMb9uo2mJF3P.htm](pathfinder-bestiary-2/H8KNSMb9uo2mJF3P.htm)|Twigjack|auto-trad|
|[HBkk9MYLomjKDKr4.htm](pathfinder-bestiary-2/HBkk9MYLomjKDKr4.htm)|Azuretzi|auto-trad|
|[hFbfXmjMkGEzMkQ1.htm](pathfinder-bestiary-2/hFbfXmjMkGEzMkQ1.htm)|Raven|auto-trad|
|[HhSoTr6mpwV09Y4k.htm](pathfinder-bestiary-2/HhSoTr6mpwV09Y4k.htm)|Irnakurse|auto-trad|
|[HI7SecILsRTmOtky.htm](pathfinder-bestiary-2/HI7SecILsRTmOtky.htm)|Shocker Lizard|auto-trad|
|[hIWzgjW2E2K6ImPA.htm](pathfinder-bestiary-2/hIWzgjW2E2K6ImPA.htm)|Bog Mummy|auto-trad|
|[HjmqeJuWdmY5imiz.htm](pathfinder-bestiary-2/HjmqeJuWdmY5imiz.htm)|Triton|auto-trad|
|[HmeEHIXL46rBQWAa.htm](pathfinder-bestiary-2/HmeEHIXL46rBQWAa.htm)|Frost Troll|auto-trad|
|[hmnNDCNV425MisdA.htm](pathfinder-bestiary-2/hmnNDCNV425MisdA.htm)|Eremite|auto-trad|
|[HNEUSYnInffvGPHK.htm](pathfinder-bestiary-2/HNEUSYnInffvGPHK.htm)|Specter|auto-trad|
|[Ho1Dx2dXlQZYLApP.htm](pathfinder-bestiary-2/Ho1Dx2dXlQZYLApP.htm)|Sylph Sneak|auto-trad|
|[hvKRgNiQtQJhHk9u.htm](pathfinder-bestiary-2/hvKRgNiQtQJhHk9u.htm)|Imentesh|auto-trad|
|[hvoIxruNUei1auQT.htm](pathfinder-bestiary-2/hvoIxruNUei1auQT.htm)|Melody On The Wind|auto-trad|
|[Hwy4YrNVVr7NnsMC.htm](pathfinder-bestiary-2/Hwy4YrNVVr7NnsMC.htm)|Mudwretch|auto-trad|
|[HXWT5QxmOCn0poMt.htm](pathfinder-bestiary-2/HXWT5QxmOCn0poMt.htm)|Stingray|auto-trad|
|[HzeOxVy3VXxaJmC5.htm](pathfinder-bestiary-2/HzeOxVy3VXxaJmC5.htm)|Adult Umbral Dragon|auto-trad|
|[i8C4jqI3VdozylBL.htm](pathfinder-bestiary-2/i8C4jqI3VdozylBL.htm)|Cornugon|auto-trad|
|[iLZpGlyBWxS4Idbi.htm](pathfinder-bestiary-2/iLZpGlyBWxS4Idbi.htm)|Emperor Cobra|auto-trad|
|[iW6WKMVV3Ug8sa2q.htm](pathfinder-bestiary-2/iW6WKMVV3Ug8sa2q.htm)|Thanadaemon|auto-trad|
|[iZ3B52CYBCfqAaOo.htm](pathfinder-bestiary-2/iZ3B52CYBCfqAaOo.htm)|Adult Magma Dragon (Spellcaster)|auto-trad|
|[j43O2vBu8eu2wC5z.htm](pathfinder-bestiary-2/j43O2vBu8eu2wC5z.htm)|Vrykolakas Master|auto-trad|
|[jCF9HjF42SPXTptP.htm](pathfinder-bestiary-2/jCF9HjF42SPXTptP.htm)|Marrmora|auto-trad|
|[jCJUvzs1FaL6PAkq.htm](pathfinder-bestiary-2/jCJUvzs1FaL6PAkq.htm)|Bodak|auto-trad|
|[JCoILqEvwovT7tkj.htm](pathfinder-bestiary-2/JCoILqEvwovT7tkj.htm)|Chupacabra|auto-trad|
|[JgePINrFINmlsvn3.htm](pathfinder-bestiary-2/JgePINrFINmlsvn3.htm)|Hound Of Tindalos|auto-trad|
|[jgMFqFDNUWsrnnBH.htm](pathfinder-bestiary-2/jgMFqFDNUWsrnnBH.htm)|Tick Swarm|auto-trad|
|[jhr6WpTeY8GXXwK1.htm](pathfinder-bestiary-2/jhr6WpTeY8GXXwK1.htm)|Ifrit Pyrochemist|auto-trad|
|[JJKxWVExhUEno9j0.htm](pathfinder-bestiary-2/JJKxWVExhUEno9j0.htm)|Giant Leech|auto-trad|
|[JKBO2qqNrRezSb7d.htm](pathfinder-bestiary-2/JKBO2qqNrRezSb7d.htm)|Ancient Magma Dragon|auto-trad|
|[jpjhbGvaduK4iqwq.htm](pathfinder-bestiary-2/jpjhbGvaduK4iqwq.htm)|Spark Bat|auto-trad|
|[jQ7zBVDCEhuMOOTl.htm](pathfinder-bestiary-2/jQ7zBVDCEhuMOOTl.htm)|Nabasu|auto-trad|
|[JqnzS6RhUnOjnUdb.htm](pathfinder-bestiary-2/JqnzS6RhUnOjnUdb.htm)|Adult Cloud Dragon (Spellcaster)|auto-trad|
|[kapPWNOxXOLcd4VF.htm](pathfinder-bestiary-2/kapPWNOxXOLcd4VF.htm)|Amoeba Swarm|auto-trad|
|[kIzBE6VaHQbJ3kdx.htm](pathfinder-bestiary-2/kIzBE6VaHQbJ3kdx.htm)|Red Dracolisk|auto-trad|
|[kKH3uMHbVZQmEtxj.htm](pathfinder-bestiary-2/kKH3uMHbVZQmEtxj.htm)|Jotund Troll|auto-trad|
|[Km07kb7vPbSXO7EL.htm](pathfinder-bestiary-2/Km07kb7vPbSXO7EL.htm)|Young Crystal Dragon (Spellcaster)|auto-trad|
|[Kr5LtF0bqZJAC2Wn.htm](pathfinder-bestiary-2/Kr5LtF0bqZJAC2Wn.htm)|Ogre Spider|auto-trad|
|[Kw4UUxt5rorsGjsK.htm](pathfinder-bestiary-2/Kw4UUxt5rorsGjsK.htm)|Yellow Musk Brute|auto-trad|
|[l3kFjH4n9Shet6bz.htm](pathfinder-bestiary-2/l3kFjH4n9Shet6bz.htm)|Fen Mosquito Swarm|auto-trad|
|[l4UOJbphMT7jZ0xB.htm](pathfinder-bestiary-2/l4UOJbphMT7jZ0xB.htm)|Giant Crab|auto-trad|
|[LcdQZ2NTG2eJswXA.htm](pathfinder-bestiary-2/LcdQZ2NTG2eJswXA.htm)|Sceaduinar|auto-trad|
|[le7VaOJyQQnl37Wa.htm](pathfinder-bestiary-2/le7VaOJyQQnl37Wa.htm)|Giant Tick|auto-trad|
|[LQL8HU6tISXZwlOw.htm](pathfinder-bestiary-2/LQL8HU6tISXZwlOw.htm)|Skaveling|auto-trad|
|[LtaVzD7ADtWmBWap.htm](pathfinder-bestiary-2/LtaVzD7ADtWmBWap.htm)|Skrik Nettle|auto-trad|
|[luPebMfNmwtOZcWR.htm](pathfinder-bestiary-2/luPebMfNmwtOZcWR.htm)|Thrasfyr|auto-trad|
|[lUYlClIPITmhzepe.htm](pathfinder-bestiary-2/lUYlClIPITmhzepe.htm)|Two-Headed Troll|auto-trad|
|[lvvLNyZpbb4Ydz3U.htm](pathfinder-bestiary-2/lvvLNyZpbb4Ydz3U.htm)|Petitioner (Axis)|auto-trad|
|[LZp7LLcFzDBlrwod.htm](pathfinder-bestiary-2/LZp7LLcFzDBlrwod.htm)|Nixie|auto-trad|
|[M2BkGD9YwEmCvD9D.htm](pathfinder-bestiary-2/M2BkGD9YwEmCvD9D.htm)|Invidiak|auto-trad|
|[m40M5R7wcm5DSVK2.htm](pathfinder-bestiary-2/m40M5R7wcm5DSVK2.htm)|Dust Mephit|auto-trad|
|[M4mDroIJlrBYfHkM.htm](pathfinder-bestiary-2/M4mDroIJlrBYfHkM.htm)|Giant Crawling Hand|auto-trad|
|[MCZgXBC0I4Oovcjd.htm](pathfinder-bestiary-2/MCZgXBC0I4Oovcjd.htm)|Giant Wolverine|auto-trad|
|[MDdbC8XDUPFg4FCv.htm](pathfinder-bestiary-2/MDdbC8XDUPFg4FCv.htm)|Spriggan Warlord|auto-trad|
|[mpBOeFrbtx1wMrNn.htm](pathfinder-bestiary-2/mpBOeFrbtx1wMrNn.htm)|Frost Worm|auto-trad|
|[MUVc2ZjwGtlpcuX1.htm](pathfinder-bestiary-2/MUVc2ZjwGtlpcuX1.htm)|Gosreg|auto-trad|
|[MYE3jqYh3CrdWolX.htm](pathfinder-bestiary-2/MYE3jqYh3CrdWolX.htm)|Petitioner (Heaven)|auto-trad|
|[mZLOTDHhChme6mfh.htm](pathfinder-bestiary-2/mZLOTDHhChme6mfh.htm)|Young Crystal Dragon|auto-trad|
|[mZxVXRI1Zh3HUXxn.htm](pathfinder-bestiary-2/mZxVXRI1Zh3HUXxn.htm)|Living Thunderclap|auto-trad|
|[n1jTYFRCaqF1nQrf.htm](pathfinder-bestiary-2/n1jTYFRCaqF1nQrf.htm)|Titan Centipede|auto-trad|
|[N3XfcwDn1Nu7milG.htm](pathfinder-bestiary-2/N3XfcwDn1Nu7milG.htm)|Icewyrm|auto-trad|
|[n6cv7eCzPSs4dbHn.htm](pathfinder-bestiary-2/n6cv7eCzPSs4dbHn.htm)|Dweomercat|auto-trad|
|[N7Ej1AyPPolzboex.htm](pathfinder-bestiary-2/N7Ej1AyPPolzboex.htm)|Grindylow|auto-trad|
|[NAy7eUqFI8tGm3Ug.htm](pathfinder-bestiary-2/NAy7eUqFI8tGm3Ug.htm)|Taiga Linnorm|auto-trad|
|[nipVBTEgG6itWJSW.htm](pathfinder-bestiary-2/nipVBTEgG6itWJSW.htm)|Shoal Linnorm|auto-trad|
|[Nk3Inn5jel1VQMLt.htm](pathfinder-bestiary-2/Nk3Inn5jel1VQMLt.htm)|Radiant Warden|auto-trad|
|[NlMqwcN2ZaALOVSY.htm](pathfinder-bestiary-2/NlMqwcN2ZaALOVSY.htm)|Carnivorous Blob|auto-trad|
|[nOAQehZL3045YMX1.htm](pathfinder-bestiary-2/nOAQehZL3045YMX1.htm)|Young Umbral Dragon|auto-trad|
|[NoCtPLRoVvloZe63.htm](pathfinder-bestiary-2/NoCtPLRoVvloZe63.htm)|Adult Cloud Dragon|auto-trad|
|[NQ6qRvpD9DIwVxt3.htm](pathfinder-bestiary-2/NQ6qRvpD9DIwVxt3.htm)|Vampiric Mist|auto-trad|
|[nRsH7unLIRWq0ZHC.htm](pathfinder-bestiary-2/nRsH7unLIRWq0ZHC.htm)|Akata|auto-trad|
|[nSDXAfFOF2gEKmkM.htm](pathfinder-bestiary-2/nSDXAfFOF2gEKmkM.htm)|Movanic Deva|auto-trad|
|[NtezcliwnH4R1tJw.htm](pathfinder-bestiary-2/NtezcliwnH4R1tJw.htm)|Thulgant|auto-trad|
|[nxR3nseHT01YmOQo.htm](pathfinder-bestiary-2/nxR3nseHT01YmOQo.htm)|Piscodaemon|auto-trad|
|[nzLGh9GKCk7imto3.htm](pathfinder-bestiary-2/nzLGh9GKCk7imto3.htm)|Blodeuwedd|auto-trad|
|[o2IbtStBj11QCRvS.htm](pathfinder-bestiary-2/o2IbtStBj11QCRvS.htm)|Rhinoceros|auto-trad|
|[O69kXMo6shkA606k.htm](pathfinder-bestiary-2/O69kXMo6shkA606k.htm)|Choker|auto-trad|
|[O9LzsedLTUVFtOix.htm](pathfinder-bestiary-2/O9LzsedLTUVFtOix.htm)|Black Scorpion|auto-trad|
|[OaKx1ZaU25i3cypS.htm](pathfinder-bestiary-2/OaKx1ZaU25i3cypS.htm)|Theletos|auto-trad|
|[OaNtHZAraCrVLuSo.htm](pathfinder-bestiary-2/OaNtHZAraCrVLuSo.htm)|Mohrg|auto-trad|
|[OERK0GGnaEOthcdV.htm](pathfinder-bestiary-2/OERK0GGnaEOthcdV.htm)|Cave Fisher|auto-trad|
|[OFHr8UhsBOOgj0ef.htm](pathfinder-bestiary-2/OFHr8UhsBOOgj0ef.htm)|Intellect Devourer|auto-trad|
|[oKogG9AwUZ9iBsm3.htm](pathfinder-bestiary-2/oKogG9AwUZ9iBsm3.htm)|Derghodaemon|auto-trad|
|[oMZaSpBmWddKZ1KN.htm](pathfinder-bestiary-2/oMZaSpBmWddKZ1KN.htm)|Ancient Brine Dragon|auto-trad|
|[oSQ3Q9tzTohksAtf.htm](pathfinder-bestiary-2/oSQ3Q9tzTohksAtf.htm)|Crawling Hand|auto-trad|
|[OuimMwM3yFRy5tRK.htm](pathfinder-bestiary-2/OuimMwM3yFRy5tRK.htm)|Evangelist|auto-trad|
|[OVRvL7fKERLsM4KP.htm](pathfinder-bestiary-2/OVRvL7fKERLsM4KP.htm)|Grodair|auto-trad|
|[p5ZGpvIfEiBgMG2f.htm](pathfinder-bestiary-2/p5ZGpvIfEiBgMG2f.htm)|Petitioner (Ethereal Plane)|auto-trad|
|[PAGytLw1QnAiNYC2.htm](pathfinder-bestiary-2/PAGytLw1QnAiNYC2.htm)|Coil Spy|auto-trad|
|[PKAs8bMBMpnegmwq.htm](pathfinder-bestiary-2/PKAs8bMBMpnegmwq.htm)|Badger|auto-trad|
|[pqqe4IKAmUXdf7VM.htm](pathfinder-bestiary-2/pqqe4IKAmUXdf7VM.htm)|Adult Brine Dragon (Spellcaster)|auto-trad|
|[prNJZOU8tdRIZITT.htm](pathfinder-bestiary-2/prNJZOU8tdRIZITT.htm)|Doprillu|auto-trad|
|[pxlZ8QbxMK6b3l5w.htm](pathfinder-bestiary-2/pxlZ8QbxMK6b3l5w.htm)|Leng Spider|auto-trad|
|[PXTQ8wTvnd5qF1hW.htm](pathfinder-bestiary-2/PXTQ8wTvnd5qF1hW.htm)|Adult Brine Dragon|auto-trad|
|[qdlP3qTZtjzZXpmQ.htm](pathfinder-bestiary-2/qdlP3qTZtjzZXpmQ.htm)|Spriggan Bully|auto-trad|
|[QHNXT5bZDqKRzwEF.htm](pathfinder-bestiary-2/QHNXT5bZDqKRzwEF.htm)|Spear Frog|auto-trad|
|[qjAtPDIi7OyUGsNy.htm](pathfinder-bestiary-2/qjAtPDIi7OyUGsNy.htm)|Bythos|auto-trad|
|[QLY246Z6hP41LnCF.htm](pathfinder-bestiary-2/QLY246Z6hP41LnCF.htm)|Belker|auto-trad|
|[QpTRWkbZ6k6xP4tR.htm](pathfinder-bestiary-2/QpTRWkbZ6k6xP4tR.htm)|Petitioner (Plane of Water)|auto-trad|
|[QR3AC4mkpfz6KPSh.htm](pathfinder-bestiary-2/QR3AC4mkpfz6KPSh.htm)|Cythnigot|auto-trad|
|[R0dyG06kbNYiCqRW.htm](pathfinder-bestiary-2/R0dyG06kbNYiCqRW.htm)|Water Yai|auto-trad|
|[R0YrD0izubtQYbTU.htm](pathfinder-bestiary-2/R0YrD0izubtQYbTU.htm)|Leprechaun|auto-trad|
|[r3yLNfqS00g8sdrT.htm](pathfinder-bestiary-2/r3yLNfqS00g8sdrT.htm)|Duneshaker Solifugid|auto-trad|
|[r5i3MkSNgNBYa5Cm.htm](pathfinder-bestiary-2/r5i3MkSNgNBYa5Cm.htm)|Filth Fire|auto-trad|
|[r9L6tdT44kYeN1FV.htm](pathfinder-bestiary-2/r9L6tdT44kYeN1FV.htm)|Giant Chameleon|auto-trad|
|[RFCxaIS4OkacCpxD.htm](pathfinder-bestiary-2/RFCxaIS4OkacCpxD.htm)|Froghemoth|auto-trad|
|[ril4ohkYrf5mGwHO.htm](pathfinder-bestiary-2/ril4ohkYrf5mGwHO.htm)|Attic Whisperer|auto-trad|
|[RknMXlwJcbvGhk3n.htm](pathfinder-bestiary-2/RknMXlwJcbvGhk3n.htm)|Reef Octopus|auto-trad|
|[rpq2rHlZjy5g31xo.htm](pathfinder-bestiary-2/rpq2rHlZjy5g31xo.htm)|Grippli Scout|auto-trad|
|[RqABskvexmp5sT7X.htm](pathfinder-bestiary-2/RqABskvexmp5sT7X.htm)|Giant Cockroach|auto-trad|
|[RqcIiiNNEkF28ui2.htm](pathfinder-bestiary-2/RqcIiiNNEkF28ui2.htm)|Orca|auto-trad|
|[Rt3lbxJ4jcwGnpF3.htm](pathfinder-bestiary-2/Rt3lbxJ4jcwGnpF3.htm)|Petitioner (Plane of Earth)|auto-trad|
|[RUdP7qWg6pnKrPnh.htm](pathfinder-bestiary-2/RUdP7qWg6pnKrPnh.htm)|Striding Fire|auto-trad|
|[rZdaxjM7CFVAEq2e.htm](pathfinder-bestiary-2/rZdaxjM7CFVAEq2e.htm)|Pachycephalosaurus|auto-trad|
|[s0wn50S5mGxagSAa.htm](pathfinder-bestiary-2/s0wn50S5mGxagSAa.htm)|Zyss Serpentfolk|auto-trad|
|[s8EjxYqdN4gOvcBh.htm](pathfinder-bestiary-2/s8EjxYqdN4gOvcBh.htm)|Spinosaurus|auto-trad|
|[sbVdWfLZ2rdPh9Ec.htm](pathfinder-bestiary-2/sbVdWfLZ2rdPh9Ec.htm)|Carrion Golem|auto-trad|
|[scSsdEmG0jkEnyLV.htm](pathfinder-bestiary-2/scSsdEmG0jkEnyLV.htm)|Young Brine Dragon|auto-trad|
|[SFpMOxBwiS529dJk.htm](pathfinder-bestiary-2/SFpMOxBwiS529dJk.htm)|Brownie|auto-trad|
|[SfPoGbcaQqPlgJ85.htm](pathfinder-bestiary-2/SfPoGbcaQqPlgJ85.htm)|Baobhan Sith|auto-trad|
|[SG8fOxo4SS7bXpav.htm](pathfinder-bestiary-2/SG8fOxo4SS7bXpav.htm)|Bog Strider|auto-trad|
|[sGI1gh7EPVSRadhY.htm](pathfinder-bestiary-2/sGI1gh7EPVSRadhY.htm)|Granite Glyptodont|auto-trad|
|[sHKhlvs40YywkYw4.htm](pathfinder-bestiary-2/sHKhlvs40YywkYw4.htm)|Petitioner (Dimension of Dreams)|auto-trad|
|[SKooDsYDMEloMG16.htm](pathfinder-bestiary-2/SKooDsYDMEloMG16.htm)|Giant Jellyfish|auto-trad|
|[sMNqIHHxcVBXG08h.htm](pathfinder-bestiary-2/sMNqIHHxcVBXG08h.htm)|Cairn Wight|auto-trad|
|[SpnJQIHLeiJOjEWR.htm](pathfinder-bestiary-2/SpnJQIHLeiJOjEWR.htm)|Shadow Giant|auto-trad|
|[SsR4wiSWA7xhlkH9.htm](pathfinder-bestiary-2/SsR4wiSWA7xhlkH9.htm)|Iguanodon|auto-trad|
|[sY3VkazdyPiMgMj1.htm](pathfinder-bestiary-2/sY3VkazdyPiMgMj1.htm)|Fjord Linnorm|auto-trad|
|[SY8pvLPqc2EgP9Xj.htm](pathfinder-bestiary-2/SY8pvLPqc2EgP9Xj.htm)|Soul Eater|auto-trad|
|[SYaC46yrqcyp16Dq.htm](pathfinder-bestiary-2/SYaC46yrqcyp16Dq.htm)|Weretiger|auto-trad|
|[T3low0wQNMO0SEtr.htm](pathfinder-bestiary-2/T3low0wQNMO0SEtr.htm)|Leucrotta|auto-trad|
|[t8XwrLnvMnz3Z1al.htm](pathfinder-bestiary-2/t8XwrLnvMnz3Z1al.htm)|Catrina|auto-trad|
|[T9vzmpvpRL5xjrhd.htm](pathfinder-bestiary-2/T9vzmpvpRL5xjrhd.htm)|Giant Badger|auto-trad|
|[TCFurVn1aWhukqxb.htm](pathfinder-bestiary-2/TCFurVn1aWhukqxb.htm)|Petitioner (Dead Vault)|auto-trad|
|[tCJEjNDUxyV1GzE3.htm](pathfinder-bestiary-2/tCJEjNDUxyV1GzE3.htm)|Hamatula|auto-trad|
|[tdCyjq57rgfPezjT.htm](pathfinder-bestiary-2/tdCyjq57rgfPezjT.htm)|Carbuncle|auto-trad|
|[TeUO57XO1Mqk1cTl.htm](pathfinder-bestiary-2/TeUO57XO1Mqk1cTl.htm)|Ancient Magma Dragon (Spellcaster)|auto-trad|
|[tIDbOvltrFsgnwgf.htm](pathfinder-bestiary-2/tIDbOvltrFsgnwgf.htm)|Sea Drake|auto-trad|
|[tieZOKf2qX7GsL9H.htm](pathfinder-bestiary-2/tieZOKf2qX7GsL9H.htm)|Ice Golem|auto-trad|
|[TKlnOKpYJkg1dNwb.htm](pathfinder-bestiary-2/TKlnOKpYJkg1dNwb.htm)|Totenmaske|auto-trad|
|[tOVGYQueeJwLtn0v.htm](pathfinder-bestiary-2/tOVGYQueeJwLtn0v.htm)|Denizen of Leng|auto-trad|
|[TRXxfSSk8oKHvMn5.htm](pathfinder-bestiary-2/TRXxfSSk8oKHvMn5.htm)|Adult Umbral Dragon (Spellcaster)|auto-trad|
|[tu31BdhnxNknt7kK.htm](pathfinder-bestiary-2/tu31BdhnxNknt7kK.htm)|Marut|auto-trad|
|[TzdLLC68qlgrn91m.htm](pathfinder-bestiary-2/TzdLLC68qlgrn91m.htm)|Nyogoth|auto-trad|
|[tZkiE8hnN5e9KIzi.htm](pathfinder-bestiary-2/tZkiE8hnN5e9KIzi.htm)|Stygira|auto-trad|
|[U2hAALepVC90CNhQ.htm](pathfinder-bestiary-2/U2hAALepVC90CNhQ.htm)|Rusalka|auto-trad|
|[uBG93M52pa84qIt5.htm](pathfinder-bestiary-2/uBG93M52pa84qIt5.htm)|Giant Mosquito|auto-trad|
|[UcUlBj3brC80A8gq.htm](pathfinder-bestiary-2/UcUlBj3brC80A8gq.htm)|Snapping Turtle|auto-trad|
|[Ud6Iy1GolgtwrKQK.htm](pathfinder-bestiary-2/Ud6Iy1GolgtwrKQK.htm)|Babau|auto-trad|
|[UgEN8JYAT6tLbryy.htm](pathfinder-bestiary-2/UgEN8JYAT6tLbryy.htm)|Ahuizotl|auto-trad|
|[uGXTQYuRNtgPt1Pp.htm](pathfinder-bestiary-2/uGXTQYuRNtgPt1Pp.htm)|Skulk|auto-trad|
|[uiTUtcXixZ6TdKYE.htm](pathfinder-bestiary-2/uiTUtcXixZ6TdKYE.htm)|Vanth|auto-trad|
|[uIYl2DKLlcFLHNKo.htm](pathfinder-bestiary-2/uIYl2DKLlcFLHNKo.htm)|Young Brine Dragon (Spellcaster)|auto-trad|
|[UJHJVPPvV0VbQqlW.htm](pathfinder-bestiary-2/UJHJVPPvV0VbQqlW.htm)|Adult Crystal Dragon|auto-trad|
|[UnWr8QSMq2VZUQhX.htm](pathfinder-bestiary-2/UnWr8QSMq2VZUQhX.htm)|Tatzlwyrm|auto-trad|
|[uQBfbNaIHdt0l2QB.htm](pathfinder-bestiary-2/uQBfbNaIHdt0l2QB.htm)|Quickling|auto-trad|
|[UqFObUjgFAlWrriA.htm](pathfinder-bestiary-2/UqFObUjgFAlWrriA.htm)|Polar Bear|auto-trad|
|[URYQ5goPCmrQvJf5.htm](pathfinder-bestiary-2/URYQ5goPCmrQvJf5.htm)|Giant Ant|auto-trad|
|[UTnrcGoCIXR95zKi.htm](pathfinder-bestiary-2/UTnrcGoCIXR95zKi.htm)|Giant Whiptail Centipede|auto-trad|
|[V6OBYa51VcehPj22.htm](pathfinder-bestiary-2/V6OBYa51VcehPj22.htm)|Gray Ooze|auto-trad|
|[Va2LUALLnnfWX3wq.htm](pathfinder-bestiary-2/Va2LUALLnnfWX3wq.htm)|Vrolikai|auto-trad|
|[veyyE0brfjIoQK1q.htm](pathfinder-bestiary-2/veyyE0brfjIoQK1q.htm)|Icicle Snake|auto-trad|
|[vg3nE7p855SXkXbz.htm](pathfinder-bestiary-2/vg3nE7p855SXkXbz.htm)|Bebilith|auto-trad|
|[vHTvuteLd2kYX3ib.htm](pathfinder-bestiary-2/vHTvuteLd2kYX3ib.htm)|Flytrap Leshy|auto-trad|
|[VHXsmfnOBRBXB6TL.htm](pathfinder-bestiary-2/VHXsmfnOBRBXB6TL.htm)|Megalania|auto-trad|
|[vjEHKCgMunMHKl7r.htm](pathfinder-bestiary-2/vjEHKCgMunMHKl7r.htm)|Wereboar|auto-trad|
|[VlxScTgNFzdc6KUY.htm](pathfinder-bestiary-2/VlxScTgNFzdc6KUY.htm)|Young Cloud Dragon (Spellcaster)|auto-trad|
|[VPDKzeTY8O1XrKLk.htm](pathfinder-bestiary-2/VPDKzeTY8O1XrKLk.htm)|Giant Amoeba|auto-trad|
|[VrkmTQKB3Ln7wVTr.htm](pathfinder-bestiary-2/VrkmTQKB3Ln7wVTr.htm)|Behemoth Hippopotamus|auto-trad|
|[vXXEdDy8ciGJxt3i.htm](pathfinder-bestiary-2/vXXEdDy8ciGJxt3i.htm)|Mist Stalker|auto-trad|
|[VYEPG1G64OaCQmwf.htm](pathfinder-bestiary-2/VYEPG1G64OaCQmwf.htm)|Elasmosaurus|auto-trad|
|[W1OXnXGF22uybovO.htm](pathfinder-bestiary-2/W1OXnXGF22uybovO.htm)|Taiga Giant|auto-trad|
|[W2gd0emEVbD8EGmf.htm](pathfinder-bestiary-2/W2gd0emEVbD8EGmf.htm)|Marsh Giant|auto-trad|
|[wBGUIPJMm9Van9GQ.htm](pathfinder-bestiary-2/wBGUIPJMm9Van9GQ.htm)|Ice Yai|auto-trad|
|[wByXwxI5r7YRlkAK.htm](pathfinder-bestiary-2/wByXwxI5r7YRlkAK.htm)|Shoggti|auto-trad|
|[wCsfRovcZMZxSdGm.htm](pathfinder-bestiary-2/wCsfRovcZMZxSdGm.htm)|Zebub|auto-trad|
|[wE9kb53ZHifUfdD8.htm](pathfinder-bestiary-2/wE9kb53ZHifUfdD8.htm)|Petitioner (Abyss)|auto-trad|
|[WfzhHOqGb3bR1e7e.htm](pathfinder-bestiary-2/WfzhHOqGb3bR1e7e.htm)|Ancient Umbral Dragon (Spellcaster)|auto-trad|
|[wjcBMB7OvCHrnS39.htm](pathfinder-bestiary-2/wjcBMB7OvCHrnS39.htm)|Petitioner (Material Plane)|auto-trad|
|[WjZCNuHNKfeeHCQS.htm](pathfinder-bestiary-2/WjZCNuHNKfeeHCQS.htm)|Worm That Walks Cultist|auto-trad|
|[WKsogmP2X2F68Zdm.htm](pathfinder-bestiary-2/WKsogmP2X2F68Zdm.htm)|Devourer|auto-trad|
|[WMiPblOVq5w2Fa1x.htm](pathfinder-bestiary-2/WMiPblOVq5w2Fa1x.htm)|Aolaz|auto-trad|
|[WPTXi1WGKAvIIsrH.htm](pathfinder-bestiary-2/WPTXi1WGKAvIIsrH.htm)|Ancient Brine Dragon (Spellcaster)|auto-trad|
|[wqiFcGPARy8zmQfd.htm](pathfinder-bestiary-2/wqiFcGPARy8zmQfd.htm)|Hezrou|auto-trad|
|[WqppjHF2Y8yC6veS.htm](pathfinder-bestiary-2/WqppjHF2Y8yC6veS.htm)|Shadow Drake|auto-trad|
|[wqsmnoby2n3BOvKu.htm](pathfinder-bestiary-2/wqsmnoby2n3BOvKu.htm)|Moonflower|auto-trad|
|[wrzlJufGEfMg5kVE.htm](pathfinder-bestiary-2/wrzlJufGEfMg5kVE.htm)|Raven Swarm|auto-trad|
|[Wtok8DP7ybbEJ1fB.htm](pathfinder-bestiary-2/Wtok8DP7ybbEJ1fB.htm)|Cairn Linnorm|auto-trad|
|[wtxNIFVSpVFuYfmI.htm](pathfinder-bestiary-2/wtxNIFVSpVFuYfmI.htm)|Ancient Crystal Dragon|auto-trad|
|[wxBQsGLdti2xcGVT.htm](pathfinder-bestiary-2/wxBQsGLdti2xcGVT.htm)|Neothelid|auto-trad|
|[WxSpfUReYJb4StxG.htm](pathfinder-bestiary-2/WxSpfUReYJb4StxG.htm)|Blue-Ringed Octopus|auto-trad|
|[xarabMfYuo7yvD1N.htm](pathfinder-bestiary-2/xarabMfYuo7yvD1N.htm)|Giant Dragonfly Nymph|auto-trad|
|[Xb0uY2tg1Cd3kZd0.htm](pathfinder-bestiary-2/Xb0uY2tg1Cd3kZd0.htm)|Jyoti|auto-trad|
|[XCNRztCxn6EbJ0gr.htm](pathfinder-bestiary-2/XCNRztCxn6EbJ0gr.htm)|Norn|auto-trad|
|[xdIHvCBspeSXRL6A.htm](pathfinder-bestiary-2/xdIHvCBspeSXRL6A.htm)|Ghonhatine|auto-trad|
|[xfcFXLbadD3KdlHW.htm](pathfinder-bestiary-2/xfcFXLbadD3KdlHW.htm)|Lerritan|auto-trad|
|[xIvv8vKBa0YsTEzt.htm](pathfinder-bestiary-2/xIvv8vKBa0YsTEzt.htm)|Korred|auto-trad|
|[xjV0TmPNfKR7UcKu.htm](pathfinder-bestiary-2/xjV0TmPNfKR7UcKu.htm)|Osyluth|auto-trad|
|[xrAx0xj0UsM4DG5T.htm](pathfinder-bestiary-2/xrAx0xj0UsM4DG5T.htm)|Oread Guard|auto-trad|
|[xUtl7Nkb6iJpPUMK.htm](pathfinder-bestiary-2/xUtl7Nkb6iJpPUMK.htm)|Destrachan|auto-trad|
|[Xv74O1mFzzP06IXl.htm](pathfinder-bestiary-2/Xv74O1mFzzP06IXl.htm)|Hippogriff|auto-trad|
|[xxP5FJotshmUQNtY.htm](pathfinder-bestiary-2/xxP5FJotshmUQNtY.htm)|Black Bear|auto-trad|
|[XZZdG5RosoYsF237.htm](pathfinder-bestiary-2/XZZdG5RosoYsF237.htm)|Compsognathus|auto-trad|
|[Y0qsrBFTFMbaiLzr.htm](pathfinder-bestiary-2/Y0qsrBFTFMbaiLzr.htm)|Jabberwock|auto-trad|
|[Y1jsLqhsbqoXiGQe.htm](pathfinder-bestiary-2/Y1jsLqhsbqoXiGQe.htm)|Giant Toad|auto-trad|
|[Y1lwEf06O1ijOnwt.htm](pathfinder-bestiary-2/Y1lwEf06O1ijOnwt.htm)|Augnagar|auto-trad|
|[y2LqIPBHv2dA0pjI.htm](pathfinder-bestiary-2/y2LqIPBHv2dA0pjI.htm)|Necrophidius|auto-trad|
|[Y5uPBnABTaAnpdqz.htm](pathfinder-bestiary-2/Y5uPBnABTaAnpdqz.htm)|Young Umbral Dragon (Spellcaster)|auto-trad|
|[y9D2wh63pBzIyI7i.htm](pathfinder-bestiary-2/y9D2wh63pBzIyI7i.htm)|Giant Maggot|auto-trad|
|[Y9SDbANT1DWI1DKU.htm](pathfinder-bestiary-2/Y9SDbANT1DWI1DKU.htm)|Animate Dream|auto-trad|
|[yBglC9GBy6p1x87t.htm](pathfinder-bestiary-2/yBglC9GBy6p1x87t.htm)|Tendriculos|auto-trad|
|[YFvur5htRwt9EfQs.htm](pathfinder-bestiary-2/YFvur5htRwt9EfQs.htm)|Sandpoint Devil|auto-trad|
|[yIpB2uLyeBWQjfsn.htm](pathfinder-bestiary-2/yIpB2uLyeBWQjfsn.htm)|Gylou|auto-trad|
|[yr5xSfvSt7vA45wx.htm](pathfinder-bestiary-2/yr5xSfvSt7vA45wx.htm)|Young Cloud Dragon|auto-trad|
|[ytsHNgRwLzabsrZt.htm](pathfinder-bestiary-2/ytsHNgRwLzabsrZt.htm)|Meladaemon|auto-trad|
|[YUZF9WhDmfzma3dE.htm](pathfinder-bestiary-2/YUZF9WhDmfzma3dE.htm)|Nalfeshnee|auto-trad|
|[Yv0005Vym1peVKHq.htm](pathfinder-bestiary-2/Yv0005Vym1peVKHq.htm)|Ravener Husk|auto-trad|
|[yvmX4J21XE1wqDar.htm](pathfinder-bestiary-2/yvmX4J21XE1wqDar.htm)|Esobok|auto-trad|
|[yXP9vS35tZ5iAUix.htm](pathfinder-bestiary-2/yXP9vS35tZ5iAUix.htm)|Petitioner (Astral Plane)|auto-trad|
|[YZeJRw4hvKyBrNFx.htm](pathfinder-bestiary-2/YZeJRw4hvKyBrNFx.htm)|Petitioner (Hell)|auto-trad|
|[Z2v611Np3J1MBkpv.htm](pathfinder-bestiary-2/Z2v611Np3J1MBkpv.htm)|Dream Spider|auto-trad|
|[z39LYFGYmOsoGVSh.htm](pathfinder-bestiary-2/z39LYFGYmOsoGVSh.htm)|Hippocampus|auto-trad|
|[Z3J7uGaLmlFRfwPk.htm](pathfinder-bestiary-2/Z3J7uGaLmlFRfwPk.htm)|Magma Scorpion|auto-trad|
|[Z4eI4EhXBWDs8bl8.htm](pathfinder-bestiary-2/Z4eI4EhXBWDs8bl8.htm)|Undine Hydromancer|auto-trad|
|[Z9rOUqCqAaMjvgBc.htm](pathfinder-bestiary-2/Z9rOUqCqAaMjvgBc.htm)|Drainberry Bush|auto-trad|
|[zA9tCwjyViY6ZPfS.htm](pathfinder-bestiary-2/zA9tCwjyViY6ZPfS.htm)|Ancient Cloud Dragon|auto-trad|
|[zAxCU99Jwky6QoBy.htm](pathfinder-bestiary-2/zAxCU99Jwky6QoBy.htm)|Living Boulder|auto-trad|
|[ZBTqN3DNjdreXCu7.htm](pathfinder-bestiary-2/ZBTqN3DNjdreXCu7.htm)|Mastodon|auto-trad|
|[ZGEf08j9q4fIPDFd.htm](pathfinder-bestiary-2/ZGEf08j9q4fIPDFd.htm)|Dread Wraith|auto-trad|
|[zjPdqdQGNjO3Kto7.htm](pathfinder-bestiary-2/zjPdqdQGNjO3Kto7.htm)|Aurumvorax|auto-trad|
|[ZKTz1ABoPTtlgYIy.htm](pathfinder-bestiary-2/ZKTz1ABoPTtlgYIy.htm)|Hound Archon|auto-trad|
|[ZrDGTNxlISyxNAKX.htm](pathfinder-bestiary-2/ZrDGTNxlISyxNAKX.htm)|Bralani|auto-trad|
|[zXZrUTQSI0WxAsF5.htm](pathfinder-bestiary-2/zXZrUTQSI0WxAsF5.htm)|Verdurous Ooze|auto-trad|
|[ZyZMJ19Ea0cfydK4.htm](pathfinder-bestiary-2/ZyZMJ19Ea0cfydK4.htm)|Petitioner (Shadow Plane)|auto-trad|
|[zzu3q9m4kJzgnNfl.htm](pathfinder-bestiary-2/zzu3q9m4kJzgnNfl.htm)|Black Dracolisk|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[kElZCC1zMQVftoi9.htm](pathfinder-bestiary-2/kElZCC1zMQVftoi9.htm)|Isqulug|Isqulug|modificada|
|[q6FDEqLZQVjZfJ3x.htm](pathfinder-bestiary-2/q6FDEqLZQVjZfJ3x.htm)|Spiral Centurion|Centurión espiral|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
