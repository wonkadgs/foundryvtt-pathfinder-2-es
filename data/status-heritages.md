# Estado de la traducción (heritages)

 * **auto-trad**: 225
 * **modificada**: 2


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0Iv6LfT3UEt8taj5.htm](heritages/0Iv6LfT3UEt8taj5.htm)|Warden Human (BB)|auto-trad|
|[0TFf82gcfxXG9A54.htm](heritages/0TFf82gcfxXG9A54.htm)|Spellkeeper Shisk|auto-trad|
|[0vaeOoECfVD5EGbq.htm](heritages/0vaeOoECfVD5EGbq.htm)|Warrenbred Hobgoblin|auto-trad|
|[1dYDucCIaZpCJqBc.htm](heritages/1dYDucCIaZpCJqBc.htm)|Arctic Elf|auto-trad|
|[1lv7RMp7t5iqeUFT.htm](heritages/1lv7RMp7t5iqeUFT.htm)|Hunting Catfolk|auto-trad|
|[1oLMOmLpurfWTTff.htm](heritages/1oLMOmLpurfWTTff.htm)|Aasimar|auto-trad|
|[1wVDYY9Wue0G5R9Q.htm](heritages/1wVDYY9Wue0G5R9Q.htm)|Whisper Elf|auto-trad|
|[2cii5ZkBsJ4DYdd2.htm](heritages/2cii5ZkBsJ4DYdd2.htm)|Cactus Leshy|auto-trad|
|[2hLDilS6qbjHxgVS.htm](heritages/2hLDilS6qbjHxgVS.htm)|Dogtooth Tengu|auto-trad|
|[2jsWmnKtidCTpaQV.htm](heritages/2jsWmnKtidCTpaQV.htm)|Venomshield Nagaji|auto-trad|
|[2kMltxs2rmxRSxfV.htm](heritages/2kMltxs2rmxRSxfV.htm)|Hunter Automaton|auto-trad|
|[2kSzKDtwbcILZTIe.htm](heritages/2kSzKDtwbcILZTIe.htm)|Snaptongue Grippli|auto-trad|
|[32oX6hHUY6K8N70Q.htm](heritages/32oX6hHUY6K8N70Q.htm)|Charhide Goblin|auto-trad|
|[35k2aujXYvqUCSS1.htm](heritages/35k2aujXYvqUCSS1.htm)|Cavern Kobold|auto-trad|
|[3F5ffk7cmnrBhPcT.htm](heritages/3F5ffk7cmnrBhPcT.htm)|Liminal Catfolk|auto-trad|
|[3reGfXH0S82hM7Gp.htm](heritages/3reGfXH0S82hM7Gp.htm)|Ganzi|auto-trad|
|[5A1wMPdzN1OWE4cY.htm](heritages/5A1wMPdzN1OWE4cY.htm)|Caveclimber Kobold|auto-trad|
|[5CqsBKCZuGON53Hk.htm](heritages/5CqsBKCZuGON53Hk.htm)|Forge Dwarf|auto-trad|
|[6dMd4JG0ndrObEUj.htm](heritages/6dMd4JG0ndrObEUj.htm)|Winter Catfolk|auto-trad|
|[6JKdAZGa8odFzleS.htm](heritages/6JKdAZGa8odFzleS.htm)|Farsight Goloma|auto-trad|
|[6KhxY5ArGFhLF7r1.htm](heritages/6KhxY5ArGFhLF7r1.htm)|Thorned Rose|auto-trad|
|[6rIIsZg3tOyIU3g3.htm](heritages/6rIIsZg3tOyIU3g3.htm)|Frilled Lizardfolk|auto-trad|
|[6xxXtgj3fcCi53lt.htm](heritages/6xxXtgj3fcCi53lt.htm)|Sandstrider Lizardfolk|auto-trad|
|[7gGcpQMqnZhBDZLI.htm](heritages/7gGcpQMqnZhBDZLI.htm)|Adaptive Anadi|auto-trad|
|[7kHg780SAsu2FNfP.htm](heritages/7kHg780SAsu2FNfP.htm)|Stuffed Poppet|auto-trad|
|[7lFPhRMAFXQsXUP2.htm](heritages/7lFPhRMAFXQsXUP2.htm)|Snow Rat|auto-trad|
|[7p9HtLzWBHc18JDW.htm](heritages/7p9HtLzWBHc18JDW.htm)|Deep Rat|auto-trad|
|[7vHLPleFpSqKAjWG.htm](heritages/7vHLPleFpSqKAjWG.htm)|Stormtossed Tengu|auto-trad|
|[7wdeVadvchdM0aPK.htm](heritages/7wdeVadvchdM0aPK.htm)|Mistbreath Azarketi|auto-trad|
|[7ZDCShtRg5QZggrU.htm](heritages/7ZDCShtRg5QZggrU.htm)|Inured Azarketi|auto-trad|
|[85tRKGZUTFa6pKpG.htm](heritages/85tRKGZUTFa6pKpG.htm)|Oathkeeper Dwarf|auto-trad|
|[87h0jepQuzIbN7jN.htm](heritages/87h0jepQuzIbN7jN.htm)|Fungus Leshy|auto-trad|
|[8Gsa8KFsHizEwSHU.htm](heritages/8Gsa8KFsHizEwSHU.htm)|Badlands Orc|auto-trad|
|[8wGUh9RsMUamOKjh.htm](heritages/8wGUh9RsMUamOKjh.htm)|Tailed Goblin|auto-trad|
|[9Iu1gFEuvVz9zaYU.htm](heritages/9Iu1gFEuvVz9zaYU.htm)|Spined Azarketi|auto-trad|
|[9mS8EGLlGUOzSAzP.htm](heritages/9mS8EGLlGUOzSAzP.htm)|Nightglider Strix|auto-trad|
|[a6F2WjYU8D0suT8T.htm](heritages/a6F2WjYU8D0suT8T.htm)|Razortooth Goblin|auto-trad|
|[AVZJI5wP2X5o0LL3.htm](heritages/AVZJI5wP2X5o0LL3.htm)|Trogloshi|auto-trad|
|[B89BCo6LtI3SJq54.htm](heritages/B89BCo6LtI3SJq54.htm)|Sweetbreath Gnoll|auto-trad|
|[BFOsMnWfXL1oaWkY.htm](heritages/BFOsMnWfXL1oaWkY.htm)|Steelskin Hobgoblin|auto-trad|
|[BhbwjTFw2V67XF35.htm](heritages/BhbwjTFw2V67XF35.htm)|Keen-Venom Vishkanya|auto-trad|
|[BjuZKA7lzFSjKbif.htm](heritages/BjuZKA7lzFSjKbif.htm)|Polyglot Android|auto-trad|
|[bKr34Uvxc2XClr9q.htm](heritages/bKr34Uvxc2XClr9q.htm)|Strong Oak|auto-trad|
|[bLhIBwqdjTiVJ5qm.htm](heritages/bLhIBwqdjTiVJ5qm.htm)|Clawed Catfolk|auto-trad|
|[bmA9JK06rnOKpNLr.htm](heritages/bmA9JK06rnOKpNLr.htm)|Poisonhide Grippli|auto-trad|
|[By1y7HCfVPgX2GmI.htm](heritages/By1y7HCfVPgX2GmI.htm)|Ragdyan Vanara|auto-trad|
|[CCwTBSNTw0caN1jd.htm](heritages/CCwTBSNTw0caN1jd.htm)|Mutated Fleshwarp|auto-trad|
|[cCy8vsZENlwiAyZ6.htm](heritages/cCy8vsZENlwiAyZ6.htm)|Twilight Halfling|auto-trad|
|[cDElVLonQvUK3vVk.htm](heritages/cDElVLonQvUK3vVk.htm)|Whipfang Nagaji|auto-trad|
|[ClshvrjvBTMm3INX.htm](heritages/ClshvrjvBTMm3INX.htm)|Bandaagee Vanara|auto-trad|
|[CMf0qluB0LXWReew.htm](heritages/CMf0qluB0LXWReew.htm)|Ancient Ash|auto-trad|
|[cnbwtbDmlD0KoLqY.htm](heritages/cnbwtbDmlD0KoLqY.htm)|Insightful Goloma|auto-trad|
|[CnCFZfuKzAYqy61e.htm](heritages/CnCFZfuKzAYqy61e.htm)|Athamasi|auto-trad|
|[Csezts78L4FMaskB.htm](heritages/Csezts78L4FMaskB.htm)|Lethoci|auto-trad|
|[Cv7BOjuziOQ0PO9r.htm](heritages/Cv7BOjuziOQ0PO9r.htm)|Windup Poppet|auto-trad|
|[cwOUw7kofcAiY01I.htm](heritages/cwOUw7kofcAiY01I.htm)|Snaring Anadi|auto-trad|
|[CzOHITB2ihLGqMuJ.htm](heritages/CzOHITB2ihLGqMuJ.htm)|Runtboss Hobgoblin|auto-trad|
|[CZx9HMmoOwcpkLY8.htm](heritages/CZx9HMmoOwcpkLY8.htm)|Root Leshy|auto-trad|
|[d0bNxgGqvaCkFlhN.htm](heritages/d0bNxgGqvaCkFlhN.htm)|Umbral Gnome|auto-trad|
|[D3hTAqgwSank8OyO.htm](heritages/D3hTAqgwSank8OyO.htm)|Fey-Touched Gnome|auto-trad|
|[d7NC4C19AgkspQQg.htm](heritages/d7NC4C19AgkspQQg.htm)|Battle-Trained Human (BB)|auto-trad|
|[daaXga11ov9YQVNq.htm](heritages/daaXga11ov9YQVNq.htm)|Polychromatic Anadi|auto-trad|
|[dJeiekfqGQ8dkwsO.htm](heritages/dJeiekfqGQ8dkwsO.htm)|Wetlander Lizardfolk|auto-trad|
|[dQqurQys37aJYb26.htm](heritages/dQqurQys37aJYb26.htm)|Leaf Leshy|auto-trad|
|[dwKCwwtWetvPmJks.htm](heritages/dwKCwwtWetvPmJks.htm)|Rainfall Orc|auto-trad|
|[EEvA4uj8h3zDiAfP.htm](heritages/EEvA4uj8h3zDiAfP.htm)|Treedweller Goblin|auto-trad|
|[eFsD7W6hnK33jlDQ.htm](heritages/eFsD7W6hnK33jlDQ.htm)|Sewer Rat|auto-trad|
|[EHDYVhJcZ9uPUjfZ.htm](heritages/EHDYVhJcZ9uPUjfZ.htm)|Toy Poppet|auto-trad|
|[EKY9v7SF1hVsUdbH.htm](heritages/EKY9v7SF1hVsUdbH.htm)|Changeling|auto-trad|
|[EoWwvDdoMqN5x0c9.htm](heritages/EoWwvDdoMqN5x0c9.htm)|Rite of Light|auto-trad|
|[Eq42wZ5OTweJLnLU.htm](heritages/Eq42wZ5OTweJLnLU.htm)|Gutsy Halfling|auto-trad|
|[etkuQkjkNLPLnjkA.htm](heritages/etkuQkjkNLPLnjkA.htm)|Wellspring Gnome|auto-trad|
|[evXJISqyhl3fHE9u.htm](heritages/evXJISqyhl3fHE9u.htm)|Vine Leshy|auto-trad|
|[faLb2rczsrxAuOTt.htm](heritages/faLb2rczsrxAuOTt.htm)|Rite of Knowing|auto-trad|
|[Fgysc0A1pFQE8PMA.htm](heritages/Fgysc0A1pFQE8PMA.htm)|Lorekeeper Shisk|auto-trad|
|[FLWUYM2XxYwnIDQf.htm](heritages/FLWUYM2XxYwnIDQf.htm)|Xyloshi|auto-trad|
|[fROPRHGyUn4PgcER.htm](heritages/fROPRHGyUn4PgcER.htm)|Longsnout Rat|auto-trad|
|[fWT7Mo2vFC10H4Wq.htm](heritages/fWT7Mo2vFC10H4Wq.htm)|Songbird Strix|auto-trad|
|[g4FRxyuHndZu4KTo.htm](heritages/g4FRxyuHndZu4KTo.htm)|Jinxed Tengu|auto-trad|
|[G8jfMayPv4vZvAVr.htm](heritages/G8jfMayPv4vZvAVr.htm)|Sylph|auto-trad|
|[G9Gwfi8ZIva52uGp.htm](heritages/G9Gwfi8ZIva52uGp.htm)|Jinxed Halfling|auto-trad|
|[GAn2cdhBE9Bqa85s.htm](heritages/GAn2cdhBE9Bqa85s.htm)|Beastkin|auto-trad|
|[gfXSF1TafBAmZo2u.htm](heritages/gfXSF1TafBAmZo2u.htm)|Grig|auto-trad|
|[gKLTlzAVapXTQ86V.htm](heritages/gKLTlzAVapXTQ86V.htm)|Reflection|auto-trad|
|[GlejQr3rBh3sn8sL.htm](heritages/GlejQr3rBh3sn8sL.htm)|River Azarketi|auto-trad|
|[Gmk7oNITvMVBy78Z.htm](heritages/Gmk7oNITvMVBy78Z.htm)|Undine|auto-trad|
|[GpnHIonrLN8TFZci.htm](heritages/GpnHIonrLN8TFZci.htm)|Rite of Invocation|auto-trad|
|[gQyPU441J3rGt8mD.htm](heritages/gQyPU441J3rGt8mD.htm)|Snow Goblin|auto-trad|
|[gyoN45SVfRZwHMkk.htm](heritages/gyoN45SVfRZwHMkk.htm)|Irongut Goblin|auto-trad|
|[h2VKMYAlUIFAAXVG.htm](heritages/h2VKMYAlUIFAAXVG.htm)|Nyktera|auto-trad|
|[hFBwsVcSnNCJoimo.htm](heritages/hFBwsVcSnNCJoimo.htm)|Versatile Heritage|auto-trad|
|[HFHSh2RWuxa4GhhQ.htm](heritages/HFHSh2RWuxa4GhhQ.htm)|Benthic Azarketi|auto-trad|
|[hOPOyyt7qZXYYCOU.htm](heritages/hOPOyyt7qZXYYCOU.htm)|Nine Lives Catfolk|auto-trad|
|[HpqQ5VQ0w4HqYgVC.htm](heritages/HpqQ5VQ0w4HqYgVC.htm)|Jungle Catfolk|auto-trad|
|[hTl3uc6y1kTuo9ac.htm](heritages/hTl3uc6y1kTuo9ac.htm)|Seaweed Leshy|auto-trad|
|[hzW7VoRDYsKJB8ku.htm](heritages/hzW7VoRDYsKJB8ku.htm)|Surgewise Fleshwarp|auto-trad|
|[idGDjqi1q3Ft8bAZ.htm](heritages/idGDjqi1q3Ft8bAZ.htm)|Nomadic Halfling|auto-trad|
|[ievKYUc53q0mroGp.htm](heritages/ievKYUc53q0mroGp.htm)|Lotus Leshy|auto-trad|
|[IFg2tqmAFFnU8UNU.htm](heritages/IFg2tqmAFFnU8UNU.htm)|Celestial Envoy Kitsune|auto-trad|
|[ikNJZRxUjcRLisO6.htm](heritages/ikNJZRxUjcRLisO6.htm)|Elfbane Hobgoblin|auto-trad|
|[iOREr80Q0SsvPP8B.htm](heritages/iOREr80Q0SsvPP8B.htm)|Sacred Nagaji|auto-trad|
|[isJhIPhT4MsjJvoq.htm](heritages/isJhIPhT4MsjJvoq.htm)|Fishseeker Shoony|auto-trad|
|[ITgkqfnAOJCbcIys.htm](heritages/ITgkqfnAOJCbcIys.htm)|Oread|auto-trad|
|[iY2CCqoMc2bRdoas.htm](heritages/iY2CCqoMc2bRdoas.htm)|Created Fleshwarp|auto-trad|
|[J0eAmntxXywr9sGt.htm](heritages/J0eAmntxXywr9sGt.htm)|Mage Automaton|auto-trad|
|[j0R1SyJP8k4G2Hkn.htm](heritages/j0R1SyJP8k4G2Hkn.htm)|Scavenger Strix|auto-trad|
|[j1nzBc9Pui7vsJ9o.htm](heritages/j1nzBc9Pui7vsJ9o.htm)|Sharpshooter Automaton|auto-trad|
|[Je15UGsLWYaaGJSW.htm](heritages/Je15UGsLWYaaGJSW.htm)|Ghost Poppet|auto-trad|
|[jEtVesbqYcWGbBYk.htm](heritages/jEtVesbqYcWGbBYk.htm)|Seer Elf|auto-trad|
|[K124fCpU03SJvmeP.htm](heritages/K124fCpU03SJvmeP.htm)|Warmarch Hobgoblin|auto-trad|
|[k4AU5tjtngDOIqrB.htm](heritages/k4AU5tjtngDOIqrB.htm)|Deep Fetchling|auto-trad|
|[KaokXdiE3ewXprdL.htm](heritages/KaokXdiE3ewXprdL.htm)|Pine Leshy|auto-trad|
|[KbG2BZ3Sbr3xU1sW.htm](heritages/KbG2BZ3Sbr3xU1sW.htm)|Pixie|auto-trad|
|[KcozzlkFAqShDEzo.htm](heritages/KcozzlkFAqShDEzo.htm)|Stronggut Shisk|auto-trad|
|[kHHcvJBJNiPJTuna.htm](heritages/kHHcvJBJNiPJTuna.htm)|Wisp Fetchling|auto-trad|
|[kHT9dFJt5yTjeYoB.htm](heritages/kHT9dFJt5yTjeYoB.htm)|Frozen Wind Kitsune|auto-trad|
|[kiKxnKd7Dfegk9dM.htm](heritages/kiKxnKd7Dfegk9dM.htm)|Desert Elf|auto-trad|
|[KJ2dSDXP9d5hJHzd.htm](heritages/KJ2dSDXP9d5hJHzd.htm)|Frightful Goloma|auto-trad|
|[KO33MNyY9VqNQmbZ.htm](heritages/KO33MNyY9VqNQmbZ.htm)|Wintertouched Human|auto-trad|
|[Kq3k1Z6IWGVsLrmg.htm](heritages/Kq3k1Z6IWGVsLrmg.htm)|Lahkgyan Vanara|auto-trad|
|[kRDsVbhdBVeSlpBa.htm](heritages/kRDsVbhdBVeSlpBa.htm)|Anvil Dwarf|auto-trad|
|[kTlJqhC7ZSE8P8lu.htm](heritages/kTlJqhC7ZSE8P8lu.htm)|Venomous Anadi|auto-trad|
|[kXp8qRh5AgtD4Izi.htm](heritages/kXp8qRh5AgtD4Izi.htm)|Witch Gnoll|auto-trad|
|[lDT5h3f5GXRj42Ir.htm](heritages/lDT5h3f5GXRj42Ir.htm)|Stonestep Shisk|auto-trad|
|[lj5iHaiY0IwCCptd.htm](heritages/lj5iHaiY0IwCCptd.htm)|Aphorite|auto-trad|
|[LlUEmCDOLSZaGOyI.htm](heritages/LlUEmCDOLSZaGOyI.htm)|Titan Nagaji|auto-trad|
|[Lp7ywxabmm88Gei6.htm](heritages/Lp7ywxabmm88Gei6.htm)|Observant Halfling|auto-trad|
|[LU4i3qXtyzeTGWZQ.htm](heritages/LU4i3qXtyzeTGWZQ.htm)|Luminous Sprite|auto-trad|
|[m9rrlchS10xHFA2G.htm](heritages/m9rrlchS10xHFA2G.htm)|Venomtail Kobold|auto-trad|
|[mBH1L01kYmB8EL56.htm](heritages/mBH1L01kYmB8EL56.htm)|Empty Sky Kitsune|auto-trad|
|[MeMAAtUlZmFgKSMF.htm](heritages/MeMAAtUlZmFgKSMF.htm)|Elemental Heart Dwarf|auto-trad|
|[MhXHEh7utEfxBwmc.htm](heritages/MhXHEh7utEfxBwmc.htm)|Thickcoat Shoony|auto-trad|
|[Mj7uHxxVkRUlOFwJ.htm](heritages/Mj7uHxxVkRUlOFwJ.htm)|Hillock Halfling|auto-trad|
|[Mmezbef0c1fbJaVV.htm](heritages/Mmezbef0c1fbJaVV.htm)|Impersonator Android|auto-trad|
|[mnhpCk9dIwMuFegM.htm](heritages/mnhpCk9dIwMuFegM.htm)|Paddler Shoony|auto-trad|
|[MQx7miBXUmOHycqJ.htm](heritages/MQx7miBXUmOHycqJ.htm)|Laborer Android|auto-trad|
|[MtH5bq0MhaMQbJEL.htm](heritages/MtH5bq0MhaMQbJEL.htm)|Murkeyed Azarketi|auto-trad|
|[MTTU2t7x6TjvUDnE.htm](heritages/MTTU2t7x6TjvUDnE.htm)|Hold-Scarred Orc|auto-trad|
|[MUujYQYWg6PNVaaN.htm](heritages/MUujYQYWg6PNVaaN.htm)|Predator Strix|auto-trad|
|[mZqaKQkvadBbNubM.htm](heritages/mZqaKQkvadBbNubM.htm)|Compact Skeleton|auto-trad|
|[n2DKA0OQQcfvZRly.htm](heritages/n2DKA0OQQcfvZRly.htm)|Technological Fleshwarp|auto-trad|
|[n2eJEjA8pnOMiuCm.htm](heritages/n2eJEjA8pnOMiuCm.htm)|Smokeworker Hobgoblin|auto-trad|
|[N36ZR4lh9eCazDaN.htm](heritages/N36ZR4lh9eCazDaN.htm)|Half-Elf|auto-trad|
|[Nd9hdX8rdYyRozw8.htm](heritages/Nd9hdX8rdYyRozw8.htm)|Ancient Elf|auto-trad|
|[NfIAGatB1KIzt8G7.htm](heritages/NfIAGatB1KIzt8G7.htm)|Cavern Elf|auto-trad|
|[nUCRd8tmz3C6LM0T.htm](heritages/nUCRd8tmz3C6LM0T.htm)|Wajaghand Vanara|auto-trad|
|[NWbdAN5gDse0ad7C.htm](heritages/NWbdAN5gDse0ad7C.htm)|Dark Fields Kitsune|auto-trad|
|[NWsZ0cIeghyzk9bU.htm](heritages/NWsZ0cIeghyzk9bU.htm)|Sharp-Eared Catfolk|auto-trad|
|[nXQxlmjH24Eb8h2Q.htm](heritages/nXQxlmjH24Eb8h2Q.htm)|Battle-Ready Orc|auto-trad|
|[OBxrlZKg0IC5n238.htm](heritages/OBxrlZKg0IC5n238.htm)|Venom-Resistant Vishkanya|auto-trad|
|[ohOJHeFenX97sBHf.htm](heritages/ohOJHeFenX97sBHf.htm)|Scalekeeper Vishkanya|auto-trad|
|[OoUqJJB77VfWbWRM.htm](heritages/OoUqJJB77VfWbWRM.htm)|Cliffscale Lizardfolk|auto-trad|
|[OtqOC3ElpF444qMe.htm](heritages/OtqOC3ElpF444qMe.htm)|Discarded Fleshwarp|auto-trad|
|[P8BP1un5BTrwXoBy.htm](heritages/P8BP1un5BTrwXoBy.htm)|Dragonscaled Kobold|auto-trad|
|[P8Rl3dUsq8AzXLHC.htm](heritages/P8Rl3dUsq8AzXLHC.htm)|Sturdy Skeleton|auto-trad|
|[PbXqlzRdQbKLQx1R.htm](heritages/PbXqlzRdQbKLQx1R.htm)|Old-Blood Vishkanya|auto-trad|
|[pJ395g22dKNoufIK.htm](heritages/pJ395g22dKNoufIK.htm)|Nascent|auto-trad|
|[PQuJEYI0UFl8W7fH.htm](heritages/PQuJEYI0UFl8W7fH.htm)|Cataphract Fleshwarp|auto-trad|
|[ptpK6H1rM4Bu3ry4.htm](heritages/ptpK6H1rM4Bu3ry4.htm)|Mountainkeeper Tengu|auto-trad|
|[PwxbD5VSJ0Yroqvp.htm](heritages/PwxbD5VSJ0Yroqvp.htm)|Liminal Fetchling|auto-trad|
|[pZ1u2ScWrBXSaAqQ.htm](heritages/pZ1u2ScWrBXSaAqQ.htm)|Winter Orc|auto-trad|
|[q2omqJ9t0skGTYki.htm](heritages/q2omqJ9t0skGTYki.htm)|Rite of Reinforcement|auto-trad|
|[qbWaybAX1LK7kUyY.htm](heritages/qbWaybAX1LK7kUyY.htm)|Thalassic Azarketi|auto-trad|
|[qM566kCXljkOpocA.htm](heritages/qM566kCXljkOpocA.htm)|Taloned Tengu|auto-trad|
|[rFdVYKtHsZzRCsSd.htm](heritages/rFdVYKtHsZzRCsSd.htm)|Stickytoe Grippli|auto-trad|
|[rKV11HWREwjjMIum.htm](heritages/rKV11HWREwjjMIum.htm)|Skyborn Tengu|auto-trad|
|[RKz7Z5pefXKiv9JE.htm](heritages/RKz7Z5pefXKiv9JE.htm)|Suli|auto-trad|
|[rQJBtQ9uKUzK9ktK.htm](heritages/rQJBtQ9uKUzK9ktK.htm)|Shortshanks Hobgoblin|auto-trad|
|[RuQSx0QsirIKxwKY.htm](heritages/RuQSx0QsirIKxwKY.htm)|Warrior Android|auto-trad|
|[RxNBBMFZwPA3Vlg3.htm](heritages/RxNBBMFZwPA3Vlg3.htm)|Shoreline Strix|auto-trad|
|[RZHr0olieS6YdYE9.htm](heritages/RZHr0olieS6YdYE9.htm)|Fodder Skeleton|auto-trad|
|[S1062No0sYH35AhN.htm](heritages/S1062No0sYH35AhN.htm)|Tactile Azarketi|auto-trad|
|[sEnMG5zbnXdJvVPz.htm](heritages/sEnMG5zbnXdJvVPz.htm)|Cloudleaper Lizardfolk|auto-trad|
|[sGzhnQpgWErX1bmx.htm](heritages/sGzhnQpgWErX1bmx.htm)|Vigilant Goloma|auto-trad|
|[SqEcb1c3yeoJMxm0.htm](heritages/SqEcb1c3yeoJMxm0.htm)|Great Gnoll|auto-trad|
|[Svk2CHwvurK1QQhD.htm](heritages/Svk2CHwvurK1QQhD.htm)|Ifrit|auto-trad|
|[tarfuEXmi0E0Enfy.htm](heritages/tarfuEXmi0E0Enfy.htm)|Shadow Rat|auto-trad|
|[TDc9MXLXkgEFoKdD.htm](heritages/TDc9MXLXkgEFoKdD.htm)|Flexible Catfolk|auto-trad|
|[tf3edMCyS15GhFPx.htm](heritages/tf3edMCyS15GhFPx.htm)|Hooded Nagaji|auto-trad|
|[tLd8Qg82AwEbbmgX.htm](heritages/tLd8Qg82AwEbbmgX.htm)|Half-Orc|auto-trad|
|[TQFE10VFvh9wb8zb.htm](heritages/TQFE10VFvh9wb8zb.htm)|Woodstalker Lizardfolk|auto-trad|
|[twayjFuXbsvyHUwy.htm](heritages/twayjFuXbsvyHUwy.htm)|Desert Rat|auto-trad|
|[tXC5Gwn9D5x0ouJh.htm](heritages/tXC5Gwn9D5x0ouJh.htm)|Sensate Gnome|auto-trad|
|[TYvzNoL5ldmB5F76.htm](heritages/TYvzNoL5ldmB5F76.htm)|Melixie|auto-trad|
|[U882U2NUUGL6u3rL.htm](heritages/U882U2NUUGL6u3rL.htm)|Tunnel Rat|auto-trad|
|[UaD5VDoFlILEmbyz.htm](heritages/UaD5VDoFlILEmbyz.htm)|Dhampir|auto-trad|
|[udMXXjFirjARYr4p.htm](heritages/udMXXjFirjARYr4p.htm)|Ant Gnoll|auto-trad|
|[ULj56ZoW7dWdnBvu.htm](heritages/ULj56ZoW7dWdnBvu.htm)|Unbreakable Goblin|auto-trad|
|[UV2sABrTC5teOXTE.htm](heritages/UV2sABrTC5teOXTE.htm)|Strong-Blooded Dwarf|auto-trad|
|[uxtcKTkD62SmrUoh.htm](heritages/uxtcKTkD62SmrUoh.htm)|Shapewrought Fleshwarp|auto-trad|
|[VAo6NnrCEAAOUSkc.htm](heritages/VAo6NnrCEAAOUSkc.htm)|Resolute Fetchling|auto-trad|
|[vDEfNzjLpGJU54cz.htm](heritages/vDEfNzjLpGJU54cz.htm)|Quillcoat Shisk|auto-trad|
|[VgL18yU7pysdoZZG.htm](heritages/VgL18yU7pysdoZZG.htm)|Artisan Android|auto-trad|
|[ViKRoVgog172r163.htm](heritages/ViKRoVgog172r163.htm)|Vivacious Gnome|auto-trad|
|[VqgrYMaAwnNjT9Mn.htm](heritages/VqgrYMaAwnNjT9Mn.htm)|Enchanting Lily|auto-trad|
|[vrU3lmDO7FYzmuQc.htm](heritages/vrU3lmDO7FYzmuQc.htm)|Prismatic Vishkanya|auto-trad|
|[VRyX00OuPGsJSurM.htm](heritages/VRyX00OuPGsJSurM.htm)|Spellscale Kobold|auto-trad|
|[VSyOvtgJ1ZNpIVgC.htm](heritages/VSyOvtgJ1ZNpIVgC.htm)|Rock Dwarf|auto-trad|
|[VTtXwBxrfRUXSL38.htm](heritages/VTtXwBxrfRUXSL38.htm)|Death Warden Dwarf|auto-trad|
|[VvEAFoxuddYNBmNc.htm](heritages/VvEAFoxuddYNBmNc.htm)|Grave Orc|auto-trad|
|[VYfpTUuXJM3iBOz0.htm](heritages/VYfpTUuXJM3iBOz0.htm)|Unseen Lizardfolk|auto-trad|
|[w5801ArZQCU8IXnU.htm](heritages/w5801ArZQCU8IXnU.htm)|Wishborn Poppet|auto-trad|
|[WaCn0mcivFv1omNK.htm](heritages/WaCn0mcivFv1omNK.htm)|Strongjaw Kobold|auto-trad|
|[wB8xiQB4RDbzOOvR.htm](heritages/wB8xiQB4RDbzOOvR.htm)|Monstrous Skeleton|auto-trad|
|[WEzgrxBRFBGdj8Hx.htm](heritages/WEzgrxBRFBGdj8Hx.htm)|Wavediver Tengu|auto-trad|
|[wGMVflH4t1UXrNn5.htm](heritages/wGMVflH4t1UXrNn5.htm)|Tiefling|auto-trad|
|[wHO5luJMODbDLXNi.htm](heritages/wHO5luJMODbDLXNi.htm)|Bright Fetchling|auto-trad|
|[Wk4HyaZtC1j221i1.htm](heritages/Wk4HyaZtC1j221i1.htm)|Earthly Wilds Kitsune|auto-trad|
|[wn4EbYk1QN3tyFhh.htm](heritages/wn4EbYk1QN3tyFhh.htm)|Deep Orc|auto-trad|
|[wNnsjird4OQe0s6p.htm](heritages/wNnsjird4OQe0s6p.htm)|Gourd Leshy|auto-trad|
|[WxcbLvufI6JBpLt0.htm](heritages/WxcbLvufI6JBpLt0.htm)|Spindly Anadi|auto-trad|
|[x5S4MNQ0aqUmgHcC.htm](heritages/x5S4MNQ0aqUmgHcC.htm)|Vicious Goloma|auto-trad|
|[xaTTN5anLEBzWCzv.htm](heritages/xaTTN5anLEBzWCzv.htm)|Windweb Grippli|auto-trad|
|[XeXWsvcWU3Zaj5WC.htm](heritages/XeXWsvcWU3Zaj5WC.htm)|Chameleon Gnome|auto-trad|
|[xtRIYizCjLg9qe1Z.htm](heritages/xtRIYizCjLg9qe1Z.htm)|Wildwood Halfling|auto-trad|
|[y24ykEUfpIu5Gp6D.htm](heritages/y24ykEUfpIu5Gp6D.htm)|Warrior Automaton|auto-trad|
|[yL6944LrPo2HNdEJ.htm](heritages/yL6944LrPo2HNdEJ.htm)|Ancient-Blooded Dwarf|auto-trad|
|[yVtcyAbLmWCIHHZi.htm](heritages/yVtcyAbLmWCIHHZi.htm)|Rite of Passage|auto-trad|
|[ywNXVLZtwrAStyh1.htm](heritages/ywNXVLZtwrAStyh1.htm)|Elusive Vishkanya|auto-trad|
|[z4cvqtpkkAYoFpHa.htm](heritages/z4cvqtpkkAYoFpHa.htm)|Bloodhound Shoony|auto-trad|
|[zcO93E8gAW1tDYKk.htm](heritages/zcO93E8gAW1tDYKk.htm)|Draxie|auto-trad|
|[zPhArF36ZVgLeVUU.htm](heritages/zPhArF36ZVgLeVUU.htm)|Ancient Scale Azarketi|auto-trad|
|[zVf0Hlp5xG0Q7kmc.htm](heritages/zVf0Hlp5xG0Q7kmc.htm)|Skilled Heritage|auto-trad|
|[ZW8GX14n3ZGievK1.htm](heritages/ZW8GX14n3ZGievK1.htm)|Tunnelflood Kobold|auto-trad|
|[ZZKZkeSP5TuT62IA.htm](heritages/ZZKZkeSP5TuT62IA.htm)|Duskwalker|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[L6zfGzLMDLHbZ7VV.htm](heritages/L6zfGzLMDLHbZ7VV.htm)|Fruit Leshy|Fruit Leshy (s), leshys (pl.)|modificada|
|[nW1gi13E62Feto2w.htm](heritages/nW1gi13E62Feto2w.htm)|Woodland Elf|Elfo silvano|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
