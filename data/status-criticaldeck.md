# Estado de la traducción (criticaldeck)

 * **auto-trad**: 106


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0BESWYMP9vEbG3cr.htm](criticaldeck/0BESWYMP9vEbG3cr.htm)|Critical Fumble Deck #9|auto-trad|
|[0tG5q8JkCaG1EY5s.htm](criticaldeck/0tG5q8JkCaG1EY5s.htm)|Critical Fumble Deck #1|auto-trad|
|[1urnmEt4sn4ATDRL.htm](criticaldeck/1urnmEt4sn4ATDRL.htm)|Critical Hit Deck #29|auto-trad|
|[1xTQ3C9aySvCCPXw.htm](criticaldeck/1xTQ3C9aySvCCPXw.htm)|Critical Hit Deck #35|auto-trad|
|[2CCPMTk5DlZslpOp.htm](criticaldeck/2CCPMTk5DlZslpOp.htm)|Critical Hit Deck #18|auto-trad|
|[2PtjynRwj4bBm4rZ.htm](criticaldeck/2PtjynRwj4bBm4rZ.htm)|Critical Fumble Deck #44|auto-trad|
|[3vz4m5O9oyhVGe7t.htm](criticaldeck/3vz4m5O9oyhVGe7t.htm)|Critical Fumble Deck #26|auto-trad|
|[4MkOtc3bBYsBGF97.htm](criticaldeck/4MkOtc3bBYsBGF97.htm)|Critical Fumble Deck #6|auto-trad|
|[642qVWBhXlYwOhED.htm](criticaldeck/642qVWBhXlYwOhED.htm)|Critical Fumble Deck #39|auto-trad|
|[6X9N5ieXqWitEVKw.htm](criticaldeck/6X9N5ieXqWitEVKw.htm)|Critical Fumble Deck #53|auto-trad|
|[7RcrHguk3riysqgZ.htm](criticaldeck/7RcrHguk3riysqgZ.htm)|Critical Hit Deck #40|auto-trad|
|[8D8NPBNMxQftBmyd.htm](criticaldeck/8D8NPBNMxQftBmyd.htm)|Critical Fumble Deck #46|auto-trad|
|[8OoQs1TeJBW5UVby.htm](criticaldeck/8OoQs1TeJBW5UVby.htm)|Critical Hit Deck #21|auto-trad|
|[99BxUbTtFlyPWkq5.htm](criticaldeck/99BxUbTtFlyPWkq5.htm)|Critical Fumble Deck #7|auto-trad|
|[A0gsgMjgDpkOXtYp.htm](criticaldeck/A0gsgMjgDpkOXtYp.htm)|Critical Hit Deck #12|auto-trad|
|[AXZZ59kX1JNpIjKo.htm](criticaldeck/AXZZ59kX1JNpIjKo.htm)|Critical Fumble Deck #24|auto-trad|
|[BgI1SePPP5QW9wSQ.htm](criticaldeck/BgI1SePPP5QW9wSQ.htm)|Critical Hit Deck #27|auto-trad|
|[bZF7zf43UBxS188h.htm](criticaldeck/bZF7zf43UBxS188h.htm)|Critical Hit Deck #48|auto-trad|
|[bzIzGjyNUc52lhCS.htm](criticaldeck/bzIzGjyNUc52lhCS.htm)|Critical Hit Deck #4|auto-trad|
|[cPORMYYrqMaPfjnx.htm](criticaldeck/cPORMYYrqMaPfjnx.htm)|Critical Hit Deck #47|auto-trad|
|[cY3fpFYpmsUK9kKb.htm](criticaldeck/cY3fpFYpmsUK9kKb.htm)|Critical Hit Deck #25|auto-trad|
|[dCiYhvB488kKvpO0.htm](criticaldeck/dCiYhvB488kKvpO0.htm)|Critical Hit Deck #36|auto-trad|
|[DeZIaLR9okv231ek.htm](criticaldeck/DeZIaLR9okv231ek.htm)|Critical Fumble Deck #32|auto-trad|
|[dP4Zy3ILcjWRx6ZO.htm](criticaldeck/dP4Zy3ILcjWRx6ZO.htm)|Critical Hit Deck #45|auto-trad|
|[DpY6k60UrpvKgjx2.htm](criticaldeck/DpY6k60UrpvKgjx2.htm)|Critical Fumble Deck #4|auto-trad|
|[dvL063pJYJpCMsA6.htm](criticaldeck/dvL063pJYJpCMsA6.htm)|Critical Fumble Deck #52|auto-trad|
|[DYaq4QSRuyhp4DQL.htm](criticaldeck/DYaq4QSRuyhp4DQL.htm)|Critical Hit Deck #51|auto-trad|
|[e08YEjoNa60xrAYS.htm](criticaldeck/e08YEjoNa60xrAYS.htm)|Critical Fumble Deck #45|auto-trad|
|[e9imRGVLbIWYVUXf.htm](criticaldeck/e9imRGVLbIWYVUXf.htm)|Critical Hit Deck #1|auto-trad|
|[EaNgK8fG3Fur9uzp.htm](criticaldeck/EaNgK8fG3Fur9uzp.htm)|Critical Hit Deck #17|auto-trad|
|[EP333dUdoJ8xdkzq.htm](criticaldeck/EP333dUdoJ8xdkzq.htm)|Critical Fumble Deck #5|auto-trad|
|[EpWswZucGxXiLNyP.htm](criticaldeck/EpWswZucGxXiLNyP.htm)|Critical Hit Deck #43|auto-trad|
|[eVEZLDyvu2tqiYZn.htm](criticaldeck/eVEZLDyvu2tqiYZn.htm)|Critical Hit Deck #28|auto-trad|
|[f1JjbSLpmPw5McIb.htm](criticaldeck/f1JjbSLpmPw5McIb.htm)|Critical Hit Deck #42|auto-trad|
|[FHnfWQKntBkZJk6G.htm](criticaldeck/FHnfWQKntBkZJk6G.htm)|Critical Hit Deck #15|auto-trad|
|[gdYlgrtfGK8mAoXK.htm](criticaldeck/gdYlgrtfGK8mAoXK.htm)|Critical Hit Deck #9|auto-trad|
|[gLXD4uNnLLtLoeck.htm](criticaldeck/gLXD4uNnLLtLoeck.htm)|Critical Fumble Deck #23|auto-trad|
|[GUtWJynPuVmatqNF.htm](criticaldeck/GUtWJynPuVmatqNF.htm)|Critical Fumble Deck #16|auto-trad|
|[gWa7wGUfUvYoIK8z.htm](criticaldeck/gWa7wGUfUvYoIK8z.htm)|Critical Fumble Deck #20|auto-trad|
|[HERWPcFlzK2FTPLJ.htm](criticaldeck/HERWPcFlzK2FTPLJ.htm)|Critical Fumble Deck #47|auto-trad|
|[I5omhoK6rqfCZiUZ.htm](criticaldeck/I5omhoK6rqfCZiUZ.htm)|Critical Fumble Deck #21|auto-trad|
|[IrUeMJoKEYUSJKs1.htm](criticaldeck/IrUeMJoKEYUSJKs1.htm)|Critical Fumble Deck #19|auto-trad|
|[IvDJkDpe3JxMHaj1.htm](criticaldeck/IvDJkDpe3JxMHaj1.htm)|Critical Fumble Deck #28|auto-trad|
|[ivko8YN075ThpNsb.htm](criticaldeck/ivko8YN075ThpNsb.htm)|Critical Hit Deck #32|auto-trad|
|[iZYZTEtihsVkbRKR.htm](criticaldeck/iZYZTEtihsVkbRKR.htm)|Critical Hit Deck #8|auto-trad|
|[JfnvVoWqEHN1ymd5.htm](criticaldeck/JfnvVoWqEHN1ymd5.htm)|Critical Hit Deck #14|auto-trad|
|[jLnyeFUJ76ZZieVJ.htm](criticaldeck/jLnyeFUJ76ZZieVJ.htm)|Critical Fumble Deck #33|auto-trad|
|[K2U6PVeYrvNagqti.htm](criticaldeck/K2U6PVeYrvNagqti.htm)|Critical Fumble Deck #27|auto-trad|
|[kjoBsS79JcjUUPnr.htm](criticaldeck/kjoBsS79JcjUUPnr.htm)|Critical Fumble Deck #38|auto-trad|
|[kjUiMaSmcOQXxoeo.htm](criticaldeck/kjUiMaSmcOQXxoeo.htm)|Critical Fumble Deck #11|auto-trad|
|[klZe3T5N3T79oQvN.htm](criticaldeck/klZe3T5N3T79oQvN.htm)|Critical Hit Deck #19|auto-trad|
|[kvQc6aNvJaBH6rEC.htm](criticaldeck/kvQc6aNvJaBH6rEC.htm)|Critical Hit Deck #24|auto-trad|
|[lAPaa4MtkDZy6nZN.htm](criticaldeck/lAPaa4MtkDZy6nZN.htm)|Critical Hit Deck #38|auto-trad|
|[LI8WRDVWPbwr6pcG.htm](criticaldeck/LI8WRDVWPbwr6pcG.htm)|Critical Hit Deck #52|auto-trad|
|[lXew3cZFdIYM3NJs.htm](criticaldeck/lXew3cZFdIYM3NJs.htm)|Critical Hit Deck #10|auto-trad|
|[LY6T2YgrSoI79BDR.htm](criticaldeck/LY6T2YgrSoI79BDR.htm)|Critical Fumble Deck #8|auto-trad|
|[maXudTqcDiAj86Ym.htm](criticaldeck/maXudTqcDiAj86Ym.htm)|Critical Hit Deck #6|auto-trad|
|[mcniWnifXwXkZocv.htm](criticaldeck/mcniWnifXwXkZocv.htm)|Critical Fumble Deck #41|auto-trad|
|[mwz1Ukw9laGyRocF.htm](criticaldeck/mwz1Ukw9laGyRocF.htm)|Critical Fumble Deck #51|auto-trad|
|[nI9pM4SeM39SRVaR.htm](criticaldeck/nI9pM4SeM39SRVaR.htm)|Critical Hit Deck #41|auto-trad|
|[NNfsKAHcKvTuyha4.htm](criticaldeck/NNfsKAHcKvTuyha4.htm)|Critical Fumble Deck #36|auto-trad|
|[nSU4nBU3fBTvb2k3.htm](criticaldeck/nSU4nBU3fBTvb2k3.htm)|Critical Hit Deck #20|auto-trad|
|[OvQYM50fG50aOHfR.htm](criticaldeck/OvQYM50fG50aOHfR.htm)|Critical Hit Deck #34|auto-trad|
|[pjR0QGXE58oWS8uU.htm](criticaldeck/pjR0QGXE58oWS8uU.htm)|Critical Hit Deck #7|auto-trad|
|[Ps9VYxI2zJPG6PEV.htm](criticaldeck/Ps9VYxI2zJPG6PEV.htm)|Critical Fumble Deck #22|auto-trad|
|[pzk8lcb4ykgtn97D.htm](criticaldeck/pzk8lcb4ykgtn97D.htm)|Critical Hit Deck #23|auto-trad|
|[QbnfMqilzwyzk0bf.htm](criticaldeck/QbnfMqilzwyzk0bf.htm)|Critical Fumble Deck #34|auto-trad|
|[QCHhZd0F0DmvVxYP.htm](criticaldeck/QCHhZd0F0DmvVxYP.htm)|Critical Fumble Deck #37|auto-trad|
|[qD4ybNUceFacSJGu.htm](criticaldeck/qD4ybNUceFacSJGu.htm)|Critical Fumble Deck #3|auto-trad|
|[qD8KoshDJsayGbCB.htm](criticaldeck/qD8KoshDJsayGbCB.htm)|Critical Fumble Deck #40|auto-trad|
|[qOmgdhhShYp4vlfi.htm](criticaldeck/qOmgdhhShYp4vlfi.htm)|Critical Hit Deck #3|auto-trad|
|[QR3e1uCNURvxDzbV.htm](criticaldeck/QR3e1uCNURvxDzbV.htm)|Critical Hit Deck #50|auto-trad|
|[QuhotSkof0ao2kJo.htm](criticaldeck/QuhotSkof0ao2kJo.htm)|Critical Fumble Deck #50|auto-trad|
|[Rkv1BGGapMuMyMHs.htm](criticaldeck/Rkv1BGGapMuMyMHs.htm)|Critical Hit Deck #37|auto-trad|
|[rLNl2PdawSadogJd.htm](criticaldeck/rLNl2PdawSadogJd.htm)|Critical Hit Deck #2|auto-trad|
|[RZAYqAHzFxnbD01e.htm](criticaldeck/RZAYqAHzFxnbD01e.htm)|Critical Fumble Deck #30|auto-trad|
|[SQAqU3m7BXHm8TGC.htm](criticaldeck/SQAqU3m7BXHm8TGC.htm)|Critical Hit Deck #39|auto-trad|
|[TbXmAqEzgW1nZV2r.htm](criticaldeck/TbXmAqEzgW1nZV2r.htm)|Critical Fumble Deck #49|auto-trad|
|[tHmyeunsbv4TZAbw.htm](criticaldeck/tHmyeunsbv4TZAbw.htm)|Critical Fumble Deck #17|auto-trad|
|[Toz2ZIpy7N4pDmyi.htm](criticaldeck/Toz2ZIpy7N4pDmyi.htm)|Critical Fumble Deck #18|auto-trad|
|[txOOwTfhpIJj4LU1.htm](criticaldeck/txOOwTfhpIJj4LU1.htm)|Critical Fumble Deck #12|auto-trad|
|[TYTNFtVvFqFBM8oy.htm](criticaldeck/TYTNFtVvFqFBM8oy.htm)|Critical Fumble Deck #43|auto-trad|
|[UheptedVuKxVHSQY.htm](criticaldeck/UheptedVuKxVHSQY.htm)|Critical Fumble Deck #25|auto-trad|
|[unEu0FFXZfTtvF2b.htm](criticaldeck/unEu0FFXZfTtvF2b.htm)|Critical Hit Deck #26|auto-trad|
|[v7aUL4DIk7lkw1EO.htm](criticaldeck/v7aUL4DIk7lkw1EO.htm)|Critical Hit Deck #44|auto-trad|
|[vCvtsUB6C6PEdvog.htm](criticaldeck/vCvtsUB6C6PEdvog.htm)|Critical Hit Deck #11|auto-trad|
|[VfijeX07Jy9QPYPb.htm](criticaldeck/VfijeX07Jy9QPYPb.htm)|Critical Fumble Deck #14|auto-trad|
|[vFXt9SSkNst7qD6v.htm](criticaldeck/vFXt9SSkNst7qD6v.htm)|Critical Fumble Deck #13|auto-trad|
|[vXPWnWa7B1qsrOMa.htm](criticaldeck/vXPWnWa7B1qsrOMa.htm)|Critical Hit Deck #5|auto-trad|
|[wcqXFhggTcAutBk6.htm](criticaldeck/wcqXFhggTcAutBk6.htm)|Critical Fumble Deck #29|auto-trad|
|[wCsoyffDxSWQ2c8o.htm](criticaldeck/wCsoyffDxSWQ2c8o.htm)|Critical Hit Deck #46|auto-trad|
|[wemXkXGY7sl0tUQR.htm](criticaldeck/wemXkXGY7sl0tUQR.htm)|Critical Hit Deck #16|auto-trad|
|[wga13mR4vo73tbTY.htm](criticaldeck/wga13mR4vo73tbTY.htm)|Critical Fumble Deck #10|auto-trad|
|[WGBL7l5DatQnszg7.htm](criticaldeck/WGBL7l5DatQnszg7.htm)|Critical Fumble Deck #35|auto-trad|
|[wHwwHq9RcOUvwFeB.htm](criticaldeck/wHwwHq9RcOUvwFeB.htm)|Critical Hit Deck #22|auto-trad|
|[Wn47ZhXINo7jz8JZ.htm](criticaldeck/Wn47ZhXINo7jz8JZ.htm)|Critical Fumble Deck #2|auto-trad|
|[x4850Hq18S9qYGed.htm](criticaldeck/x4850Hq18S9qYGed.htm)|Critical Hit Deck #31|auto-trad|
|[XhX99wCRrwB1JFnl.htm](criticaldeck/XhX99wCRrwB1JFnl.htm)|Critical Hit Deck #33|auto-trad|
|[XmpWko9I3NDN6rjJ.htm](criticaldeck/XmpWko9I3NDN6rjJ.htm)|Critical Hit Deck #53|auto-trad|
|[xsBlA35rvfHSzJe4.htm](criticaldeck/xsBlA35rvfHSzJe4.htm)|Critical Fumble Deck #15|auto-trad|
|[y4EgjWn5MDIQC7JR.htm](criticaldeck/y4EgjWn5MDIQC7JR.htm)|Critical Hit Deck #30|auto-trad|
|[Y4J49n6WFOEXwaf2.htm](criticaldeck/Y4J49n6WFOEXwaf2.htm)|Critical Hit Deck #13|auto-trad|
|[yBE9e9l5KW7SdHZH.htm](criticaldeck/yBE9e9l5KW7SdHZH.htm)|Critical Fumble Deck #31|auto-trad|
|[YcQEfWxkmpMWXUz4.htm](criticaldeck/YcQEfWxkmpMWXUz4.htm)|Critical Hit Deck #49|auto-trad|
|[YRY6c3WdBEuKiOCv.htm](criticaldeck/YRY6c3WdBEuKiOCv.htm)|Critical Fumble Deck #42|auto-trad|
|[Zx67JiRVzstgEa32.htm](criticaldeck/Zx67JiRVzstgEa32.htm)|Critical Fumble Deck #48|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
