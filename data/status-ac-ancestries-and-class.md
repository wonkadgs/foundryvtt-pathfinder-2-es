# Estado de la traducción (ac-ancestries-and-class)

 * **auto-trad**: 49


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0BVoHy7R86KJoani.htm](ac-ancestries-and-class/0BVoHy7R86KJoani.htm)|Monitor Lizard|auto-trad|
|[2DHIdNZsWW0QMktP.htm](ac-ancestries-and-class/2DHIdNZsWW0QMktP.htm)|Legchair|auto-trad|
|[2nVLooSZdV9tJw9E.htm](ac-ancestries-and-class/2nVLooSZdV9tJw9E.htm)|Ghost|auto-trad|
|[5mO4H9Etz6OZDXDp.htm](ac-ancestries-and-class/5mO4H9Etz6OZDXDp.htm)|Bird|auto-trad|
|[6dNZ1K8X9OgK8aCL.htm](ac-ancestries-and-class/6dNZ1K8X9OgK8aCL.htm)|Rhinoceros|auto-trad|
|[6r0IkniXbYu0bPdi.htm](ac-ancestries-and-class/6r0IkniXbYu0bPdi.htm)|Pangolin|auto-trad|
|[8an4Nsuh2Md5wWHI.htm](ac-ancestries-and-class/8an4Nsuh2Md5wWHI.htm)|Undead Hand|auto-trad|
|[8gSeDqJLLQRlQWis.htm](ac-ancestries-and-class/8gSeDqJLLQRlQWis.htm)|Horse|auto-trad|
|[8z71PBlZBSXkKkLm.htm](ac-ancestries-and-class/8z71PBlZBSXkKkLm.htm)|Cave Gecko|auto-trad|
|[9T6H4GbqxMm44Acv.htm](ac-ancestries-and-class/9T6H4GbqxMm44Acv.htm)|Crocodile|auto-trad|
|[a9luOko3LqrhIe2k.htm](ac-ancestries-and-class/a9luOko3LqrhIe2k.htm)|Vulture|auto-trad|
|[dUAqUKIdieoFVHRp.htm](ac-ancestries-and-class/dUAqUKIdieoFVHRp.htm)|Fiery Leopard|auto-trad|
|[DUnwMSEutz35cCvB.htm](ac-ancestries-and-class/DUnwMSEutz35cCvB.htm)|Skeletal Mount|auto-trad|
|[eBgMfYf0PVbsGOYp.htm](ac-ancestries-and-class/eBgMfYf0PVbsGOYp.htm)|Bear|auto-trad|
|[efkFqs6LXzWkawfL.htm](ac-ancestries-and-class/efkFqs6LXzWkawfL.htm)|Hyena|auto-trad|
|[eVcgERbL1hyeqCaW.htm](ac-ancestries-and-class/eVcgERbL1hyeqCaW.htm)|Skeletal Servant|auto-trad|
|[hynwj4kRnb7yLNGu.htm](ac-ancestries-and-class/hynwj4kRnb7yLNGu.htm)|Vampiric Animal|auto-trad|
|[i2uYnClqjYI8YVun.htm](ac-ancestries-and-class/i2uYnClqjYI8YVun.htm)|Elephant|auto-trad|
|[ikbae8cDXga00PbG.htm](ac-ancestries-and-class/ikbae8cDXga00PbG.htm)|Zombie Carrion Bird|auto-trad|
|[jEK6PPGWmaTFAEKr.htm](ac-ancestries-and-class/jEK6PPGWmaTFAEKr.htm)|Camel|auto-trad|
|[jTRsRVhl5NswEueA.htm](ac-ancestries-and-class/jTRsRVhl5NswEueA.htm)|Boar|auto-trad|
|[jUYTm3wUR0GX1zfO.htm](ac-ancestries-and-class/jUYTm3wUR0GX1zfO.htm)|Ulgrem-Lurann|auto-trad|
|[kaE0e2oLPj4S3SdA.htm](ac-ancestries-and-class/kaE0e2oLPj4S3SdA.htm)|Blood Wolf|auto-trad|
|[kbPrh3KBomvQcLOn.htm](ac-ancestries-and-class/kbPrh3KBomvQcLOn.htm)|Shadow Hound|auto-trad|
|[kKbXAP5Vk6iYC98b.htm](ac-ancestries-and-class/kKbXAP5Vk6iYC98b.htm)|Dromaeosaur|auto-trad|
|[KMHnOhuO57vnUHZH.htm](ac-ancestries-and-class/KMHnOhuO57vnUHZH.htm)|Water Wraith|auto-trad|
|[lOMF4UyhjZcL7HM9.htm](ac-ancestries-and-class/lOMF4UyhjZcL7HM9.htm)|Badger|auto-trad|
|[lOzvLCCy9QRnYw6w.htm](ac-ancestries-and-class/lOzvLCCy9QRnYw6w.htm)|Cat|auto-trad|
|[Mld98WoM7R1wOQtX.htm](ac-ancestries-and-class/Mld98WoM7R1wOQtX.htm)|Zombie Mount|auto-trad|
|[MpqwqLf7AaLOfyZh.htm](ac-ancestries-and-class/MpqwqLf7AaLOfyZh.htm)|Snake|auto-trad|
|[Nab2afki863WAtBG.htm](ac-ancestries-and-class/Nab2afki863WAtBG.htm)|Tyrannosaurus|auto-trad|
|[O50K7MUf0Xw8wHd6.htm](ac-ancestries-and-class/O50K7MUf0Xw8wHd6.htm)|Cave Pterosaur|auto-trad|
|[oB8ylf56HRtXE9yq.htm](ac-ancestries-and-class/oB8ylf56HRtXE9yq.htm)|Shark|auto-trad|
|[pAiHiUC9NfbndLNG.htm](ac-ancestries-and-class/pAiHiUC9NfbndLNG.htm)|Riding Drake|auto-trad|
|[QJghnxvSitRl5ubE.htm](ac-ancestries-and-class/QJghnxvSitRl5ubE.htm)|Gibtas|auto-trad|
|[qLvwY3aKGHVrbgfk.htm](ac-ancestries-and-class/qLvwY3aKGHVrbgfk.htm)|Ape|auto-trad|
|[Qwm9g6hS7f45sAIj.htm](ac-ancestries-and-class/Qwm9g6hS7f45sAIj.htm)|Bat|auto-trad|
|[SMYGh6WPjKIMAmXc.htm](ac-ancestries-and-class/SMYGh6WPjKIMAmXc.htm)|Moth|auto-trad|
|[tT2lEqAPTM2u1hNV.htm](ac-ancestries-and-class/tT2lEqAPTM2u1hNV.htm)|Scorpion|auto-trad|
|[UwzROlMRrkPDrWjk.htm](ac-ancestries-and-class/UwzROlMRrkPDrWjk.htm)|Triceratops|auto-trad|
|[vbBkKVl9tgnVasFU.htm](ac-ancestries-and-class/vbBkKVl9tgnVasFU.htm)|Wolf|auto-trad|
|[vvlTOzxghrlewzB0.htm](ac-ancestries-and-class/vvlTOzxghrlewzB0.htm)|Skeletal Bird of Prey|auto-trad|
|[wcS3dKMoIxc77rtp.htm](ac-ancestries-and-class/wcS3dKMoIxc77rtp.htm)|Terror Bird|auto-trad|
|[WthugGo14WNxa3L5.htm](ac-ancestries-and-class/WthugGo14WNxa3L5.htm)|Skeletal Constrictor|auto-trad|
|[WwQVAQTnry5vfGbC.htm](ac-ancestries-and-class/WwQVAQTnry5vfGbC.htm)|Ankylosaurus|auto-trad|
|[wZlIF2VpF2OO3hQR.htm](ac-ancestries-and-class/wZlIF2VpF2OO3hQR.htm)|Capybara|auto-trad|
|[X9YDnJdJfEPDk7l2.htm](ac-ancestries-and-class/X9YDnJdJfEPDk7l2.htm)|Arboreal Sapling|auto-trad|
|[xiSoZz2JHX095nHo.htm](ac-ancestries-and-class/xiSoZz2JHX095nHo.htm)|Zombie|auto-trad|
|[Yixn2Jz0f3eT1Wkt.htm](ac-ancestries-and-class/Yixn2Jz0f3eT1Wkt.htm)|Beetle|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
