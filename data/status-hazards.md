# Estado de la traducción (hazards)

 * **auto-trad**: 52
 * **modificada**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0dg9YrjsDi6Ap3jF.htm](hazards/0dg9YrjsDi6Ap3jF.htm)|Web Lurker Deadfall|auto-trad|
|[1egK7HFAFdHxMS9N.htm](hazards/1egK7HFAFdHxMS9N.htm)|Sportlebore|auto-trad|
|[2GAOUxDfoA48uCWP.htm](hazards/2GAOUxDfoA48uCWP.htm)|Fireball Rune|auto-trad|
|[491qhVbjsHnOuMZW.htm](hazards/491qhVbjsHnOuMZW.htm)|Electric Latch Rune|auto-trad|
|[4jAJh3cTLIgkqtou.htm](hazards/4jAJh3cTLIgkqtou.htm)|Malevolent Mannequins|auto-trad|
|[4jJxLlimyQaIVXlt.htm](hazards/4jJxLlimyQaIVXlt.htm)|Darkside Mirror|auto-trad|
|[4O7wKZdeAemTEbvG.htm](hazards/4O7wKZdeAemTEbvG.htm)|Slamming Door|auto-trad|
|[6In2S3lDnxNgZ2np.htm](hazards/6In2S3lDnxNgZ2np.htm)|Titanic Flytrap|auto-trad|
|[7VqibTAEXXX6PIhh.htm](hazards/7VqibTAEXXX6PIhh.htm)|Scythe Blades|auto-trad|
|[8ewUvJlvn6LVjoXJ.htm](hazards/8ewUvJlvn6LVjoXJ.htm)|Second Chance|auto-trad|
|[8gAoSgBJN8QqzP1R.htm](hazards/8gAoSgBJN8QqzP1R.htm)|Frozen Moment|auto-trad|
|[98rS64gLzy1ReXoR.htm](hazards/98rS64gLzy1ReXoR.htm)|Hampering Web|auto-trad|
|[A93flWUsot3FmC7t.htm](hazards/A93flWUsot3FmC7t.htm)|Yellow Mold|auto-trad|
|[AM3YY2Zfe2ChJHd7.htm](hazards/AM3YY2Zfe2ChJHd7.htm)|Telekinetic Swarm Trap|auto-trad|
|[BHq5wpQU8hQEke8D.htm](hazards/BHq5wpQU8hQEke8D.htm)|Hidden Pit|auto-trad|
|[BsZ6o2YrwVovKfNh.htm](hazards/BsZ6o2YrwVovKfNh.htm)|Armageddon Orb|auto-trad|
|[C6nFe8SCWJ8FmLOT.htm](hazards/C6nFe8SCWJ8FmLOT.htm)|Quicksand|auto-trad|
|[d3YklujLpBFC5HfB.htm](hazards/d3YklujLpBFC5HfB.htm)|Ghostly Choir|auto-trad|
|[gB9WkJtH88jJQa5Z.htm](hazards/gB9WkJtH88jJQa5Z.htm)|Plummeting Doom|auto-trad|
|[gFt2nzQrVgXM9tmJ.htm](hazards/gFt2nzQrVgXM9tmJ.htm)|Treacherous Scree|auto-trad|
|[H2GX04CQXLPQHT8h.htm](hazards/H2GX04CQXLPQHT8h.htm)|Wheel Of Misery|auto-trad|
|[H8CPGJn81JSTCRNx.htm](hazards/H8CPGJn81JSTCRNx.htm)|Polymorph Trap|auto-trad|
|[HnPd9Vqh5NHKEdRq.htm](hazards/HnPd9Vqh5NHKEdRq.htm)|Spinning Blade Pillar|auto-trad|
|[inUlZWE1isqnTRc5.htm](hazards/inUlZWE1isqnTRc5.htm)|Jealous Abjurer|auto-trad|
|[J4YChuob7MIPT5Mq.htm](hazards/J4YChuob7MIPT5Mq.htm)|Vorpal Executioner|auto-trad|
|[lgyGhpyPosriQUzE.htm](hazards/lgyGhpyPosriQUzE.htm)|Perilous Flash Flood|auto-trad|
|[LLPsEKLoVmoPleJS.htm](hazards/LLPsEKLoVmoPleJS.htm)|Green Slime|auto-trad|
|[lVqVDjXnHboMif7F.htm](hazards/lVqVDjXnHboMif7F.htm)|Poisoned Dart Gallery|auto-trad|
|[m4PRYxFq9ojcwesh.htm](hazards/m4PRYxFq9ojcwesh.htm)|Pharaoh's Ward|auto-trad|
|[mMXHyWdmmAN0GPvG.htm](hazards/mMXHyWdmmAN0GPvG.htm)|Banshee's Symphony|auto-trad|
|[mWhmhYBHH9X1Ebb9.htm](hazards/mWhmhYBHH9X1Ebb9.htm)|Insistent Privacy Fence|auto-trad|
|[nO4osrBRnpWKFCMP.htm](hazards/nO4osrBRnpWKFCMP.htm)|Summoning Rune|auto-trad|
|[O0qA1ElCOgYGEBtL.htm](hazards/O0qA1ElCOgYGEBtL.htm)|Eternal Flame|auto-trad|
|[OekigjNLNp9XENjx.htm](hazards/OekigjNLNp9XENjx.htm)|Drowning Pit|auto-trad|
|[oNLgR1iq6MVvNRWo.htm](hazards/oNLgR1iq6MVvNRWo.htm)|Lava Flume Tube|auto-trad|
|[Or0jjL8xS3GyiMq0.htm](hazards/Or0jjL8xS3GyiMq0.htm)|Web Lurker Noose|auto-trad|
|[OSPYSuckHhHl4Cr9.htm](hazards/OSPYSuckHhHl4Cr9.htm)|Steam Vents|auto-trad|
|[Q8bXKgDm8eguqThB.htm](hazards/Q8bXKgDm8eguqThB.htm)|Brown Mold|auto-trad|
|[sFaAWmy1szDRmFtk.htm](hazards/sFaAWmy1szDRmFtk.htm)|Confounding Betrayal|auto-trad|
|[siylw0zIh1g4VnCW.htm](hazards/siylw0zIh1g4VnCW.htm)|Grasp Of The Damned|auto-trad|
|[su4TRBOoso4vjkoK.htm](hazards/su4TRBOoso4vjkoK.htm)|Bloodthirsty Urge|auto-trad|
|[tbwGr6FIr5WpvQ6l.htm](hazards/tbwGr6FIr5WpvQ6l.htm)|Hammer Of Forbiddance|auto-trad|
|[uEZ4Jv2wNyukJTRL.htm](hazards/uEZ4Jv2wNyukJTRL.htm)|Hallucination Powder Trap|auto-trad|
|[Uw2iVgbbeyn3mOjt.htm](hazards/Uw2iVgbbeyn3mOjt.htm)|Spectral Reflection|auto-trad|
|[v2xIxZ9ZZ6fJyATF.htm](hazards/v2xIxZ9ZZ6fJyATF.htm)|Poisoned Lock|auto-trad|
|[VA4VL3kVUxBYbwRf.htm](hazards/VA4VL3kVUxBYbwRf.htm)|Snowfall|auto-trad|
|[vlMuFskctUvjJe8X.htm](hazards/vlMuFskctUvjJe8X.htm)|Spear Launcher|auto-trad|
|[vTdWEBzJzltMM6r4.htm](hazards/vTdWEBzJzltMM6r4.htm)|Shrieker|auto-trad|
|[wHUIZ5QhG37cRzSV.htm](hazards/wHUIZ5QhG37cRzSV.htm)|Gravehall Trap|auto-trad|
|[xkqjwu1ox0pQLOnb.htm](hazards/xkqjwu1ox0pQLOnb.htm)|Bottomless Pit|auto-trad|
|[yM4G2LvMwvkIRx0G.htm](hazards/yM4G2LvMwvkIRx0G.htm)|Planar Rift|auto-trad|
|[ZUCGvc2dTJUlM9dC.htm](hazards/ZUCGvc2dTJUlM9dC.htm)|Dance Of Death|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[g9HovYB4pfHgIML9.htm](hazards/g9HovYB4pfHgIML9.htm)|Flensing Blades|Flensing Blades|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
