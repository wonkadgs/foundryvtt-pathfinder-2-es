# Estado de la traducción (macros)

 * **auto-trad**: 9


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[084MOWWSEVNwpHVG.htm](macros/084MOWWSEVNwpHVG.htm)|Perception for Selected Tokens|auto-trad|
|[0GU2sdy3r2MeC56x.htm](macros/0GU2sdy3r2MeC56x.htm)|Rest for the Night|auto-trad|
|[6duZj0Ygiqv712rq.htm](macros/6duZj0Ygiqv712rq.htm)|Treat Wounds|auto-trad|
|[aS6F7PSUlS9JM5jr.htm](macros/aS6F7PSUlS9JM5jr.htm)|Take a Breather|auto-trad|
|[MAHxEeGf31wqv3jp.htm](macros/MAHxEeGf31wqv3jp.htm)|XP|auto-trad|
|[mxHKWibjPrgfJTDg.htm](macros/mxHKWibjPrgfJTDg.htm)|Earn Income|auto-trad|
|[NQkc5rKoeFemdVHr.htm](macros/NQkc5rKoeFemdVHr.htm)|Travel Duration|auto-trad|
|[s2sa8lo9dcIA6UGe.htm](macros/s2sa8lo9dcIA6UGe.htm)|Toggle Compendium Browser|auto-trad|
|[yBuEphSaJJ7V9Yw3.htm](macros/yBuEphSaJJ7V9Yw3.htm)|Stealth for Selected Tokens|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
