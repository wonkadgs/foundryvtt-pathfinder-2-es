# Estado de la traducción (book-of-the-dead-bestiary)

 * **auto-trad**: 116


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0PrrwvV1936eSCQy.htm](book-of-the-dead-bestiary/0PrrwvV1936eSCQy.htm)|Tormented (Burning)|auto-trad|
|[1854X0YrbGyTnhCy.htm](book-of-the-dead-bestiary/1854X0YrbGyTnhCy.htm)|Sadistic Conductor|auto-trad|
|[1hdQjYLtup92Jlls.htm](book-of-the-dead-bestiary/1hdQjYLtup92Jlls.htm)|Obrousian|auto-trad|
|[27NxweHr9rB1hDCn.htm](book-of-the-dead-bestiary/27NxweHr9rB1hDCn.htm)|Deathless Acolyte Of Urgathoa|auto-trad|
|[3CdslCv2CMP7O12F.htm](book-of-the-dead-bestiary/3CdslCv2CMP7O12F.htm)|Mummified Cat|auto-trad|
|[3pQvzrGhxO3bUwYT.htm](book-of-the-dead-bestiary/3pQvzrGhxO3bUwYT.htm)|Iruxi Ossature|auto-trad|
|[40P14vZZXKLhBD6q.htm](book-of-the-dead-bestiary/40P14vZZXKLhBD6q.htm)|Sykever|auto-trad|
|[5ukWvQqK17BQl31O.htm](book-of-the-dead-bestiary/5ukWvQqK17BQl31O.htm)|Lacedon|auto-trad|
|[7JWjoMGf7f7PZgSD.htm](book-of-the-dead-bestiary/7JWjoMGf7f7PZgSD.htm)|Ice Mummy|auto-trad|
|[7pk1DLc96LMcHBB5.htm](book-of-the-dead-bestiary/7pk1DLc96LMcHBB5.htm)|Vanyver|auto-trad|
|[7WqlOvjoqURmeorA.htm](book-of-the-dead-bestiary/7WqlOvjoqURmeorA.htm)|Death Coach|auto-trad|
|[8jd6xVMgAeQEfect.htm](book-of-the-dead-bestiary/8jd6xVMgAeQEfect.htm)|Nasurgeth|auto-trad|
|[8o27tdIZFp0eTs5N.htm](book-of-the-dead-bestiary/8o27tdIZFp0eTs5N.htm)|Zombie Lord|auto-trad|
|[8qB0gj8salw8746I.htm](book-of-the-dead-bestiary/8qB0gj8salw8746I.htm)|Mummy Prophet Of Set|auto-trad|
|[8tX1seLMsXto5Kni.htm](book-of-the-dead-bestiary/8tX1seLMsXto5Kni.htm)|Ectoplasmic Grasp|auto-trad|
|[A5GOni2mXNhVsdRg.htm](book-of-the-dead-bestiary/A5GOni2mXNhVsdRg.htm)|Faithless Ecclesiarch|auto-trad|
|[abnTemoEnsqZPbKZ.htm](book-of-the-dead-bestiary/abnTemoEnsqZPbKZ.htm)|Last Guard|auto-trad|
|[avBb4LvR3TsYpXma.htm](book-of-the-dead-bestiary/avBb4LvR3TsYpXma.htm)|Queen Sluagh|auto-trad|
|[AXp5EzgP7p8FrYpN.htm](book-of-the-dead-bestiary/AXp5EzgP7p8FrYpN.htm)|Tormented (Crushing)|auto-trad|
|[ayCK1wCQY7mCCyxh.htm](book-of-the-dead-bestiary/ayCK1wCQY7mCCyxh.htm)|Locking Door|auto-trad|
|[bckYmdaq03CUDdc5.htm](book-of-the-dead-bestiary/bckYmdaq03CUDdc5.htm)|Gholdako|auto-trad|
|[c6qRZuHQ7RHJEAtj.htm](book-of-the-dead-bestiary/c6qRZuHQ7RHJEAtj.htm)|Grappling Spirit|auto-trad|
|[cEJh8SY1YDULyhI9.htm](book-of-the-dead-bestiary/cEJh8SY1YDULyhI9.htm)|Relictner Eroder|auto-trad|
|[Cl1SZNiURw65rz4p.htm](book-of-the-dead-bestiary/Cl1SZNiURw65rz4p.htm)|Silent Stalker|auto-trad|
|[dcLHA7tihCF2Mraj.htm](book-of-the-dead-bestiary/dcLHA7tihCF2Mraj.htm)|Tormented (Drowning)|auto-trad|
|[dgQ9VMuC63T5LW5h.htm](book-of-the-dead-bestiary/dgQ9VMuC63T5LW5h.htm)|Weight of Guilt|auto-trad|
|[dKkHFA4aBgk82QJO.htm](book-of-the-dead-bestiary/dKkHFA4aBgk82QJO.htm)|Skeletal Soldier|auto-trad|
|[dlizffh8cFUFOhUf.htm](book-of-the-dead-bestiary/dlizffh8cFUFOhUf.htm)|Jitterbone Contortionist|auto-trad|
|[DMZo6DXmScDXJpJd.htm](book-of-the-dead-bestiary/DMZo6DXmScDXJpJd.htm)|Hunter Wight|auto-trad|
|[DUFaigFhqeKLbrMG.htm](book-of-the-dead-bestiary/DUFaigFhqeKLbrMG.htm)|Grasping Dead|auto-trad|
|[DVpP5ObHDoT0OULK.htm](book-of-the-dead-bestiary/DVpP5ObHDoT0OULK.htm)|Prowler Wight|auto-trad|
|[e49MDE5dJQ1XFq3O.htm](book-of-the-dead-bestiary/e49MDE5dJQ1XFq3O.htm)|Predatory Rabbit|auto-trad|
|[EDhme1IKgO34NDrt.htm](book-of-the-dead-bestiary/EDhme1IKgO34NDrt.htm)|Festering Gnasher|auto-trad|
|[eEE3lRhXCjGzlRLJ.htm](book-of-the-dead-bestiary/eEE3lRhXCjGzlRLJ.htm)|Shredskin|auto-trad|
|[eLAoo8l1I3OiibwW.htm](book-of-the-dead-bestiary/eLAoo8l1I3OiibwW.htm)|Cannibalistic Echoes|auto-trad|
|[Ez8wXPDKOzxvxnqS.htm](book-of-the-dead-bestiary/Ez8wXPDKOzxvxnqS.htm)|Blood-Soaked Soil|auto-trad|
|[f2dAjBXK55w6rnsh.htm](book-of-the-dead-bestiary/f2dAjBXK55w6rnsh.htm)|Tormented (Dislocation)|auto-trad|
|[FVXCVh3Y0LdfoIC5.htm](book-of-the-dead-bestiary/FVXCVh3Y0LdfoIC5.htm)|Siphoning Spirit|auto-trad|
|[GabMKY8QJOulyqAr.htm](book-of-the-dead-bestiary/GabMKY8QJOulyqAr.htm)|Ghost Stampede|auto-trad|
|[GcHzyaMYK5QeKUyM.htm](book-of-the-dead-bestiary/GcHzyaMYK5QeKUyM.htm)|Pale Stranger|auto-trad|
|[gXo04F7O4pwOY698.htm](book-of-the-dead-bestiary/gXo04F7O4pwOY698.htm)|Gallowdead|auto-trad|
|[h0OtQsMR4OqnYatd.htm](book-of-the-dead-bestiary/h0OtQsMR4OqnYatd.htm)|Deathless Hierophant Of Urgathoa|auto-trad|
|[hDQmEtisrgRmufUW.htm](book-of-the-dead-bestiary/hDQmEtisrgRmufUW.htm)|Fluxwraith|auto-trad|
|[hhoSyH9QthtvFptC.htm](book-of-the-dead-bestiary/hhoSyH9QthtvFptC.htm)|Siabrae|auto-trad|
|[hjBcN7ulP6FSK7ie.htm](book-of-the-dead-bestiary/hjBcN7ulP6FSK7ie.htm)|Tormented (Starvation)|auto-trad|
|[I2XdTFyxnnRdmWsi.htm](book-of-the-dead-bestiary/I2XdTFyxnnRdmWsi.htm)|Toppling Furniture|auto-trad|
|[iEQOUQk1wVHFsajW.htm](book-of-the-dead-bestiary/iEQOUQk1wVHFsajW.htm)|Geist|auto-trad|
|[iL2BkoRa6Zrvg1wN.htm](book-of-the-dead-bestiary/iL2BkoRa6Zrvg1wN.htm)|Desperate Hunger|auto-trad|
|[ipVQuGff2OeTVwFK.htm](book-of-the-dead-bestiary/ipVQuGff2OeTVwFK.htm)|Skeletal Mage|auto-trad|
|[IrzECfuPttgGMbLa.htm](book-of-the-dead-bestiary/IrzECfuPttgGMbLa.htm)|Glimpse Grave|auto-trad|
|[ishwgxZAlNJxNwGE.htm](book-of-the-dead-bestiary/ishwgxZAlNJxNwGE.htm)|Daqqanoenyent|auto-trad|
|[j777BjOqZff6S1v9.htm](book-of-the-dead-bestiary/j777BjOqZff6S1v9.htm)|Bhuta|auto-trad|
|[j88xR2MqqZrmF5Wz.htm](book-of-the-dead-bestiary/j88xR2MqqZrmF5Wz.htm)|Raw Nerve|auto-trad|
|[JazJz2crkoG9koQR.htm](book-of-the-dead-bestiary/JazJz2crkoG9koQR.htm)|Fallen Champion|auto-trad|
|[jIypNMJE7rVYpItG.htm](book-of-the-dead-bestiary/jIypNMJE7rVYpItG.htm)|Fiddling Bones|auto-trad|
|[KhHVStbsPSuPElFI.htm](book-of-the-dead-bestiary/KhHVStbsPSuPElFI.htm)|Excorion|auto-trad|
|[kY8MSttryLjbI5wN.htm](book-of-the-dead-bestiary/kY8MSttryLjbI5wN.htm)|Wight Commander|auto-trad|
|[L0JAq1IGEsjJwTbl.htm](book-of-the-dead-bestiary/L0JAq1IGEsjJwTbl.htm)|Minister Of Tumult|auto-trad|
|[laYufBuih3cT95j4.htm](book-of-the-dead-bestiary/laYufBuih3cT95j4.htm)|Hollow Serpent|auto-trad|
|[lhzNUX83TTYpJuma.htm](book-of-the-dead-bestiary/lhzNUX83TTYpJuma.htm)|Unrisen|auto-trad|
|[LNgHO51cJF4HPL78.htm](book-of-the-dead-bestiary/LNgHO51cJF4HPL78.htm)|Shattered Window|auto-trad|
|[LWLUaSHl8YCZdDMH.htm](book-of-the-dead-bestiary/LWLUaSHl8YCZdDMH.htm)|Vetalarana Emergent|auto-trad|
|[M59XiYnJ4Z3bSwCC.htm](book-of-the-dead-bestiary/M59XiYnJ4Z3bSwCC.htm)|Polong|auto-trad|
|[mDe4WW1pSWXS113j.htm](book-of-the-dead-bestiary/mDe4WW1pSWXS113j.htm)|Flood of Spirits|auto-trad|
|[mL4l2kwPPSMKwzQo.htm](book-of-the-dead-bestiary/mL4l2kwPPSMKwzQo.htm)|Onryo|auto-trad|
|[nbVljZhCnWgGxA18.htm](book-of-the-dead-bestiary/nbVljZhCnWgGxA18.htm)|Zombie Owlbear|auto-trad|
|[NcqruxA82SFvTnD1.htm](book-of-the-dead-bestiary/NcqruxA82SFvTnD1.htm)|Child of Urgathoa|auto-trad|
|[Nzf3AfA46cBiWCwN.htm](book-of-the-dead-bestiary/Nzf3AfA46cBiWCwN.htm)|Vetalarana Manipulator|auto-trad|
|[ol2lji9lH7PXh1uw.htm](book-of-the-dead-bestiary/ol2lji9lH7PXh1uw.htm)|Bone Croupier|auto-trad|
|[OsIjZpdtKmKrrBID.htm](book-of-the-dead-bestiary/OsIjZpdtKmKrrBID.htm)|Priest Of Kabriri|auto-trad|
|[pDA6tCwQ3pE6ji4U.htm](book-of-the-dead-bestiary/pDA6tCwQ3pE6ji4U.htm)|Shadern Immolator|auto-trad|
|[PH4GHPsekbDdyX3j.htm](book-of-the-dead-bestiary/PH4GHPsekbDdyX3j.htm)|Phantom Jailer|auto-trad|
|[Pum2HQMMrFS89JnW.htm](book-of-the-dead-bestiary/Pum2HQMMrFS89JnW.htm)|Entombed Spirit|auto-trad|
|[pzh6N7lfVk5261CV.htm](book-of-the-dead-bestiary/pzh6N7lfVk5261CV.htm)|Spirit Cyclone|auto-trad|
|[qO20so7Mv2pmsLL1.htm](book-of-the-dead-bestiary/qO20so7Mv2pmsLL1.htm)|Husk Zombie|auto-trad|
|[S5dXAmvghYCIrQDe.htm](book-of-the-dead-bestiary/S5dXAmvghYCIrQDe.htm)|Decrepit Mummy|auto-trad|
|[sgeULAtaLvg0Uhyn.htm](book-of-the-dead-bestiary/sgeULAtaLvg0Uhyn.htm)|Cadaverous Rake|auto-trad|
|[sucEX2JFrVevTNjU.htm](book-of-the-dead-bestiary/sucEX2JFrVevTNjU.htm)|Drake Skeleton|auto-trad|
|[T9osQMcC96l4X9lk.htm](book-of-the-dead-bestiary/T9osQMcC96l4X9lk.htm)|Beetle Carapace|auto-trad|
|[tb2nr2ycARadI3Pg.htm](book-of-the-dead-bestiary/tb2nr2ycARadI3Pg.htm)|Iroran Mummy|auto-trad|
|[TipfHGlT5ctn1JV0.htm](book-of-the-dead-bestiary/TipfHGlT5ctn1JV0.htm)|Zombie Mammoth|auto-trad|
|[Tjgk2iMSjSbUnuXO.htm](book-of-the-dead-bestiary/Tjgk2iMSjSbUnuXO.htm)|Cold Spot|auto-trad|
|[ToGAEKKOplpXzaQQ.htm](book-of-the-dead-bestiary/ToGAEKKOplpXzaQQ.htm)|Wolf Skeleton|auto-trad|
|[tqTcM8VqFMyuQ0hY.htm](book-of-the-dead-bestiary/tqTcM8VqFMyuQ0hY.htm)|Final Words|auto-trad|
|[uE6YuCCqU715E1Ay.htm](book-of-the-dead-bestiary/uE6YuCCqU715E1Ay.htm)|Bloodthirsty Toy|auto-trad|
|[UMDBlaViAdaw2lnN.htm](book-of-the-dead-bestiary/UMDBlaViAdaw2lnN.htm)|Ghost Pirate Captain|auto-trad|
|[uqY9TkQTO5n9rCLZ.htm](book-of-the-dead-bestiary/uqY9TkQTO5n9rCLZ.htm)|Llorona|auto-trad|
|[Ur3dzfmvtN7lyPNG.htm](book-of-the-dead-bestiary/Ur3dzfmvtN7lyPNG.htm)|Taunting Skull|auto-trad|
|[uURI8Netd0ytD9vc.htm](book-of-the-dead-bestiary/uURI8Netd0ytD9vc.htm)|Sluagh Reaper|auto-trad|
|[vaVIJeQnKFUeaC8K.htm](book-of-the-dead-bestiary/vaVIJeQnKFUeaC8K.htm)|Combusted|auto-trad|
|[vHEJOONP1ERjuxxl.htm](book-of-the-dead-bestiary/vHEJOONP1ERjuxxl.htm)|Urveth|auto-trad|
|[VN2Vz1dxA9ti66bC.htm](book-of-the-dead-bestiary/VN2Vz1dxA9ti66bC.htm)|Tormented (Impalement)|auto-trad|
|[VNego1px6HN8mtUl.htm](book-of-the-dead-bestiary/VNego1px6HN8mtUl.htm)|Phantom Footsteps|auto-trad|
|[vOuiG3tRcr8yL4jh.htm](book-of-the-dead-bestiary/vOuiG3tRcr8yL4jh.htm)|Ghul|auto-trad|
|[VQqdG5PKdCFHXquy.htm](book-of-the-dead-bestiary/VQqdG5PKdCFHXquy.htm)|Disembodied Voices|auto-trad|
|[vvxnRuYBU2uCBPPI.htm](book-of-the-dead-bestiary/vvxnRuYBU2uCBPPI.htm)|Little Man In The Woods|auto-trad|
|[VYLTAIVA9qUsbujo.htm](book-of-the-dead-bestiary/VYLTAIVA9qUsbujo.htm)|Waldgeist|auto-trad|
|[wGEZUOgoJNkgXx9Z.htm](book-of-the-dead-bestiary/wGEZUOgoJNkgXx9Z.htm)|Horde Lich|auto-trad|
|[Wv9iS7JacYjymqlY.htm](book-of-the-dead-bestiary/Wv9iS7JacYjymqlY.htm)|Harlo Krant|auto-trad|
|[XrSjNpkJkBO6ysRG.htm](book-of-the-dead-bestiary/XrSjNpkJkBO6ysRG.htm)|Gashadokuro|auto-trad|
|[XuHQXwAGBdS1s2Kq.htm](book-of-the-dead-bestiary/XuHQXwAGBdS1s2Kq.htm)|Frenetic Musician|auto-trad|
|[xxFiRpQdvbKd0P12.htm](book-of-the-dead-bestiary/xxFiRpQdvbKd0P12.htm)|Hungry Ghost|auto-trad|
|[xyOxzfIUTrN5c8ex.htm](book-of-the-dead-bestiary/xyOxzfIUTrN5c8ex.htm)|Blood Tears|auto-trad|
|[Y2Fiu9T1xwSBPVTV.htm](book-of-the-dead-bestiary/Y2Fiu9T1xwSBPVTV.htm)|Corpseroot|auto-trad|
|[ypOgr8kPy6KqXAAd.htm](book-of-the-dead-bestiary/ypOgr8kPy6KqXAAd.htm)|Seetangeist|auto-trad|
|[Yv9e7NSCzPuhlCnH.htm](book-of-the-dead-bestiary/Yv9e7NSCzPuhlCnH.htm)|Graveknight Warmaster|auto-trad|
|[z3BpUMkUFoXqOFgf.htm](book-of-the-dead-bestiary/z3BpUMkUFoXqOFgf.htm)|Scorned Hound|auto-trad|
|[z72hPIIjIiLIxnor.htm](book-of-the-dead-bestiary/z72hPIIjIiLIxnor.htm)|Withered|auto-trad|
|[z8WJtCyoVHlcSxYy.htm](book-of-the-dead-bestiary/z8WJtCyoVHlcSxYy.htm)|Skeletal Titan|auto-trad|
|[Ze8SKqa4T8lhSDup.htm](book-of-the-dead-bestiary/Ze8SKqa4T8lhSDup.htm)|Pale Sovereign|auto-trad|
|[zOEuwWcvD7sQA1kc.htm](book-of-the-dead-bestiary/zOEuwWcvD7sQA1kc.htm)|Ichor Slinger|auto-trad|
|[zPQZLswJISQrvPb7.htm](book-of-the-dead-bestiary/zPQZLswJISQrvPb7.htm)|Runecarved Lich|auto-trad|
|[zqftTUpxqkdLx2IY.htm](book-of-the-dead-bestiary/zqftTUpxqkdLx2IY.htm)|Ecorche|auto-trad|
|[ZqZlji7aCGCGATMP.htm](book-of-the-dead-bestiary/ZqZlji7aCGCGATMP.htm)|Zombie Snake|auto-trad|
|[ZSrMFLrb5CzFH5WX.htm](book-of-the-dead-bestiary/ZSrMFLrb5CzFH5WX.htm)|Provincial Jiang-shi|auto-trad|
|[ZzKfcOf7CWSZIsAE.htm](book-of-the-dead-bestiary/ZzKfcOf7CWSZIsAE.htm)|Violent Shove|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
