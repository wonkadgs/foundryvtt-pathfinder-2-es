# Estado de la traducción (troubles-in-otari-bestiary-items)

 * **auto-trad**: 78


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[088hXYj7GIHJtlm7.htm](troubles-in-otari-bestiary-items/088hXYj7GIHJtlm7.htm)|Hand Crossbow|auto-trad|
|[0gQuuCA2OyaQRNwR.htm](troubles-in-otari-bestiary-items/0gQuuCA2OyaQRNwR.htm)|Stench|auto-trad|
|[0SzWuQBdiVOEj4WC.htm](troubles-in-otari-bestiary-items/0SzWuQBdiVOEj4WC.htm)|Shattering Urn|auto-trad|
|[19bgEIET0naweDE4.htm](troubles-in-otari-bestiary-items/19bgEIET0naweDE4.htm)|Darkvision|auto-trad|
|[1on1nKLods5yCirm.htm](troubles-in-otari-bestiary-items/1on1nKLods5yCirm.htm)|Sneak Attack|auto-trad|
|[1uUxlDMn7nj3yjAy.htm](troubles-in-otari-bestiary-items/1uUxlDMn7nj3yjAy.htm)|Divine Innate Spells|auto-trad|
|[56wLde8SVEfys89N.htm](troubles-in-otari-bestiary-items/56wLde8SVEfys89N.htm)|Vine|auto-trad|
|[5v6fepRCSyg6ZABC.htm](troubles-in-otari-bestiary-items/5v6fepRCSyg6ZABC.htm)|Darkvision|auto-trad|
|[71j8rIykcKntxLJQ.htm](troubles-in-otari-bestiary-items/71j8rIykcKntxLJQ.htm)|Collapse|auto-trad|
|[73fsCGNUERkE3I1S.htm](troubles-in-otari-bestiary-items/73fsCGNUERkE3I1S.htm)|Hypnotic Stare|auto-trad|
|[742oUZV30i6otpgL.htm](troubles-in-otari-bestiary-items/742oUZV30i6otpgL.htm)|Stinky Leaves|auto-trad|
|[7TD5aX6vNauxTw5T.htm](troubles-in-otari-bestiary-items/7TD5aX6vNauxTw5T.htm)|Arcane Bond|auto-trad|
|[8hva3s7KDSqeUTKh.htm](troubles-in-otari-bestiary-items/8hva3s7KDSqeUTKh.htm)|Dagger|auto-trad|
|[8mPErkHbf2ju6RgN.htm](troubles-in-otari-bestiary-items/8mPErkHbf2ju6RgN.htm)|Ferocity|auto-trad|
|[9d61GjN8ciAoj4Z2.htm](troubles-in-otari-bestiary-items/9d61GjN8ciAoj4Z2.htm)|Low-Light Vision|auto-trad|
|[9JzCRTSPmLBMHyll.htm](troubles-in-otari-bestiary-items/9JzCRTSPmLBMHyll.htm)|Darkvision|auto-trad|
|[9Nve1oYElRWcJhIh.htm](troubles-in-otari-bestiary-items/9Nve1oYElRWcJhIh.htm)|Unburdened Iron|auto-trad|
|[aADQJA0W9vAaOmXT.htm](troubles-in-otari-bestiary-items/aADQJA0W9vAaOmXT.htm)|Maul|auto-trad|
|[ae0LOykLqNxqAXLN.htm](troubles-in-otari-bestiary-items/ae0LOykLqNxqAXLN.htm)|Darkvision|auto-trad|
|[b200ENWSAzXNlSMf.htm](troubles-in-otari-bestiary-items/b200ENWSAzXNlSMf.htm)|Battle Axe|auto-trad|
|[BH97LqHtuqNlighQ.htm](troubles-in-otari-bestiary-items/BH97LqHtuqNlighQ.htm)|Hurried Retreat|auto-trad|
|[c6BnmJR85HoUuFbA.htm](troubles-in-otari-bestiary-items/c6BnmJR85HoUuFbA.htm)|Inflict Pain|auto-trad|
|[ccosYJxMalM6rfN8.htm](troubles-in-otari-bestiary-items/ccosYJxMalM6rfN8.htm)|Spike Trap|auto-trad|
|[d5T3nJCRgqqqQvzD.htm](troubles-in-otari-bestiary-items/d5T3nJCRgqqqQvzD.htm)|Battle Cry|auto-trad|
|[di2qBg3DVoOSKzDk.htm](troubles-in-otari-bestiary-items/di2qBg3DVoOSKzDk.htm)|Unburdened Iron|auto-trad|
|[dwvQyKJ9gUWC4S3f.htm](troubles-in-otari-bestiary-items/dwvQyKJ9gUWC4S3f.htm)|Starknife Attack|auto-trad|
|[eIrvs73CKJq05yZP.htm](troubles-in-otari-bestiary-items/eIrvs73CKJq05yZP.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[Fges35T0kVykR3rl.htm](troubles-in-otari-bestiary-items/Fges35T0kVykR3rl.htm)|Sneak Attack|auto-trad|
|[Fk4R8F6Vr2Lr7mZZ.htm](troubles-in-otari-bestiary-items/Fk4R8F6Vr2Lr7mZZ.htm)|Step into Nightmares|auto-trad|
|[gqjrPshyFW4Y4vrS.htm](troubles-in-otari-bestiary-items/gqjrPshyFW4Y4vrS.htm)|Javelin|auto-trad|
|[H8wipabcGbcu7thF.htm](troubles-in-otari-bestiary-items/H8wipabcGbcu7thF.htm)|Waving Weed|auto-trad|
|[hyIUDH9G8UlF3pne.htm](troubles-in-otari-bestiary-items/hyIUDH9G8UlF3pne.htm)|Knockdown|auto-trad|
|[I0AZeY12ooiFLuuW.htm](troubles-in-otari-bestiary-items/I0AZeY12ooiFLuuW.htm)|Powerful Shove|auto-trad|
|[I6Vb3uZRlv6pkiNj.htm](troubles-in-otari-bestiary-items/I6Vb3uZRlv6pkiNj.htm)|Unbalancing Blow|auto-trad|
|[i74S7XGGuuWnr88J.htm](troubles-in-otari-bestiary-items/i74S7XGGuuWnr88J.htm)|Mauler|auto-trad|
|[Ihvi8o6Gm4pwbhph.htm](troubles-in-otari-bestiary-items/Ihvi8o6Gm4pwbhph.htm)|Fangs|auto-trad|
|[iIaK3retIHJkXgrh.htm](troubles-in-otari-bestiary-items/iIaK3retIHJkXgrh.htm)|Bushwhack|auto-trad|
|[Iq79xetoAoYDXkjs.htm](troubles-in-otari-bestiary-items/Iq79xetoAoYDXkjs.htm)|Darkvision|auto-trad|
|[J08BqvSNQN7R2E9v.htm](troubles-in-otari-bestiary-items/J08BqvSNQN7R2E9v.htm)|Ferocity|auto-trad|
|[k5kr2ILvlJNg5oKb.htm](troubles-in-otari-bestiary-items/k5kr2ILvlJNg5oKb.htm)|Darkvision|auto-trad|
|[l8f0B8czvLKLsc06.htm](troubles-in-otari-bestiary-items/l8f0B8czvLKLsc06.htm)|Web Noose|auto-trad|
|[ll1kYssjNZ4S5vnd.htm](troubles-in-otari-bestiary-items/ll1kYssjNZ4S5vnd.htm)|Brutish Shove|auto-trad|
|[lLR6EoZxZTWifZUj.htm](troubles-in-otari-bestiary-items/lLR6EoZxZTWifZUj.htm)|Maul|auto-trad|
|[lykdG2IwAxNTGqQo.htm](troubles-in-otari-bestiary-items/lykdG2IwAxNTGqQo.htm)|Darkvision|auto-trad|
|[MKiOmYkknzLyP0Am.htm](troubles-in-otari-bestiary-items/MKiOmYkknzLyP0Am.htm)|Javelin|auto-trad|
|[Nc23oEL8srJURx0P.htm](troubles-in-otari-bestiary-items/Nc23oEL8srJURx0P.htm)|Remove Memory|auto-trad|
|[NoYQle3HJatFQcgH.htm](troubles-in-otari-bestiary-items/NoYQle3HJatFQcgH.htm)|Noose|auto-trad|
|[nUWlISx8ev0qZpTG.htm](troubles-in-otari-bestiary-items/nUWlISx8ev0qZpTG.htm)|Low-Light Vision|auto-trad|
|[O9himtaJvrfCALhE.htm](troubles-in-otari-bestiary-items/O9himtaJvrfCALhE.htm)|Magical Starknife|auto-trad|
|[OOCu4hNQg4NvyXLU.htm](troubles-in-otari-bestiary-items/OOCu4hNQg4NvyXLU.htm)|Arcane Prepared Spells|auto-trad|
|[pgQRipY9qrXPcNlY.htm](troubles-in-otari-bestiary-items/pgQRipY9qrXPcNlY.htm)|Darkvision|auto-trad|
|[qEbFimPOeE5aFvE7.htm](troubles-in-otari-bestiary-items/qEbFimPOeE5aFvE7.htm)|Dagger|auto-trad|
|[QfnsBk0XQ6x9wJSe.htm](troubles-in-otari-bestiary-items/QfnsBk0XQ6x9wJSe.htm)|Quick Trap|auto-trad|
|[QfWg8mYxqbJly84b.htm](troubles-in-otari-bestiary-items/QfWg8mYxqbJly84b.htm)|Shortsword|auto-trad|
|[qunm5Cfit6Q1T4dI.htm](troubles-in-otari-bestiary-items/qunm5Cfit6Q1T4dI.htm)|Wing Flash|auto-trad|
|[QVjozo4fqxCzzccc.htm](troubles-in-otari-bestiary-items/QVjozo4fqxCzzccc.htm)|Rapier|auto-trad|
|[QZb6NNceGC3aMTMO.htm](troubles-in-otari-bestiary-items/QZb6NNceGC3aMTMO.htm)|Staff|auto-trad|
|[STheFAvLWD42KLo9.htm](troubles-in-otari-bestiary-items/STheFAvLWD42KLo9.htm)|Nimble Dodge|auto-trad|
|[symt5PZrNoJWeIqe.htm](troubles-in-otari-bestiary-items/symt5PZrNoJWeIqe.htm)|Fist|auto-trad|
|[u4infcHTr2ku9Uxi.htm](troubles-in-otari-bestiary-items/u4infcHTr2ku9Uxi.htm)|Spear|auto-trad|
|[uGR730eqkGcdJEfo.htm](troubles-in-otari-bestiary-items/uGR730eqkGcdJEfo.htm)|Javelin|auto-trad|
|[UkurqY6wQGrS67eI.htm](troubles-in-otari-bestiary-items/UkurqY6wQGrS67eI.htm)|Divine Prepared Spells|auto-trad|
|[UMd6rwUlzKFS3lvJ.htm](troubles-in-otari-bestiary-items/UMd6rwUlzKFS3lvJ.htm)|Javelin|auto-trad|
|[VePco7JeJmUM7ijK.htm](troubles-in-otari-bestiary-items/VePco7JeJmUM7ijK.htm)|Attack of Opportunity|auto-trad|
|[Vkudm63q6gEmBgGp.htm](troubles-in-otari-bestiary-items/Vkudm63q6gEmBgGp.htm)|Low-Light Vision|auto-trad|
|[vlVKxFijTHI7cMPh.htm](troubles-in-otari-bestiary-items/vlVKxFijTHI7cMPh.htm)|Necromancer|auto-trad|
|[W6XkoyVR7QH9Z8lg.htm](troubles-in-otari-bestiary-items/W6XkoyVR7QH9Z8lg.htm)|Necromantic Defense|auto-trad|
|[wBuoAb6EXYtmwPmo.htm](troubles-in-otari-bestiary-items/wBuoAb6EXYtmwPmo.htm)|Longsword|auto-trad|
|[Wigs6zeZoSmogj8P.htm](troubles-in-otari-bestiary-items/Wigs6zeZoSmogj8P.htm)|Seedpod|auto-trad|
|[WqgfgkwqUVMJK44b.htm](troubles-in-otari-bestiary-items/WqgfgkwqUVMJK44b.htm)|Jaws|auto-trad|
|[wyzsbdPo28UWUYTD.htm](troubles-in-otari-bestiary-items/wyzsbdPo28UWUYTD.htm)|Unburdened Iron|auto-trad|
|[YB4lJDTFGnMJZxPX.htm](troubles-in-otari-bestiary-items/YB4lJDTFGnMJZxPX.htm)|Jaws|auto-trad|
|[YhlP3I6uuAc1wEik.htm](troubles-in-otari-bestiary-items/YhlP3I6uuAc1wEik.htm)|Divine Innate Spells|auto-trad|
|[z7UOnDRw58SSvHLY.htm](troubles-in-otari-bestiary-items/z7UOnDRw58SSvHLY.htm)|Fist|auto-trad|
|[ZhDg0uKQhEzdCReZ.htm](troubles-in-otari-bestiary-items/ZhDg0uKQhEzdCReZ.htm)|Fist|auto-trad|
|[zhR9Ys8AyYNjAz29.htm](troubles-in-otari-bestiary-items/zhR9Ys8AyYNjAz29.htm)|Arcane Focus Spells|auto-trad|
|[zNOKH6cGU4FaspAX.htm](troubles-in-otari-bestiary-items/zNOKH6cGU4FaspAX.htm)|Woodland Stride|auto-trad|
|[ZxSGALYRVWtMBLTi.htm](troubles-in-otari-bestiary-items/ZxSGALYRVWtMBLTi.htm)|Darkvision|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
