# Estado de la traducción (pathfinder-society-boons)

 * **auto-trad**: 131


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0CqkYRFUlb0tB9li.htm](pathfinder-society-boons/0CqkYRFUlb0tB9li.htm)|S1-23 - Seasoned Diplomat|auto-trad|
|[0FwLrQb5EeQDAUNW.htm](pathfinder-society-boons/0FwLrQb5EeQDAUNW.htm)|Naturalist|auto-trad|
|[0izXWwYqRGQq74mG.htm](pathfinder-society-boons/0izXWwYqRGQq74mG.htm)|S1-07 - Blast from the Past|auto-trad|
|[0QRG6TAVwCWL2Zr8.htm](pathfinder-society-boons/0QRG6TAVwCWL2Zr8.htm)|Heroic Inspiration|auto-trad|
|[0zhQQK3gvu75ac1Q.htm](pathfinder-society-boons/0zhQQK3gvu75ac1Q.htm)|Eager Protege|auto-trad|
|[1FWpmtZHOLRvEIdp.htm](pathfinder-society-boons/1FWpmtZHOLRvEIdp.htm)|Verdant Wheel Champion|auto-trad|
|[1VWMLe12ZFqF9jah.htm](pathfinder-society-boons/1VWMLe12ZFqF9jah.htm)|Hireling, Expert|auto-trad|
|[1welewgoB0z86Nms.htm](pathfinder-society-boons/1welewgoB0z86Nms.htm)|S1-03 - Team Player|auto-trad|
|[2QzXBsURLO7xECxO.htm](pathfinder-society-boons/2QzXBsURLO7xECxO.htm)|FoP - Fiery Companion|auto-trad|
|[3K8yzfU2t9chVv6h.htm](pathfinder-society-boons/3K8yzfU2t9chVv6h.htm)|AoA #149: Boon B - Katapesh Companion|auto-trad|
|[3lHqim7ttvoMmVXW.htm](pathfinder-society-boons/3lHqim7ttvoMmVXW.htm)|Sellback Plan|auto-trad|
|[4lLhGgG30TMu8tIx.htm](pathfinder-society-boons/4lLhGgG30TMu8tIx.htm)|FoP - Noala's Lessons|auto-trad|
|[4O3qxD3I4LUjl2Vq.htm](pathfinder-society-boons/4O3qxD3I4LUjl2Vq.htm)|Wayfinder, Harmonic|auto-trad|
|[5tWlPSGW5AEk10GJ.htm](pathfinder-society-boons/5tWlPSGW5AEk10GJ.htm)|Consummate Dabbler|auto-trad|
|[632pDmCtbQxBlE3d.htm](pathfinder-society-boons/632pDmCtbQxBlE3d.htm)|S1-02 - Cryptid Scholar|auto-trad|
|[6frkVrsCgTgFhMjl.htm](pathfinder-society-boons/6frkVrsCgTgFhMjl.htm)|Vigilant Seal Champion|auto-trad|
|[6gVFdrqhs0mihRao.htm](pathfinder-society-boons/6gVFdrqhs0mihRao.htm)|S1-Q02 - One-Who-Waits|auto-trad|
|[6oTXrz56Evik64Fy.htm](pathfinder-society-boons/6oTXrz56Evik64Fy.htm)|S1-04 - A Thorny Situation|auto-trad|
|[6XUhCWU0h0qbgL5Z.htm](pathfinder-society-boons/6XUhCWU0h0qbgL5Z.htm)|S2-00 - Spark of Life (levels 7-8)|auto-trad|
|[8cMFcCa3h9XfWNkZ.htm](pathfinder-society-boons/8cMFcCa3h9XfWNkZ.htm)|AoA #149: Boon A - Advanced Training|auto-trad|
|[8fbBYNkFQgU3IQV0.htm](pathfinder-society-boons/8fbBYNkFQgU3IQV0.htm)|Grand Archive Champion|auto-trad|
|[95R55tdobViU7ZWe.htm](pathfinder-society-boons/95R55tdobViU7ZWe.htm)|Mentor, Magical|auto-trad|
|[9w8TdCsGEpttXYuy.htm](pathfinder-society-boons/9w8TdCsGEpttXYuy.htm)|Mentor, Worldly|auto-trad|
|[aG5cua0K2SlurXxw.htm](pathfinder-society-boons/aG5cua0K2SlurXxw.htm)|Promotional Accessory|auto-trad|
|[awcOD618fuXZN3rh.htm](pathfinder-society-boons/awcOD618fuXZN3rh.htm)|Practiced Medic|auto-trad|
|[B7fDgC6tcv41lcP7.htm](pathfinder-society-boons/B7fDgC6tcv41lcP7.htm)|S1-Q11 - Amateur Genealogist|auto-trad|
|[bcsusUErwGQYHj6d.htm](pathfinder-society-boons/bcsusUErwGQYHj6d.htm)|Multicultural Training|auto-trad|
|[BEaLe5M23Q7LKTLL.htm](pathfinder-society-boons/BEaLe5M23Q7LKTLL.htm)|Wayfinder, Adamant|auto-trad|
|[bMp6W5kPNyqBoOxm.htm](pathfinder-society-boons/bMp6W5kPNyqBoOxm.htm)|Charitable Adventure|auto-trad|
|[ca869lJo8E51WcW4.htm](pathfinder-society-boons/ca869lJo8E51WcW4.htm)|Hireling, Master|auto-trad|
|[cjjRFoOE1GtjEA3F.htm](pathfinder-society-boons/cjjRFoOE1GtjEA3F.htm)|Promotional Service Award|auto-trad|
|[CjW54WSPDcykzqj9.htm](pathfinder-society-boons/CjW54WSPDcykzqj9.htm)|AoA #148: Boon B - Expanded Summoning|auto-trad|
|[CKyGNdBL8Qt4ysKt.htm](pathfinder-society-boons/CKyGNdBL8Qt4ysKt.htm)|S2-03 - Fluent in Cyclops|auto-trad|
|[CLRt3iFqWlMwCNVl.htm](pathfinder-society-boons/CLRt3iFqWlMwCNVl.htm)|Vigilant Seal Champion, Improved|auto-trad|
|[CPcMqEd4H5yIVl3i.htm](pathfinder-society-boons/CPcMqEd4H5yIVl3i.htm)|World Traveller|auto-trad|
|[CW73M2apBaQh6wLm.htm](pathfinder-society-boons/CW73M2apBaQh6wLm.htm)|Unparalleled Scholarship|auto-trad|
|[cyrntY2zgDyzxQGO.htm](pathfinder-society-boons/cyrntY2zgDyzxQGO.htm)|Verdant Wheel Champion, Improved|auto-trad|
|[DcMAGJWq2G5kGRWn.htm](pathfinder-society-boons/DcMAGJWq2G5kGRWn.htm)|S1-16 - Blood Offering|auto-trad|
|[DD1mlAEaXdM1pBze.htm](pathfinder-society-boons/DD1mlAEaXdM1pBze.htm)|S1-Q07 - Amateur Adjuster|auto-trad|
|[DkqU67wxvbDu0Qrs.htm](pathfinder-society-boons/DkqU67wxvbDu0Qrs.htm)|Mentor, Rugged|auto-trad|
|[DWcmLzzT1eYk76c7.htm](pathfinder-society-boons/DWcmLzzT1eYk76c7.htm)|AoA #146: Mwangi Summoner|auto-trad|
|[En3RdoXDAtyXdNKq.htm](pathfinder-society-boons/En3RdoXDAtyXdNKq.htm)|Heroic Intervention|auto-trad|
|[ePZOmPbGXzrEaXXs.htm](pathfinder-society-boons/ePZOmPbGXzrEaXXs.htm)|S1-11 - Devil's Keep|auto-trad|
|[fTndDUqUDSVXM0Qm.htm](pathfinder-society-boons/fTndDUqUDSVXM0Qm.htm)|S1-11 - Diggen the Liar|auto-trad|
|[G0hv2CI7TKcUcSVs.htm](pathfinder-society-boons/G0hv2CI7TKcUcSVs.htm)|Vault Delver|auto-trad|
|[G7fN5K1TfmNhTb2w.htm](pathfinder-society-boons/G7fN5K1TfmNhTb2w.htm)|S2-00 - Spark of Life (levels 1-2)|auto-trad|
|[gdkQ04RgMJZ0QxbZ.htm](pathfinder-society-boons/gdkQ04RgMJZ0QxbZ.htm)|Crafter's Workshop|auto-trad|
|[GIdtqEFKKgwPmDBq.htm](pathfinder-society-boons/GIdtqEFKKgwPmDBq.htm)|S1-05 - Experienced Mountaineer|auto-trad|
|[giyLRNefaC3IsFay.htm](pathfinder-society-boons/giyLRNefaC3IsFay.htm)|S1-10 - Tarnbreaker Champions|auto-trad|
|[Gmzby7whs5O0wuYC.htm](pathfinder-society-boons/Gmzby7whs5O0wuYC.htm)|Untarnished Reputation|auto-trad|
|[GRiNBy8yv4sWp1ua.htm](pathfinder-society-boons/GRiNBy8yv4sWp1ua.htm)|Bequethal|auto-trad|
|[grkmoNwwHOROKER7.htm](pathfinder-society-boons/grkmoNwwHOROKER7.htm)|AoA #150: Boon A - Legacy of Ashes|auto-trad|
|[hghR0f5IetZBvyDR.htm](pathfinder-society-boons/hghR0f5IetZBvyDR.htm)|Heroic Defiance|auto-trad|
|[hxp0fDMrB7BT3y3T.htm](pathfinder-society-boons/hxp0fDMrB7BT3y3T.htm)|S1-15 - Finadar Leshy|auto-trad|
|[hy1gMMpRmujuw8i4.htm](pathfinder-society-boons/hy1gMMpRmujuw8i4.htm)|Leader By Example|auto-trad|
|[ig06ikA6rnNP1JUb.htm](pathfinder-society-boons/ig06ikA6rnNP1JUb.htm)|S1-01 - Society Connections|auto-trad|
|[iRLU5DTWY16HIqcc.htm](pathfinder-society-boons/iRLU5DTWY16HIqcc.htm)|S1-22 - Doom Averted|auto-trad|
|[J1hUP5SI4MJvlK36.htm](pathfinder-society-boons/J1hUP5SI4MJvlK36.htm)|Mentor, Skillful|auto-trad|
|[jzJ575vAsw2MaRgo.htm](pathfinder-society-boons/jzJ575vAsw2MaRgo.htm)|Envoy's Alliance Champion|auto-trad|
|[JznxnYTs38GYpW1p.htm](pathfinder-society-boons/JznxnYTs38GYpW1p.htm)|S1-14 - Big Game Hunter|auto-trad|
|[K21KQsrR3RMcJ1du.htm](pathfinder-society-boons/K21KQsrR3RMcJ1du.htm)|Swift Traveler|auto-trad|
|[KXO4CUklzuJ0tZnw.htm](pathfinder-society-boons/KXO4CUklzuJ0tZnw.htm)|Home Region|auto-trad|
|[KZrXvKlAVcpLgGF8.htm](pathfinder-society-boons/KZrXvKlAVcpLgGF8.htm)|Society Recruiter|auto-trad|
|[l8WNWPFUdxcqEG6e.htm](pathfinder-society-boons/l8WNWPFUdxcqEG6e.htm)|Envoy's Alliance Champion, Improved|auto-trad|
|[lJ1lNwGxV0IrsN2z.htm](pathfinder-society-boons/lJ1lNwGxV0IrsN2z.htm)|S1-08 - River Kingdoms Politician|auto-trad|
|[M1flDLvjKBH9QPcx.htm](pathfinder-society-boons/M1flDLvjKBH9QPcx.htm)|Storied Talent|auto-trad|
|[M2lGGGaPIOimeA31.htm](pathfinder-society-boons/M2lGGGaPIOimeA31.htm)|Resurrection Plan|auto-trad|
|[M8cJ8qOwWj7XN6h1.htm](pathfinder-society-boons/M8cJ8qOwWj7XN6h1.htm)|Radiant Oath Champion|auto-trad|
|[MLn6UbZsU4PphXLE.htm](pathfinder-society-boons/MLn6UbZsU4PphXLE.htm)|S1-Q06 - Secrets of the Jistkan Alchemists|auto-trad|
|[MN3MlIx7djj7WDNY.htm](pathfinder-society-boons/MN3MlIx7djj7WDNY.htm)|Meticulous Appraisal|auto-trad|
|[mrlQrZd0yBwOj2lk.htm](pathfinder-society-boons/mrlQrZd0yBwOj2lk.htm)|Hireling, Professional|auto-trad|
|[Mu5GQ9ZEz3geAYxK.htm](pathfinder-society-boons/Mu5GQ9ZEz3geAYxK.htm)|Leshy Companion|auto-trad|
|[MuhZZkLR8y9eEndN.htm](pathfinder-society-boons/MuhZZkLR8y9eEndN.htm)|S1-Q10 - Sewer Dragon Recruit|auto-trad|
|[Nadcb08xcTAoPBEX.htm](pathfinder-society-boons/Nadcb08xcTAoPBEX.htm)|AoA #147: Boon A - Heroic Membership|auto-trad|
|[NR19RYJLveHrFYvf.htm](pathfinder-society-boons/NR19RYJLveHrFYvf.htm)|S1-Q03 - Legacy of the Gorget|auto-trad|
|[ocS6B3AV1n7yHFd5.htm](pathfinder-society-boons/ocS6B3AV1n7yHFd5.htm)|Exemplary Recruiter|auto-trad|
|[oOB1uKvoBMGj7ykZ.htm](pathfinder-society-boons/oOB1uKvoBMGj7ykZ.htm)|Curse Breaker|auto-trad|
|[opaFl76Nbz1dITLz.htm](pathfinder-society-boons/opaFl76Nbz1dITLz.htm)|Secondary Initiation|auto-trad|
|[P1sdPHrbAfqrEAM3.htm](pathfinder-society-boons/P1sdPHrbAfqrEAM3.htm)|S1-01 - Engraved Wayfinder|auto-trad|
|[p8TKPInhya6oab1w.htm](pathfinder-society-boons/p8TKPInhya6oab1w.htm)|S1-19 - Iolite Trainee Hobgoblin|auto-trad|
|[PbEqXHWiyzii4g8T.htm](pathfinder-society-boons/PbEqXHWiyzii4g8T.htm)|S1-25 - Grand Finale|auto-trad|
|[ph7CO8ong1sZmrNc.htm](pathfinder-society-boons/ph7CO8ong1sZmrNc.htm)|Horizon Hunters Champion|auto-trad|
|[prHShOQxNpLXwlss.htm](pathfinder-society-boons/prHShOQxNpLXwlss.htm)|S1-Q04 - Fane's Friend|auto-trad|
|[PXtBhRVHM28angjV.htm](pathfinder-society-boons/PXtBhRVHM28angjV.htm)|Mentor, Protective|auto-trad|
|[qaSlz43CHPiCoIOC.htm](pathfinder-society-boons/qaSlz43CHPiCoIOC.htm)|Radiant Oath Champion, Improved|auto-trad|
|[rddlbymeG9KbQYQM.htm](pathfinder-society-boons/rddlbymeG9KbQYQM.htm)|Translator|auto-trad|
|[rdY0vyXfrIlkQgqd.htm](pathfinder-society-boons/rdY0vyXfrIlkQgqd.htm)|Beginnings and Endings|auto-trad|
|[rFgWvC1UADsavY5J.htm](pathfinder-society-boons/rFgWvC1UADsavY5J.htm)|Resist Corruption|auto-trad|
|[RGSw5tAc4ALp97Qe.htm](pathfinder-society-boons/RGSw5tAc4ALp97Qe.htm)|S1-Q04 - Stella's Associate|auto-trad|
|[rJfATB750MbndGeP.htm](pathfinder-society-boons/rJfATB750MbndGeP.htm)|S2-08 - Baba Yaga's Tutelage|auto-trad|
|[RJYIefY9Skx8CAgw.htm](pathfinder-society-boons/RJYIefY9Skx8CAgw.htm)|S1-Q09 - Wayfinder Connections|auto-trad|
|[RLzWznPsb2VpbxTy.htm](pathfinder-society-boons/RLzWznPsb2VpbxTy.htm)|S1-Q12 - Fruit Basket|auto-trad|
|[sBXsfT0uYltF8TFE.htm](pathfinder-society-boons/sBXsfT0uYltF8TFE.htm)|Hireling|auto-trad|
|[Sc9gT40T28tFhSzq.htm](pathfinder-society-boons/Sc9gT40T28tFhSzq.htm)|Horizon Hunters Champion, Improved|auto-trad|
|[SDDbh7KmDn5HUbst.htm](pathfinder-society-boons/SDDbh7KmDn5HUbst.htm)|Promotional Vestments|auto-trad|
|[SI37oVZxjwXew3E4.htm](pathfinder-society-boons/SI37oVZxjwXew3E4.htm)|Bring Them Back Alive|auto-trad|
|[SujEcE3H5n9kx1PI.htm](pathfinder-society-boons/SujEcE3H5n9kx1PI.htm)|Wayfinder, Esoteric|auto-trad|
|[SxUBWlQj6BT5g4Rw.htm](pathfinder-society-boons/SxUBWlQj6BT5g4Rw.htm)|Wayfinder|auto-trad|
|[T7gyK9LV14wI7Xdy.htm](pathfinder-society-boons/T7gyK9LV14wI7Xdy.htm)|S1-07 - To Seal and Protect|auto-trad|
|[taUqYoHuWH9uEbsn.htm](pathfinder-society-boons/taUqYoHuWH9uEbsn.htm)|S1-17 - Fey Influence|auto-trad|
|[Tmqa8Q9zaYr0uj6F.htm](pathfinder-society-boons/Tmqa8Q9zaYr0uj6F.htm)|Grand Archive Champion, Improved|auto-trad|
|[tX2vxY8RSdcBayEu.htm](pathfinder-society-boons/tX2vxY8RSdcBayEu.htm)|S1-18 - Narsen's Web|auto-trad|
|[U6QqFDstHNjEeOUS.htm](pathfinder-society-boons/U6QqFDstHNjEeOUS.htm)|Wayfinder, Rugged|auto-trad|
|[UNlaVxb0pDov3efw.htm](pathfinder-society-boons/UNlaVxb0pDov3efw.htm)|Heroic Resurgence|auto-trad|
|[UWZfORgk1yVNV0RF.htm](pathfinder-society-boons/UWZfORgk1yVNV0RF.htm)|Exotic Edge|auto-trad|
|[VLjkX7HpDqKaidNd.htm](pathfinder-society-boons/VLjkX7HpDqKaidNd.htm)|AoA #148: Boon A - Crystal Clear|auto-trad|
|[Vmg31SsMjjimghzP.htm](pathfinder-society-boons/Vmg31SsMjjimghzP.htm)|Menace Under Otari|auto-trad|
|[vYSA5M9fgBdYiGLT.htm](pathfinder-society-boons/vYSA5M9fgBdYiGLT.htm)|Mentor, Combat|auto-trad|
|[wcOnL9nFDfCkNVMZ.htm](pathfinder-society-boons/wcOnL9nFDfCkNVMZ.htm)|AoA #149: Boon C - Expanded Summoning|auto-trad|
|[wdjRk8JaZGP1mjqD.htm](pathfinder-society-boons/wdjRk8JaZGP1mjqD.htm)|S1-09 - Iruxi Bane|auto-trad|
|[We3sJV72xtdPeMjp.htm](pathfinder-society-boons/We3sJV72xtdPeMjp.htm)|AoA #150: Boon B - Paragon of Promise|auto-trad|
|[WGjcz2mkgGB1qCWw.htm](pathfinder-society-boons/WGjcz2mkgGB1qCWw.htm)|Preserve|auto-trad|
|[wkgBg5UD3bChfl4b.htm](pathfinder-society-boons/wkgBg5UD3bChfl4b.htm)|Heroic Recall|auto-trad|
|[WV4J7NMgKXNlXonW.htm](pathfinder-society-boons/WV4J7NMgKXNlXonW.htm)|AoA #147: Boon B - Expanded Summoning|auto-trad|
|[WVOP420jZ4jSTYI8.htm](pathfinder-society-boons/WVOP420jZ4jSTYI8.htm)|S1-06 - Traveler of the Spirit Road|auto-trad|
|[WYHPJyza1BJgtFq4.htm](pathfinder-society-boons/WYHPJyza1BJgtFq4.htm)|S1-Q01 - Sand Slide|auto-trad|
|[Wz2F0qk4rxZeH311.htm](pathfinder-society-boons/Wz2F0qk4rxZeH311.htm)|Heroic Hustle|auto-trad|
|[XBf4Uz3n0AwSmpXx.htm](pathfinder-society-boons/XBf4Uz3n0AwSmpXx.htm)|Adversary Lore|auto-trad|
|[XkNW2dq5hwWOViYg.htm](pathfinder-society-boons/XkNW2dq5hwWOViYg.htm)|Off-Hours Study|auto-trad|
|[XoamInggkxLh6Zo0.htm](pathfinder-society-boons/XoamInggkxLh6Zo0.htm)|Academic Conference|auto-trad|
|[XOVDOCsvNBX5LS8j.htm](pathfinder-society-boons/XOVDOCsvNBX5LS8j.htm)|S1-12 - Valais's Assurance|auto-trad|
|[xVMouu7E7Xy7AGER.htm](pathfinder-society-boons/xVMouu7E7Xy7AGER.htm)|S1-13 - Convention Hero|auto-trad|
|[XybjaZJlmBFFztwJ.htm](pathfinder-society-boons/XybjaZJlmBFFztwJ.htm)|S1-04 - Touched by the Storm|auto-trad|
|[Y69ZRmuoj0XK9AZm.htm](pathfinder-society-boons/Y69ZRmuoj0XK9AZm.htm)|S1-18 - Light in the Dark|auto-trad|
|[y8sk7Q4uAtacYdR2.htm](pathfinder-society-boons/y8sk7Q4uAtacYdR2.htm)|S1-Q02 - Student of the Unforgiving Fire|auto-trad|
|[ybrynsFe4a0jJcgz.htm](pathfinder-society-boons/ybrynsFe4a0jJcgz.htm)|S2-00 - Spark of Life (levels 3-6)|auto-trad|
|[yNTePhAPVMguqUVA.htm](pathfinder-society-boons/yNTePhAPVMguqUVA.htm)|S1-Q08 - Numerian Archaeologist|auto-trad|
|[YVOyEfO8tTx4jj2q.htm](pathfinder-society-boons/YVOyEfO8tTx4jj2q.htm)|S1-20 - Waters of Warlock's Barrow|auto-trad|
|[YVpNrVBLRrHjaR4C.htm](pathfinder-society-boons/YVpNrVBLRrHjaR4C.htm)|S1-00 - Nexian Researcher|auto-trad|
|[ZtLTqT4G5q8SLJCT.htm](pathfinder-society-boons/ZtLTqT4G5q8SLJCT.htm)|S1-09 - Ally of the Iruxi|auto-trad|
|[ZZvIzyH6SwyVkEYm.htm](pathfinder-society-boons/ZZvIzyH6SwyVkEYm.htm)|S1-21 - Maze Walker|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
