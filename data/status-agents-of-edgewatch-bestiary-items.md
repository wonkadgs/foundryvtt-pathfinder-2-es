# Estado de la traducción (agents-of-edgewatch-bestiary-items)

 * **auto-trad**: 1165
 * **modificada**: 33
 * **ninguna**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[1jGFJIbrEUb7ObhO.htm](agents-of-edgewatch-bestiary-items/1jGFJIbrEUb7ObhO.htm)|No MAP|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[00AnAb9FZBwVZGjI.htm](agents-of-edgewatch-bestiary-items/00AnAb9FZBwVZGjI.htm)|Jaws|auto-trad|
|[02vYXhCEPmoLEU7V.htm](agents-of-edgewatch-bestiary-items/02vYXhCEPmoLEU7V.htm)|Fast Healing 5|auto-trad|
|[034N8wrms1VjpEX4.htm](agents-of-edgewatch-bestiary-items/034N8wrms1VjpEX4.htm)|Terrible Justice|auto-trad|
|[09M4q5fVCuvOwcWk.htm](agents-of-edgewatch-bestiary-items/09M4q5fVCuvOwcWk.htm)|Poison Ink|auto-trad|
|[0AtXXmHLOdGkB70d.htm](agents-of-edgewatch-bestiary-items/0AtXXmHLOdGkB70d.htm)|Fast Healing 10|auto-trad|
|[0DxZM2ZhwpKrUolW.htm](agents-of-edgewatch-bestiary-items/0DxZM2ZhwpKrUolW.htm)|Sneak Attack|auto-trad|
|[0Fzs97xBu4GMOwjA.htm](agents-of-edgewatch-bestiary-items/0Fzs97xBu4GMOwjA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[0GDvJYPFAaQoah9E.htm](agents-of-edgewatch-bestiary-items/0GDvJYPFAaQoah9E.htm)|Syringe|auto-trad|
|[0Jid4uAhNoUlXWoo.htm](agents-of-edgewatch-bestiary-items/0Jid4uAhNoUlXWoo.htm)|Guildmaster's Lead|auto-trad|
|[0jNbTDLglMGWVCtF.htm](agents-of-edgewatch-bestiary-items/0jNbTDLglMGWVCtF.htm)|Pincer|auto-trad|
|[0jRv23fTxJTfzMQx.htm](agents-of-edgewatch-bestiary-items/0jRv23fTxJTfzMQx.htm)|Innate Occult Spells|auto-trad|
|[0KI7dYfaduuCOAlE.htm](agents-of-edgewatch-bestiary-items/0KI7dYfaduuCOAlE.htm)|Rejuvenation|auto-trad|
|[0kL2ZW2fqmtImuCu.htm](agents-of-edgewatch-bestiary-items/0kL2ZW2fqmtImuCu.htm)|Grab|auto-trad|
|[0mlWlB8x9buvEw3g.htm](agents-of-edgewatch-bestiary-items/0mlWlB8x9buvEw3g.htm)|Swarm Mind|auto-trad|
|[0rqwxWSqNK0Ikmdw.htm](agents-of-edgewatch-bestiary-items/0rqwxWSqNK0Ikmdw.htm)|Graveknight's Curse|auto-trad|
|[0TIsrEfH3mfhYqMB.htm](agents-of-edgewatch-bestiary-items/0TIsrEfH3mfhYqMB.htm)|Eye-Opener|auto-trad|
|[0tqsvUhGYvPuB2ga.htm](agents-of-edgewatch-bestiary-items/0tqsvUhGYvPuB2ga.htm)|Reshape Reality|auto-trad|
|[0UpmO63mWa667Q99.htm](agents-of-edgewatch-bestiary-items/0UpmO63mWa667Q99.htm)|Tactical Aura|auto-trad|
|[0vSVpFhBgh2bSdVc.htm](agents-of-edgewatch-bestiary-items/0vSVpFhBgh2bSdVc.htm)|Fist|auto-trad|
|[0yo458L980s9JXH6.htm](agents-of-edgewatch-bestiary-items/0yo458L980s9JXH6.htm)|Pest Haven|auto-trad|
|[10AhR44dLRwNyyyT.htm](agents-of-edgewatch-bestiary-items/10AhR44dLRwNyyyT.htm)|Alchemical Rupture|auto-trad|
|[126ppmFKAd49r2hi.htm](agents-of-edgewatch-bestiary-items/126ppmFKAd49r2hi.htm)|Najra Swarm Attack|auto-trad|
|[1a9J9jdVUsp4RIxQ.htm](agents-of-edgewatch-bestiary-items/1a9J9jdVUsp4RIxQ.htm)|Sky and Heaven Stance|auto-trad|
|[1bSj6FFb2A1ZOyrZ.htm](agents-of-edgewatch-bestiary-items/1bSj6FFb2A1ZOyrZ.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[1cQlKfzWAd8gXGFa.htm](agents-of-edgewatch-bestiary-items/1cQlKfzWAd8gXGFa.htm)|Malefic Binding|auto-trad|
|[1d7P7fxvDCam2ALx.htm](agents-of-edgewatch-bestiary-items/1d7P7fxvDCam2ALx.htm)|Vein Walker|auto-trad|
|[1DABV9KyoaCfVd3F.htm](agents-of-edgewatch-bestiary-items/1DABV9KyoaCfVd3F.htm)|Darkvision|auto-trad|
|[1eq3XxcWMvDVBheO.htm](agents-of-edgewatch-bestiary-items/1eq3XxcWMvDVBheO.htm)|Telepathy 100 feet|auto-trad|
|[1GcnjXs1m3fitCxq.htm](agents-of-edgewatch-bestiary-items/1GcnjXs1m3fitCxq.htm)|Sacrilegious Aura|auto-trad|
|[1h5CBWyXY0MAHGtm.htm](agents-of-edgewatch-bestiary-items/1h5CBWyXY0MAHGtm.htm)|Bloody Sneak Attack|auto-trad|
|[1HjWqCIXUjmknlZE.htm](agents-of-edgewatch-bestiary-items/1HjWqCIXUjmknlZE.htm)|Adamantine Strikes|auto-trad|
|[1mBHiJQyWY7LwsCN.htm](agents-of-edgewatch-bestiary-items/1mBHiJQyWY7LwsCN.htm)|Rend|auto-trad|
|[1OKW1epSXihm5XRb.htm](agents-of-edgewatch-bestiary-items/1OKW1epSXihm5XRb.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[1pf3cBO6a0unyMKL.htm](agents-of-edgewatch-bestiary-items/1pf3cBO6a0unyMKL.htm)|Jaws|auto-trad|
|[1qQjPJIus3hZOUXY.htm](agents-of-edgewatch-bestiary-items/1qQjPJIus3hZOUXY.htm)|Powerful Swipe|auto-trad|
|[1Rk88GORg0fjreo8.htm](agents-of-edgewatch-bestiary-items/1Rk88GORg0fjreo8.htm)|Chain Up|auto-trad|
|[1sozJvnrR9nNQ2qu.htm](agents-of-edgewatch-bestiary-items/1sozJvnrR9nNQ2qu.htm)|Sneak Attack|auto-trad|
|[1UFXWKQIPIkKudjR.htm](agents-of-edgewatch-bestiary-items/1UFXWKQIPIkKudjR.htm)|Fangs|auto-trad|
|[1vHKAVMhE8gmaKS2.htm](agents-of-edgewatch-bestiary-items/1vHKAVMhE8gmaKS2.htm)|Low-Light Vision|auto-trad|
|[1wPZqdlI8xQ4yfN9.htm](agents-of-edgewatch-bestiary-items/1wPZqdlI8xQ4yfN9.htm)|Fangs|auto-trad|
|[22ts3ClOQFeLq0Jt.htm](agents-of-edgewatch-bestiary-items/22ts3ClOQFeLq0Jt.htm)|Water Shield|auto-trad|
|[268BBCle1rCc1g1d.htm](agents-of-edgewatch-bestiary-items/268BBCle1rCc1g1d.htm)|Cannon Fusillade|auto-trad|
|[26ZZztmP0cFmWp6x.htm](agents-of-edgewatch-bestiary-items/26ZZztmP0cFmWp6x.htm)|Precise Tremorsense 40 feet|auto-trad|
|[281TWDcEhOImRl44.htm](agents-of-edgewatch-bestiary-items/281TWDcEhOImRl44.htm)|Divine Innate Spells|auto-trad|
|[29elTy7XDcAju8yK.htm](agents-of-edgewatch-bestiary-items/29elTy7XDcAju8yK.htm)|Ghaele's Gaze|auto-trad|
|[29naBTIemmuYyMKB.htm](agents-of-edgewatch-bestiary-items/29naBTIemmuYyMKB.htm)|Skip Between|auto-trad|
|[2EL49f3yp855RUpq.htm](agents-of-edgewatch-bestiary-items/2EL49f3yp855RUpq.htm)|Quick Movements|auto-trad|
|[2FxUvyDjnESqE9bl.htm](agents-of-edgewatch-bestiary-items/2FxUvyDjnESqE9bl.htm)|Quick Brew|auto-trad|
|[2gGx394noy8wbdH3.htm](agents-of-edgewatch-bestiary-items/2gGx394noy8wbdH3.htm)|Constrict|auto-trad|
|[2JJtxgkrLOcIfYQz.htm](agents-of-edgewatch-bestiary-items/2JJtxgkrLOcIfYQz.htm)|Mirror Dart|auto-trad|
|[2JN5a5P81ZqQkOsD.htm](agents-of-edgewatch-bestiary-items/2JN5a5P81ZqQkOsD.htm)|Painful Light|auto-trad|
|[2pMaO2Z0N8Vx4Yy8.htm](agents-of-edgewatch-bestiary-items/2pMaO2Z0N8Vx4Yy8.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[2S79HfbIxhTAsZFh.htm](agents-of-edgewatch-bestiary-items/2S79HfbIxhTAsZFh.htm)|At-Will Spells|auto-trad|
|[2TFdxsGPr1xaX73k.htm](agents-of-edgewatch-bestiary-items/2TFdxsGPr1xaX73k.htm)|Syringe|auto-trad|
|[2tZ1okZGWrxi91Ba.htm](agents-of-edgewatch-bestiary-items/2tZ1okZGWrxi91Ba.htm)|Telepathy 100 feet|auto-trad|
|[2Vevg87ax4Fd8BnK.htm](agents-of-edgewatch-bestiary-items/2Vevg87ax4Fd8BnK.htm)|Shield Bash|auto-trad|
|[2w1WQr635JVnsORb.htm](agents-of-edgewatch-bestiary-items/2w1WQr635JVnsORb.htm)|Swift Sneak|auto-trad|
|[2wSPUQgCOkfTQCUJ.htm](agents-of-edgewatch-bestiary-items/2wSPUQgCOkfTQCUJ.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[2Wzd91SDBc89gsI9.htm](agents-of-edgewatch-bestiary-items/2Wzd91SDBc89gsI9.htm)|Tail|auto-trad|
|[2yp2PpkHqWXuA5k0.htm](agents-of-edgewatch-bestiary-items/2yp2PpkHqWXuA5k0.htm)|Change Shape|auto-trad|
|[2zmTtiyeZNvKPOcI.htm](agents-of-edgewatch-bestiary-items/2zmTtiyeZNvKPOcI.htm)|Ink Blood|auto-trad|
|[2zo8BFj6mkav8MPT.htm](agents-of-edgewatch-bestiary-items/2zo8BFj6mkav8MPT.htm)|Ferocious Devotion|auto-trad|
|[3039pLEWxpDZZrH0.htm](agents-of-edgewatch-bestiary-items/3039pLEWxpDZZrH0.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[31SjrOIjFWX8if4N.htm](agents-of-edgewatch-bestiary-items/31SjrOIjFWX8if4N.htm)|Poison Weapon|auto-trad|
|[32FbUxGNpWinXjEq.htm](agents-of-edgewatch-bestiary-items/32FbUxGNpWinXjEq.htm)|Goblin Scuttle|auto-trad|
|[345q6v1fOXwCpJpM.htm](agents-of-edgewatch-bestiary-items/345q6v1fOXwCpJpM.htm)|Attack of Opportunity|auto-trad|
|[35yXdy43dCClA3wy.htm](agents-of-edgewatch-bestiary-items/35yXdy43dCClA3wy.htm)|Smooth Operator|auto-trad|
|[3aasDsiXoE4xtSVZ.htm](agents-of-edgewatch-bestiary-items/3aasDsiXoE4xtSVZ.htm)|Distracting Shadows|auto-trad|
|[3BvCMBxHiHXEMq2P.htm](agents-of-edgewatch-bestiary-items/3BvCMBxHiHXEMq2P.htm)|Bonesense (Imprecise) 30 feet|auto-trad|
|[3bYjif3A9K5vszf9.htm](agents-of-edgewatch-bestiary-items/3bYjif3A9K5vszf9.htm)|Claw|auto-trad|
|[3dd754R1VUxFyZiT.htm](agents-of-edgewatch-bestiary-items/3dd754R1VUxFyZiT.htm)|Secret of Rebirth|auto-trad|
|[3fSPm7dDpsyRRKqg.htm](agents-of-edgewatch-bestiary-items/3fSPm7dDpsyRRKqg.htm)|Fast Healing 8|auto-trad|
|[3g9fOfW6T5VklwH5.htm](agents-of-edgewatch-bestiary-items/3g9fOfW6T5VklwH5.htm)|Compression|auto-trad|
|[3jd4JNaR2XiNoddM.htm](agents-of-edgewatch-bestiary-items/3jd4JNaR2XiNoddM.htm)|Jaws|auto-trad|
|[3jh7PEda42lpDxXR.htm](agents-of-edgewatch-bestiary-items/3jh7PEda42lpDxXR.htm)|Light Blindness|auto-trad|
|[3N2FHMvzMRnvpsId.htm](agents-of-edgewatch-bestiary-items/3N2FHMvzMRnvpsId.htm)|Human Hunter|auto-trad|
|[3SoJ3oq6Q1vAEy8o.htm](agents-of-edgewatch-bestiary-items/3SoJ3oq6Q1vAEy8o.htm)|Grab|auto-trad|
|[3sUb7J0bD92GSjki.htm](agents-of-edgewatch-bestiary-items/3sUb7J0bD92GSjki.htm)|Innate Occult Spells|auto-trad|
|[3TDeHZ2NkeZhRp86.htm](agents-of-edgewatch-bestiary-items/3TDeHZ2NkeZhRp86.htm)|Despair Ray|auto-trad|
|[3VSwbSPRGAYY8tg5.htm](agents-of-edgewatch-bestiary-items/3VSwbSPRGAYY8tg5.htm)|Songblade|auto-trad|
|[3vUeNTCG7OjIBa45.htm](agents-of-edgewatch-bestiary-items/3vUeNTCG7OjIBa45.htm)|Forced Regeneration|auto-trad|
|[3xd9C3zcdYxijFhU.htm](agents-of-edgewatch-bestiary-items/3xd9C3zcdYxijFhU.htm)|Trample|auto-trad|
|[46OKgjftfLLd55Dz.htm](agents-of-edgewatch-bestiary-items/46OKgjftfLLd55Dz.htm)|Katana|auto-trad|
|[4CShIdKgZz5cAVsW.htm](agents-of-edgewatch-bestiary-items/4CShIdKgZz5cAVsW.htm)|Occult Rituals|auto-trad|
|[4dEZdXNmp0qZ2uWV.htm](agents-of-edgewatch-bestiary-items/4dEZdXNmp0qZ2uWV.htm)|Darkvision|auto-trad|
|[4EqqjuXvtgi5CQMY.htm](agents-of-edgewatch-bestiary-items/4EqqjuXvtgi5CQMY.htm)|Skyward Slash|auto-trad|
|[4fuCvW4VqkpcxMz7.htm](agents-of-edgewatch-bestiary-items/4fuCvW4VqkpcxMz7.htm)|At-Will Spells|auto-trad|
|[4G2h2y2d0Zz3woFP.htm](agents-of-edgewatch-bestiary-items/4G2h2y2d0Zz3woFP.htm)|Wave|auto-trad|
|[4jItoqJFtYwZm9bE.htm](agents-of-edgewatch-bestiary-items/4jItoqJFtYwZm9bE.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[4LPTg9hErKNAmmv0.htm](agents-of-edgewatch-bestiary-items/4LPTg9hErKNAmmv0.htm)|Negative Healing|auto-trad|
|[4LuKFdOn742r53ct.htm](agents-of-edgewatch-bestiary-items/4LuKFdOn742r53ct.htm)|Dagger|auto-trad|
|[4mfr0BXdQQ9ZuRDr.htm](agents-of-edgewatch-bestiary-items/4mfr0BXdQQ9ZuRDr.htm)|Deepen the Wound|auto-trad|
|[4nJSWcGlNE3k6uH0.htm](agents-of-edgewatch-bestiary-items/4nJSWcGlNE3k6uH0.htm)|Black Ink Delirium|auto-trad|
|[4p4BNBKvDMPM8yRG.htm](agents-of-edgewatch-bestiary-items/4p4BNBKvDMPM8yRG.htm)|Cleric Domain Spells|auto-trad|
|[4QRkjb5aTrVTZYRO.htm](agents-of-edgewatch-bestiary-items/4QRkjb5aTrVTZYRO.htm)|Jealous Musician|auto-trad|
|[4RXnnP8CpvFi5RjK.htm](agents-of-edgewatch-bestiary-items/4RXnnP8CpvFi5RjK.htm)|All-Around Vision|auto-trad|
|[4T0hd15HFgEoLTEv.htm](agents-of-edgewatch-bestiary-items/4T0hd15HFgEoLTEv.htm)|Slow|auto-trad|
|[4TtLSpRhmqV1Dvp1.htm](agents-of-edgewatch-bestiary-items/4TtLSpRhmqV1Dvp1.htm)|Tail Whip|auto-trad|
|[4UhxcsjIkaaA0FnK.htm](agents-of-edgewatch-bestiary-items/4UhxcsjIkaaA0FnK.htm)|Telepathy 100 feet|auto-trad|
|[4UovGCnsjker1UNi.htm](agents-of-edgewatch-bestiary-items/4UovGCnsjker1UNi.htm)|Sneak Attack|auto-trad|
|[4uyjbjTFC0HefTzh.htm](agents-of-edgewatch-bestiary-items/4uyjbjTFC0HefTzh.htm)|Arcane Prepared Spells|auto-trad|
|[4VKUGZEqMWaJycr4.htm](agents-of-edgewatch-bestiary-items/4VKUGZEqMWaJycr4.htm)|Shield Block|auto-trad|
|[4Wo8cEyKXFYXahnb.htm](agents-of-edgewatch-bestiary-items/4Wo8cEyKXFYXahnb.htm)|Entropy Sense (Imprecise) 60 feet|auto-trad|
|[53IhzCwi4y8yO9XX.htm](agents-of-edgewatch-bestiary-items/53IhzCwi4y8yO9XX.htm)|Drain Bonded Item|auto-trad|
|[571EUWSacME4MxgN.htm](agents-of-edgewatch-bestiary-items/571EUWSacME4MxgN.htm)|Morningstar|auto-trad|
|[57lDKo6dcd3jNhcT.htm](agents-of-edgewatch-bestiary-items/57lDKo6dcd3jNhcT.htm)|Jaws|auto-trad|
|[5aBlY3dBYmIMAZwP.htm](agents-of-edgewatch-bestiary-items/5aBlY3dBYmIMAZwP.htm)|Swinging Chandelier|auto-trad|
|[5B9QSKBjBa9tC0SG.htm](agents-of-edgewatch-bestiary-items/5B9QSKBjBa9tC0SG.htm)|Stunning Strike|auto-trad|
|[5bx4G4sDLKNJ0Fim.htm](agents-of-edgewatch-bestiary-items/5bx4G4sDLKNJ0Fim.htm)|Frightful Presence|auto-trad|
|[5DSmWdkGY2DeiYD8.htm](agents-of-edgewatch-bestiary-items/5DSmWdkGY2DeiYD8.htm)|Attach|auto-trad|
|[5GTOdw4CBKeecBvX.htm](agents-of-edgewatch-bestiary-items/5GTOdw4CBKeecBvX.htm)|Spiked Chain|auto-trad|
|[5hCSuILSsdP2wKtN.htm](agents-of-edgewatch-bestiary-items/5hCSuILSsdP2wKtN.htm)|Cheek Pouches|auto-trad|
|[5HdfiD2BmZU6LzvL.htm](agents-of-edgewatch-bestiary-items/5HdfiD2BmZU6LzvL.htm)|Whirlwind Strike|auto-trad|
|[5Hk5r8bi7Nulj5pY.htm](agents-of-edgewatch-bestiary-items/5Hk5r8bi7Nulj5pY.htm)|Negative Healing|auto-trad|
|[5inXNyZGoLUMii53.htm](agents-of-edgewatch-bestiary-items/5inXNyZGoLUMii53.htm)|Slither|auto-trad|
|[5izc2RKfDYtjIicX.htm](agents-of-edgewatch-bestiary-items/5izc2RKfDYtjIicX.htm)|Rejuvenation|auto-trad|
|[5qAf1qV6aq6q3pQl.htm](agents-of-edgewatch-bestiary-items/5qAf1qV6aq6q3pQl.htm)|Little Favors|auto-trad|
|[5rYhqUb2YVDE98ag.htm](agents-of-edgewatch-bestiary-items/5rYhqUb2YVDE98ag.htm)|Warpwave Spell|auto-trad|
|[5vpT5j3f28sZPdRn.htm](agents-of-edgewatch-bestiary-items/5vpT5j3f28sZPdRn.htm)|Collapse|auto-trad|
|[5wnaaHpUx6f7lycE.htm](agents-of-edgewatch-bestiary-items/5wnaaHpUx6f7lycE.htm)|Low-Light Vision|auto-trad|
|[5wVIixUHmaim2A4K.htm](agents-of-edgewatch-bestiary-items/5wVIixUHmaim2A4K.htm)|Consume Flesh|auto-trad|
|[5Yk1UPJggdSuj7Kw.htm](agents-of-edgewatch-bestiary-items/5Yk1UPJggdSuj7Kw.htm)|Claw|auto-trad|
|[5Z1XSibUkx9LaIrv.htm](agents-of-edgewatch-bestiary-items/5Z1XSibUkx9LaIrv.htm)|Claws|auto-trad|
|[6BA0nR5VEDa0Aw15.htm](agents-of-edgewatch-bestiary-items/6BA0nR5VEDa0Aw15.htm)|Surface-Bound|auto-trad|
|[6cFodLsS9nMte5ts.htm](agents-of-edgewatch-bestiary-items/6cFodLsS9nMte5ts.htm)|Fill Tank|auto-trad|
|[6DnRipdoNRQWMUyZ.htm](agents-of-edgewatch-bestiary-items/6DnRipdoNRQWMUyZ.htm)|Spit|auto-trad|
|[6gBFZhqoL9eg2XDz.htm](agents-of-edgewatch-bestiary-items/6gBFZhqoL9eg2XDz.htm)|Collective Attack|auto-trad|
|[6hx8YYYNNoWo6xwu.htm](agents-of-edgewatch-bestiary-items/6hx8YYYNNoWo6xwu.htm)|Divine Innate Spells|auto-trad|
|[6M89EOsieWisWkK3.htm](agents-of-edgewatch-bestiary-items/6M89EOsieWisWkK3.htm)|Heavy Crossbow|auto-trad|
|[6nFnVyRyLOu3DldL.htm](agents-of-edgewatch-bestiary-items/6nFnVyRyLOu3DldL.htm)|Crossbow|auto-trad|
|[6Nu42xqROJzl0XrC.htm](agents-of-edgewatch-bestiary-items/6Nu42xqROJzl0XrC.htm)|Hampering Strike|auto-trad|
|[6pCE969IqFRVqLzJ.htm](agents-of-edgewatch-bestiary-items/6pCE969IqFRVqLzJ.htm)|Engulf|auto-trad|
|[6pcwqNViNq5cqn8U.htm](agents-of-edgewatch-bestiary-items/6pcwqNViNq5cqn8U.htm)|Claw|auto-trad|
|[6PRImWQWZEOuSAeI.htm](agents-of-edgewatch-bestiary-items/6PRImWQWZEOuSAeI.htm)|Dagger|auto-trad|
|[6Tw3xiLUkr4AYhGN.htm](agents-of-edgewatch-bestiary-items/6Tw3xiLUkr4AYhGN.htm)|Quick Bomber|auto-trad|
|[6u1eMdIQZV0dWPQU.htm](agents-of-edgewatch-bestiary-items/6u1eMdIQZV0dWPQU.htm)|Opportune Dodge|auto-trad|
|[6YAq6Oj3jOtxCRWq.htm](agents-of-edgewatch-bestiary-items/6YAq6Oj3jOtxCRWq.htm)|Claw|auto-trad|
|[70miglztK7B97hcW.htm](agents-of-edgewatch-bestiary-items/70miglztK7B97hcW.htm)|Darkvision|auto-trad|
|[71ypVkmQCnSo1C8X.htm](agents-of-edgewatch-bestiary-items/71ypVkmQCnSo1C8X.htm)|Claw|auto-trad|
|[735O6tGVpk8PcXiV.htm](agents-of-edgewatch-bestiary-items/735O6tGVpk8PcXiV.htm)|Innate Arcane Spells|auto-trad|
|[745PY8vNnU4dRB3J.htm](agents-of-edgewatch-bestiary-items/745PY8vNnU4dRB3J.htm)|Diabolic Certitude|auto-trad|
|[76fFiqfOfbkyXm8t.htm](agents-of-edgewatch-bestiary-items/76fFiqfOfbkyXm8t.htm)|Spiked Chain|auto-trad|
|[79KeB3EaNuz3Wjd2.htm](agents-of-edgewatch-bestiary-items/79KeB3EaNuz3Wjd2.htm)|Spatial Riptide|auto-trad|
|[7ArMrPYZDTroaUsN.htm](agents-of-edgewatch-bestiary-items/7ArMrPYZDTroaUsN.htm)|At-Will Spells|auto-trad|
|[7biZlvn3vY2e7oEj.htm](agents-of-edgewatch-bestiary-items/7biZlvn3vY2e7oEj.htm)|Divert Strike|auto-trad|
|[7GSRG0kO6Z9IWhXQ.htm](agents-of-edgewatch-bestiary-items/7GSRG0kO6Z9IWhXQ.htm)|Attack of Opportunity|auto-trad|
|[7iz6trwOzT3IfpHj.htm](agents-of-edgewatch-bestiary-items/7iz6trwOzT3IfpHj.htm)|Constant Spells|auto-trad|
|[7KBnvkWiiKe2PzQ5.htm](agents-of-edgewatch-bestiary-items/7KBnvkWiiKe2PzQ5.htm)|Horn|auto-trad|
|[7kENjgZrG04Exwhk.htm](agents-of-edgewatch-bestiary-items/7kENjgZrG04Exwhk.htm)|Telepathy 100 Feet|auto-trad|
|[7kep2BZKcI8m29QV.htm](agents-of-edgewatch-bestiary-items/7kep2BZKcI8m29QV.htm)|Rejuvenation|auto-trad|
|[7l3dJqmfIwNmUyfj.htm](agents-of-edgewatch-bestiary-items/7l3dJqmfIwNmUyfj.htm)|Negative Healing|auto-trad|
|[7m8XF9B86oB7by1T.htm](agents-of-edgewatch-bestiary-items/7m8XF9B86oB7by1T.htm)|Attack of Opportunity|auto-trad|
|[7OjXQSbCokE3yjiM.htm](agents-of-edgewatch-bestiary-items/7OjXQSbCokE3yjiM.htm)|Shovel|auto-trad|
|[7PJ0xQKU3T5EtaPW.htm](agents-of-edgewatch-bestiary-items/7PJ0xQKU3T5EtaPW.htm)|Heaven's Thunder|auto-trad|
|[7qqes4mzlHueunJX.htm](agents-of-edgewatch-bestiary-items/7qqes4mzlHueunJX.htm)|Deep Breath|auto-trad|
|[7RZClAttcFohXVTd.htm](agents-of-edgewatch-bestiary-items/7RZClAttcFohXVTd.htm)|Little Favors|auto-trad|
|[7UeREmeUuipS0pKu.htm](agents-of-edgewatch-bestiary-items/7UeREmeUuipS0pKu.htm)|Dart|auto-trad|
|[7UsBYA3xYXL2TFT2.htm](agents-of-edgewatch-bestiary-items/7UsBYA3xYXL2TFT2.htm)|Darkvision|auto-trad|
|[7vOzE1FHZEotvswQ.htm](agents-of-edgewatch-bestiary-items/7vOzE1FHZEotvswQ.htm)|+2 Status Bonus on Saves vs. Shove|auto-trad|
|[84E8AqCBiRIBEHuG.htm](agents-of-edgewatch-bestiary-items/84E8AqCBiRIBEHuG.htm)|Dagger|auto-trad|
|[87HtA52J9Oqfjsft.htm](agents-of-edgewatch-bestiary-items/87HtA52J9Oqfjsft.htm)|Illusory Persona|auto-trad|
|[8ADZbtdcgOMgnF3e.htm](agents-of-edgewatch-bestiary-items/8ADZbtdcgOMgnF3e.htm)|Occult Ritual|auto-trad|
|[8aw6p0tL8IYj3lNK.htm](agents-of-edgewatch-bestiary-items/8aw6p0tL8IYj3lNK.htm)|Tail|auto-trad|
|[8bbFiMBoHc5BG8QA.htm](agents-of-edgewatch-bestiary-items/8bbFiMBoHc5BG8QA.htm)|Darkvision|auto-trad|
|[8dM3dH69X49kSo82.htm](agents-of-edgewatch-bestiary-items/8dM3dH69X49kSo82.htm)|Dagger|auto-trad|
|[8IxYcWmidpRnNMrc.htm](agents-of-edgewatch-bestiary-items/8IxYcWmidpRnNMrc.htm)|Divine Innate Spells|auto-trad|
|[8j0TGPf1kZlcIwpn.htm](agents-of-edgewatch-bestiary-items/8j0TGPf1kZlcIwpn.htm)|Steal Memories|auto-trad|
|[8JslG7YLjptMOVC0.htm](agents-of-edgewatch-bestiary-items/8JslG7YLjptMOVC0.htm)|Sudden Betrayal|auto-trad|
|[8JzS4wK4EWHWFJlY.htm](agents-of-edgewatch-bestiary-items/8JzS4wK4EWHWFJlY.htm)|Chopper|auto-trad|
|[8K2BcPcACmOHHydT.htm](agents-of-edgewatch-bestiary-items/8K2BcPcACmOHHydT.htm)|Dreadsong|auto-trad|
|[8k84XkMz9ZT6F3TB.htm](agents-of-edgewatch-bestiary-items/8k84XkMz9ZT6F3TB.htm)|Rapier|auto-trad|
|[8kuBPd4vWsUhaCao.htm](agents-of-edgewatch-bestiary-items/8kuBPd4vWsUhaCao.htm)|Blinding Stream|auto-trad|
|[8N0oYLqMb0kjQ009.htm](agents-of-edgewatch-bestiary-items/8N0oYLqMb0kjQ009.htm)|Wizard School Spells|auto-trad|
|[8NRxk6m07y9TpopE.htm](agents-of-edgewatch-bestiary-items/8NRxk6m07y9TpopE.htm)|Sneak Attack|auto-trad|
|[8QIEpGKWvwKK8mSZ.htm](agents-of-edgewatch-bestiary-items/8QIEpGKWvwKK8mSZ.htm)|Scraping Clamor|auto-trad|
|[8QnTTWcdqDNyOtIl.htm](agents-of-edgewatch-bestiary-items/8QnTTWcdqDNyOtIl.htm)|Miasma of Pollution|auto-trad|
|[8Qz8Y3OYthQCPvSH.htm](agents-of-edgewatch-bestiary-items/8Qz8Y3OYthQCPvSH.htm)|Constant Spells|auto-trad|
|[8RfMfocN311lqv8N.htm](agents-of-edgewatch-bestiary-items/8RfMfocN311lqv8N.htm)|Spell-Imbued Blade|auto-trad|
|[8RQS62riVfrsiZjr.htm](agents-of-edgewatch-bestiary-items/8RQS62riVfrsiZjr.htm)|Radiant Ray|auto-trad|
|[8rukLsOFemgZkuGO.htm](agents-of-edgewatch-bestiary-items/8rukLsOFemgZkuGO.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[8sPdH0UwMRq9RTHB.htm](agents-of-edgewatch-bestiary-items/8sPdH0UwMRq9RTHB.htm)|Darkvision|auto-trad|
|[8tU7XeCCuWdlIydb.htm](agents-of-edgewatch-bestiary-items/8tU7XeCCuWdlIydb.htm)|Fist|auto-trad|
|[8ubCXo0Jx35U5xxc.htm](agents-of-edgewatch-bestiary-items/8ubCXo0Jx35U5xxc.htm)|Golem Antimagic|auto-trad|
|[8xAxPvW0rww5NovC.htm](agents-of-edgewatch-bestiary-items/8xAxPvW0rww5NovC.htm)|Greater Warpwave Strike|auto-trad|
|[90dz4KzJNBG1WfGw.htm](agents-of-edgewatch-bestiary-items/90dz4KzJNBG1WfGw.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[92BXBHbmRG68ifPd.htm](agents-of-edgewatch-bestiary-items/92BXBHbmRG68ifPd.htm)|Amplify Spell|auto-trad|
|[933yJ4bfZhr9zTIX.htm](agents-of-edgewatch-bestiary-items/933yJ4bfZhr9zTIX.htm)|Collapse|auto-trad|
|[95kvqPPSJNio8vAK.htm](agents-of-edgewatch-bestiary-items/95kvqPPSJNio8vAK.htm)|Berserk|auto-trad|
|[97KS2uPGBbRl3fOQ.htm](agents-of-edgewatch-bestiary-items/97KS2uPGBbRl3fOQ.htm)|Engulf|auto-trad|
|[9Af2sRyOtaTDufyy.htm](agents-of-edgewatch-bestiary-items/9Af2sRyOtaTDufyy.htm)|Maul|auto-trad|
|[9cRBZZnKohlEmRzR.htm](agents-of-edgewatch-bestiary-items/9cRBZZnKohlEmRzR.htm)|Dagger|auto-trad|
|[9duTJhK5paVZrMkr.htm](agents-of-edgewatch-bestiary-items/9duTJhK5paVZrMkr.htm)|At-Will Spells|auto-trad|
|[9dxZ2UN0t2f32r0f.htm](agents-of-edgewatch-bestiary-items/9dxZ2UN0t2f32r0f.htm)|Bury in Offal|auto-trad|
|[9fvVPY7pFKhpLQwU.htm](agents-of-edgewatch-bestiary-items/9fvVPY7pFKhpLQwU.htm)|Divine Innate Spells|auto-trad|
|[9gEiH150yl4bKC6n.htm](agents-of-edgewatch-bestiary-items/9gEiH150yl4bKC6n.htm)|Divine Innate Spells|auto-trad|
|[9gGdxZjwIsTVIuxu.htm](agents-of-edgewatch-bestiary-items/9gGdxZjwIsTVIuxu.htm)|Alchemical Formulas (5th)|auto-trad|
|[9HOzCgOAKeHP5EhB.htm](agents-of-edgewatch-bestiary-items/9HOzCgOAKeHP5EhB.htm)|Venom Explosion|auto-trad|
|[9hvKp9EJbq6Log0q.htm](agents-of-edgewatch-bestiary-items/9hvKp9EJbq6Log0q.htm)|Consume Flesh|auto-trad|
|[9jJY47nOXFMztxEo.htm](agents-of-edgewatch-bestiary-items/9jJY47nOXFMztxEo.htm)|Divine Innate Spells|auto-trad|
|[9LK5ORluncHekkZ5.htm](agents-of-edgewatch-bestiary-items/9LK5ORluncHekkZ5.htm)|Swarming Bites|auto-trad|
|[9m6cGcYmQptppNHf.htm](agents-of-edgewatch-bestiary-items/9m6cGcYmQptppNHf.htm)|Change Shape|auto-trad|
|[9n6E9GRbgNOMXBmN.htm](agents-of-edgewatch-bestiary-items/9n6E9GRbgNOMXBmN.htm)|Grab|auto-trad|
|[9oInACsH1lSDnaHW.htm](agents-of-edgewatch-bestiary-items/9oInACsH1lSDnaHW.htm)|Spinning Blade|auto-trad|
|[9ShSRecAk97rJkLS.htm](agents-of-edgewatch-bestiary-items/9ShSRecAk97rJkLS.htm)|Motion Sense 60 feet|auto-trad|
|[9sMCsySJHETUYLfH.htm](agents-of-edgewatch-bestiary-items/9sMCsySJHETUYLfH.htm)|Staff|auto-trad|
|[9WqBdZ30eNWIpXaC.htm](agents-of-edgewatch-bestiary-items/9WqBdZ30eNWIpXaC.htm)|Fangs|auto-trad|
|[A04N1HzBOQCcYnM2.htm](agents-of-edgewatch-bestiary-items/A04N1HzBOQCcYnM2.htm)|Blackfinger Blight|auto-trad|
|[a1Es9ETdZdfagQRX.htm](agents-of-edgewatch-bestiary-items/a1Es9ETdZdfagQRX.htm)|Divine Innate Spells|auto-trad|
|[A2LL8i2v8LsE6Ilp.htm](agents-of-edgewatch-bestiary-items/A2LL8i2v8LsE6Ilp.htm)|Cleaver|auto-trad|
|[a34FGdPF5cxVbX4J.htm](agents-of-edgewatch-bestiary-items/a34FGdPF5cxVbX4J.htm)|Greater Darkvision|auto-trad|
|[A4evoVscksRyehl1.htm](agents-of-edgewatch-bestiary-items/A4evoVscksRyehl1.htm)|Greater Planar Rift|auto-trad|
|[A4o54wh2xPLY1V7q.htm](agents-of-edgewatch-bestiary-items/A4o54wh2xPLY1V7q.htm)|Composite Shortbow|auto-trad|
|[a5d4jfdl2s8CiAsa.htm](agents-of-edgewatch-bestiary-items/a5d4jfdl2s8CiAsa.htm)|Breath of Lies|auto-trad|
|[A6EBzxn9xWiWavNB.htm](agents-of-edgewatch-bestiary-items/A6EBzxn9xWiWavNB.htm)|Telepathy 100 feet|auto-trad|
|[a6jNbmkq4m7CmbSA.htm](agents-of-edgewatch-bestiary-items/a6jNbmkq4m7CmbSA.htm)|Lawkeeper|auto-trad|
|[A7VgvFTQFpm1Cc79.htm](agents-of-edgewatch-bestiary-items/A7VgvFTQFpm1Cc79.htm)|Smoke Bomb Launcher|auto-trad|
|[A8r84vdl4zcvpCMg.htm](agents-of-edgewatch-bestiary-items/A8r84vdl4zcvpCMg.htm)|Free Blade|auto-trad|
|[aap2jSjnDkxEKZUI.htm](agents-of-edgewatch-bestiary-items/aap2jSjnDkxEKZUI.htm)|Fist|auto-trad|
|[abpRv79lZLaukj8M.htm](agents-of-edgewatch-bestiary-items/abpRv79lZLaukj8M.htm)|Warhammer|auto-trad|
|[aBQGxKqOuBqF1FkF.htm](agents-of-edgewatch-bestiary-items/aBQGxKqOuBqF1FkF.htm)|Double Stab|auto-trad|
|[ACezcH9GTIcXGlSB.htm](agents-of-edgewatch-bestiary-items/ACezcH9GTIcXGlSB.htm)|Mortal Shell|auto-trad|
|[AD2GrziGoOFfzrk7.htm](agents-of-edgewatch-bestiary-items/AD2GrziGoOFfzrk7.htm)|Contingency Plan|auto-trad|
|[aGg1kw2fRdZFdeew.htm](agents-of-edgewatch-bestiary-items/aGg1kw2fRdZFdeew.htm)|Stone Fist|auto-trad|
|[AHGv0wFC9jb2bwlQ.htm](agents-of-edgewatch-bestiary-items/AHGv0wFC9jb2bwlQ.htm)|Claw|auto-trad|
|[AIJka2Zew5I1e7Lo.htm](agents-of-edgewatch-bestiary-items/AIJka2Zew5I1e7Lo.htm)|Attack of Opportunity|auto-trad|
|[AiOnTNJ8o76YiJze.htm](agents-of-edgewatch-bestiary-items/AiOnTNJ8o76YiJze.htm)|Attack of Opportunity|auto-trad|
|[aJI86W6ShmBc7gPN.htm](agents-of-edgewatch-bestiary-items/aJI86W6ShmBc7gPN.htm)|Entropy Sense (Imprecise) 120 feet|auto-trad|
|[akEAcbm2fRfGZg8v.htm](agents-of-edgewatch-bestiary-items/akEAcbm2fRfGZg8v.htm)|Conjure Swords|auto-trad|
|[AkqOlXLB6K0JLoqz.htm](agents-of-edgewatch-bestiary-items/AkqOlXLB6K0JLoqz.htm)|Wail of the Betrayed|auto-trad|
|[aMNM0X0dVvVRdlZT.htm](agents-of-edgewatch-bestiary-items/aMNM0X0dVvVRdlZT.htm)|Vulnerable to Neutralize Poison|auto-trad|
|[ANZcdjjZfZOfuJYL.htm](agents-of-edgewatch-bestiary-items/ANZcdjjZfZOfuJYL.htm)|Jaws|auto-trad|
|[apIdQFYWsMBgRVKk.htm](agents-of-edgewatch-bestiary-items/apIdQFYWsMBgRVKk.htm)|Pincer|auto-trad|
|[apKzqJtSb9Y34OBk.htm](agents-of-edgewatch-bestiary-items/apKzqJtSb9Y34OBk.htm)|Attack of Opportunity|auto-trad|
|[AQMbWGVUpkE6QLbs.htm](agents-of-edgewatch-bestiary-items/AQMbWGVUpkE6QLbs.htm)|Kiss|auto-trad|
|[aqwHyoUR6cj9y6s8.htm](agents-of-edgewatch-bestiary-items/aqwHyoUR6cj9y6s8.htm)|Focused Gaze|auto-trad|
|[aSPT9O0IpzqjiqG0.htm](agents-of-edgewatch-bestiary-items/aSPT9O0IpzqjiqG0.htm)|Seal Room|auto-trad|
|[aSvIfWvkrhDYQZn3.htm](agents-of-edgewatch-bestiary-items/aSvIfWvkrhDYQZn3.htm)|Regeneration 20 (Deactivated by Fire or Acid)|auto-trad|
|[AsXbDxRSKFmiQ6jb.htm](agents-of-edgewatch-bestiary-items/AsXbDxRSKFmiQ6jb.htm)|Enfeebling Bite|auto-trad|
|[aSzrKKXR7hl8ZVhX.htm](agents-of-edgewatch-bestiary-items/aSzrKKXR7hl8ZVhX.htm)|Stone Spike|auto-trad|
|[at6MabtHqhZPINQ3.htm](agents-of-edgewatch-bestiary-items/at6MabtHqhZPINQ3.htm)|Suction|auto-trad|
|[AT8OH6P1ZPmkA0CR.htm](agents-of-edgewatch-bestiary-items/AT8OH6P1ZPmkA0CR.htm)|Paralysis|auto-trad|
|[AuIi30v2cCwEK9Oy.htm](agents-of-edgewatch-bestiary-items/AuIi30v2cCwEK9Oy.htm)|Flog Mercilessly|auto-trad|
|[AV34BEM8xG7BwDYs.htm](agents-of-edgewatch-bestiary-items/AV34BEM8xG7BwDYs.htm)|Crossbow|auto-trad|
|[Av9NysCBnGty9we0.htm](agents-of-edgewatch-bestiary-items/Av9NysCBnGty9we0.htm)|Furious Wallop|auto-trad|
|[aVFV0hNNlEPetZWI.htm](agents-of-edgewatch-bestiary-items/aVFV0hNNlEPetZWI.htm)|Poisoner's Staff (Major)|auto-trad|
|[AvJUZJ1PD1HyuHdf.htm](agents-of-edgewatch-bestiary-items/AvJUZJ1PD1HyuHdf.htm)|Crossbow|auto-trad|
|[aVyixje5UBQb21sX.htm](agents-of-edgewatch-bestiary-items/aVyixje5UBQb21sX.htm)|Divine Innate Spells|auto-trad|
|[awh4If0CSlamWfUO.htm](agents-of-edgewatch-bestiary-items/awh4If0CSlamWfUO.htm)|Improved Grab|auto-trad|
|[awO8dArKZVWslq53.htm](agents-of-edgewatch-bestiary-items/awO8dArKZVWslq53.htm)|Claw|auto-trad|
|[aZlGemIJeXm9jJEE.htm](agents-of-edgewatch-bestiary-items/aZlGemIJeXm9jJEE.htm)|Knife Thrower|auto-trad|
|[aZvRC9a03MwZf7rr.htm](agents-of-edgewatch-bestiary-items/aZvRC9a03MwZf7rr.htm)|Frightening Critical|auto-trad|
|[b0iAXGEWNNI2u5Tg.htm](agents-of-edgewatch-bestiary-items/b0iAXGEWNNI2u5Tg.htm)|War Razor|auto-trad|
|[B0YPC9dAo09oqw5y.htm](agents-of-edgewatch-bestiary-items/B0YPC9dAo09oqw5y.htm)|Innate Occult Spells|auto-trad|
|[b1QZ33BjtQXdUVJj.htm](agents-of-edgewatch-bestiary-items/b1QZ33BjtQXdUVJj.htm)|Cannon Arm|auto-trad|
|[B3Ppv6QafImenNGu.htm](agents-of-edgewatch-bestiary-items/B3Ppv6QafImenNGu.htm)|Claw|auto-trad|
|[B6M6fTBtJwAQDYCs.htm](agents-of-edgewatch-bestiary-items/B6M6fTBtJwAQDYCs.htm)|Swarm Mind|auto-trad|
|[B8N51wc1bGZzdZ7d.htm](agents-of-edgewatch-bestiary-items/B8N51wc1bGZzdZ7d.htm)|Hateful Gaze|auto-trad|
|[b8sV2RLnt3RmLO2t.htm](agents-of-edgewatch-bestiary-items/b8sV2RLnt3RmLO2t.htm)|Upward Stab|auto-trad|
|[b9gVzcE3UM4ixbBV.htm](agents-of-edgewatch-bestiary-items/b9gVzcE3UM4ixbBV.htm)|Tendrils Come Alive|auto-trad|
|[Ba9gUN2kmGTDwJ7v.htm](agents-of-edgewatch-bestiary-items/Ba9gUN2kmGTDwJ7v.htm)|Bloody Mayhem|auto-trad|
|[BasU6Iuc7hRxhoYG.htm](agents-of-edgewatch-bestiary-items/BasU6Iuc7hRxhoYG.htm)|Form Tool|auto-trad|
|[BcDHhgCwCxAUCy1q.htm](agents-of-edgewatch-bestiary-items/BcDHhgCwCxAUCy1q.htm)|Jaws|auto-trad|
|[BdWPHFjkOXFGTDgi.htm](agents-of-edgewatch-bestiary-items/BdWPHFjkOXFGTDgi.htm)|Attack of Opportunity|auto-trad|
|[BeG2KYyzwrCfE6KK.htm](agents-of-edgewatch-bestiary-items/BeG2KYyzwrCfE6KK.htm)|Warpwave Strike|auto-trad|
|[BEoBeAAFDzMQN6GW.htm](agents-of-edgewatch-bestiary-items/BEoBeAAFDzMQN6GW.htm)|Darkvision|auto-trad|
|[bfsuL0uIFF1F4Guy.htm](agents-of-edgewatch-bestiary-items/bfsuL0uIFF1F4Guy.htm)|Fangs|auto-trad|
|[BGk06cTCmFh4ZooN.htm](agents-of-edgewatch-bestiary-items/BGk06cTCmFh4ZooN.htm)|Telepathy 100 feet|auto-trad|
|[BHZkmFQkiTD4QBd2.htm](agents-of-edgewatch-bestiary-items/BHZkmFQkiTD4QBd2.htm)|Dagger|auto-trad|
|[bi6VcgRWUdub6hl4.htm](agents-of-edgewatch-bestiary-items/bi6VcgRWUdub6hl4.htm)|Subsume|auto-trad|
|[Bi7jDnQux0YzmGpg.htm](agents-of-edgewatch-bestiary-items/Bi7jDnQux0YzmGpg.htm)|Telepathy 100 feet|auto-trad|
|[biy9pcVadULFQhYw.htm](agents-of-edgewatch-bestiary-items/biy9pcVadULFQhYw.htm)|Reaper's Lancet Sheath|auto-trad|
|[Blq1uGh1lHrneL8W.htm](agents-of-edgewatch-bestiary-items/Blq1uGh1lHrneL8W.htm)|Constant Spells|auto-trad|
|[bLsJ5WlwMamd2QGg.htm](agents-of-edgewatch-bestiary-items/bLsJ5WlwMamd2QGg.htm)|Rejuvenation|auto-trad|
|[Bm63scXZG8oZql5K.htm](agents-of-edgewatch-bestiary-items/Bm63scXZG8oZql5K.htm)|Fist|auto-trad|
|[bmbqZlQmEmeXm2DS.htm](agents-of-edgewatch-bestiary-items/bmbqZlQmEmeXm2DS.htm)|Treacherous Veil|auto-trad|
|[bplHxNRZHOun4UiH.htm](agents-of-edgewatch-bestiary-items/bplHxNRZHOun4UiH.htm)|Hand Crossbow|auto-trad|
|[bpTCeqf0WRsdxC9t.htm](agents-of-edgewatch-bestiary-items/bpTCeqf0WRsdxC9t.htm)|Venom Stream|auto-trad|
|[bqTN2IAncaogzgo9.htm](agents-of-edgewatch-bestiary-items/bqTN2IAncaogzgo9.htm)|Weapon Master|auto-trad|
|[BrOfryWmR4ITLzIV.htm](agents-of-edgewatch-bestiary-items/BrOfryWmR4ITLzIV.htm)|Flame Dart|auto-trad|
|[bSa3Y6h5y9hcXmYa.htm](agents-of-edgewatch-bestiary-items/bSa3Y6h5y9hcXmYa.htm)|Katar|auto-trad|
|[bsbrnl0pxRuL6acz.htm](agents-of-edgewatch-bestiary-items/bsbrnl0pxRuL6acz.htm)|Hose|auto-trad|
|[BSfiT0dLYi9Xp9gb.htm](agents-of-edgewatch-bestiary-items/BSfiT0dLYi9Xp9gb.htm)|Divine Prepared Spells|auto-trad|
|[BsQZslLQ0FJcYne3.htm](agents-of-edgewatch-bestiary-items/BsQZslLQ0FJcYne3.htm)|Reshape Reality|auto-trad|
|[BT64pCzbpph5pC4r.htm](agents-of-edgewatch-bestiary-items/BT64pCzbpph5pC4r.htm)|Darkvision|auto-trad|
|[BTVYmpw94v3oWTGl.htm](agents-of-edgewatch-bestiary-items/BTVYmpw94v3oWTGl.htm)|Constant Spells|auto-trad|
|[bVqJIi6r8cTALSZa.htm](agents-of-edgewatch-bestiary-items/bVqJIi6r8cTALSZa.htm)|Dagger|auto-trad|
|[BW9xnDoY4BohVAGZ.htm](agents-of-edgewatch-bestiary-items/BW9xnDoY4BohVAGZ.htm)|Focus Spells|auto-trad|
|[bWtvDdGa2KvPHNmx.htm](agents-of-edgewatch-bestiary-items/bWtvDdGa2KvPHNmx.htm)|Grab|auto-trad|
|[byUSs0eF5Ctqs4xY.htm](agents-of-edgewatch-bestiary-items/byUSs0eF5Ctqs4xY.htm)|Sea Hag's Bargain|auto-trad|
|[bZAhdsbYFuZ2syFl.htm](agents-of-edgewatch-bestiary-items/bZAhdsbYFuZ2syFl.htm)|Easy to Call|auto-trad|
|[C01m9GnZW7b5Uk9K.htm](agents-of-edgewatch-bestiary-items/C01m9GnZW7b5Uk9K.htm)|Shadow Step|auto-trad|
|[C2rrPgxtXj2SQdVM.htm](agents-of-edgewatch-bestiary-items/C2rrPgxtXj2SQdVM.htm)|Dagger|auto-trad|
|[C5DSJUvGx6T45XYO.htm](agents-of-edgewatch-bestiary-items/C5DSJUvGx6T45XYO.htm)|Attack of Opportunity (Stinger Only)|auto-trad|
|[c6pExwVTDS5Qyrw4.htm](agents-of-edgewatch-bestiary-items/c6pExwVTDS5Qyrw4.htm)|Constant Spells|auto-trad|
|[C8jCG9WTW2fP6H0b.htm](agents-of-edgewatch-bestiary-items/C8jCG9WTW2fP6H0b.htm)|Poison Frenzy|auto-trad|
|[C8jd840fpmYd3wDB.htm](agents-of-edgewatch-bestiary-items/C8jd840fpmYd3wDB.htm)|Suffocating Fumes|auto-trad|
|[CaQuzIFC9BNqHDUQ.htm](agents-of-edgewatch-bestiary-items/CaQuzIFC9BNqHDUQ.htm)|Dagger|auto-trad|
|[CBZ2xBopPRbaMVV0.htm](agents-of-edgewatch-bestiary-items/CBZ2xBopPRbaMVV0.htm)|Capsize|auto-trad|
|[CcCVlFjqLYk2DTlc.htm](agents-of-edgewatch-bestiary-items/CcCVlFjqLYk2DTlc.htm)|+1 Status to Will Saves vs. Mental|auto-trad|
|[CCz3eaJWNZAPwqBS.htm](agents-of-edgewatch-bestiary-items/CCz3eaJWNZAPwqBS.htm)|Master Brawler|auto-trad|
|[cdoPISuZdSbxOwT1.htm](agents-of-edgewatch-bestiary-items/cdoPISuZdSbxOwT1.htm)|Spell Choke|auto-trad|
|[ce1K4Mc00gFvS1v8.htm](agents-of-edgewatch-bestiary-items/ce1K4Mc00gFvS1v8.htm)|Telepathy 100 feet|auto-trad|
|[cg0RVhBfO8MxjPpI.htm](agents-of-edgewatch-bestiary-items/cg0RVhBfO8MxjPpI.htm)|Superior Senses|auto-trad|
|[CgEMdYYlkqnUPBfB.htm](agents-of-edgewatch-bestiary-items/CgEMdYYlkqnUPBfB.htm)|Perfect Will|auto-trad|
|[chuMKB6cHYONy4NC.htm](agents-of-edgewatch-bestiary-items/chuMKB6cHYONy4NC.htm)|Toxic Gas|auto-trad|
|[CIh6yDe29eBqfdsD.htm](agents-of-edgewatch-bestiary-items/CIh6yDe29eBqfdsD.htm)|Haphazard Hack|auto-trad|
|[ciQ62q9OynsChQSL.htm](agents-of-edgewatch-bestiary-items/ciQ62q9OynsChQSL.htm)|Waves of Sorrow|auto-trad|
|[CK9zFRMfz5AvtehE.htm](agents-of-edgewatch-bestiary-items/CK9zFRMfz5AvtehE.htm)|Focus Spells|auto-trad|
|[CkJTSqPlqI5Fj2b7.htm](agents-of-edgewatch-bestiary-items/CkJTSqPlqI5Fj2b7.htm)|Mirror Hand|auto-trad|
|[cNdgAyzrxvT3Z68j.htm](agents-of-edgewatch-bestiary-items/cNdgAyzrxvT3Z68j.htm)|+2 Status to All Saves vs. Composition Spells|auto-trad|
|[cneQw5areaRAqZhr.htm](agents-of-edgewatch-bestiary-items/cneQw5areaRAqZhr.htm)|Shortsword|auto-trad|
|[cNLxEiiU06b04C55.htm](agents-of-edgewatch-bestiary-items/cNLxEiiU06b04C55.htm)|Tentacle|auto-trad|
|[cny7NT0gNZmXhDv6.htm](agents-of-edgewatch-bestiary-items/cny7NT0gNZmXhDv6.htm)|Tentacle|auto-trad|
|[CpdC4SJqhh9UFhSi.htm](agents-of-edgewatch-bestiary-items/CpdC4SJqhh9UFhSi.htm)|Jaws|auto-trad|
|[cSFNzQ52byIdvdWb.htm](agents-of-edgewatch-bestiary-items/cSFNzQ52byIdvdWb.htm)|Broken Quills|auto-trad|
|[Ct2gfJE7OAo8ZR44.htm](agents-of-edgewatch-bestiary-items/Ct2gfJE7OAo8ZR44.htm)|Constant Spells|auto-trad|
|[Ctkj7r3YYAFSQEKx.htm](agents-of-edgewatch-bestiary-items/Ctkj7r3YYAFSQEKx.htm)|Disgorge Portal|auto-trad|
|[CU5CXS5c5trwAd9g.htm](agents-of-edgewatch-bestiary-items/CU5CXS5c5trwAd9g.htm)|Steady Spellcasting|auto-trad|
|[CUYM17Clq1YTEBxz.htm](agents-of-edgewatch-bestiary-items/CUYM17Clq1YTEBxz.htm)|Maddening Whispers|auto-trad|
|[cvCrwykv9kKHfIwG.htm](agents-of-edgewatch-bestiary-items/cvCrwykv9kKHfIwG.htm)|Darkvision|auto-trad|
|[CVEw8LgJymgw5uI2.htm](agents-of-edgewatch-bestiary-items/CVEw8LgJymgw5uI2.htm)|Rend|auto-trad|
|[CVTZnv3ZcE4LbDPg.htm](agents-of-edgewatch-bestiary-items/CVTZnv3ZcE4LbDPg.htm)|Breath Weapon|auto-trad|
|[CWcYZHN9MSfxTR2k.htm](agents-of-edgewatch-bestiary-items/CWcYZHN9MSfxTR2k.htm)|Sneak Attack|auto-trad|
|[CwNHaEk7qrMHHyza.htm](agents-of-edgewatch-bestiary-items/CwNHaEk7qrMHHyza.htm)|Constant Spells|auto-trad|
|[CWOEh5RnFG5cysRk.htm](agents-of-edgewatch-bestiary-items/CWOEh5RnFG5cysRk.htm)|Tentacle Trip|auto-trad|
|[cX2EH9YzIFFgfGra.htm](agents-of-edgewatch-bestiary-items/cX2EH9YzIFFgfGra.htm)|Dagger|auto-trad|
|[cXt1NhRBZEXIv1ju.htm](agents-of-edgewatch-bestiary-items/cXt1NhRBZEXIv1ju.htm)|Telepathy 100 feet|auto-trad|
|[cYpzRpaINHeW2XxC.htm](agents-of-edgewatch-bestiary-items/cYpzRpaINHeW2XxC.htm)|Poison Weapon|auto-trad|
|[czNwyIcrJKwQNfoZ.htm](agents-of-edgewatch-bestiary-items/czNwyIcrJKwQNfoZ.htm)|Constant Spells|auto-trad|
|[D27lYuRNmEYB5vjo.htm](agents-of-edgewatch-bestiary-items/D27lYuRNmEYB5vjo.htm)|Wipe Away Cracks|auto-trad|
|[DBzG2AKUenn28L2w.htm](agents-of-edgewatch-bestiary-items/DBzG2AKUenn28L2w.htm)|War Razor|auto-trad|
|[dcDZ5fYx6PsHPo4t.htm](agents-of-edgewatch-bestiary-items/dcDZ5fYx6PsHPo4t.htm)|At-Will Spells|auto-trad|
|[DdFilDnuHDObT81V.htm](agents-of-edgewatch-bestiary-items/DdFilDnuHDObT81V.htm)|Freeze Floor|auto-trad|
|[dDvj6Zila83uSsch.htm](agents-of-edgewatch-bestiary-items/dDvj6Zila83uSsch.htm)|Flying Blade|auto-trad|
|[deQl75TOphyii6YU.htm](agents-of-edgewatch-bestiary-items/deQl75TOphyii6YU.htm)|Planar Coven|auto-trad|
|[DF3OKBUIdabo7i9j.htm](agents-of-edgewatch-bestiary-items/DF3OKBUIdabo7i9j.htm)|Divine Innate Spells|auto-trad|
|[dff7pIEmGTfCShQo.htm](agents-of-edgewatch-bestiary-items/dff7pIEmGTfCShQo.htm)|Hardness 5|auto-trad|
|[dffHcWQsvtoeLOID.htm](agents-of-edgewatch-bestiary-items/dffHcWQsvtoeLOID.htm)|Divine Rituals|auto-trad|
|[dfm82amOgBxSgH3m.htm](agents-of-edgewatch-bestiary-items/dfm82amOgBxSgH3m.htm)|Longspear|auto-trad|
|[DGsILrvl3LJIeZIu.htm](agents-of-edgewatch-bestiary-items/DGsILrvl3LJIeZIu.htm)|Enshroud|auto-trad|
|[Dh4LCrV41VJjACPD.htm](agents-of-edgewatch-bestiary-items/Dh4LCrV41VJjACPD.htm)|Arcane Cannon|auto-trad|
|[dhD7eewlB6LvvS2M.htm](agents-of-edgewatch-bestiary-items/dhD7eewlB6LvvS2M.htm)|Darkvision|auto-trad|
|[dhPK5EdGfI5j8Yse.htm](agents-of-edgewatch-bestiary-items/dhPK5EdGfI5j8Yse.htm)|Thunk 'n' Slice|auto-trad|
|[DJUjRfb1YyWOvrSi.htm](agents-of-edgewatch-bestiary-items/DJUjRfb1YyWOvrSi.htm)|At-Will Spells|auto-trad|
|[DLJfYIHzmCQGAW0s.htm](agents-of-edgewatch-bestiary-items/DLJfYIHzmCQGAW0s.htm)|Rabbit Punch|auto-trad|
|[Dm6lDovIDGIcISuP.htm](agents-of-edgewatch-bestiary-items/Dm6lDovIDGIcISuP.htm)|Magnetize the Living|auto-trad|
|[DNsLOiJgbt3epj8c.htm](agents-of-edgewatch-bestiary-items/DNsLOiJgbt3epj8c.htm)|Mob Rush|auto-trad|
|[DNVNuZGCllMw1hmA.htm](agents-of-edgewatch-bestiary-items/DNVNuZGCllMw1hmA.htm)|Drowning Grasp|auto-trad|
|[DOYd8fquer3sIStG.htm](agents-of-edgewatch-bestiary-items/DOYd8fquer3sIStG.htm)|Entropic Feedback|auto-trad|
|[dPoLgUKZRxlWnRba.htm](agents-of-edgewatch-bestiary-items/dPoLgUKZRxlWnRba.htm)|Swallow Whole|auto-trad|
|[DREnRy1SAHY6dCiU.htm](agents-of-edgewatch-bestiary-items/DREnRy1SAHY6dCiU.htm)|Alchemical Formulas (10th)|auto-trad|
|[drfXFnc49cl52C2X.htm](agents-of-edgewatch-bestiary-items/drfXFnc49cl52C2X.htm)|Foot|auto-trad|
|[DrsLgW5tbXlYEqJ1.htm](agents-of-edgewatch-bestiary-items/DrsLgW5tbXlYEqJ1.htm)|Alien Presence|auto-trad|
|[dRVd3bgtgiGOetEC.htm](agents-of-edgewatch-bestiary-items/dRVd3bgtgiGOetEC.htm)|Darkvision|auto-trad|
|[DSVJsEHiWFPACcNX.htm](agents-of-edgewatch-bestiary-items/DSVJsEHiWFPACcNX.htm)|Attack of Opportunity|auto-trad|
|[dwuIf4lpimWqI6eV.htm](agents-of-edgewatch-bestiary-items/dwuIf4lpimWqI6eV.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Dz4rpuIMNqJy4V90.htm](agents-of-edgewatch-bestiary-items/Dz4rpuIMNqJy4V90.htm)|Inky Imitator|auto-trad|
|[E0zQqvJo9nRAZs7T.htm](agents-of-edgewatch-bestiary-items/E0zQqvJo9nRAZs7T.htm)|Blowtorch|auto-trad|
|[E3ItzWH7n7tJ4AB0.htm](agents-of-edgewatch-bestiary-items/E3ItzWH7n7tJ4AB0.htm)|Entropy Sense (Imprecise) 60 feet|auto-trad|
|[e5D3G73TDu6XWskj.htm](agents-of-edgewatch-bestiary-items/e5D3G73TDu6XWskj.htm)|Twisting Reach|auto-trad|
|[E5E4EFfOXmYDktze.htm](agents-of-edgewatch-bestiary-items/E5E4EFfOXmYDktze.htm)|War Razor|auto-trad|
|[E85UiB4OeqzsCKWf.htm](agents-of-edgewatch-bestiary-items/E85UiB4OeqzsCKWf.htm)|Darkvision|auto-trad|
|[E9Cr9P2L3cp3NzRD.htm](agents-of-edgewatch-bestiary-items/E9Cr9P2L3cp3NzRD.htm)|Aura of Angry Crystals|auto-trad|
|[EaAkzJuaOgph4evs.htm](agents-of-edgewatch-bestiary-items/EaAkzJuaOgph4evs.htm)|Jaws|auto-trad|
|[EaBeGYjdT1VVqvT6.htm](agents-of-edgewatch-bestiary-items/EaBeGYjdT1VVqvT6.htm)|Split|auto-trad|
|[eaCjNNNqsgrBB55K.htm](agents-of-edgewatch-bestiary-items/eaCjNNNqsgrBB55K.htm)|Hand Crossbow|auto-trad|
|[EbIhMOd0cHtkhNcM.htm](agents-of-edgewatch-bestiary-items/EbIhMOd0cHtkhNcM.htm)|Zealous Restoration|auto-trad|
|[EbuMoQ5idyQJDEXD.htm](agents-of-edgewatch-bestiary-items/EbuMoQ5idyQJDEXD.htm)|Paralyzing Gaze|auto-trad|
|[ecUXFzmWTDeBzbIu.htm](agents-of-edgewatch-bestiary-items/ecUXFzmWTDeBzbIu.htm)|Attack of Opportunity|auto-trad|
|[ED7Sb3O9SLxFnTRr.htm](agents-of-edgewatch-bestiary-items/ED7Sb3O9SLxFnTRr.htm)|Unholy Greatsword|auto-trad|
|[EEMf91Mnk5n3dYup.htm](agents-of-edgewatch-bestiary-items/EEMf91Mnk5n3dYup.htm)|Frantic Grasp|auto-trad|
|[EF6rT8DI1QtqCDmn.htm](agents-of-edgewatch-bestiary-items/EF6rT8DI1QtqCDmn.htm)|Improved Grab|auto-trad|
|[EFGYVjmnPR7oeSdb.htm](agents-of-edgewatch-bestiary-items/EFGYVjmnPR7oeSdb.htm)|Spear|auto-trad|
|[efKlG6Tj9bAd6SQ9.htm](agents-of-edgewatch-bestiary-items/efKlG6Tj9bAd6SQ9.htm)|Treasure Sense (Imprecise) 30 feet|auto-trad|
|[EGOkkUlxQlgkV8Ao.htm](agents-of-edgewatch-bestiary-items/EGOkkUlxQlgkV8Ao.htm)|Dagger|auto-trad|
|[eGuMBSFkjeJsBOLy.htm](agents-of-edgewatch-bestiary-items/eGuMBSFkjeJsBOLy.htm)|Fangs|auto-trad|
|[eIDlmbUtMfTc108y.htm](agents-of-edgewatch-bestiary-items/eIDlmbUtMfTc108y.htm)|Throw Shadow Blade|auto-trad|
|[EjUqgzIjC8XDdB6h.htm](agents-of-edgewatch-bestiary-items/EjUqgzIjC8XDdB6h.htm)|Dagger|auto-trad|
|[eJY1bfWIhxuD4h0H.htm](agents-of-edgewatch-bestiary-items/eJY1bfWIhxuD4h0H.htm)|Delay Condition|auto-trad|
|[ekdPtlR0pjDVDM4T.htm](agents-of-edgewatch-bestiary-items/ekdPtlR0pjDVDM4T.htm)|Go for the Eyes|auto-trad|
|[EKp5irpqgnTwEyjX.htm](agents-of-edgewatch-bestiary-items/EKp5irpqgnTwEyjX.htm)|Backdrop|auto-trad|
|[EKw9z5qbMiwdPSwH.htm](agents-of-edgewatch-bestiary-items/EKw9z5qbMiwdPSwH.htm)|Darkvision|auto-trad|
|[ELoLmxNU0ywpt3HM.htm](agents-of-edgewatch-bestiary-items/ELoLmxNU0ywpt3HM.htm)|Stone Spike|auto-trad|
|[ElQzIcZqeoNNQayH.htm](agents-of-edgewatch-bestiary-items/ElQzIcZqeoNNQayH.htm)|Dagger|auto-trad|
|[emBPB2m0d0BQpI2N.htm](agents-of-edgewatch-bestiary-items/emBPB2m0d0BQpI2N.htm)|Leaching Glare|auto-trad|
|[EMKkZjLFHbAYJztn.htm](agents-of-edgewatch-bestiary-items/EMKkZjLFHbAYJztn.htm)|Innate Occult Spells|auto-trad|
|[eosKGTfGo3nA3EVl.htm](agents-of-edgewatch-bestiary-items/eosKGTfGo3nA3EVl.htm)|Recalibrate|auto-trad|
|[ePfqkUvCF1uDrS24.htm](agents-of-edgewatch-bestiary-items/ePfqkUvCF1uDrS24.htm)|Darkvision|auto-trad|
|[ePXkUAkuSGauQ7Jv.htm](agents-of-edgewatch-bestiary-items/ePXkUAkuSGauQ7Jv.htm)|Supersonic Hearing|auto-trad|
|[Eq6IkJnS6klg5Kqj.htm](agents-of-edgewatch-bestiary-items/Eq6IkJnS6klg5Kqj.htm)|Target Culprit|auto-trad|
|[er6I32kkHdLAxCXI.htm](agents-of-edgewatch-bestiary-items/er6I32kkHdLAxCXI.htm)|Stamper|auto-trad|
|[EtuysfUGpQnBFAaU.htm](agents-of-edgewatch-bestiary-items/EtuysfUGpQnBFAaU.htm)|Rapier|auto-trad|
|[eTWouqSZqarzUigD.htm](agents-of-edgewatch-bestiary-items/eTWouqSZqarzUigD.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[eVjcGt7GJnLCOU3R.htm](agents-of-edgewatch-bestiary-items/eVjcGt7GJnLCOU3R.htm)|Constrict|auto-trad|
|[EWsYBE6UDRYE7p46.htm](agents-of-edgewatch-bestiary-items/EWsYBE6UDRYE7p46.htm)|Skitterstitch Venom|auto-trad|
|[EXg9UirCZfna0Tsv.htm](agents-of-edgewatch-bestiary-items/EXg9UirCZfna0Tsv.htm)|Hook Claw|auto-trad|
|[EyiD5AxylSuM4vcO.htm](agents-of-edgewatch-bestiary-items/EyiD5AxylSuM4vcO.htm)|Dagger|auto-trad|
|[eYUQ4i2nFz1iBeTO.htm](agents-of-edgewatch-bestiary-items/eYUQ4i2nFz1iBeTO.htm)|Grab|auto-trad|
|[eyUSuPWwgWZeCeA7.htm](agents-of-edgewatch-bestiary-items/eyUSuPWwgWZeCeA7.htm)|Attack of Opportunity|auto-trad|
|[Ez5LQ3xID56lShet.htm](agents-of-edgewatch-bestiary-items/Ez5LQ3xID56lShet.htm)|Infector|auto-trad|
|[f0fmwIGLi4szm1lO.htm](agents-of-edgewatch-bestiary-items/f0fmwIGLi4szm1lO.htm)|Darkvision|auto-trad|
|[F0wNdQAwukZzTRRS.htm](agents-of-edgewatch-bestiary-items/F0wNdQAwukZzTRRS.htm)|Alchemical Torrent|auto-trad|
|[f3cXxnZGDx1KqvCZ.htm](agents-of-edgewatch-bestiary-items/f3cXxnZGDx1KqvCZ.htm)|Sneak Attack|auto-trad|
|[f52vv2ucD59hRBEO.htm](agents-of-edgewatch-bestiary-items/f52vv2ucD59hRBEO.htm)|Constrict|auto-trad|
|[f6aAjLfElEhbsLCC.htm](agents-of-edgewatch-bestiary-items/f6aAjLfElEhbsLCC.htm)|Dirty Bomb|auto-trad|
|[f9SGRyWKHiuofcI9.htm](agents-of-edgewatch-bestiary-items/f9SGRyWKHiuofcI9.htm)|Ravenous Jaws|auto-trad|
|[fBWMN8Yh6mm05krO.htm](agents-of-edgewatch-bestiary-items/fBWMN8Yh6mm05krO.htm)|First Step|auto-trad|
|[fC6dc9AdYYKBeuWn.htm](agents-of-edgewatch-bestiary-items/fC6dc9AdYYKBeuWn.htm)|Daemonic Pledge|auto-trad|
|[FcWnKeqz5Ffy5vxu.htm](agents-of-edgewatch-bestiary-items/FcWnKeqz5Ffy5vxu.htm)|At-Will Spells|auto-trad|
|[fgprZ9vs1zfRUz3o.htm](agents-of-edgewatch-bestiary-items/fgprZ9vs1zfRUz3o.htm)|Dispel Vulnerability|auto-trad|
|[FH3nsGdSWs8rbUYr.htm](agents-of-edgewatch-bestiary-items/FH3nsGdSWs8rbUYr.htm)|Poisonous Cloud|auto-trad|
|[FHYZijWKqxyPWq4b.htm](agents-of-edgewatch-bestiary-items/FHYZijWKqxyPWq4b.htm)|Construct Armor|auto-trad|
|[fJd0mVVeFIuCWuEo.htm](agents-of-edgewatch-bestiary-items/fJd0mVVeFIuCWuEo.htm)|Occult Innate Spells|auto-trad|
|[fJjXPvTiys4MFIsw.htm](agents-of-edgewatch-bestiary-items/fJjXPvTiys4MFIsw.htm)|Divine Innate Spells|auto-trad|
|[FK02FgrkiyUcMuJp.htm](agents-of-edgewatch-bestiary-items/FK02FgrkiyUcMuJp.htm)|Divine Innate Spells|auto-trad|
|[fkNlQGLkTlnATC6Y.htm](agents-of-edgewatch-bestiary-items/fkNlQGLkTlnATC6Y.htm)|Swallow Whole|auto-trad|
|[fkyMMwCkFLHVH5BV.htm](agents-of-edgewatch-bestiary-items/fkyMMwCkFLHVH5BV.htm)|Steady Spellcasting|auto-trad|
|[Fm6lAJIPw6ObpzHZ.htm](agents-of-edgewatch-bestiary-items/Fm6lAJIPw6ObpzHZ.htm)|Pull Under|auto-trad|
|[FMCAwZcfOhS476dW.htm](agents-of-edgewatch-bestiary-items/FMCAwZcfOhS476dW.htm)|Darkvision|auto-trad|
|[fMk2JkXosTd2BjxC.htm](agents-of-edgewatch-bestiary-items/fMk2JkXosTd2BjxC.htm)|Darkvision|auto-trad|
|[fmyoJEwSeBRrb9Ik.htm](agents-of-edgewatch-bestiary-items/fmyoJEwSeBRrb9Ik.htm)|+1 Status Bonus on Saves vs. Divine Magic|auto-trad|
|[fPhaooKcxBRh5Bsx.htm](agents-of-edgewatch-bestiary-items/fPhaooKcxBRh5Bsx.htm)|Sneak Attack|auto-trad|
|[FqjfY1OlrUKVd4mw.htm](agents-of-edgewatch-bestiary-items/FqjfY1OlrUKVd4mw.htm)|Darkvision|auto-trad|
|[fqPyHyqdqW4NMFk3.htm](agents-of-edgewatch-bestiary-items/fqPyHyqdqW4NMFk3.htm)|Infused Items|auto-trad|
|[fRDoPX53siUmNijE.htm](agents-of-edgewatch-bestiary-items/fRDoPX53siUmNijE.htm)|Steady Spellcasting|auto-trad|
|[fRnsLPos2xrH1GzQ.htm](agents-of-edgewatch-bestiary-items/fRnsLPos2xrH1GzQ.htm)|Low-Light Vision|auto-trad|
|[FsASThbyptq4QfWi.htm](agents-of-edgewatch-bestiary-items/FsASThbyptq4QfWi.htm)|Trample|auto-trad|
|[fSTbX2DA6uu9MkuN.htm](agents-of-edgewatch-bestiary-items/fSTbX2DA6uu9MkuN.htm)|Greater Darkvision|auto-trad|
|[fu5LQi1ag0KXxEby.htm](agents-of-edgewatch-bestiary-items/fu5LQi1ag0KXxEby.htm)|Prickly Defense|auto-trad|
|[FvZvIio9Hz2dvltI.htm](agents-of-edgewatch-bestiary-items/FvZvIio9Hz2dvltI.htm)|Duck and Weave|auto-trad|
|[FXTugA9GorPxMpXh.htm](agents-of-edgewatch-bestiary-items/FXTugA9GorPxMpXh.htm)|Designate Apostate|auto-trad|
|[FY9YGIGBEeh0FNKi.htm](agents-of-edgewatch-bestiary-items/FY9YGIGBEeh0FNKi.htm)|Mother Venom Poison|auto-trad|
|[G406qwAGsa4C7jmR.htm](agents-of-edgewatch-bestiary-items/G406qwAGsa4C7jmR.htm)|Attack of Opportunity|auto-trad|
|[g407ooIFn82B2iXk.htm](agents-of-edgewatch-bestiary-items/g407ooIFn82B2iXk.htm)|Claw|auto-trad|
|[g67xAp632gfQapHl.htm](agents-of-edgewatch-bestiary-items/g67xAp632gfQapHl.htm)|Overwhelming Anguish|auto-trad|
|[g8bRqHNpfEqsTYyx.htm](agents-of-edgewatch-bestiary-items/g8bRqHNpfEqsTYyx.htm)|Internal Spell Strike|auto-trad|
|[G8ORuPOn8J7KCfJc.htm](agents-of-edgewatch-bestiary-items/G8ORuPOn8J7KCfJc.htm)|Prepared Divine Spells|auto-trad|
|[gCFv5lyk3Wfoe8GB.htm](agents-of-edgewatch-bestiary-items/gCFv5lyk3Wfoe8GB.htm)|Javelin|auto-trad|
|[gczvNivaLBoVgpla.htm](agents-of-edgewatch-bestiary-items/gczvNivaLBoVgpla.htm)|Mirror Jump|auto-trad|
|[GElFkGSpAoSzMnqX.htm](agents-of-edgewatch-bestiary-items/GElFkGSpAoSzMnqX.htm)|Push|auto-trad|
|[GGJBUd4JmwxnZtYj.htm](agents-of-edgewatch-bestiary-items/GGJBUd4JmwxnZtYj.htm)|Pincer|auto-trad|
|[gi71u7E3lwMd8bQT.htm](agents-of-edgewatch-bestiary-items/gi71u7E3lwMd8bQT.htm)|Attack of Opportunity|auto-trad|
|[GjE2VyfYPjLsNFGi.htm](agents-of-edgewatch-bestiary-items/GjE2VyfYPjLsNFGi.htm)|Jaws|auto-trad|
|[GJh2y3nd5dKEWe8I.htm](agents-of-edgewatch-bestiary-items/GJh2y3nd5dKEWe8I.htm)|Displacement|auto-trad|
|[glyWTuwYyEk8AaC1.htm](agents-of-edgewatch-bestiary-items/glyWTuwYyEk8AaC1.htm)|Dubious Shifting|auto-trad|
|[gMEgsUUpGodwiTBG.htm](agents-of-edgewatch-bestiary-items/gMEgsUUpGodwiTBG.htm)|Club|auto-trad|
|[gMURrHnMDkhqOaJp.htm](agents-of-edgewatch-bestiary-items/gMURrHnMDkhqOaJp.htm)|At-Will Spells|auto-trad|
|[GneEI6sPR4fF7SgD.htm](agents-of-edgewatch-bestiary-items/GneEI6sPR4fF7SgD.htm)|Low-Light Vision|auto-trad|
|[GnobVyc3rnNPsKdE.htm](agents-of-edgewatch-bestiary-items/GnobVyc3rnNPsKdE.htm)|Grab|auto-trad|
|[GnvMkeIBg6ZpYXzJ.htm](agents-of-edgewatch-bestiary-items/GnvMkeIBg6ZpYXzJ.htm)|Pseudopod Burst|auto-trad|
|[gPHdCqQsilHM5yrV.htm](agents-of-edgewatch-bestiary-items/gPHdCqQsilHM5yrV.htm)|Improved Grab|auto-trad|
|[gpQwZhqK70OriQ2S.htm](agents-of-edgewatch-bestiary-items/gpQwZhqK70OriQ2S.htm)|Divine Innate Spells|auto-trad|
|[Gqv7Fk6y6KrpOB3W.htm](agents-of-edgewatch-bestiary-items/Gqv7Fk6y6KrpOB3W.htm)|Occult Prepared Spells|auto-trad|
|[GrbNOULOdg2VBphU.htm](agents-of-edgewatch-bestiary-items/GrbNOULOdg2VBphU.htm)|Sneak Attack|auto-trad|
|[gTlSQCqmrYTzoJgt.htm](agents-of-edgewatch-bestiary-items/gTlSQCqmrYTzoJgt.htm)|Jaws|auto-trad|
|[GtoDWNmX34925xMc.htm](agents-of-edgewatch-bestiary-items/GtoDWNmX34925xMc.htm)|Unbalancing Blow|auto-trad|
|[GTs7sVH9yhH8mKkM.htm](agents-of-edgewatch-bestiary-items/GTs7sVH9yhH8mKkM.htm)|Nonlethal Training|auto-trad|
|[GtVYqqSXE71FA0xR.htm](agents-of-edgewatch-bestiary-items/GtVYqqSXE71FA0xR.htm)|Thunderstone|auto-trad|
|[GUrHLtuqnl4x3KHH.htm](agents-of-edgewatch-bestiary-items/GUrHLtuqnl4x3KHH.htm)|Burst of Speed|auto-trad|
|[GutcPWbxQjMoihFA.htm](agents-of-edgewatch-bestiary-items/GutcPWbxQjMoihFA.htm)|Water Step|auto-trad|
|[Gv8gIlACS3hfoYJF.htm](agents-of-edgewatch-bestiary-items/Gv8gIlACS3hfoYJF.htm)|Greater Holy Blade|auto-trad|
|[GVWSny9n33dg608R.htm](agents-of-edgewatch-bestiary-items/GVWSny9n33dg608R.htm)|Dagger|auto-trad|
|[Gy4RoLuOaQU79ESu.htm](agents-of-edgewatch-bestiary-items/Gy4RoLuOaQU79ESu.htm)|Sneak Attack|auto-trad|
|[gzMwqFLG38aJHUIa.htm](agents-of-edgewatch-bestiary-items/gzMwqFLG38aJHUIa.htm)|Purple Worm Venom|auto-trad|
|[gZWXLF5Vb3kNdm0I.htm](agents-of-edgewatch-bestiary-items/gZWXLF5Vb3kNdm0I.htm)|Focus Spells|auto-trad|
|[H1Aapl7br22RF2Pe.htm](agents-of-edgewatch-bestiary-items/H1Aapl7br22RF2Pe.htm)|Primal Spontaneous Spells|auto-trad|
|[H2cjvx6x95qaiQFo.htm](agents-of-edgewatch-bestiary-items/H2cjvx6x95qaiQFo.htm)|Marrow Rot|auto-trad|
|[h8NOsLMsLkhSoPDm.htm](agents-of-edgewatch-bestiary-items/h8NOsLMsLkhSoPDm.htm)|Change Shape|auto-trad|
|[haax0MkiNqA6MN2L.htm](agents-of-edgewatch-bestiary-items/haax0MkiNqA6MN2L.htm)|Rallying Call|auto-trad|
|[HaPAHwncLeNsBABj.htm](agents-of-edgewatch-bestiary-items/HaPAHwncLeNsBABj.htm)|Acid Spit|auto-trad|
|[HAvR3sxXnbRxAmUE.htm](agents-of-edgewatch-bestiary-items/HAvR3sxXnbRxAmUE.htm)|Claw|auto-trad|
|[hbg1FrfQIQ6EAWuz.htm](agents-of-edgewatch-bestiary-items/hbg1FrfQIQ6EAWuz.htm)|Opportune Backstab|auto-trad|
|[HCuDpfMGf5F5xT6L.htm](agents-of-edgewatch-bestiary-items/HCuDpfMGf5F5xT6L.htm)|At-Will Spells|auto-trad|
|[hDbNl3kiEPn8AUos.htm](agents-of-edgewatch-bestiary-items/hDbNl3kiEPn8AUos.htm)|Capsize|auto-trad|
|[HeE0E8xiY1o4FYiX.htm](agents-of-edgewatch-bestiary-items/HeE0E8xiY1o4FYiX.htm)|Dagger|auto-trad|
|[HffwSLkW7mUAJAlB.htm](agents-of-edgewatch-bestiary-items/HffwSLkW7mUAJAlB.htm)|Wing|auto-trad|
|[HGIf9bu9E1RjDKaF.htm](agents-of-edgewatch-bestiary-items/HGIf9bu9E1RjDKaF.htm)|Dominate|auto-trad|
|[HhajioTT4qQe67v3.htm](agents-of-edgewatch-bestiary-items/HhajioTT4qQe67v3.htm)|Improved Grab|auto-trad|
|[HHDLYWqh0LHr49Uu.htm](agents-of-edgewatch-bestiary-items/HHDLYWqh0LHr49Uu.htm)|Stitch Skin|auto-trad|
|[hhg7o2qV6JntAswy.htm](agents-of-edgewatch-bestiary-items/hhg7o2qV6JntAswy.htm)|+2 Status Bonus on Saves vs. Trip|auto-trad|
|[HIbGLn4unRGk8EZL.htm](agents-of-edgewatch-bestiary-items/HIbGLn4unRGk8EZL.htm)|Dart Volley|auto-trad|
|[hJJc012AkwlepsfP.htm](agents-of-edgewatch-bestiary-items/hJJc012AkwlepsfP.htm)|Odorless|auto-trad|
|[HkUjt1e6CiwdfHoy.htm](agents-of-edgewatch-bestiary-items/HkUjt1e6CiwdfHoy.htm)|Breath Weapon|auto-trad|
|[HLhPjN1be3uh5emL.htm](agents-of-edgewatch-bestiary-items/HLhPjN1be3uh5emL.htm)|Flurry of Blows|auto-trad|
|[HLLn9CHqmRrWk4IN.htm](agents-of-edgewatch-bestiary-items/HLLn9CHqmRrWk4IN.htm)|Nimble Dodge|auto-trad|
|[HMCS1B66LSvBKspC.htm](agents-of-edgewatch-bestiary-items/HMCS1B66LSvBKspC.htm)|Splatter|auto-trad|
|[hnspRHRc7JzzGcNY.htm](agents-of-edgewatch-bestiary-items/hnspRHRc7JzzGcNY.htm)|Knockdown|auto-trad|
|[HNYjS5WAb54frbpw.htm](agents-of-edgewatch-bestiary-items/HNYjS5WAb54frbpw.htm)|Charming Liar|auto-trad|
|[hoABDC4bNqLsgmbp.htm](agents-of-edgewatch-bestiary-items/hoABDC4bNqLsgmbp.htm)|Dagger|auto-trad|
|[hozDdAMhO9qf2Buo.htm](agents-of-edgewatch-bestiary-items/hozDdAMhO9qf2Buo.htm)|Decapitate|auto-trad|
|[Hq1vagZwNS6eDSNA.htm](agents-of-edgewatch-bestiary-items/Hq1vagZwNS6eDSNA.htm)|Tainted Backlash|auto-trad|
|[hrbhUgMW0KZvWovb.htm](agents-of-edgewatch-bestiary-items/hrbhUgMW0KZvWovb.htm)|Katar Specialist|auto-trad|
|[HrV9IU9zNImvgkv7.htm](agents-of-edgewatch-bestiary-items/HrV9IU9zNImvgkv7.htm)|Grab|auto-trad|
|[hScPOOUe2XVGeyIa.htm](agents-of-edgewatch-bestiary-items/hScPOOUe2XVGeyIa.htm)|Bronze Fist|auto-trad|
|[Hss992fc1sCOcDVo.htm](agents-of-edgewatch-bestiary-items/Hss992fc1sCOcDVo.htm)|Electric Reflexes|auto-trad|
|[Ht4guo51Eky0gFrz.htm](agents-of-edgewatch-bestiary-items/Ht4guo51Eky0gFrz.htm)|Partial Amphibian|auto-trad|
|[hup5Hnb2EZPEeNg7.htm](agents-of-edgewatch-bestiary-items/hup5Hnb2EZPEeNg7.htm)|Darkvision|auto-trad|
|[hvjSsYJt6z0pbDL0.htm](agents-of-edgewatch-bestiary-items/hvjSsYJt6z0pbDL0.htm)|Mandibles|auto-trad|
|[hvsbuonawLt5vOLp.htm](agents-of-edgewatch-bestiary-items/hvsbuonawLt5vOLp.htm)|Brutish Shove|auto-trad|
|[hvTnI184eIoLfwPq.htm](agents-of-edgewatch-bestiary-items/hvTnI184eIoLfwPq.htm)|Berserk Slam|auto-trad|
|[hwDOgihBpaGyHO9Q.htm](agents-of-edgewatch-bestiary-items/hwDOgihBpaGyHO9Q.htm)|Dagger|auto-trad|
|[hxG5VYuZdf33T05P.htm](agents-of-edgewatch-bestiary-items/hxG5VYuZdf33T05P.htm)|Attack of Opportunity|auto-trad|
|[HxSPrzqufbKb8gOo.htm](agents-of-edgewatch-bestiary-items/HxSPrzqufbKb8gOo.htm)|Negative Healing|auto-trad|
|[hytdhHAVEMZ2IlnQ.htm](agents-of-edgewatch-bestiary-items/hytdhHAVEMZ2IlnQ.htm)|Claws|auto-trad|
|[hzLoXfk0XZ09nziz.htm](agents-of-edgewatch-bestiary-items/hzLoXfk0XZ09nziz.htm)|Terrifying Gaze|auto-trad|
|[i0POO0dwNFDOcYuV.htm](agents-of-edgewatch-bestiary-items/i0POO0dwNFDOcYuV.htm)|Summon Devil|auto-trad|
|[i1fagw7VusZxwIdz.htm](agents-of-edgewatch-bestiary-items/i1fagw7VusZxwIdz.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[I23rYMszuvKL97k7.htm](agents-of-edgewatch-bestiary-items/I23rYMszuvKL97k7.htm)|Protean Anatomy|auto-trad|
|[I5UOcgBhEV16sOvv.htm](agents-of-edgewatch-bestiary-items/I5UOcgBhEV16sOvv.htm)|Studied Strike|auto-trad|
|[I7zUPwVMtrP17lu5.htm](agents-of-edgewatch-bestiary-items/I7zUPwVMtrP17lu5.htm)|Eschew Materials|auto-trad|
|[IBTLrjDIdwriv1Sh.htm](agents-of-edgewatch-bestiary-items/IBTLrjDIdwriv1Sh.htm)|Attack of Opportunity|auto-trad|
|[iccpDuEAwMrtLyBh.htm](agents-of-edgewatch-bestiary-items/iccpDuEAwMrtLyBh.htm)|Wire Catch|auto-trad|
|[IcNV9BTE0VD3ZNXy.htm](agents-of-edgewatch-bestiary-items/IcNV9BTE0VD3ZNXy.htm)|Intimidating Stare|auto-trad|
|[IDckQwFfttdnib8W.htm](agents-of-edgewatch-bestiary-items/IDckQwFfttdnib8W.htm)|Poisoned Dart|auto-trad|
|[Idet44bdRqy8cmfx.htm](agents-of-edgewatch-bestiary-items/Idet44bdRqy8cmfx.htm)|Dagger|auto-trad|
|[IdUgaBFlawtCWK5m.htm](agents-of-edgewatch-bestiary-items/IdUgaBFlawtCWK5m.htm)|Constant Spells|auto-trad|
|[IEuErW2Q6KaGlk21.htm](agents-of-edgewatch-bestiary-items/IEuErW2Q6KaGlk21.htm)|Hook and Flay|auto-trad|
|[If0QHT3SwMga7arR.htm](agents-of-edgewatch-bestiary-items/If0QHT3SwMga7arR.htm)|Darkvision|auto-trad|
|[IfCbrxEtWCoJCOgv.htm](agents-of-edgewatch-bestiary-items/IfCbrxEtWCoJCOgv.htm)|Longsword|auto-trad|
|[iFX6Z8UsTWukjUxp.htm](agents-of-edgewatch-bestiary-items/iFX6Z8UsTWukjUxp.htm)|Quick Alchemy|auto-trad|
|[iG9uOtBJolj3P4CM.htm](agents-of-edgewatch-bestiary-items/iG9uOtBJolj3P4CM.htm)|Spew Cloud|auto-trad|
|[igIwGgzOBvjtlLdx.htm](agents-of-edgewatch-bestiary-items/igIwGgzOBvjtlLdx.htm)|Transparent|auto-trad|
|[IH1SiADzFCDakX8w.htm](agents-of-edgewatch-bestiary-items/IH1SiADzFCDakX8w.htm)|Out You Go|auto-trad|
|[II1iKHEPt0qdurXn.htm](agents-of-edgewatch-bestiary-items/II1iKHEPt0qdurXn.htm)|Plunder Life|auto-trad|
|[IiO8ytUL6tJgUAXR.htm](agents-of-edgewatch-bestiary-items/IiO8ytUL6tJgUAXR.htm)|Vargouille Venom|auto-trad|
|[iJUSlDT7ZVFtjNSy.htm](agents-of-edgewatch-bestiary-items/iJUSlDT7ZVFtjNSy.htm)|Spider Swarm Host|auto-trad|
|[Ikix1zKqDpYhzRYA.htm](agents-of-edgewatch-bestiary-items/Ikix1zKqDpYhzRYA.htm)|Infector|auto-trad|
|[IKQ2kDBIPfAILZjf.htm](agents-of-edgewatch-bestiary-items/IKQ2kDBIPfAILZjf.htm)|Alchemical Bomb|auto-trad|
|[ikVC2siqOrYTYZtS.htm](agents-of-edgewatch-bestiary-items/ikVC2siqOrYTYZtS.htm)|Second Chance|auto-trad|
|[Iky2gPwt5DqOgbkl.htm](agents-of-edgewatch-bestiary-items/Iky2gPwt5DqOgbkl.htm)|Push 10 feet|auto-trad|
|[IlMxWLhPQQrxWb4s.htm](agents-of-edgewatch-bestiary-items/IlMxWLhPQQrxWb4s.htm)|Wind|auto-trad|
|[INFGwX7PUOvEy0iD.htm](agents-of-edgewatch-bestiary-items/INFGwX7PUOvEy0iD.htm)|Trail of Flame|auto-trad|
|[iNUgfYO7f8c2RdLm.htm](agents-of-edgewatch-bestiary-items/iNUgfYO7f8c2RdLm.htm)|Rend (jaws)|auto-trad|
|[iOt4Y0EmIGeBhl6u.htm](agents-of-edgewatch-bestiary-items/iOt4Y0EmIGeBhl6u.htm)|Darkvision|auto-trad|
|[iprvaUuVaBlST8Su.htm](agents-of-edgewatch-bestiary-items/iprvaUuVaBlST8Su.htm)|Low-Light Vision|auto-trad|
|[iqSjrlDup1I8i5VX.htm](agents-of-edgewatch-bestiary-items/iqSjrlDup1I8i5VX.htm)|Bloody Feet|auto-trad|
|[ISXykb5wK1DoaIOw.htm](agents-of-edgewatch-bestiary-items/ISXykb5wK1DoaIOw.htm)|Terrain Advantage|auto-trad|
|[ITQk9n2tp7J5FDQa.htm](agents-of-edgewatch-bestiary-items/ITQk9n2tp7J5FDQa.htm)|Change Shape|auto-trad|
|[iUGSF5wsY0hhEr0g.htm](agents-of-edgewatch-bestiary-items/iUGSF5wsY0hhEr0g.htm)|Grab|auto-trad|
|[iV2kvaEO0RYmWI8X.htm](agents-of-edgewatch-bestiary-items/iV2kvaEO0RYmWI8X.htm)|Blade|auto-trad|
|[IVbYmIS1u62AEFZm.htm](agents-of-edgewatch-bestiary-items/IVbYmIS1u62AEFZm.htm)|Scalding Spray|auto-trad|
|[iYdGVR75KvoZWFO3.htm](agents-of-edgewatch-bestiary-items/iYdGVR75KvoZWFO3.htm)|Toxic Mastery|auto-trad|
|[IZqqliLOZ1AtFOJF.htm](agents-of-edgewatch-bestiary-items/IZqqliLOZ1AtFOJF.htm)|Sap|auto-trad|
|[j2lpHYGglR2Lc1Ut.htm](agents-of-edgewatch-bestiary-items/j2lpHYGglR2Lc1Ut.htm)|Darkvision|auto-trad|
|[J6CumHMGaEuUCyrp.htm](agents-of-edgewatch-bestiary-items/J6CumHMGaEuUCyrp.htm)|Open Hatch|auto-trad|
|[j72OO3UZ0KrTLLO6.htm](agents-of-edgewatch-bestiary-items/j72OO3UZ0KrTLLO6.htm)|Ghoul Fever|auto-trad|
|[j7j4zEpOdEbRtRuk.htm](agents-of-edgewatch-bestiary-items/j7j4zEpOdEbRtRuk.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[j7oe7252bXGLrvgk.htm](agents-of-edgewatch-bestiary-items/j7oe7252bXGLrvgk.htm)|Reaper's Lancet Blade|auto-trad|
|[jcfMZ2vEUL2nEfFQ.htm](agents-of-edgewatch-bestiary-items/jcfMZ2vEUL2nEfFQ.htm)|Surreptitious Siege|auto-trad|
|[JCwAfoQqJDmPTVLF.htm](agents-of-edgewatch-bestiary-items/JCwAfoQqJDmPTVLF.htm)|Crystal Sense (Imprecise) 30 feet|auto-trad|
|[jdam6t7WxAcXyrY6.htm](agents-of-edgewatch-bestiary-items/jdam6t7WxAcXyrY6.htm)|Claw|auto-trad|
|[jDE564VbgKyhi1ch.htm](agents-of-edgewatch-bestiary-items/jDE564VbgKyhi1ch.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[jfhtS3mRJ8Vf1K2F.htm](agents-of-edgewatch-bestiary-items/jfhtS3mRJ8Vf1K2F.htm)|Longsword|auto-trad|
|[jgogVBpbS3zZv6XG.htm](agents-of-edgewatch-bestiary-items/jgogVBpbS3zZv6XG.htm)|Rapier|auto-trad|
|[JHDUCoAwvNv120KU.htm](agents-of-edgewatch-bestiary-items/JHDUCoAwvNv120KU.htm)|At-Will Spells|auto-trad|
|[jirCmoCjYEZm147H.htm](agents-of-edgewatch-bestiary-items/jirCmoCjYEZm147H.htm)|Darkvision|auto-trad|
|[JJiuY4l7O3Ujlg8S.htm](agents-of-edgewatch-bestiary-items/JJiuY4l7O3Ujlg8S.htm)|Speed|auto-trad|
|[JJluFLD9A2iKiNi4.htm](agents-of-edgewatch-bestiary-items/JJluFLD9A2iKiNi4.htm)|Double Punch|auto-trad|
|[jJSKOhaRoCp1WwP4.htm](agents-of-edgewatch-bestiary-items/jJSKOhaRoCp1WwP4.htm)|Inhabit Vessel|auto-trad|
|[jkHMXDAED128Pydk.htm](agents-of-edgewatch-bestiary-items/jkHMXDAED128Pydk.htm)|Heavy Crossbow|auto-trad|
|[Jkps95l9DKtpNrr8.htm](agents-of-edgewatch-bestiary-items/Jkps95l9DKtpNrr8.htm)|Divine Prepared Spells|auto-trad|
|[JNMs0qEBFo57y0tr.htm](agents-of-edgewatch-bestiary-items/JNMs0qEBFo57y0tr.htm)|Rend|auto-trad|
|[JnTr0G2JvwVYzdfR.htm](agents-of-edgewatch-bestiary-items/JnTr0G2JvwVYzdfR.htm)|Detaining Strike|auto-trad|
|[jq9NuOTE6q7jHwO1.htm](agents-of-edgewatch-bestiary-items/jq9NuOTE6q7jHwO1.htm)|Greater Darkvision|auto-trad|
|[JqbE9PdaDo8dWRhu.htm](agents-of-edgewatch-bestiary-items/JqbE9PdaDo8dWRhu.htm)|Jaws|auto-trad|
|[JQLaH8N9rTPhEzqj.htm](agents-of-edgewatch-bestiary-items/JQLaH8N9rTPhEzqj.htm)|Sword Cane Duelist|auto-trad|
|[JRKYJD9kEhVvnpPF.htm](agents-of-edgewatch-bestiary-items/JRKYJD9kEhVvnpPF.htm)|Jaws|auto-trad|
|[JUNDTV3wwrM9gFjE.htm](agents-of-edgewatch-bestiary-items/JUNDTV3wwrM9gFjE.htm)|Crossbow|auto-trad|
|[JXdEhHs5I4scpvbs.htm](agents-of-edgewatch-bestiary-items/JXdEhHs5I4scpvbs.htm)|Shortsword|auto-trad|
|[JXIun04dusvrOS6n.htm](agents-of-edgewatch-bestiary-items/JXIun04dusvrOS6n.htm)|Kiss of the Speakers|auto-trad|
|[jybYCirNRpxTm5fl.htm](agents-of-edgewatch-bestiary-items/jybYCirNRpxTm5fl.htm)|Fist|auto-trad|
|[jzOEMi9hAVAe1K1V.htm](agents-of-edgewatch-bestiary-items/jzOEMi9hAVAe1K1V.htm)|Poisoned Needle|auto-trad|
|[jZQm4Bhn7fdp3YRx.htm](agents-of-edgewatch-bestiary-items/jZQm4Bhn7fdp3YRx.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[k0BFzCiBWr8fRZ4m.htm](agents-of-edgewatch-bestiary-items/k0BFzCiBWr8fRZ4m.htm)|Obscuring Grab|auto-trad|
|[k1KchqR9i8uSyT6e.htm](agents-of-edgewatch-bestiary-items/k1KchqR9i8uSyT6e.htm)|Combustible|auto-trad|
|[k1kPrQ3Krhcipl4E.htm](agents-of-edgewatch-bestiary-items/k1kPrQ3Krhcipl4E.htm)|Fist|auto-trad|
|[k1m2BVdO1iqRouch.htm](agents-of-edgewatch-bestiary-items/k1m2BVdO1iqRouch.htm)|Emotion Sense 120 feet|auto-trad|
|[K287MomJCMfPXPLf.htm](agents-of-edgewatch-bestiary-items/K287MomJCMfPXPLf.htm)|Smoke Vision|auto-trad|
|[K2NRmCiVWR4uMsRg.htm](agents-of-edgewatch-bestiary-items/K2NRmCiVWR4uMsRg.htm)|Vanish in Reflections|auto-trad|
|[K2xZ73LiKk2uqR28.htm](agents-of-edgewatch-bestiary-items/K2xZ73LiKk2uqR28.htm)|Blackfinger's Prayer|auto-trad|
|[K4f9RbGiZucMUGD8.htm](agents-of-edgewatch-bestiary-items/K4f9RbGiZucMUGD8.htm)|Attack of Opportunity|auto-trad|
|[K68kyyTQ4cOmTtUk.htm](agents-of-edgewatch-bestiary-items/K68kyyTQ4cOmTtUk.htm)|Mind-Numbing Grasp|auto-trad|
|[k6q40GuzzYWifu3y.htm](agents-of-edgewatch-bestiary-items/k6q40GuzzYWifu3y.htm)|Divine Prepared Spells|auto-trad|
|[K7CFCrn5nlo3dzku.htm](agents-of-edgewatch-bestiary-items/K7CFCrn5nlo3dzku.htm)|Scourge|auto-trad|
|[kckDvYIQBBGcjLNo.htm](agents-of-edgewatch-bestiary-items/kckDvYIQBBGcjLNo.htm)|Scimitar|auto-trad|
|[kcrnWNWqaiPR3tg9.htm](agents-of-edgewatch-bestiary-items/kcrnWNWqaiPR3tg9.htm)|Arcane Spontaneous Spells|auto-trad|
|[KcYjsnv1Da60rZqp.htm](agents-of-edgewatch-bestiary-items/KcYjsnv1Da60rZqp.htm)|Low-Light Vision|auto-trad|
|[kEiDcm42Prsww2ja.htm](agents-of-edgewatch-bestiary-items/kEiDcm42Prsww2ja.htm)|Alchemical Reaction|auto-trad|
|[kELQNLDmBgZ4uIU5.htm](agents-of-edgewatch-bestiary-items/kELQNLDmBgZ4uIU5.htm)|Improved Grab|auto-trad|
|[KeuEtlVRXnSoTjPo.htm](agents-of-edgewatch-bestiary-items/KeuEtlVRXnSoTjPo.htm)|Darkvision|auto-trad|
|[KFdVbNhK30Xo6GCp.htm](agents-of-edgewatch-bestiary-items/KFdVbNhK30Xo6GCp.htm)|Ooze Tendril|auto-trad|
|[Kfw3xJtFTIHlmDoM.htm](agents-of-edgewatch-bestiary-items/Kfw3xJtFTIHlmDoM.htm)|Improved Grab|auto-trad|
|[kfZVz80JupwQ5DE1.htm](agents-of-edgewatch-bestiary-items/kfZVz80JupwQ5DE1.htm)|Bloody Chain Aura|auto-trad|
|[kIPI4tNPGahloPyK.htm](agents-of-edgewatch-bestiary-items/kIPI4tNPGahloPyK.htm)|Darkvision|auto-trad|
|[kK15wEyRUz3vGzjl.htm](agents-of-edgewatch-bestiary-items/kK15wEyRUz3vGzjl.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[KkQu3TqvmvRwh5Gx.htm](agents-of-edgewatch-bestiary-items/KkQu3TqvmvRwh5Gx.htm)|Crush of Hundreds|auto-trad|
|[kMMwsDVDDuFjQhm3.htm](agents-of-edgewatch-bestiary-items/kMMwsDVDDuFjQhm3.htm)|Fist|auto-trad|
|[KOfo0zVfgBhhWGjT.htm](agents-of-edgewatch-bestiary-items/KOfo0zVfgBhhWGjT.htm)|Quill Cannon|auto-trad|
|[Kp4jOhCQVcuNVP2j.htm](agents-of-edgewatch-bestiary-items/Kp4jOhCQVcuNVP2j.htm)|Vital Transfusion|auto-trad|
|[kPfYHs4PPwutz153.htm](agents-of-edgewatch-bestiary-items/kPfYHs4PPwutz153.htm)|Grabbing Trunk|auto-trad|
|[krn8dqAmFwPEmo0O.htm](agents-of-edgewatch-bestiary-items/krn8dqAmFwPEmo0O.htm)|Darkvision|auto-trad|
|[KRYmDgL0v3sgSPhE.htm](agents-of-edgewatch-bestiary-items/KRYmDgL0v3sgSPhE.htm)|Explosion|auto-trad|
|[kS3RkK66eSUF1gOu.htm](agents-of-edgewatch-bestiary-items/kS3RkK66eSUF1gOu.htm)|Tail|auto-trad|
|[kspfnXbpzKtPxBp2.htm](agents-of-edgewatch-bestiary-items/kspfnXbpzKtPxBp2.htm)|Darkvision|auto-trad|
|[kVdw249CmzAbj17U.htm](agents-of-edgewatch-bestiary-items/kVdw249CmzAbj17U.htm)|Riptide|auto-trad|
|[KWEh7hh4al6ViDpu.htm](agents-of-edgewatch-bestiary-items/KWEh7hh4al6ViDpu.htm)|Heavy Crossbow|auto-trad|
|[KyPmodj8yfI4cI99.htm](agents-of-edgewatch-bestiary-items/KyPmodj8yfI4cI99.htm)|Overtake Soul|auto-trad|
|[KZ4Ixv66QE0EQIct.htm](agents-of-edgewatch-bestiary-items/KZ4Ixv66QE0EQIct.htm)|Pseudopod|auto-trad|
|[kzNiR7bLBxL4aWfR.htm](agents-of-edgewatch-bestiary-items/kzNiR7bLBxL4aWfR.htm)|Quick Draw|auto-trad|
|[kZPMz3gjTr4SFKIs.htm](agents-of-edgewatch-bestiary-items/kZPMz3gjTr4SFKIs.htm)|Darkvision|auto-trad|
|[l5qCiwvxOStA4H26.htm](agents-of-edgewatch-bestiary-items/l5qCiwvxOStA4H26.htm)|Claw|auto-trad|
|[l5xkjpvgvFHWi6Yj.htm](agents-of-edgewatch-bestiary-items/l5xkjpvgvFHWi6Yj.htm)|Innate Divine Spells|auto-trad|
|[l7wT4LDdfJXJETHf.htm](agents-of-edgewatch-bestiary-items/l7wT4LDdfJXJETHf.htm)|Clockwork Brain|auto-trad|
|[LaDAOhsZJlBZ3eiZ.htm](agents-of-edgewatch-bestiary-items/LaDAOhsZJlBZ3eiZ.htm)|Shortsword|auto-trad|
|[laNWVOBt5DoTHn3G.htm](agents-of-edgewatch-bestiary-items/laNWVOBt5DoTHn3G.htm)|Warpwave Strike|auto-trad|
|[LBQk96wXoxYrTUZi.htm](agents-of-edgewatch-bestiary-items/LBQk96wXoxYrTUZi.htm)|Distort Magic|auto-trad|
|[LCEc1hLLB843Ltt3.htm](agents-of-edgewatch-bestiary-items/LCEc1hLLB843Ltt3.htm)|Fist|auto-trad|
|[LCKRgv7IeeWygjY3.htm](agents-of-edgewatch-bestiary-items/LCKRgv7IeeWygjY3.htm)|Spirit Blast Ray|auto-trad|
|[LFIJhcEKJhKijVm1.htm](agents-of-edgewatch-bestiary-items/LFIJhcEKJhKijVm1.htm)|Shock|auto-trad|
|[lftFJteRXqRBmg0d.htm](agents-of-edgewatch-bestiary-items/lftFJteRXqRBmg0d.htm)|Telepathy 100 feet|auto-trad|
|[lHgISCDkIa7T34nH.htm](agents-of-edgewatch-bestiary-items/lHgISCDkIa7T34nH.htm)|Spawn Mirror Duplicate|auto-trad|
|[LiFDw1yyHumRLJFc.htm](agents-of-edgewatch-bestiary-items/LiFDw1yyHumRLJFc.htm)|Grab|auto-trad|
|[lJYPy9JbnZy8cvV0.htm](agents-of-edgewatch-bestiary-items/lJYPy9JbnZy8cvV0.htm)|Dagger|auto-trad|
|[lMrRe942X6FotrEf.htm](agents-of-edgewatch-bestiary-items/lMrRe942X6FotrEf.htm)|Darkvision|auto-trad|
|[LnwbIbIzWsV3rQvP.htm](agents-of-edgewatch-bestiary-items/LnwbIbIzWsV3rQvP.htm)|Divine Innate Spells|auto-trad|
|[loHKBZQtnioHzbFm.htm](agents-of-edgewatch-bestiary-items/loHKBZQtnioHzbFm.htm)|Quick Bomber|auto-trad|
|[LPMvePNhEQEvIeA1.htm](agents-of-edgewatch-bestiary-items/LPMvePNhEQEvIeA1.htm)|Alchemical Injection|auto-trad|
|[lpqsopfJ3k9tHtel.htm](agents-of-edgewatch-bestiary-items/lpqsopfJ3k9tHtel.htm)|Innate Divine Spells|auto-trad|
|[LQEhpFQTxwqQ8b2q.htm](agents-of-edgewatch-bestiary-items/LQEhpFQTxwqQ8b2q.htm)|Deflective Dodge|auto-trad|
|[LveVTPzs4hEN09k6.htm](agents-of-edgewatch-bestiary-items/LveVTPzs4hEN09k6.htm)|Darkvision|auto-trad|
|[LvmDMsbTpm0IyK9u.htm](agents-of-edgewatch-bestiary-items/LvmDMsbTpm0IyK9u.htm)|Fast Swallow|auto-trad|
|[lVTqn24kqmGrjWJI.htm](agents-of-edgewatch-bestiary-items/lVTqn24kqmGrjWJI.htm)|Spit|auto-trad|
|[lVvJa2dDy1MeSjQ1.htm](agents-of-edgewatch-bestiary-items/lVvJa2dDy1MeSjQ1.htm)|Splash Poison|auto-trad|
|[LvYtP53GFOEynRGV.htm](agents-of-edgewatch-bestiary-items/LvYtP53GFOEynRGV.htm)|Skip Between|auto-trad|
|[LWnz2t8s48aN2oK3.htm](agents-of-edgewatch-bestiary-items/LWnz2t8s48aN2oK3.htm)|Harrow Card|auto-trad|
|[LxrfittzDR3SPd3W.htm](agents-of-edgewatch-bestiary-items/LxrfittzDR3SPd3W.htm)|Fill Lungs|auto-trad|
|[LYl3vW92ci5HFVXk.htm](agents-of-edgewatch-bestiary-items/LYl3vW92ci5HFVXk.htm)|Alchemical Chambers|auto-trad|
|[LyRSaBlkqwNMCaQT.htm](agents-of-edgewatch-bestiary-items/LyRSaBlkqwNMCaQT.htm)|Slam|auto-trad|
|[lYvWF5v5Vah9KWsN.htm](agents-of-edgewatch-bestiary-items/lYvWF5v5Vah9KWsN.htm)|Golem Antimagic|auto-trad|
|[LzeE4Oi1C9zb69rd.htm](agents-of-edgewatch-bestiary-items/LzeE4Oi1C9zb69rd.htm)|Darkvision|auto-trad|
|[LzNIxnRUBN5s28I4.htm](agents-of-edgewatch-bestiary-items/LzNIxnRUBN5s28I4.htm)|Darkvision|auto-trad|
|[M0P9lsWNp222StTi.htm](agents-of-edgewatch-bestiary-items/M0P9lsWNp222StTi.htm)|Pseudopod|auto-trad|
|[M0y7VtVeytggUvVq.htm](agents-of-edgewatch-bestiary-items/M0y7VtVeytggUvVq.htm)|Jaws|auto-trad|
|[M203mlcBNFvAqyIJ.htm](agents-of-edgewatch-bestiary-items/M203mlcBNFvAqyIJ.htm)|Stone Robes|auto-trad|
|[M2i1Ktvj26SGDm7s.htm](agents-of-edgewatch-bestiary-items/M2i1Ktvj26SGDm7s.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[m6L9nqkoxL1pP5V6.htm](agents-of-edgewatch-bestiary-items/m6L9nqkoxL1pP5V6.htm)|Attack of Opportunity (Trip Only)|auto-trad|
|[M7E6dxLskMLaX43G.htm](agents-of-edgewatch-bestiary-items/M7E6dxLskMLaX43G.htm)|Dimensional Pit|auto-trad|
|[m81fVuHWpnJwd8cc.htm](agents-of-edgewatch-bestiary-items/m81fVuHWpnJwd8cc.htm)|At-Will Spells|auto-trad|
|[Ma9MtDSiauZ5onNI.htm](agents-of-edgewatch-bestiary-items/Ma9MtDSiauZ5onNI.htm)|Nimble Swim|auto-trad|
|[mBGuEJPcBGcngTxE.htm](agents-of-edgewatch-bestiary-items/mBGuEJPcBGcngTxE.htm)|Horn|auto-trad|
|[MClpWaT5c8jTmcFK.htm](agents-of-edgewatch-bestiary-items/MClpWaT5c8jTmcFK.htm)|Flush|auto-trad|
|[McLV2Lx51f0UF1YX.htm](agents-of-edgewatch-bestiary-items/McLV2Lx51f0UF1YX.htm)|Poison Ink|auto-trad|
|[mdqTsawy6o4sNLqp.htm](agents-of-edgewatch-bestiary-items/mdqTsawy6o4sNLqp.htm)|Nimble Dodge|auto-trad|
|[mDzfLBnuMo5KYFSr.htm](agents-of-edgewatch-bestiary-items/mDzfLBnuMo5KYFSr.htm)|Cast Down|auto-trad|
|[mHKZKkSzSU0RL4MI.htm](agents-of-edgewatch-bestiary-items/mHKZKkSzSU0RL4MI.htm)|Sap|auto-trad|
|[MIGvbijvRTovVpJE.htm](agents-of-edgewatch-bestiary-items/MIGvbijvRTovVpJE.htm)|Vomit Blood|auto-trad|
|[mjEOGwxDnDNV6VB0.htm](agents-of-edgewatch-bestiary-items/mjEOGwxDnDNV6VB0.htm)|Swarming|auto-trad|
|[mk8H61X7q0MpYFgq.htm](agents-of-edgewatch-bestiary-items/mk8H61X7q0MpYFgq.htm)|Prepared Divine Spells|auto-trad|
|[Mlm12XchuKW2Zkqk.htm](agents-of-edgewatch-bestiary-items/Mlm12XchuKW2Zkqk.htm)|Lifesense 30 feet|auto-trad|
|[mmdbAeOOVVjLALCx.htm](agents-of-edgewatch-bestiary-items/mmdbAeOOVVjLALCx.htm)|Rapid Repair|auto-trad|
|[mNgkZuWHNH6FJsSb.htm](agents-of-edgewatch-bestiary-items/mNgkZuWHNH6FJsSb.htm)|Trickster's Ace|auto-trad|
|[mOEFev3A3LvUINAz.htm](agents-of-edgewatch-bestiary-items/mOEFev3A3LvUINAz.htm)|Deep Breath|auto-trad|
|[mOEnnV4W8qmCRdYl.htm](agents-of-edgewatch-bestiary-items/mOEnnV4W8qmCRdYl.htm)|Claw|auto-trad|
|[MpraNid9IzBbGn7L.htm](agents-of-edgewatch-bestiary-items/MpraNid9IzBbGn7L.htm)|Dagger|auto-trad|
|[MQ2Rp3l2BaBlZCiE.htm](agents-of-edgewatch-bestiary-items/MQ2Rp3l2BaBlZCiE.htm)|Darkvision|auto-trad|
|[mqpmTykaUCnVLmSj.htm](agents-of-edgewatch-bestiary-items/mqpmTykaUCnVLmSj.htm)|Claw|auto-trad|
|[mRbro73gnpsZOfZa.htm](agents-of-edgewatch-bestiary-items/mRbro73gnpsZOfZa.htm)|Claw|auto-trad|
|[mRPwn3ixbA1j5Wfz.htm](agents-of-edgewatch-bestiary-items/mRPwn3ixbA1j5Wfz.htm)|Study Target|auto-trad|
|[MRSyx2ZsaFXN3kSn.htm](agents-of-edgewatch-bestiary-items/MRSyx2ZsaFXN3kSn.htm)|Power Attack|auto-trad|
|[mryw4DfSBV3Xx30n.htm](agents-of-edgewatch-bestiary-items/mryw4DfSBV3Xx30n.htm)|Grab|auto-trad|
|[MRzdtZQQLPCyGK5c.htm](agents-of-edgewatch-bestiary-items/MRzdtZQQLPCyGK5c.htm)|Rituals|auto-trad|
|[mTP4bYZoCnN514ZB.htm](agents-of-edgewatch-bestiary-items/mTP4bYZoCnN514ZB.htm)|Spit|auto-trad|
|[MvGg9ZU03PHCDYj9.htm](agents-of-edgewatch-bestiary-items/MvGg9ZU03PHCDYj9.htm)|Quick Stitch|auto-trad|
|[MwIhQXzCMNpAds0Q.htm](agents-of-edgewatch-bestiary-items/MwIhQXzCMNpAds0Q.htm)|Constant Spells|auto-trad|
|[mYdklqhaT2LcRlN3.htm](agents-of-edgewatch-bestiary-items/mYdklqhaT2LcRlN3.htm)|Determination|auto-trad|
|[mYFvnq9cvcGTpyjV.htm](agents-of-edgewatch-bestiary-items/mYFvnq9cvcGTpyjV.htm)|Constant Spells|auto-trad|
|[MyTJRF05gqbWnyPq.htm](agents-of-edgewatch-bestiary-items/MyTJRF05gqbWnyPq.htm)|Metallify|auto-trad|
|[MZuAw11uvBDLHKj7.htm](agents-of-edgewatch-bestiary-items/MZuAw11uvBDLHKj7.htm)|Shortsword|auto-trad|
|[mZVd3ZcajfGKAm79.htm](agents-of-edgewatch-bestiary-items/mZVd3ZcajfGKAm79.htm)|At-Will Spells|auto-trad|
|[n0iAPmFsdZlSVqHV.htm](agents-of-edgewatch-bestiary-items/n0iAPmFsdZlSVqHV.htm)|Dagger|auto-trad|
|[n4IYsyn6jwdp0itB.htm](agents-of-edgewatch-bestiary-items/n4IYsyn6jwdp0itB.htm)|Blade Legs|auto-trad|
|[N5zf8LJKXg8KdskQ.htm](agents-of-edgewatch-bestiary-items/N5zf8LJKXg8KdskQ.htm)|Kharnas's Blessing|auto-trad|
|[n72S7jEvm5AQeVky.htm](agents-of-edgewatch-bestiary-items/n72S7jEvm5AQeVky.htm)|Dagger|auto-trad|
|[N85UwJsuAe8nwuWV.htm](agents-of-edgewatch-bestiary-items/N85UwJsuAe8nwuWV.htm)|Spell Circle|auto-trad|
|[n8gYgKZb2msziuyh.htm](agents-of-edgewatch-bestiary-items/n8gYgKZb2msziuyh.htm)|Darkvision|auto-trad|
|[N8sxUVjIH1fEWXxu.htm](agents-of-edgewatch-bestiary-items/N8sxUVjIH1fEWXxu.htm)|Arcane Prepared Spells|auto-trad|
|[n9d7kqBHgMv3tOmv.htm](agents-of-edgewatch-bestiary-items/n9d7kqBHgMv3tOmv.htm)|Choking Smog|auto-trad|
|[n9oO2EI8HwHSgtLl.htm](agents-of-edgewatch-bestiary-items/n9oO2EI8HwHSgtLl.htm)|Terinav Root Poison|auto-trad|
|[nA0QdxrRaEjv6RhP.htm](agents-of-edgewatch-bestiary-items/nA0QdxrRaEjv6RhP.htm)|Attack of Opportunity|auto-trad|
|[nA5lgWKn3AryAuFs.htm](agents-of-edgewatch-bestiary-items/nA5lgWKn3AryAuFs.htm)|Twisted Desires|auto-trad|
|[Nbx9ouRvtXU7r7XP.htm](agents-of-edgewatch-bestiary-items/Nbx9ouRvtXU7r7XP.htm)|Dart Volley|auto-trad|
|[nCwbF6k6DjALgFuy.htm](agents-of-edgewatch-bestiary-items/nCwbF6k6DjALgFuy.htm)|Rage|auto-trad|
|[NDjY88hxyNPqDwPf.htm](agents-of-edgewatch-bestiary-items/NDjY88hxyNPqDwPf.htm)|Quick Bomber|auto-trad|
|[Ng3bVzRpGnsUr9AL.htm](agents-of-edgewatch-bestiary-items/Ng3bVzRpGnsUr9AL.htm)|Dagger|auto-trad|
|[NGB5wLXcmJCijnEn.htm](agents-of-edgewatch-bestiary-items/NGB5wLXcmJCijnEn.htm)|Jaws|auto-trad|
|[NGOG0Y8bjtNkNrjz.htm](agents-of-edgewatch-bestiary-items/NGOG0Y8bjtNkNrjz.htm)|Spear|auto-trad|
|[NHDyB7FzdAXPJvx3.htm](agents-of-edgewatch-bestiary-items/NHDyB7FzdAXPJvx3.htm)|At-Will Spells|auto-trad|
|[nHj4mI5oMJrWP9jm.htm](agents-of-edgewatch-bestiary-items/nHj4mI5oMJrWP9jm.htm)|Grab|auto-trad|
|[NhWlBh7zqwLP19mb.htm](agents-of-edgewatch-bestiary-items/NhWlBh7zqwLP19mb.htm)|Mindtwisting Utterance|auto-trad|
|[nhz5F428cTKGkc9v.htm](agents-of-edgewatch-bestiary-items/nhz5F428cTKGkc9v.htm)|Inexorable March|auto-trad|
|[NI333m4B4OMeocLf.htm](agents-of-edgewatch-bestiary-items/NI333m4B4OMeocLf.htm)|Darkvision|auto-trad|
|[NILfejzFJ409TN4i.htm](agents-of-edgewatch-bestiary-items/NILfejzFJ409TN4i.htm)|Poison Conversion|auto-trad|
|[niqal0ySwUPyqG0P.htm](agents-of-edgewatch-bestiary-items/niqal0ySwUPyqG0P.htm)|Eye Probe|auto-trad|
|[NIWpD2eqbaBc5M8m.htm](agents-of-edgewatch-bestiary-items/NIWpD2eqbaBc5M8m.htm)|Fist|auto-trad|
|[Nl7SDGVnqgpHC581.htm](agents-of-edgewatch-bestiary-items/Nl7SDGVnqgpHC581.htm)|At-Will Spells|auto-trad|
|[nO9dKSqf8ZjJRuCy.htm](agents-of-edgewatch-bestiary-items/nO9dKSqf8ZjJRuCy.htm)|Change Shape|auto-trad|
|[NPjuafWJNiaGNe5E.htm](agents-of-edgewatch-bestiary-items/NPjuafWJNiaGNe5E.htm)|Divine Innate Spells|auto-trad|
|[nqhGLlFeLoFFBN9g.htm](agents-of-edgewatch-bestiary-items/nqhGLlFeLoFFBN9g.htm)|Light Form|auto-trad|
|[nqTM0MryJthdq3ZY.htm](agents-of-edgewatch-bestiary-items/nqTM0MryJthdq3ZY.htm)|Extra Reaction|auto-trad|
|[nRzUgQJS26xT7C1q.htm](agents-of-edgewatch-bestiary-items/nRzUgQJS26xT7C1q.htm)|Innate Occult Spells|auto-trad|
|[NsvkBq0sahLrYsTg.htm](agents-of-edgewatch-bestiary-items/NsvkBq0sahLrYsTg.htm)|Elven Curve Blade|auto-trad|
|[Ntt0pX48FlDnOFb3.htm](agents-of-edgewatch-bestiary-items/Ntt0pX48FlDnOFb3.htm)|Arcane Innate Spells|auto-trad|
|[nurJkCpZvHiPZo76.htm](agents-of-edgewatch-bestiary-items/nurJkCpZvHiPZo76.htm)|Project False Image|auto-trad|
|[nvUzHv6therpBM38.htm](agents-of-edgewatch-bestiary-items/nvUzHv6therpBM38.htm)|Aquatic Ambush|auto-trad|
|[nW0moOpMRYhgNi82.htm](agents-of-edgewatch-bestiary-items/nW0moOpMRYhgNi82.htm)|Trunk|auto-trad|
|[nXrzkclnA7IMvnFu.htm](agents-of-edgewatch-bestiary-items/nXrzkclnA7IMvnFu.htm)|Pitfall and Plunger|auto-trad|
|[nXZygeRYzYRESgcP.htm](agents-of-edgewatch-bestiary-items/nXZygeRYzYRESgcP.htm)|Fist|auto-trad|
|[nyahaCV93O7YYyau.htm](agents-of-edgewatch-bestiary-items/nyahaCV93O7YYyau.htm)|Breach Vulnerability|auto-trad|
|[NyD5wXkVBGB2Wu0I.htm](agents-of-edgewatch-bestiary-items/NyD5wXkVBGB2Wu0I.htm)|Protean Anatomy 20|auto-trad|
|[o03yd3s67EeP9WOh.htm](agents-of-edgewatch-bestiary-items/o03yd3s67EeP9WOh.htm)|Prescient Revision|auto-trad|
|[o11O0K7oIAQznUJo.htm](agents-of-edgewatch-bestiary-items/o11O0K7oIAQznUJo.htm)|Bloody Spew|auto-trad|
|[O19b688nMXlfxdQP.htm](agents-of-edgewatch-bestiary-items/O19b688nMXlfxdQP.htm)|Twin Takedown|auto-trad|
|[o1pfLDPNAQy8FT0p.htm](agents-of-edgewatch-bestiary-items/o1pfLDPNAQy8FT0p.htm)|Point Blank|auto-trad|
|[o1vU8G41mZhtjXsb.htm](agents-of-edgewatch-bestiary-items/o1vU8G41mZhtjXsb.htm)|Sling|auto-trad|
|[O3FvbwzFjYGBuqWx.htm](agents-of-edgewatch-bestiary-items/O3FvbwzFjYGBuqWx.htm)|Shield Block|auto-trad|
|[O6wQ2EWNzgJGn6Ta.htm](agents-of-edgewatch-bestiary-items/O6wQ2EWNzgJGn6Ta.htm)|Hidden Paragon|auto-trad|
|[O97E3bY0dRr5pLCH.htm](agents-of-edgewatch-bestiary-items/O97E3bY0dRr5pLCH.htm)|Inflict Warpwave|auto-trad|
|[oaYvNv83OSKkOI6V.htm](agents-of-edgewatch-bestiary-items/oaYvNv83OSKkOI6V.htm)|Performance Anxiety|auto-trad|
|[oAz0VaKaZ2Hym3GL.htm](agents-of-edgewatch-bestiary-items/oAz0VaKaZ2Hym3GL.htm)|Dagger|auto-trad|
|[ob3CKat2QrsjJqn4.htm](agents-of-edgewatch-bestiary-items/ob3CKat2QrsjJqn4.htm)|Innate Occult Spells|auto-trad|
|[oBDa2RafFTX6BBua.htm](agents-of-edgewatch-bestiary-items/oBDa2RafFTX6BBua.htm)|Grab|auto-trad|
|[ObuiuOqfP6nhvs9S.htm](agents-of-edgewatch-bestiary-items/ObuiuOqfP6nhvs9S.htm)|Tug|auto-trad|
|[OCmlinuwWYcoVWLc.htm](agents-of-edgewatch-bestiary-items/OCmlinuwWYcoVWLc.htm)|Scurry|auto-trad|
|[odph4tUxJEzbuuiq.htm](agents-of-edgewatch-bestiary-items/odph4tUxJEzbuuiq.htm)|Impaler|auto-trad|
|[OdVJynq0IATGWujh.htm](agents-of-edgewatch-bestiary-items/OdVJynq0IATGWujh.htm)|Foot|auto-trad|
|[oEHHDWURCYEb1t7W.htm](agents-of-edgewatch-bestiary-items/oEHHDWURCYEb1t7W.htm)|Jaws|auto-trad|
|[OESsshXWIJSX1LE7.htm](agents-of-edgewatch-bestiary-items/OESsshXWIJSX1LE7.htm)|Blood-Fueled Titter|auto-trad|
|[of0eB3PZMwreV0L4.htm](agents-of-edgewatch-bestiary-items/of0eB3PZMwreV0L4.htm)|Furious Pacifier|auto-trad|
|[OF5xPm4it251t0zo.htm](agents-of-edgewatch-bestiary-items/OF5xPm4it251t0zo.htm)|Reflective Plating|auto-trad|
|[OF6N85WER1HxJgCK.htm](agents-of-edgewatch-bestiary-items/OF6N85WER1HxJgCK.htm)|Integrated Launcher|auto-trad|
|[ofiTdcCz7bSlRXmy.htm](agents-of-edgewatch-bestiary-items/ofiTdcCz7bSlRXmy.htm)|Marrow Rot|auto-trad|
|[oFx3vt7MvXsfxzSp.htm](agents-of-edgewatch-bestiary-items/oFx3vt7MvXsfxzSp.htm)|Fist|auto-trad|
|[OfYcuvZbwxbjF6cG.htm](agents-of-edgewatch-bestiary-items/OfYcuvZbwxbjF6cG.htm)|Vein Walker|auto-trad|
|[oIOsV3m1VCQehtGp.htm](agents-of-edgewatch-bestiary-items/oIOsV3m1VCQehtGp.htm)|Magic Horn|auto-trad|
|[OIT5GvqodAryKJpv.htm](agents-of-edgewatch-bestiary-items/OIT5GvqodAryKJpv.htm)|Sling|auto-trad|
|[ojlesPsjiDHjASWl.htm](agents-of-edgewatch-bestiary-items/ojlesPsjiDHjASWl.htm)|Arcane Spontaneous Spells|auto-trad|
|[oJWZY02OG1m5LrP6.htm](agents-of-edgewatch-bestiary-items/oJWZY02OG1m5LrP6.htm)|Telekinetic Reach|auto-trad|
|[oKdyZTnDb5xbqpF3.htm](agents-of-edgewatch-bestiary-items/oKdyZTnDb5xbqpF3.htm)|Hamstring|auto-trad|
|[OmGZxI6hAdkJlkwY.htm](agents-of-edgewatch-bestiary-items/OmGZxI6hAdkJlkwY.htm)|Attack of Opportunity|auto-trad|
|[OMtflrg7VM1ogHqZ.htm](agents-of-edgewatch-bestiary-items/OMtflrg7VM1ogHqZ.htm)|Tegresin's Greeting|auto-trad|
|[OofTe2WDiLiEdYTm.htm](agents-of-edgewatch-bestiary-items/OofTe2WDiLiEdYTm.htm)|Halfling Luck|auto-trad|
|[oOj2D6OXRFRGfvRp.htm](agents-of-edgewatch-bestiary-items/oOj2D6OXRFRGfvRp.htm)|Vomit Blood|auto-trad|
|[OpV99ABQlIY3FF4G.htm](agents-of-edgewatch-bestiary-items/OpV99ABQlIY3FF4G.htm)|Escort from the Premises|auto-trad|
|[oq1IG7RwkiKMV9mP.htm](agents-of-edgewatch-bestiary-items/oq1IG7RwkiKMV9mP.htm)|Occult Innate Spells|auto-trad|
|[OqEu80bVGGZXPfVs.htm](agents-of-edgewatch-bestiary-items/OqEu80bVGGZXPfVs.htm)|Devastating Blast|auto-trad|
|[oQgehOwZMcJQ5i4V.htm](agents-of-edgewatch-bestiary-items/oQgehOwZMcJQ5i4V.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Os5QOeYEAod9Yg1T.htm](agents-of-edgewatch-bestiary-items/Os5QOeYEAod9Yg1T.htm)|Tentacle|auto-trad|
|[osA98Km4yrxpF38T.htm](agents-of-edgewatch-bestiary-items/osA98Km4yrxpF38T.htm)|Steel Quill|auto-trad|
|[ossSXTeiyXQFkTmA.htm](agents-of-edgewatch-bestiary-items/ossSXTeiyXQFkTmA.htm)|Attack of Opportunity|auto-trad|
|[OTSaRNEQPlVfldSC.htm](agents-of-edgewatch-bestiary-items/OTSaRNEQPlVfldSC.htm)|At-Will Spells|auto-trad|
|[oTX4OOEDagBTll1x.htm](agents-of-edgewatch-bestiary-items/oTX4OOEDagBTll1x.htm)|Prepared Arcane Spells|auto-trad|
|[oXzm9KqLtbgKvm14.htm](agents-of-edgewatch-bestiary-items/oXzm9KqLtbgKvm14.htm)|Summon Elementals|auto-trad|
|[ozjGo4jmzdNhJfAA.htm](agents-of-edgewatch-bestiary-items/ozjGo4jmzdNhJfAA.htm)|Shortsword|auto-trad|
|[p8i5dtNaUJaS4W11.htm](agents-of-edgewatch-bestiary-items/p8i5dtNaUJaS4W11.htm)|Battle Axe|auto-trad|
|[p8PqMvUhFUkgJL4b.htm](agents-of-edgewatch-bestiary-items/p8PqMvUhFUkgJL4b.htm)|Fist|auto-trad|
|[Pc7yhV1GBS53uadp.htm](agents-of-edgewatch-bestiary-items/Pc7yhV1GBS53uadp.htm)|Crossbow|auto-trad|
|[PCytOdylz3vMj2WE.htm](agents-of-edgewatch-bestiary-items/PCytOdylz3vMj2WE.htm)|Claw|auto-trad|
|[pEH5ZGr2J9sZg6ce.htm](agents-of-edgewatch-bestiary-items/pEH5ZGr2J9sZg6ce.htm)|Stabbing Beast Venom|auto-trad|
|[pEr8VvYetu8yNn5t.htm](agents-of-edgewatch-bestiary-items/pEr8VvYetu8yNn5t.htm)|Efficient Winding|auto-trad|
|[PFD814sBXDi1v2I7.htm](agents-of-edgewatch-bestiary-items/PFD814sBXDi1v2I7.htm)|Sneak Attack|auto-trad|
|[PfYigOWR2ynFziwC.htm](agents-of-edgewatch-bestiary-items/PfYigOWR2ynFziwC.htm)|Spear|auto-trad|
|[pgSTLlpCYo6JjJeZ.htm](agents-of-edgewatch-bestiary-items/pgSTLlpCYo6JjJeZ.htm)|Improved Grab|auto-trad|
|[phWjGBiRvnlb7wDJ.htm](agents-of-edgewatch-bestiary-items/phWjGBiRvnlb7wDJ.htm)|Constrict|auto-trad|
|[PigzABnoSkZuUAGP.htm](agents-of-edgewatch-bestiary-items/PigzABnoSkZuUAGP.htm)|Expanded Splash|auto-trad|
|[pJcNWcLAWBXtvefA.htm](agents-of-edgewatch-bestiary-items/pJcNWcLAWBXtvefA.htm)|Clockwork Reconstruction|auto-trad|
|[PJdaCe5La2FhB5AN.htm](agents-of-edgewatch-bestiary-items/PJdaCe5La2FhB5AN.htm)|Swift Leap|auto-trad|
|[pjqAB7UZDOSJ8Dvg.htm](agents-of-edgewatch-bestiary-items/pjqAB7UZDOSJ8Dvg.htm)|Scissors|auto-trad|
|[pk7WhgmEvUXgGRiz.htm](agents-of-edgewatch-bestiary-items/pk7WhgmEvUXgGRiz.htm)|No MAP|auto-trad|
|[pl4CFdDU1X88olbS.htm](agents-of-edgewatch-bestiary-items/pl4CFdDU1X88olbS.htm)|Captive Rake|auto-trad|
|[PlFu6E3ZCVF3JCam.htm](agents-of-edgewatch-bestiary-items/PlFu6E3ZCVF3JCam.htm)|Nightmare Rider|auto-trad|
|[pNlE1um147bT0E6P.htm](agents-of-edgewatch-bestiary-items/pNlE1um147bT0E6P.htm)|Animated Statues|auto-trad|
|[pnx6alHWYJ3kLnBW.htm](agents-of-edgewatch-bestiary-items/pnx6alHWYJ3kLnBW.htm)|Proven Devotion|auto-trad|
|[PoQgBkVgVkqOOV5e.htm](agents-of-edgewatch-bestiary-items/PoQgBkVgVkqOOV5e.htm)|Fling Offal|auto-trad|
|[PQM8ukSoaiRa6PDB.htm](agents-of-edgewatch-bestiary-items/PQM8ukSoaiRa6PDB.htm)|Innate Occult Spells|auto-trad|
|[psPKDFy6x0wj7ko0.htm](agents-of-edgewatch-bestiary-items/psPKDFy6x0wj7ko0.htm)|Cleaver|auto-trad|
|[Ptf4oQIP2T6623QL.htm](agents-of-edgewatch-bestiary-items/Ptf4oQIP2T6623QL.htm)|Leaping Charge|auto-trad|
|[PtGiCuINX35GsQBg.htm](agents-of-edgewatch-bestiary-items/PtGiCuINX35GsQBg.htm)|Darkvision|auto-trad|
|[pu72YVc5Xv4KyIqf.htm](agents-of-edgewatch-bestiary-items/pu72YVc5Xv4KyIqf.htm)|Dagger|auto-trad|
|[pUqjSa0MTNAe7fLX.htm](agents-of-edgewatch-bestiary-items/pUqjSa0MTNAe7fLX.htm)|Guillotine Blade|auto-trad|
|[PvgGyV08NoFmrceo.htm](agents-of-edgewatch-bestiary-items/PvgGyV08NoFmrceo.htm)|Jaws|auto-trad|
|[PvjXCmuXDpKKHSIf.htm](agents-of-edgewatch-bestiary-items/PvjXCmuXDpKKHSIf.htm)|Innate Primal Spells|auto-trad|
|[pWqtIfqDJ0Fag2CW.htm](agents-of-edgewatch-bestiary-items/pWqtIfqDJ0Fag2CW.htm)|Waylay|auto-trad|
|[PWYoCF0aSsWVwxCI.htm](agents-of-edgewatch-bestiary-items/PWYoCF0aSsWVwxCI.htm)|Sneak Attack|auto-trad|
|[PxaUCfocdE8pfDRJ.htm](agents-of-edgewatch-bestiary-items/PxaUCfocdE8pfDRJ.htm)|Dagger|auto-trad|
|[py2SpejCikEONHQm.htm](agents-of-edgewatch-bestiary-items/py2SpejCikEONHQm.htm)|Constant Spells|auto-trad|
|[q0zeFHZ9WaJ92IKN.htm](agents-of-edgewatch-bestiary-items/q0zeFHZ9WaJ92IKN.htm)|Jaws|auto-trad|
|[Q10SWwitCSafoJ4L.htm](agents-of-edgewatch-bestiary-items/Q10SWwitCSafoJ4L.htm)|Shared Diversion|auto-trad|
|[Q1MxJ1mg3fC5zwcl.htm](agents-of-edgewatch-bestiary-items/Q1MxJ1mg3fC5zwcl.htm)|Grab|auto-trad|
|[q47XrkqT8jn5rAqq.htm](agents-of-edgewatch-bestiary-items/q47XrkqT8jn5rAqq.htm)|Grab|auto-trad|
|[Q4P1CH4AQAlYpZ0X.htm](agents-of-edgewatch-bestiary-items/Q4P1CH4AQAlYpZ0X.htm)|Golem Antimagic|auto-trad|
|[q4WljID3qrZ5Fpnr.htm](agents-of-edgewatch-bestiary-items/q4WljID3qrZ5Fpnr.htm)|Wind-Up|auto-trad|
|[qALAo7rXGBdfG3m4.htm](agents-of-edgewatch-bestiary-items/qALAo7rXGBdfG3m4.htm)|Dagger|auto-trad|
|[Qc7N0GKOoFezaIK1.htm](agents-of-edgewatch-bestiary-items/Qc7N0GKOoFezaIK1.htm)|Fist|auto-trad|
|[QCKNcu26UUfwBynU.htm](agents-of-edgewatch-bestiary-items/QCKNcu26UUfwBynU.htm)|Calculated Splash|auto-trad|
|[qCVeUkgwYXq7lu8z.htm](agents-of-edgewatch-bestiary-items/qCVeUkgwYXq7lu8z.htm)|Bloody Handprint|auto-trad|
|[QDbM7WGGXh21h9Xw.htm](agents-of-edgewatch-bestiary-items/QDbM7WGGXh21h9Xw.htm)|Nimble Dodge|auto-trad|
|[qFILNq2kmX5xF0Dt.htm](agents-of-edgewatch-bestiary-items/qFILNq2kmX5xF0Dt.htm)|Black Ink Delirium|auto-trad|
|[Qg91eQ3wUA0CISzT.htm](agents-of-edgewatch-bestiary-items/Qg91eQ3wUA0CISzT.htm)|Occult Innate Spells|auto-trad|
|[QgE3ujjsFZSthy7z.htm](agents-of-edgewatch-bestiary-items/QgE3ujjsFZSthy7z.htm)|Illusory Copies|auto-trad|
|[qgStk0bh308sgMGP.htm](agents-of-edgewatch-bestiary-items/qgStk0bh308sgMGP.htm)|Dagger|auto-trad|
|[qgu0AUWtGhup6Nyf.htm](agents-of-edgewatch-bestiary-items/qgu0AUWtGhup6Nyf.htm)|Spine|auto-trad|
|[qHyTxio8Rf0fGBM5.htm](agents-of-edgewatch-bestiary-items/qHyTxio8Rf0fGBM5.htm)|Constant Spells|auto-trad|
|[QJApLqpe9NsQa7B2.htm](agents-of-edgewatch-bestiary-items/QJApLqpe9NsQa7B2.htm)|Attack of Opportunity|auto-trad|
|[qLsi1de5hGC6uHjL.htm](agents-of-edgewatch-bestiary-items/qLsi1de5hGC6uHjL.htm)|Spell Ambush|auto-trad|
|[QMeKA8gRtzqiRWQ5.htm](agents-of-edgewatch-bestiary-items/QMeKA8gRtzqiRWQ5.htm)|Tail|auto-trad|
|[QMmywYM7lzMllSGS.htm](agents-of-edgewatch-bestiary-items/QMmywYM7lzMllSGS.htm)|Darkvision|auto-trad|
|[qnVCozwzjFK6gieU.htm](agents-of-edgewatch-bestiary-items/qnVCozwzjFK6gieU.htm)|Dagger|auto-trad|
|[qOiRJ4goVAPViPIt.htm](agents-of-edgewatch-bestiary-items/qOiRJ4goVAPViPIt.htm)|Hurl|auto-trad|
|[qoxTaaEWLHDHtzML.htm](agents-of-edgewatch-bestiary-items/qoxTaaEWLHDHtzML.htm)|Claw|auto-trad|
|[QozBQD2QZP7kMYOG.htm](agents-of-edgewatch-bestiary-items/QozBQD2QZP7kMYOG.htm)|Control Body|auto-trad|
|[QP06eTmf8XusaVPV.htm](agents-of-edgewatch-bestiary-items/QP06eTmf8XusaVPV.htm)|Explode|auto-trad|
|[QprgJpKB6Wfofr3t.htm](agents-of-edgewatch-bestiary-items/QprgJpKB6Wfofr3t.htm)|Focus Spells|auto-trad|
|[QSKXWJ8br4qwHbtf.htm](agents-of-edgewatch-bestiary-items/QSKXWJ8br4qwHbtf.htm)|Paralysis|auto-trad|
|[quBDtUjWWKx7qiFU.htm](agents-of-edgewatch-bestiary-items/quBDtUjWWKx7qiFU.htm)|Innate Primal Spells|auto-trad|
|[Quf28S7PJstH1L0b.htm](agents-of-edgewatch-bestiary-items/Quf28S7PJstH1L0b.htm)|Spill Eyeballs|auto-trad|
|[qUO8L9YDXGIF28f3.htm](agents-of-edgewatch-bestiary-items/qUO8L9YDXGIF28f3.htm)|Feeblemind Ray|auto-trad|
|[QURekRpCFthbGcMS.htm](agents-of-edgewatch-bestiary-items/QURekRpCFthbGcMS.htm)|Scatterbrain Palm|auto-trad|
|[Qv8OyAidG7v9lnwp.htm](agents-of-edgewatch-bestiary-items/Qv8OyAidG7v9lnwp.htm)|Tear Flesh|auto-trad|
|[qVB5KclnPFeoDGPo.htm](agents-of-edgewatch-bestiary-items/qVB5KclnPFeoDGPo.htm)|Darkvision|auto-trad|
|[r1GptaN7xdyOuJwe.htm](agents-of-edgewatch-bestiary-items/r1GptaN7xdyOuJwe.htm)|Tail|auto-trad|
|[R1OmPKgnrpEPWGr6.htm](agents-of-edgewatch-bestiary-items/R1OmPKgnrpEPWGr6.htm)|Attack of Opportunity|auto-trad|
|[r2CSyvxtD75InoFP.htm](agents-of-edgewatch-bestiary-items/r2CSyvxtD75InoFP.htm)|Sneak Attack|auto-trad|
|[r47iaCjc2jC8iNnL.htm](agents-of-edgewatch-bestiary-items/r47iaCjc2jC8iNnL.htm)|Draconic Momentum|auto-trad|
|[r7vHhfdRSulBJtov.htm](agents-of-edgewatch-bestiary-items/r7vHhfdRSulBJtov.htm)|Truth Vulnerability|auto-trad|
|[RB7QxnNxpj30rjOL.htm](agents-of-edgewatch-bestiary-items/RB7QxnNxpj30rjOL.htm)|Constant Spells|auto-trad|
|[rbbCn6nAeaPo4dUD.htm](agents-of-edgewatch-bestiary-items/rbbCn6nAeaPo4dUD.htm)|Pseudopod|auto-trad|
|[rBy8DTqkrMtivM2r.htm](agents-of-edgewatch-bestiary-items/rBy8DTqkrMtivM2r.htm)|At-Will Spells|auto-trad|
|[rDNFst5KVOAvifdy.htm](agents-of-edgewatch-bestiary-items/rDNFst5KVOAvifdy.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[rDNKRU6LA2e9G96b.htm](agents-of-edgewatch-bestiary-items/rDNKRU6LA2e9G96b.htm)|Jaws|auto-trad|
|[REB6Z7mAMxxSKiun.htm](agents-of-edgewatch-bestiary-items/REB6Z7mAMxxSKiun.htm)|At-Will Spells|auto-trad|
|[RfaP98Gi8H94iPk3.htm](agents-of-edgewatch-bestiary-items/RfaP98Gi8H94iPk3.htm)|Crossbow|auto-trad|
|[RgfNXAlieDWY02I4.htm](agents-of-edgewatch-bestiary-items/RgfNXAlieDWY02I4.htm)|Alchemical Bomb|auto-trad|
|[RGHyYuZOOFXsZGfS.htm](agents-of-edgewatch-bestiary-items/RGHyYuZOOFXsZGfS.htm)|Fist|auto-trad|
|[rhQHRT4MPNImnl8e.htm](agents-of-edgewatch-bestiary-items/rhQHRT4MPNImnl8e.htm)|True Appearance|auto-trad|
|[RI07SPUsr3EfIXQX.htm](agents-of-edgewatch-bestiary-items/RI07SPUsr3EfIXQX.htm)|Tail|auto-trad|
|[RI1LAadtF6vq96g2.htm](agents-of-edgewatch-bestiary-items/RI1LAadtF6vq96g2.htm)|Claw|auto-trad|
|[Rif86iiefabTTeXm.htm](agents-of-edgewatch-bestiary-items/Rif86iiefabTTeXm.htm)|Hand Crossbow|auto-trad|
|[RiF9Uw43dbvL8TEa.htm](agents-of-edgewatch-bestiary-items/RiF9Uw43dbvL8TEa.htm)|Consume Poison|auto-trad|
|[rJILDneSIHcw62xJ.htm](agents-of-edgewatch-bestiary-items/rJILDneSIHcw62xJ.htm)|Dagger|auto-trad|
|[rJu8a6w24iFChezA.htm](agents-of-edgewatch-bestiary-items/rJu8a6w24iFChezA.htm)|Transfusion Aura|auto-trad|
|[rJXLqXZbLYPfyPpc.htm](agents-of-edgewatch-bestiary-items/rJXLqXZbLYPfyPpc.htm)|Lifesense 60 feet|auto-trad|
|[RkUX9VYzuGsIycUb.htm](agents-of-edgewatch-bestiary-items/RkUX9VYzuGsIycUb.htm)|Blood Scent|auto-trad|
|[rLFEG0L8t229JOcM.htm](agents-of-edgewatch-bestiary-items/rLFEG0L8t229JOcM.htm)|Jaws|auto-trad|
|[rlsHyYTiGdVwRStH.htm](agents-of-edgewatch-bestiary-items/rlsHyYTiGdVwRStH.htm)|Grab|auto-trad|
|[rlZ9VxWPwn9XfGgL.htm](agents-of-edgewatch-bestiary-items/rlZ9VxWPwn9XfGgL.htm)|Breath Weapon|auto-trad|
|[rMKwSTFsZNjo251o.htm](agents-of-edgewatch-bestiary-items/rMKwSTFsZNjo251o.htm)|Dueling Parry|auto-trad|
|[RPBXXyVoR76aOb4H.htm](agents-of-edgewatch-bestiary-items/RPBXXyVoR76aOb4H.htm)|Rapier|auto-trad|
|[RPGIrySxh4Qa0yaP.htm](agents-of-edgewatch-bestiary-items/RPGIrySxh4Qa0yaP.htm)|Sneak Attack|auto-trad|
|[rpruEIwmxeOzTZOp.htm](agents-of-edgewatch-bestiary-items/rpruEIwmxeOzTZOp.htm)|Dream Haunting|auto-trad|
|[rPZdz4pgzoaAanDn.htm](agents-of-edgewatch-bestiary-items/rPZdz4pgzoaAanDn.htm)|Swallow Whole|auto-trad|
|[RQ0R47s8yyK8gYS8.htm](agents-of-edgewatch-bestiary-items/RQ0R47s8yyK8gYS8.htm)|Stretching Step|auto-trad|
|[rr5bpEab2TNVmDUt.htm](agents-of-edgewatch-bestiary-items/rr5bpEab2TNVmDUt.htm)|Constrict|auto-trad|
|[RSzJjCjwpz8jIS3J.htm](agents-of-edgewatch-bestiary-items/RSzJjCjwpz8jIS3J.htm)|Iron Golem Poison|auto-trad|
|[rtBp1EzJQp3x5EsU.htm](agents-of-edgewatch-bestiary-items/rtBp1EzJQp3x5EsU.htm)|Darkvision|auto-trad|
|[rwp43QY136LRMqdW.htm](agents-of-edgewatch-bestiary-items/rwp43QY136LRMqdW.htm)|Darkvision|auto-trad|
|[rWzEv0M5A3nxCW6P.htm](agents-of-edgewatch-bestiary-items/rWzEv0M5A3nxCW6P.htm)|Constant Spells|auto-trad|
|[rxkGSEBo9iXMnETl.htm](agents-of-edgewatch-bestiary-items/rxkGSEBo9iXMnETl.htm)|At-Will Spells|auto-trad|
|[rY3uWiPQqOCSmYTu.htm](agents-of-edgewatch-bestiary-items/rY3uWiPQqOCSmYTu.htm)|Lore Master|auto-trad|
|[Ry9m2AVrebalVrSx.htm](agents-of-edgewatch-bestiary-items/Ry9m2AVrebalVrSx.htm)|Fist|auto-trad|
|[RywUmURTSevVYI7y.htm](agents-of-edgewatch-bestiary-items/RywUmURTSevVYI7y.htm)|Poisoner's Staff|auto-trad|
|[rZ6LtOq5W7Ef8R1W.htm](agents-of-edgewatch-bestiary-items/rZ6LtOq5W7Ef8R1W.htm)|Whirling Slashes|auto-trad|
|[S00gbcUTroHxccVJ.htm](agents-of-edgewatch-bestiary-items/S00gbcUTroHxccVJ.htm)|Pummeling Assault|auto-trad|
|[s1576Kzeq8JMVNz7.htm](agents-of-edgewatch-bestiary-items/s1576Kzeq8JMVNz7.htm)|Empathetic Response|auto-trad|
|[s4YFi2Scm3NoZSHU.htm](agents-of-edgewatch-bestiary-items/s4YFi2Scm3NoZSHU.htm)|Darting Flurry|auto-trad|
|[s5ejDTLYAXnRuoub.htm](agents-of-edgewatch-bestiary-items/s5ejDTLYAXnRuoub.htm)|Jaws|auto-trad|
|[s6jGCfKSGTptJo1G.htm](agents-of-edgewatch-bestiary-items/s6jGCfKSGTptJo1G.htm)|Blinding Bile|auto-trad|
|[s7eTzERmLr5TUCJL.htm](agents-of-edgewatch-bestiary-items/s7eTzERmLr5TUCJL.htm)|Jaws|auto-trad|
|[s7QApXyE5DjGF9aY.htm](agents-of-edgewatch-bestiary-items/s7QApXyE5DjGF9aY.htm)|Morningstar|auto-trad|
|[S8lzbxGXfumHXYYq.htm](agents-of-edgewatch-bestiary-items/S8lzbxGXfumHXYYq.htm)|Flay|auto-trad|
|[sbYDSGZlPbw5odWZ.htm](agents-of-edgewatch-bestiary-items/sbYDSGZlPbw5odWZ.htm)|Vulnerable to Shatter|auto-trad|
|[SczLJpfQwYYTKEvD.htm](agents-of-edgewatch-bestiary-items/SczLJpfQwYYTKEvD.htm)|Watery Simulacra|auto-trad|
|[sDzBK6rs6LhvAFPu.htm](agents-of-edgewatch-bestiary-items/sDzBK6rs6LhvAFPu.htm)|All-Around Vision|auto-trad|
|[SEDzDCcVcLItgSUy.htm](agents-of-edgewatch-bestiary-items/SEDzDCcVcLItgSUy.htm)|Reap|auto-trad|
|[Sf1u8xUKN1LLV2FB.htm](agents-of-edgewatch-bestiary-items/Sf1u8xUKN1LLV2FB.htm)|Grab|auto-trad|
|[sFeJf8GnNPDabG1D.htm](agents-of-edgewatch-bestiary-items/sFeJf8GnNPDabG1D.htm)|Fast Healing 10|auto-trad|
|[SGQATSbzafV9SYlH.htm](agents-of-edgewatch-bestiary-items/SGQATSbzafV9SYlH.htm)|Coordinated Distraction|auto-trad|
|[sgRdRygQY6ggn4fy.htm](agents-of-edgewatch-bestiary-items/sgRdRygQY6ggn4fy.htm)|Change Shape|auto-trad|
|[shJIHzhh4j6cG3tv.htm](agents-of-edgewatch-bestiary-items/shJIHzhh4j6cG3tv.htm)|Flurry of Claws|auto-trad|
|[siibhLVqReWpmq6r.htm](agents-of-edgewatch-bestiary-items/siibhLVqReWpmq6r.htm)|Darkvision|auto-trad|
|[SJraLZmz7x1C04ki.htm](agents-of-edgewatch-bestiary-items/SJraLZmz7x1C04ki.htm)|Vulnerable to Rust|auto-trad|
|[SMg5BMoXiwN9nxmJ.htm](agents-of-edgewatch-bestiary-items/SMg5BMoXiwN9nxmJ.htm)|Running Reload|auto-trad|
|[SMwhRQyOrogJbUal.htm](agents-of-edgewatch-bestiary-items/SMwhRQyOrogJbUal.htm)|Lifedrinker|auto-trad|
|[snJ95Lkh1TmrNndQ.htm](agents-of-edgewatch-bestiary-items/snJ95Lkh1TmrNndQ.htm)|Splash of Color|auto-trad|
|[SNqbJ3OBpkCDVRSr.htm](agents-of-edgewatch-bestiary-items/SNqbJ3OBpkCDVRSr.htm)|Shortsword|auto-trad|
|[sODZwisWeLEBKV8P.htm](agents-of-edgewatch-bestiary-items/sODZwisWeLEBKV8P.htm)|Prepared Arcane Spells|auto-trad|
|[SoEJRrsphmHa4aoG.htm](agents-of-edgewatch-bestiary-items/SoEJRrsphmHa4aoG.htm)|Rejuvenation|auto-trad|
|[SoNwHVeZMLSrrDEw.htm](agents-of-edgewatch-bestiary-items/SoNwHVeZMLSrrDEw.htm)|Items|auto-trad|
|[spdJfofzwtFFM3Cs.htm](agents-of-edgewatch-bestiary-items/spdJfofzwtFFM3Cs.htm)|At-Will Spells|auto-trad|
|[SriOV0CxqQFmpPCv.htm](agents-of-edgewatch-bestiary-items/SriOV0CxqQFmpPCv.htm)|Sneak Attack|auto-trad|
|[STPSnFrl7yD39Fni.htm](agents-of-edgewatch-bestiary-items/STPSnFrl7yD39Fni.htm)|Holy Greatsword|auto-trad|
|[sTXhA6HFH3RVnqjV.htm](agents-of-edgewatch-bestiary-items/sTXhA6HFH3RVnqjV.htm)|Final Blow|auto-trad|
|[sVBakTcoJU3kPGlY.htm](agents-of-edgewatch-bestiary-items/sVBakTcoJU3kPGlY.htm)|Whirlwind Kick|auto-trad|
|[SvCc6HA58iZaOg9T.htm](agents-of-edgewatch-bestiary-items/SvCc6HA58iZaOg9T.htm)|Unrelativity Field|auto-trad|
|[sZjoXHynGmaJPLpi.htm](agents-of-edgewatch-bestiary-items/sZjoXHynGmaJPLpi.htm)|Combustible|auto-trad|
|[sZRQVUQ7xiN6ZTK9.htm](agents-of-edgewatch-bestiary-items/sZRQVUQ7xiN6ZTK9.htm)|Grab|auto-trad|
|[T2NZbbINwDvovbim.htm](agents-of-edgewatch-bestiary-items/T2NZbbINwDvovbim.htm)|Darkvision|auto-trad|
|[T2pFSnOpz24hsKkz.htm](agents-of-edgewatch-bestiary-items/T2pFSnOpz24hsKkz.htm)|Hand Crossbow|auto-trad|
|[T4zHj7DFiaCBwf6f.htm](agents-of-edgewatch-bestiary-items/T4zHj7DFiaCBwf6f.htm)|Go Dark|auto-trad|
|[t5jAYbO0kLhWne6I.htm](agents-of-edgewatch-bestiary-items/t5jAYbO0kLhWne6I.htm)|Fist|auto-trad|
|[t5skSqnTS4aKuuan.htm](agents-of-edgewatch-bestiary-items/t5skSqnTS4aKuuan.htm)|Surprise Attack|auto-trad|
|[t7R0LcoxHegwmHyq.htm](agents-of-edgewatch-bestiary-items/t7R0LcoxHegwmHyq.htm)|Rapier|auto-trad|
|[T8yXNvNM2mPGZfoq.htm](agents-of-edgewatch-bestiary-items/T8yXNvNM2mPGZfoq.htm)|At-Will Spells|auto-trad|
|[TADC3gK2VlA4Jp9l.htm](agents-of-edgewatch-bestiary-items/TADC3gK2VlA4Jp9l.htm)|Mirror Duplicate|auto-trad|
|[TAic4n8t4egJtNJX.htm](agents-of-edgewatch-bestiary-items/TAic4n8t4egJtNJX.htm)|Deny Advantage|auto-trad|
|[TBb2TTPl5fRKJQJU.htm](agents-of-edgewatch-bestiary-items/TBb2TTPl5fRKJQJU.htm)|Grasping Bites|auto-trad|
|[Tbf45tR5JDVDyJCc.htm](agents-of-edgewatch-bestiary-items/Tbf45tR5JDVDyJCc.htm)|Rotting Aura|auto-trad|
|[tBGcGGksEkKqFPe7.htm](agents-of-edgewatch-bestiary-items/tBGcGGksEkKqFPe7.htm)|Alietta|auto-trad|
|[TcOqYdc5uITUBa9m.htm](agents-of-edgewatch-bestiary-items/TcOqYdc5uITUBa9m.htm)|Attack of Opportunity|auto-trad|
|[TeXV6pOiXpdWsvDV.htm](agents-of-edgewatch-bestiary-items/TeXV6pOiXpdWsvDV.htm)|Darkvision|auto-trad|
|[TfGlX9oclwIPR62r.htm](agents-of-edgewatch-bestiary-items/TfGlX9oclwIPR62r.htm)|Toxin-Inured|auto-trad|
|[Tfnx5jxpKaKtZ7ES.htm](agents-of-edgewatch-bestiary-items/Tfnx5jxpKaKtZ7ES.htm)|Choose Weakness|auto-trad|
|[TFvB8Q4vaggsZeH2.htm](agents-of-edgewatch-bestiary-items/TFvB8Q4vaggsZeH2.htm)|Infused Items|auto-trad|
|[TGZnX69H3vH84pPZ.htm](agents-of-edgewatch-bestiary-items/TGZnX69H3vH84pPZ.htm)|Ooze Tendril|auto-trad|
|[ThVw3eJTb5RzjlG8.htm](agents-of-edgewatch-bestiary-items/ThVw3eJTb5RzjlG8.htm)|Rapier Hand|auto-trad|
|[tIl9HHbyeahh21jc.htm](agents-of-edgewatch-bestiary-items/tIl9HHbyeahh21jc.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[Tillz0YYXHueJmui.htm](agents-of-edgewatch-bestiary-items/Tillz0YYXHueJmui.htm)|Dimensional Slide|auto-trad|
|[TiZR6umibymo5txc.htm](agents-of-edgewatch-bestiary-items/TiZR6umibymo5txc.htm)|Sickle|auto-trad|
|[Tk7abWUpvDDV6Qg0.htm](agents-of-edgewatch-bestiary-items/Tk7abWUpvDDV6Qg0.htm)|Succor Vulnerability|auto-trad|
|[TLLEMhBLZebrCg2V.htm](agents-of-edgewatch-bestiary-items/TLLEMhBLZebrCg2V.htm)|Clutching Cobbles|auto-trad|
|[TmEWLqRkXgHmnIV4.htm](agents-of-edgewatch-bestiary-items/TmEWLqRkXgHmnIV4.htm)|Change Shape|auto-trad|
|[tMhjT7tf9r9vZX4C.htm](agents-of-edgewatch-bestiary-items/tMhjT7tf9r9vZX4C.htm)|Vargouille Transformation|auto-trad|
|[tnOFl9MIpqf5OkVX.htm](agents-of-edgewatch-bestiary-items/tnOFl9MIpqf5OkVX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Tq4JjTyPFg3Q57Z4.htm](agents-of-edgewatch-bestiary-items/Tq4JjTyPFg3Q57Z4.htm)|Trident|auto-trad|
|[tvVxQhJ0X1FuWMhv.htm](agents-of-edgewatch-bestiary-items/tvVxQhJ0X1FuWMhv.htm)|Dagger|auto-trad|
|[tWZVevRequPfr7TC.htm](agents-of-edgewatch-bestiary-items/tWZVevRequPfr7TC.htm)|Innate Primal Spells|auto-trad|
|[TxhGff0LtzQslOci.htm](agents-of-edgewatch-bestiary-items/TxhGff0LtzQslOci.htm)|Easy to Call|auto-trad|
|[TXU2NotAQBux9iMZ.htm](agents-of-edgewatch-bestiary-items/TXU2NotAQBux9iMZ.htm)|Darkvision|auto-trad|
|[tyx67m7w5wjz8Xc5.htm](agents-of-edgewatch-bestiary-items/tyx67m7w5wjz8Xc5.htm)|Spear|auto-trad|
|[tZNd4FllHFLFPMgx.htm](agents-of-edgewatch-bestiary-items/tZNd4FllHFLFPMgx.htm)|Claw|auto-trad|
|[U0v9mqG5NfAe3NgW.htm](agents-of-edgewatch-bestiary-items/U0v9mqG5NfAe3NgW.htm)|Fast Healing 15|auto-trad|
|[U0xgJtIHGK7aaxUB.htm](agents-of-edgewatch-bestiary-items/U0xgJtIHGK7aaxUB.htm)|Negative Healing|auto-trad|
|[U1E1Xka1zLQxC4tT.htm](agents-of-edgewatch-bestiary-items/U1E1Xka1zLQxC4tT.htm)|Activate Attractor|auto-trad|
|[u1ffp4yNEVawu9dE.htm](agents-of-edgewatch-bestiary-items/u1ffp4yNEVawu9dE.htm)|Arcane Prepared Spells|auto-trad|
|[u4BY6CXSH5AXUTMv.htm](agents-of-edgewatch-bestiary-items/u4BY6CXSH5AXUTMv.htm)|Continuous Barrage|auto-trad|
|[u4Or7r9xHL9MH7uK.htm](agents-of-edgewatch-bestiary-items/u4Or7r9xHL9MH7uK.htm)|Negative Healing|auto-trad|
|[U5JrBAObBwJKEgSJ.htm](agents-of-edgewatch-bestiary-items/U5JrBAObBwJKEgSJ.htm)|Claw|auto-trad|
|[U64bIK9noEw8M2Ro.htm](agents-of-edgewatch-bestiary-items/U64bIK9noEw8M2Ro.htm)|Blood Chain|auto-trad|
|[U7PjzQ5AjISpvEhR.htm](agents-of-edgewatch-bestiary-items/U7PjzQ5AjISpvEhR.htm)|Greatpick|auto-trad|
|[U8JHbhefcDKeNERu.htm](agents-of-edgewatch-bestiary-items/U8JHbhefcDKeNERu.htm)|Constant Spells|auto-trad|
|[UAnT1rt4pcCFs6br.htm](agents-of-edgewatch-bestiary-items/UAnT1rt4pcCFs6br.htm)|Draconic Frenzy|auto-trad|
|[udFVDwS0vDvYW3tw.htm](agents-of-edgewatch-bestiary-items/udFVDwS0vDvYW3tw.htm)|Bloody Rebel|auto-trad|
|[uffmVAC7FAC8GDmU.htm](agents-of-edgewatch-bestiary-items/uffmVAC7FAC8GDmU.htm)|At-Will Spells|auto-trad|
|[UFTgVHYhhpDzdMpd.htm](agents-of-edgewatch-bestiary-items/UFTgVHYhhpDzdMpd.htm)|Darkvision|auto-trad|
|[uIpZm7bCNAs8c7BV.htm](agents-of-edgewatch-bestiary-items/uIpZm7bCNAs8c7BV.htm)|Vorpal Garrote|auto-trad|
|[UiWgpQaBrzfpuuvN.htm](agents-of-edgewatch-bestiary-items/UiWgpQaBrzfpuuvN.htm)|Dagger|auto-trad|
|[ukjOJLhlz5hOZcuy.htm](agents-of-edgewatch-bestiary-items/ukjOJLhlz5hOZcuy.htm)|Crystallize Blood|auto-trad|
|[uLs1qWcXHyZE14d2.htm](agents-of-edgewatch-bestiary-items/uLs1qWcXHyZE14d2.htm)|Darkvision|auto-trad|
|[uOIdVHoijKlxOrxM.htm](agents-of-edgewatch-bestiary-items/uOIdVHoijKlxOrxM.htm)|Abyssal Plague|auto-trad|
|[UOmYO7g7ej3NwdzH.htm](agents-of-edgewatch-bestiary-items/UOmYO7g7ej3NwdzH.htm)|Protean Anatomy 25|auto-trad|
|[uP5k61RUIateqmiN.htm](agents-of-edgewatch-bestiary-items/uP5k61RUIateqmiN.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[UP6qek6RNSS32oOG.htm](agents-of-edgewatch-bestiary-items/UP6qek6RNSS32oOG.htm)|Darkvision|auto-trad|
|[UTB8Ay3kwYWUQkpJ.htm](agents-of-edgewatch-bestiary-items/UTB8Ay3kwYWUQkpJ.htm)|Darkvision|auto-trad|
|[utBhBKO5t1Pl4TVo.htm](agents-of-edgewatch-bestiary-items/utBhBKO5t1Pl4TVo.htm)|Planar Coven|auto-trad|
|[UUw22Ccx1nqHZgFR.htm](agents-of-edgewatch-bestiary-items/UUw22Ccx1nqHZgFR.htm)|Unbalancing Blow|auto-trad|
|[uv8lYUKGJVNvxD5Q.htm](agents-of-edgewatch-bestiary-items/uv8lYUKGJVNvxD5Q.htm)|Darkvision|auto-trad|
|[uVoFX3A8pbUaPYi4.htm](agents-of-edgewatch-bestiary-items/uVoFX3A8pbUaPYi4.htm)|Divine Rituals|auto-trad|
|[uW2CbK6jDKtg1sG4.htm](agents-of-edgewatch-bestiary-items/uW2CbK6jDKtg1sG4.htm)|Storm Bringer|auto-trad|
|[UX4F0eelHvADT40O.htm](agents-of-edgewatch-bestiary-items/UX4F0eelHvADT40O.htm)|Gearblade|auto-trad|
|[uXNPc5LpmbvMlOa5.htm](agents-of-edgewatch-bestiary-items/uXNPc5LpmbvMlOa5.htm)|Breathe Death|auto-trad|
|[UylfhYpRBAz42DrH.htm](agents-of-edgewatch-bestiary-items/UylfhYpRBAz42DrH.htm)|Flurry of Blows|auto-trad|
|[UYn0m6msQksZeSqS.htm](agents-of-edgewatch-bestiary-items/UYn0m6msQksZeSqS.htm)|Exorcism|auto-trad|
|[uypEiarY3kwsOyuI.htm](agents-of-edgewatch-bestiary-items/uypEiarY3kwsOyuI.htm)|Divine Rituals|auto-trad|
|[uYvjUwSl8dqCBiVf.htm](agents-of-edgewatch-bestiary-items/uYvjUwSl8dqCBiVf.htm)|Temple Sword|auto-trad|
|[uzwUjI3gD8tO8Chh.htm](agents-of-edgewatch-bestiary-items/uzwUjI3gD8tO8Chh.htm)|Vulnerable to Flesh to Stone|auto-trad|
|[V3S7DLZ4aWxgcg1R.htm](agents-of-edgewatch-bestiary-items/V3S7DLZ4aWxgcg1R.htm)|Poison Ink|auto-trad|
|[vA2R6FhhaArEEKGJ.htm](agents-of-edgewatch-bestiary-items/vA2R6FhhaArEEKGJ.htm)|Claw|auto-trad|
|[va3CnUqjuHNbajzr.htm](agents-of-edgewatch-bestiary-items/va3CnUqjuHNbajzr.htm)|Impromptu Toxin|auto-trad|
|[vCow6RPYhJCBoynk.htm](agents-of-edgewatch-bestiary-items/vCow6RPYhJCBoynk.htm)|Staff|auto-trad|
|[VdYfW7jwgrSYwMvK.htm](agents-of-edgewatch-bestiary-items/VdYfW7jwgrSYwMvK.htm)|Grab|auto-trad|
|[VeiaoxMMsn7EH0Ri.htm](agents-of-edgewatch-bestiary-items/VeiaoxMMsn7EH0Ri.htm)|Purple Worm Sting|auto-trad|
|[vfIc0za5DwDp2KUV.htm](agents-of-edgewatch-bestiary-items/vfIc0za5DwDp2KUV.htm)|Divine Innate Spells|auto-trad|
|[vGPvwYP6vUTp3ILa.htm](agents-of-edgewatch-bestiary-items/vGPvwYP6vUTp3ILa.htm)|Composite Shortbow|auto-trad|
|[Vgy7PoKunyfdAFR8.htm](agents-of-edgewatch-bestiary-items/Vgy7PoKunyfdAFR8.htm)|Attack of Opportunity|auto-trad|
|[Vhh4ycX46oiBHKeS.htm](agents-of-edgewatch-bestiary-items/Vhh4ycX46oiBHKeS.htm)|Divine Innate Spells|auto-trad|
|[VHpzidpp88FeTXoa.htm](agents-of-edgewatch-bestiary-items/VHpzidpp88FeTXoa.htm)|Attack of Opportunity|auto-trad|
|[viGvtlLGXHhsAVh8.htm](agents-of-edgewatch-bestiary-items/viGvtlLGXHhsAVh8.htm)|Quick Stow|auto-trad|
|[Vj7wAYFFJXEJe0Ag.htm](agents-of-edgewatch-bestiary-items/Vj7wAYFFJXEJe0Ag.htm)|Stinger|auto-trad|
|[vJdzS91Cmz3WZZXD.htm](agents-of-edgewatch-bestiary-items/vJdzS91Cmz3WZZXD.htm)|Soul Siphon|auto-trad|
|[vL5RasoMgasgdDwB.htm](agents-of-edgewatch-bestiary-items/vL5RasoMgasgdDwB.htm)|Innate Arcane Spells|auto-trad|
|[VL9GemmMcofwTOB1.htm](agents-of-edgewatch-bestiary-items/VL9GemmMcofwTOB1.htm)|Chaos Hand|auto-trad|
|[vLQku20flQOsiRfM.htm](agents-of-edgewatch-bestiary-items/vLQku20flQOsiRfM.htm)|Jaws|auto-trad|
|[vObvjZsVBRVevJEo.htm](agents-of-edgewatch-bestiary-items/vObvjZsVBRVevJEo.htm)|Grab|auto-trad|
|[Vqq99svjAY5zIGFW.htm](agents-of-edgewatch-bestiary-items/Vqq99svjAY5zIGFW.htm)|Dagger|auto-trad|
|[VrU5JQWGpeNs37xp.htm](agents-of-edgewatch-bestiary-items/VrU5JQWGpeNs37xp.htm)|Constant Spells|auto-trad|
|[vRwV27JhbYTjeiSz.htm](agents-of-edgewatch-bestiary-items/vRwV27JhbYTjeiSz.htm)|Ritual|auto-trad|
|[VSaPPvdgJDTYrZHJ.htm](agents-of-edgewatch-bestiary-items/VSaPPvdgJDTYrZHJ.htm)|Knockdown|auto-trad|
|[VsMHrDx86310JmtX.htm](agents-of-edgewatch-bestiary-items/VsMHrDx86310JmtX.htm)|Sneak Attack|auto-trad|
|[vu11UJ4Yclcvha1F.htm](agents-of-edgewatch-bestiary-items/vu11UJ4Yclcvha1F.htm)|Prepared Arcane Spells|auto-trad|
|[VV329B6w0rioBP7u.htm](agents-of-edgewatch-bestiary-items/VV329B6w0rioBP7u.htm)|Shortsword|auto-trad|
|[vvMB5rLp5hejSAmy.htm](agents-of-edgewatch-bestiary-items/vvMB5rLp5hejSAmy.htm)|Arrogant Taunts|auto-trad|
|[VVMmL1VxeAzF1IBI.htm](agents-of-edgewatch-bestiary-items/VVMmL1VxeAzF1IBI.htm)|Demanding Orders|auto-trad|
|[Vw3bmxYidV4ocAb1.htm](agents-of-edgewatch-bestiary-items/Vw3bmxYidV4ocAb1.htm)|Drain Wand|auto-trad|
|[vW6qQ6HvTQdopnBl.htm](agents-of-edgewatch-bestiary-items/vW6qQ6HvTQdopnBl.htm)|Bloody Sneak Attack|auto-trad|
|[Vy9EtFybJQqo55xn.htm](agents-of-edgewatch-bestiary-items/Vy9EtFybJQqo55xn.htm)|Hypnotic Glow|auto-trad|
|[Vyd3GhdzugQrKNRt.htm](agents-of-edgewatch-bestiary-items/Vyd3GhdzugQrKNRt.htm)|Prepared Arcane Spells|auto-trad|
|[vynPyIOZZDBVXGPa.htm](agents-of-edgewatch-bestiary-items/vynPyIOZZDBVXGPa.htm)|Scent|auto-trad|
|[VyojjmuQFpZR3NHk.htm](agents-of-edgewatch-bestiary-items/VyojjmuQFpZR3NHk.htm)|Statue Shortsword|auto-trad|
|[vYvC5Vb8SsSJOIxk.htm](agents-of-edgewatch-bestiary-items/vYvC5Vb8SsSJOIxk.htm)|Innate Arcane Spells|auto-trad|
|[VzGeaeMNgzVgxvGo.htm](agents-of-edgewatch-bestiary-items/VzGeaeMNgzVgxvGo.htm)|Lusca Venom|auto-trad|
|[W211sY7aEqXIhFeB.htm](agents-of-edgewatch-bestiary-items/W211sY7aEqXIhFeB.htm)|Darkvision|auto-trad|
|[w3uomQxkiwXJF2hj.htm](agents-of-edgewatch-bestiary-items/w3uomQxkiwXJF2hj.htm)|Light Ray|auto-trad|
|[w53ToXCIso2kLmSW.htm](agents-of-edgewatch-bestiary-items/w53ToXCIso2kLmSW.htm)|Divine Innate Spells|auto-trad|
|[w5G4Ty8RBA7MDSTS.htm](agents-of-edgewatch-bestiary-items/w5G4Ty8RBA7MDSTS.htm)|Pitfall|auto-trad|
|[W5xPRbSycfvcG1bj.htm](agents-of-edgewatch-bestiary-items/W5xPRbSycfvcG1bj.htm)|Dart|auto-trad|
|[WaEIc7jlHfXb63gH.htm](agents-of-edgewatch-bestiary-items/WaEIc7jlHfXb63gH.htm)|Blasphemous Arms|auto-trad|
|[wAYzRxbZPCt548k7.htm](agents-of-edgewatch-bestiary-items/wAYzRxbZPCt548k7.htm)|Dual Assault|auto-trad|
|[wazlV57b7ZEUGjn6.htm](agents-of-edgewatch-bestiary-items/wazlV57b7ZEUGjn6.htm)|Steady Spellcasting|auto-trad|
|[Wbny5fScmVtKCw6w.htm](agents-of-edgewatch-bestiary-items/Wbny5fScmVtKCw6w.htm)|Whirring Doom|auto-trad|
|[wBPluFhMyu7RSQgs.htm](agents-of-edgewatch-bestiary-items/wBPluFhMyu7RSQgs.htm)|Light Mace|auto-trad|
|[Wc0IrjjTSicQkUfW.htm](agents-of-edgewatch-bestiary-items/Wc0IrjjTSicQkUfW.htm)|Wind Up|auto-trad|
|[WCFZ8IEU5wGPGAZK.htm](agents-of-edgewatch-bestiary-items/WCFZ8IEU5wGPGAZK.htm)|Opportune Backstab|auto-trad|
|[wCtIIrUW3W5fxJS5.htm](agents-of-edgewatch-bestiary-items/wCtIIrUW3W5fxJS5.htm)|Psychokinetic Trumpet|auto-trad|
|[wdrujBr7WxOu0zlx.htm](agents-of-edgewatch-bestiary-items/wdrujBr7WxOu0zlx.htm)|Swallow Whole|auto-trad|
|[WeIVzFTJRVOOXc8w.htm](agents-of-edgewatch-bestiary-items/WeIVzFTJRVOOXc8w.htm)|Magic Susceptibility|auto-trad|
|[wFFcTblSQv1w7t5j.htm](agents-of-edgewatch-bestiary-items/wFFcTblSQv1w7t5j.htm)|Lava Bomb|auto-trad|
|[wG13bULump9xDoJR.htm](agents-of-edgewatch-bestiary-items/wG13bULump9xDoJR.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Wg2JQdgjmSOEpeH4.htm](agents-of-edgewatch-bestiary-items/Wg2JQdgjmSOEpeH4.htm)|Longsword|auto-trad|
|[wGuVhWBsG7ERdKlr.htm](agents-of-edgewatch-bestiary-items/wGuVhWBsG7ERdKlr.htm)|Extending Chandeliers|auto-trad|
|[wh8aWKGdA2HjMBi7.htm](agents-of-edgewatch-bestiary-items/wh8aWKGdA2HjMBi7.htm)|Cleric Domain Spells|auto-trad|
|[wHtIgMdmPGGys5hu.htm](agents-of-edgewatch-bestiary-items/wHtIgMdmPGGys5hu.htm)|Divine Prepared Spells|auto-trad|
|[WibchVOYf3MLcdy7.htm](agents-of-edgewatch-bestiary-items/WibchVOYf3MLcdy7.htm)|Spike|auto-trad|
|[WJcrXfekpCSuZzoT.htm](agents-of-edgewatch-bestiary-items/WJcrXfekpCSuZzoT.htm)|[Two Actions] Water Jet|auto-trad|
|[wjucZTZEvK64kwlz.htm](agents-of-edgewatch-bestiary-items/wjucZTZEvK64kwlz.htm)|Exile|auto-trad|
|[WjzMU3WoQ0I6gVRQ.htm](agents-of-edgewatch-bestiary-items/WjzMU3WoQ0I6gVRQ.htm)|Bloody Spew|auto-trad|
|[wKQPyzZESm0w8vni.htm](agents-of-edgewatch-bestiary-items/wKQPyzZESm0w8vni.htm)|Protean Anatomy|auto-trad|
|[WlrYnJI19Nc7qelL.htm](agents-of-edgewatch-bestiary-items/WlrYnJI19Nc7qelL.htm)|Longsword|auto-trad|
|[WNRDgclsqI4zJ8Zn.htm](agents-of-edgewatch-bestiary-items/WNRDgclsqI4zJ8Zn.htm)|Greater Flaming Greataxe|auto-trad|
|[wPp48cMhLkEqI4UE.htm](agents-of-edgewatch-bestiary-items/wPp48cMhLkEqI4UE.htm)|Binding Blast|auto-trad|
|[wpS81V45xNq0wZpT.htm](agents-of-edgewatch-bestiary-items/wpS81V45xNq0wZpT.htm)|Venomous Spit|auto-trad|
|[WQ6JLrvLYwJnndgB.htm](agents-of-edgewatch-bestiary-items/WQ6JLrvLYwJnndgB.htm)|At-Will Spells|auto-trad|
|[WQakf5K0a5EhkRkV.htm](agents-of-edgewatch-bestiary-items/WQakf5K0a5EhkRkV.htm)|Sneak Attack|auto-trad|
|[wqR1FOPVr3IfWeO6.htm](agents-of-edgewatch-bestiary-items/wqR1FOPVr3IfWeO6.htm)|Innate Occult Spells|auto-trad|
|[WSvQFfsRQ4UpAb4L.htm](agents-of-edgewatch-bestiary-items/WSvQFfsRQ4UpAb4L.htm)|At-Will Spells|auto-trad|
|[wtewhF2XR7w6V0Wm.htm](agents-of-edgewatch-bestiary-items/wtewhF2XR7w6V0Wm.htm)|Incendiary Spit|auto-trad|
|[wTQI2GpK1QxS5tKd.htm](agents-of-edgewatch-bestiary-items/wTQI2GpK1QxS5tKd.htm)|Constant Spells|auto-trad|
|[WUdhVBp748zzwj4z.htm](agents-of-edgewatch-bestiary-items/WUdhVBp748zzwj4z.htm)|Negative Healing|auto-trad|
|[wuytt71AuRJcaryt.htm](agents-of-edgewatch-bestiary-items/wuytt71AuRJcaryt.htm)|Sidestep|auto-trad|
|[wvGApQMwNilwaRG1.htm](agents-of-edgewatch-bestiary-items/wvGApQMwNilwaRG1.htm)|Massive Scythe|auto-trad|
|[wvqzKo67ZU0rKlN6.htm](agents-of-edgewatch-bestiary-items/wvqzKo67ZU0rKlN6.htm)|Jaws|auto-trad|
|[wW8lBy7vi5CcDHTN.htm](agents-of-edgewatch-bestiary-items/wW8lBy7vi5CcDHTN.htm)|Retch of Foulness|auto-trad|
|[wWBBBQdGjY5Kvg0N.htm](agents-of-edgewatch-bestiary-items/wWBBBQdGjY5Kvg0N.htm)|Constant Spells|auto-trad|
|[WWLbZOyOAUEoJrVO.htm](agents-of-edgewatch-bestiary-items/WWLbZOyOAUEoJrVO.htm)|Fast Swallow|auto-trad|
|[WxDAGBp5686PFyEA.htm](agents-of-edgewatch-bestiary-items/WxDAGBp5686PFyEA.htm)|Claw|auto-trad|
|[x2ySAWMOcn67UNgo.htm](agents-of-edgewatch-bestiary-items/x2ySAWMOcn67UNgo.htm)|Innate Primal Spells|auto-trad|
|[X6LedzeSnW4xbem7.htm](agents-of-edgewatch-bestiary-items/X6LedzeSnW4xbem7.htm)|Sneak Attack|auto-trad|
|[X7yV3UIdROaskajA.htm](agents-of-edgewatch-bestiary-items/X7yV3UIdROaskajA.htm)|Suicidal Obedience|auto-trad|
|[x85871wfT5AGGHSb.htm](agents-of-edgewatch-bestiary-items/x85871wfT5AGGHSb.htm)|Predictive Impediment|auto-trad|
|[X8bVkdHSJwnQfIoj.htm](agents-of-edgewatch-bestiary-items/X8bVkdHSJwnQfIoj.htm)|Mirror Senses|auto-trad|
|[xaK2BOT4rtFista9.htm](agents-of-edgewatch-bestiary-items/xaK2BOT4rtFista9.htm)|Blade of the Rabbit Prince|auto-trad|
|[xB5y0S4TFwSTIZuN.htm](agents-of-edgewatch-bestiary-items/xB5y0S4TFwSTIZuN.htm)|Swallow Whole|auto-trad|
|[XbpdSS6FXHoqmiIy.htm](agents-of-edgewatch-bestiary-items/XbpdSS6FXHoqmiIy.htm)|Rend|auto-trad|
|[Xc5ErxpysKbiLVjW.htm](agents-of-edgewatch-bestiary-items/Xc5ErxpysKbiLVjW.htm)|Tentacle|auto-trad|
|[XC6rH5AiexqlIUrJ.htm](agents-of-edgewatch-bestiary-items/XC6rH5AiexqlIUrJ.htm)|Crowded Mob|auto-trad|
|[xcCXMK0pQFPWRPA9.htm](agents-of-edgewatch-bestiary-items/xcCXMK0pQFPWRPA9.htm)|Grab|auto-trad|
|[XCR8NvW2d1UwWElX.htm](agents-of-edgewatch-bestiary-items/XCR8NvW2d1UwWElX.htm)|Arm Activation|auto-trad|
|[XEmgx6gs9UBM9Fqd.htm](agents-of-edgewatch-bestiary-items/XEmgx6gs9UBM9Fqd.htm)|Storm of Claws|auto-trad|
|[xhZKk4oQRgs08i0T.htm](agents-of-edgewatch-bestiary-items/xhZKk4oQRgs08i0T.htm)|Nimble Dodge|auto-trad|
|[XjKcKhw6OKjMZYZd.htm](agents-of-edgewatch-bestiary-items/XjKcKhw6OKjMZYZd.htm)|Jaws|auto-trad|
|[xkC8ZjLdXCpQeb8J.htm](agents-of-edgewatch-bestiary-items/xkC8ZjLdXCpQeb8J.htm)|Regeneration 20 (Deactivated by Evil)|auto-trad|
|[XKe1YQHD2E6lSSDn.htm](agents-of-edgewatch-bestiary-items/XKe1YQHD2E6lSSDn.htm)|Shriek|auto-trad|
|[xm43sElSiUh9TZcn.htm](agents-of-edgewatch-bestiary-items/xm43sElSiUh9TZcn.htm)|Prepared Divine Spells|auto-trad|
|[xpaIY6o97hpi3r6c.htm](agents-of-edgewatch-bestiary-items/xpaIY6o97hpi3r6c.htm)|Club|auto-trad|
|[XpTvRTQsAv9H9zpJ.htm](agents-of-edgewatch-bestiary-items/XpTvRTQsAv9H9zpJ.htm)|Constrict|auto-trad|
|[XPZi9QdRnv0anTSq.htm](agents-of-edgewatch-bestiary-items/XPZi9QdRnv0anTSq.htm)|Greater Flaming Javelin|auto-trad|
|[XrbjsVj4ZB7Fx9Dk.htm](agents-of-edgewatch-bestiary-items/XrbjsVj4ZB7Fx9Dk.htm)|Dagger|auto-trad|
|[XrNCDIzaEOy3x2U7.htm](agents-of-edgewatch-bestiary-items/XrNCDIzaEOy3x2U7.htm)|Occult Innate Spells|auto-trad|
|[XrrgRKZ5Cs3t7Wzc.htm](agents-of-edgewatch-bestiary-items/XrrgRKZ5Cs3t7Wzc.htm)|Entropy Sense (Imprecise) 60 feet|auto-trad|
|[XsO5LFwrPWmXxkjs.htm](agents-of-edgewatch-bestiary-items/XsO5LFwrPWmXxkjs.htm)|Innate Divine Spells|auto-trad|
|[XVBhNwitUtia7zY6.htm](agents-of-edgewatch-bestiary-items/XVBhNwitUtia7zY6.htm)|+2 Circumstance Bonus on Saves vs. Poison|auto-trad|
|[xYJikLuNmwyBRVgt.htm](agents-of-edgewatch-bestiary-items/xYJikLuNmwyBRVgt.htm)|Ghostly Hand|auto-trad|
|[xzlfpLa29fzw4xH5.htm](agents-of-edgewatch-bestiary-items/xzlfpLa29fzw4xH5.htm)|Swipe|auto-trad|
|[xzuxIMk5QK7iPQ9j.htm](agents-of-edgewatch-bestiary-items/xzuxIMk5QK7iPQ9j.htm)|Divine Innate Spells|auto-trad|
|[y4FvJN07K6t62yW1.htm](agents-of-edgewatch-bestiary-items/y4FvJN07K6t62yW1.htm)|Rolling Assault|auto-trad|
|[Y7P7UpMZajGcxNz4.htm](agents-of-edgewatch-bestiary-items/Y7P7UpMZajGcxNz4.htm)|Sneak Attack|auto-trad|
|[Y83KtMbjPwV7yApV.htm](agents-of-edgewatch-bestiary-items/Y83KtMbjPwV7yApV.htm)|Light Blindness|auto-trad|
|[Y86gdLAapKCoO1Pl.htm](agents-of-edgewatch-bestiary-items/Y86gdLAapKCoO1Pl.htm)|Barrel|auto-trad|
|[y8wwK8yOkMWsAZoY.htm](agents-of-edgewatch-bestiary-items/y8wwK8yOkMWsAZoY.htm)|Tail|auto-trad|
|[Y974djSHleUUJ1cg.htm](agents-of-edgewatch-bestiary-items/Y974djSHleUUJ1cg.htm)|Claws|auto-trad|
|[yAHJOd3dkfOae1Kf.htm](agents-of-edgewatch-bestiary-items/yAHJOd3dkfOae1Kf.htm)|Magic Immunity|auto-trad|
|[yAihXkZJ3wXkWXOm.htm](agents-of-edgewatch-bestiary-items/yAihXkZJ3wXkWXOm.htm)|Jaws|auto-trad|
|[Yb2OjQReLk3HgLoc.htm](agents-of-edgewatch-bestiary-items/Yb2OjQReLk3HgLoc.htm)|Feed on Fear|auto-trad|
|[YDAmsDKjMjg1NU8x.htm](agents-of-edgewatch-bestiary-items/YDAmsDKjMjg1NU8x.htm)|Head Hunter|auto-trad|
|[YE6DApvJToive72E.htm](agents-of-edgewatch-bestiary-items/YE6DApvJToive72E.htm)|Paint the Masses|auto-trad|
|[YEhIEhTt74V6zF2F.htm](agents-of-edgewatch-bestiary-items/YEhIEhTt74V6zF2F.htm)|Pseudopod|auto-trad|
|[YH6BaTJoWcGrOFjD.htm](agents-of-edgewatch-bestiary-items/YH6BaTJoWcGrOFjD.htm)|Crossbow|auto-trad|
|[Yh9EKYQqgpz6wFJx.htm](agents-of-edgewatch-bestiary-items/Yh9EKYQqgpz6wFJx.htm)|Tail|auto-trad|
|[YhPJNRCs20xqntX6.htm](agents-of-edgewatch-bestiary-items/YhPJNRCs20xqntX6.htm)|Motion Sense 60 feet|auto-trad|
|[yIeOGWTZXqk546wz.htm](agents-of-edgewatch-bestiary-items/yIeOGWTZXqk546wz.htm)|Claw|auto-trad|
|[yJXSa8oHuL9yaPqg.htm](agents-of-edgewatch-bestiary-items/yJXSa8oHuL9yaPqg.htm)|Wary|auto-trad|
|[ykHarPFAY9t176Uv.htm](agents-of-edgewatch-bestiary-items/ykHarPFAY9t176Uv.htm)|Attack of Opportunity|auto-trad|
|[ylBiUlJT4zFUEOca.htm](agents-of-edgewatch-bestiary-items/ylBiUlJT4zFUEOca.htm)|Dagger|auto-trad|
|[YlkEautzPcw7ota2.htm](agents-of-edgewatch-bestiary-items/YlkEautzPcw7ota2.htm)|Divine Innate Spells|auto-trad|
|[YMK6yeIqUwKCrXpF.htm](agents-of-edgewatch-bestiary-items/YMK6yeIqUwKCrXpF.htm)|Composite Shortbow|auto-trad|
|[yMXbFPudLdFxCrfJ.htm](agents-of-edgewatch-bestiary-items/yMXbFPudLdFxCrfJ.htm)|Prehensile Tail|auto-trad|
|[YNkhegfdFfE7yfGn.htm](agents-of-edgewatch-bestiary-items/YNkhegfdFfE7yfGn.htm)|Formation Attack|auto-trad|
|[YoEkR6sBza4UBMYy.htm](agents-of-edgewatch-bestiary-items/YoEkR6sBza4UBMYy.htm)|Essence Drain|auto-trad|
|[yoqc9IMPx8HSR6k1.htm](agents-of-edgewatch-bestiary-items/yoqc9IMPx8HSR6k1.htm)|Grab|auto-trad|
|[yOvz4I15CCPk78ns.htm](agents-of-edgewatch-bestiary-items/yOvz4I15CCPk78ns.htm)|Smoke Vision|auto-trad|
|[yqKz1nsBwvpywBaj.htm](agents-of-edgewatch-bestiary-items/yqKz1nsBwvpywBaj.htm)|Prepared Arcane Spells|auto-trad|
|[yqYxldud8C52XbLb.htm](agents-of-edgewatch-bestiary-items/yqYxldud8C52XbLb.htm)|Darkvision|auto-trad|
|[Yra4blZoY3d1QYtS.htm](agents-of-edgewatch-bestiary-items/Yra4blZoY3d1QYtS.htm)|Constant Spells|auto-trad|
|[Ys0t5quGKBZoMSOI.htm](agents-of-edgewatch-bestiary-items/Ys0t5quGKBZoMSOI.htm)|Pseudopod|auto-trad|
|[Ys9PpeeVpBUmvYSZ.htm](agents-of-edgewatch-bestiary-items/Ys9PpeeVpBUmvYSZ.htm)|At-Will Spells|auto-trad|
|[yTV624ydUotNS7Qa.htm](agents-of-edgewatch-bestiary-items/yTV624ydUotNS7Qa.htm)|Vital Transfusion|auto-trad|
|[yut11i3oSOwRk5mZ.htm](agents-of-edgewatch-bestiary-items/yut11i3oSOwRk5mZ.htm)|Najra Lizard Venom|auto-trad|
|[yVDBxObK8ZPhiFLN.htm](agents-of-edgewatch-bestiary-items/yVDBxObK8ZPhiFLN.htm)|Attack of Opportunity|auto-trad|
|[yVT8Le2gpdDtMHay.htm](agents-of-edgewatch-bestiary-items/yVT8Le2gpdDtMHay.htm)|Shield Block|auto-trad|
|[YW2WcfCcpCi1cDl2.htm](agents-of-edgewatch-bestiary-items/YW2WcfCcpCi1cDl2.htm)|Thoughtsense (Imprecise) 120 feet|auto-trad|
|[yX6VKlUamzU5KOQr.htm](agents-of-edgewatch-bestiary-items/yX6VKlUamzU5KOQr.htm)|Bonesense (Imprecise) 30 feet|auto-trad|
|[YXf0epw5INfNsVTi.htm](agents-of-edgewatch-bestiary-items/YXf0epw5INfNsVTi.htm)|Vermicular Movement|auto-trad|
|[YY5ytAHtfhcxwxto.htm](agents-of-edgewatch-bestiary-items/YY5ytAHtfhcxwxto.htm)|Sneak Attack|auto-trad|
|[yyZMl8O8UfRO0cwF.htm](agents-of-edgewatch-bestiary-items/yyZMl8O8UfRO0cwF.htm)|Tremorsense (Imprecise) 120 feet|auto-trad|
|[yzD6DHeUdHo1tXNp.htm](agents-of-edgewatch-bestiary-items/yzD6DHeUdHo1tXNp.htm)|Summon Steed|auto-trad|
|[YzsKsO21XfN1KAry.htm](agents-of-edgewatch-bestiary-items/YzsKsO21XfN1KAry.htm)|Dread Gaze|auto-trad|
|[yzzm5eV3r1RGpar6.htm](agents-of-edgewatch-bestiary-items/yzzm5eV3r1RGpar6.htm)|+1 Status Bonus on Saves vs. Incapacitation Effects|auto-trad|
|[Z0dIBhDdvys895pC.htm](agents-of-edgewatch-bestiary-items/Z0dIBhDdvys895pC.htm)|Rituals|auto-trad|
|[z53waMQlGY7ahvCA.htm](agents-of-edgewatch-bestiary-items/z53waMQlGY7ahvCA.htm)|Hook and Flay|auto-trad|
|[Z5GiE6QWjQtsaPbJ.htm](agents-of-edgewatch-bestiary-items/Z5GiE6QWjQtsaPbJ.htm)|Capture Subject|auto-trad|
|[z7mNGwtluLbrFYpO.htm](agents-of-edgewatch-bestiary-items/z7mNGwtluLbrFYpO.htm)|Sling|auto-trad|
|[ZaivUgHp2bK6CmBQ.htm](agents-of-edgewatch-bestiary-items/ZaivUgHp2bK6CmBQ.htm)|Protean Anatomy 10|auto-trad|
|[zaQRIy856WuUiHBw.htm](agents-of-edgewatch-bestiary-items/zaQRIy856WuUiHBw.htm)|Overdrive Engine|auto-trad|
|[zBa632069HwaM4cT.htm](agents-of-edgewatch-bestiary-items/zBa632069HwaM4cT.htm)|Dagger|auto-trad|
|[ZBfbG1IkIDHWmhM2.htm](agents-of-edgewatch-bestiary-items/ZBfbG1IkIDHWmhM2.htm)|Wall Run|auto-trad|
|[ZbNsjTdTl9JRvGxk.htm](agents-of-edgewatch-bestiary-items/ZbNsjTdTl9JRvGxk.htm)|Bullyrag Beatdown|auto-trad|
|[Zc0ocF6ln2ziU1iE.htm](agents-of-edgewatch-bestiary-items/Zc0ocF6ln2ziU1iE.htm)|Agonizing Wail|auto-trad|
|[ZCDanTgpw20dpNUH.htm](agents-of-edgewatch-bestiary-items/ZCDanTgpw20dpNUH.htm)|Regeneration 30 (Deactivated by Lawful)|auto-trad|
|[zchaebxlIBynZinm.htm](agents-of-edgewatch-bestiary-items/zchaebxlIBynZinm.htm)|Toxic Fumes|auto-trad|
|[zDBvkCFJMYUXSbxN.htm](agents-of-edgewatch-bestiary-items/zDBvkCFJMYUXSbxN.htm)|Arcane Prepared Spells|auto-trad|
|[zdKpdDk0vI7szcPH.htm](agents-of-edgewatch-bestiary-items/zdKpdDk0vI7szcPH.htm)|Dagger|auto-trad|
|[zdtAExquN9fICX40.htm](agents-of-edgewatch-bestiary-items/zdtAExquN9fICX40.htm)|Telepathy 100 feet|auto-trad|
|[Ze4gxM4HMIEzvJgg.htm](agents-of-edgewatch-bestiary-items/Ze4gxM4HMIEzvJgg.htm)|Main-Gauche|auto-trad|
|[ZEbYuhDFGc3fADE1.htm](agents-of-edgewatch-bestiary-items/ZEbYuhDFGc3fADE1.htm)|Sneak Attack|auto-trad|
|[zeH3OrBLioIRoYb5.htm](agents-of-edgewatch-bestiary-items/zeH3OrBLioIRoYb5.htm)|Deny Advantage|auto-trad|
|[zehVjK1f2C0jpnAB.htm](agents-of-edgewatch-bestiary-items/zehVjK1f2C0jpnAB.htm)|Innate Occult Spells|auto-trad|
|[ZFx4kB8JAYfSX4xr.htm](agents-of-edgewatch-bestiary-items/ZFx4kB8JAYfSX4xr.htm)|Extend Legs|auto-trad|
|[zghmY4aHHsaTIPLa.htm](agents-of-edgewatch-bestiary-items/zghmY4aHHsaTIPLa.htm)|Quick Alchemy|auto-trad|
|[zGvdZdakNLZQ0BMF.htm](agents-of-edgewatch-bestiary-items/zGvdZdakNLZQ0BMF.htm)|Greater Darkvision|auto-trad|
|[ZHs955g8FhuO2EvW.htm](agents-of-edgewatch-bestiary-items/ZHs955g8FhuO2EvW.htm)|Shield Block|auto-trad|
|[Zi59HbzpU7nXEarp.htm](agents-of-edgewatch-bestiary-items/Zi59HbzpU7nXEarp.htm)|Fast Healing 20|auto-trad|
|[zJ4cmL8xK1QGb5IF.htm](agents-of-edgewatch-bestiary-items/zJ4cmL8xK1QGb5IF.htm)|Attack of Opportunity|auto-trad|
|[ZjdHv1h2KMIjC0bA.htm](agents-of-edgewatch-bestiary-items/ZjdHv1h2KMIjC0bA.htm)|Prepared Arcane Spells|auto-trad|
|[ZJvsVjbPWteUphTK.htm](agents-of-edgewatch-bestiary-items/ZJvsVjbPWteUphTK.htm)|Concussive Beatdown|auto-trad|
|[ZkE9BJn0zrQtzRMW.htm](agents-of-edgewatch-bestiary-items/ZkE9BJn0zrQtzRMW.htm)|Writhing Arms|auto-trad|
|[zMxrSkK2U9CDEp0u.htm](agents-of-edgewatch-bestiary-items/zMxrSkK2U9CDEp0u.htm)|Bone Drink|auto-trad|
|[zn2a2GvxqbknqXbo.htm](agents-of-edgewatch-bestiary-items/zn2a2GvxqbknqXbo.htm)|Entropy Sense (Imprecise) 60 feet|auto-trad|
|[ZNWERIfXN1fj8u4P.htm](agents-of-edgewatch-bestiary-items/ZNWERIfXN1fj8u4P.htm)|Chain Expert|auto-trad|
|[ZoDu12928nPzUoAD.htm](agents-of-edgewatch-bestiary-items/ZoDu12928nPzUoAD.htm)|Zealous Restoration|auto-trad|
|[zqI0OQ5w5KAQApsl.htm](agents-of-edgewatch-bestiary-items/zqI0OQ5w5KAQApsl.htm)|Shortsword|auto-trad|
|[ZtFghl2g5noJE7Cw.htm](agents-of-edgewatch-bestiary-items/ZtFghl2g5noJE7Cw.htm)|Darkvision|auto-trad|
|[ztgpLrKdJjctex5J.htm](agents-of-edgewatch-bestiary-items/ztgpLrKdJjctex5J.htm)|Pollution Infusion|auto-trad|
|[zvkxJ9ZreNjX3yJL.htm](agents-of-edgewatch-bestiary-items/zvkxJ9ZreNjX3yJL.htm)|Flurry of Pods|auto-trad|
|[ZX0T6lbLJyKn8KuN.htm](agents-of-edgewatch-bestiary-items/ZX0T6lbLJyKn8KuN.htm)|Unbalancing Blow|auto-trad|
|[ZX4MxqemrRYlaJEf.htm](agents-of-edgewatch-bestiary-items/ZX4MxqemrRYlaJEf.htm)|Greater Darkvision|auto-trad|
|[zX6RvKVyn16ug6vJ.htm](agents-of-edgewatch-bestiary-items/zX6RvKVyn16ug6vJ.htm)|Change Shape|auto-trad|
|[zxw1AsQiZy4HbIBa.htm](agents-of-edgewatch-bestiary-items/zxw1AsQiZy4HbIBa.htm)|Motion Sense 100 feet|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[157GGkuQcvXmLwPu.htm](agents-of-edgewatch-bestiary-items/157GGkuQcvXmLwPu.htm)|Hammering Flurry|Ráfaga de martillazos|modificada|
|[7nwDQbdYpJ5U6EZs.htm](agents-of-edgewatch-bestiary-items/7nwDQbdYpJ5U6EZs.htm)|Call Toxins|Llamada Toxinas|modificada|
|[8qsC3gMbkSTTQOdY.htm](agents-of-edgewatch-bestiary-items/8qsC3gMbkSTTQOdY.htm)|Impale|Empalar|modificada|
|[9KEf9z8ZVsw7qXJg.htm](agents-of-edgewatch-bestiary-items/9KEf9z8ZVsw7qXJg.htm)|Bloody Jab|Bloody Jab|modificada|
|[aV20X0Mp1CqH1tqz.htm](agents-of-edgewatch-bestiary-items/aV20X0Mp1CqH1tqz.htm)|Fan of Daggers|Abanico de Dagas|modificada|
|[bA6R5JBcAw7StwCO.htm](agents-of-edgewatch-bestiary-items/bA6R5JBcAw7StwCO.htm)|Cane of the Maelstrom|Bastón De La Vorágine|modificada|
|[BofxZCq1wekuufql.htm](agents-of-edgewatch-bestiary-items/BofxZCq1wekuufql.htm)|Joro Spider Venom|Joro Spider Venom|modificada|
|[BSAdAEHe8VdvYOjg.htm](agents-of-edgewatch-bestiary-items/BSAdAEHe8VdvYOjg.htm)|Bottled Lightning|Rayo Embotellado|modificada|
|[Bx3FBCXmpwC6wpPZ.htm](agents-of-edgewatch-bestiary-items/Bx3FBCXmpwC6wpPZ.htm)|Fist|Puño|modificada|
|[cerEbvB6QEc8UcuU.htm](agents-of-edgewatch-bestiary-items/cerEbvB6QEc8UcuU.htm)|No Escape|Sin huida posible|modificada|
|[ckqlqEZcPT6ZKZTJ.htm](agents-of-edgewatch-bestiary-items/ckqlqEZcPT6ZKZTJ.htm)|Club|Club|modificada|
|[CUtiSdjK9OwxIfKR.htm](agents-of-edgewatch-bestiary-items/CUtiSdjK9OwxIfKR.htm)|Shatter Defenses|Destrozar defensas|modificada|
|[cW925U5mT8WZIcql.htm](agents-of-edgewatch-bestiary-items/cW925U5mT8WZIcql.htm)|Shortbow|Arco corto|modificada|
|[E0iLnf03NhzlNZTT.htm](agents-of-edgewatch-bestiary-items/E0iLnf03NhzlNZTT.htm)|Moderate Alchemist's Fire|Fuego de alquimista moderado|modificada|
|[HjL44xV6BvZNBNH8.htm](agents-of-edgewatch-bestiary-items/HjL44xV6BvZNBNH8.htm)|Frost Vial|Gélida de frasco de escarcha|modificada|
|[jC7yNSXPp6Y4dqOF.htm](agents-of-edgewatch-bestiary-items/jC7yNSXPp6Y4dqOF.htm)|Nightmare Assault|Asalto de pesadilla|modificada|
|[lmVllcsiFHe8ybRy.htm](agents-of-edgewatch-bestiary-items/lmVllcsiFHe8ybRy.htm)|Wretched Weeps|Wretched Weeps|modificada|
|[lZznKQkFobPXBxDT.htm](agents-of-edgewatch-bestiary-items/lZznKQkFobPXBxDT.htm)|Warpwave Strike|Golpe de Onda Warp|modificada|
|[M9ciwVyPg7uZSM44.htm](agents-of-edgewatch-bestiary-items/M9ciwVyPg7uZSM44.htm)|Web|Telara|modificada|
|[mIVZhiAlmqc8WWY7.htm](agents-of-edgewatch-bestiary-items/mIVZhiAlmqc8WWY7.htm)|Flay|Flay|modificada|
|[NRUwbsWBSVtCGgrp.htm](agents-of-edgewatch-bestiary-items/NRUwbsWBSVtCGgrp.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[NvOUKM0pOaTFyrWw.htm](agents-of-edgewatch-bestiary-items/NvOUKM0pOaTFyrWw.htm)|Close Cutter|Close Cutter|modificada|
|[OMRu84X8qswYsTyJ.htm](agents-of-edgewatch-bestiary-items/OMRu84X8qswYsTyJ.htm)|Fangs|Colmillos|modificada|
|[P2D3nIr4b8QtzEFx.htm](agents-of-edgewatch-bestiary-items/P2D3nIr4b8QtzEFx.htm)|Overflowing Boiling Water|Desbordamiento de agua hirviendo|modificada|
|[p777SbqGil3xrtzd.htm](agents-of-edgewatch-bestiary-items/p777SbqGil3xrtzd.htm)|Flay|Flay|modificada|
|[pxtdz9Cb1KUHXsrM.htm](agents-of-edgewatch-bestiary-items/pxtdz9Cb1KUHXsrM.htm)|Corpse Wallow|Corpse Wallow|modificada|
|[qi5ozQVZsQZavdpC.htm](agents-of-edgewatch-bestiary-items/qi5ozQVZsQZavdpC.htm)|Moderate Acid Flask|Frasco de ácido moderado|modificada|
|[SGuyP6Rc9SZRoRYv.htm](agents-of-edgewatch-bestiary-items/SGuyP6Rc9SZRoRYv.htm)|Lesser Alchemist's Fire|Fuego de alquimista menor|modificada|
|[SLWi3J2O0e3QXfYB.htm](agents-of-edgewatch-bestiary-items/SLWi3J2O0e3QXfYB.htm)|Clobber|Clobber|modificada|
|[vZXFIePI3zP2Og1M.htm](agents-of-edgewatch-bestiary-items/vZXFIePI3zP2Og1M.htm)|Web Trap|Trampa telara|modificada|
|[Y7LWmp8wJgF4O24C.htm](agents-of-edgewatch-bestiary-items/Y7LWmp8wJgF4O24C.htm)|Slam Shut|Slam Shut|modificada|
|[yhVKDXeyQEVchPJW.htm](agents-of-edgewatch-bestiary-items/yhVKDXeyQEVchPJW.htm)|Dismember|Desmembrar|modificada|
|[ysA7ztYuAl3O3J7A.htm](agents-of-edgewatch-bestiary-items/ysA7ztYuAl3O3J7A.htm)|Nightmare Cudgel|Garrote de pesadilla|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
