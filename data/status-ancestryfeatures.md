# Estado de la traducción (ancestryfeatures)

 * **auto-trad**: 38
 * **modificada**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[feat-00-8sxtjVsk9HBY5yAv.htm](ancestryfeatures/feat-00-8sxtjVsk9HBY5yAv.htm)|Glowing Horn|auto-trad|
|[feat-00-AUlPRySCqE6o6LHH.htm](ancestryfeatures/feat-00-AUlPRySCqE6o6LHH.htm)|Eyes in Back|auto-trad|
|[feat-00-AzGJN1wwLFaLJIeo.htm](ancestryfeatures/feat-00-AzGJN1wwLFaLJIeo.htm)|Aquatic Adaptation|auto-trad|
|[feat-00-BHPDeqQHqi7ukCUW.htm](ancestryfeatures/feat-00-BHPDeqQHqi7ukCUW.htm)|Constructed (Poppet)|auto-trad|
|[feat-00-bHUrm79wJCcI7L3A.htm](ancestryfeatures/feat-00-bHUrm79wJCcI7L3A.htm)|Sunlight|auto-trad|
|[feat-00-dCp517IUFJk8JvQc.htm](ancestryfeatures/feat-00-dCp517IUFJk8JvQc.htm)|Fangs|auto-trad|
|[feat-00-dkZ8RxdQFJrdxwQo.htm](ancestryfeatures/feat-00-dkZ8RxdQFJrdxwQo.htm)|Revulsion|auto-trad|
|[feat-00-DRtaqOHXTRtGRIUT.htm](ancestryfeatures/feat-00-DRtaqOHXTRtGRIUT.htm)|Low-Light Vision|auto-trad|
|[feat-00-dtNsRAhCRfteA1ev.htm](ancestryfeatures/feat-00-dtNsRAhCRfteA1ev.htm)|Blunt Snout|auto-trad|
|[feat-00-E28a45fUC2OkXZXY.htm](ancestryfeatures/feat-00-E28a45fUC2OkXZXY.htm)|Constructed Body|auto-trad|
|[feat-00-egpiSWBrNBb1Fmig.htm](ancestryfeatures/feat-00-egpiSWBrNBb1Fmig.htm)|Draconic Exemplar|auto-trad|
|[feat-00-Eyuqu6eIaoGCjnMv.htm](ancestryfeatures/feat-00-Eyuqu6eIaoGCjnMv.htm)|Clan Dagger|auto-trad|
|[feat-00-HHVQDp61ehcpdiU8.htm](ancestryfeatures/feat-00-HHVQDp61ehcpdiU8.htm)|Darkvision|auto-trad|
|[feat-00-IXyXCMBldrU5G60e.htm](ancestryfeatures/feat-00-IXyXCMBldrU5G60e.htm)|Innate Venom|auto-trad|
|[feat-00-jatezR4bENwhC6GL.htm](ancestryfeatures/feat-00-jatezR4bENwhC6GL.htm)|Bite|auto-trad|
|[feat-00-jS8GamSTl8yMLL4F.htm](ancestryfeatures/feat-00-jS8GamSTl8yMLL4F.htm)|Fangs (Nagaji)|auto-trad|
|[feat-00-kMgyOI4kBIEtFvhb.htm](ancestryfeatures/feat-00-kMgyOI4kBIEtFvhb.htm)|Swim|auto-trad|
|[feat-00-mEDTJi7d1bTEiwUD.htm](ancestryfeatures/feat-00-mEDTJi7d1bTEiwUD.htm)|Unusual Anatomy|auto-trad|
|[feat-00-mnhmhOKWLiOD0lev.htm](ancestryfeatures/feat-00-mnhmhOKWLiOD0lev.htm)|Constructed|auto-trad|
|[feat-00-N0zhJ0whkDJPlftl.htm](ancestryfeatures/feat-00-N0zhJ0whkDJPlftl.htm)|Photosynthesis|auto-trad|
|[feat-00-NfkxFWUeG6g41e8w.htm](ancestryfeatures/feat-00-NfkxFWUeG6g41e8w.htm)|Claws|auto-trad|
|[feat-00-oCIO7UJqbpTkI62l.htm](ancestryfeatures/feat-00-oCIO7UJqbpTkI62l.htm)|Wings|auto-trad|
|[feat-00-PtvzTm2gjdCKao4I.htm](ancestryfeatures/feat-00-PtvzTm2gjdCKao4I.htm)|Prehensile Tail|auto-trad|
|[feat-00-qJD3PJdoSXFrZEwr.htm](ancestryfeatures/feat-00-qJD3PJdoSXFrZEwr.htm)|Sharp Beak|auto-trad|
|[feat-00-qKh6MxgE0cwde6mC.htm](ancestryfeatures/feat-00-qKh6MxgE0cwde6mC.htm)|Flammable|auto-trad|
|[feat-00-QyBfftocP1i43Qrp.htm](ancestryfeatures/feat-00-QyBfftocP1i43Qrp.htm)|Empathic Sense|auto-trad|
|[feat-00-R6rcqRsBR0KIho5n.htm](ancestryfeatures/feat-00-R6rcqRsBR0KIho5n.htm)|Change Shape (Anadi)|auto-trad|
|[feat-00-RYrY7o0i6s7KW9io.htm](ancestryfeatures/feat-00-RYrY7o0i6s7KW9io.htm)|Automaton Core|auto-trad|
|[feat-00-SAbzItAI4uwbdnQk.htm](ancestryfeatures/feat-00-SAbzItAI4uwbdnQk.htm)|Basic Undead Benefits|auto-trad|
|[feat-00-sL1hHxrHdMNIZVAd.htm](ancestryfeatures/feat-00-sL1hHxrHdMNIZVAd.htm)|Land on Your Feet|auto-trad|
|[feat-00-Sm3tKetM6kddTio3.htm](ancestryfeatures/feat-00-Sm3tKetM6kddTio3.htm)|Plant Nourishment|auto-trad|
|[feat-00-TRw4oBZBFZG96jKO.htm](ancestryfeatures/feat-00-TRw4oBZBFZG96jKO.htm)|Magical Strikes|auto-trad|
|[feat-00-uSAYmU7PO2QoOWhB.htm](ancestryfeatures/feat-00-uSAYmU7PO2QoOWhB.htm)|Emotionally Unaware|auto-trad|
|[feat-00-vPhPgzpRjYDMT9Kq.htm](ancestryfeatures/feat-00-vPhPgzpRjYDMT9Kq.htm)|Greater Darkvision|auto-trad|
|[feat-00-vt67b8uoNEbskcBv.htm](ancestryfeatures/feat-00-vt67b8uoNEbskcBv.htm)|Hydration|auto-trad|
|[feat-00-y1EmCv2cEb5hXBwx.htm](ancestryfeatures/feat-00-y1EmCv2cEb5hXBwx.htm)|Keen Eyes|auto-trad|
|[feat-00-YGk41WV42aTM7CQV.htm](ancestryfeatures/feat-00-YGk41WV42aTM7CQV.htm)|Advanced Undead Benefits|auto-trad|
|[feat-00-Ymg6WqeJqOyLJLEr.htm](ancestryfeatures/feat-00-Ymg6WqeJqOyLJLEr.htm)|Change Shape (Kitsune)|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[feat-00-BgHrucbZ9TH92RDv.htm](ancestryfeatures/feat-00-BgHrucbZ9TH92RDv.htm)|Sunlight Healing|Curación por luz solar|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
