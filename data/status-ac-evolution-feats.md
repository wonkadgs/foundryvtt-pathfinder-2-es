# Estado de la traducción (ac-evolution-feats)

 * **auto-trad**: 34


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[00eOhkScQi1K5GZk.htm](ac-evolution-feats/00eOhkScQi1K5GZk.htm)|Resilient Shell|auto-trad|
|[4iJgmh1XmMGdzSF8.htm](ac-evolution-feats/4iJgmh1XmMGdzSF8.htm)|Vibration Sense|auto-trad|
|[4mkzupImKqVs2JkW.htm](ac-evolution-feats/4mkzupImKqVs2JkW.htm)|Advanced Weaponry|auto-trad|
|[Bf1jBWTdKVppsLBC.htm](ac-evolution-feats/Bf1jBWTdKVppsLBC.htm)|Steed Form|auto-trad|
|[BRad9X7kjcTcj1mf.htm](ac-evolution-feats/BRad9X7kjcTcj1mf.htm)|Summoner Dedication Adjustment|auto-trad|
|[CaZ36nx2kGFRr0ER.htm](ac-evolution-feats/CaZ36nx2kGFRr0ER.htm)|Burrowing Form|auto-trad|
|[DzQ1melMWQPbbcyu.htm](ac-evolution-feats/DzQ1melMWQPbbcyu.htm)|Eidolon's Opportunity|auto-trad|
|[eF3CKuQwqXJXIQPL.htm](ac-evolution-feats/eF3CKuQwqXJXIQPL.htm)|Ever-Vigilant Senses|auto-trad|
|[eh6Kd7ozEyeRgd3l.htm](ac-evolution-feats/eh6Kd7ozEyeRgd3l.htm)|Ranged Combatant|auto-trad|
|[en0ikSriHVja8kGM.htm](ac-evolution-feats/en0ikSriHVja8kGM.htm)|Magical Adept|auto-trad|
|[ezmJ2kq6N647wd13.htm](ac-evolution-feats/ezmJ2kq6N647wd13.htm)|Glider Form|auto-trad|
|[iYt3zEgqDnVhnHVf.htm](ac-evolution-feats/iYt3zEgqDnVhnHVf.htm)|Blood Frenzy|auto-trad|
|[Ln8r5xj2kAs81uu1.htm](ac-evolution-feats/Ln8r5xj2kAs81uu1.htm)|Defend Summoner|auto-trad|
|[Ls6CaGfZZAlmY8F9.htm](ac-evolution-feats/Ls6CaGfZZAlmY8F9.htm)|Hulking Size|auto-trad|
|[m50Xh9RRYj2NxnX2.htm](ac-evolution-feats/m50Xh9RRYj2NxnX2.htm)|Phase Out|auto-trad|
|[mz0Y50ZaV7hysmmJ.htm](ac-evolution-feats/mz0Y50ZaV7hysmmJ.htm)|Constricting Hold|auto-trad|
|[OGTIinVaETSz7kdW.htm](ac-evolution-feats/OGTIinVaETSz7kdW.htm)|Eidolon's Wrath|auto-trad|
|[OqWpydcRNgVlmm2k.htm](ac-evolution-feats/OqWpydcRNgVlmm2k.htm)|Bloodletting Claws|auto-trad|
|[prfy7faUtWFNeIXH.htm](ac-evolution-feats/prfy7faUtWFNeIXH.htm)|Dual Energy Heart|auto-trad|
|[RBvfnbDcvNDqAMgP.htm](ac-evolution-feats/RBvfnbDcvNDqAMgP.htm)|Alacritous Action|auto-trad|
|[Rd1ESvt7UHwFUD6r.htm](ac-evolution-feats/Rd1ESvt7UHwFUD6r.htm)|Expanded Senses|auto-trad|
|[rhWFwue5eYVr5Omr.htm](ac-evolution-feats/rhWFwue5eYVr5Omr.htm)|Shrink Down|auto-trad|
|[Ryed1oD5pMqREk0j.htm](ac-evolution-feats/Ryed1oD5pMqREk0j.htm)|Amphibious Form|auto-trad|
|[sbzX5d0kDO9IXinm.htm](ac-evolution-feats/sbzX5d0kDO9IXinm.htm)|Grasping Limbs|auto-trad|
|[TtoKz5zWdEtjydlk.htm](ac-evolution-feats/TtoKz5zWdEtjydlk.htm)|Weighty Impact|auto-trad|
|[V4rqSvDeIlZCdbVQ.htm](ac-evolution-feats/V4rqSvDeIlZCdbVQ.htm)|Magical Understudy|auto-trad|
|[val0CdVJxuHkpgFG.htm](ac-evolution-feats/val0CdVJxuHkpgFG.htm)|Energy Heart|auto-trad|
|[VUaI1q3MqoJqo5o5.htm](ac-evolution-feats/VUaI1q3MqoJqo5o5.htm)|Towering Size|auto-trad|
|[VzVKCWACGqnKaVbv.htm](ac-evolution-feats/VzVKCWACGqnKaVbv.htm)|Trample|auto-trad|
|[wifAadJvu5dKvMVm.htm](ac-evolution-feats/wifAadJvu5dKvMVm.htm)|Spell-Repelling Form|auto-trad|
|[wOtKDptwbz9hU9ZR.htm](ac-evolution-feats/wOtKDptwbz9hU9ZR.htm)|Energy Resistance|auto-trad|
|[X4XOW6G0Z22QniKa.htm](ac-evolution-feats/X4XOW6G0Z22QniKa.htm)|Pushing Attack|auto-trad|
|[yCSsQaTFINIbr9Gh.htm](ac-evolution-feats/yCSsQaTFINIbr9Gh.htm)|Magical Master|auto-trad|
|[Zn7DZ9xfADjtOwst.htm](ac-evolution-feats/Zn7DZ9xfADjtOwst.htm)|Miniaturize|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
