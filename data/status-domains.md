# Estado de la traducción (domains)

 * **auto-trad**: 53
 * **modificada**: 5


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0cbxczrql4MwAHwV.htm](domains/0cbxczrql4MwAHwV.htm)|Glyph Domain|auto-trad|
|[1BU8deh48XZFclWl.htm](domains/1BU8deh48XZFclWl.htm)|Healing Domain|auto-trad|
|[1NHV4ujqoR2JVWpY.htm](domains/1NHV4ujqoR2JVWpY.htm)|Travel Domain|auto-trad|
|[41qaGiMit8nDP4xv.htm](domains/41qaGiMit8nDP4xv.htm)|Abomination Domain|auto-trad|
|[6klWznsb0f2bNg3T.htm](domains/6klWznsb0f2bNg3T.htm)|Void Domain|auto-trad|
|[8ITGLquhimrr9CNv.htm](domains/8ITGLquhimrr9CNv.htm)|Indulgence Domain|auto-trad|
|[8pPvbTMZLIsvCwQk.htm](domains/8pPvbTMZLIsvCwQk.htm)|Darkness Domain|auto-trad|
|[9blxcDLIRPWenK5f.htm](domains/9blxcDLIRPWenK5f.htm)|Plague Domain|auto-trad|
|[9tsJg13xeJLGzzGV.htm](domains/9tsJg13xeJLGzzGV.htm)|Undeath Domain|auto-trad|
|[a0fe0kFowMMwUFZa.htm](domains/a0fe0kFowMMwUFZa.htm)|Nightmares Domain|auto-trad|
|[AaY3BmDItGry4oac.htm](domains/AaY3BmDItGry4oac.htm)|Decay Domain|auto-trad|
|[B40VxP6oZ0mIR4PS.htm](domains/B40VxP6oZ0mIR4PS.htm)|Fate Domain|auto-trad|
|[BlovCvjhk4Ag07w2.htm](domains/BlovCvjhk4Ag07w2.htm)|Dreams Domain|auto-trad|
|[c9odhpRoKId5dXmn.htm](domains/c9odhpRoKId5dXmn.htm)|Perfection Domain|auto-trad|
|[dnljU1twPjH4KFgO.htm](domains/dnljU1twPjH4KFgO.htm)|Swarm Domain|auto-trad|
|[fqr2OnTww3bAq0ae.htm](domains/fqr2OnTww3bAq0ae.htm)|Sun Domain|auto-trad|
|[fVfFKKvGocG2JM5q.htm](domains/fVfFKKvGocG2JM5q.htm)|Toil Domain|auto-trad|
|[giUsAWI9NbpdeUzl.htm](domains/giUsAWI9NbpdeUzl.htm)|Knowledge Domain|auto-trad|
|[HpZ4NQIqBRcFihyE.htm](domains/HpZ4NQIqBRcFihyE.htm)|Wealth Domain|auto-trad|
|[HYe7Yv1fYUANVVI3.htm](domains/HYe7Yv1fYUANVVI3.htm)|Death Domain|auto-trad|
|[i4UU3qCjIMwejIQF.htm](domains/i4UU3qCjIMwejIQF.htm)|Delirium Domain|auto-trad|
|[J7K7kHoIE69558Su.htm](domains/J7K7kHoIE69558Su.htm)|Freedom Domain|auto-trad|
|[jWmGQxJvKh5y5zfB.htm](domains/jWmGQxJvKh5y5zfB.htm)|Protection Domain|auto-trad|
|[KXFxeEyD6MmJ3a6V.htm](domains/KXFxeEyD6MmJ3a6V.htm)|Naga Domain|auto-trad|
|[KzuJAIWdjwoPjHkc.htm](domains/KzuJAIWdjwoPjHkc.htm)|Secrecy Domain|auto-trad|
|[l2EFJssJKu7rG77m.htm](domains/l2EFJssJKu7rG77m.htm)|Luck Domain|auto-trad|
|[M7koZH0zimcMgRDb.htm](domains/M7koZH0zimcMgRDb.htm)|Destruction Domain|auto-trad|
|[MktBsoHR9HsKrbbr.htm](domains/MktBsoHR9HsKrbbr.htm)|Zeal Domain|auto-trad|
|[NA4v0iwIPgkde8DP.htm](domains/NA4v0iwIPgkde8DP.htm)|Ambition Domain|auto-trad|
|[O1qeC0mIufSf3wv5.htm](domains/O1qeC0mIufSf3wv5.htm)|Passion Domain|auto-trad|
|[OsM8NfP408uB6yTi.htm](domains/OsM8NfP408uB6yTi.htm)|Wyrmkin Domain|auto-trad|
|[p5Q5RGl1lKgs5DZZ.htm](domains/p5Q5RGl1lKgs5DZZ.htm)|Soul Domain|auto-trad|
|[PrFvU65ewfst69Mp.htm](domains/PrFvU65ewfst69Mp.htm)|Water Domain|auto-trad|
|[rIDXRIdb9m2E3qC6.htm](domains/rIDXRIdb9m2E3qC6.htm)|Earth Domain|auto-trad|
|[rIZ7OoG8c4Cct42M.htm](domains/rIZ7OoG8c4Cct42M.htm)|Vigil Domain|auto-trad|
|[TpFgfwcWrfT8zVMP.htm](domains/TpFgfwcWrfT8zVMP.htm)|Pain Domain|auto-trad|
|[udASTZy5jJWFCt5w.htm](domains/udASTZy5jJWFCt5w.htm)|Time Domain|auto-trad|
|[unN0otycQZanf3va.htm](domains/unN0otycQZanf3va.htm)|Duty Domain|auto-trad|
|[uy8GUGIOmEUNqIhH.htm](domains/uy8GUGIOmEUNqIhH.htm)|Trickery Domain|auto-trad|
|[v4SDXgCuPdZqhMeL.htm](domains/v4SDXgCuPdZqhMeL.htm)|Fire Domain|auto-trad|
|[wCPGej4ZwdKCNtym.htm](domains/wCPGej4ZwdKCNtym.htm)|Change Domain|auto-trad|
|[wPtGuF1bh4wvKE6Q.htm](domains/wPtGuF1bh4wvKE6Q.htm)|Confidence Domain|auto-trad|
|[WrmaTmOHojfhiENF.htm](domains/WrmaTmOHojfhiENF.htm)|Truth Domain|auto-trad|
|[X7MkBRJGUIp91k6f.htm](domains/X7MkBRJGUIp91k6f.htm)|Sorrow Domain|auto-trad|
|[Xs6XznYHOZyQ0hJl.htm](domains/Xs6XznYHOZyQ0hJl.htm)|Tyranny Domain|auto-trad|
|[xYx8UD0JnFyBHGhJ.htm](domains/xYx8UD0JnFyBHGhJ.htm)|Nature Domain|auto-trad|
|[Y2kOBQydrsSqGCyn.htm](domains/Y2kOBQydrsSqGCyn.htm)|Magic Domain|auto-trad|
|[y3TTKFLPbP09HZUW.htm](domains/y3TTKFLPbP09HZUW.htm)|Cold Domain|auto-trad|
|[YQ6IT8DgEpqvOREx.htm](domains/YQ6IT8DgEpqvOREx.htm)|Might Domain|auto-trad|
|[ywn4ODaUt382Z3Nz.htm](domains/ywn4ODaUt382Z3Nz.htm)|Lightning Domain|auto-trad|
|[ZAx1RUB376BjNdlF.htm](domains/ZAx1RUB376BjNdlF.htm)|Repose Domain|auto-trad|
|[Ze2hoTyOQHbaQ6jD.htm](domains/Ze2hoTyOQHbaQ6jD.htm)|Air Domain|auto-trad|
|[zec5N7EnDJANGHmy.htm](domains/zec5N7EnDJANGHmy.htm)|Dust Domain|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[1aoUqGYDrdpnPWio.htm](domains/1aoUqGYDrdpnPWio.htm)|Family Domain|Dominio familiar|modificada|
|[FJ9D4qpeRhJvjHai.htm](domains/FJ9D4qpeRhJvjHai.htm)|Star Domain|Dominio Estrella|modificada|
|[mBvjWSvg7UYdS9TL.htm](domains/mBvjWSvg7UYdS9TL.htm)|Moon Domain|Dominio de la Luna|modificada|
|[MRHDhBQvgJhDZ1zq.htm](domains/MRHDhBQvgJhDZ1zq.htm)|Cities Domain|Dominio de las ciudades|modificada|
|[ZyFTUCbA0zYrzynD.htm](domains/ZyFTUCbA0zYrzynD.htm)|Creation Domain|Dominio de la creación|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
