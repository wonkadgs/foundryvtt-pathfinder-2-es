# Estado de la traducción (ac-construct-companions)

 * **auto-trad**: 5
 * **modificada**: 3


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[4kKU6MVBzwBmaA37.htm](ac-construct-companions/4kKU6MVBzwBmaA37.htm)|Advanced Construct (Small)|auto-trad|
|[i4Fz5yg9JufpyVns.htm](ac-construct-companions/i4Fz5yg9JufpyVns.htm)|Unarmed Strike 1|auto-trad|
|[JG6DlMcAdHpL2Hr7.htm](ac-construct-companions/JG6DlMcAdHpL2Hr7.htm)|Prototype Construct|auto-trad|
|[khT8AUf22ezPV0ji.htm](ac-construct-companions/khT8AUf22ezPV0ji.htm)|Advanced Construct (Large)|auto-trad|
|[mjDtydpNNfI9SuFq.htm](ac-construct-companions/mjDtydpNNfI9SuFq.htm)|Unarmed Strike 2|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[5v2Kj6Jh2YYGaSQq.htm](ac-construct-companions/5v2Kj6Jh2YYGaSQq.htm)|Advanced Construct|Advanced Construct (Medium)|modificada|
|[B3XfI8hQKdr7WG4R.htm](ac-construct-companions/B3XfI8hQKdr7WG4R.htm)|Paragon Construct|Paragon Construct|modificada|
|[f9ydomcD8DnSEXYV.htm](ac-construct-companions/f9ydomcD8DnSEXYV.htm)|Incredible Construct|Constructo Sensacional|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
