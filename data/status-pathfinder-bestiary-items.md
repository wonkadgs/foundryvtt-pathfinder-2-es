# Estado de la traducción (pathfinder-bestiary-items)

 * **auto-trad**: 3721
 * **ninguna**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[8glkghiOL3IQb37J.htm](pathfinder-bestiary-items/8glkghiOL3IQb37J.htm)|Breath Weapon|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[00dvi5eqFTozS4ym.htm](pathfinder-bestiary-items/00dvi5eqFTozS4ym.htm)|Vortex|auto-trad|
|[00veaqCowh9RkTrj.htm](pathfinder-bestiary-items/00veaqCowh9RkTrj.htm)|Attack of Opportunity|auto-trad|
|[03GLgyeAGjU6XdPt.htm](pathfinder-bestiary-items/03GLgyeAGjU6XdPt.htm)|Nature Empathy|auto-trad|
|[055jXlPNVGmKtAdN.htm](pathfinder-bestiary-items/055jXlPNVGmKtAdN.htm)|Water Mastery|auto-trad|
|[093ZnioHRqaohRiq.htm](pathfinder-bestiary-items/093ZnioHRqaohRiq.htm)|Throw Rock|auto-trad|
|[09dEV7gYGOvdo2T6.htm](pathfinder-bestiary-items/09dEV7gYGOvdo2T6.htm)|Water Mastery|auto-trad|
|[09tr8RqXPJwNExtR.htm](pathfinder-bestiary-items/09tr8RqXPJwNExtR.htm)|Deafening Aria|auto-trad|
|[0A47mfqDMbHBxJqs.htm](pathfinder-bestiary-items/0A47mfqDMbHBxJqs.htm)|Frightful Presence|auto-trad|
|[0bcvSP6hBMQd3ATb.htm](pathfinder-bestiary-items/0bcvSP6hBMQd3ATb.htm)|Snow Vision|auto-trad|
|[0BMCA9P7mLMAMs10.htm](pathfinder-bestiary-items/0BMCA9P7mLMAMs10.htm)|Sunder Objects|auto-trad|
|[0C8sO34ceJyWY2uw.htm](pathfinder-bestiary-items/0C8sO34ceJyWY2uw.htm)|Tail|auto-trad|
|[0d64w4IL9e8s6Tft.htm](pathfinder-bestiary-items/0d64w4IL9e8s6Tft.htm)|Frightful Presence|auto-trad|
|[0EEefBsju5qv29p5.htm](pathfinder-bestiary-items/0EEefBsju5qv29p5.htm)|Fangs|auto-trad|
|[0Ew7KigrRdcxAx7K.htm](pathfinder-bestiary-items/0Ew7KigrRdcxAx7K.htm)|Jaws|auto-trad|
|[0F1cIENDKxVfgMvZ.htm](pathfinder-bestiary-items/0F1cIENDKxVfgMvZ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[0FCCLjtCWvBlUoFA.htm](pathfinder-bestiary-items/0FCCLjtCWvBlUoFA.htm)|Draconic Frenzy|auto-trad|
|[0fQVIqLvgnfSP4JM.htm](pathfinder-bestiary-items/0fQVIqLvgnfSP4JM.htm)|Stinger|auto-trad|
|[0Hf2BPIsl4GkboQ4.htm](pathfinder-bestiary-items/0Hf2BPIsl4GkboQ4.htm)|Dagger|auto-trad|
|[0HSgD6W8PvrZ62ts.htm](pathfinder-bestiary-items/0HSgD6W8PvrZ62ts.htm)|Shark Commune 150 feet|auto-trad|
|[0IoDOGJ3snGokTeZ.htm](pathfinder-bestiary-items/0IoDOGJ3snGokTeZ.htm)|Draconic Momentum|auto-trad|
|[0IyzaM7Q6UJqM2St.htm](pathfinder-bestiary-items/0IyzaM7Q6UJqM2St.htm)|Spear|auto-trad|
|[0IZG1AJ5MeYSR9mc.htm](pathfinder-bestiary-items/0IZG1AJ5MeYSR9mc.htm)|Tail|auto-trad|
|[0JCakW3iPRouHFU2.htm](pathfinder-bestiary-items/0JCakW3iPRouHFU2.htm)|Swallow Whole|auto-trad|
|[0kjAz4Df4rLFyFN6.htm](pathfinder-bestiary-items/0kjAz4Df4rLFyFN6.htm)|Grab|auto-trad|
|[0KLll6op6LwSp19s.htm](pathfinder-bestiary-items/0KLll6op6LwSp19s.htm)|Hydra Regeneration|auto-trad|
|[0L6qwj5JpEiwbnph.htm](pathfinder-bestiary-items/0L6qwj5JpEiwbnph.htm)|Storm of Jaws|auto-trad|
|[0lFm2C1Slc60GuoD.htm](pathfinder-bestiary-items/0lFm2C1Slc60GuoD.htm)|Change Shape|auto-trad|
|[0lokdD3KSOzhNMOi.htm](pathfinder-bestiary-items/0lokdD3KSOzhNMOi.htm)|Arcane Prepared Spells|auto-trad|
|[0lvwb1cNAaOUc6Be.htm](pathfinder-bestiary-items/0lvwb1cNAaOUc6Be.htm)|End the Charade|auto-trad|
|[0mC5JVblweUZ0WxU.htm](pathfinder-bestiary-items/0mC5JVblweUZ0WxU.htm)|Jaws|auto-trad|
|[0mf0RfMRzuYHP7t3.htm](pathfinder-bestiary-items/0mf0RfMRzuYHP7t3.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[0mOkmWdA59zQVh9W.htm](pathfinder-bestiary-items/0mOkmWdA59zQVh9W.htm)|At-Will Spells|auto-trad|
|[0naynsYzlUHn8xiZ.htm](pathfinder-bestiary-items/0naynsYzlUHn8xiZ.htm)|Breath Weapon|auto-trad|
|[0odea1XiQLeiXEH3.htm](pathfinder-bestiary-items/0odea1XiQLeiXEH3.htm)|Draconic Momentum|auto-trad|
|[0oICLdGDJMUmfVhn.htm](pathfinder-bestiary-items/0oICLdGDJMUmfVhn.htm)|Draconic Momentum|auto-trad|
|[0OOTRB9nI9G0zd8v.htm](pathfinder-bestiary-items/0OOTRB9nI9G0zd8v.htm)|Breath Weapon|auto-trad|
|[0oxd4GYRm9A47FeP.htm](pathfinder-bestiary-items/0oxd4GYRm9A47FeP.htm)|Darkvision|auto-trad|
|[0Pe1uBGVehRLhBim.htm](pathfinder-bestiary-items/0Pe1uBGVehRLhBim.htm)|Attack of Opportunity|auto-trad|
|[0PlcoqdlSWJK4XCo.htm](pathfinder-bestiary-items/0PlcoqdlSWJK4XCo.htm)|At-Will Spells|auto-trad|
|[0QQqIESJlQrO4KUL.htm](pathfinder-bestiary-items/0QQqIESJlQrO4KUL.htm)|Talon|auto-trad|
|[0Qt9ULbGzJV8ZG3W.htm](pathfinder-bestiary-items/0Qt9ULbGzJV8ZG3W.htm)|Shadow Spawn|auto-trad|
|[0qVGuAvlfZ7EplZ5.htm](pathfinder-bestiary-items/0qVGuAvlfZ7EplZ5.htm)|Infuse Weapon|auto-trad|
|[0qvSsLdTulsncbsi.htm](pathfinder-bestiary-items/0qvSsLdTulsncbsi.htm)|Hurried Retreat|auto-trad|
|[0QWVJ7Of8cQNKp0N.htm](pathfinder-bestiary-items/0QWVJ7Of8cQNKp0N.htm)|Slink in Shadows|auto-trad|
|[0rf640gBxRc7gfuM.htm](pathfinder-bestiary-items/0rf640gBxRc7gfuM.htm)|Spatial Riptide|auto-trad|
|[0RsLlXY365ytSohG.htm](pathfinder-bestiary-items/0RsLlXY365ytSohG.htm)|Countermeasures|auto-trad|
|[0RWfxFkqPwKKKdiY.htm](pathfinder-bestiary-items/0RWfxFkqPwKKKdiY.htm)|Ground Slam|auto-trad|
|[0SVph284K2Css2S6.htm](pathfinder-bestiary-items/0SVph284K2Css2S6.htm)|Implant Eggs|auto-trad|
|[0t42IbY5dtDOP3PD.htm](pathfinder-bestiary-items/0t42IbY5dtDOP3PD.htm)|Jaws|auto-trad|
|[0UgwXoyN2VLdj2sc.htm](pathfinder-bestiary-items/0UgwXoyN2VLdj2sc.htm)|Darkvision|auto-trad|
|[0UIiiS68BvgFMTnC.htm](pathfinder-bestiary-items/0UIiiS68BvgFMTnC.htm)|Darkvision|auto-trad|
|[0uJ1CwhddCrEZUCC.htm](pathfinder-bestiary-items/0uJ1CwhddCrEZUCC.htm)|Darkvision|auto-trad|
|[0uxUMo5bNxKA5eIg.htm](pathfinder-bestiary-items/0uxUMo5bNxKA5eIg.htm)|Change Shape|auto-trad|
|[0uxyJLMRCpbV8CsL.htm](pathfinder-bestiary-items/0uxyJLMRCpbV8CsL.htm)|Low-Light Vision|auto-trad|
|[0vN6RMlNxCUWYSPj.htm](pathfinder-bestiary-items/0vN6RMlNxCUWYSPj.htm)|Spore Pod|auto-trad|
|[0vSVpFhBgh2bSdVc.htm](pathfinder-bestiary-items/0vSVpFhBgh2bSdVc.htm)|Fist|auto-trad|
|[0w3FhoNrrNel3ZcH.htm](pathfinder-bestiary-items/0w3FhoNrrNel3ZcH.htm)|Negative Healing|auto-trad|
|[0WRHP09tquSt6GBg.htm](pathfinder-bestiary-items/0WRHP09tquSt6GBg.htm)|Darkvision|auto-trad|
|[0xfJmXxtDph8nDjZ.htm](pathfinder-bestiary-items/0xfJmXxtDph8nDjZ.htm)|Dagger|auto-trad|
|[0xo9GafMlJXTsSNk.htm](pathfinder-bestiary-items/0xo9GafMlJXTsSNk.htm)|Predator's Advantage|auto-trad|
|[0Xocyn67jFjoV2Ff.htm](pathfinder-bestiary-items/0Xocyn67jFjoV2Ff.htm)|Catch Rock|auto-trad|
|[0XydXCehPsY2yGto.htm](pathfinder-bestiary-items/0XydXCehPsY2yGto.htm)|Bristles|auto-trad|
|[0Xz9Bpg1z49xuutq.htm](pathfinder-bestiary-items/0Xz9Bpg1z49xuutq.htm)|At-Will Spells|auto-trad|
|[0yW0yjKdtDS5ur0I.htm](pathfinder-bestiary-items/0yW0yjKdtDS5ur0I.htm)|Breath Weapon|auto-trad|
|[0ZKaotgANUyAmZDc.htm](pathfinder-bestiary-items/0ZKaotgANUyAmZDc.htm)|Cat's Luck|auto-trad|
|[11mRPoFSEt1nYAnE.htm](pathfinder-bestiary-items/11mRPoFSEt1nYAnE.htm)|Slowing Pulse|auto-trad|
|[12LD6QHUzml95C9n.htm](pathfinder-bestiary-items/12LD6QHUzml95C9n.htm)|Frost Greatsword|auto-trad|
|[13YXYsRSLVocnnzA.htm](pathfinder-bestiary-items/13YXYsRSLVocnnzA.htm)|Jaws|auto-trad|
|[16mvgME2H3Zp8aMW.htm](pathfinder-bestiary-items/16mvgME2H3Zp8aMW.htm)|Jaws|auto-trad|
|[19cNQjbQEVxmiger.htm](pathfinder-bestiary-items/19cNQjbQEVxmiger.htm)|Telekinetic Whirlwind|auto-trad|
|[1AbBtZhPY7lYpSm0.htm](pathfinder-bestiary-items/1AbBtZhPY7lYpSm0.htm)|Goblin Pox|auto-trad|
|[1AemwaFBoSAJ1UHj.htm](pathfinder-bestiary-items/1AemwaFBoSAJ1UHj.htm)|Change Shape|auto-trad|
|[1ArJ2KyY45DuuLtC.htm](pathfinder-bestiary-items/1ArJ2KyY45DuuLtC.htm)|Crashing Wind|auto-trad|
|[1AzFNDZFiaJxTAJf.htm](pathfinder-bestiary-items/1AzFNDZFiaJxTAJf.htm)|Undead Steed|auto-trad|
|[1BwIo7mgXKcuS2Vg.htm](pathfinder-bestiary-items/1BwIo7mgXKcuS2Vg.htm)|Weakening Strike|auto-trad|
|[1cbIF1wUJSF3WWPk.htm](pathfinder-bestiary-items/1cbIF1wUJSF3WWPk.htm)|Telekinetic Object (Bludgeoning)|auto-trad|
|[1dUCb8dBnYQ0kq4t.htm](pathfinder-bestiary-items/1dUCb8dBnYQ0kq4t.htm)|Divine Innate Spells|auto-trad|
|[1eQUxiVPWo5ndGVE.htm](pathfinder-bestiary-items/1eQUxiVPWo5ndGVE.htm)|Frightful Presence|auto-trad|
|[1EVHcXa6gUWUY0Cl.htm](pathfinder-bestiary-items/1EVHcXa6gUWUY0Cl.htm)|Darkvision|auto-trad|
|[1EXMfAvy6zP7X2ix.htm](pathfinder-bestiary-items/1EXMfAvy6zP7X2ix.htm)|Consume Memories|auto-trad|
|[1fzwu2J1jldVzBPc.htm](pathfinder-bestiary-items/1fzwu2J1jldVzBPc.htm)|Coil|auto-trad|
|[1GBTPnqIIGld3Wu1.htm](pathfinder-bestiary-items/1GBTPnqIIGld3Wu1.htm)|Dragonscaled|auto-trad|
|[1gKtaE366xqxGDdV.htm](pathfinder-bestiary-items/1gKtaE366xqxGDdV.htm)|Formation|auto-trad|
|[1HJ58kI3YTxmr0Cw.htm](pathfinder-bestiary-items/1HJ58kI3YTxmr0Cw.htm)|At-Will Spells|auto-trad|
|[1hjabmTtR1jzUEh9.htm](pathfinder-bestiary-items/1hjabmTtR1jzUEh9.htm)|Hellish Revenge|auto-trad|
|[1ich4lSlipgnuhyC.htm](pathfinder-bestiary-items/1ich4lSlipgnuhyC.htm)|Despair|auto-trad|
|[1jREnmv7xdTiRRHp.htm](pathfinder-bestiary-items/1jREnmv7xdTiRRHp.htm)|Sneak Attack|auto-trad|
|[1L0T2L67vRGgNyeg.htm](pathfinder-bestiary-items/1L0T2L67vRGgNyeg.htm)|Darkvision|auto-trad|
|[1lCsSRhidCzWS3Hd.htm](pathfinder-bestiary-items/1lCsSRhidCzWS3Hd.htm)|Darkvision|auto-trad|
|[1msRWqXGXC7BMldV.htm](pathfinder-bestiary-items/1msRWqXGXC7BMldV.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[1N1JI8DtK2Tm842J.htm](pathfinder-bestiary-items/1N1JI8DtK2Tm842J.htm)|Deep Breath|auto-trad|
|[1N7APEdVVZHtrMup.htm](pathfinder-bestiary-items/1N7APEdVVZHtrMup.htm)|Cacodaemonia|auto-trad|
|[1O3Wpc1lqSdFezG5.htm](pathfinder-bestiary-items/1O3Wpc1lqSdFezG5.htm)|Darkvision|auto-trad|
|[1oQJIAQyfTpYuBCX.htm](pathfinder-bestiary-items/1oQJIAQyfTpYuBCX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[1P2Kly3ELf2X8jdv.htm](pathfinder-bestiary-items/1P2Kly3ELf2X8jdv.htm)|Jaw|auto-trad|
|[1pyQC74ZkhcOllQ8.htm](pathfinder-bestiary-items/1pyQC74ZkhcOllQ8.htm)|Hatchet|auto-trad|
|[1qFhgFYCKIDaF8mk.htm](pathfinder-bestiary-items/1qFhgFYCKIDaF8mk.htm)|Devour Soul|auto-trad|
|[1qswe7SVM8T7WkWr.htm](pathfinder-bestiary-items/1qswe7SVM8T7WkWr.htm)|Darkvision|auto-trad|
|[1QVkgQh59Pmq5fP3.htm](pathfinder-bestiary-items/1QVkgQh59Pmq5fP3.htm)|Splintered Ground|auto-trad|
|[1RIzUNU0v2cfgsjD.htm](pathfinder-bestiary-items/1RIzUNU0v2cfgsjD.htm)|Darkvision|auto-trad|
|[1rQD15HWHf8e3q5K.htm](pathfinder-bestiary-items/1rQD15HWHf8e3q5K.htm)|Jaws|auto-trad|
|[1TeUoHVS0bkCD9jX.htm](pathfinder-bestiary-items/1TeUoHVS0bkCD9jX.htm)|Ghostly Hand|auto-trad|
|[1TgSjt8UKPwTjAH9.htm](pathfinder-bestiary-items/1TgSjt8UKPwTjAH9.htm)|Rugged Travel|auto-trad|
|[1U37Lsr3NMgzLqLM.htm](pathfinder-bestiary-items/1U37Lsr3NMgzLqLM.htm)|Mandibles|auto-trad|
|[1UAh4ooLm8sQPmeN.htm](pathfinder-bestiary-items/1UAh4ooLm8sQPmeN.htm)|Spirit Touch|auto-trad|
|[1UmNfowZ4HWxS9rx.htm](pathfinder-bestiary-items/1UmNfowZ4HWxS9rx.htm)|Devastating Blast|auto-trad|
|[1uWYYut6bpKdXgNp.htm](pathfinder-bestiary-items/1uWYYut6bpKdXgNp.htm)|Arcane Prepared Spells|auto-trad|
|[1uZuWVNr0woQanKG.htm](pathfinder-bestiary-items/1uZuWVNr0woQanKG.htm)|Claw Rake|auto-trad|
|[1V9uaQOnCMGgMxl2.htm](pathfinder-bestiary-items/1V9uaQOnCMGgMxl2.htm)|Darkvision|auto-trad|
|[1VdxeWdeAuS34ymv.htm](pathfinder-bestiary-items/1VdxeWdeAuS34ymv.htm)|Grab|auto-trad|
|[1Vdy1h0xvUBMB9Mg.htm](pathfinder-bestiary-items/1Vdy1h0xvUBMB9Mg.htm)|Sticky Spores|auto-trad|
|[1vjI6FDegZD6lS85.htm](pathfinder-bestiary-items/1vjI6FDegZD6lS85.htm)|Arcane Prepared Spells|auto-trad|
|[1VqUGL35cW3NbTOz.htm](pathfinder-bestiary-items/1VqUGL35cW3NbTOz.htm)|Attack of Opportunity|auto-trad|
|[1wtLTJx2lkvun6gV.htm](pathfinder-bestiary-items/1wtLTJx2lkvun6gV.htm)|Ice Climb|auto-trad|
|[1xcDVCY3sx0uDDys.htm](pathfinder-bestiary-items/1xcDVCY3sx0uDDys.htm)|Greater Constrict|auto-trad|
|[1xLTVMSCCIoZiFMc.htm](pathfinder-bestiary-items/1xLTVMSCCIoZiFMc.htm)|Constant Spells|auto-trad|
|[1yMkzmd8oyagICBZ.htm](pathfinder-bestiary-items/1yMkzmd8oyagICBZ.htm)|Ice Climb|auto-trad|
|[1zCVgprb4WVHkIld.htm](pathfinder-bestiary-items/1zCVgprb4WVHkIld.htm)|At-Will Spells|auto-trad|
|[206EmbcftIC6vntv.htm](pathfinder-bestiary-items/206EmbcftIC6vntv.htm)|Big Swing|auto-trad|
|[209XT5BBx9vzGaVU.htm](pathfinder-bestiary-items/209XT5BBx9vzGaVU.htm)|Frost Longspear|auto-trad|
|[20rnB59A7ShaRC2n.htm](pathfinder-bestiary-items/20rnB59A7ShaRC2n.htm)|Occult Spontaneous Spells|auto-trad|
|[20wHQZMjGAPl30DH.htm](pathfinder-bestiary-items/20wHQZMjGAPl30DH.htm)|Darkvision|auto-trad|
|[20xbqCZ1qrIT5Yqx.htm](pathfinder-bestiary-items/20xbqCZ1qrIT5Yqx.htm)|Ensnare|auto-trad|
|[21Edo8B2yXlM0Sd7.htm](pathfinder-bestiary-items/21Edo8B2yXlM0Sd7.htm)|At-Will Spells|auto-trad|
|[21tFYXEcsuyuh5GQ.htm](pathfinder-bestiary-items/21tFYXEcsuyuh5GQ.htm)|Warbling Song|auto-trad|
|[23duj3dLvK5Gn9tK.htm](pathfinder-bestiary-items/23duj3dLvK5Gn9tK.htm)|Sneak Attack|auto-trad|
|[24g2FSPaGiTvJB6C.htm](pathfinder-bestiary-items/24g2FSPaGiTvJB6C.htm)|Fling|auto-trad|
|[27wudVSiScZejHwt.htm](pathfinder-bestiary-items/27wudVSiScZejHwt.htm)|Negative Healing|auto-trad|
|[28p2N35JT0y99D6q.htm](pathfinder-bestiary-items/28p2N35JT0y99D6q.htm)|Trackless Step|auto-trad|
|[28P5kFSjVh3w1R9o.htm](pathfinder-bestiary-items/28P5kFSjVh3w1R9o.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[28PBBJfMs67GTIf3.htm](pathfinder-bestiary-items/28PBBJfMs67GTIf3.htm)|Leg|auto-trad|
|[29EzhqCR2u7R7K4N.htm](pathfinder-bestiary-items/29EzhqCR2u7R7K4N.htm)|At-Will Spells|auto-trad|
|[2arq3zK9f6JQ5ZcN.htm](pathfinder-bestiary-items/2arq3zK9f6JQ5ZcN.htm)|Darkvision|auto-trad|
|[2BBPsLk2YiOeDwpm.htm](pathfinder-bestiary-items/2BBPsLk2YiOeDwpm.htm)|Darkvision|auto-trad|
|[2bECIomVCBjAE2ET.htm](pathfinder-bestiary-items/2bECIomVCBjAE2ET.htm)|Divine Innate Spells|auto-trad|
|[2d3ZGRIXERcjqnmJ.htm](pathfinder-bestiary-items/2d3ZGRIXERcjqnmJ.htm)|Grab|auto-trad|
|[2DfR3FEW268SuAYe.htm](pathfinder-bestiary-items/2DfR3FEW268SuAYe.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[2ECIwvcXVQ6w8vi5.htm](pathfinder-bestiary-items/2ECIwvcXVQ6w8vi5.htm)|Punishing Momentum|auto-trad|
|[2ER4MKCdewC2qqrQ.htm](pathfinder-bestiary-items/2ER4MKCdewC2qqrQ.htm)|Darkvision|auto-trad|
|[2eV3trm8nKeqQ2Z4.htm](pathfinder-bestiary-items/2eV3trm8nKeqQ2Z4.htm)|Distracting Frolic|auto-trad|
|[2fZ7E297Z4PtEWQg.htm](pathfinder-bestiary-items/2fZ7E297Z4PtEWQg.htm)|Hoof|auto-trad|
|[2gy1EqZ99rbJPoec.htm](pathfinder-bestiary-items/2gy1EqZ99rbJPoec.htm)|Greater Darkvision|auto-trad|
|[2h0yKU2nKX609M3Q.htm](pathfinder-bestiary-items/2h0yKU2nKX609M3Q.htm)|Jaws|auto-trad|
|[2hjq0oo2EsTcXPJe.htm](pathfinder-bestiary-items/2hjq0oo2EsTcXPJe.htm)|Tail|auto-trad|
|[2iGIMuumKfirLAzh.htm](pathfinder-bestiary-items/2iGIMuumKfirLAzh.htm)|Arcane Innate Spells|auto-trad|
|[2in6dEOh1ygunO7S.htm](pathfinder-bestiary-items/2in6dEOh1ygunO7S.htm)|Constant Spells|auto-trad|
|[2J5aw0w2MvuE4ISK.htm](pathfinder-bestiary-items/2J5aw0w2MvuE4ISK.htm)|Claw|auto-trad|
|[2JiFczs0m1iIKqPZ.htm](pathfinder-bestiary-items/2JiFczs0m1iIKqPZ.htm)|Flurry of Strands|auto-trad|
|[2jnfsvQ0IphAdWs2.htm](pathfinder-bestiary-items/2jnfsvQ0IphAdWs2.htm)|Sneak Attack|auto-trad|
|[2lGYb3ZO8pwxgMhD.htm](pathfinder-bestiary-items/2lGYb3ZO8pwxgMhD.htm)|Focus Gaze|auto-trad|
|[2m9M9QruB9jYdEjd.htm](pathfinder-bestiary-items/2m9M9QruB9jYdEjd.htm)|Darkvision|auto-trad|
|[2MJUBT30Vn8bziTk.htm](pathfinder-bestiary-items/2MJUBT30Vn8bziTk.htm)|Two-Headed Strike|auto-trad|
|[2NA51H5N0n0VDjGt.htm](pathfinder-bestiary-items/2NA51H5N0n0VDjGt.htm)|Darkvision|auto-trad|
|[2nd3Ete1Wir3K3bj.htm](pathfinder-bestiary-items/2nd3Ete1Wir3K3bj.htm)|Wave|auto-trad|
|[2nWoUs5yTHmZJd6s.htm](pathfinder-bestiary-items/2nWoUs5yTHmZJd6s.htm)|Breath Weapon|auto-trad|
|[2OIVJJBOOtBD6Bce.htm](pathfinder-bestiary-items/2OIVJJBOOtBD6Bce.htm)|Horn|auto-trad|
|[2OPayJrYVYfP1LZk.htm](pathfinder-bestiary-items/2OPayJrYVYfP1LZk.htm)|Red Cap|auto-trad|
|[2PnyMPtvBpBFJJyw.htm](pathfinder-bestiary-items/2PnyMPtvBpBFJJyw.htm)|Primal Innate Spells|auto-trad|
|[2QFZHcrVJOpbzlkr.htm](pathfinder-bestiary-items/2QFZHcrVJOpbzlkr.htm)|At-Will Spells|auto-trad|
|[2QnPX6haUy6RjKvT.htm](pathfinder-bestiary-items/2QnPX6haUy6RjKvT.htm)|Frenzy Pheromone|auto-trad|
|[2rAKighB6Ywhk5s3.htm](pathfinder-bestiary-items/2rAKighB6Ywhk5s3.htm)|Shock|auto-trad|
|[2rqAbhpsIBtY1lZp.htm](pathfinder-bestiary-items/2rqAbhpsIBtY1lZp.htm)|Regeneration 15 (Deactivated by Chaotic)|auto-trad|
|[2S4BfxyE7xLYIYr3.htm](pathfinder-bestiary-items/2S4BfxyE7xLYIYr3.htm)|Grab|auto-trad|
|[2sLJMpYvshlQx7Ck.htm](pathfinder-bestiary-items/2sLJMpYvshlQx7Ck.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[2T8gBoUiFYXYQ2wp.htm](pathfinder-bestiary-items/2T8gBoUiFYXYQ2wp.htm)|Rock|auto-trad|
|[2TBzK79NxowRc0As.htm](pathfinder-bestiary-items/2TBzK79NxowRc0As.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[2Tk2P2vXOBYquTtd.htm](pathfinder-bestiary-items/2Tk2P2vXOBYquTtd.htm)|Draconic Frenzy|auto-trad|
|[2u5CXi7tZODYDpbJ.htm](pathfinder-bestiary-items/2u5CXi7tZODYDpbJ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[2ujro0hgy7xKZGw3.htm](pathfinder-bestiary-items/2ujro0hgy7xKZGw3.htm)|Constant Spells|auto-trad|
|[2UpzHQ985k9cDkOT.htm](pathfinder-bestiary-items/2UpzHQ985k9cDkOT.htm)|Swiftness|auto-trad|
|[2V1LMw2K5gNCyFjF.htm](pathfinder-bestiary-items/2V1LMw2K5gNCyFjF.htm)|Shuln Saliva|auto-trad|
|[2V6mUk75bI02ShGa.htm](pathfinder-bestiary-items/2V6mUk75bI02ShGa.htm)|Silver Rapier|auto-trad|
|[2vBMTexIigEtaKt3.htm](pathfinder-bestiary-items/2vBMTexIigEtaKt3.htm)|Breath Weapon|auto-trad|
|[2vkb6KDhuhqgqrlz.htm](pathfinder-bestiary-items/2vkb6KDhuhqgqrlz.htm)|Warhammer|auto-trad|
|[2vwlAmF93myITMjw.htm](pathfinder-bestiary-items/2vwlAmF93myITMjw.htm)|Sling|auto-trad|
|[2vzPRuTJZNWARxiC.htm](pathfinder-bestiary-items/2vzPRuTJZNWARxiC.htm)|Tail Lash|auto-trad|
|[2wdfh7JIT7SjhXIz.htm](pathfinder-bestiary-items/2wdfh7JIT7SjhXIz.htm)|Claw|auto-trad|
|[2wvbMbHq2fdLmeju.htm](pathfinder-bestiary-items/2wvbMbHq2fdLmeju.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[2xaMJwr5gjV30x3x.htm](pathfinder-bestiary-items/2xaMJwr5gjV30x3x.htm)|Drench|auto-trad|
|[2XSVapoeItmk2zU0.htm](pathfinder-bestiary-items/2XSVapoeItmk2zU0.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[2YipTTPgQfiSBAHG.htm](pathfinder-bestiary-items/2YipTTPgQfiSBAHG.htm)|Climb Stone|auto-trad|
|[2YwZxBJyyprGUZdO.htm](pathfinder-bestiary-items/2YwZxBJyyprGUZdO.htm)|Jaws|auto-trad|
|[30Jii0vTB1Vs9wP6.htm](pathfinder-bestiary-items/30Jii0vTB1Vs9wP6.htm)|Jaws|auto-trad|
|[311KXJPh4RSeBSYh.htm](pathfinder-bestiary-items/311KXJPh4RSeBSYh.htm)|Low-Light Vision|auto-trad|
|[314KNkmZCbFk7BMQ.htm](pathfinder-bestiary-items/314KNkmZCbFk7BMQ.htm)|Darkvision|auto-trad|
|[36QKwrAooz1cygR4.htm](pathfinder-bestiary-items/36QKwrAooz1cygR4.htm)|Cytillesh Stare|auto-trad|
|[36TWBrEib7ZN4zrU.htm](pathfinder-bestiary-items/36TWBrEib7ZN4zrU.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[36UmeWHlowHAjVXV.htm](pathfinder-bestiary-items/36UmeWHlowHAjVXV.htm)|Combat Grab|auto-trad|
|[383lohD54D45DNRU.htm](pathfinder-bestiary-items/383lohD54D45DNRU.htm)|Lance|auto-trad|
|[387c1CSiNx4uWeqZ.htm](pathfinder-bestiary-items/387c1CSiNx4uWeqZ.htm)|Sneak Attack|auto-trad|
|[38uEEYKxUvG8TDFw.htm](pathfinder-bestiary-items/38uEEYKxUvG8TDFw.htm)|Crossbow|auto-trad|
|[39ZIhV5v80216sO2.htm](pathfinder-bestiary-items/39ZIhV5v80216sO2.htm)|Manifold Vision|auto-trad|
|[3A8TmAuVP2C3XDsX.htm](pathfinder-bestiary-items/3A8TmAuVP2C3XDsX.htm)|Refuse Pile|auto-trad|
|[3aQmojgOn0OyqrYB.htm](pathfinder-bestiary-items/3aQmojgOn0OyqrYB.htm)|Retributive Strike|auto-trad|
|[3b7IgOx3d3REF2NR.htm](pathfinder-bestiary-items/3b7IgOx3d3REF2NR.htm)|Swoop|auto-trad|
|[3BMDL7DFtWP5HozZ.htm](pathfinder-bestiary-items/3BMDL7DFtWP5HozZ.htm)|Traveler's Aura|auto-trad|
|[3cGYB1usBk6SXYto.htm](pathfinder-bestiary-items/3cGYB1usBk6SXYto.htm)|Jaws|auto-trad|
|[3CJoA00OmtSsKcBR.htm](pathfinder-bestiary-items/3CJoA00OmtSsKcBR.htm)|Aquatic Ambush|auto-trad|
|[3d5cEC77yvSI6Hub.htm](pathfinder-bestiary-items/3d5cEC77yvSI6Hub.htm)|Darkvision|auto-trad|
|[3EFMknJHLeVqcKmf.htm](pathfinder-bestiary-items/3EFMknJHLeVqcKmf.htm)|Low-Light Vision|auto-trad|
|[3EkDCh4CkGrgHhlf.htm](pathfinder-bestiary-items/3EkDCh4CkGrgHhlf.htm)|Antenna Disarm|auto-trad|
|[3Eksrmh390T4SrP7.htm](pathfinder-bestiary-items/3Eksrmh390T4SrP7.htm)|Magic Immunity|auto-trad|
|[3g9yO52wil4VC4Dz.htm](pathfinder-bestiary-items/3g9yO52wil4VC4Dz.htm)|Fangs|auto-trad|
|[3GFUGIkhpZDZer4p.htm](pathfinder-bestiary-items/3GFUGIkhpZDZer4p.htm)|Hoof|auto-trad|
|[3GqX6rg7gxLsmuvd.htm](pathfinder-bestiary-items/3GqX6rg7gxLsmuvd.htm)|Rock|auto-trad|
|[3gYmeheE54dPEZnX.htm](pathfinder-bestiary-items/3gYmeheE54dPEZnX.htm)|Storm Breath|auto-trad|
|[3H0tPL2AeonSy2gM.htm](pathfinder-bestiary-items/3H0tPL2AeonSy2gM.htm)|Jet|auto-trad|
|[3HkWPtBVUhYiB7al.htm](pathfinder-bestiary-items/3HkWPtBVUhYiB7al.htm)|Nimble Dodge|auto-trad|
|[3HPvO9ipnRupy7G0.htm](pathfinder-bestiary-items/3HPvO9ipnRupy7G0.htm)|Living Shield|auto-trad|
|[3Jg7XQGrHZ2lpqc1.htm](pathfinder-bestiary-items/3Jg7XQGrHZ2lpqc1.htm)|Darkvision|auto-trad|
|[3jppXkfB2qjbeRic.htm](pathfinder-bestiary-items/3jppXkfB2qjbeRic.htm)|Vortex|auto-trad|
|[3KkpzgxFNPh9aZJ4.htm](pathfinder-bestiary-items/3KkpzgxFNPh9aZJ4.htm)|Talon|auto-trad|
|[3nwKZarzSffmcKzH.htm](pathfinder-bestiary-items/3nwKZarzSffmcKzH.htm)|Improved Grab|auto-trad|
|[3o5i63MTjWyPqXG3.htm](pathfinder-bestiary-items/3o5i63MTjWyPqXG3.htm)|Transfer Protection|auto-trad|
|[3psvmOBMbToGGKnF.htm](pathfinder-bestiary-items/3psvmOBMbToGGKnF.htm)|Claw|auto-trad|
|[3RchvgPIdNc2KUu9.htm](pathfinder-bestiary-items/3RchvgPIdNc2KUu9.htm)|Fist|auto-trad|
|[3RCPLD1d6zfYm2VI.htm](pathfinder-bestiary-items/3RCPLD1d6zfYm2VI.htm)|Rugged Travel|auto-trad|
|[3SNH7v25J8TbPWvC.htm](pathfinder-bestiary-items/3SNH7v25J8TbPWvC.htm)|Construct Armor (Hardness 2)|auto-trad|
|[3SpNnQV9X8P44yW0.htm](pathfinder-bestiary-items/3SpNnQV9X8P44yW0.htm)|Darkvision|auto-trad|
|[3TfIMe12rJpDf9LZ.htm](pathfinder-bestiary-items/3TfIMe12rJpDf9LZ.htm)|Self-Loathing|auto-trad|
|[3tXLO7Kl4rqCcOSV.htm](pathfinder-bestiary-items/3tXLO7Kl4rqCcOSV.htm)|Darkvision|auto-trad|
|[3tyBaMgyINlZCj76.htm](pathfinder-bestiary-items/3tyBaMgyINlZCj76.htm)|Fist|auto-trad|
|[3TZWlkMSDjauBP7y.htm](pathfinder-bestiary-items/3TZWlkMSDjauBP7y.htm)|Jaws|auto-trad|
|[3uMuKyVKQxpFxP0o.htm](pathfinder-bestiary-items/3uMuKyVKQxpFxP0o.htm)|Flash of Brutality|auto-trad|
|[3USivzG0qztGd1dK.htm](pathfinder-bestiary-items/3USivzG0qztGd1dK.htm)|Knockdown|auto-trad|
|[3VDIY4vCKlW0qvFl.htm](pathfinder-bestiary-items/3VDIY4vCKlW0qvFl.htm)|Fist|auto-trad|
|[3ViZWXlxM2CUiPGG.htm](pathfinder-bestiary-items/3ViZWXlxM2CUiPGG.htm)|Quicken Pestilence|auto-trad|
|[3wMfK5zZSqFhhTgs.htm](pathfinder-bestiary-items/3wMfK5zZSqFhhTgs.htm)|Primordial Roar|auto-trad|
|[3WumD6XU2ACGghbI.htm](pathfinder-bestiary-items/3WumD6XU2ACGghbI.htm)|+2 Circumstance to All Saves vs. Dream and Sleep|auto-trad|
|[3wWPzjr3ra62ks2k.htm](pathfinder-bestiary-items/3wWPzjr3ra62ks2k.htm)|Swarm Mind|auto-trad|
|[3X0mRQVlCkn2NC2d.htm](pathfinder-bestiary-items/3X0mRQVlCkn2NC2d.htm)|Acid Glob|auto-trad|
|[3XJEBmHGHfUj7TbN.htm](pathfinder-bestiary-items/3XJEBmHGHfUj7TbN.htm)|Piteous Moan|auto-trad|
|[3XNydMhIMm8EBfhy.htm](pathfinder-bestiary-items/3XNydMhIMm8EBfhy.htm)|Dagger|auto-trad|
|[3xoqcUVujuOLU773.htm](pathfinder-bestiary-items/3xoqcUVujuOLU773.htm)|Menacing Guardian|auto-trad|
|[3ZSVEZBMKleuEd84.htm](pathfinder-bestiary-items/3ZSVEZBMKleuEd84.htm)|Dominate|auto-trad|
|[40xrU4USJeyzTqIE.htm](pathfinder-bestiary-items/40xrU4USJeyzTqIE.htm)|Low-Light Vision|auto-trad|
|[42Uo3GU6JFA7w7G8.htm](pathfinder-bestiary-items/42Uo3GU6JFA7w7G8.htm)|Darkvision|auto-trad|
|[44gZk9CBkA5y9VOb.htm](pathfinder-bestiary-items/44gZk9CBkA5y9VOb.htm)|Mass Laughter|auto-trad|
|[44mTKtGhXhfJhnBh.htm](pathfinder-bestiary-items/44mTKtGhXhfJhnBh.htm)|Fast Healing 2 (While Touching Fire)|auto-trad|
|[465xP8CbWAyUkrIj.htm](pathfinder-bestiary-items/465xP8CbWAyUkrIj.htm)|Flame of Justice|auto-trad|
|[47dklzJrGaCDSZ2N.htm](pathfinder-bestiary-items/47dklzJrGaCDSZ2N.htm)|Tremorsense (Imprecise) 30 feet (Creatures Touching its Web)|auto-trad|
|[47Ksb5cr0D7tx1KE.htm](pathfinder-bestiary-items/47Ksb5cr0D7tx1KE.htm)|Low-Light Vision|auto-trad|
|[4adEu2Sh2OYsvkLV.htm](pathfinder-bestiary-items/4adEu2Sh2OYsvkLV.htm)|Fist|auto-trad|
|[4BNXIgVORiBDrL95.htm](pathfinder-bestiary-items/4BNXIgVORiBDrL95.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[4eIvZ5FrB8u5CTbJ.htm](pathfinder-bestiary-items/4eIvZ5FrB8u5CTbJ.htm)|Tentacle Flurry|auto-trad|
|[4ESvAYPL0OBd4s6Q.htm](pathfinder-bestiary-items/4ESvAYPL0OBd4s6Q.htm)|Gestalt|auto-trad|
|[4eVppzidbAbwJeF2.htm](pathfinder-bestiary-items/4eVppzidbAbwJeF2.htm)|Claw|auto-trad|
|[4EXgRMqNIRLXxB1c.htm](pathfinder-bestiary-items/4EXgRMqNIRLXxB1c.htm)|Shortbow|auto-trad|
|[4fZp6kK5h1WOCAAq.htm](pathfinder-bestiary-items/4fZp6kK5h1WOCAAq.htm)|Compression|auto-trad|
|[4gkC6A8D27Jzv7nH.htm](pathfinder-bestiary-items/4gkC6A8D27Jzv7nH.htm)|Broad Swipe|auto-trad|
|[4gO7xPscU8GkItt4.htm](pathfinder-bestiary-items/4gO7xPscU8GkItt4.htm)|Spear|auto-trad|
|[4gP2dSNnL1rudfAt.htm](pathfinder-bestiary-items/4gP2dSNnL1rudfAt.htm)|Tongue|auto-trad|
|[4hDLxVe3DC4KrgiS.htm](pathfinder-bestiary-items/4hDLxVe3DC4KrgiS.htm)|Tail|auto-trad|
|[4ii4lkO4XldDodwj.htm](pathfinder-bestiary-items/4ii4lkO4XldDodwj.htm)|Frightful Presence|auto-trad|
|[4ilCcGowAXU156OC.htm](pathfinder-bestiary-items/4ilCcGowAXU156OC.htm)|Coven Ritual|auto-trad|
|[4Iw8N4ANO4rwmrYg.htm](pathfinder-bestiary-items/4Iw8N4ANO4rwmrYg.htm)|Tentacle|auto-trad|
|[4JCt8tmxCghbphU5.htm](pathfinder-bestiary-items/4JCt8tmxCghbphU5.htm)|Catch Rock|auto-trad|
|[4jo5EjR59Kycpzcy.htm](pathfinder-bestiary-items/4jo5EjR59Kycpzcy.htm)|Hand Crossbow|auto-trad|
|[4k5fQuKObwYhz1PU.htm](pathfinder-bestiary-items/4k5fQuKObwYhz1PU.htm)|Jaws|auto-trad|
|[4kBeGDMZLn75niF4.htm](pathfinder-bestiary-items/4kBeGDMZLn75niF4.htm)|Sound Imitation|auto-trad|
|[4lmj5hd20I5GvQJz.htm](pathfinder-bestiary-items/4lmj5hd20I5GvQJz.htm)|Spring Upon Prey|auto-trad|
|[4lt2OIvDnuajjUbn.htm](pathfinder-bestiary-items/4lt2OIvDnuajjUbn.htm)|Divine Rituals|auto-trad|
|[4Lxyi7ekwyeXWdZe.htm](pathfinder-bestiary-items/4Lxyi7ekwyeXWdZe.htm)|Claw|auto-trad|
|[4n19omVeZZAAGXBk.htm](pathfinder-bestiary-items/4n19omVeZZAAGXBk.htm)|Coven|auto-trad|
|[4N1vw9fFoiAgxSn1.htm](pathfinder-bestiary-items/4N1vw9fFoiAgxSn1.htm)|Flying Strafe|auto-trad|
|[4NBL0GmmD67fgNk5.htm](pathfinder-bestiary-items/4NBL0GmmD67fgNk5.htm)|Claw|auto-trad|
|[4NNU4WrjaWJrngyL.htm](pathfinder-bestiary-items/4NNU4WrjaWJrngyL.htm)|Engulf|auto-trad|
|[4NqTHQqAkLmiUovi.htm](pathfinder-bestiary-items/4NqTHQqAkLmiUovi.htm)|Stench|auto-trad|
|[4NvG4sOzNrtZikHp.htm](pathfinder-bestiary-items/4NvG4sOzNrtZikHp.htm)|Low-Light Vision|auto-trad|
|[4P8IOAM51aWrHmLb.htm](pathfinder-bestiary-items/4P8IOAM51aWrHmLb.htm)|Axe Vulnerability|auto-trad|
|[4pbTTxqHWnbnAW7T.htm](pathfinder-bestiary-items/4pbTTxqHWnbnAW7T.htm)|Spider Minions|auto-trad|
|[4pcckxSu3lSKQKx9.htm](pathfinder-bestiary-items/4pcckxSu3lSKQKx9.htm)|+2 Circumstance to All Saves vs. Disease|auto-trad|
|[4pzu3XHcK4Rd1GQX.htm](pathfinder-bestiary-items/4pzu3XHcK4Rd1GQX.htm)|Wing Deflection|auto-trad|
|[4QNftOG1O0v3IrVA.htm](pathfinder-bestiary-items/4QNftOG1O0v3IrVA.htm)|Trip Up|auto-trad|
|[4RfTIZfhWkxNhBsn.htm](pathfinder-bestiary-items/4RfTIZfhWkxNhBsn.htm)|Occult Innate Spells|auto-trad|
|[4rs5uETeGIZjVUCC.htm](pathfinder-bestiary-items/4rs5uETeGIZjVUCC.htm)|Dimensional Dervish|auto-trad|
|[4s0p8fCnssqfycUf.htm](pathfinder-bestiary-items/4s0p8fCnssqfycUf.htm)|Swallow Whole|auto-trad|
|[4SeHhCM0ulS4TIjL.htm](pathfinder-bestiary-items/4SeHhCM0ulS4TIjL.htm)|Darkvision|auto-trad|
|[4tkIBY4fl80WZyr6.htm](pathfinder-bestiary-items/4tkIBY4fl80WZyr6.htm)|Spit Venom|auto-trad|
|[4tRvd9W1xA0JvgPJ.htm](pathfinder-bestiary-items/4tRvd9W1xA0JvgPJ.htm)|Claw|auto-trad|
|[4tsSsz41K5abcM8D.htm](pathfinder-bestiary-items/4tsSsz41K5abcM8D.htm)|Swallow Whole|auto-trad|
|[4TtLSpRhmqV1Dvp1.htm](pathfinder-bestiary-items/4TtLSpRhmqV1Dvp1.htm)|Tail Whip|auto-trad|
|[4uh4OO7zv1oZkB4W.htm](pathfinder-bestiary-items/4uh4OO7zv1oZkB4W.htm)|Dogslicer|auto-trad|
|[4uV5kuuPiKE59I3w.htm](pathfinder-bestiary-items/4uV5kuuPiKE59I3w.htm)|At-Will Spells|auto-trad|
|[4uWQE7ermpG8oEr3.htm](pathfinder-bestiary-items/4uWQE7ermpG8oEr3.htm)|Glaive|auto-trad|
|[4uy8ctqHkWgUS4KY.htm](pathfinder-bestiary-items/4uy8ctqHkWgUS4KY.htm)|Breath Weapon|auto-trad|
|[4vfOgL2x3vJgpsBu.htm](pathfinder-bestiary-items/4vfOgL2x3vJgpsBu.htm)|Glide|auto-trad|
|[4VHRW4Y0OmyqAHpQ.htm](pathfinder-bestiary-items/4VHRW4Y0OmyqAHpQ.htm)|Primal Innate Spells|auto-trad|
|[4vrCCq5VVcSm3rwb.htm](pathfinder-bestiary-items/4vrCCq5VVcSm3rwb.htm)|Low-Light Vision|auto-trad|
|[4WUyRzJH8pu1zq5J.htm](pathfinder-bestiary-items/4WUyRzJH8pu1zq5J.htm)|Channel Rot|auto-trad|
|[4Y4K0inFOH3iWaot.htm](pathfinder-bestiary-items/4Y4K0inFOH3iWaot.htm)|Jaws|auto-trad|
|[4Y6ugZExUjpvvoVy.htm](pathfinder-bestiary-items/4Y6ugZExUjpvvoVy.htm)|Darkvision|auto-trad|
|[4ysvHeALdBNPXE0e.htm](pathfinder-bestiary-items/4ysvHeALdBNPXE0e.htm)|Arcane Prepared Spells|auto-trad|
|[50Meu5uWTOPYtMu6.htm](pathfinder-bestiary-items/50Meu5uWTOPYtMu6.htm)|Regeneration 20 (Deactivated by Acid or Fire)|auto-trad|
|[50tgcNZoPBn2RCy3.htm](pathfinder-bestiary-items/50tgcNZoPBn2RCy3.htm)|Claw|auto-trad|
|[53FM1WWIJj3p1kzG.htm](pathfinder-bestiary-items/53FM1WWIJj3p1kzG.htm)|At-Will Spells|auto-trad|
|[53LkbbyoyISSppMe.htm](pathfinder-bestiary-items/53LkbbyoyISSppMe.htm)|Purple Worm Venom|auto-trad|
|[53TdkHMcaLAUgDXf.htm](pathfinder-bestiary-items/53TdkHMcaLAUgDXf.htm)|Dragon Heat|auto-trad|
|[53xmmigXl7qYER3Y.htm](pathfinder-bestiary-items/53xmmigXl7qYER3Y.htm)|Infuse Weapons|auto-trad|
|[54j0gybWlIAueFkZ.htm](pathfinder-bestiary-items/54j0gybWlIAueFkZ.htm)|Shortsword|auto-trad|
|[54pvmAtUtSvN1SQx.htm](pathfinder-bestiary-items/54pvmAtUtSvN1SQx.htm)|Shield Block|auto-trad|
|[55EdzLqRdJ3A5Win.htm](pathfinder-bestiary-items/55EdzLqRdJ3A5Win.htm)|Draconic Momentum|auto-trad|
|[55Hvc61MUgVoahwb.htm](pathfinder-bestiary-items/55Hvc61MUgVoahwb.htm)|Terrain Advantage|auto-trad|
|[56yBsaqp9GmItJzk.htm](pathfinder-bestiary-items/56yBsaqp9GmItJzk.htm)|Breath Weapon|auto-trad|
|[57lDKo6dcd3jNhcT.htm](pathfinder-bestiary-items/57lDKo6dcd3jNhcT.htm)|Jaws|auto-trad|
|[57QWHxHOoldzPbcN.htm](pathfinder-bestiary-items/57QWHxHOoldzPbcN.htm)|Claw|auto-trad|
|[59wy5xK7MEQmYRxg.htm](pathfinder-bestiary-items/59wy5xK7MEQmYRxg.htm)|Earth Glide|auto-trad|
|[5CiCYm71I6kB4UPC.htm](pathfinder-bestiary-items/5CiCYm71I6kB4UPC.htm)|Masterful Quickened Casting|auto-trad|
|[5dshGJm0RD445JZG.htm](pathfinder-bestiary-items/5dshGJm0RD445JZG.htm)|Horns|auto-trad|
|[5e9xpAPgJWQxMqDB.htm](pathfinder-bestiary-items/5e9xpAPgJWQxMqDB.htm)|Low-Light Vision|auto-trad|
|[5ednVEQACX3S93gP.htm](pathfinder-bestiary-items/5ednVEQACX3S93gP.htm)|Constant Spells|auto-trad|
|[5Em7mvb9ul24rwbD.htm](pathfinder-bestiary-items/5Em7mvb9ul24rwbD.htm)|Frightful Presence|auto-trad|
|[5EvbvNghD1THDwUR.htm](pathfinder-bestiary-items/5EvbvNghD1THDwUR.htm)|Negative Healing|auto-trad|
|[5fkFF7Wnl8Mmf8Zu.htm](pathfinder-bestiary-items/5fkFF7Wnl8Mmf8Zu.htm)|Draconic Frenzy|auto-trad|
|[5HpQse2MIBQbs10B.htm](pathfinder-bestiary-items/5HpQse2MIBQbs10B.htm)|Web|auto-trad|
|[5HRKFbIvbD2qXTXD.htm](pathfinder-bestiary-items/5HRKFbIvbD2qXTXD.htm)|Hand Crossbow|auto-trad|
|[5I2ELiZx1Aa9j37J.htm](pathfinder-bestiary-items/5I2ELiZx1Aa9j37J.htm)|Attack of Opportunity|auto-trad|
|[5IHmSdZpGxrw76lc.htm](pathfinder-bestiary-items/5IHmSdZpGxrw76lc.htm)|Darkvision|auto-trad|
|[5k5qe7LZeapjusIM.htm](pathfinder-bestiary-items/5k5qe7LZeapjusIM.htm)|Breath Weapon|auto-trad|
|[5kEltylMi3m28RpQ.htm](pathfinder-bestiary-items/5kEltylMi3m28RpQ.htm)|Darkvision|auto-trad|
|[5kWr1s58IkPKdMcL.htm](pathfinder-bestiary-items/5kWr1s58IkPKdMcL.htm)|Mirage|auto-trad|
|[5lNem5v3czqg4yta.htm](pathfinder-bestiary-items/5lNem5v3czqg4yta.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[5McIXOemuKXFd7Ep.htm](pathfinder-bestiary-items/5McIXOemuKXFd7Ep.htm)|Vicious Wounds|auto-trad|
|[5NPT2o7xLwm4QVG0.htm](pathfinder-bestiary-items/5NPT2o7xLwm4QVG0.htm)|Grab|auto-trad|
|[5nrxrLm9SgLXHRhi.htm](pathfinder-bestiary-items/5nrxrLm9SgLXHRhi.htm)|Staff|auto-trad|
|[5O3D1KLFuAtqX80b.htm](pathfinder-bestiary-items/5O3D1KLFuAtqX80b.htm)|Grabbing Trunk|auto-trad|
|[5o4Qh26axxAvJeUp.htm](pathfinder-bestiary-items/5o4Qh26axxAvJeUp.htm)|Grab|auto-trad|
|[5ogrmYbXtCuT4HPN.htm](pathfinder-bestiary-items/5ogrmYbXtCuT4HPN.htm)|Corrupt Water|auto-trad|
|[5ozQMc30WbtUYecf.htm](pathfinder-bestiary-items/5ozQMc30WbtUYecf.htm)|Draconic Frenzy|auto-trad|
|[5P26T61QIn85Jbfk.htm](pathfinder-bestiary-items/5P26T61QIn85Jbfk.htm)|Constrict|auto-trad|
|[5PDURAzo537r6kje.htm](pathfinder-bestiary-items/5PDURAzo537r6kje.htm)|Constrict|auto-trad|
|[5pgk77sKtK8gCgSb.htm](pathfinder-bestiary-items/5pgk77sKtK8gCgSb.htm)|Jaws|auto-trad|
|[5prFU0v8oG5s1kHj.htm](pathfinder-bestiary-items/5prFU0v8oG5s1kHj.htm)|Darkvision|auto-trad|
|[5pZXZ7pLE1RdGKX5.htm](pathfinder-bestiary-items/5pZXZ7pLE1RdGKX5.htm)|Improved Grab|auto-trad|
|[5Qq44Y2MrPleByoB.htm](pathfinder-bestiary-items/5Qq44Y2MrPleByoB.htm)|Composite Longbow|auto-trad|
|[5Rd1LNWiczurzj0C.htm](pathfinder-bestiary-items/5Rd1LNWiczurzj0C.htm)|Breath Weapon|auto-trad|
|[5rP5cz0JHgxkYHIF.htm](pathfinder-bestiary-items/5rP5cz0JHgxkYHIF.htm)|Primal Innate Spells|auto-trad|
|[5rSjCpmIctYFNqcn.htm](pathfinder-bestiary-items/5rSjCpmIctYFNqcn.htm)|Draconic Frenzy|auto-trad|
|[5THRshFo5sJLDIle.htm](pathfinder-bestiary-items/5THRshFo5sJLDIle.htm)|Improved Grab|auto-trad|
|[5TLoRDxeWBDh6a6e.htm](pathfinder-bestiary-items/5TLoRDxeWBDh6a6e.htm)|Steady Spellcasting|auto-trad|
|[5TVlHOsmwK2Om8pu.htm](pathfinder-bestiary-items/5TVlHOsmwK2Om8pu.htm)|Hunt Prey|auto-trad|
|[5U4QH2sM8ipWEAcR.htm](pathfinder-bestiary-items/5U4QH2sM8ipWEAcR.htm)|Fast Healing 1|auto-trad|
|[5ulvx0zqprJryzvq.htm](pathfinder-bestiary-items/5ulvx0zqprJryzvq.htm)|Attack of Opportunity|auto-trad|
|[5UO7IHt7Yn75Z9rL.htm](pathfinder-bestiary-items/5UO7IHt7Yn75Z9rL.htm)|Grab|auto-trad|
|[5Uq09OsuqP9JPmlt.htm](pathfinder-bestiary-items/5Uq09OsuqP9JPmlt.htm)|Wavesense 30 feet|auto-trad|
|[5wsaNGdGzezNTnea.htm](pathfinder-bestiary-items/5wsaNGdGzezNTnea.htm)|Twisting Tail|auto-trad|
|[5xEEdMhQDZQFuUpv.htm](pathfinder-bestiary-items/5xEEdMhQDZQFuUpv.htm)|Archon's Door|auto-trad|
|[5yDMt3Vna9iVApvJ.htm](pathfinder-bestiary-items/5yDMt3Vna9iVApvJ.htm)|Blood Scent|auto-trad|
|[5Yp3kZDHE8gZhhLe.htm](pathfinder-bestiary-items/5Yp3kZDHE8gZhhLe.htm)|Fist|auto-trad|
|[62IVWtvdiJi9PSZ1.htm](pathfinder-bestiary-items/62IVWtvdiJi9PSZ1.htm)|Attack of Opportunity|auto-trad|
|[62o3Ky65LWW5uKNE.htm](pathfinder-bestiary-items/62o3Ky65LWW5uKNE.htm)|Trample|auto-trad|
|[62TQ4X0N6cfOxwQQ.htm](pathfinder-bestiary-items/62TQ4X0N6cfOxwQQ.htm)|Acid Flask (Lesser)|auto-trad|
|[63a8hHsiAUlNCTdu.htm](pathfinder-bestiary-items/63a8hHsiAUlNCTdu.htm)|Surprise Attacker|auto-trad|
|[63iXwIDFMddnz4kz.htm](pathfinder-bestiary-items/63iXwIDFMddnz4kz.htm)|Eagle Dive|auto-trad|
|[64dsDb6VCwVbCOBD.htm](pathfinder-bestiary-items/64dsDb6VCwVbCOBD.htm)|Jaws|auto-trad|
|[64oqzNrwPutYdBRh.htm](pathfinder-bestiary-items/64oqzNrwPutYdBRh.htm)|Regeneration 20 (Deactivated by Cold or Evil)|auto-trad|
|[65E6IqqYN0IW2HsH.htm](pathfinder-bestiary-items/65E6IqqYN0IW2HsH.htm)|Powerful Charge|auto-trad|
|[67MCs85SzER633da.htm](pathfinder-bestiary-items/67MCs85SzER633da.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[69DgtYVywDs0D5pB.htm](pathfinder-bestiary-items/69DgtYVywDs0D5pB.htm)|Darkvision|auto-trad|
|[6aClhFTvO5NOf1iA.htm](pathfinder-bestiary-items/6aClhFTvO5NOf1iA.htm)|Aura of Misfortune|auto-trad|
|[6aID4H750Zqy0EOH.htm](pathfinder-bestiary-items/6aID4H750Zqy0EOH.htm)|Cold Iron Silver Longsword|auto-trad|
|[6BFQljbOQZkiMjjS.htm](pathfinder-bestiary-items/6BFQljbOQZkiMjjS.htm)|Vulnerable to Sunlight|auto-trad|
|[6bt34Jgx0jymQKsU.htm](pathfinder-bestiary-items/6bt34Jgx0jymQKsU.htm)|Bloodletting|auto-trad|
|[6CWD9zTeiLEB09vN.htm](pathfinder-bestiary-items/6CWD9zTeiLEB09vN.htm)|Fast Healing 2 (While Underground)|auto-trad|
|[6ESE4WVjACGF6VGi.htm](pathfinder-bestiary-items/6ESE4WVjACGF6VGi.htm)|Coven Spells|auto-trad|
|[6evj7LX0P0Xyv3W8.htm](pathfinder-bestiary-items/6evj7LX0P0Xyv3W8.htm)|Arcane Innate Spells|auto-trad|
|[6F6RBqJmz0JUvLc3.htm](pathfinder-bestiary-items/6F6RBqJmz0JUvLc3.htm)|Divine Innate Spells|auto-trad|
|[6f6zqdb6XsTeZuRW.htm](pathfinder-bestiary-items/6f6zqdb6XsTeZuRW.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[6fRxttEYXtfGW1OF.htm](pathfinder-bestiary-items/6fRxttEYXtfGW1OF.htm)|Darkvision|auto-trad|
|[6gO1vT5DmiULZ2Bv.htm](pathfinder-bestiary-items/6gO1vT5DmiULZ2Bv.htm)|Nimble Dodge|auto-trad|
|[6gzE3WOZs1801eyS.htm](pathfinder-bestiary-items/6gzE3WOZs1801eyS.htm)|Acid Spit|auto-trad|
|[6h9jexgK3OdaDWsq.htm](pathfinder-bestiary-items/6h9jexgK3OdaDWsq.htm)|Peace Vulnerability|auto-trad|
|[6hIXdmiCBTVmIryb.htm](pathfinder-bestiary-items/6hIXdmiCBTVmIryb.htm)|At-Will Spells|auto-trad|
|[6HJGU4tyzZ6JeEbD.htm](pathfinder-bestiary-items/6HJGU4tyzZ6JeEbD.htm)|Constant Spells|auto-trad|
|[6HOLWC4q1wPrnZql.htm](pathfinder-bestiary-items/6HOLWC4q1wPrnZql.htm)|Arcane Innate Spells|auto-trad|
|[6HYP2meWyAXuDtrA.htm](pathfinder-bestiary-items/6HYP2meWyAXuDtrA.htm)|Darkvision|auto-trad|
|[6i8TGhbPoCodXsn0.htm](pathfinder-bestiary-items/6i8TGhbPoCodXsn0.htm)|Constant Spells|auto-trad|
|[6ibGu5FoaHhUfj6a.htm](pathfinder-bestiary-items/6ibGu5FoaHhUfj6a.htm)|Darkvision|auto-trad|
|[6ii2IeWpDJ46u3xM.htm](pathfinder-bestiary-items/6ii2IeWpDJ46u3xM.htm)|Rapier|auto-trad|
|[6inoR3FiOAzUbSZ8.htm](pathfinder-bestiary-items/6inoR3FiOAzUbSZ8.htm)|Savage|auto-trad|
|[6iyj26yHFPai9vr3.htm](pathfinder-bestiary-items/6iyj26yHFPai9vr3.htm)|Wind Strike|auto-trad|
|[6JIPvxKV7RTv9wq3.htm](pathfinder-bestiary-items/6JIPvxKV7RTv9wq3.htm)|Low-Light Vision|auto-trad|
|[6MX0hGFGosdch5VS.htm](pathfinder-bestiary-items/6MX0hGFGosdch5VS.htm)|Attack of Opportunity|auto-trad|
|[6N6oDDqKb0CaILEx.htm](pathfinder-bestiary-items/6N6oDDqKb0CaILEx.htm)|Aklys|auto-trad|
|[6nUf7iLY8JoKVMkD.htm](pathfinder-bestiary-items/6nUf7iLY8JoKVMkD.htm)|Rock|auto-trad|
|[6NxJOtjBC8UhEkqr.htm](pathfinder-bestiary-items/6NxJOtjBC8UhEkqr.htm)|Divine Innate Spells|auto-trad|
|[6ORqPbdQEDKtPs55.htm](pathfinder-bestiary-items/6ORqPbdQEDKtPs55.htm)|Grab|auto-trad|
|[6OspJylC319MBk17.htm](pathfinder-bestiary-items/6OspJylC319MBk17.htm)|Change Shape|auto-trad|
|[6pFoOIHjIKB8RSDO.htm](pathfinder-bestiary-items/6pFoOIHjIKB8RSDO.htm)|Darkvision|auto-trad|
|[6pKzCiashjeFYDdw.htm](pathfinder-bestiary-items/6pKzCiashjeFYDdw.htm)|Constant Spells|auto-trad|
|[6pmMLkbrsF7wRVrQ.htm](pathfinder-bestiary-items/6pmMLkbrsF7wRVrQ.htm)|Arcane Prepared Spells|auto-trad|
|[6PogEuLJfheJMDfl.htm](pathfinder-bestiary-items/6PogEuLJfheJMDfl.htm)|Fist|auto-trad|
|[6ptXWWAwrafuOZ7Z.htm](pathfinder-bestiary-items/6ptXWWAwrafuOZ7Z.htm)|Occult Innate Spells|auto-trad|
|[6QbeDmIcvY8S3ldj.htm](pathfinder-bestiary-items/6QbeDmIcvY8S3ldj.htm)|Lifesense 30 feet|auto-trad|
|[6ROvbbPgi1hsjjkb.htm](pathfinder-bestiary-items/6ROvbbPgi1hsjjkb.htm)|Attack of Opportunity|auto-trad|
|[6SbDtVTo4XWUEGO7.htm](pathfinder-bestiary-items/6SbDtVTo4XWUEGO7.htm)|Darkvision|auto-trad|
|[6SHvRrPDTa74t0cD.htm](pathfinder-bestiary-items/6SHvRrPDTa74t0cD.htm)|Rock|auto-trad|
|[6TdllGeax8h6457M.htm](pathfinder-bestiary-items/6TdllGeax8h6457M.htm)|Naturally Invisible|auto-trad|
|[6THNxAaI4CZSj0oW.htm](pathfinder-bestiary-items/6THNxAaI4CZSj0oW.htm)|Pit Fiend Venom|auto-trad|
|[6tSYdMbuSY1NSMOt.htm](pathfinder-bestiary-items/6tSYdMbuSY1NSMOt.htm)|Sunlight Powerlessness|auto-trad|
|[6Upo1ddHWd1c49bj.htm](pathfinder-bestiary-items/6Upo1ddHWd1c49bj.htm)|Arcane Prepared Spells|auto-trad|
|[6UZkK4IZ3193CYxr.htm](pathfinder-bestiary-items/6UZkK4IZ3193CYxr.htm)|Frightful Presence|auto-trad|
|[6V0TnokqVZcRCz7V.htm](pathfinder-bestiary-items/6V0TnokqVZcRCz7V.htm)|Arcane Innate Spells|auto-trad|
|[6vLerwjydBOU4Iv7.htm](pathfinder-bestiary-items/6vLerwjydBOU4Iv7.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[6VrL0WH1rLsYXFpD.htm](pathfinder-bestiary-items/6VrL0WH1rLsYXFpD.htm)|Claw|auto-trad|
|[6WolzdmuMWdkmSpl.htm](pathfinder-bestiary-items/6WolzdmuMWdkmSpl.htm)|Attack of Opportunity|auto-trad|
|[6wXxUMuYVHcPeCm1.htm](pathfinder-bestiary-items/6wXxUMuYVHcPeCm1.htm)|Hatchet|auto-trad|
|[6xP4wJsTXGXVjqRg.htm](pathfinder-bestiary-items/6xP4wJsTXGXVjqRg.htm)|Darkvision|auto-trad|
|[6xrvE4IdhDnQNzDW.htm](pathfinder-bestiary-items/6xrvE4IdhDnQNzDW.htm)|Darkvision|auto-trad|
|[6Y0H1qKcOuLLE2PK.htm](pathfinder-bestiary-items/6Y0H1qKcOuLLE2PK.htm)|Maddening Cacophony|auto-trad|
|[6yBN6jKH0nlzgvOT.htm](pathfinder-bestiary-items/6yBN6jKH0nlzgvOT.htm)|Darkvision|auto-trad|
|[6yZ26OupmzzPO1vZ.htm](pathfinder-bestiary-items/6yZ26OupmzzPO1vZ.htm)|Miasma|auto-trad|
|[6Z8Si7EyPGbAS0VJ.htm](pathfinder-bestiary-items/6Z8Si7EyPGbAS0VJ.htm)|Low-Light Vision|auto-trad|
|[6zbiPHhWaAV5pJd6.htm](pathfinder-bestiary-items/6zbiPHhWaAV5pJd6.htm)|Wriggling Beard|auto-trad|
|[73IYPzz4xQBDqyCv.htm](pathfinder-bestiary-items/73IYPzz4xQBDqyCv.htm)|Divine Rituals|auto-trad|
|[748MxBiZMmMCjxzu.htm](pathfinder-bestiary-items/748MxBiZMmMCjxzu.htm)|Attack of Opportunity|auto-trad|
|[74wDKknM5LCOcK73.htm](pathfinder-bestiary-items/74wDKknM5LCOcK73.htm)|Hears Heartbeats (Imprecise) 60 feet|auto-trad|
|[74Xu2C6KzxtSFcNe.htm](pathfinder-bestiary-items/74Xu2C6KzxtSFcNe.htm)|Two Heads|auto-trad|
|[765hQtSimhXMwS7m.htm](pathfinder-bestiary-items/765hQtSimhXMwS7m.htm)|Wrap in Coils|auto-trad|
|[76JTH3gizL3NZAYl.htm](pathfinder-bestiary-items/76JTH3gizL3NZAYl.htm)|Draconic Momentum|auto-trad|
|[78Qh6xRwrVxslcAx.htm](pathfinder-bestiary-items/78Qh6xRwrVxslcAx.htm)|Shield Block|auto-trad|
|[7aI3LWA9cnoKonHZ.htm](pathfinder-bestiary-items/7aI3LWA9cnoKonHZ.htm)|Divine Innate Spells|auto-trad|
|[7aIGZtbifYYVXpiV.htm](pathfinder-bestiary-items/7aIGZtbifYYVXpiV.htm)|Grab|auto-trad|
|[7bniftpjEDrMSUN2.htm](pathfinder-bestiary-items/7bniftpjEDrMSUN2.htm)|Fast Healing 5|auto-trad|
|[7brI3OuzocwPLZcW.htm](pathfinder-bestiary-items/7brI3OuzocwPLZcW.htm)|Quick Alchemy|auto-trad|
|[7CAtWwnu0VOFzaK9.htm](pathfinder-bestiary-items/7CAtWwnu0VOFzaK9.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[7CcUIlaj6l3irOmm.htm](pathfinder-bestiary-items/7CcUIlaj6l3irOmm.htm)|Telepathy 100 feet|auto-trad|
|[7CjSawx6weIPrIFL.htm](pathfinder-bestiary-items/7CjSawx6weIPrIFL.htm)|Flashing Runes|auto-trad|
|[7CQGpLGBAg2Jbtjv.htm](pathfinder-bestiary-items/7CQGpLGBAg2Jbtjv.htm)|Three Headed|auto-trad|
|[7CwkOMYGSOhef0ns.htm](pathfinder-bestiary-items/7CwkOMYGSOhef0ns.htm)|Breath Weapon|auto-trad|
|[7DB79rjng9ShAwNU.htm](pathfinder-bestiary-items/7DB79rjng9ShAwNU.htm)|Infrasonic Moan|auto-trad|
|[7DhYY8OTjN08dR6B.htm](pathfinder-bestiary-items/7DhYY8OTjN08dR6B.htm)|Darkvision|auto-trad|
|[7ECMLvSVWJWwwgKi.htm](pathfinder-bestiary-items/7ECMLvSVWJWwwgKi.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[7EDZA6PRAkgmOYfB.htm](pathfinder-bestiary-items/7EDZA6PRAkgmOYfB.htm)|All-Around Vision|auto-trad|
|[7EVdDztqRMJiEptJ.htm](pathfinder-bestiary-items/7EVdDztqRMJiEptJ.htm)|Coven Ritual|auto-trad|
|[7h8dpLvJlgmuZE2b.htm](pathfinder-bestiary-items/7h8dpLvJlgmuZE2b.htm)|Darkvision|auto-trad|
|[7HbtTKCzPMHlRdFT.htm](pathfinder-bestiary-items/7HbtTKCzPMHlRdFT.htm)|Tail|auto-trad|
|[7Hcv7QQPQAQcct0I.htm](pathfinder-bestiary-items/7Hcv7QQPQAQcct0I.htm)|Breath Weapon|auto-trad|
|[7HCvV8Nrw6EphM2a.htm](pathfinder-bestiary-items/7HCvV8Nrw6EphM2a.htm)|Arcane Innate Spells|auto-trad|
|[7hIKn4hwekvsOCAW.htm](pathfinder-bestiary-items/7hIKn4hwekvsOCAW.htm)|Aklys|auto-trad|
|[7hUX8z2syN9gmg7D.htm](pathfinder-bestiary-items/7hUX8z2syN9gmg7D.htm)|Tail|auto-trad|
|[7HZont6X72rI49Ax.htm](pathfinder-bestiary-items/7HZont6X72rI49Ax.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[7I0I09vYwkMcNZoL.htm](pathfinder-bestiary-items/7I0I09vYwkMcNZoL.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[7ioQp451JDOrH2ZM.htm](pathfinder-bestiary-items/7ioQp451JDOrH2ZM.htm)|Throw Rock|auto-trad|
|[7ivzzqnDwOSXosG5.htm](pathfinder-bestiary-items/7ivzzqnDwOSXosG5.htm)|Delayed Suggestion|auto-trad|
|[7IWkM6MOEOAu4DTP.htm](pathfinder-bestiary-items/7IWkM6MOEOAu4DTP.htm)|Tree Meld|auto-trad|
|[7Iww6mLyrRcTXqWv.htm](pathfinder-bestiary-items/7Iww6mLyrRcTXqWv.htm)|Halberd|auto-trad|
|[7J6yDmYeMAWvM46S.htm](pathfinder-bestiary-items/7J6yDmYeMAWvM46S.htm)|Sound Imitation|auto-trad|
|[7JcMtxcKSyHdqEQX.htm](pathfinder-bestiary-items/7JcMtxcKSyHdqEQX.htm)|Spitting Rage|auto-trad|
|[7JJXsi69PzGUt6UR.htm](pathfinder-bestiary-items/7JJXsi69PzGUt6UR.htm)|Attack of Opportunity|auto-trad|
|[7KN50dFHLzPdjJNH.htm](pathfinder-bestiary-items/7KN50dFHLzPdjJNH.htm)|Occult Innate Spells|auto-trad|
|[7KT3DPupZMzKyxiY.htm](pathfinder-bestiary-items/7KT3DPupZMzKyxiY.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[7KT3KfzD3HVUhRjt.htm](pathfinder-bestiary-items/7KT3KfzD3HVUhRjt.htm)|Darkvision|auto-trad|
|[7LQPJRBvruncsAz1.htm](pathfinder-bestiary-items/7LQPJRBvruncsAz1.htm)|Horns|auto-trad|
|[7lto9FCIXzooWeAW.htm](pathfinder-bestiary-items/7lto9FCIXzooWeAW.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[7MCn4tFozydZTxeL.htm](pathfinder-bestiary-items/7MCn4tFozydZTxeL.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[7nAuNsDTN5NBtC7a.htm](pathfinder-bestiary-items/7nAuNsDTN5NBtC7a.htm)|Darkvision|auto-trad|
|[7O6Ud0aHDjF3hlFD.htm](pathfinder-bestiary-items/7O6Ud0aHDjF3hlFD.htm)|Infest Environs|auto-trad|
|[7ODNVUAaNraSivsT.htm](pathfinder-bestiary-items/7ODNVUAaNraSivsT.htm)|Shortbow|auto-trad|
|[7ogTgoQe4WAvtdJ7.htm](pathfinder-bestiary-items/7ogTgoQe4WAvtdJ7.htm)|Light Blindness|auto-trad|
|[7P4VEdy4KccmQ5Gp.htm](pathfinder-bestiary-items/7P4VEdy4KccmQ5Gp.htm)|Echolocation (Precise) 20 feet|auto-trad|
|[7pkevpR7Am9eNCD5.htm](pathfinder-bestiary-items/7pkevpR7Am9eNCD5.htm)|Swallow Whole|auto-trad|
|[7PQkG1Vfc7aynb2X.htm](pathfinder-bestiary-items/7PQkG1Vfc7aynb2X.htm)|Frightening Display|auto-trad|
|[7pRxaQFmTGlbnKEr.htm](pathfinder-bestiary-items/7pRxaQFmTGlbnKEr.htm)|Darkvision|auto-trad|
|[7PTn0lPspj6Mo8E7.htm](pathfinder-bestiary-items/7PTn0lPspj6Mo8E7.htm)|Darkvision|auto-trad|
|[7pYPgvPhNqIQ9RwA.htm](pathfinder-bestiary-items/7pYPgvPhNqIQ9RwA.htm)|Improved Knockdown|auto-trad|
|[7q0tz0RZaa6kAE5S.htm](pathfinder-bestiary-items/7q0tz0RZaa6kAE5S.htm)|Constant Spells|auto-trad|
|[7Q1l6xgw6nJEEbEr.htm](pathfinder-bestiary-items/7Q1l6xgw6nJEEbEr.htm)|Light Blindness|auto-trad|
|[7qULZEHZX6JmtrVP.htm](pathfinder-bestiary-items/7qULZEHZX6JmtrVP.htm)|Breath Weapon|auto-trad|
|[7rqZnXnqggRlsoMe.htm](pathfinder-bestiary-items/7rqZnXnqggRlsoMe.htm)|Darkvision|auto-trad|
|[7RTjFlVzsxhjtwX4.htm](pathfinder-bestiary-items/7RTjFlVzsxhjtwX4.htm)|Consume Flesh|auto-trad|
|[7SNUgV6SLqC5TzXJ.htm](pathfinder-bestiary-items/7SNUgV6SLqC5TzXJ.htm)|Greater Darkvision|auto-trad|
|[7swXlOjSl2oHLuDv.htm](pathfinder-bestiary-items/7swXlOjSl2oHLuDv.htm)|Horsechopper|auto-trad|
|[7Tl0EsClc5Q8xh8G.htm](pathfinder-bestiary-items/7Tl0EsClc5Q8xh8G.htm)|Jaws|auto-trad|
|[7tWhbtD4uoAp2v5H.htm](pathfinder-bestiary-items/7tWhbtD4uoAp2v5H.htm)|Attack of Opportunity|auto-trad|
|[7WxioYo3ZgMSuL5O.htm](pathfinder-bestiary-items/7WxioYo3ZgMSuL5O.htm)|Leg|auto-trad|
|[7XdX39vruCh59MIe.htm](pathfinder-bestiary-items/7XdX39vruCh59MIe.htm)|Divine Revulsion|auto-trad|
|[7XIYM7XKRyeIRUn6.htm](pathfinder-bestiary-items/7XIYM7XKRyeIRUn6.htm)|Snow Vision|auto-trad|
|[7xKIhg7lon987ZWq.htm](pathfinder-bestiary-items/7xKIhg7lon987ZWq.htm)|Arcane Prepared Spells|auto-trad|
|[7XN2uoXvAP0XCvYn.htm](pathfinder-bestiary-items/7XN2uoXvAP0XCvYn.htm)|Holy Blade|auto-trad|
|[7Z1lYeu3xHpx2SPZ.htm](pathfinder-bestiary-items/7Z1lYeu3xHpx2SPZ.htm)|Claw|auto-trad|
|[7ZnWKFvAT28mjh65.htm](pathfinder-bestiary-items/7ZnWKFvAT28mjh65.htm)|Attack of Opportunity|auto-trad|
|[7ZZXXt4mJ5JwYsKh.htm](pathfinder-bestiary-items/7ZZXXt4mJ5JwYsKh.htm)|Claw|auto-trad|
|[80b2NmnlvaPumpDV.htm](pathfinder-bestiary-items/80b2NmnlvaPumpDV.htm)|Earthbound|auto-trad|
|[80CuEcGtDQ6fUIyY.htm](pathfinder-bestiary-items/80CuEcGtDQ6fUIyY.htm)|Drowning Drone|auto-trad|
|[80jq5AttlXMgdD6Q.htm](pathfinder-bestiary-items/80jq5AttlXMgdD6Q.htm)|Arcane Prepared Spells|auto-trad|
|[80wSAXH5wAj5t4fT.htm](pathfinder-bestiary-items/80wSAXH5wAj5t4fT.htm)|Darkvision|auto-trad|
|[81HaFjfnZXozTasw.htm](pathfinder-bestiary-items/81HaFjfnZXozTasw.htm)|Low-Light Vision|auto-trad|
|[81RWhoONaToFh2uz.htm](pathfinder-bestiary-items/81RWhoONaToFh2uz.htm)|Claw|auto-trad|
|[82cDSepgqLoFDjw8.htm](pathfinder-bestiary-items/82cDSepgqLoFDjw8.htm)|Frightful Presence|auto-trad|
|[82HFjbpYVotosFsA.htm](pathfinder-bestiary-items/82HFjbpYVotosFsA.htm)|Regeneration 50 (Deactivated by Sonic)|auto-trad|
|[82KaMhK0BRaEZMbn.htm](pathfinder-bestiary-items/82KaMhK0BRaEZMbn.htm)|Retributive Strike|auto-trad|
|[83qHKEXcHOEaa6iS.htm](pathfinder-bestiary-items/83qHKEXcHOEaa6iS.htm)|Absorb|auto-trad|
|[83QmDBGigZxaHbbd.htm](pathfinder-bestiary-items/83QmDBGigZxaHbbd.htm)|Darkvision|auto-trad|
|[860MOhDdvHc8KanP.htm](pathfinder-bestiary-items/860MOhDdvHc8KanP.htm)|Hand|auto-trad|
|[86ZM2qT4ANthqpil.htm](pathfinder-bestiary-items/86ZM2qT4ANthqpil.htm)|Constant Spells|auto-trad|
|[871udt0tIPBiUS63.htm](pathfinder-bestiary-items/871udt0tIPBiUS63.htm)|Defoliation|auto-trad|
|[88O2MErrb1dOjHJh.htm](pathfinder-bestiary-items/88O2MErrb1dOjHJh.htm)|Curse of Frost|auto-trad|
|[8akVcjr4Y8JZR5Ht.htm](pathfinder-bestiary-items/8akVcjr4Y8JZR5Ht.htm)|Club|auto-trad|
|[8av6UdLVMPok6hmO.htm](pathfinder-bestiary-items/8av6UdLVMPok6hmO.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[8Aw8YNHiQ1odM8VY.htm](pathfinder-bestiary-items/8Aw8YNHiQ1odM8VY.htm)|Primal Prepared Spells|auto-trad|
|[8aYfDhAzJOdFBLKs.htm](pathfinder-bestiary-items/8aYfDhAzJOdFBLKs.htm)|At-Will Spells|auto-trad|
|[8B4xwRLas4rp2NnR.htm](pathfinder-bestiary-items/8B4xwRLas4rp2NnR.htm)|Falchion|auto-trad|
|[8bDtJfwyHARgU5uM.htm](pathfinder-bestiary-items/8bDtJfwyHARgU5uM.htm)|Darkvision|auto-trad|
|[8cb0jB7NP8JMjdkx.htm](pathfinder-bestiary-items/8cb0jB7NP8JMjdkx.htm)|Echolocation (Precise) 40 feet|auto-trad|
|[8cJQY3xEuodUaxz6.htm](pathfinder-bestiary-items/8cJQY3xEuodUaxz6.htm)|Harmonizing Aura|auto-trad|
|[8CNN6le6tEVrWmR5.htm](pathfinder-bestiary-items/8CNN6le6tEVrWmR5.htm)|Scythe|auto-trad|
|[8cpYftgTlNcCeNyN.htm](pathfinder-bestiary-items/8cpYftgTlNcCeNyN.htm)|Tail|auto-trad|
|[8CYcAwzfjTavahvb.htm](pathfinder-bestiary-items/8CYcAwzfjTavahvb.htm)|Constrict|auto-trad|
|[8DJ83sSlRno4pkws.htm](pathfinder-bestiary-items/8DJ83sSlRno4pkws.htm)|Arcane Innate Spells|auto-trad|
|[8EmRJwYnesD02GOh.htm](pathfinder-bestiary-items/8EmRJwYnesD02GOh.htm)|Scimitar|auto-trad|
|[8Eq3jy4LyDtP6mLP.htm](pathfinder-bestiary-items/8Eq3jy4LyDtP6mLP.htm)|Starlight Ray|auto-trad|
|[8EY2qfk7YZdvckfJ.htm](pathfinder-bestiary-items/8EY2qfk7YZdvckfJ.htm)|Rock Tunneler|auto-trad|
|[8EYAIEzeZ8GJ1Diq.htm](pathfinder-bestiary-items/8EYAIEzeZ8GJ1Diq.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[8f0fnlt5StWq0i9j.htm](pathfinder-bestiary-items/8f0fnlt5StWq0i9j.htm)|Catch Rock|auto-trad|
|[8fKFzS7zqR9XbC5k.htm](pathfinder-bestiary-items/8fKFzS7zqR9XbC5k.htm)|Curse of the Werebear|auto-trad|
|[8H68KpZMl3WGb1NP.htm](pathfinder-bestiary-items/8H68KpZMl3WGb1NP.htm)|Attack of Opportunity|auto-trad|
|[8H7QxUhMXhZWzFdG.htm](pathfinder-bestiary-items/8H7QxUhMXhZWzFdG.htm)|Darkvision|auto-trad|
|[8Hc25NUrYBwSHQuO.htm](pathfinder-bestiary-items/8Hc25NUrYBwSHQuO.htm)|Sneak Attack|auto-trad|
|[8jF0NZz94zAMhCHA.htm](pathfinder-bestiary-items/8jF0NZz94zAMhCHA.htm)|Spores|auto-trad|
|[8JKL1RJJmQI5QjnF.htm](pathfinder-bestiary-items/8JKL1RJJmQI5QjnF.htm)|Tentacle|auto-trad|
|[8JqF84SJfKy5TmkS.htm](pathfinder-bestiary-items/8JqF84SJfKy5TmkS.htm)|Draconic Momentum|auto-trad|
|[8JslG7YLjptMOVC0.htm](pathfinder-bestiary-items/8JslG7YLjptMOVC0.htm)|Sudden Betrayal|auto-trad|
|[8jwjSmQRQW8Nk5NY.htm](pathfinder-bestiary-items/8jwjSmQRQW8Nk5NY.htm)|Fist|auto-trad|
|[8KbESiYnn2ngs6MZ.htm](pathfinder-bestiary-items/8KbESiYnn2ngs6MZ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[8ks0Roogu62RATk0.htm](pathfinder-bestiary-items/8ks0Roogu62RATk0.htm)|Darkvision|auto-trad|
|[8LvVStEHJB30KVCE.htm](pathfinder-bestiary-items/8LvVStEHJB30KVCE.htm)|Terrifying Croak|auto-trad|
|[8LYUZymM9ZN8WDw9.htm](pathfinder-bestiary-items/8LYUZymM9ZN8WDw9.htm)|Claw|auto-trad|
|[8MiIAzmA9gWbpF5s.htm](pathfinder-bestiary-items/8MiIAzmA9gWbpF5s.htm)|Breath Weapon|auto-trad|
|[8MSWR1BxkhlhSm54.htm](pathfinder-bestiary-items/8MSWR1BxkhlhSm54.htm)|Catch Rock|auto-trad|
|[8oiunFF5LcYpynHM.htm](pathfinder-bestiary-items/8oiunFF5LcYpynHM.htm)|Grab|auto-trad|
|[8PkUYYXKF3YdJiBM.htm](pathfinder-bestiary-items/8PkUYYXKF3YdJiBM.htm)|Draconic Bite|auto-trad|
|[8R3jmOcyIfgNNdKr.htm](pathfinder-bestiary-items/8R3jmOcyIfgNNdKr.htm)|Negative Healing|auto-trad|
|[8RIFanFu6JDiIRH1.htm](pathfinder-bestiary-items/8RIFanFu6JDiIRH1.htm)|Thrash|auto-trad|
|[8SogXDgTbT7ghBVs.htm](pathfinder-bestiary-items/8SogXDgTbT7ghBVs.htm)|Jaws|auto-trad|
|[8u1Hcx3t6JzOkXtF.htm](pathfinder-bestiary-items/8u1Hcx3t6JzOkXtF.htm)|Fast Healing 10|auto-trad|
|[8uoOHgzV0mRLhzhE.htm](pathfinder-bestiary-items/8uoOHgzV0mRLhzhE.htm)|Tree Dependent|auto-trad|
|[8VCoSCtEbUqhtmnI.htm](pathfinder-bestiary-items/8VCoSCtEbUqhtmnI.htm)|Swallow Whole|auto-trad|
|[8W4is1CtFAUBw0Zd.htm](pathfinder-bestiary-items/8W4is1CtFAUBw0Zd.htm)|Sneak Attack|auto-trad|
|[8W67Bxaonilv5KO0.htm](pathfinder-bestiary-items/8W67Bxaonilv5KO0.htm)|Exhale Miasma|auto-trad|
|[8wBdJ7S9sRjGCmmT.htm](pathfinder-bestiary-items/8wBdJ7S9sRjGCmmT.htm)|Arcane Innate Spells|auto-trad|
|[8WeL4lERSooxfBXE.htm](pathfinder-bestiary-items/8WeL4lERSooxfBXE.htm)|Breath Weapon|auto-trad|
|[8WFwsPMOCNHrJBF8.htm](pathfinder-bestiary-items/8WFwsPMOCNHrJBF8.htm)|Blood Frenzy|auto-trad|
|[8WMc08Sjn5CVzmDo.htm](pathfinder-bestiary-items/8WMc08Sjn5CVzmDo.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[8wxpM0AIu2KtOOy1.htm](pathfinder-bestiary-items/8wxpM0AIu2KtOOy1.htm)|Terrain Advantage|auto-trad|
|[8wYxLklNMjT1ZPCT.htm](pathfinder-bestiary-items/8wYxLklNMjT1ZPCT.htm)|Telepathy 100 feet|auto-trad|
|[8x287oYylzurAA7X.htm](pathfinder-bestiary-items/8x287oYylzurAA7X.htm)|Jaws|auto-trad|
|[8xYRzzHeY6uZztPe.htm](pathfinder-bestiary-items/8xYRzzHeY6uZztPe.htm)|Unstoppable Charge|auto-trad|
|[8yN0Tb58TCHXqGYI.htm](pathfinder-bestiary-items/8yN0Tb58TCHXqGYI.htm)|Arcane Spontaneous Spells|auto-trad|
|[8yXu3fI5NfUS8lDH.htm](pathfinder-bestiary-items/8yXu3fI5NfUS8lDH.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[8ZY3GiKagZnMX8T1.htm](pathfinder-bestiary-items/8ZY3GiKagZnMX8T1.htm)|High Winds|auto-trad|
|[90eQFUneXcsHeKSK.htm](pathfinder-bestiary-items/90eQFUneXcsHeKSK.htm)|Staff|auto-trad|
|[91Bz1Au1eeA0F5Kc.htm](pathfinder-bestiary-items/91Bz1Au1eeA0F5Kc.htm)|Breach|auto-trad|
|[91iOrkVbzeP7cK2M.htm](pathfinder-bestiary-items/91iOrkVbzeP7cK2M.htm)|Adhesive|auto-trad|
|[93n1e9vtLuuwjXGm.htm](pathfinder-bestiary-items/93n1e9vtLuuwjXGm.htm)|Divine Rituals|auto-trad|
|[94oa0RrcsKbas3ys.htm](pathfinder-bestiary-items/94oa0RrcsKbas3ys.htm)|Divine Innate Spells|auto-trad|
|[95k2q11INtOhFqCd.htm](pathfinder-bestiary-items/95k2q11INtOhFqCd.htm)|Double Attack|auto-trad|
|[95kvqPPSJNio8vAK.htm](pathfinder-bestiary-items/95kvqPPSJNio8vAK.htm)|Berserk|auto-trad|
|[96XPNyAYt1NHy3H5.htm](pathfinder-bestiary-items/96XPNyAYt1NHy3H5.htm)|Blood Leech|auto-trad|
|[9750NAX3RoGRTvAW.htm](pathfinder-bestiary-items/9750NAX3RoGRTvAW.htm)|Claw|auto-trad|
|[977xdYIDaM8Nrj7X.htm](pathfinder-bestiary-items/977xdYIDaM8Nrj7X.htm)|Tail|auto-trad|
|[97U65T0kh2mLB67Q.htm](pathfinder-bestiary-items/97U65T0kh2mLB67Q.htm)|Darkvision|auto-trad|
|[98fcfAaERbR2T29q.htm](pathfinder-bestiary-items/98fcfAaERbR2T29q.htm)|Overwhelming Light|auto-trad|
|[99jBJtnLmD48VOSS.htm](pathfinder-bestiary-items/99jBJtnLmD48VOSS.htm)|Claw|auto-trad|
|[99NmF28aoamnNDi6.htm](pathfinder-bestiary-items/99NmF28aoamnNDi6.htm)|Divine Innate Spells|auto-trad|
|[9Aa0Gupq2BHChPd3.htm](pathfinder-bestiary-items/9Aa0Gupq2BHChPd3.htm)|Greataxe|auto-trad|
|[9AEzHj5y2T93e7EF.htm](pathfinder-bestiary-items/9AEzHj5y2T93e7EF.htm)|Regurgitate|auto-trad|
|[9aYwSAhxxa0rnngi.htm](pathfinder-bestiary-items/9aYwSAhxxa0rnngi.htm)|Tail|auto-trad|
|[9B8Sr2aqKRJttXog.htm](pathfinder-bestiary-items/9B8Sr2aqKRJttXog.htm)|Constant Spells|auto-trad|
|[9boiwlXx7yggBu6F.htm](pathfinder-bestiary-items/9boiwlXx7yggBu6F.htm)|Abyssal Healing|auto-trad|
|[9bomxaBEHSLvRrkq.htm](pathfinder-bestiary-items/9bomxaBEHSLvRrkq.htm)|Statue|auto-trad|
|[9D1QwW0n9RRjhkh0.htm](pathfinder-bestiary-items/9D1QwW0n9RRjhkh0.htm)|Frightful Presence|auto-trad|
|[9dhNquuh2uBurLft.htm](pathfinder-bestiary-items/9dhNquuh2uBurLft.htm)|Low-Light Vision|auto-trad|
|[9DJr6sgB54eTLxDn.htm](pathfinder-bestiary-items/9DJr6sgB54eTLxDn.htm)|Hoof|auto-trad|
|[9EIwBDwTdZ56ngZa.htm](pathfinder-bestiary-items/9EIwBDwTdZ56ngZa.htm)|Jaws|auto-trad|
|[9ewh7tMyC0NRIpZx.htm](pathfinder-bestiary-items/9ewh7tMyC0NRIpZx.htm)|Drain Bonded Item|auto-trad|
|[9falExFfOIxN5EKp.htm](pathfinder-bestiary-items/9falExFfOIxN5EKp.htm)|Stench|auto-trad|
|[9FQ3eh9wkGZD4b1O.htm](pathfinder-bestiary-items/9FQ3eh9wkGZD4b1O.htm)|Retributive Strike|auto-trad|
|[9g6k1mFztcJlfe5v.htm](pathfinder-bestiary-items/9g6k1mFztcJlfe5v.htm)|Frightful Presence|auto-trad|
|[9Gpl3fy3X5489jJF.htm](pathfinder-bestiary-items/9Gpl3fy3X5489jJF.htm)|Inferno Leap|auto-trad|
|[9gYASQQf328TmVpW.htm](pathfinder-bestiary-items/9gYASQQf328TmVpW.htm)|Pin Prey|auto-trad|
|[9h7bBppeEIWnTtJq.htm](pathfinder-bestiary-items/9h7bBppeEIWnTtJq.htm)|At-Will Spells|auto-trad|
|[9hjHckpMu7NDpfRe.htm](pathfinder-bestiary-items/9hjHckpMu7NDpfRe.htm)|At-Will Spells|auto-trad|
|[9il80y0VHh8ZAWdY.htm](pathfinder-bestiary-items/9il80y0VHh8ZAWdY.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[9InxDbn3EL8Jsg7a.htm](pathfinder-bestiary-items/9InxDbn3EL8Jsg7a.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[9IuPTxCvRCR7HHp2.htm](pathfinder-bestiary-items/9IuPTxCvRCR7HHp2.htm)|Light Blindness|auto-trad|
|[9iYmbVoVyCe6SNu9.htm](pathfinder-bestiary-items/9iYmbVoVyCe6SNu9.htm)|Coffin Restoration|auto-trad|
|[9ja6Q3mPufB7rxnt.htm](pathfinder-bestiary-items/9ja6Q3mPufB7rxnt.htm)|Running Reload|auto-trad|
|[9jVKyOMk9msVj1hz.htm](pathfinder-bestiary-items/9jVKyOMk9msVj1hz.htm)|Capsize|auto-trad|
|[9KaWgpJiexo6SDru.htm](pathfinder-bestiary-items/9KaWgpJiexo6SDru.htm)|At-Will Spells|auto-trad|
|[9KbMP0mPbueRMViR.htm](pathfinder-bestiary-items/9KbMP0mPbueRMViR.htm)|Lightning Crash|auto-trad|
|[9MKpsapkMcaRXfuH.htm](pathfinder-bestiary-items/9MKpsapkMcaRXfuH.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[9nfzrmPzAAStRjAe.htm](pathfinder-bestiary-items/9nfzrmPzAAStRjAe.htm)|Quick Draw|auto-trad|
|[9NSBH5zk6mEIcPjF.htm](pathfinder-bestiary-items/9NSBH5zk6mEIcPjF.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[9OItti667EywItzF.htm](pathfinder-bestiary-items/9OItti667EywItzF.htm)|Favored Prey|auto-trad|
|[9OiXFTpXGJPjEwUL.htm](pathfinder-bestiary-items/9OiXFTpXGJPjEwUL.htm)|Grab|auto-trad|
|[9oO34owbXFTjCGng.htm](pathfinder-bestiary-items/9oO34owbXFTjCGng.htm)|Darkvision|auto-trad|
|[9oOOWjBu08zVDEgp.htm](pathfinder-bestiary-items/9oOOWjBu08zVDEgp.htm)|Greatsword|auto-trad|
|[9OZuMP6qLeoyehVx.htm](pathfinder-bestiary-items/9OZuMP6qLeoyehVx.htm)|Nymph's Beauty|auto-trad|
|[9RDkmFyfywWSOj2V.htm](pathfinder-bestiary-items/9RDkmFyfywWSOj2V.htm)|Breath Weapon|auto-trad|
|[9ROgeFg0LqFYjWto.htm](pathfinder-bestiary-items/9ROgeFg0LqFYjWto.htm)|Awaken Tree|auto-trad|
|[9RRqDOZEOM8rUI1P.htm](pathfinder-bestiary-items/9RRqDOZEOM8rUI1P.htm)|Pack Attack|auto-trad|
|[9S7ODMh2ZM6NXVpF.htm](pathfinder-bestiary-items/9S7ODMh2ZM6NXVpF.htm)|Enfeebling Humors|auto-trad|
|[9SboqcQona3PXQIx.htm](pathfinder-bestiary-items/9SboqcQona3PXQIx.htm)|Destructive Croak|auto-trad|
|[9SCUupqh4itf7aTx.htm](pathfinder-bestiary-items/9SCUupqh4itf7aTx.htm)|Brain Blisters|auto-trad|
|[9TCF9GcaMoTKkPSj.htm](pathfinder-bestiary-items/9TCF9GcaMoTKkPSj.htm)|Paralysis|auto-trad|
|[9tXYGUH9QFkHOzla.htm](pathfinder-bestiary-items/9tXYGUH9QFkHOzla.htm)|Composite Longbow|auto-trad|
|[9UvuMlLHVEKID1Jq.htm](pathfinder-bestiary-items/9UvuMlLHVEKID1Jq.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[9V1gE8pVVGhGFEWQ.htm](pathfinder-bestiary-items/9V1gE8pVVGhGFEWQ.htm)|Darkvision|auto-trad|
|[9v2mzebQFMTkRDd9.htm](pathfinder-bestiary-items/9v2mzebQFMTkRDd9.htm)|Jaws|auto-trad|
|[9VHbtuY8gJuurG3U.htm](pathfinder-bestiary-items/9VHbtuY8gJuurG3U.htm)|Draconic Momentum|auto-trad|
|[9wiqhTppGBReF5MG.htm](pathfinder-bestiary-items/9wiqhTppGBReF5MG.htm)|Trample|auto-trad|
|[9yPuatISy0aUTsVE.htm](pathfinder-bestiary-items/9yPuatISy0aUTsVE.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[9YuL1pu4ZHOAaxcZ.htm](pathfinder-bestiary-items/9YuL1pu4ZHOAaxcZ.htm)|Spore Cloud|auto-trad|
|[9yzexwwuWU0ABr5D.htm](pathfinder-bestiary-items/9yzexwwuWU0ABr5D.htm)|Tail|auto-trad|
|[A0HAbzH4Gjn59ZLw.htm](pathfinder-bestiary-items/A0HAbzH4Gjn59ZLw.htm)|Deep Breath|auto-trad|
|[a0noDtcOBbRDMiow.htm](pathfinder-bestiary-items/a0noDtcOBbRDMiow.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[a0WPpTOOGc0FAn2o.htm](pathfinder-bestiary-items/a0WPpTOOGc0FAn2o.htm)|Swarm Mind|auto-trad|
|[a1diHlUPKBt1e8ZM.htm](pathfinder-bestiary-items/a1diHlUPKBt1e8ZM.htm)|Frightful Presence|auto-trad|
|[a2pZeef5wn9y5xAA.htm](pathfinder-bestiary-items/a2pZeef5wn9y5xAA.htm)|Slow|auto-trad|
|[a2vmCuPpbC3JaPX5.htm](pathfinder-bestiary-items/a2vmCuPpbC3JaPX5.htm)|Club|auto-trad|
|[A33mGw1DCgRJRDSe.htm](pathfinder-bestiary-items/A33mGw1DCgRJRDSe.htm)|Tongue Grab|auto-trad|
|[a3oRCd6Fs4j19XDe.htm](pathfinder-bestiary-items/a3oRCd6Fs4j19XDe.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[a3pejXzpAHLXkYWu.htm](pathfinder-bestiary-items/a3pejXzpAHLXkYWu.htm)|Greater Darkvision|auto-trad|
|[a4bag8JDNGO7xCiD.htm](pathfinder-bestiary-items/a4bag8JDNGO7xCiD.htm)|Savage|auto-trad|
|[a4I77q6CzdLCZCG6.htm](pathfinder-bestiary-items/a4I77q6CzdLCZCG6.htm)|Shortbow|auto-trad|
|[A4Iy8pTKRCuXe0bh.htm](pathfinder-bestiary-items/A4Iy8pTKRCuXe0bh.htm)|At-Will Spells|auto-trad|
|[a4Zz1r0Kbk6SA66t.htm](pathfinder-bestiary-items/a4Zz1r0Kbk6SA66t.htm)|Spear|auto-trad|
|[A53fmiOtIr4DZqy0.htm](pathfinder-bestiary-items/A53fmiOtIr4DZqy0.htm)|Pack Attack|auto-trad|
|[A6gmij9OXIW0o9vj.htm](pathfinder-bestiary-items/A6gmij9OXIW0o9vj.htm)|Giant Centipede Venom|auto-trad|
|[A7AIAW82kyBDKhWn.htm](pathfinder-bestiary-items/A7AIAW82kyBDKhWn.htm)|Quick Capture|auto-trad|
|[a7b4jERQkCw4Wcb8.htm](pathfinder-bestiary-items/a7b4jERQkCw4Wcb8.htm)|Tail|auto-trad|
|[a8iHr4CAafw5H0VD.htm](pathfinder-bestiary-items/a8iHr4CAafw5H0VD.htm)|Occult Innate Spells|auto-trad|
|[A8YjaMzEWbFC9JWF.htm](pathfinder-bestiary-items/A8YjaMzEWbFC9JWF.htm)|Grab|auto-trad|
|[a8yy3kqZmWzMBKci.htm](pathfinder-bestiary-items/a8yy3kqZmWzMBKci.htm)|Brain Collector Venom|auto-trad|
|[A97t9GcQF8k7V7By.htm](pathfinder-bestiary-items/A97t9GcQF8k7V7By.htm)|Claw|auto-trad|
|[a9hdRp8sRfsMk1cJ.htm](pathfinder-bestiary-items/a9hdRp8sRfsMk1cJ.htm)|Draconic Momentum|auto-trad|
|[a9nhBEqNFJ9N1rH5.htm](pathfinder-bestiary-items/a9nhBEqNFJ9N1rH5.htm)|Barbed Leg|auto-trad|
|[a9UivdapXwh89X3a.htm](pathfinder-bestiary-items/a9UivdapXwh89X3a.htm)|Rock|auto-trad|
|[AAFWcmRlGxb76Pps.htm](pathfinder-bestiary-items/AAFWcmRlGxb76Pps.htm)|Occult Innate Spells|auto-trad|
|[aAGnyUk4Vj6iSM7n.htm](pathfinder-bestiary-items/aAGnyUk4Vj6iSM7n.htm)|Spring Upon Prey|auto-trad|
|[aat9fAhVKzDZsfyl.htm](pathfinder-bestiary-items/aat9fAhVKzDZsfyl.htm)|Powerful Jumper|auto-trad|
|[AazKaSGAUXE2To0x.htm](pathfinder-bestiary-items/AazKaSGAUXE2To0x.htm)|Darkvision|auto-trad|
|[aB9m1CJfYd8FCIEx.htm](pathfinder-bestiary-items/aB9m1CJfYd8FCIEx.htm)|Tail|auto-trad|
|[AbbMWup6eJg0RC0O.htm](pathfinder-bestiary-items/AbbMWup6eJg0RC0O.htm)|Low-Light Vision|auto-trad|
|[ABE8at8xt3SIvjqx.htm](pathfinder-bestiary-items/ABE8at8xt3SIvjqx.htm)|Implant Core|auto-trad|
|[abEaFObv46UrcGMU.htm](pathfinder-bestiary-items/abEaFObv46UrcGMU.htm)|Aquatic Ambush|auto-trad|
|[abrvwmCxto1mr7Xu.htm](pathfinder-bestiary-items/abrvwmCxto1mr7Xu.htm)|Sand Glide|auto-trad|
|[acAG7Wir41UpIJRx.htm](pathfinder-bestiary-items/acAG7Wir41UpIJRx.htm)|Occult Innate Spells|auto-trad|
|[AcBYEH0GxgM4iV5v.htm](pathfinder-bestiary-items/AcBYEH0GxgM4iV5v.htm)|Ice Tunneler|auto-trad|
|[AcetPc7srVrIa4vs.htm](pathfinder-bestiary-items/AcetPc7srVrIa4vs.htm)|Attack of Opportunity (Special)|auto-trad|
|[achgTT7jV4XPMG5S.htm](pathfinder-bestiary-items/achgTT7jV4XPMG5S.htm)|Draconic Momentum|auto-trad|
|[ACHlnYzXVxizzqPw.htm](pathfinder-bestiary-items/ACHlnYzXVxizzqPw.htm)|Light Vulnerability|auto-trad|
|[aCvMYE328wAIa1Kx.htm](pathfinder-bestiary-items/aCvMYE328wAIa1Kx.htm)|Dagger|auto-trad|
|[ACyh3o79qXzSiRlS.htm](pathfinder-bestiary-items/ACyh3o79qXzSiRlS.htm)|Coven Ritual|auto-trad|
|[aD6MGcEF5IHZE5vn.htm](pathfinder-bestiary-items/aD6MGcEF5IHZE5vn.htm)|Whirlwind|auto-trad|
|[ade7S9RWaPXF7KmF.htm](pathfinder-bestiary-items/ade7S9RWaPXF7KmF.htm)|Darkvision|auto-trad|
|[ADJpet8hkChh9ljl.htm](pathfinder-bestiary-items/ADJpet8hkChh9ljl.htm)|Earth Glide|auto-trad|
|[ADmvv8bwerUunNDn.htm](pathfinder-bestiary-items/ADmvv8bwerUunNDn.htm)|Negative Healing|auto-trad|
|[Adq96cMm6jUHjnw3.htm](pathfinder-bestiary-items/Adq96cMm6jUHjnw3.htm)|At-Will Spells|auto-trad|
|[Ads9d3lRyv0Z8u5p.htm](pathfinder-bestiary-items/Ads9d3lRyv0Z8u5p.htm)|Constant Spells|auto-trad|
|[ady0G6RCsUbZ7gev.htm](pathfinder-bestiary-items/ady0G6RCsUbZ7gev.htm)|Emotional Frenzy|auto-trad|
|[aE5LiTLYluL3FG7u.htm](pathfinder-bestiary-items/aE5LiTLYluL3FG7u.htm)|Desert Thirst|auto-trad|
|[aeJceILHyteCIR57.htm](pathfinder-bestiary-items/aeJceILHyteCIR57.htm)|Jaws|auto-trad|
|[Aern77CIRMOy8Se3.htm](pathfinder-bestiary-items/Aern77CIRMOy8Se3.htm)|Claw|auto-trad|
|[aeZh7jl8AKKWs8Qv.htm](pathfinder-bestiary-items/aeZh7jl8AKKWs8Qv.htm)|Rust|auto-trad|
|[Af1bf54ub8OEzyPl.htm](pathfinder-bestiary-items/Af1bf54ub8OEzyPl.htm)|Pseudopod|auto-trad|
|[AFLPUVObYcXb0ATY.htm](pathfinder-bestiary-items/AFLPUVObYcXb0ATY.htm)|Beak|auto-trad|
|[AflyUplOQICTA75a.htm](pathfinder-bestiary-items/AflyUplOQICTA75a.htm)|Rush of Water|auto-trad|
|[afRIl1Q7FwNUtxuI.htm](pathfinder-bestiary-items/afRIl1Q7FwNUtxuI.htm)|Nature Empathy|auto-trad|
|[Afsdsjz6Zk8smEoN.htm](pathfinder-bestiary-items/Afsdsjz6Zk8smEoN.htm)|Bonecrunching Bite|auto-trad|
|[AFuY9kW00fzj7WH5.htm](pathfinder-bestiary-items/AFuY9kW00fzj7WH5.htm)|Ranseur|auto-trad|
|[aGg1kw2fRdZFdeew.htm](pathfinder-bestiary-items/aGg1kw2fRdZFdeew.htm)|Stone Fist|auto-trad|
|[AGjtcz0cAWzBXlEx.htm](pathfinder-bestiary-items/AGjtcz0cAWzBXlEx.htm)|Unluck Aura|auto-trad|
|[agqHW8i0ToF0OIyF.htm](pathfinder-bestiary-items/agqHW8i0ToF0OIyF.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[AH4yYBLDuS7Gvkck.htm](pathfinder-bestiary-items/AH4yYBLDuS7Gvkck.htm)|Primal Prepared Spells|auto-trad|
|[ah6fU0jl6IqyfBw4.htm](pathfinder-bestiary-items/ah6fU0jl6IqyfBw4.htm)|Telepathy (Touch)|auto-trad|
|[AHGv0wFC9jb2bwlQ.htm](pathfinder-bestiary-items/AHGv0wFC9jb2bwlQ.htm)|Claw|auto-trad|
|[AHjbBF5emgeLzhga.htm](pathfinder-bestiary-items/AHjbBF5emgeLzhga.htm)|Darkvision|auto-trad|
|[ahtVNJjKjna3LTc1.htm](pathfinder-bestiary-items/ahtVNJjKjna3LTc1.htm)|Alchemical Injection|auto-trad|
|[AIacQlgZQnMm7C6j.htm](pathfinder-bestiary-items/AIacQlgZQnMm7C6j.htm)|Greatsword|auto-trad|
|[AIcffB6vSk2oEmqt.htm](pathfinder-bestiary-items/AIcffB6vSk2oEmqt.htm)|Ground Slam|auto-trad|
|[aidpPDwvXoVTKC6j.htm](pathfinder-bestiary-items/aidpPDwvXoVTKC6j.htm)|Grab|auto-trad|
|[AiKIcXFJx9lEEfnt.htm](pathfinder-bestiary-items/AiKIcXFJx9lEEfnt.htm)|Surge|auto-trad|
|[aIqWT8KUlYwUY9x2.htm](pathfinder-bestiary-items/aIqWT8KUlYwUY9x2.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[aivVKzlEOwieqgj1.htm](pathfinder-bestiary-items/aivVKzlEOwieqgj1.htm)|Flame of Justice|auto-trad|
|[ajDZIRytCVKqw5RN.htm](pathfinder-bestiary-items/ajDZIRytCVKqw5RN.htm)|Constrict|auto-trad|
|[AJXSn9fEM5rYks8x.htm](pathfinder-bestiary-items/AJXSn9fEM5rYks8x.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[AjYmWzjrt54sexgk.htm](pathfinder-bestiary-items/AjYmWzjrt54sexgk.htm)|Feed on Emotion|auto-trad|
|[AkGZKjw58GNy5tNW.htm](pathfinder-bestiary-items/AkGZKjw58GNy5tNW.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[AkOZsS5Ar1Py89Zo.htm](pathfinder-bestiary-items/AkOZsS5Ar1Py89Zo.htm)|Tendril|auto-trad|
|[AKwJMkLiCXFd1s8q.htm](pathfinder-bestiary-items/AKwJMkLiCXFd1s8q.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[AMKIm1QMEUOXLP3p.htm](pathfinder-bestiary-items/AMKIm1QMEUOXLP3p.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[aMNM0X0dVvVRdlZT.htm](pathfinder-bestiary-items/aMNM0X0dVvVRdlZT.htm)|Vulnerable to Neutralize Poison|auto-trad|
|[AMOJU0wX8r6flwf4.htm](pathfinder-bestiary-items/AMOJU0wX8r6flwf4.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[AmPgQIXqmES8fImF.htm](pathfinder-bestiary-items/AmPgQIXqmES8fImF.htm)|Climb Stone|auto-trad|
|[an6fdel1IBQ8fJmP.htm](pathfinder-bestiary-items/an6fdel1IBQ8fJmP.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[AniyCnlqZmw3IO9L.htm](pathfinder-bestiary-items/AniyCnlqZmw3IO9L.htm)|Change Shape|auto-trad|
|[ANrh02werSO57j4X.htm](pathfinder-bestiary-items/ANrh02werSO57j4X.htm)|Wing|auto-trad|
|[anRs7G7U6p0N15yh.htm](pathfinder-bestiary-items/anRs7G7U6p0N15yh.htm)|Negative Healing|auto-trad|
|[aoLTJq3XbJ29K6yY.htm](pathfinder-bestiary-items/aoLTJq3XbJ29K6yY.htm)|Throw Rock|auto-trad|
|[aoyjyhv7Dw4gIwd3.htm](pathfinder-bestiary-items/aoyjyhv7Dw4gIwd3.htm)|Claw|auto-trad|
|[Ap0BhEjEIF39DlVA.htm](pathfinder-bestiary-items/Ap0BhEjEIF39DlVA.htm)|Tail|auto-trad|
|[apIdQFYWsMBgRVKk.htm](pathfinder-bestiary-items/apIdQFYWsMBgRVKk.htm)|Pincer|auto-trad|
|[aPjm9C2ibRhYAr0x.htm](pathfinder-bestiary-items/aPjm9C2ibRhYAr0x.htm)|Telepathy 120 feet|auto-trad|
|[APm9xzjMvRMHIYon.htm](pathfinder-bestiary-items/APm9xzjMvRMHIYon.htm)|Scorpion Venom|auto-trad|
|[aPpWgGl2IQgPJjH1.htm](pathfinder-bestiary-items/aPpWgGl2IQgPJjH1.htm)|Fist|auto-trad|
|[Aq3s4Ckr3PMnxD4H.htm](pathfinder-bestiary-items/Aq3s4Ckr3PMnxD4H.htm)|Breath Weapon|auto-trad|
|[aQJcYgTKQBvgKvIv.htm](pathfinder-bestiary-items/aQJcYgTKQBvgKvIv.htm)|Wing Deflection|auto-trad|
|[aqwHyoUR6cj9y6s8.htm](pathfinder-bestiary-items/aqwHyoUR6cj9y6s8.htm)|Focused Gaze|auto-trad|
|[aQZZc8Ut3mpK2lO3.htm](pathfinder-bestiary-items/aQZZc8Ut3mpK2lO3.htm)|At-Will Spells|auto-trad|
|[aR1dX4hkVtUEcDD2.htm](pathfinder-bestiary-items/aR1dX4hkVtUEcDD2.htm)|At-Will Spells|auto-trad|
|[aRbpPF0TFfUG64dX.htm](pathfinder-bestiary-items/aRbpPF0TFfUG64dX.htm)|Divine Innate Spells|auto-trad|
|[ArpzcwnZJV6NMI5C.htm](pathfinder-bestiary-items/ArpzcwnZJV6NMI5C.htm)|Divine Innate Spells|auto-trad|
|[ARWbmjbxbCd0JMUv.htm](pathfinder-bestiary-items/ARWbmjbxbCd0JMUv.htm)|Spores|auto-trad|
|[AS5E8Dafjstrafqy.htm](pathfinder-bestiary-items/AS5E8Dafjstrafqy.htm)|Constant Spells|auto-trad|
|[asa3KdXDutuDly1Q.htm](pathfinder-bestiary-items/asa3KdXDutuDly1Q.htm)|Primal Prepared Spells|auto-trad|
|[AsGPSfdLJ7pQhwZZ.htm](pathfinder-bestiary-items/AsGPSfdLJ7pQhwZZ.htm)|Attack of Opportunity|auto-trad|
|[AsXbDxRSKFmiQ6jb.htm](pathfinder-bestiary-items/AsXbDxRSKFmiQ6jb.htm)|Enfeebling Bite|auto-trad|
|[atH4c29XMbIXGwpG.htm](pathfinder-bestiary-items/atH4c29XMbIXGwpG.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[ATs94SggOJoPD71u.htm](pathfinder-bestiary-items/ATs94SggOJoPD71u.htm)|Diabolic Healing|auto-trad|
|[AtTvfif8mNvTl8HD.htm](pathfinder-bestiary-items/AtTvfif8mNvTl8HD.htm)|Ferocity|auto-trad|
|[aTtxivkD4mgfosfH.htm](pathfinder-bestiary-items/aTtxivkD4mgfosfH.htm)|Change Shape|auto-trad|
|[AtU6PHgBXlRy0fUh.htm](pathfinder-bestiary-items/AtU6PHgBXlRy0fUh.htm)|Jaws|auto-trad|
|[AuBndTd199sHLYXX.htm](pathfinder-bestiary-items/AuBndTd199sHLYXX.htm)|Divine Innate Spells|auto-trad|
|[AuhMdlvvBkXqKNG8.htm](pathfinder-bestiary-items/AuhMdlvvBkXqKNG8.htm)|Telekinetic Object (Slashing)|auto-trad|
|[AUlLGXQgQx21Rynz.htm](pathfinder-bestiary-items/AUlLGXQgQx21Rynz.htm)|Wing Deflection|auto-trad|
|[AuM50DWRYYFugkl6.htm](pathfinder-bestiary-items/AuM50DWRYYFugkl6.htm)|Terrifying Display|auto-trad|
|[auqHGoaXoCI4Z25O.htm](pathfinder-bestiary-items/auqHGoaXoCI4Z25O.htm)|Beak|auto-trad|
|[AV27N9R0VVXyHIcK.htm](pathfinder-bestiary-items/AV27N9R0VVXyHIcK.htm)|Divine Innate Spells|auto-trad|
|[Av5F60Xe1vXr81pb.htm](pathfinder-bestiary-items/Av5F60Xe1vXr81pb.htm)|+2 to Will Saves vs. Mental|auto-trad|
|[AVpXnwBBGoOo68pw.htm](pathfinder-bestiary-items/AVpXnwBBGoOo68pw.htm)|Turn to Mist|auto-trad|
|[avV91CD5YGdSAFSf.htm](pathfinder-bestiary-items/avV91CD5YGdSAFSf.htm)|Eat Away|auto-trad|
|[aWJTgaTUKotr2LWA.htm](pathfinder-bestiary-items/aWJTgaTUKotr2LWA.htm)|Constrict|auto-trad|
|[aWM3N85sMlll5djQ.htm](pathfinder-bestiary-items/aWM3N85sMlll5djQ.htm)|Abyssal Plague|auto-trad|
|[AWS8p85NHI3IGL2n.htm](pathfinder-bestiary-items/AWS8p85NHI3IGL2n.htm)|Dominate|auto-trad|
|[AWTYToB1yUp2T16A.htm](pathfinder-bestiary-items/AWTYToB1yUp2T16A.htm)|Cold Aura|auto-trad|
|[aWzRjtpeOVb7ece0.htm](pathfinder-bestiary-items/aWzRjtpeOVb7ece0.htm)|Branch|auto-trad|
|[axeTxBouSunEePNo.htm](pathfinder-bestiary-items/axeTxBouSunEePNo.htm)|Acid Flask|auto-trad|
|[Axg3G5azQ1xnZunc.htm](pathfinder-bestiary-items/Axg3G5azQ1xnZunc.htm)|Arcane Spontaneous Spells|auto-trad|
|[aY1GSHaV78XgFK6W.htm](pathfinder-bestiary-items/aY1GSHaV78XgFK6W.htm)|At-Will Spells|auto-trad|
|[aZ0lzdc6SrYbvfX6.htm](pathfinder-bestiary-items/aZ0lzdc6SrYbvfX6.htm)|Constrict|auto-trad|
|[AzIejcCA4nY9zlmB.htm](pathfinder-bestiary-items/AzIejcCA4nY9zlmB.htm)|Sandstorm Breath|auto-trad|
|[AzJK5a45GubW0U4t.htm](pathfinder-bestiary-items/AzJK5a45GubW0U4t.htm)|Arcane Innate Spells|auto-trad|
|[aZNmBtBro1xYaubO.htm](pathfinder-bestiary-items/aZNmBtBro1xYaubO.htm)|Drider Venom|auto-trad|
|[azSRd7to2x2eVjTv.htm](pathfinder-bestiary-items/azSRd7to2x2eVjTv.htm)|Scimitar|auto-trad|
|[b0CZ1TMWNNWhztYK.htm](pathfinder-bestiary-items/b0CZ1TMWNNWhztYK.htm)|Fist|auto-trad|
|[b1CPKzVXEGyolvUo.htm](pathfinder-bestiary-items/b1CPKzVXEGyolvUo.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[b1w5wps2XBnNPOls.htm](pathfinder-bestiary-items/b1w5wps2XBnNPOls.htm)|Furious Claws|auto-trad|
|[b24uVlcva1St9NKy.htm](pathfinder-bestiary-items/b24uVlcva1St9NKy.htm)|Darkvision|auto-trad|
|[B2QWyyEvVmWZPUkk.htm](pathfinder-bestiary-items/B2QWyyEvVmWZPUkk.htm)|Lurching Charge|auto-trad|
|[b2WLYPv43nso3X3U.htm](pathfinder-bestiary-items/b2WLYPv43nso3X3U.htm)|Jaws|auto-trad|
|[b3OSC1hy3efFqhm7.htm](pathfinder-bestiary-items/b3OSC1hy3efFqhm7.htm)|Seductive Presence|auto-trad|
|[B3yqLomVN3P7lLnV.htm](pathfinder-bestiary-items/B3yqLomVN3P7lLnV.htm)|Trident|auto-trad|
|[b4GFgu6HgDqvji6X.htm](pathfinder-bestiary-items/b4GFgu6HgDqvji6X.htm)|Tail|auto-trad|
|[B4uQ3uTmODEiTxnM.htm](pathfinder-bestiary-items/B4uQ3uTmODEiTxnM.htm)|Low-Light Vision|auto-trad|
|[b5sQlwDziqEPeMij.htm](pathfinder-bestiary-items/b5sQlwDziqEPeMij.htm)|Arcane Prepared Spells|auto-trad|
|[B5U0kqS1gFhaSpqc.htm](pathfinder-bestiary-items/B5U0kqS1gFhaSpqc.htm)|Fist|auto-trad|
|[B6szcvpWsKu9fNp6.htm](pathfinder-bestiary-items/B6szcvpWsKu9fNp6.htm)|Claw|auto-trad|
|[B7oDXvjrHROptNlb.htm](pathfinder-bestiary-items/B7oDXvjrHROptNlb.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[B7rzf0nBJmzg8x0y.htm](pathfinder-bestiary-items/B7rzf0nBJmzg8x0y.htm)|Arcane Innate Spells|auto-trad|
|[B7v1R6e83IW0B1SR.htm](pathfinder-bestiary-items/B7v1R6e83IW0B1SR.htm)|Low-Light Vision|auto-trad|
|[B8eTnDbhYn9WARrP.htm](pathfinder-bestiary-items/B8eTnDbhYn9WARrP.htm)|Regeneration 10 (Deactivated by Cold Iron)|auto-trad|
|[b8kddR2Im2nSOnDM.htm](pathfinder-bestiary-items/b8kddR2Im2nSOnDM.htm)|Gallop|auto-trad|
|[b8LAOUk743PuI1qO.htm](pathfinder-bestiary-items/b8LAOUk743PuI1qO.htm)|Constant Spells|auto-trad|
|[B9C716BDIWeqDrLI.htm](pathfinder-bestiary-items/B9C716BDIWeqDrLI.htm)|Jaws|auto-trad|
|[b9p4QWRBH39ePPdV.htm](pathfinder-bestiary-items/b9p4QWRBH39ePPdV.htm)|Morningstar|auto-trad|
|[B9QrAlNM1MoP8sDk.htm](pathfinder-bestiary-items/B9QrAlNM1MoP8sDk.htm)|Bardic Lore|auto-trad|
|[B9VILNOR0aKT4LQ0.htm](pathfinder-bestiary-items/B9VILNOR0aKT4LQ0.htm)|Low-Light Vision|auto-trad|
|[B9YtjVsCCmeVw39y.htm](pathfinder-bestiary-items/B9YtjVsCCmeVw39y.htm)|Fist|auto-trad|
|[bA2QzO0CkyheAgub.htm](pathfinder-bestiary-items/bA2QzO0CkyheAgub.htm)|Claw|auto-trad|
|[BaQdyDW5VvRs5ZV4.htm](pathfinder-bestiary-items/BaQdyDW5VvRs5ZV4.htm)|Claw|auto-trad|
|[BbFyjI0o6Aed5DYL.htm](pathfinder-bestiary-items/BbFyjI0o6Aed5DYL.htm)|Strafing Chomp|auto-trad|
|[BblU1XBSC9IdeWlp.htm](pathfinder-bestiary-items/BblU1XBSC9IdeWlp.htm)|Jaws|auto-trad|
|[bcGhVPJyGDyUGvLK.htm](pathfinder-bestiary-items/bcGhVPJyGDyUGvLK.htm)|Divine Spontaneous Spells|auto-trad|
|[bCgl0VBfwaBeDgOh.htm](pathfinder-bestiary-items/bCgl0VBfwaBeDgOh.htm)|Longspear|auto-trad|
|[bcl3zcX6TOOupNt9.htm](pathfinder-bestiary-items/bcl3zcX6TOOupNt9.htm)|Knockdown|auto-trad|
|[Bcm7iwcliF6ChhW7.htm](pathfinder-bestiary-items/Bcm7iwcliF6ChhW7.htm)|Tarn Linnorm Venom|auto-trad|
|[bdbMvBqIkhaOvg4j.htm](pathfinder-bestiary-items/bdbMvBqIkhaOvg4j.htm)|Slime Squirt|auto-trad|
|[bDkXVdgOwJv0PBRc.htm](pathfinder-bestiary-items/bDkXVdgOwJv0PBRc.htm)|Barbed Maw|auto-trad|
|[bDogEMyUIoxPMls3.htm](pathfinder-bestiary-items/bDogEMyUIoxPMls3.htm)|Aquatic Dash|auto-trad|
|[bdrrSnIH32ERMP25.htm](pathfinder-bestiary-items/bdrrSnIH32ERMP25.htm)|Roar|auto-trad|
|[Be8ZCKKmNDNPgCM0.htm](pathfinder-bestiary-items/Be8ZCKKmNDNPgCM0.htm)|Goliath Spider Venom|auto-trad|
|[BER1sjugNCn7ml14.htm](pathfinder-bestiary-items/BER1sjugNCn7ml14.htm)|Crumble|auto-trad|
|[bEsmMOS1PqhLHHbh.htm](pathfinder-bestiary-items/bEsmMOS1PqhLHHbh.htm)|Swarming Bites|auto-trad|
|[BeuWnsp3t3NisaY3.htm](pathfinder-bestiary-items/BeuWnsp3t3NisaY3.htm)|Mandibles|auto-trad|
|[BF9LWkoQrDrBUzhl.htm](pathfinder-bestiary-items/BF9LWkoQrDrBUzhl.htm)|Shape Ice|auto-trad|
|[bftpGvdJeX5iLTfn.htm](pathfinder-bestiary-items/bftpGvdJeX5iLTfn.htm)|Hunk Of Meat|auto-trad|
|[bg3DA0xcxyfSwJZm.htm](pathfinder-bestiary-items/bg3DA0xcxyfSwJZm.htm)|Staff|auto-trad|
|[bgkXxmVMntPQOmND.htm](pathfinder-bestiary-items/bgkXxmVMntPQOmND.htm)|Rapier|auto-trad|
|[bgmY8mouK9Yal1iO.htm](pathfinder-bestiary-items/bgmY8mouK9Yal1iO.htm)|At-Will Spells|auto-trad|
|[bGNrdbl4JTRtmeMy.htm](pathfinder-bestiary-items/bGNrdbl4JTRtmeMy.htm)|Darkvision|auto-trad|
|[bgQ0VnpFrkVAGfAI.htm](pathfinder-bestiary-items/bgQ0VnpFrkVAGfAI.htm)|Claw|auto-trad|
|[bGvsYcaEa1DdcONa.htm](pathfinder-bestiary-items/bGvsYcaEa1DdcONa.htm)|Arcane Innate Spells|auto-trad|
|[BhAemzsQzTnyMQTs.htm](pathfinder-bestiary-items/BhAemzsQzTnyMQTs.htm)|Light Blindness|auto-trad|
|[bhI0M1dnsLjItat7.htm](pathfinder-bestiary-items/bhI0M1dnsLjItat7.htm)|Mandibles|auto-trad|
|[bhRfrSYjod5132GE.htm](pathfinder-bestiary-items/bhRfrSYjod5132GE.htm)|Acid Maw|auto-trad|
|[bHxNMVIdbA3qM962.htm](pathfinder-bestiary-items/bHxNMVIdbA3qM962.htm)|Darkvision|auto-trad|
|[Bie1u5IuxXpAhbC7.htm](pathfinder-bestiary-items/Bie1u5IuxXpAhbC7.htm)|Jaws|auto-trad|
|[BJ1PhJfeJAlUL4sL.htm](pathfinder-bestiary-items/BJ1PhJfeJAlUL4sL.htm)|Tremorsense (Imprecise) 100 feet|auto-trad|
|[Bj9AvzdEeKXM1Lpy.htm](pathfinder-bestiary-items/Bj9AvzdEeKXM1Lpy.htm)|Leg|auto-trad|
|[BJ9pqonMdpZOYbY7.htm](pathfinder-bestiary-items/BJ9pqonMdpZOYbY7.htm)|Low-Light Vision|auto-trad|
|[bjF022r2jEalWdPs.htm](pathfinder-bestiary-items/bjF022r2jEalWdPs.htm)|Swarm Mind|auto-trad|
|[BjiGVDonTx2JHmxh.htm](pathfinder-bestiary-items/BjiGVDonTx2JHmxh.htm)|Low-Light Vision|auto-trad|
|[BJiQlSKAx6WhMT1p.htm](pathfinder-bestiary-items/BJiQlSKAx6WhMT1p.htm)|Low-Light Vision|auto-trad|
|[bjN59WI6VS1niQFU.htm](pathfinder-bestiary-items/bjN59WI6VS1niQFU.htm)|Fast Swallow|auto-trad|
|[bjVIgy73LNkpiuOw.htm](pathfinder-bestiary-items/bjVIgy73LNkpiuOw.htm)|Dagger|auto-trad|
|[bK065dhAE5PbYbis.htm](pathfinder-bestiary-items/bK065dhAE5PbYbis.htm)|At-Will Spells|auto-trad|
|[Bk52AZEcClvtQXB0.htm](pathfinder-bestiary-items/Bk52AZEcClvtQXB0.htm)|Jaws|auto-trad|
|[Bk5ADVa0aOQ7APmH.htm](pathfinder-bestiary-items/Bk5ADVa0aOQ7APmH.htm)|Tail|auto-trad|
|[bKldotvWkspZfjHv.htm](pathfinder-bestiary-items/bKldotvWkspZfjHv.htm)|Occult Innate Spells|auto-trad|
|[BKmS1pjKN4QsePV7.htm](pathfinder-bestiary-items/BKmS1pjKN4QsePV7.htm)|Low-Light Vision|auto-trad|
|[bKmTRJI4PjS70sWZ.htm](pathfinder-bestiary-items/bKmTRJI4PjS70sWZ.htm)|Inexorable March|auto-trad|
|[bKPOLbNfdVcrEpjV.htm](pathfinder-bestiary-items/bKPOLbNfdVcrEpjV.htm)|Bonds of Iron|auto-trad|
|[BkQtrsdTqOIyZInG.htm](pathfinder-bestiary-items/BkQtrsdTqOIyZInG.htm)|Darkvision|auto-trad|
|[bkVvJziJZPgu8vV0.htm](pathfinder-bestiary-items/bkVvJziJZPgu8vV0.htm)|Darkvision|auto-trad|
|[BleHEEpt3dwEMeDo.htm](pathfinder-bestiary-items/BleHEEpt3dwEMeDo.htm)|Treacherous Aura|auto-trad|
|[blGUawuZyyQVnVXf.htm](pathfinder-bestiary-items/blGUawuZyyQVnVXf.htm)|Pounce|auto-trad|
|[BLleGAYzhc0cIyIz.htm](pathfinder-bestiary-items/BLleGAYzhc0cIyIz.htm)|Darkvision|auto-trad|
|[bLrblJlMOfhn39My.htm](pathfinder-bestiary-items/bLrblJlMOfhn39My.htm)|Swarm Mind|auto-trad|
|[BlUA1CHpOuotxrV7.htm](pathfinder-bestiary-items/BlUA1CHpOuotxrV7.htm)|Foot|auto-trad|
|[bmbqZlQmEmeXm2DS.htm](pathfinder-bestiary-items/bmbqZlQmEmeXm2DS.htm)|Treacherous Veil|auto-trad|
|[BMgUFhUSXPid36Be.htm](pathfinder-bestiary-items/BMgUFhUSXPid36Be.htm)|Ethereal Web Trap|auto-trad|
|[BmhB6hl9Ivt5pnw6.htm](pathfinder-bestiary-items/BmhB6hl9Ivt5pnw6.htm)|Talon|auto-trad|
|[bmmCoxENGAgHtWP1.htm](pathfinder-bestiary-items/bmmCoxENGAgHtWP1.htm)|Web Sense|auto-trad|
|[bmQdHng3a04PljTz.htm](pathfinder-bestiary-items/bmQdHng3a04PljTz.htm)|Divine Spontaneous Spells|auto-trad|
|[BmQOyLxye6DAZpWI.htm](pathfinder-bestiary-items/BmQOyLxye6DAZpWI.htm)|Light Blindness|auto-trad|
|[BMYNq39vlF0aowNr.htm](pathfinder-bestiary-items/BMYNq39vlF0aowNr.htm)|Capsize|auto-trad|
|[bnuflGRdVoZz0Rf9.htm](pathfinder-bestiary-items/bnuflGRdVoZz0Rf9.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[BnuggroyV0iJLdlp.htm](pathfinder-bestiary-items/BnuggroyV0iJLdlp.htm)|Constant Spells|auto-trad|
|[bo8qDCWaF4txPsi1.htm](pathfinder-bestiary-items/bo8qDCWaF4txPsi1.htm)|Keen Returning Hatchet|auto-trad|
|[BOFFwI5nKDfABluP.htm](pathfinder-bestiary-items/BOFFwI5nKDfABluP.htm)|Negative Healing|auto-trad|
|[bOieIj9yjp8Tyi0J.htm](pathfinder-bestiary-items/bOieIj9yjp8Tyi0J.htm)|Claw|auto-trad|
|[BoUOzgE6tap1MfMd.htm](pathfinder-bestiary-items/BoUOzgE6tap1MfMd.htm)|At-Will Spells|auto-trad|
|[bPBjZcWUOmlUEYIw.htm](pathfinder-bestiary-items/bPBjZcWUOmlUEYIw.htm)|Jaws|auto-trad|
|[BPCk1pWTtkLjllYB.htm](pathfinder-bestiary-items/BPCk1pWTtkLjllYB.htm)|Claw|auto-trad|
|[bpECAdKNxGcT7pXU.htm](pathfinder-bestiary-items/bpECAdKNxGcT7pXU.htm)|Twin Feint|auto-trad|
|[bpsAcAXXrfV4uyDd.htm](pathfinder-bestiary-items/bpsAcAXXrfV4uyDd.htm)|Tail|auto-trad|
|[bQaD3ryddLU1MPFx.htm](pathfinder-bestiary-items/bQaD3ryddLU1MPFx.htm)|Primal Innate Spells|auto-trad|
|[bqcTHqymDNnpRDg9.htm](pathfinder-bestiary-items/bqcTHqymDNnpRDg9.htm)|Buck|auto-trad|
|[bQOfAaZEAHrScsHM.htm](pathfinder-bestiary-items/bQOfAaZEAHrScsHM.htm)|Grab|auto-trad|
|[BqxVlrDpgV0pxdb6.htm](pathfinder-bestiary-items/BqxVlrDpgV0pxdb6.htm)|Darkvision|auto-trad|
|[BsLJO9VEEJtk5wse.htm](pathfinder-bestiary-items/BsLJO9VEEJtk5wse.htm)|Hand Crossbow|auto-trad|
|[BsoxzxmGvzcKefKT.htm](pathfinder-bestiary-items/BsoxzxmGvzcKefKT.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[bsSr0xdB56iFuS70.htm](pathfinder-bestiary-items/bsSr0xdB56iFuS70.htm)|All-Around Vision|auto-trad|
|[bT8ABzfkEP7llB3c.htm](pathfinder-bestiary-items/bT8ABzfkEP7llB3c.htm)|Frightful Presence|auto-trad|
|[BtBJ8FVnBLJgGYxG.htm](pathfinder-bestiary-items/BtBJ8FVnBLJgGYxG.htm)|Wing|auto-trad|
|[bthV65bmBiFxafJ6.htm](pathfinder-bestiary-items/bthV65bmBiFxafJ6.htm)|Divine Innate Spells|auto-trad|
|[BudK3JDDVjbWqw8G.htm](pathfinder-bestiary-items/BudK3JDDVjbWqw8G.htm)|Warpwave Strike|auto-trad|
|[BuEcwGM7NH9is0w9.htm](pathfinder-bestiary-items/BuEcwGM7NH9is0w9.htm)|Eye Beams|auto-trad|
|[BuTCzXbSgKU068vi.htm](pathfinder-bestiary-items/BuTCzXbSgKU068vi.htm)|Occult Innate Spells|auto-trad|
|[BuvtHVq16FzLQ1Fi.htm](pathfinder-bestiary-items/BuvtHVq16FzLQ1Fi.htm)|Divine Innate Spells|auto-trad|
|[BVtWjIcXEObj4Rr9.htm](pathfinder-bestiary-items/BVtWjIcXEObj4Rr9.htm)|Hunted Shot|auto-trad|
|[bVwQccP3j81toBAq.htm](pathfinder-bestiary-items/bVwQccP3j81toBAq.htm)|Flame of Justice|auto-trad|
|[bW0FVpeKKVStkJVf.htm](pathfinder-bestiary-items/bW0FVpeKKVStkJVf.htm)|Telepathy 100 feet|auto-trad|
|[bWfE1BR1kz8fIanF.htm](pathfinder-bestiary-items/bWfE1BR1kz8fIanF.htm)|Vicious Gore|auto-trad|
|[bwU9HIhgyMLShCWw.htm](pathfinder-bestiary-items/bwU9HIhgyMLShCWw.htm)|Crossbow|auto-trad|
|[bWytg8B4wFHQLuzN.htm](pathfinder-bestiary-items/bWytg8B4wFHQLuzN.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[Bx7nabKeB4DLhe7B.htm](pathfinder-bestiary-items/Bx7nabKeB4DLhe7B.htm)|Darkvision|auto-trad|
|[BXuDhj7X68wG6zma.htm](pathfinder-bestiary-items/BXuDhj7X68wG6zma.htm)|Javelin|auto-trad|
|[by7p3pdmrMKbVYk8.htm](pathfinder-bestiary-items/by7p3pdmrMKbVYk8.htm)|Tail|auto-trad|
|[bY7rkyUV1wBS80Uz.htm](pathfinder-bestiary-items/bY7rkyUV1wBS80Uz.htm)|Low-Light Vision|auto-trad|
|[bYG1y6PsSmulLnqZ.htm](pathfinder-bestiary-items/bYG1y6PsSmulLnqZ.htm)|Change Shape|auto-trad|
|[ByItiXXLE0ZzOvm8.htm](pathfinder-bestiary-items/ByItiXXLE0ZzOvm8.htm)|Improved Grab|auto-trad|
|[bz1ZQoFYo4soy8jN.htm](pathfinder-bestiary-items/bz1ZQoFYo4soy8jN.htm)|Darkvision|auto-trad|
|[bZ4JNU36mcWHHrmR.htm](pathfinder-bestiary-items/bZ4JNU36mcWHHrmR.htm)|Grasping Tendrils|auto-trad|
|[bZL57I2PUMygGVWO.htm](pathfinder-bestiary-items/bZL57I2PUMygGVWO.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[bzNpoAXw0BCP1dGK.htm](pathfinder-bestiary-items/bzNpoAXw0BCP1dGK.htm)|Improved Knockdown|auto-trad|
|[BzrCf46VVV0IyaJb.htm](pathfinder-bestiary-items/BzrCf46VVV0IyaJb.htm)|Attack of Opportunity (Special)|auto-trad|
|[c09zz4QRa6hJSZ6q.htm](pathfinder-bestiary-items/c09zz4QRa6hJSZ6q.htm)|Blackaxe - Teleport|auto-trad|
|[C0y22etsAkFpPbDt.htm](pathfinder-bestiary-items/C0y22etsAkFpPbDt.htm)|Negative Healing|auto-trad|
|[C2OIrqZ6uMpRgLEF.htm](pathfinder-bestiary-items/C2OIrqZ6uMpRgLEF.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[C2pJTs2tt6oHDcFW.htm](pathfinder-bestiary-items/C2pJTs2tt6oHDcFW.htm)|Change Shape|auto-trad|
|[c2UaCFr89s3CtUzy.htm](pathfinder-bestiary-items/c2UaCFr89s3CtUzy.htm)|Rock|auto-trad|
|[C35zZH9Tm4QXYQ4y.htm](pathfinder-bestiary-items/C35zZH9Tm4QXYQ4y.htm)|Breath Weapon|auto-trad|
|[c3JTz3NjO3wCrokf.htm](pathfinder-bestiary-items/c3JTz3NjO3wCrokf.htm)|Draconic Momentum|auto-trad|
|[c3nJwvrVvJlQrgWU.htm](pathfinder-bestiary-items/c3nJwvrVvJlQrgWU.htm)|Attack of Opportunity|auto-trad|
|[C3vgtExLgNdN7GHz.htm](pathfinder-bestiary-items/C3vgtExLgNdN7GHz.htm)|Vorpal Cold Iron Silver Longsword|auto-trad|
|[c4LXwYKMdM5R7B7K.htm](pathfinder-bestiary-items/c4LXwYKMdM5R7B7K.htm)|Hidden Movement|auto-trad|
|[C5CTHxWjBYotqr2K.htm](pathfinder-bestiary-items/C5CTHxWjBYotqr2K.htm)|Breath Weapon|auto-trad|
|[c5Kaa1L8XCouqJy1.htm](pathfinder-bestiary-items/c5Kaa1L8XCouqJy1.htm)|Divine Innate Spells|auto-trad|
|[C5Knn5mQqyHvaE7Z.htm](pathfinder-bestiary-items/C5Knn5mQqyHvaE7Z.htm)|Fist|auto-trad|
|[C5NNlv2jeZwTRcPG.htm](pathfinder-bestiary-items/C5NNlv2jeZwTRcPG.htm)|Primal Innate Spells|auto-trad|
|[C5O7yPqYGSLmmeA2.htm](pathfinder-bestiary-items/C5O7yPqYGSLmmeA2.htm)|Light Mace|auto-trad|
|[C6VJPsP2T62aUaDR.htm](pathfinder-bestiary-items/C6VJPsP2T62aUaDR.htm)|Fangs|auto-trad|
|[C77jgOGQyRA9OepX.htm](pathfinder-bestiary-items/C77jgOGQyRA9OepX.htm)|Claw|auto-trad|
|[C7DnHa0hbdRjZtG9.htm](pathfinder-bestiary-items/C7DnHa0hbdRjZtG9.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[c8cI240EHdwf8MM1.htm](pathfinder-bestiary-items/c8cI240EHdwf8MM1.htm)|Deadly Mandibles|auto-trad|
|[casqOYm41JuCEUGY.htm](pathfinder-bestiary-items/casqOYm41JuCEUGY.htm)|Wide Swing|auto-trad|
|[CasRaBMpXnWDgBUq.htm](pathfinder-bestiary-items/CasRaBMpXnWDgBUq.htm)|Divine Rituals|auto-trad|
|[CAyvne51mSjL5IzV.htm](pathfinder-bestiary-items/CAyvne51mSjL5IzV.htm)|Darkvision|auto-trad|
|[Cb2C8LIPyMVI1r9J.htm](pathfinder-bestiary-items/Cb2C8LIPyMVI1r9J.htm)|Vent|auto-trad|
|[CblyGPqXFB5ALfDo.htm](pathfinder-bestiary-items/CblyGPqXFB5ALfDo.htm)|Darkvision|auto-trad|
|[CBSPcFxMeCnKziJ9.htm](pathfinder-bestiary-items/CBSPcFxMeCnKziJ9.htm)|Constant Spells|auto-trad|
|[cbuxKyYefdlX5AAX.htm](pathfinder-bestiary-items/cbuxKyYefdlX5AAX.htm)|Bloodletting|auto-trad|
|[CBZgnwJ9S03gtLHf.htm](pathfinder-bestiary-items/CBZgnwJ9S03gtLHf.htm)|Focused Assault|auto-trad|
|[cC853WQiGJtDTkRc.htm](pathfinder-bestiary-items/cC853WQiGJtDTkRc.htm)|Frightful Presence|auto-trad|
|[cckOUw1gK3GNCnML.htm](pathfinder-bestiary-items/cckOUw1gK3GNCnML.htm)|Slime|auto-trad|
|[CcLeMibU2tmiAzzF.htm](pathfinder-bestiary-items/CcLeMibU2tmiAzzF.htm)|Low-Light Vision|auto-trad|
|[Cd0NzEZrJzdhDSY8.htm](pathfinder-bestiary-items/Cd0NzEZrJzdhDSY8.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[CDkktCbVeomEuX4D.htm](pathfinder-bestiary-items/CDkktCbVeomEuX4D.htm)|Filth Wallow|auto-trad|
|[ce9aKxH1S1gitPNO.htm](pathfinder-bestiary-items/ce9aKxH1S1gitPNO.htm)|Fangs|auto-trad|
|[CellesdPVQW5Dk8g.htm](pathfinder-bestiary-items/CellesdPVQW5Dk8g.htm)|Ferocity|auto-trad|
|[CEngjOQBRH1DpQeo.htm](pathfinder-bestiary-items/CEngjOQBRH1DpQeo.htm)|Breach|auto-trad|
|[Cf4Skii5emWWMeWd.htm](pathfinder-bestiary-items/Cf4Skii5emWWMeWd.htm)|Breath Weapon|auto-trad|
|[cFtfekZXzWpLtSzL.htm](pathfinder-bestiary-items/cFtfekZXzWpLtSzL.htm)|Amnesia Venom|auto-trad|
|[Cfu9UWLk4FBlTQTo.htm](pathfinder-bestiary-items/Cfu9UWLk4FBlTQTo.htm)|Reactive Gnaw|auto-trad|
|[CfzB5r8CSHETuXfA.htm](pathfinder-bestiary-items/CfzB5r8CSHETuXfA.htm)|Site Bound|auto-trad|
|[cg8c4dIdBFiff9le.htm](pathfinder-bestiary-items/cg8c4dIdBFiff9le.htm)|Sweeping Hook|auto-trad|
|[cGjIW67qp4XNCsT3.htm](pathfinder-bestiary-items/cGjIW67qp4XNCsT3.htm)|Jaws|auto-trad|
|[cGPPwZfpkBdll6m3.htm](pathfinder-bestiary-items/cGPPwZfpkBdll6m3.htm)|Constant Spells|auto-trad|
|[cgTMxtmKD5ESJlyX.htm](pathfinder-bestiary-items/cgTMxtmKD5ESJlyX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[CGWJ8z84ruiOubNo.htm](pathfinder-bestiary-items/CGWJ8z84ruiOubNo.htm)|Darkvision|auto-trad|
|[Ch3sywJ2yffhRgXI.htm](pathfinder-bestiary-items/Ch3sywJ2yffhRgXI.htm)|Jaws|auto-trad|
|[cHbcVpImsTWKA9DW.htm](pathfinder-bestiary-items/cHbcVpImsTWKA9DW.htm)|Tail Sweep|auto-trad|
|[CHG9uWZmFmSExlPo.htm](pathfinder-bestiary-items/CHG9uWZmFmSExlPo.htm)|Tail|auto-trad|
|[CHS0DAe3ekrw8ve1.htm](pathfinder-bestiary-items/CHS0DAe3ekrw8ve1.htm)|Shortbow|auto-trad|
|[CHt0fL7eCWfy5Rm7.htm](pathfinder-bestiary-items/CHt0fL7eCWfy5Rm7.htm)|Darkvision|auto-trad|
|[Chvo35nO1IOSkoxZ.htm](pathfinder-bestiary-items/Chvo35nO1IOSkoxZ.htm)|Low-Light Vision|auto-trad|
|[cIAptRydc370Xjbd.htm](pathfinder-bestiary-items/cIAptRydc370Xjbd.htm)|Intense Heat|auto-trad|
|[CiirPvlIj0ZtuHce.htm](pathfinder-bestiary-items/CiirPvlIj0ZtuHce.htm)|Vengeful Anger|auto-trad|
|[cIlup2qYJhZmAmRn.htm](pathfinder-bestiary-items/cIlup2qYJhZmAmRn.htm)|Primal Innate Spells|auto-trad|
|[ciyqPmkNFmHZPuIP.htm](pathfinder-bestiary-items/ciyqPmkNFmHZPuIP.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[CjdoOLjRkqyfp25v.htm](pathfinder-bestiary-items/CjdoOLjRkqyfp25v.htm)|Rejuvenation|auto-trad|
|[cjHi3qrYqK60e3Nn.htm](pathfinder-bestiary-items/cjHi3qrYqK60e3Nn.htm)|Attack of Opportunity (Special)|auto-trad|
|[cJo1BrfvKmNfCJOd.htm](pathfinder-bestiary-items/cJo1BrfvKmNfCJOd.htm)|Jaw|auto-trad|
|[cjpf10fNDqFN7zh9.htm](pathfinder-bestiary-items/cjpf10fNDqFN7zh9.htm)|Jaws|auto-trad|
|[ckyWLmudGWk8ota8.htm](pathfinder-bestiary-items/ckyWLmudGWk8ota8.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[cKztX9AO2dzSFhZu.htm](pathfinder-bestiary-items/cKztX9AO2dzSFhZu.htm)|Spike Stones|auto-trad|
|[cL0kuYeKKUFMb9w0.htm](pathfinder-bestiary-items/cL0kuYeKKUFMb9w0.htm)|Confounding Lash|auto-trad|
|[cL9t5xg0FTPCRpjR.htm](pathfinder-bestiary-items/cL9t5xg0FTPCRpjR.htm)|Improved Grab|auto-trad|
|[clqibIDmibjUw6f7.htm](pathfinder-bestiary-items/clqibIDmibjUw6f7.htm)|Jaws|auto-trad|
|[CLQtaRBZzEVhkEpc.htm](pathfinder-bestiary-items/CLQtaRBZzEVhkEpc.htm)|Staff Gems|auto-trad|
|[cM51Cf58q73IyWOj.htm](pathfinder-bestiary-items/cM51Cf58q73IyWOj.htm)|Trackless Step|auto-trad|
|[CMfpQM5btdzVWxB5.htm](pathfinder-bestiary-items/CMfpQM5btdzVWxB5.htm)|Slashing Claws|auto-trad|
|[CMFwSAQpmkkqeHxH.htm](pathfinder-bestiary-items/CMFwSAQpmkkqeHxH.htm)|Grab|auto-trad|
|[cMyzo0AivvJxq0Ly.htm](pathfinder-bestiary-items/cMyzo0AivvJxq0Ly.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[cndA0H7XPHDhDL2I.htm](pathfinder-bestiary-items/cndA0H7XPHDhDL2I.htm)|Arcane Innate Spells|auto-trad|
|[CnmgzGmi2Avq67QA.htm](pathfinder-bestiary-items/CnmgzGmi2Avq67QA.htm)|Dogslicer|auto-trad|
|[cnueIB7ag8UbEdIb.htm](pathfinder-bestiary-items/cnueIB7ag8UbEdIb.htm)|Darkvision|auto-trad|
|[COesoRf5oc4fLJhF.htm](pathfinder-bestiary-items/COesoRf5oc4fLJhF.htm)|Greater Darkvision|auto-trad|
|[coknqLEZIU2gBBH1.htm](pathfinder-bestiary-items/coknqLEZIU2gBBH1.htm)|Writhing Arms|auto-trad|
|[ComzTzx9Gu4QDQxN.htm](pathfinder-bestiary-items/ComzTzx9Gu4QDQxN.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[cOsFGJ38rHgozF9z.htm](pathfinder-bestiary-items/cOsFGJ38rHgozF9z.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[COureLl9794sHLIb.htm](pathfinder-bestiary-items/COureLl9794sHLIb.htm)|Blowgun|auto-trad|
|[Cp2wPvwVrx6QHzjb.htm](pathfinder-bestiary-items/Cp2wPvwVrx6QHzjb.htm)|Trample|auto-trad|
|[cP4AQbueWMfANEBz.htm](pathfinder-bestiary-items/cP4AQbueWMfANEBz.htm)|Greatclub|auto-trad|
|[cpfFNNa5pJRIFFS4.htm](pathfinder-bestiary-items/cpfFNNa5pJRIFFS4.htm)|Ranseur|auto-trad|
|[cPMrUDg67L1MxOgP.htm](pathfinder-bestiary-items/cPMrUDg67L1MxOgP.htm)|Darkvision|auto-trad|
|[cPq1a6i8dlZo6SPu.htm](pathfinder-bestiary-items/cPq1a6i8dlZo6SPu.htm)|Breath Weapon|auto-trad|
|[cprgu1LyMWRqzI0w.htm](pathfinder-bestiary-items/cprgu1LyMWRqzI0w.htm)|Claw|auto-trad|
|[cPuEYKhbrReSjt1J.htm](pathfinder-bestiary-items/cPuEYKhbrReSjt1J.htm)|Touch of Charity|auto-trad|
|[cpZUZlhLCvv2qK9o.htm](pathfinder-bestiary-items/cpZUZlhLCvv2qK9o.htm)|Swipe|auto-trad|
|[cqn01fpxgrAl4NvI.htm](pathfinder-bestiary-items/cqn01fpxgrAl4NvI.htm)|Wraith Spawn|auto-trad|
|[cQnMBBEfd291uJ4j.htm](pathfinder-bestiary-items/cQnMBBEfd291uJ4j.htm)|Stinger|auto-trad|
|[CQRb2PNLd84eBkAH.htm](pathfinder-bestiary-items/CQRb2PNLd84eBkAH.htm)|Claw|auto-trad|
|[cQrkrn9uX9hV860V.htm](pathfinder-bestiary-items/cQrkrn9uX9hV860V.htm)|Greater Darkvision|auto-trad|
|[CqU1NxH3VCWX924G.htm](pathfinder-bestiary-items/CqU1NxH3VCWX924G.htm)|Occult Innate Spells|auto-trad|
|[cr8chy1goN1ZSrnT.htm](pathfinder-bestiary-items/cr8chy1goN1ZSrnT.htm)|Mound|auto-trad|
|[crbSG0me6XVQjR9f.htm](pathfinder-bestiary-items/crbSG0me6XVQjR9f.htm)|Gust|auto-trad|
|[cRpfBVlR934upDge.htm](pathfinder-bestiary-items/cRpfBVlR934upDge.htm)|Negative Healing|auto-trad|
|[Crv6pRa77FWxBY6M.htm](pathfinder-bestiary-items/Crv6pRa77FWxBY6M.htm)|Wasp Larva|auto-trad|
|[crXV18abZr2DcEh8.htm](pathfinder-bestiary-items/crXV18abZr2DcEh8.htm)|Regurgitate|auto-trad|
|[CSxqThhPm4iTCCOP.htm](pathfinder-bestiary-items/CSxqThhPm4iTCCOP.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Cti6KJQlr8VT1UaM.htm](pathfinder-bestiary-items/Cti6KJQlr8VT1UaM.htm)|Light Form|auto-trad|
|[cTKVS3tS8RBLruwP.htm](pathfinder-bestiary-items/cTKVS3tS8RBLruwP.htm)|Darkvision|auto-trad|
|[cTy576jgZ9YTvVnf.htm](pathfinder-bestiary-items/cTy576jgZ9YTvVnf.htm)|Jaws|auto-trad|
|[CtYKJciKGy9Wtza1.htm](pathfinder-bestiary-items/CtYKJciKGy9Wtza1.htm)|Aklys|auto-trad|
|[Cu6kJlXyx18Zj7JW.htm](pathfinder-bestiary-items/Cu6kJlXyx18Zj7JW.htm)|Jaws|auto-trad|
|[Cu6pedSvnaK0fqOF.htm](pathfinder-bestiary-items/Cu6pedSvnaK0fqOF.htm)|Demand|auto-trad|
|[CUwhBzgJAPCU9WcT.htm](pathfinder-bestiary-items/CUwhBzgJAPCU9WcT.htm)|Paralyzing Touch|auto-trad|
|[CUZdx9N2AmVtEjB4.htm](pathfinder-bestiary-items/CUZdx9N2AmVtEjB4.htm)|Sudden Strike|auto-trad|
|[CvfLXQsndA3wb5S1.htm](pathfinder-bestiary-items/CvfLXQsndA3wb5S1.htm)|Foot|auto-trad|
|[cwbkGiwxXBEcaKO5.htm](pathfinder-bestiary-items/cwbkGiwxXBEcaKO5.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[cxCbteeoXr6WayNl.htm](pathfinder-bestiary-items/cxCbteeoXr6WayNl.htm)|Claw|auto-trad|
|[CyCkk2GjnKUtz2Va.htm](pathfinder-bestiary-items/CyCkk2GjnKUtz2Va.htm)|Improved Grab|auto-trad|
|[cYDXg8exhcvpvPs8.htm](pathfinder-bestiary-items/cYDXg8exhcvpvPs8.htm)|Darkvision|auto-trad|
|[CyJqBVfPXX4Fp283.htm](pathfinder-bestiary-items/CyJqBVfPXX4Fp283.htm)|Gallop|auto-trad|
|[CyjUUa3DmRYWXY48.htm](pathfinder-bestiary-items/CyjUUa3DmRYWXY48.htm)|Darkvision|auto-trad|
|[cyykndggOJBnOeK7.htm](pathfinder-bestiary-items/cyykndggOJBnOeK7.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[cYzNZC3Tju4useHD.htm](pathfinder-bestiary-items/cYzNZC3Tju4useHD.htm)|Phantom Mount|auto-trad|
|[CZ7Y6ryNge5pbthf.htm](pathfinder-bestiary-items/CZ7Y6ryNge5pbthf.htm)|Claw|auto-trad|
|[cZc6wNhJDpipF2uK.htm](pathfinder-bestiary-items/cZc6wNhJDpipF2uK.htm)|Shield Block|auto-trad|
|[czqtIybNHs8mmEeg.htm](pathfinder-bestiary-items/czqtIybNHs8mmEeg.htm)|Create Spawn|auto-trad|
|[czVzxuwWmCRLgnuc.htm](pathfinder-bestiary-items/czVzxuwWmCRLgnuc.htm)|Forced Regeneration|auto-trad|
|[czwGWW73rkPW7vBg.htm](pathfinder-bestiary-items/czwGWW73rkPW7vBg.htm)|Goblin Scuttle|auto-trad|
|[D01DgsftPfTKC645.htm](pathfinder-bestiary-items/D01DgsftPfTKC645.htm)|Focus Beauty|auto-trad|
|[D0Bef8hMS0mQSDNY.htm](pathfinder-bestiary-items/D0Bef8hMS0mQSDNY.htm)|Tentacle|auto-trad|
|[D0BJOT03V2FE7mKT.htm](pathfinder-bestiary-items/D0BJOT03V2FE7mKT.htm)|Sneak Attack|auto-trad|
|[D0q0aNWo1X51cNe4.htm](pathfinder-bestiary-items/D0q0aNWo1X51cNe4.htm)|Annihilation Beams|auto-trad|
|[d0rdYMltT1ZmbABn.htm](pathfinder-bestiary-items/d0rdYMltT1ZmbABn.htm)|Darkvision|auto-trad|
|[d15C0z784CMcZVi4.htm](pathfinder-bestiary-items/d15C0z784CMcZVi4.htm)|Low-Light Vision|auto-trad|
|[d1Cp4BUTiymkJs98.htm](pathfinder-bestiary-items/d1Cp4BUTiymkJs98.htm)|All-Around Vision|auto-trad|
|[d1U4RaKdh9zQHieX.htm](pathfinder-bestiary-items/d1U4RaKdh9zQHieX.htm)|Low-Light Vision|auto-trad|
|[D1ZUrE1107HIW39r.htm](pathfinder-bestiary-items/D1ZUrE1107HIW39r.htm)|Thrash|auto-trad|
|[d26CTzuFe7OR9K5L.htm](pathfinder-bestiary-items/d26CTzuFe7OR9K5L.htm)|Claw|auto-trad|
|[D2ezhwANZLcMNbz9.htm](pathfinder-bestiary-items/D2ezhwANZLcMNbz9.htm)|Death Frenzy|auto-trad|
|[D2tbPVYsNe8XmlsW.htm](pathfinder-bestiary-items/D2tbPVYsNe8XmlsW.htm)|Frill Defense|auto-trad|
|[d3l2xwpTV1XkZBAO.htm](pathfinder-bestiary-items/d3l2xwpTV1XkZBAO.htm)|Spine Rake|auto-trad|
|[D4C1yHkCqpQckxgr.htm](pathfinder-bestiary-items/D4C1yHkCqpQckxgr.htm)|Tail Lash|auto-trad|
|[D4kBzUzbrcoM0VDr.htm](pathfinder-bestiary-items/D4kBzUzbrcoM0VDr.htm)|Jaws|auto-trad|
|[D4XA5YtJ0G4E1and.htm](pathfinder-bestiary-items/D4XA5YtJ0G4E1and.htm)|Jaws|auto-trad|
|[d574MMQ1MIrxdW3F.htm](pathfinder-bestiary-items/d574MMQ1MIrxdW3F.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[D5HDqkSWA0AZ7YAh.htm](pathfinder-bestiary-items/D5HDqkSWA0AZ7YAh.htm)|Constant Spells|auto-trad|
|[D64jIxws5U3eMcfZ.htm](pathfinder-bestiary-items/D64jIxws5U3eMcfZ.htm)|Divine Rituals|auto-trad|
|[D6yxsFtZFG5ysaek.htm](pathfinder-bestiary-items/D6yxsFtZFG5ysaek.htm)|Darkvision|auto-trad|
|[D71sSjwa6Oa7yHvw.htm](pathfinder-bestiary-items/D71sSjwa6Oa7yHvw.htm)|Grab|auto-trad|
|[D78seCDEZ9opnhed.htm](pathfinder-bestiary-items/D78seCDEZ9opnhed.htm)|Fangs|auto-trad|
|[D7nCoDXcwdCyO16R.htm](pathfinder-bestiary-items/D7nCoDXcwdCyO16R.htm)|Light Blindness|auto-trad|
|[d7roS2QZUtlhKfcO.htm](pathfinder-bestiary-items/d7roS2QZUtlhKfcO.htm)|Darkvision|auto-trad|
|[D89X40fEKxTsupQC.htm](pathfinder-bestiary-items/D89X40fEKxTsupQC.htm)|Ochre Acid|auto-trad|
|[d8HnQHeqQlzrKyFt.htm](pathfinder-bestiary-items/d8HnQHeqQlzrKyFt.htm)|Shove into Stone|auto-trad|
|[d9nsBoLgYoGWORv2.htm](pathfinder-bestiary-items/d9nsBoLgYoGWORv2.htm)|Golem Antimagic|auto-trad|
|[daIbHqtjaWyJns1q.htm](pathfinder-bestiary-items/daIbHqtjaWyJns1q.htm)|Darkvision|auto-trad|
|[dALR9L0wlW2puqiD.htm](pathfinder-bestiary-items/dALR9L0wlW2puqiD.htm)|Stench|auto-trad|
|[dB4Pr3yiNKH32fE8.htm](pathfinder-bestiary-items/dB4Pr3yiNKH32fE8.htm)|Knockdown|auto-trad|
|[DbfLZXVZVTQMJoyI.htm](pathfinder-bestiary-items/DbfLZXVZVTQMJoyI.htm)|Web Sense|auto-trad|
|[dBX2IpwGpcj3w0iE.htm](pathfinder-bestiary-items/dBX2IpwGpcj3w0iE.htm)|Darkvision|auto-trad|
|[dCLTLCDtx7ax8PwY.htm](pathfinder-bestiary-items/dCLTLCDtx7ax8PwY.htm)|Low-Light Vision|auto-trad|
|[DcTsGXo4ifg7cNUg.htm](pathfinder-bestiary-items/DcTsGXo4ifg7cNUg.htm)|Jaws|auto-trad|
|[dCuU1ER0aEC8sDuN.htm](pathfinder-bestiary-items/dCuU1ER0aEC8sDuN.htm)|Breath Weapon|auto-trad|
|[DCviUG6ackduxBeM.htm](pathfinder-bestiary-items/DCviUG6ackduxBeM.htm)|Primal Innate Spells|auto-trad|
|[dcxAPtLWxZkmq1es.htm](pathfinder-bestiary-items/dcxAPtLWxZkmq1es.htm)|At-Will Spells|auto-trad|
|[dD0COaIGpOldIcFD.htm](pathfinder-bestiary-items/dD0COaIGpOldIcFD.htm)|Shield Bash|auto-trad|
|[DD4lBYDa99ebm7Cb.htm](pathfinder-bestiary-items/DD4lBYDa99ebm7Cb.htm)|Rend|auto-trad|
|[DD9mP7VmHRk4Lm97.htm](pathfinder-bestiary-items/DD9mP7VmHRk4Lm97.htm)|Steady Spellcasting|auto-trad|
|[DDA6tlj5qsfP9Xb2.htm](pathfinder-bestiary-items/DDA6tlj5qsfP9Xb2.htm)|Constant Spells|auto-trad|
|[dDIdPBqIyM1WM4kD.htm](pathfinder-bestiary-items/dDIdPBqIyM1WM4kD.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[DdzQDZCnZhRpU8ny.htm](pathfinder-bestiary-items/DdzQDZCnZhRpU8ny.htm)|Divine Rituals|auto-trad|
|[dECHo3MtMAC4Dkh8.htm](pathfinder-bestiary-items/dECHo3MtMAC4Dkh8.htm)|Claw|auto-trad|
|[Dee07oE1Pj8EjAKU.htm](pathfinder-bestiary-items/Dee07oE1Pj8EjAKU.htm)|At-Will Spells|auto-trad|
|[deEbLCLRy4mZDvLo.htm](pathfinder-bestiary-items/deEbLCLRy4mZDvLo.htm)|Breath Weapon|auto-trad|
|[deLQWsbCp3jKrpIv.htm](pathfinder-bestiary-items/deLQWsbCp3jKrpIv.htm)|Mauler|auto-trad|
|[deSS0MPuciBz6LLK.htm](pathfinder-bestiary-items/deSS0MPuciBz6LLK.htm)|Syringe|auto-trad|
|[dF7ibA1On3Nvjxqk.htm](pathfinder-bestiary-items/dF7ibA1On3Nvjxqk.htm)|Hurried Retreat|auto-trad|
|[DFaamiHPt04lXEiq.htm](pathfinder-bestiary-items/DFaamiHPt04lXEiq.htm)|Mucus Cloud|auto-trad|
|[DFldqlT6ytrPqmmo.htm](pathfinder-bestiary-items/DFldqlT6ytrPqmmo.htm)|Bastard Sword|auto-trad|
|[DfnmY7gUQzG365ax.htm](pathfinder-bestiary-items/DfnmY7gUQzG365ax.htm)|Web Wrappings|auto-trad|
|[DG1zbZJXQydDgeBF.htm](pathfinder-bestiary-items/DG1zbZJXQydDgeBF.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[DG6zH2k37QwOHJ9R.htm](pathfinder-bestiary-items/DG6zH2k37QwOHJ9R.htm)|Web Burst|auto-trad|
|[DGMkGaAm8sHK2QzD.htm](pathfinder-bestiary-items/DGMkGaAm8sHK2QzD.htm)|Beak|auto-trad|
|[dH7eKw8pDJQDkR3P.htm](pathfinder-bestiary-items/dH7eKw8pDJQDkR3P.htm)|Constrict|auto-trad|
|[DI6Nw0XKEZZJLY8D.htm](pathfinder-bestiary-items/DI6Nw0XKEZZJLY8D.htm)|Motion Sense 60 feet|auto-trad|
|[DiC6r2dHBydpjZtt.htm](pathfinder-bestiary-items/DiC6r2dHBydpjZtt.htm)|Breath Weapon|auto-trad|
|[DicxiMfpeviQRxSo.htm](pathfinder-bestiary-items/DicxiMfpeviQRxSo.htm)|Reel In|auto-trad|
|[dId4UcnaI1F6ww0V.htm](pathfinder-bestiary-items/dId4UcnaI1F6ww0V.htm)|Frost Vial|auto-trad|
|[DiGgJNOY5Na3eYp8.htm](pathfinder-bestiary-items/DiGgJNOY5Na3eYp8.htm)|Filth Fever|auto-trad|
|[dIKN1uas299xNK54.htm](pathfinder-bestiary-items/dIKN1uas299xNK54.htm)|Stinger|auto-trad|
|[DiXBVXgoU2aPsjbA.htm](pathfinder-bestiary-items/DiXBVXgoU2aPsjbA.htm)|Darkvision|auto-trad|
|[DJ1EqBElcu3IaCwK.htm](pathfinder-bestiary-items/DJ1EqBElcu3IaCwK.htm)|Low-Light Vision|auto-trad|
|[dJE6KlD0Qh5yDU7w.htm](pathfinder-bestiary-items/dJE6KlD0Qh5yDU7w.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[DjE8LDaTqE6Y8m1b.htm](pathfinder-bestiary-items/DjE8LDaTqE6Y8m1b.htm)|Daemonic Pestilence|auto-trad|
|[DjU3nUmp5OOFcVhl.htm](pathfinder-bestiary-items/DjU3nUmp5OOFcVhl.htm)|Darkvision|auto-trad|
|[dJxa1u0JvHmiIXKG.htm](pathfinder-bestiary-items/dJxa1u0JvHmiIXKG.htm)|Sound Imitation|auto-trad|
|[dK5Meg25B2t4B34g.htm](pathfinder-bestiary-items/dK5Meg25B2t4B34g.htm)|Rejection Vulnerability|auto-trad|
|[dkfqFFDoPeLbQKNp.htm](pathfinder-bestiary-items/dkfqFFDoPeLbQKNp.htm)|Construct Armor (Hardness 6)|auto-trad|
|[Dkveyw8m1JIyOZch.htm](pathfinder-bestiary-items/Dkveyw8m1JIyOZch.htm)|Darkvision|auto-trad|
|[DLFCqMiTmmTYgmtk.htm](pathfinder-bestiary-items/DLFCqMiTmmTYgmtk.htm)|Aklys|auto-trad|
|[dlQJzPWPs48f9b4c.htm](pathfinder-bestiary-items/dlQJzPWPs48f9b4c.htm)|Sneak Attack|auto-trad|
|[DlqsOMt7KvKvez2f.htm](pathfinder-bestiary-items/DlqsOMt7KvKvez2f.htm)|Draconic Frenzy|auto-trad|
|[DM0bCE11AyauvvEH.htm](pathfinder-bestiary-items/DM0bCE11AyauvvEH.htm)|Constant Spells|auto-trad|
|[DmNGJHLmXcMGdmUS.htm](pathfinder-bestiary-items/DmNGJHLmXcMGdmUS.htm)|Orc Knuckle Dagger|auto-trad|
|[dmZlzmRsiydMPknc.htm](pathfinder-bestiary-items/dmZlzmRsiydMPknc.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[DmZnAZGJF17LOU4y.htm](pathfinder-bestiary-items/DmZnAZGJF17LOU4y.htm)|Free Blade|auto-trad|
|[Dn7B8j6FSbVD4U9x.htm](pathfinder-bestiary-items/Dn7B8j6FSbVD4U9x.htm)|Stench|auto-trad|
|[dNxKxgTAaIDS3UWL.htm](pathfinder-bestiary-items/dNxKxgTAaIDS3UWL.htm)|Low-Light Vision|auto-trad|
|[dOeNHCZ73nqH6mGc.htm](pathfinder-bestiary-items/dOeNHCZ73nqH6mGc.htm)|At-Will Spells|auto-trad|
|[doHZQiWCurTpCiMw.htm](pathfinder-bestiary-items/doHZQiWCurTpCiMw.htm)|Constant Spells|auto-trad|
|[DOoR2SsbZF96IxlF.htm](pathfinder-bestiary-items/DOoR2SsbZF96IxlF.htm)|Coven Spells|auto-trad|
|[DOr9FvQdsMfK9He0.htm](pathfinder-bestiary-items/DOr9FvQdsMfK9He0.htm)|Darkvision|auto-trad|
|[DOWxg5PQXXUG8Ddy.htm](pathfinder-bestiary-items/DOWxg5PQXXUG8Ddy.htm)|Beak|auto-trad|
|[dp1n1eKrdXWBGI47.htm](pathfinder-bestiary-items/dp1n1eKrdXWBGI47.htm)|Jaws|auto-trad|
|[DpI5QZQJMNjqef3w.htm](pathfinder-bestiary-items/DpI5QZQJMNjqef3w.htm)|Surprise Attacker|auto-trad|
|[dpljRBjacJBvCCw1.htm](pathfinder-bestiary-items/dpljRBjacJBvCCw1.htm)|Darkvision|auto-trad|
|[dPsbBLbGfNPn6v9L.htm](pathfinder-bestiary-items/dPsbBLbGfNPn6v9L.htm)|Breath Weapon|auto-trad|
|[DqJFXur9S0e0C54j.htm](pathfinder-bestiary-items/DqJFXur9S0e0C54j.htm)|Darkvision|auto-trad|
|[dRF3nPzB91HYIbXx.htm](pathfinder-bestiary-items/dRF3nPzB91HYIbXx.htm)|Ice Climb|auto-trad|
|[drMUmP8Z6LhZyhRI.htm](pathfinder-bestiary-items/drMUmP8Z6LhZyhRI.htm)|Carry Off Prey|auto-trad|
|[DRnLEIRSbgIT3V0F.htm](pathfinder-bestiary-items/DRnLEIRSbgIT3V0F.htm)|Hand Crossbow|auto-trad|
|[dRtVAtuxu9SD6zpN.htm](pathfinder-bestiary-items/dRtVAtuxu9SD6zpN.htm)|Darkvision|auto-trad|
|[dsA6QTAsFP5Juo0N.htm](pathfinder-bestiary-items/dsA6QTAsFP5Juo0N.htm)|Jaws|auto-trad|
|[dSMYHVMIltxeN4ms.htm](pathfinder-bestiary-items/dSMYHVMIltxeN4ms.htm)|Negative Healing|auto-trad|
|[DsvyLX53TeuZHMQ3.htm](pathfinder-bestiary-items/DsvyLX53TeuZHMQ3.htm)|Clawed Feet|auto-trad|
|[DtiXNG3ROuAaBhN3.htm](pathfinder-bestiary-items/DtiXNG3ROuAaBhN3.htm)|Constrict|auto-trad|
|[DTXq4teaiLU9haNE.htm](pathfinder-bestiary-items/DTXq4teaiLU9haNE.htm)|Wide Swing|auto-trad|
|[DuOMgloNkM9gTAZj.htm](pathfinder-bestiary-items/DuOMgloNkM9gTAZj.htm)|Attack of Opportunity|auto-trad|
|[dupLF5DYOhmcq8LG.htm](pathfinder-bestiary-items/dupLF5DYOhmcq8LG.htm)|Tail|auto-trad|
|[DV3sGNI2RVQcTbmS.htm](pathfinder-bestiary-items/DV3sGNI2RVQcTbmS.htm)|Darkvision|auto-trad|
|[DVDscRqpim10UFQ4.htm](pathfinder-bestiary-items/DVDscRqpim10UFQ4.htm)|Constant Spells|auto-trad|
|[dvenwngmH8FFaPNZ.htm](pathfinder-bestiary-items/dvenwngmH8FFaPNZ.htm)|Arcane Innate Spells|auto-trad|
|[dVGwhqUf0F3FAVqi.htm](pathfinder-bestiary-items/dVGwhqUf0F3FAVqi.htm)|Primal Innate Spells|auto-trad|
|[DvjFG8xZGQmhN7AK.htm](pathfinder-bestiary-items/DvjFG8xZGQmhN7AK.htm)|Glutton's Rush|auto-trad|
|[dvuK2slsg05eHdpZ.htm](pathfinder-bestiary-items/dvuK2slsg05eHdpZ.htm)|Fist|auto-trad|
|[dWgrFKYnx0FM8zl3.htm](pathfinder-bestiary-items/dWgrFKYnx0FM8zl3.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[DWRBHDiwImN0PCp9.htm](pathfinder-bestiary-items/DWRBHDiwImN0PCp9.htm)|Overwhelming Breath|auto-trad|
|[dX7SlBGB2vdMkHMB.htm](pathfinder-bestiary-items/dX7SlBGB2vdMkHMB.htm)|Low-Light Vision|auto-trad|
|[dXBqzoJaaPfabPMM.htm](pathfinder-bestiary-items/dXBqzoJaaPfabPMM.htm)|Constant Spells|auto-trad|
|[DxjaCZWO3bSIqi7x.htm](pathfinder-bestiary-items/DxjaCZWO3bSIqi7x.htm)|Arcane Innate Spells|auto-trad|
|[DxJcguaM5VY5ipoX.htm](pathfinder-bestiary-items/DxJcguaM5VY5ipoX.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[dxLrvmfMtHkddYo4.htm](pathfinder-bestiary-items/dxLrvmfMtHkddYo4.htm)|Spirit Touch|auto-trad|
|[DXn63TBMmkKeN9Wu.htm](pathfinder-bestiary-items/DXn63TBMmkKeN9Wu.htm)|Coven|auto-trad|
|[DxRFNpdsIZg6cyAK.htm](pathfinder-bestiary-items/DxRFNpdsIZg6cyAK.htm)|Flash of Insight|auto-trad|
|[Dy5EMIJUvL46ibmR.htm](pathfinder-bestiary-items/Dy5EMIJUvL46ibmR.htm)|Fast Healing 20|auto-trad|
|[DyABhSgZNJ3ICo85.htm](pathfinder-bestiary-items/DyABhSgZNJ3ICo85.htm)|Spear|auto-trad|
|[DYRACVN0h19oKZpk.htm](pathfinder-bestiary-items/DYRACVN0h19oKZpk.htm)|Radiance Dependence|auto-trad|
|[dZJ67fQHX1jc4FRl.htm](pathfinder-bestiary-items/dZJ67fQHX1jc4FRl.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[DzJcNWKMqkWqtgAX.htm](pathfinder-bestiary-items/DzJcNWKMqkWqtgAX.htm)|Glaive|auto-trad|
|[DZQzqTM8bO1vt4ps.htm](pathfinder-bestiary-items/DZQzqTM8bO1vt4ps.htm)|Extend Strands|auto-trad|
|[dZrpK1Q6edVEhAZ7.htm](pathfinder-bestiary-items/dZrpK1Q6edVEhAZ7.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[DzWzbp1qMJwPTy0T.htm](pathfinder-bestiary-items/DzWzbp1qMJwPTy0T.htm)|Shield Block|auto-trad|
|[Dzxj1t6HVexa7Wyz.htm](pathfinder-bestiary-items/Dzxj1t6HVexa7Wyz.htm)|Plummet|auto-trad|
|[DZXUlh01CHmN4S3z.htm](pathfinder-bestiary-items/DZXUlh01CHmN4S3z.htm)|Jaws|auto-trad|
|[dZZfqVkrZJJMxi6A.htm](pathfinder-bestiary-items/dZZfqVkrZJJMxi6A.htm)|Low-Light Vision|auto-trad|
|[dzZj81bjKpvnxgLG.htm](pathfinder-bestiary-items/dzZj81bjKpvnxgLG.htm)|Darkvision|auto-trad|
|[e0fcFHdf91dsur91.htm](pathfinder-bestiary-items/e0fcFHdf91dsur91.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[E0hXra1OswZG4gQM.htm](pathfinder-bestiary-items/E0hXra1OswZG4gQM.htm)|Reactive Lash|auto-trad|
|[E11uRLNnDwbjSmiw.htm](pathfinder-bestiary-items/E11uRLNnDwbjSmiw.htm)|Change Shape|auto-trad|
|[e1GGSqrMS3yqPrVw.htm](pathfinder-bestiary-items/e1GGSqrMS3yqPrVw.htm)|Fiddle|auto-trad|
|[E36GUJSTq2NvkRmA.htm](pathfinder-bestiary-items/E36GUJSTq2NvkRmA.htm)|Confessor's Aura|auto-trad|
|[e3mEtYHIO4f0NZZc.htm](pathfinder-bestiary-items/e3mEtYHIO4f0NZZc.htm)|Arcane Prepared Spells|auto-trad|
|[E3WrwhsH06axDfAu.htm](pathfinder-bestiary-items/E3WrwhsH06axDfAu.htm)|Improved Grab|auto-trad|
|[E5pqXYYcXl9kYesX.htm](pathfinder-bestiary-items/E5pqXYYcXl9kYesX.htm)|Tail|auto-trad|
|[e5zu7QFkUzeeeoVM.htm](pathfinder-bestiary-items/e5zu7QFkUzeeeoVM.htm)|Smoke Vision|auto-trad|
|[e6gQA5jXDMbCKyDj.htm](pathfinder-bestiary-items/e6gQA5jXDMbCKyDj.htm)|Swarming Bites|auto-trad|
|[E6yHKgr49GpD3BXa.htm](pathfinder-bestiary-items/E6yHKgr49GpD3BXa.htm)|Abyssal Knowledge|auto-trad|
|[e7CxhtU5yoASkey8.htm](pathfinder-bestiary-items/e7CxhtU5yoASkey8.htm)|Swift Leap|auto-trad|
|[e7Rk8yVGVqEP62yA.htm](pathfinder-bestiary-items/e7Rk8yVGVqEP62yA.htm)|Divine Innate Spells|auto-trad|
|[e7t6qbZcSApHA7cS.htm](pathfinder-bestiary-items/e7t6qbZcSApHA7cS.htm)|Undetectable|auto-trad|
|[e800cwjTJcAqZ2x0.htm](pathfinder-bestiary-items/e800cwjTJcAqZ2x0.htm)|Feed|auto-trad|
|[e8BUlHLevzRJnHEc.htm](pathfinder-bestiary-items/e8BUlHLevzRJnHEc.htm)|Bushwhack|auto-trad|
|[e8oPQH0Ss9z5oimc.htm](pathfinder-bestiary-items/e8oPQH0Ss9z5oimc.htm)|Divine Innate Spells|auto-trad|
|[E8SrLXAiFextgqNP.htm](pathfinder-bestiary-items/E8SrLXAiFextgqNP.htm)|Jaws|auto-trad|
|[EaebstxdYxHFj1bR.htm](pathfinder-bestiary-items/EaebstxdYxHFj1bR.htm)|Jaws|auto-trad|
|[EajnVKV3NKyICaQ8.htm](pathfinder-bestiary-items/EajnVKV3NKyICaQ8.htm)|Hand Crossbow|auto-trad|
|[Eaucw0lxqJ7eKezw.htm](pathfinder-bestiary-items/Eaucw0lxqJ7eKezw.htm)|Low-Light Vision|auto-trad|
|[Eb5mdjpNuWacl147.htm](pathfinder-bestiary-items/Eb5mdjpNuWacl147.htm)|Divine Innate Spells|auto-trad|
|[eBfnitbq2Wb56ZRG.htm](pathfinder-bestiary-items/eBfnitbq2Wb56ZRG.htm)|Flaming Strafe|auto-trad|
|[EbIYH4fdrCUr8wIS.htm](pathfinder-bestiary-items/EbIYH4fdrCUr8wIS.htm)|Force Body|auto-trad|
|[ebr1KbgBZOCMt92B.htm](pathfinder-bestiary-items/ebr1KbgBZOCMt92B.htm)|Jaws|auto-trad|
|[EbuMoQ5idyQJDEXD.htm](pathfinder-bestiary-items/EbuMoQ5idyQJDEXD.htm)|Paralyzing Gaze|auto-trad|
|[EC797WDulwqw96ch.htm](pathfinder-bestiary-items/EC797WDulwqw96ch.htm)|Darkvision|auto-trad|
|[eczygEywMNK3dNd7.htm](pathfinder-bestiary-items/eczygEywMNK3dNd7.htm)|Divine Innate Spells|auto-trad|
|[eDKxGQf3yPUpgqh0.htm](pathfinder-bestiary-items/eDKxGQf3yPUpgqh0.htm)|Attack of Opportunity (Special)|auto-trad|
|[EdlfzUPi8lEnewu1.htm](pathfinder-bestiary-items/EdlfzUPi8lEnewu1.htm)|Dual Tusks|auto-trad|
|[eDMgpSPjK69nCKMZ.htm](pathfinder-bestiary-items/eDMgpSPjK69nCKMZ.htm)|Wing|auto-trad|
|[Eejxi0k01WzbvUUh.htm](pathfinder-bestiary-items/Eejxi0k01WzbvUUh.htm)|Shield Block|auto-trad|
|[EEVvFBuUd4ji3NX1.htm](pathfinder-bestiary-items/EEVvFBuUd4ji3NX1.htm)|Darkvision|auto-trad|
|[eEwJYKPQM7YTAFt4.htm](pathfinder-bestiary-items/eEwJYKPQM7YTAFt4.htm)|Occult Innate Spells|auto-trad|
|[Ef73Htwp9vIyrZWi.htm](pathfinder-bestiary-items/Ef73Htwp9vIyrZWi.htm)|Darkvision|auto-trad|
|[efQXyayOwi0nURrx.htm](pathfinder-bestiary-items/efQXyayOwi0nURrx.htm)|Darkvision|auto-trad|
|[eFUBcFVqZ2zpFt8W.htm](pathfinder-bestiary-items/eFUBcFVqZ2zpFt8W.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[eG1Bf18XYFvkALTq.htm](pathfinder-bestiary-items/eG1Bf18XYFvkALTq.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[eg7TTGELwmYoMNM7.htm](pathfinder-bestiary-items/eg7TTGELwmYoMNM7.htm)|Seed|auto-trad|
|[eGGqJI9VH7B5iZnC.htm](pathfinder-bestiary-items/eGGqJI9VH7B5iZnC.htm)|Aquatic Ambush|auto-trad|
|[eGqp10EE3Ein6SqI.htm](pathfinder-bestiary-items/eGqp10EE3Ein6SqI.htm)|Tusk|auto-trad|
|[egrcinyTFaF0OUfA.htm](pathfinder-bestiary-items/egrcinyTFaF0OUfA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[egV0y60X9Okkmuoc.htm](pathfinder-bestiary-items/egV0y60X9Okkmuoc.htm)|Hunting Spider Venom|auto-trad|
|[EhdD8hFrqyISTFtx.htm](pathfinder-bestiary-items/EhdD8hFrqyISTFtx.htm)|Primal Prepared Spells|auto-trad|
|[Ehvmz1GzuHGjHUz3.htm](pathfinder-bestiary-items/Ehvmz1GzuHGjHUz3.htm)|Tail|auto-trad|
|[EhZr8qLYudW24Syo.htm](pathfinder-bestiary-items/EhZr8qLYudW24Syo.htm)|Darkvision|auto-trad|
|[eI1CqQ6uSb4iGiLS.htm](pathfinder-bestiary-items/eI1CqQ6uSb4iGiLS.htm)|Sneak Attack|auto-trad|
|[ei3nQVCq6MHIPrMy.htm](pathfinder-bestiary-items/ei3nQVCq6MHIPrMy.htm)|Lurking Death|auto-trad|
|[EI3OMbX3GaC67Bnt.htm](pathfinder-bestiary-items/EI3OMbX3GaC67Bnt.htm)|Darkvision|auto-trad|
|[eIFUAYDyIaHqOP6A.htm](pathfinder-bestiary-items/eIFUAYDyIaHqOP6A.htm)|Primal Innate Spells|auto-trad|
|[EIOiFljIAD1Zzdzr.htm](pathfinder-bestiary-items/EIOiFljIAD1Zzdzr.htm)|Spike Stones|auto-trad|
|[eIPX17zhmSnmtddW.htm](pathfinder-bestiary-items/eIPX17zhmSnmtddW.htm)|Darkvision|auto-trad|
|[eIQH5yy2l8qe0bvR.htm](pathfinder-bestiary-items/eIQH5yy2l8qe0bvR.htm)|Constrict|auto-trad|
|[EISoRK24Azg22k0m.htm](pathfinder-bestiary-items/EISoRK24Azg22k0m.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[Eizm8JTXmxGFPtFL.htm](pathfinder-bestiary-items/Eizm8JTXmxGFPtFL.htm)|Verdant Burst|auto-trad|
|[Ej293LsxUUFjWNK8.htm](pathfinder-bestiary-items/Ej293LsxUUFjWNK8.htm)|Spear|auto-trad|
|[EkSi5KM6hOWO5WxD.htm](pathfinder-bestiary-items/EkSi5KM6hOWO5WxD.htm)|Kraken Ink|auto-trad|
|[el4yrHrtMr3LobtV.htm](pathfinder-bestiary-items/el4yrHrtMr3LobtV.htm)|Gust|auto-trad|
|[El6yEwdcVoYxSpbv.htm](pathfinder-bestiary-items/El6yEwdcVoYxSpbv.htm)|Claw|auto-trad|
|[elZBS29nJWZadS4G.htm](pathfinder-bestiary-items/elZBS29nJWZadS4G.htm)|Wing Deflection|auto-trad|
|[EmEYyJxT9UcKh4lK.htm](pathfinder-bestiary-items/EmEYyJxT9UcKh4lK.htm)|Jaws|auto-trad|
|[emPcqeSFXdyr6CgV.htm](pathfinder-bestiary-items/emPcqeSFXdyr6CgV.htm)|Terrifying Charge|auto-trad|
|[Emupr9krjKNn5xjb.htm](pathfinder-bestiary-items/Emupr9krjKNn5xjb.htm)|Mauler|auto-trad|
|[EMWzNZneWpK3nLER.htm](pathfinder-bestiary-items/EMWzNZneWpK3nLER.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[EMzbdikMCvVBaXbA.htm](pathfinder-bestiary-items/EMzbdikMCvVBaXbA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[EnBK4J1yRnqIdTCA.htm](pathfinder-bestiary-items/EnBK4J1yRnqIdTCA.htm)|Battle Axe|auto-trad|
|[endjF5MDlvADZc05.htm](pathfinder-bestiary-items/endjF5MDlvADZc05.htm)|Divine Innate Spells|auto-trad|
|[Eng6Mv69WWeKb4Wq.htm](pathfinder-bestiary-items/Eng6Mv69WWeKb4Wq.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[enj0s2MOCB4NTllp.htm](pathfinder-bestiary-items/enj0s2MOCB4NTllp.htm)|Draconic Momentum|auto-trad|
|[ENSJ3YzJmKPzUCB9.htm](pathfinder-bestiary-items/ENSJ3YzJmKPzUCB9.htm)|Divine Rituals|auto-trad|
|[eNTumcCOMnTdNzht.htm](pathfinder-bestiary-items/eNTumcCOMnTdNzht.htm)|Earth Glide|auto-trad|
|[eoAPr8flq8fSsMDX.htm](pathfinder-bestiary-items/eoAPr8flq8fSsMDX.htm)|Occult Rituals|auto-trad|
|[eOgrqVcnppzRiPms.htm](pathfinder-bestiary-items/eOgrqVcnppzRiPms.htm)|Push 10 feet|auto-trad|
|[eoHwXTgs9Au82i4J.htm](pathfinder-bestiary-items/eoHwXTgs9Au82i4J.htm)|Nightmare Guardian|auto-trad|
|[eOj1lBtBYURSS36K.htm](pathfinder-bestiary-items/eOj1lBtBYURSS36K.htm)|Glimpse of Redemption|auto-trad|
|[eOkkPMnXhWSlJr5h.htm](pathfinder-bestiary-items/eOkkPMnXhWSlJr5h.htm)|Divine Innate Spells|auto-trad|
|[ePRrl3MsWtcXXzD7.htm](pathfinder-bestiary-items/ePRrl3MsWtcXXzD7.htm)|Terrifying Touch|auto-trad|
|[epscB9eQsqVToqtK.htm](pathfinder-bestiary-items/epscB9eQsqVToqtK.htm)|Rock Or Metal Debris|auto-trad|
|[eqCGFR9UmHOuUpMI.htm](pathfinder-bestiary-items/eqCGFR9UmHOuUpMI.htm)|Greataxe|auto-trad|
|[eqiwUtaFLOTC1oND.htm](pathfinder-bestiary-items/eqiwUtaFLOTC1oND.htm)|Divine Innate Spells|auto-trad|
|[eqROgg0bgVWksPcZ.htm](pathfinder-bestiary-items/eqROgg0bgVWksPcZ.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[EQs2G0f4rbGphFRv.htm](pathfinder-bestiary-items/EQs2G0f4rbGphFRv.htm)|Banishing Swipe|auto-trad|
|[ERaLBCCkqUJWJVvG.htm](pathfinder-bestiary-items/ERaLBCCkqUJWJVvG.htm)|Buck|auto-trad|
|[erc12GafQUSAqN7h.htm](pathfinder-bestiary-items/erc12GafQUSAqN7h.htm)|Pummel|auto-trad|
|[ERGEti8Jc1RonWRj.htm](pathfinder-bestiary-items/ERGEti8Jc1RonWRj.htm)|Change Shape|auto-trad|
|[ErhX9xAIKos8xjgd.htm](pathfinder-bestiary-items/ErhX9xAIKos8xjgd.htm)|Trident|auto-trad|
|[ErNsSmjjx1k38FuX.htm](pathfinder-bestiary-items/ErNsSmjjx1k38FuX.htm)|Regeneration 10 (Deactivated by Cold Iron)|auto-trad|
|[erqQJ4VimGjwkftc.htm](pathfinder-bestiary-items/erqQJ4VimGjwkftc.htm)|Arcane Innate Spells|auto-trad|
|[esaeLxI8stHVxilE.htm](pathfinder-bestiary-items/esaeLxI8stHVxilE.htm)|Twisting Tail|auto-trad|
|[escewXwusyFQw1t9.htm](pathfinder-bestiary-items/escewXwusyFQw1t9.htm)|Breath Weapon|auto-trad|
|[ESgOv0J4fLFsukAx.htm](pathfinder-bestiary-items/ESgOv0J4fLFsukAx.htm)|Darkvision|auto-trad|
|[esNHCG2RP8SOfTuA.htm](pathfinder-bestiary-items/esNHCG2RP8SOfTuA.htm)|Axe Vulnerability|auto-trad|
|[ESSGqodO4c5dPMOG.htm](pathfinder-bestiary-items/ESSGqodO4c5dPMOG.htm)|Jaws|auto-trad|
|[ESyhQLjcnQvrHNwr.htm](pathfinder-bestiary-items/ESyhQLjcnQvrHNwr.htm)|Tail|auto-trad|
|[ETwwR7V1iIbvqCuB.htm](pathfinder-bestiary-items/ETwwR7V1iIbvqCuB.htm)|Claw|auto-trad|
|[Eu72dpTXiYJQD7Hm.htm](pathfinder-bestiary-items/Eu72dpTXiYJQD7Hm.htm)|Web Tether|auto-trad|
|[EUf1gqfe5un9Aprz.htm](pathfinder-bestiary-items/EUf1gqfe5un9Aprz.htm)|Vine|auto-trad|
|[eUjlMs7XKcwlCfZQ.htm](pathfinder-bestiary-items/eUjlMs7XKcwlCfZQ.htm)|Claw|auto-trad|
|[eulyI60JHNUYs39w.htm](pathfinder-bestiary-items/eulyI60JHNUYs39w.htm)|Slow|auto-trad|
|[eumI77vCN35YAXRJ.htm](pathfinder-bestiary-items/eumI77vCN35YAXRJ.htm)|Divine Prepared Spells|auto-trad|
|[euRdPTOZHeLTJZHA.htm](pathfinder-bestiary-items/euRdPTOZHeLTJZHA.htm)|Swiftness|auto-trad|
|[eUtG7jyrUZUQhm6G.htm](pathfinder-bestiary-items/eUtG7jyrUZUQhm6G.htm)|Divine Innate Spells|auto-trad|
|[eWc160opF23yBpjF.htm](pathfinder-bestiary-items/eWc160opF23yBpjF.htm)|Darkvision|auto-trad|
|[eWlvpYGryxxhSJkS.htm](pathfinder-bestiary-items/eWlvpYGryxxhSJkS.htm)|Infernal Investment|auto-trad|
|[EwP4uiI1GXQ7aYst.htm](pathfinder-bestiary-items/EwP4uiI1GXQ7aYst.htm)|Thoughtlance|auto-trad|
|[EWqlu1saEnvsN9kN.htm](pathfinder-bestiary-items/EWqlu1saEnvsN9kN.htm)|Fist|auto-trad|
|[EwYIwG40atiWUwhI.htm](pathfinder-bestiary-items/EwYIwG40atiWUwhI.htm)|Breath Weapon|auto-trad|
|[ex0dTLedYlwxNB3T.htm](pathfinder-bestiary-items/ex0dTLedYlwxNB3T.htm)|Swallow Whole|auto-trad|
|[ExqWcZmrd3RhXgLt.htm](pathfinder-bestiary-items/ExqWcZmrd3RhXgLt.htm)|Darkvision|auto-trad|
|[exYCtLHRcdeuOCdp.htm](pathfinder-bestiary-items/exYCtLHRcdeuOCdp.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[eYD6EiiIYvny4Vq2.htm](pathfinder-bestiary-items/eYD6EiiIYvny4Vq2.htm)|Golem Antimagic|auto-trad|
|[EydNDPNpUXcKp0PD.htm](pathfinder-bestiary-items/EydNDPNpUXcKp0PD.htm)|Draconic Frenzy|auto-trad|
|[EyTNAb0HgeYGoaPL.htm](pathfinder-bestiary-items/EyTNAb0HgeYGoaPL.htm)|Arcane Innate Spells|auto-trad|
|[EzSKWtWM2tSTtZwA.htm](pathfinder-bestiary-items/EzSKWtWM2tSTtZwA.htm)|Claw|auto-trad|
|[f024HTNWMSNizvqn.htm](pathfinder-bestiary-items/f024HTNWMSNizvqn.htm)|Jaws|auto-trad|
|[f0JUzUCQYwMZkmpA.htm](pathfinder-bestiary-items/f0JUzUCQYwMZkmpA.htm)|Darkvision|auto-trad|
|[F0YALhW0VHUpxUtT.htm](pathfinder-bestiary-items/F0YALhW0VHUpxUtT.htm)|Low-Light Vision|auto-trad|
|[F2mCARScyPh6sD0k.htm](pathfinder-bestiary-items/F2mCARScyPh6sD0k.htm)|Smoke Vision|auto-trad|
|[f3DzwJJdLBEbrQYp.htm](pathfinder-bestiary-items/f3DzwJJdLBEbrQYp.htm)|Darkvision|auto-trad|
|[f4rcDncEwu2OdC1a.htm](pathfinder-bestiary-items/f4rcDncEwu2OdC1a.htm)|Jaws|auto-trad|
|[f4yHe8ZeeX1UHyWc.htm](pathfinder-bestiary-items/f4yHe8ZeeX1UHyWc.htm)|Javelin|auto-trad|
|[f52Whk1lgUmI3JmT.htm](pathfinder-bestiary-items/f52Whk1lgUmI3JmT.htm)|High Winds|auto-trad|
|[F8wXzsUNliJVU8bv.htm](pathfinder-bestiary-items/F8wXzsUNliJVU8bv.htm)|Composite Shortbow|auto-trad|
|[F8xpT7h5a5ZviXU5.htm](pathfinder-bestiary-items/F8xpT7h5a5ZviXU5.htm)|Feed on Fear|auto-trad|
|[Fa3Nn9PdJzbO1Fwg.htm](pathfinder-bestiary-items/Fa3Nn9PdJzbO1Fwg.htm)|Dart|auto-trad|
|[FaGxK1ootpG6uFmP.htm](pathfinder-bestiary-items/FaGxK1ootpG6uFmP.htm)|Low-Light Vision|auto-trad|
|[FAqkYvZwqQTcg84m.htm](pathfinder-bestiary-items/FAqkYvZwqQTcg84m.htm)|Primal Rituals|auto-trad|
|[Fb73mys1NUmxKq5Q.htm](pathfinder-bestiary-items/Fb73mys1NUmxKq5Q.htm)|Attack of Opportunity|auto-trad|
|[fb8hCFDmwVrYAiRL.htm](pathfinder-bestiary-items/fb8hCFDmwVrYAiRL.htm)|At-Will Spells|auto-trad|
|[Fc10tuSKXi4OAWw6.htm](pathfinder-bestiary-items/Fc10tuSKXi4OAWw6.htm)|Deep Plunge|auto-trad|
|[fchD9qieGM9L8Vag.htm](pathfinder-bestiary-items/fchD9qieGM9L8Vag.htm)|Breath Weapon|auto-trad|
|[fCL0YMWwrX0efcpJ.htm](pathfinder-bestiary-items/fCL0YMWwrX0efcpJ.htm)|Draconic Momentum|auto-trad|
|[fctwwJ6S2jeNgFwD.htm](pathfinder-bestiary-items/fctwwJ6S2jeNgFwD.htm)|Constant Spells|auto-trad|
|[FCz6EfXJsvdhlDq5.htm](pathfinder-bestiary-items/FCz6EfXJsvdhlDq5.htm)|Tail|auto-trad|
|[fD21lHfQwFLm6D9N.htm](pathfinder-bestiary-items/fD21lHfQwFLm6D9N.htm)|Claw|auto-trad|
|[fd6LoAjku28Cm4oo.htm](pathfinder-bestiary-items/fd6LoAjku28Cm4oo.htm)|Kukri|auto-trad|
|[FdJtaUOMkZdu6F7C.htm](pathfinder-bestiary-items/FdJtaUOMkZdu6F7C.htm)|Fangs|auto-trad|
|[fDmw9feXNMKuAci4.htm](pathfinder-bestiary-items/fDmw9feXNMKuAci4.htm)|Telepathy 100 feet|auto-trad|
|[FdQL1f8wOuuy69Tz.htm](pathfinder-bestiary-items/FdQL1f8wOuuy69Tz.htm)|Sneak Attack|auto-trad|
|[FDVxe2jOvdrTKmQY.htm](pathfinder-bestiary-items/FDVxe2jOvdrTKmQY.htm)|Draconic Momentum|auto-trad|
|[FE2mBRuPXC4LPn2b.htm](pathfinder-bestiary-items/FE2mBRuPXC4LPn2b.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[fEbZopQgOxVNZDnI.htm](pathfinder-bestiary-items/fEbZopQgOxVNZDnI.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[fegp7nmVFKZeTSA5.htm](pathfinder-bestiary-items/fegp7nmVFKZeTSA5.htm)|Piercing Hymn|auto-trad|
|[FEiMSx6vAjQ1oKaY.htm](pathfinder-bestiary-items/FEiMSx6vAjQ1oKaY.htm)|Swallow Whole|auto-trad|
|[Ff5wpbYD7hi6qmph.htm](pathfinder-bestiary-items/Ff5wpbYD7hi6qmph.htm)|Constant Spells|auto-trad|
|[FFjYHtFi991MRIlJ.htm](pathfinder-bestiary-items/FFjYHtFi991MRIlJ.htm)|Fire Mote|auto-trad|
|[ffZLFXRhI2leU0qq.htm](pathfinder-bestiary-items/ffZLFXRhI2leU0qq.htm)|Push 5 feet|auto-trad|
|[fG5TPYbHYwwOdPbD.htm](pathfinder-bestiary-items/fG5TPYbHYwwOdPbD.htm)|Claw|auto-trad|
|[FGDntcY8lQY0p68N.htm](pathfinder-bestiary-items/FGDntcY8lQY0p68N.htm)|Guisarme|auto-trad|
|[FGm1xbUPgE0BRiSf.htm](pathfinder-bestiary-items/FGm1xbUPgE0BRiSf.htm)|Focused Assault|auto-trad|
|[fgRAIdUznaID2VqA.htm](pathfinder-bestiary-items/fgRAIdUznaID2VqA.htm)|Smoke Vision|auto-trad|
|[fHam88iT0r3U5tcl.htm](pathfinder-bestiary-items/fHam88iT0r3U5tcl.htm)|Tinker|auto-trad|
|[fhjQVAdn1BOQnXvF.htm](pathfinder-bestiary-items/fhjQVAdn1BOQnXvF.htm)|Darkvision|auto-trad|
|[FhLr7TEb7n9FRT5I.htm](pathfinder-bestiary-items/FhLr7TEb7n9FRT5I.htm)|Pack Attack|auto-trad|
|[FHuTUU6djcJ06GPe.htm](pathfinder-bestiary-items/FHuTUU6djcJ06GPe.htm)|Foot|auto-trad|
|[FHYZijWKqxyPWq4b.htm](pathfinder-bestiary-items/FHYZijWKqxyPWq4b.htm)|Construct Armor (Hardness 10)|auto-trad|
|[fI8bsqTQlNTlSpdF.htm](pathfinder-bestiary-items/fI8bsqTQlNTlSpdF.htm)|Greatclub|auto-trad|
|[fifv91vxtwlIpzXx.htm](pathfinder-bestiary-items/fifv91vxtwlIpzXx.htm)|Fast Swallow|auto-trad|
|[fIssfjOuDo0yfwLc.htm](pathfinder-bestiary-items/fIssfjOuDo0yfwLc.htm)|Eerie Flexibility|auto-trad|
|[FJrLVkHRXOOEh4xr.htm](pathfinder-bestiary-items/FJrLVkHRXOOEh4xr.htm)|Darkvision|auto-trad|
|[fjsDRLGq7Hpbxrfn.htm](pathfinder-bestiary-items/fjsDRLGq7Hpbxrfn.htm)|Bear Empathy|auto-trad|
|[fK43gOGpvZ5SUgT8.htm](pathfinder-bestiary-items/fK43gOGpvZ5SUgT8.htm)|Push 10 feet|auto-trad|
|[FKd3nk9u6PpHnAtX.htm](pathfinder-bestiary-items/FKd3nk9u6PpHnAtX.htm)|Draconic Momentum|auto-trad|
|[FkFTivkiFFdzjwbO.htm](pathfinder-bestiary-items/FkFTivkiFFdzjwbO.htm)|Telepathy 100 feet|auto-trad|
|[fKjSxZ4xLl3lTtl0.htm](pathfinder-bestiary-items/fKjSxZ4xLl3lTtl0.htm)|Grab|auto-trad|
|[fkSqSiW3qLpaq6hp.htm](pathfinder-bestiary-items/fkSqSiW3qLpaq6hp.htm)|Body|auto-trad|
|[fLIOuEGoDxmXYqNf.htm](pathfinder-bestiary-items/fLIOuEGoDxmXYqNf.htm)|Snack|auto-trad|
|[FLKA2EOS4qdy02od.htm](pathfinder-bestiary-items/FLKA2EOS4qdy02od.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[FLxvnmMACdVSSD13.htm](pathfinder-bestiary-items/FLxvnmMACdVSSD13.htm)|Draconic Frenzy|auto-trad|
|[fLzaGzA3jMdzslmt.htm](pathfinder-bestiary-items/fLzaGzA3jMdzslmt.htm)|Metal Scent 30 feet|auto-trad|
|[fm9psBEIByOlaBKs.htm](pathfinder-bestiary-items/fm9psBEIByOlaBKs.htm)|Shape Ice|auto-trad|
|[FMmsMdd150OLekUM.htm](pathfinder-bestiary-items/FMmsMdd150OLekUM.htm)|Darkvision|auto-trad|
|[fMWLAq5jGWQRA55Z.htm](pathfinder-bestiary-items/fMWLAq5jGWQRA55Z.htm)|Claw|auto-trad|
|[Fn227VCdI0YQvtkg.htm](pathfinder-bestiary-items/Fn227VCdI0YQvtkg.htm)|Frightful Presence|auto-trad|
|[FOakiHyYV4zfX0lj.htm](pathfinder-bestiary-items/FOakiHyYV4zfX0lj.htm)|Smoke|auto-trad|
|[fOaKOMONKFNcP7B5.htm](pathfinder-bestiary-items/fOaKOMONKFNcP7B5.htm)|Claw|auto-trad|
|[FoGyFrPjwdSydW4U.htm](pathfinder-bestiary-items/FoGyFrPjwdSydW4U.htm)|Leaping Charge|auto-trad|
|[fOnFVdptokDlFcTW.htm](pathfinder-bestiary-items/fOnFVdptokDlFcTW.htm)|Whirlwind of Hooks|auto-trad|
|[fOXEVyfWiHQRSyiC.htm](pathfinder-bestiary-items/fOXEVyfWiHQRSyiC.htm)|Greater Darkvision|auto-trad|
|[fP1MPx5mw7OZ0cma.htm](pathfinder-bestiary-items/fP1MPx5mw7OZ0cma.htm)|Slime|auto-trad|
|[fP5Iv5QbWHvLChA4.htm](pathfinder-bestiary-items/fP5Iv5QbWHvLChA4.htm)|Infused Items|auto-trad|
|[Fpalcgxc6alPgOA4.htm](pathfinder-bestiary-items/Fpalcgxc6alPgOA4.htm)|Scent (Imprecise) 80 feet|auto-trad|
|[FPjjWa8f7H8lZRFS.htm](pathfinder-bestiary-items/FPjjWa8f7H8lZRFS.htm)|Darkvision|auto-trad|
|[fQ0vMmyTwWHXQy7V.htm](pathfinder-bestiary-items/fQ0vMmyTwWHXQy7V.htm)|Horns|auto-trad|
|[fQiZcSYvDH1xIFor.htm](pathfinder-bestiary-items/fQiZcSYvDH1xIFor.htm)|Jaws|auto-trad|
|[fQv7cRqi12UCfasq.htm](pathfinder-bestiary-items/fQv7cRqi12UCfasq.htm)|Darkvision|auto-trad|
|[fqwCiWMkfhbYojJI.htm](pathfinder-bestiary-items/fqwCiWMkfhbYojJI.htm)|Golem Antimagic|auto-trad|
|[fRDoPX53siUmNijE.htm](pathfinder-bestiary-items/fRDoPX53siUmNijE.htm)|Steady Spellcasting|auto-trad|
|[FrDW4cm9w2zRRONj.htm](pathfinder-bestiary-items/FrDW4cm9w2zRRONj.htm)|Arcane Prepared Spells|auto-trad|
|[FReUvGRM1Bo8CeGZ.htm](pathfinder-bestiary-items/FReUvGRM1Bo8CeGZ.htm)|Occult Innate Spells|auto-trad|
|[freYXZtmDhD5zIrA.htm](pathfinder-bestiary-items/freYXZtmDhD5zIrA.htm)|Javelin|auto-trad|
|[fRkzUI9I50wuPJvX.htm](pathfinder-bestiary-items/fRkzUI9I50wuPJvX.htm)|Skittering Assault|auto-trad|
|[FrlYBDNV4RyfqLaY.htm](pathfinder-bestiary-items/FrlYBDNV4RyfqLaY.htm)|Jaws|auto-trad|
|[FrOech6HlG52AZBy.htm](pathfinder-bestiary-items/FrOech6HlG52AZBy.htm)|Jaws|auto-trad|
|[FRSknzZLlvXTktV6.htm](pathfinder-bestiary-items/FRSknzZLlvXTktV6.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[fs49A2QDgfg74MTh.htm](pathfinder-bestiary-items/fs49A2QDgfg74MTh.htm)|Independent Brains|auto-trad|
|[FsFg8KtMHYPQyyIC.htm](pathfinder-bestiary-items/FsFg8KtMHYPQyyIC.htm)|Fire Healing|auto-trad|
|[FSk7wzzH1ToTF3ie.htm](pathfinder-bestiary-items/FSk7wzzH1ToTF3ie.htm)|Grab|auto-trad|
|[fSUvadSPPhS9kOAB.htm](pathfinder-bestiary-items/fSUvadSPPhS9kOAB.htm)|Greater Constrict|auto-trad|
|[FTA0YvX69f46caFj.htm](pathfinder-bestiary-items/FTA0YvX69f46caFj.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[ftbiIlDYazAiyESd.htm](pathfinder-bestiary-items/ftbiIlDYazAiyESd.htm)|At-Will Spells|auto-trad|
|[fTBOn2wHEnOkR4t9.htm](pathfinder-bestiary-items/fTBOn2wHEnOkR4t9.htm)|Scoff at the Divine|auto-trad|
|[FtCodDvKYE5srIOI.htm](pathfinder-bestiary-items/FtCodDvKYE5srIOI.htm)|Foot|auto-trad|
|[fTIWo8M9tHghfIgU.htm](pathfinder-bestiary-items/fTIWo8M9tHghfIgU.htm)|Darkvision|auto-trad|
|[FTMgXssm5HK4Io0O.htm](pathfinder-bestiary-items/FTMgXssm5HK4Io0O.htm)|Vortex|auto-trad|
|[fTPjypXzhscP42dn.htm](pathfinder-bestiary-items/fTPjypXzhscP42dn.htm)|Darkvision|auto-trad|
|[FtQATmD9TKhRQwoZ.htm](pathfinder-bestiary-items/FtQATmD9TKhRQwoZ.htm)|Constant Spells|auto-trad|
|[FuDdgAP7wwq6LCFO.htm](pathfinder-bestiary-items/FuDdgAP7wwq6LCFO.htm)|Serpent Venom|auto-trad|
|[FUgfYGhsCxf8KU54.htm](pathfinder-bestiary-items/FUgfYGhsCxf8KU54.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[FUIS7buU2stLWVpK.htm](pathfinder-bestiary-items/FUIS7buU2stLWVpK.htm)|Claws|auto-trad|
|[Fuv2PWZEnIbyJ99B.htm](pathfinder-bestiary-items/Fuv2PWZEnIbyJ99B.htm)|Take Them Down!|auto-trad|
|[FUwL7sL45SGQB0N6.htm](pathfinder-bestiary-items/FUwL7sL45SGQB0N6.htm)|Attack of Opportunity (Special)|auto-trad|
|[fUzY6OZeD7f6IHE4.htm](pathfinder-bestiary-items/fUzY6OZeD7f6IHE4.htm)|Elemental Endurance|auto-trad|
|[fvIjW0iSvoVCNnVG.htm](pathfinder-bestiary-items/fvIjW0iSvoVCNnVG.htm)|Disturbing Vision|auto-trad|
|[FVn9vebx0n5CJW76.htm](pathfinder-bestiary-items/FVn9vebx0n5CJW76.htm)|Tail|auto-trad|
|[fvSVZqV8GEHEzMVu.htm](pathfinder-bestiary-items/fvSVZqV8GEHEzMVu.htm)|Spore Blight|auto-trad|
|[fWTtIUDtbSfOzgqv.htm](pathfinder-bestiary-items/fWTtIUDtbSfOzgqv.htm)|General's Cry|auto-trad|
|[FX1GZJoTSgtAlICB.htm](pathfinder-bestiary-items/FX1GZJoTSgtAlICB.htm)|Arm|auto-trad|
|[fX3ZS6kW7CqzkJuu.htm](pathfinder-bestiary-items/fX3ZS6kW7CqzkJuu.htm)|Drag|auto-trad|
|[fxVaypsTZKjHprzh.htm](pathfinder-bestiary-items/fxVaypsTZKjHprzh.htm)|Change Shape|auto-trad|
|[fxy74wWSDj5riJSB.htm](pathfinder-bestiary-items/fxy74wWSDj5riJSB.htm)|Constant Spells|auto-trad|
|[fYKBzNcZosPojEyp.htm](pathfinder-bestiary-items/fYKBzNcZosPojEyp.htm)|Desert Thirst|auto-trad|
|[FyUL1Dfsy8YJIJH4.htm](pathfinder-bestiary-items/FyUL1Dfsy8YJIJH4.htm)|Sticky Strand|auto-trad|
|[Fz4qoI14He66tmpf.htm](pathfinder-bestiary-items/Fz4qoI14He66tmpf.htm)|Commander's Aura|auto-trad|
|[Fzou6YacIfdWk8Rx.htm](pathfinder-bestiary-items/Fzou6YacIfdWk8Rx.htm)|Windsense 240 feet|auto-trad|
|[fZTQMVZbBOPoJ37I.htm](pathfinder-bestiary-items/fZTQMVZbBOPoJ37I.htm)|Constrict|auto-trad|
|[fZx0EfFDzx8aK7tG.htm](pathfinder-bestiary-items/fZx0EfFDzx8aK7tG.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[g1KH4siW7BJpYTfM.htm](pathfinder-bestiary-items/g1KH4siW7BJpYTfM.htm)|Claw|auto-trad|
|[g2irWtwE4YUnllGP.htm](pathfinder-bestiary-items/g2irWtwE4YUnllGP.htm)|Jaws|auto-trad|
|[g2OkSiICf1gw8aes.htm](pathfinder-bestiary-items/g2OkSiICf1gw8aes.htm)|Quicken|auto-trad|
|[g2qQpMZqRaylNASw.htm](pathfinder-bestiary-items/g2qQpMZqRaylNASw.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[G3Hz9uWIYCEyoBRq.htm](pathfinder-bestiary-items/G3Hz9uWIYCEyoBRq.htm)|Sneak Attack|auto-trad|
|[g4bJ3hTpL9J2uQKp.htm](pathfinder-bestiary-items/g4bJ3hTpL9J2uQKp.htm)|Attack of Opportunity|auto-trad|
|[G4HjijhXg27QNTqn.htm](pathfinder-bestiary-items/G4HjijhXg27QNTqn.htm)|Divine Innate Spells|auto-trad|
|[g51BzhmOH3vHLWup.htm](pathfinder-bestiary-items/g51BzhmOH3vHLWup.htm)|Wrestle|auto-trad|
|[g5dltca5pjhRzJMP.htm](pathfinder-bestiary-items/g5dltca5pjhRzJMP.htm)|Twisting Tail|auto-trad|
|[G5jsqgUC6MzdXURo.htm](pathfinder-bestiary-items/G5jsqgUC6MzdXURo.htm)|Darkvision|auto-trad|
|[g6xJoBw3WBMO4VqW.htm](pathfinder-bestiary-items/g6xJoBw3WBMO4VqW.htm)|Claw|auto-trad|
|[G7hedsdXlwV3pW3J.htm](pathfinder-bestiary-items/G7hedsdXlwV3pW3J.htm)|Darkvision|auto-trad|
|[g7nl0QahmH5JaunK.htm](pathfinder-bestiary-items/g7nl0QahmH5JaunK.htm)|Low-Light Vision|auto-trad|
|[g832bFUMFhXs1T6E.htm](pathfinder-bestiary-items/g832bFUMFhXs1T6E.htm)|Push|auto-trad|
|[G9f4Fx5llYjESmPt.htm](pathfinder-bestiary-items/G9f4Fx5llYjESmPt.htm)|Grab|auto-trad|
|[GA9tSWJmy7KFgUjB.htm](pathfinder-bestiary-items/GA9tSWJmy7KFgUjB.htm)|Primal Innate Spells|auto-trad|
|[gakDYFEsPKyFUdIg.htm](pathfinder-bestiary-items/gakDYFEsPKyFUdIg.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[GB4t0y9suPOQZ9o5.htm](pathfinder-bestiary-items/GB4t0y9suPOQZ9o5.htm)|Fangs|auto-trad|
|[GBiarRqqq8qwyZtJ.htm](pathfinder-bestiary-items/GBiarRqqq8qwyZtJ.htm)|Paralysis|auto-trad|
|[GBuKg3VtBcivhUos.htm](pathfinder-bestiary-items/GBuKg3VtBcivhUos.htm)|Improved Grab|auto-trad|
|[gC0TpsGsMQ5RDDau.htm](pathfinder-bestiary-items/gC0TpsGsMQ5RDDau.htm)|Swallow Whole|auto-trad|
|[GcJJKFDfrv0jW0K3.htm](pathfinder-bestiary-items/GcJJKFDfrv0jW0K3.htm)|Freezing Mist Breath|auto-trad|
|[gCm9tVCYX0lSCKOq.htm](pathfinder-bestiary-items/gCm9tVCYX0lSCKOq.htm)|Darkvision|auto-trad|
|[gCMdnmRVTS96OQxR.htm](pathfinder-bestiary-items/gCMdnmRVTS96OQxR.htm)|Web|auto-trad|
|[gCQ2k3U8EhFrjpCO.htm](pathfinder-bestiary-items/gCQ2k3U8EhFrjpCO.htm)|Wing Deflection|auto-trad|
|[GCyLSeOtTMt3wLur.htm](pathfinder-bestiary-items/GCyLSeOtTMt3wLur.htm)|Flying Strafe|auto-trad|
|[gD3kGPuZGxBNXQTG.htm](pathfinder-bestiary-items/gD3kGPuZGxBNXQTG.htm)|Longsword|auto-trad|
|[gdGHPerbX8DzQ8D5.htm](pathfinder-bestiary-items/gdGHPerbX8DzQ8D5.htm)|Negative Healing|auto-trad|
|[GDRN1E60FSxKpOv0.htm](pathfinder-bestiary-items/GDRN1E60FSxKpOv0.htm)|Mandibles|auto-trad|
|[gdUSuIptZvtLZ7ts.htm](pathfinder-bestiary-items/gdUSuIptZvtLZ7ts.htm)|Darkvision|auto-trad|
|[GdWAi69mvxSgSJOn.htm](pathfinder-bestiary-items/GdWAi69mvxSgSJOn.htm)|Virtue Aversion|auto-trad|
|[GE9sBpIRZwKGL1HJ.htm](pathfinder-bestiary-items/GE9sBpIRZwKGL1HJ.htm)|Beak|auto-trad|
|[GeAJNEdtRdXSvZvk.htm](pathfinder-bestiary-items/GeAJNEdtRdXSvZvk.htm)|Breath Weapon|auto-trad|
|[GEvdLrMrI8fbxU9C.htm](pathfinder-bestiary-items/GEvdLrMrI8fbxU9C.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[GF7VbTGWzxeNZeOf.htm](pathfinder-bestiary-items/GF7VbTGWzxeNZeOf.htm)|Low-Light Vision|auto-trad|
|[Gfs6EH6FQLkgQKGz.htm](pathfinder-bestiary-items/Gfs6EH6FQLkgQKGz.htm)|Drench|auto-trad|
|[GGJBUd4JmwxnZtYj.htm](pathfinder-bestiary-items/GGJBUd4JmwxnZtYj.htm)|Pincer|auto-trad|
|[GGPFGv65lu6xKmVn.htm](pathfinder-bestiary-items/GGPFGv65lu6xKmVn.htm)|Greatclub|auto-trad|
|[gGPWUqUPfUG7gzbI.htm](pathfinder-bestiary-items/gGPWUqUPfUG7gzbI.htm)|Draconic Frenzy|auto-trad|
|[ghngQIhTKh6viVvP.htm](pathfinder-bestiary-items/ghngQIhTKh6viVvP.htm)|Change Shape|auto-trad|
|[GhnXpFtWM1vyvu8V.htm](pathfinder-bestiary-items/GhnXpFtWM1vyvu8V.htm)|Construct Armor (Hardness 9)|auto-trad|
|[GhRRqxvK6T21zheo.htm](pathfinder-bestiary-items/GhRRqxvK6T21zheo.htm)|Pack Attack|auto-trad|
|[GHuGn3ZCUkbeSFiQ.htm](pathfinder-bestiary-items/GHuGn3ZCUkbeSFiQ.htm)|Filth Fever|auto-trad|
|[giE56Vm25ZKR3ZJm.htm](pathfinder-bestiary-items/giE56Vm25ZKR3ZJm.htm)|Sprinkle Pixie Dust|auto-trad|
|[Gix3fftNAeuJS7zt.htm](pathfinder-bestiary-items/Gix3fftNAeuJS7zt.htm)|Rituals|auto-trad|
|[GIxGQeMwhYMa5aqt.htm](pathfinder-bestiary-items/GIxGQeMwhYMa5aqt.htm)|Sling|auto-trad|
|[GjB9p0GotK5A2v4S.htm](pathfinder-bestiary-items/GjB9p0GotK5A2v4S.htm)|Desert Thirst|auto-trad|
|[GJh2y3nd5dKEWe8I.htm](pathfinder-bestiary-items/GJh2y3nd5dKEWe8I.htm)|Displacement|auto-trad|
|[gjHycBNENtIXpvMF.htm](pathfinder-bestiary-items/gjHycBNENtIXpvMF.htm)|Divine Innate Spells|auto-trad|
|[gJlDdyxfSGMpZN7a.htm](pathfinder-bestiary-items/gJlDdyxfSGMpZN7a.htm)|Tusks|auto-trad|
|[GJNrxWQdmK1EykYc.htm](pathfinder-bestiary-items/GJNrxWQdmK1EykYc.htm)|At-Will Spells|auto-trad|
|[gjQiQcNp4NjkcKFX.htm](pathfinder-bestiary-items/gjQiQcNp4NjkcKFX.htm)|Tail|auto-trad|
|[gjrtuvnDxfzWNWcA.htm](pathfinder-bestiary-items/gjrtuvnDxfzWNWcA.htm)|Darkvision|auto-trad|
|[gjZuHZMNsUIskGJf.htm](pathfinder-bestiary-items/gjZuHZMNsUIskGJf.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[GkFwtCILr5PvesJA.htm](pathfinder-bestiary-items/GkFwtCILr5PvesJA.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Gkho6QKI4TcQF5wN.htm](pathfinder-bestiary-items/Gkho6QKI4TcQF5wN.htm)|Wing|auto-trad|
|[GKRApZm7r1VxqXRt.htm](pathfinder-bestiary-items/GKRApZm7r1VxqXRt.htm)|Darkvision|auto-trad|
|[GliiYsgCIg3rdZ5a.htm](pathfinder-bestiary-items/GliiYsgCIg3rdZ5a.htm)|Vulnerable to Dispelling|auto-trad|
|[GlLMuMgpxJ9FZVfm.htm](pathfinder-bestiary-items/GlLMuMgpxJ9FZVfm.htm)|Armor-Rending Bite|auto-trad|
|[GLucEXJUhwdRS7q9.htm](pathfinder-bestiary-items/GLucEXJUhwdRS7q9.htm)|Claw|auto-trad|
|[glwIs2UR3O1wG0tc.htm](pathfinder-bestiary-items/glwIs2UR3O1wG0tc.htm)|Soul Lock|auto-trad|
|[GM1uYxLGklcQScOG.htm](pathfinder-bestiary-items/GM1uYxLGklcQScOG.htm)|Lifesense 60 feet|auto-trad|
|[Gm902B0XQR5W4eLu.htm](pathfinder-bestiary-items/Gm902B0XQR5W4eLu.htm)|Tied to the Land|auto-trad|
|[gMf2LQ1HIE4bCW8p.htm](pathfinder-bestiary-items/gMf2LQ1HIE4bCW8p.htm)|Spring upon Prey|auto-trad|
|[GmPqLo8cPqMgY6V2.htm](pathfinder-bestiary-items/GmPqLo8cPqMgY6V2.htm)|Grab|auto-trad|
|[GmwDsrs27J6RnwHo.htm](pathfinder-bestiary-items/GmwDsrs27J6RnwHo.htm)|Jaws|auto-trad|
|[GNpyzaHN2uioOy3x.htm](pathfinder-bestiary-items/GNpyzaHN2uioOy3x.htm)|Pick|auto-trad|
|[GNtcN0AsjAwwwARU.htm](pathfinder-bestiary-items/GNtcN0AsjAwwwARU.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GOB41k6iHfNnb28d.htm](pathfinder-bestiary-items/GOB41k6iHfNnb28d.htm)|Darkvision|auto-trad|
|[GoEK3KTZm6r0vhaU.htm](pathfinder-bestiary-items/GoEK3KTZm6r0vhaU.htm)|Infectious Aura|auto-trad|
|[GOGgw91M2vSnmc7P.htm](pathfinder-bestiary-items/GOGgw91M2vSnmc7P.htm)|Claw|auto-trad|
|[gooqrR0z9rN4mnlN.htm](pathfinder-bestiary-items/gooqrR0z9rN4mnlN.htm)|Bola Bolt|auto-trad|
|[goqMldxoGCuLp3Zc.htm](pathfinder-bestiary-items/goqMldxoGCuLp3Zc.htm)|Cloud Walk|auto-trad|
|[goSE2P0nlOvFsVEH.htm](pathfinder-bestiary-items/goSE2P0nlOvFsVEH.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GoUIra7KXFd9kUhH.htm](pathfinder-bestiary-items/GoUIra7KXFd9kUhH.htm)|Trample|auto-trad|
|[GpF7MRIldG6kiEBK.htm](pathfinder-bestiary-items/GpF7MRIldG6kiEBK.htm)|Occult Innate Spells|auto-trad|
|[gPoK6Dl5m43T6mpg.htm](pathfinder-bestiary-items/gPoK6Dl5m43T6mpg.htm)|+1 Status to All Saves vs. Disease|auto-trad|
|[gpQkk5cFJ03uvxyX.htm](pathfinder-bestiary-items/gpQkk5cFJ03uvxyX.htm)|Talon|auto-trad|
|[gQ7hVeDi4X11cJlc.htm](pathfinder-bestiary-items/gQ7hVeDi4X11cJlc.htm)|Cursed Wound|auto-trad|
|[GqnL43j2bn0yBMad.htm](pathfinder-bestiary-items/GqnL43j2bn0yBMad.htm)|Greataxe|auto-trad|
|[gQsn9Gd92k5s2c0r.htm](pathfinder-bestiary-items/gQsn9Gd92k5s2c0r.htm)|Fast Healing 2|auto-trad|
|[GrDnRSSifZtVibbk.htm](pathfinder-bestiary-items/GrDnRSSifZtVibbk.htm)|Telepathy 100 feet|auto-trad|
|[gRn39hK7VbDU7fip.htm](pathfinder-bestiary-items/gRn39hK7VbDU7fip.htm)|Low-Light Vision|auto-trad|
|[gRnmV8LgZS6qZ09j.htm](pathfinder-bestiary-items/gRnmV8LgZS6qZ09j.htm)|Low-Light Vision|auto-trad|
|[gRQ32Y4BWrQbWOlS.htm](pathfinder-bestiary-items/gRQ32Y4BWrQbWOlS.htm)|Coffin Restoration|auto-trad|
|[gRR4m0LvmYETY1sR.htm](pathfinder-bestiary-items/gRR4m0LvmYETY1sR.htm)|At-Will Spells|auto-trad|
|[grrGcmbt2tnL36tN.htm](pathfinder-bestiary-items/grrGcmbt2tnL36tN.htm)|Destructive Strike|auto-trad|
|[grrOo0mi2kz2urJF.htm](pathfinder-bestiary-items/grrOo0mi2kz2urJF.htm)|Cinder Dispersal|auto-trad|
|[Gs9kfR3NhiKWAumE.htm](pathfinder-bestiary-items/Gs9kfR3NhiKWAumE.htm)|Lantern of Hope|auto-trad|
|[GsaqDG9VKiNEXisx.htm](pathfinder-bestiary-items/GsaqDG9VKiNEXisx.htm)|Constant Spells|auto-trad|
|[gSG1OZxFHX2zLrNP.htm](pathfinder-bestiary-items/gSG1OZxFHX2zLrNP.htm)|Counterspell|auto-trad|
|[gSKgEf1cka4ughqp.htm](pathfinder-bestiary-items/gSKgEf1cka4ughqp.htm)|Darkvision|auto-trad|
|[GsQeeSV97T6FPvoY.htm](pathfinder-bestiary-items/GsQeeSV97T6FPvoY.htm)|Breath Weapon|auto-trad|
|[gSQGpCpvE3YRtvLr.htm](pathfinder-bestiary-items/gSQGpCpvE3YRtvLr.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GsSxwd5EPNBqxdWC.htm](pathfinder-bestiary-items/GsSxwd5EPNBqxdWC.htm)|Draconic Frenzy|auto-trad|
|[GsuMeqU77NP6vTqI.htm](pathfinder-bestiary-items/GsuMeqU77NP6vTqI.htm)|Protean Anatomy|auto-trad|
|[GSyhdPMQNpUa9LQJ.htm](pathfinder-bestiary-items/GSyhdPMQNpUa9LQJ.htm)|Engulf|auto-trad|
|[gUMIzpDGWwaygeXn.htm](pathfinder-bestiary-items/gUMIzpDGWwaygeXn.htm)|Tail|auto-trad|
|[gUqFH9kEouWhTaTk.htm](pathfinder-bestiary-items/gUqFH9kEouWhTaTk.htm)|Darkvision|auto-trad|
|[gurbv9AAgZYNEghZ.htm](pathfinder-bestiary-items/gurbv9AAgZYNEghZ.htm)|Darkvision|auto-trad|
|[GurQ6TuFcLhxNewa.htm](pathfinder-bestiary-items/GurQ6TuFcLhxNewa.htm)|Arcane Innate Spells|auto-trad|
|[gV8plzH46OeVvlNG.htm](pathfinder-bestiary-items/gV8plzH46OeVvlNG.htm)|Stench|auto-trad|
|[gVHdzhF7CYS5L3SK.htm](pathfinder-bestiary-items/gVHdzhF7CYS5L3SK.htm)|Burning Grasp|auto-trad|
|[GVNTgGaXcebR64nT.htm](pathfinder-bestiary-items/GVNTgGaXcebR64nT.htm)|Partitioned Anatomy|auto-trad|
|[GvYWVCMKVWLO9jDK.htm](pathfinder-bestiary-items/GvYWVCMKVWLO9jDK.htm)|Shock Maw|auto-trad|
|[GW3yAvIc0LiXOGo0.htm](pathfinder-bestiary-items/GW3yAvIc0LiXOGo0.htm)|Darkvision|auto-trad|
|[gw66iZEKmcFi0JjW.htm](pathfinder-bestiary-items/gw66iZEKmcFi0JjW.htm)|Howl|auto-trad|
|[GWK22q1k4VFPbsKf.htm](pathfinder-bestiary-items/GWK22q1k4VFPbsKf.htm)|Pack Attack|auto-trad|
|[gwswBzTnckFDYGZR.htm](pathfinder-bestiary-items/gwswBzTnckFDYGZR.htm)|Darkvision|auto-trad|
|[gx4QtoqFUMcJKh9b.htm](pathfinder-bestiary-items/gx4QtoqFUMcJKh9b.htm)|Grab|auto-trad|
|[gxbMXYNwJTWTgbf6.htm](pathfinder-bestiary-items/gxbMXYNwJTWTgbf6.htm)|Primal Innate Spells|auto-trad|
|[gxj7cGSco9mq9DF4.htm](pathfinder-bestiary-items/gxj7cGSco9mq9DF4.htm)|Fast Healing 5|auto-trad|
|[gxJrJPqtpVlM0Xe6.htm](pathfinder-bestiary-items/gxJrJPqtpVlM0Xe6.htm)|Swallow Whole|auto-trad|
|[gxq2qiGUPuQ5wCW9.htm](pathfinder-bestiary-items/gxq2qiGUPuQ5wCW9.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[GXr4u9s0uuvI5J6d.htm](pathfinder-bestiary-items/GXr4u9s0uuvI5J6d.htm)|Vampire Weaknesses|auto-trad|
|[gXUEOhZBMg1NCOMw.htm](pathfinder-bestiary-items/gXUEOhZBMg1NCOMw.htm)|Quick Capture|auto-trad|
|[gXusGY96gXnuOJVw.htm](pathfinder-bestiary-items/gXusGY96gXnuOJVw.htm)|Frightful Presence|auto-trad|
|[gYnpwjAaRfAevtAE.htm](pathfinder-bestiary-items/gYnpwjAaRfAevtAE.htm)|Primal Innate Spells|auto-trad|
|[gYS1Pu6DMoqNba15.htm](pathfinder-bestiary-items/gYS1Pu6DMoqNba15.htm)|Attack of Opportunity|auto-trad|
|[GZ55vWFhwaHyhnfM.htm](pathfinder-bestiary-items/GZ55vWFhwaHyhnfM.htm)|Avernal Fever|auto-trad|
|[gZHu2APmbYnWCFLm.htm](pathfinder-bestiary-items/gZHu2APmbYnWCFLm.htm)|Bark Orders|auto-trad|
|[GzpsUYZPjfrvA0Yh.htm](pathfinder-bestiary-items/GzpsUYZPjfrvA0Yh.htm)|Object Meld|auto-trad|
|[gzVcZTMtslepZxcC.htm](pathfinder-bestiary-items/gzVcZTMtslepZxcC.htm)|Unstoppable Burrow|auto-trad|
|[gZXfTOLjvJ7GkHxM.htm](pathfinder-bestiary-items/gZXfTOLjvJ7GkHxM.htm)|Darkvision|auto-trad|
|[gzYwg0Ob1iEiRCDc.htm](pathfinder-bestiary-items/gzYwg0Ob1iEiRCDc.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[gZywv8nUOzTjloep.htm](pathfinder-bestiary-items/gZywv8nUOzTjloep.htm)|Fire Mote|auto-trad|
|[h0WWEsFcTFj9dtG9.htm](pathfinder-bestiary-items/h0WWEsFcTFj9dtG9.htm)|Constant Spells|auto-trad|
|[h2IphaAIOQxjJB7Y.htm](pathfinder-bestiary-items/h2IphaAIOQxjJB7Y.htm)|Fist|auto-trad|
|[h2qxWUvI0fbg4RxB.htm](pathfinder-bestiary-items/h2qxWUvI0fbg4RxB.htm)|Divine Innate Spells|auto-trad|
|[h2xw74YF04RtIxtd.htm](pathfinder-bestiary-items/h2xw74YF04RtIxtd.htm)|Darkvision|auto-trad|
|[h3RAiOfN4DCwKqV0.htm](pathfinder-bestiary-items/h3RAiOfN4DCwKqV0.htm)|Putrid Stench|auto-trad|
|[h4fFfvaiDiqgTpTz.htm](pathfinder-bestiary-items/h4fFfvaiDiqgTpTz.htm)|Rejuvenation|auto-trad|
|[h4qPfDnAjrfUEFX6.htm](pathfinder-bestiary-items/h4qPfDnAjrfUEFX6.htm)|Sylvan Wine|auto-trad|
|[H64oYdSbWbITvHiH.htm](pathfinder-bestiary-items/H64oYdSbWbITvHiH.htm)|Darkvision|auto-trad|
|[h6rad5LTezkxMym3.htm](pathfinder-bestiary-items/h6rad5LTezkxMym3.htm)|Arcane Prepared Spells|auto-trad|
|[h6wQ76GXx0XhL0rW.htm](pathfinder-bestiary-items/h6wQ76GXx0XhL0rW.htm)|Claw|auto-trad|
|[h7FJgyxQFttUHPeA.htm](pathfinder-bestiary-items/h7FJgyxQFttUHPeA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[H7ILhQt7vbCdQYmv.htm](pathfinder-bestiary-items/H7ILhQt7vbCdQYmv.htm)|Deep Breath|auto-trad|
|[h7p7zhkW6OBX6N85.htm](pathfinder-bestiary-items/h7p7zhkW6OBX6N85.htm)|Luminous Fire|auto-trad|
|[H7YaFV40o1FTZLkj.htm](pathfinder-bestiary-items/H7YaFV40o1FTZLkj.htm)|Divine Innate Spells|auto-trad|
|[H85aThyyHpy8z06u.htm](pathfinder-bestiary-items/H85aThyyHpy8z06u.htm)|Pull the Strands|auto-trad|
|[H88Ay3ZweyXm4sL3.htm](pathfinder-bestiary-items/H88Ay3ZweyXm4sL3.htm)|Staff|auto-trad|
|[h8HPqnXI3dI7KFxV.htm](pathfinder-bestiary-items/h8HPqnXI3dI7KFxV.htm)|Hoof|auto-trad|
|[H8zaV182Ql2iZqTg.htm](pathfinder-bestiary-items/H8zaV182Ql2iZqTg.htm)|Low-Light Vision|auto-trad|
|[h9dbqoHHEOOBy05q.htm](pathfinder-bestiary-items/h9dbqoHHEOOBy05q.htm)|Shape Ice|auto-trad|
|[HAG0BZUyUX7lvLH9.htm](pathfinder-bestiary-items/HAG0BZUyUX7lvLH9.htm)|Darkvision|auto-trad|
|[HAOtod2wxXg71n8U.htm](pathfinder-bestiary-items/HAOtod2wxXg71n8U.htm)|Jaws|auto-trad|
|[HavBnXAT02rDNben.htm](pathfinder-bestiary-items/HavBnXAT02rDNben.htm)|Sneak Attack|auto-trad|
|[HaWsVELJ0bpMTvaP.htm](pathfinder-bestiary-items/HaWsVELJ0bpMTvaP.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[hbBhekpAkC55ljY1.htm](pathfinder-bestiary-items/hbBhekpAkC55ljY1.htm)|Fleet Performer|auto-trad|
|[hbdeUFe3D16BSKa0.htm](pathfinder-bestiary-items/hbdeUFe3D16BSKa0.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[hbr6Uqs0GRk0t1JI.htm](pathfinder-bestiary-items/hbr6Uqs0GRk0t1JI.htm)|Punishing Tail|auto-trad|
|[hcHsi8D2704vMZZf.htm](pathfinder-bestiary-items/hcHsi8D2704vMZZf.htm)|Constant Spells|auto-trad|
|[hCQUre41fnib5cSf.htm](pathfinder-bestiary-items/hCQUre41fnib5cSf.htm)|Claw|auto-trad|
|[hcXr2FC36bTLJ9kF.htm](pathfinder-bestiary-items/hcXr2FC36bTLJ9kF.htm)|Composite Shortbow|auto-trad|
|[Hczu635vDDik3NIJ.htm](pathfinder-bestiary-items/Hczu635vDDik3NIJ.htm)|Grab|auto-trad|
|[HDOKj3GkjZCeH2k3.htm](pathfinder-bestiary-items/HDOKj3GkjZCeH2k3.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[hdxkFShEWbTlfmhb.htm](pathfinder-bestiary-items/hdxkFShEWbTlfmhb.htm)|Attack of Opportunity|auto-trad|
|[hdyD6t2duHPLOLyi.htm](pathfinder-bestiary-items/hdyD6t2duHPLOLyi.htm)|Buck|auto-trad|
|[hDYxH5VuENin7Q0I.htm](pathfinder-bestiary-items/hDYxH5VuENin7Q0I.htm)|Crystal Sense (Imprecise) 60 feet|auto-trad|
|[hdZW9dGdp8kUzUjv.htm](pathfinder-bestiary-items/hdZW9dGdp8kUzUjv.htm)|Breath Weapon|auto-trad|
|[heW1rCJfQ2KMmuqd.htm](pathfinder-bestiary-items/heW1rCJfQ2KMmuqd.htm)|Ink Cloud|auto-trad|
|[HF4Dnnv97ezTQ1sy.htm](pathfinder-bestiary-items/HF4Dnnv97ezTQ1sy.htm)|Tail|auto-trad|
|[HFETDQgqq1x4amjT.htm](pathfinder-bestiary-items/HFETDQgqq1x4amjT.htm)|Occult Innate Spells|auto-trad|
|[hfNcV2xUlTHlcRk0.htm](pathfinder-bestiary-items/hfNcV2xUlTHlcRk0.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[hFpiEAg9uOUfaval.htm](pathfinder-bestiary-items/hFpiEAg9uOUfaval.htm)|Primal Innate Spells|auto-trad|
|[HFyEFMKsCld64B15.htm](pathfinder-bestiary-items/HFyEFMKsCld64B15.htm)|Avenging Bite|auto-trad|
|[HG3k71Iu7dao9DNr.htm](pathfinder-bestiary-items/HG3k71Iu7dao9DNr.htm)|Jaws|auto-trad|
|[HGrl3b1F23RjZkR7.htm](pathfinder-bestiary-items/HGrl3b1F23RjZkR7.htm)|Jaws|auto-trad|
|[HgRvaTh0mJv6CbTu.htm](pathfinder-bestiary-items/HgRvaTh0mJv6CbTu.htm)|Swarm Mind|auto-trad|
|[hH9lWhV3jA9NQJuG.htm](pathfinder-bestiary-items/hH9lWhV3jA9NQJuG.htm)|Hidden Movement|auto-trad|
|[HhkalK1aMfqDSiWQ.htm](pathfinder-bestiary-items/HhkalK1aMfqDSiWQ.htm)|Drink Blood|auto-trad|
|[hhL0DgwVWFclIPih.htm](pathfinder-bestiary-items/hhL0DgwVWFclIPih.htm)|Jaws|auto-trad|
|[hHLGOCYOj0KZUdYI.htm](pathfinder-bestiary-items/hHLGOCYOj0KZUdYI.htm)|Spike Volley|auto-trad|
|[hHrGBCeIy4565BFU.htm](pathfinder-bestiary-items/hHrGBCeIy4565BFU.htm)|Vortex Pull|auto-trad|
|[hIpNUSZedk66VUWy.htm](pathfinder-bestiary-items/hIpNUSZedk66VUWy.htm)|Darkvision|auto-trad|
|[hIxE6LMA92SPk1eC.htm](pathfinder-bestiary-items/hIxE6LMA92SPk1eC.htm)|Death Flash|auto-trad|
|[hJ1mAKCkNvCy1GY0.htm](pathfinder-bestiary-items/hJ1mAKCkNvCy1GY0.htm)|Fangs|auto-trad|
|[hj2HivadIMJbB0cD.htm](pathfinder-bestiary-items/hj2HivadIMJbB0cD.htm)|Stinger|auto-trad|
|[hJ2RMo3aTyVEm4yz.htm](pathfinder-bestiary-items/hJ2RMo3aTyVEm4yz.htm)|Spittle|auto-trad|
|[HjqV5crA9cRbpUVj.htm](pathfinder-bestiary-items/HjqV5crA9cRbpUVj.htm)|Electric Surge|auto-trad|
|[hk3y1r8nU2o8QA4C.htm](pathfinder-bestiary-items/hk3y1r8nU2o8QA4C.htm)|Thrash|auto-trad|
|[hk7qH9BpvzM7rFGe.htm](pathfinder-bestiary-items/hk7qH9BpvzM7rFGe.htm)|Sin Scent (Imprecise) 30 feet|auto-trad|
|[HknPH5MLDixmpO4o.htm](pathfinder-bestiary-items/HknPH5MLDixmpO4o.htm)|Constrict|auto-trad|
|[HKsUocYSHIn1WVXR.htm](pathfinder-bestiary-items/HKsUocYSHIn1WVXR.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[HkUjt1e6CiwdfHoy.htm](pathfinder-bestiary-items/HkUjt1e6CiwdfHoy.htm)|Breath Weapon|auto-trad|
|[HkvlzHti2lgPShwU.htm](pathfinder-bestiary-items/HkvlzHti2lgPShwU.htm)|Breath Weapon|auto-trad|
|[HKY0WgGamDu2JbUq.htm](pathfinder-bestiary-items/HKY0WgGamDu2JbUq.htm)|Darkvision|auto-trad|
|[HkzWmjfjVp6p9yR8.htm](pathfinder-bestiary-items/HkzWmjfjVp6p9yR8.htm)|Arcane Innate Spells|auto-trad|
|[HM9U7vUmg5uUan4k.htm](pathfinder-bestiary-items/HM9U7vUmg5uUan4k.htm)|Fast Healing 2 (While Underwater)|auto-trad|
|[HMeH7MYJTTTICR3k.htm](pathfinder-bestiary-items/HMeH7MYJTTTICR3k.htm)|Jaws|auto-trad|
|[hN4sFUVjQOMN0Gvq.htm](pathfinder-bestiary-items/hN4sFUVjQOMN0Gvq.htm)|Arcane Prepared Spells|auto-trad|
|[hn5GBufYeA5C1oOg.htm](pathfinder-bestiary-items/hn5GBufYeA5C1oOg.htm)|Drink Blood|auto-trad|
|[hnjL5DfkOGkcdjPb.htm](pathfinder-bestiary-items/hnjL5DfkOGkcdjPb.htm)|Divine Innate Spells|auto-trad|
|[HNM58d8JH28lGstw.htm](pathfinder-bestiary-items/HNM58d8JH28lGstw.htm)|Hoof|auto-trad|
|[Ho2TRxPdMdaTfr2V.htm](pathfinder-bestiary-items/Ho2TRxPdMdaTfr2V.htm)|Claw|auto-trad|
|[HO9oEygHulZKXSSM.htm](pathfinder-bestiary-items/HO9oEygHulZKXSSM.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[HOMxdkDlcB6S1y9k.htm](pathfinder-bestiary-items/HOMxdkDlcB6S1y9k.htm)|Ghaele's Gaze|auto-trad|
|[hook1XGZNlxXtjvJ.htm](pathfinder-bestiary-items/hook1XGZNlxXtjvJ.htm)|Fangs|auto-trad|
|[hopAbtHoJov7g8zJ.htm](pathfinder-bestiary-items/hopAbtHoJov7g8zJ.htm)|At-Will Spells|auto-trad|
|[Hor4EOQfUGAkFak1.htm](pathfinder-bestiary-items/Hor4EOQfUGAkFak1.htm)|Flaming Composite Longbow|auto-trad|
|[hperRozJh0EXe7iX.htm](pathfinder-bestiary-items/hperRozJh0EXe7iX.htm)|Telepathy 100 feet|auto-trad|
|[HPknnMADVjl3vadK.htm](pathfinder-bestiary-items/HPknnMADVjl3vadK.htm)|Champion Devotion Spells|auto-trad|
|[hQCxv2A5wNHVDzkj.htm](pathfinder-bestiary-items/hQCxv2A5wNHVDzkj.htm)|Darkvision|auto-trad|
|[hQNxvkHklgW9fvpf.htm](pathfinder-bestiary-items/hQNxvkHklgW9fvpf.htm)|Explosion|auto-trad|
|[HQrZGDENHs9wuZxK.htm](pathfinder-bestiary-items/HQrZGDENHs9wuZxK.htm)|Sling|auto-trad|
|[HQToKWTC3drtqduN.htm](pathfinder-bestiary-items/HQToKWTC3drtqduN.htm)|Telepathy 100 feet|auto-trad|
|[hr7MMe0oUKYOwUNV.htm](pathfinder-bestiary-items/hr7MMe0oUKYOwUNV.htm)|Tusk|auto-trad|
|[hr88vSwb8UXcgsA0.htm](pathfinder-bestiary-items/hr88vSwb8UXcgsA0.htm)|Claw|auto-trad|
|[Hrk3kc8RGNxK9XoN.htm](pathfinder-bestiary-items/Hrk3kc8RGNxK9XoN.htm)|Freezing Blood|auto-trad|
|[HRuAKCIYZlriHOGv.htm](pathfinder-bestiary-items/HRuAKCIYZlriHOGv.htm)|Jaws|auto-trad|
|[hRXUEhTtGJCD8X7J.htm](pathfinder-bestiary-items/hRXUEhTtGJCD8X7J.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[hrylbarG87YtJSgb.htm](pathfinder-bestiary-items/hrylbarG87YtJSgb.htm)|Breath Weapon|auto-trad|
|[HrzOkjm7joQRK88q.htm](pathfinder-bestiary-items/HrzOkjm7joQRK88q.htm)|Breath Weapon|auto-trad|
|[HScepbVGbS75R0W3.htm](pathfinder-bestiary-items/HScepbVGbS75R0W3.htm)|Negative Healing|auto-trad|
|[hsDNG89JDWAbCW9D.htm](pathfinder-bestiary-items/hsDNG89JDWAbCW9D.htm)|Beak|auto-trad|
|[hSGaU1UT8mzmPFWl.htm](pathfinder-bestiary-items/hSGaU1UT8mzmPFWl.htm)|Petrifying Gaze|auto-trad|
|[hskirvOc2VeALRbT.htm](pathfinder-bestiary-items/hskirvOc2VeALRbT.htm)|Trident|auto-trad|
|[Hss992fc1sCOcDVo.htm](pathfinder-bestiary-items/Hss992fc1sCOcDVo.htm)|Electric Reflexes|auto-trad|
|[hsU755t5zFbchRub.htm](pathfinder-bestiary-items/hsU755t5zFbchRub.htm)|Grizzly Arrival|auto-trad|
|[hT4tTJSS8Sm6qFZz.htm](pathfinder-bestiary-items/hT4tTJSS8Sm6qFZz.htm)|Shortbow|auto-trad|
|[hTNAzHRu1qlREBai.htm](pathfinder-bestiary-items/hTNAzHRu1qlREBai.htm)|Talon|auto-trad|
|[HtPykjM3gCE3oQiR.htm](pathfinder-bestiary-items/HtPykjM3gCE3oQiR.htm)|Torpor|auto-trad|
|[hU1RHjKaWqS7tyUN.htm](pathfinder-bestiary-items/hU1RHjKaWqS7tyUN.htm)|Jaws|auto-trad|
|[huAbJHogJo4HKgXV.htm](pathfinder-bestiary-items/huAbJHogJo4HKgXV.htm)|Rejuvenation|auto-trad|
|[Huqh2NJEkx1xBpoO.htm](pathfinder-bestiary-items/Huqh2NJEkx1xBpoO.htm)|Flaming Gallop|auto-trad|
|[HUT5houFl0hCEPKU.htm](pathfinder-bestiary-items/HUT5houFl0hCEPKU.htm)|Fist|auto-trad|
|[HuxqXHbNPMVfb98R.htm](pathfinder-bestiary-items/HuxqXHbNPMVfb98R.htm)|Buck|auto-trad|
|[hvdEekJwNvI4RMw1.htm](pathfinder-bestiary-items/hvdEekJwNvI4RMw1.htm)|-2 to All Saves (If Heartstone is Lost)|auto-trad|
|[HvKkuW09C9ugdKrX.htm](pathfinder-bestiary-items/HvKkuW09C9ugdKrX.htm)|Claw|auto-trad|
|[HvOzFzF3j47WTzIv.htm](pathfinder-bestiary-items/HvOzFzF3j47WTzIv.htm)|Rejuvenation|auto-trad|
|[hvrW7fEOtezPrmRv.htm](pathfinder-bestiary-items/hvrW7fEOtezPrmRv.htm)|Jaws|auto-trad|
|[hvTnI184eIoLfwPq.htm](pathfinder-bestiary-items/hvTnI184eIoLfwPq.htm)|Berserk Slam|auto-trad|
|[hWaYvdDBpwGUYAcD.htm](pathfinder-bestiary-items/hWaYvdDBpwGUYAcD.htm)|Claw|auto-trad|
|[HwtOh4jUfSRblUG1.htm](pathfinder-bestiary-items/HwtOh4jUfSRblUG1.htm)|At-Will Spells|auto-trad|
|[hwUA4Pu9qRQurhWj.htm](pathfinder-bestiary-items/hwUA4Pu9qRQurhWj.htm)|Status Sight|auto-trad|
|[hWuV8klRcI0Txecg.htm](pathfinder-bestiary-items/hWuV8klRcI0Txecg.htm)|Constant Spells|auto-trad|
|[hwvMSx3qTN1uk4iN.htm](pathfinder-bestiary-items/hwvMSx3qTN1uk4iN.htm)|Imp Venom|auto-trad|
|[hX1EY7o7nb8MZxHG.htm](pathfinder-bestiary-items/hX1EY7o7nb8MZxHG.htm)|Constant Spells|auto-trad|
|[Hxm0zO7GdMBB0p4S.htm](pathfinder-bestiary-items/Hxm0zO7GdMBB0p4S.htm)|Darkvision|auto-trad|
|[hXmPoWQ69iXdq42A.htm](pathfinder-bestiary-items/hXmPoWQ69iXdq42A.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[hxUuVqQu31iQwmv1.htm](pathfinder-bestiary-items/hxUuVqQu31iQwmv1.htm)|Draconic Frenzy|auto-trad|
|[HXXy2GteYPnNjM0H.htm](pathfinder-bestiary-items/HXXy2GteYPnNjM0H.htm)|Darkvision|auto-trad|
|[Hy8sShAxrQfV8CpX.htm](pathfinder-bestiary-items/Hy8sShAxrQfV8CpX.htm)|Darkvision|auto-trad|
|[hYcrjabm50sPyqBV.htm](pathfinder-bestiary-items/hYcrjabm50sPyqBV.htm)|Pack Attack|auto-trad|
|[hYuh2ahMpMeOTyoZ.htm](pathfinder-bestiary-items/hYuh2ahMpMeOTyoZ.htm)|Wing|auto-trad|
|[hZbArhODPV9EmZ0m.htm](pathfinder-bestiary-items/hZbArhODPV9EmZ0m.htm)|Sound Imitation|auto-trad|
|[hzcpRbbSGtnAZDHc.htm](pathfinder-bestiary-items/hzcpRbbSGtnAZDHc.htm)|Tail|auto-trad|
|[HzOfOnqjDdkk5Alq.htm](pathfinder-bestiary-items/HzOfOnqjDdkk5Alq.htm)|Change Shape|auto-trad|
|[hZqtsipwFmuBYwC2.htm](pathfinder-bestiary-items/hZqtsipwFmuBYwC2.htm)|Protean Anatomy|auto-trad|
|[HzveIcuKPfW88Zc6.htm](pathfinder-bestiary-items/HzveIcuKPfW88Zc6.htm)|At-Will Spells|auto-trad|
|[I0GaQls3ghJMJ5Ib.htm](pathfinder-bestiary-items/I0GaQls3ghJMJ5Ib.htm)|Children of the Night|auto-trad|
|[i0HAcFnNZ6dlKhwT.htm](pathfinder-bestiary-items/i0HAcFnNZ6dlKhwT.htm)|Sickle|auto-trad|
|[I0zRRHgyNtxaiNb1.htm](pathfinder-bestiary-items/I0zRRHgyNtxaiNb1.htm)|Claw|auto-trad|
|[I1o6uYgy06gzdYSk.htm](pathfinder-bestiary-items/I1o6uYgy06gzdYSk.htm)|Gallop|auto-trad|
|[I2ll4xGsFEeBgdRL.htm](pathfinder-bestiary-items/I2ll4xGsFEeBgdRL.htm)|Fast Swoop|auto-trad|
|[i3byEluD3eLH89OG.htm](pathfinder-bestiary-items/i3byEluD3eLH89OG.htm)|Horns|auto-trad|
|[i3ESVmWhqnA7Sq5u.htm](pathfinder-bestiary-items/i3ESVmWhqnA7Sq5u.htm)|Obscuring Spores|auto-trad|
|[I3pFXc7fvS41popk.htm](pathfinder-bestiary-items/I3pFXc7fvS41popk.htm)|Jet|auto-trad|
|[i561TRHP40EPtkbg.htm](pathfinder-bestiary-items/i561TRHP40EPtkbg.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[i5jZOGRVSvmKW82C.htm](pathfinder-bestiary-items/i5jZOGRVSvmKW82C.htm)|Golden Luck|auto-trad|
|[I5zNTSZyjvhCKz3D.htm](pathfinder-bestiary-items/I5zNTSZyjvhCKz3D.htm)|Pack Attack|auto-trad|
|[I6eF9oDAdSJChUG3.htm](pathfinder-bestiary-items/I6eF9oDAdSJChUG3.htm)|Darkvision|auto-trad|
|[I6lxEQY0rHjkqhZ1.htm](pathfinder-bestiary-items/I6lxEQY0rHjkqhZ1.htm)|Darkvision|auto-trad|
|[I6M7YvqndCsq8oLj.htm](pathfinder-bestiary-items/I6M7YvqndCsq8oLj.htm)|Occult Innate Spells|auto-trad|
|[i7CeNawHh8IVMpSe.htm](pathfinder-bestiary-items/i7CeNawHh8IVMpSe.htm)|Longspear|auto-trad|
|[i7ekQiw5lntgdpAT.htm](pathfinder-bestiary-items/i7ekQiw5lntgdpAT.htm)|Crossbow|auto-trad|
|[I7lbbBC8haIiOXzD.htm](pathfinder-bestiary-items/I7lbbBC8haIiOXzD.htm)|Insidious Mummy Rot|auto-trad|
|[I7qCrIGJsKvrguZB.htm](pathfinder-bestiary-items/I7qCrIGJsKvrguZB.htm)|Swoop|auto-trad|
|[i8xGkDyTBirx7HaW.htm](pathfinder-bestiary-items/i8xGkDyTBirx7HaW.htm)|Blood Scent|auto-trad|
|[I98w77MsvsKISF0z.htm](pathfinder-bestiary-items/I98w77MsvsKISF0z.htm)|Frightful Presence|auto-trad|
|[I9gdtsVJjPZVXvSq.htm](pathfinder-bestiary-items/I9gdtsVJjPZVXvSq.htm)|Tail|auto-trad|
|[i9LmbR4DKZeW0lRN.htm](pathfinder-bestiary-items/i9LmbR4DKZeW0lRN.htm)|Shard Storm|auto-trad|
|[iA3NXKVAiWzxGVaO.htm](pathfinder-bestiary-items/iA3NXKVAiWzxGVaO.htm)|Stench Suppression|auto-trad|
|[iaTfEZ50lrTlV5Ku.htm](pathfinder-bestiary-items/iaTfEZ50lrTlV5Ku.htm)|Personality Fragments|auto-trad|
|[ib3hNOxGFodAt0fV.htm](pathfinder-bestiary-items/ib3hNOxGFodAt0fV.htm)|Hand Crossbow|auto-trad|
|[ibaCsks8puTwETeL.htm](pathfinder-bestiary-items/ibaCsks8puTwETeL.htm)|Jaws|auto-trad|
|[Ibj6bgSG7PZrEoiR.htm](pathfinder-bestiary-items/Ibj6bgSG7PZrEoiR.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[ICgKjXXhG8h1g0BE.htm](pathfinder-bestiary-items/ICgKjXXhG8h1g0BE.htm)|Sneak Attack|auto-trad|
|[ICjSF2djJxtdnASi.htm](pathfinder-bestiary-items/ICjSF2djJxtdnASi.htm)|Jaws|auto-trad|
|[IcOzxaLaimK6QUsg.htm](pathfinder-bestiary-items/IcOzxaLaimK6QUsg.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[icuRBAbmsZKQdilF.htm](pathfinder-bestiary-items/icuRBAbmsZKQdilF.htm)|Dread Gaze|auto-trad|
|[iCXXt9CjiqFfuluI.htm](pathfinder-bestiary-items/iCXXt9CjiqFfuluI.htm)|Arcane Prepared Spells|auto-trad|
|[Id4tvbdVJaXtgOOu.htm](pathfinder-bestiary-items/Id4tvbdVJaXtgOOu.htm)|Jaws|auto-trad|
|[iDCc02ZuFBClDhnt.htm](pathfinder-bestiary-items/iDCc02ZuFBClDhnt.htm)|Fangs|auto-trad|
|[IdfK6A7UpGkscuOG.htm](pathfinder-bestiary-items/IdfK6A7UpGkscuOG.htm)|Ghast Fever|auto-trad|
|[idWgJAjZ5pi1uI2C.htm](pathfinder-bestiary-items/idWgJAjZ5pi1uI2C.htm)|Trunk|auto-trad|
|[iE5eD6UZECDALlPI.htm](pathfinder-bestiary-items/iE5eD6UZECDALlPI.htm)|Aura of Flame|auto-trad|
|[iEPnPUyNUxfYs63B.htm](pathfinder-bestiary-items/iEPnPUyNUxfYs63B.htm)|Attack of Opportunity (Special)|auto-trad|
|[iEqHLBXlRUlbfb37.htm](pathfinder-bestiary-items/iEqHLBXlRUlbfb37.htm)|Darkvision|auto-trad|
|[IFPnmQpUJLW9TJa9.htm](pathfinder-bestiary-items/IFPnmQpUJLW9TJa9.htm)|Wail|auto-trad|
|[IGHgjWHfJlqh674F.htm](pathfinder-bestiary-items/IGHgjWHfJlqh674F.htm)|Change Shape|auto-trad|
|[igXtaJ0MMd7h8u6O.htm](pathfinder-bestiary-items/igXtaJ0MMd7h8u6O.htm)|At-Will Spells|auto-trad|
|[iHa81hIBKMF5eXxi.htm](pathfinder-bestiary-items/iHa81hIBKMF5eXxi.htm)|Fangs|auto-trad|
|[ihI9TF3YdNJFYytf.htm](pathfinder-bestiary-items/ihI9TF3YdNJFYytf.htm)|Trample|auto-trad|
|[ii0tl0zE2tEgfPD4.htm](pathfinder-bestiary-items/ii0tl0zE2tEgfPD4.htm)|Wolf Empathy|auto-trad|
|[iIggw0JAX98xzjTY.htm](pathfinder-bestiary-items/iIggw0JAX98xzjTY.htm)|Suction|auto-trad|
|[Iiw19xLS3nyusjz1.htm](pathfinder-bestiary-items/Iiw19xLS3nyusjz1.htm)|Constrict|auto-trad|
|[IJhY1kNU4OIS7fL4.htm](pathfinder-bestiary-items/IJhY1kNU4OIS7fL4.htm)|Jaws|auto-trad|
|[ijsE8IzzuXckprkM.htm](pathfinder-bestiary-items/ijsE8IzzuXckprkM.htm)|Low-Light Vision|auto-trad|
|[IjsncXWh3Ja5Fvdo.htm](pathfinder-bestiary-items/IjsncXWh3Ja5Fvdo.htm)|Push|auto-trad|
|[iJVGp2XuaslAOfcc.htm](pathfinder-bestiary-items/iJVGp2XuaslAOfcc.htm)|Shortsword|auto-trad|
|[ikE6Os9KaQ3E0ApZ.htm](pathfinder-bestiary-items/ikE6Os9KaQ3E0ApZ.htm)|Darkvision|auto-trad|
|[IkemOnt4GVgmYXBn.htm](pathfinder-bestiary-items/IkemOnt4GVgmYXBn.htm)|At-Will Spells|auto-trad|
|[IkgoKqzoPL0WKSGG.htm](pathfinder-bestiary-items/IkgoKqzoPL0WKSGG.htm)|Divine Rituals|auto-trad|
|[ikNRBFaxo41oGwAQ.htm](pathfinder-bestiary-items/ikNRBFaxo41oGwAQ.htm)|Low-Light Vision|auto-trad|
|[IKXHj9lMhQ6Qy07y.htm](pathfinder-bestiary-items/IKXHj9lMhQ6Qy07y.htm)|Rend|auto-trad|
|[IL3VPaw5B0JI2Rce.htm](pathfinder-bestiary-items/IL3VPaw5B0JI2Rce.htm)|Constrict|auto-trad|
|[iL66w8THCpYdsALT.htm](pathfinder-bestiary-items/iL66w8THCpYdsALT.htm)|Nightmare Rider|auto-trad|
|[IlmvsLcNfbO7OmVF.htm](pathfinder-bestiary-items/IlmvsLcNfbO7OmVF.htm)|At-Will Spells|auto-trad|
|[Imio5DQk9gfplk5I.htm](pathfinder-bestiary-items/Imio5DQk9gfplk5I.htm)|Composite Shortbow|auto-trad|
|[inK7m9JBf4286NW2.htm](pathfinder-bestiary-items/inK7m9JBf4286NW2.htm)|Tail|auto-trad|
|[InkIGrftuKGUgSoo.htm](pathfinder-bestiary-items/InkIGrftuKGUgSoo.htm)|Motion Sense 60 feet|auto-trad|
|[inQcsNlqlQ4umup7.htm](pathfinder-bestiary-items/inQcsNlqlQ4umup7.htm)|Unimpeded Throw|auto-trad|
|[IoAJluZoGb9mgLGv.htm](pathfinder-bestiary-items/IoAJluZoGb9mgLGv.htm)|Inexorable|auto-trad|
|[IOgQmb8AR9d7yAlu.htm](pathfinder-bestiary-items/IOgQmb8AR9d7yAlu.htm)|At-Will Spells|auto-trad|
|[ioskOBeviPp2ll8D.htm](pathfinder-bestiary-items/ioskOBeviPp2ll8D.htm)|Web|auto-trad|
|[iP02Y8PDHLNbctbc.htm](pathfinder-bestiary-items/iP02Y8PDHLNbctbc.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[IqlIzn7bY9FduX6g.htm](pathfinder-bestiary-items/IqlIzn7bY9FduX6g.htm)|Menacing Guardian|auto-trad|
|[iqVhVxGvfUFnJ2uq.htm](pathfinder-bestiary-items/iqVhVxGvfUFnJ2uq.htm)|Darkvision|auto-trad|
|[IQwShRAySaUkCUW5.htm](pathfinder-bestiary-items/IQwShRAySaUkCUW5.htm)|Darkvision|auto-trad|
|[iQz3YxQPSZdpvSJi.htm](pathfinder-bestiary-items/iQz3YxQPSZdpvSJi.htm)|Regeneration 30 (Deactivated by Cold Iron)|auto-trad|
|[ir8tXGoz964QTWmv.htm](pathfinder-bestiary-items/ir8tXGoz964QTWmv.htm)|Web|auto-trad|
|[ira6umGEGOXqSyIk.htm](pathfinder-bestiary-items/ira6umGEGOXqSyIk.htm)|Water Spout|auto-trad|
|[IrW6qhUnZpM1uAqK.htm](pathfinder-bestiary-items/IrW6qhUnZpM1uAqK.htm)|Darkvision|auto-trad|
|[is4tKm65LxMVSzQV.htm](pathfinder-bestiary-items/is4tKm65LxMVSzQV.htm)|Trident|auto-trad|
|[Is4V63biDtNBVpi2.htm](pathfinder-bestiary-items/Is4V63biDtNBVpi2.htm)|Improved Push 5 feet|auto-trad|
|[IsMVcoiRI9sN4iQe.htm](pathfinder-bestiary-items/IsMVcoiRI9sN4iQe.htm)|Frightful Presence|auto-trad|
|[IsRnnfl27oJF1UGY.htm](pathfinder-bestiary-items/IsRnnfl27oJF1UGY.htm)|Divine Innate Spells|auto-trad|
|[IsvDyyM2ZO6XxnsU.htm](pathfinder-bestiary-items/IsvDyyM2ZO6XxnsU.htm)|Blood Frenzy|auto-trad|
|[iSVjVMy4wUULtEXe.htm](pathfinder-bestiary-items/iSVjVMy4wUULtEXe.htm)|Infuse Weapons|auto-trad|
|[Ita7hOSbzKOHxexv.htm](pathfinder-bestiary-items/Ita7hOSbzKOHxexv.htm)|Drench|auto-trad|
|[iTaObjgnXpF66Sz5.htm](pathfinder-bestiary-items/iTaObjgnXpF66Sz5.htm)|Rapier|auto-trad|
|[iTAZj4rMOPRWWJDP.htm](pathfinder-bestiary-items/iTAZj4rMOPRWWJDP.htm)|Corpse Throwing|auto-trad|
|[itJzZD6rxkMhtQQZ.htm](pathfinder-bestiary-items/itJzZD6rxkMhtQQZ.htm)|Draconic Frenzy|auto-trad|
|[ITkyellSfoRYxVGk.htm](pathfinder-bestiary-items/ITkyellSfoRYxVGk.htm)|Divine Grace|auto-trad|
|[itPzrsYivMMWwHm3.htm](pathfinder-bestiary-items/itPzrsYivMMWwHm3.htm)|Coven Spells|auto-trad|
|[itqvdf5hwWK45QOe.htm](pathfinder-bestiary-items/itqvdf5hwWK45QOe.htm)|Flail|auto-trad|
|[Itw1pfAvHSiAptUZ.htm](pathfinder-bestiary-items/Itw1pfAvHSiAptUZ.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[iu1YHlbvh64HBaor.htm](pathfinder-bestiary-items/iu1YHlbvh64HBaor.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[IUA7ZWVTepsameDv.htm](pathfinder-bestiary-items/IUA7ZWVTepsameDv.htm)|Fangs|auto-trad|
|[iUAQwRninJ973LoN.htm](pathfinder-bestiary-items/iUAQwRninJ973LoN.htm)|Jaws|auto-trad|
|[iuFRjKw2PLJl3E3F.htm](pathfinder-bestiary-items/iuFRjKw2PLJl3E3F.htm)|Pounce|auto-trad|
|[IuK4DrLEyP6BmKi2.htm](pathfinder-bestiary-items/IuK4DrLEyP6BmKi2.htm)|Draconic Frenzy|auto-trad|
|[IUmBG7HtxvrIOHUR.htm](pathfinder-bestiary-items/IUmBG7HtxvrIOHUR.htm)|Generate Bomb|auto-trad|
|[IUNBun7zEVrlp6AY.htm](pathfinder-bestiary-items/IUNBun7zEVrlp6AY.htm)|Darkvision|auto-trad|
|[iUQXXOate2Qv004f.htm](pathfinder-bestiary-items/iUQXXOate2Qv004f.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[iuyu0qM1TjakNK8f.htm](pathfinder-bestiary-items/iuyu0qM1TjakNK8f.htm)|Occult Innate Spells|auto-trad|
|[iuyYNQtDMprDDoZH.htm](pathfinder-bestiary-items/iuyYNQtDMprDDoZH.htm)|Mist Escape|auto-trad|
|[IvEp0FjtG1bxA4Xw.htm](pathfinder-bestiary-items/IvEp0FjtG1bxA4Xw.htm)|Jaws|auto-trad|
|[ivsgBmmJjZW5Wr5S.htm](pathfinder-bestiary-items/ivsgBmmJjZW5Wr5S.htm)|Blood Soak|auto-trad|
|[iVZkQBGcIBXlxkcc.htm](pathfinder-bestiary-items/iVZkQBGcIBXlxkcc.htm)|Frightful Presence|auto-trad|
|[iw0W5WgXZN9Y21ul.htm](pathfinder-bestiary-items/iw0W5WgXZN9Y21ul.htm)|Grab|auto-trad|
|[IWJbZROV26XdSO2U.htm](pathfinder-bestiary-items/IWJbZROV26XdSO2U.htm)|Arcane Innate Spells|auto-trad|
|[IWkgxOKsoYOGSw3E.htm](pathfinder-bestiary-items/IWkgxOKsoYOGSw3E.htm)|Split|auto-trad|
|[iwppReF9hEA7igPO.htm](pathfinder-bestiary-items/iwppReF9hEA7igPO.htm)|Jaws|auto-trad|
|[ix1KQX8Ji2g1e9ID.htm](pathfinder-bestiary-items/ix1KQX8Ji2g1e9ID.htm)|Skewer|auto-trad|
|[iXBVxZ5RntmImlUb.htm](pathfinder-bestiary-items/iXBVxZ5RntmImlUb.htm)|Attack of Opportunity|auto-trad|
|[IXjaAxkgZsz5958E.htm](pathfinder-bestiary-items/IXjaAxkgZsz5958E.htm)|Free Expression|auto-trad|
|[ixOGpstVQFBXDi3I.htm](pathfinder-bestiary-items/ixOGpstVQFBXDi3I.htm)|Low-Light Vision|auto-trad|
|[IXWkQwpWIcMopGvV.htm](pathfinder-bestiary-items/IXWkQwpWIcMopGvV.htm)|Arm|auto-trad|
|[iXwmnPnBaxNyz50d.htm](pathfinder-bestiary-items/iXwmnPnBaxNyz50d.htm)|Divine Innate Spells|auto-trad|
|[IyKWWIgBkLnO2AXB.htm](pathfinder-bestiary-items/IyKWWIgBkLnO2AXB.htm)|Tremorsense (Imprecise) 100 feet|auto-trad|
|[iySuinX75ehOzFDa.htm](pathfinder-bestiary-items/iySuinX75ehOzFDa.htm)|Arcane Innate Spells|auto-trad|
|[IYYi7B0j0PD17IXo.htm](pathfinder-bestiary-items/IYYi7B0j0PD17IXo.htm)|Fast Swallow|auto-trad|
|[IYZ7DcFhTBCpe9MA.htm](pathfinder-bestiary-items/IYZ7DcFhTBCpe9MA.htm)|Staff|auto-trad|
|[iz8CpHkcAiNnZfvZ.htm](pathfinder-bestiary-items/iz8CpHkcAiNnZfvZ.htm)|Divine Innate Spells|auto-trad|
|[IzdeO18d4uSX3LyW.htm](pathfinder-bestiary-items/IzdeO18d4uSX3LyW.htm)|Darkvision|auto-trad|
|[iZTnwEyJWIaqvI8B.htm](pathfinder-bestiary-items/iZTnwEyJWIaqvI8B.htm)|Ferocity|auto-trad|
|[izty6IGZL6BowyUS.htm](pathfinder-bestiary-items/izty6IGZL6BowyUS.htm)|Pack Attack|auto-trad|
|[j0pCK3mNfxlgQemu.htm](pathfinder-bestiary-items/j0pCK3mNfxlgQemu.htm)|Tail|auto-trad|
|[J1HcaTe5BjGFYDgL.htm](pathfinder-bestiary-items/J1HcaTe5BjGFYDgL.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[J2jO45MMtWXrfesA.htm](pathfinder-bestiary-items/J2jO45MMtWXrfesA.htm)|Grab|auto-trad|
|[j2n6AMiW1Ve4yYnf.htm](pathfinder-bestiary-items/j2n6AMiW1Ve4yYnf.htm)|Claw|auto-trad|
|[J3HCGhXaQnSxfglZ.htm](pathfinder-bestiary-items/J3HCGhXaQnSxfglZ.htm)|Tail|auto-trad|
|[j3ILBYm7ssA8p2Nz.htm](pathfinder-bestiary-items/j3ILBYm7ssA8p2Nz.htm)|Fire Mote|auto-trad|
|[J3OtjjfFKxnDTSmx.htm](pathfinder-bestiary-items/J3OtjjfFKxnDTSmx.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[J4J0VphHvmjPrss7.htm](pathfinder-bestiary-items/J4J0VphHvmjPrss7.htm)|Arcane Prepared Spells|auto-trad|
|[J53J6OFXMFOq27dA.htm](pathfinder-bestiary-items/J53J6OFXMFOq27dA.htm)|Woodland Stride|auto-trad|
|[j5LBfiOQPypQQTM6.htm](pathfinder-bestiary-items/j5LBfiOQPypQQTM6.htm)|Dissonant Note|auto-trad|
|[j6bfMiK3Ghz6Y86o.htm](pathfinder-bestiary-items/j6bfMiK3Ghz6Y86o.htm)|Swiftness|auto-trad|
|[J6pZtpCOYKA1AxsU.htm](pathfinder-bestiary-items/J6pZtpCOYKA1AxsU.htm)|Blood of the Night|auto-trad|
|[j6xu1V9riuAOrTpK.htm](pathfinder-bestiary-items/j6xu1V9riuAOrTpK.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[J73AKz9wgVvUQWFo.htm](pathfinder-bestiary-items/J73AKz9wgVvUQWFo.htm)|Low-Light Vision|auto-trad|
|[J7jljAzKdEbTuHtB.htm](pathfinder-bestiary-items/J7jljAzKdEbTuHtB.htm)|Darkvision|auto-trad|
|[j7Nh5AH6RZ2HuPM1.htm](pathfinder-bestiary-items/j7Nh5AH6RZ2HuPM1.htm)|Breath Weapon|auto-trad|
|[j7PKag1rt4Uml8gg.htm](pathfinder-bestiary-items/j7PKag1rt4Uml8gg.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[j7RuQ48Pmg4V96SN.htm](pathfinder-bestiary-items/j7RuQ48Pmg4V96SN.htm)|Darkvision|auto-trad|
|[J8d6vqQrqGXi53bU.htm](pathfinder-bestiary-items/J8d6vqQrqGXi53bU.htm)|Verdant Burst|auto-trad|
|[J8dl99lVefC7uvbV.htm](pathfinder-bestiary-items/J8dl99lVefC7uvbV.htm)|Lightning Lash|auto-trad|
|[j8IZenEYeIuNHysF.htm](pathfinder-bestiary-items/j8IZenEYeIuNHysF.htm)|Darkvision|auto-trad|
|[j8XDp3PWM6N8DyRV.htm](pathfinder-bestiary-items/j8XDp3PWM6N8DyRV.htm)|Telepathy 100 feet|auto-trad|
|[J9d2rpUp8ZsHKZWW.htm](pathfinder-bestiary-items/J9d2rpUp8ZsHKZWW.htm)|Spear|auto-trad|
|[j9R2dejFnXClVzKR.htm](pathfinder-bestiary-items/j9R2dejFnXClVzKR.htm)|Breath Weapon|auto-trad|
|[ja5ZUKm30pmuCb0m.htm](pathfinder-bestiary-items/ja5ZUKm30pmuCb0m.htm)|Primal Innate Spells|auto-trad|
|[JaEjyp5taeG7YCCo.htm](pathfinder-bestiary-items/JaEjyp5taeG7YCCo.htm)|Ethereal Step|auto-trad|
|[jAZY8WC1BtImPyEF.htm](pathfinder-bestiary-items/jAZY8WC1BtImPyEF.htm)|Gauntlet|auto-trad|
|[jb3f12sfPd9nzwVN.htm](pathfinder-bestiary-items/jb3f12sfPd9nzwVN.htm)|Claw|auto-trad|
|[jbjsVEFvVsslwVHa.htm](pathfinder-bestiary-items/jbjsVEFvVsslwVHa.htm)|Tail Lash|auto-trad|
|[JBksJycZVWAJ0ESS.htm](pathfinder-bestiary-items/JBksJycZVWAJ0ESS.htm)|Invoke Rune|auto-trad|
|[JBSyvkullKxEJ94w.htm](pathfinder-bestiary-items/JBSyvkullKxEJ94w.htm)|Jaws|auto-trad|
|[JCdcBeryZm0DvHqA.htm](pathfinder-bestiary-items/JCdcBeryZm0DvHqA.htm)|Low-Light Vision|auto-trad|
|[JChdzk7WZ7KUXqF7.htm](pathfinder-bestiary-items/JChdzk7WZ7KUXqF7.htm)|Calcification|auto-trad|
|[jcHkvPONg4SesmVb.htm](pathfinder-bestiary-items/jcHkvPONg4SesmVb.htm)|Electricity Aura|auto-trad|
|[Jcl5rz38QkHYDfHx.htm](pathfinder-bestiary-items/Jcl5rz38QkHYDfHx.htm)|Negative Healing|auto-trad|
|[jD38LrotmqVxwz0a.htm](pathfinder-bestiary-items/jD38LrotmqVxwz0a.htm)|Claw|auto-trad|
|[jdam6t7WxAcXyrY6.htm](pathfinder-bestiary-items/jdam6t7WxAcXyrY6.htm)|Claw|auto-trad|
|[jdNRBv6RogEW3DdF.htm](pathfinder-bestiary-items/jdNRBv6RogEW3DdF.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[JEBTVmt5rc6G8tOb.htm](pathfinder-bestiary-items/JEBTVmt5rc6G8tOb.htm)|Snatch|auto-trad|
|[jeN7zh5S6wH6ciYZ.htm](pathfinder-bestiary-items/jeN7zh5S6wH6ciYZ.htm)|Occult Innate Spells|auto-trad|
|[Jf6SQTejkZnEWA2T.htm](pathfinder-bestiary-items/Jf6SQTejkZnEWA2T.htm)|Bo Staff|auto-trad|
|[jfGKdhMjdkxsNHCf.htm](pathfinder-bestiary-items/jfGKdhMjdkxsNHCf.htm)|Tentacle|auto-trad|
|[jfJXOP78x7xyHplx.htm](pathfinder-bestiary-items/jfJXOP78x7xyHplx.htm)|Pack Attack|auto-trad|
|[JfUL91jTnBSObypg.htm](pathfinder-bestiary-items/JfUL91jTnBSObypg.htm)|Low-Light Vision|auto-trad|
|[jG0R8rvY0cpJadHK.htm](pathfinder-bestiary-items/jG0R8rvY0cpJadHK.htm)|Fist|auto-trad|
|[Jg5WkDHWpi1JRQ9k.htm](pathfinder-bestiary-items/Jg5WkDHWpi1JRQ9k.htm)|Vulnerable to Stone to Flesh|auto-trad|
|[JGlsPzMEmEwis19F.htm](pathfinder-bestiary-items/JGlsPzMEmEwis19F.htm)|Constant Spells|auto-trad|
|[JgxdK2Dmby1lwpfp.htm](pathfinder-bestiary-items/JgxdK2Dmby1lwpfp.htm)|Claw|auto-trad|
|[jhCCqt7r2yw8bad1.htm](pathfinder-bestiary-items/jhCCqt7r2yw8bad1.htm)|Leaves|auto-trad|
|[JhurblUzrKIPeUrP.htm](pathfinder-bestiary-items/JhurblUzrKIPeUrP.htm)|Flaming Gallop|auto-trad|
|[jHv2A4kh5nyPGSxF.htm](pathfinder-bestiary-items/jHv2A4kh5nyPGSxF.htm)|Jaws|auto-trad|
|[ji4Sith8FiIfdvlL.htm](pathfinder-bestiary-items/ji4Sith8FiIfdvlL.htm)|Darkvision|auto-trad|
|[jInvivt7qyjg6Q4R.htm](pathfinder-bestiary-items/jInvivt7qyjg6Q4R.htm)|Dagger|auto-trad|
|[JIRrOHVjqWOmt2TV.htm](pathfinder-bestiary-items/JIRrOHVjqWOmt2TV.htm)|Improved Grab|auto-trad|
|[jJ0nUZ8hgrpWlXtS.htm](pathfinder-bestiary-items/jJ0nUZ8hgrpWlXtS.htm)|Snatch|auto-trad|
|[jj8vv07lGAp4FELU.htm](pathfinder-bestiary-items/jj8vv07lGAp4FELU.htm)|Skeletal Lore|auto-trad|
|[JJc1A4ggjxl0zB12.htm](pathfinder-bestiary-items/JJc1A4ggjxl0zB12.htm)|Grab|auto-trad|
|[jjcSFQRRi1OZjeeX.htm](pathfinder-bestiary-items/jjcSFQRRi1OZjeeX.htm)|Inspiration|auto-trad|
|[JjOstNeMHauECGcr.htm](pathfinder-bestiary-items/JjOstNeMHauECGcr.htm)|Luminescent Aura|auto-trad|
|[JjvXFkGRXacPPnXd.htm](pathfinder-bestiary-items/JjvXFkGRXacPPnXd.htm)|Tail|auto-trad|
|[jK2StyBBiefgpRNw.htm](pathfinder-bestiary-items/jK2StyBBiefgpRNw.htm)|Darkvision|auto-trad|
|[JkIYhF9MSDwxXdYc.htm](pathfinder-bestiary-items/JkIYhF9MSDwxXdYc.htm)|At-Will Spells|auto-trad|
|[jKUKY0ByHy5vvU8u.htm](pathfinder-bestiary-items/jKUKY0ByHy5vvU8u.htm)|Longbow|auto-trad|
|[jL2ksRd3mnetwWV2.htm](pathfinder-bestiary-items/jL2ksRd3mnetwWV2.htm)|Tongue|auto-trad|
|[jL9PDQ27Dz6UAMOH.htm](pathfinder-bestiary-items/jL9PDQ27Dz6UAMOH.htm)|Scimitar|auto-trad|
|[jmDi2EBAEE9yAyK3.htm](pathfinder-bestiary-items/jmDi2EBAEE9yAyK3.htm)|Sea Serpent Algae|auto-trad|
|[jmPDVPebOQ3XVOwA.htm](pathfinder-bestiary-items/jmPDVPebOQ3XVOwA.htm)|Boar Charge|auto-trad|
|[jMTCxlT7XAx5M91e.htm](pathfinder-bestiary-items/jMTCxlT7XAx5M91e.htm)|Darkvision|auto-trad|
|[jN4atm9Y2nSfZFtO.htm](pathfinder-bestiary-items/jN4atm9Y2nSfZFtO.htm)|Improved Grab|auto-trad|
|[jnZDE6piOq3rzmdg.htm](pathfinder-bestiary-items/jnZDE6piOq3rzmdg.htm)|Create Spawn|auto-trad|
|[joBYS96mXSnZC1WB.htm](pathfinder-bestiary-items/joBYS96mXSnZC1WB.htm)|Arcane Innate Spells|auto-trad|
|[JOcNBwBFEc7623Hf.htm](pathfinder-bestiary-items/JOcNBwBFEc7623Hf.htm)|Lightning Lash|auto-trad|
|[JOlynEzl2UrSa16n.htm](pathfinder-bestiary-items/JOlynEzl2UrSa16n.htm)|Energy Drain|auto-trad|
|[jpBSdZzftYrVeiU0.htm](pathfinder-bestiary-items/jpBSdZzftYrVeiU0.htm)|Dragon Chill|auto-trad|
|[JQ3FZtUvvt6RRRXF.htm](pathfinder-bestiary-items/JQ3FZtUvvt6RRRXF.htm)|Darkvision|auto-trad|
|[JqajO4nisN5Ddc6s.htm](pathfinder-bestiary-items/JqajO4nisN5Ddc6s.htm)|Divine Innate Spells|auto-trad|
|[jqPcPNAR4c3cEI3M.htm](pathfinder-bestiary-items/jqPcPNAR4c3cEI3M.htm)|At-Will Spells|auto-trad|
|[JQxkxEB6JK80gsvb.htm](pathfinder-bestiary-items/JQxkxEB6JK80gsvb.htm)|Darkvision|auto-trad|
|[JrkBo5yZM19wv3KX.htm](pathfinder-bestiary-items/JrkBo5yZM19wv3KX.htm)|Enormous|auto-trad|
|[JRoSNngd9XENOzDl.htm](pathfinder-bestiary-items/JRoSNngd9XENOzDl.htm)|Attack of Opportunity|auto-trad|
|[jrRWYmGu9vVkAfdv.htm](pathfinder-bestiary-items/jrRWYmGu9vVkAfdv.htm)|Darkvision|auto-trad|
|[JRWZ0pJnxuJyvINn.htm](pathfinder-bestiary-items/JRWZ0pJnxuJyvINn.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[jsgHC6kdtZdnAJMT.htm](pathfinder-bestiary-items/jsgHC6kdtZdnAJMT.htm)|Ink Cloud|auto-trad|
|[jt4PpVxiITWfM8ox.htm](pathfinder-bestiary-items/jt4PpVxiITWfM8ox.htm)|Jaws|auto-trad|
|[jt6yBdIcUHJLCmAw.htm](pathfinder-bestiary-items/jt6yBdIcUHJLCmAw.htm)|Vine|auto-trad|
|[jtb1mM7gOuCllfQf.htm](pathfinder-bestiary-items/jtb1mM7gOuCllfQf.htm)|Darkvision|auto-trad|
|[jTsP7LKJDD7LlPlH.htm](pathfinder-bestiary-items/jTsP7LKJDD7LlPlH.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[JttXYbMjKJ7Vv7Zy.htm](pathfinder-bestiary-items/JttXYbMjKJ7Vv7Zy.htm)|Darkvision|auto-trad|
|[jUfHzGj1QzWogY7T.htm](pathfinder-bestiary-items/jUfHzGj1QzWogY7T.htm)|Frightful Presence|auto-trad|
|[JUgPxjF2b7DYZwEu.htm](pathfinder-bestiary-items/JUgPxjF2b7DYZwEu.htm)|Grab|auto-trad|
|[juKKPoneZ4x5mqlo.htm](pathfinder-bestiary-items/juKKPoneZ4x5mqlo.htm)|Negative Healing|auto-trad|
|[JUlSpx5USh1nfM12.htm](pathfinder-bestiary-items/JUlSpx5USh1nfM12.htm)|Jaws|auto-trad|
|[jv3hOJvw3g4A5KWY.htm](pathfinder-bestiary-items/jv3hOJvw3g4A5KWY.htm)|Draconic Frenzy|auto-trad|
|[JV5W1wo8td9aBqBd.htm](pathfinder-bestiary-items/JV5W1wo8td9aBqBd.htm)|Arcane Prepared Spells|auto-trad|
|[JvFUFSxHH2EThNAC.htm](pathfinder-bestiary-items/JvFUFSxHH2EThNAC.htm)|Breath Weapon|auto-trad|
|[jvmfyWcZp1aZiZqS.htm](pathfinder-bestiary-items/jvmfyWcZp1aZiZqS.htm)|Claw Frenzy|auto-trad|
|[Jvmi4nru2sCMB5Hn.htm](pathfinder-bestiary-items/Jvmi4nru2sCMB5Hn.htm)|Draconic Momentum|auto-trad|
|[JVyi4C8jthjCBGN6.htm](pathfinder-bestiary-items/JVyi4C8jthjCBGN6.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[jvZUoPLS8OA1SQTw.htm](pathfinder-bestiary-items/jvZUoPLS8OA1SQTw.htm)|Shortsword|auto-trad|
|[jW0BtgVKN8XQhLKs.htm](pathfinder-bestiary-items/jW0BtgVKN8XQhLKs.htm)|Divine Innate Spells|auto-trad|
|[JwLrjprg0tRZcFsL.htm](pathfinder-bestiary-items/JwLrjprg0tRZcFsL.htm)|Claw|auto-trad|
|[JWlYLXxlI4IRYbeF.htm](pathfinder-bestiary-items/JWlYLXxlI4IRYbeF.htm)|Greataxe|auto-trad|
|[JWS021oC5szbCrAS.htm](pathfinder-bestiary-items/JWS021oC5szbCrAS.htm)|Push 15 feet|auto-trad|
|[jwtPWph6mWdPZhf5.htm](pathfinder-bestiary-items/jwtPWph6mWdPZhf5.htm)|Crossbow|auto-trad|
|[Jxa5SzH4ybrGFB9q.htm](pathfinder-bestiary-items/Jxa5SzH4ybrGFB9q.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Jxw7RvEERHGRzycq.htm](pathfinder-bestiary-items/Jxw7RvEERHGRzycq.htm)|Negative Healing|auto-trad|
|[jYjpln4mc6udm6oF.htm](pathfinder-bestiary-items/jYjpln4mc6udm6oF.htm)|Low-Light Vision|auto-trad|
|[JYkKoCx49R8WXnXW.htm](pathfinder-bestiary-items/JYkKoCx49R8WXnXW.htm)|Tied to the Land|auto-trad|
|[JysKih2FAoEH8Jdb.htm](pathfinder-bestiary-items/JysKih2FAoEH8Jdb.htm)|Frightful Presence|auto-trad|
|[jYT3UtsBe68guWz1.htm](pathfinder-bestiary-items/jYT3UtsBe68guWz1.htm)|Claw|auto-trad|
|[jZ8U8eTiEuKU6toZ.htm](pathfinder-bestiary-items/jZ8U8eTiEuKU6toZ.htm)|Heat|auto-trad|
|[JzE14vwBQYTNvg3Q.htm](pathfinder-bestiary-items/JzE14vwBQYTNvg3Q.htm)|Shadow Shift|auto-trad|
|[JzIDoWltig01Ezsi.htm](pathfinder-bestiary-items/JzIDoWltig01Ezsi.htm)|Fist|auto-trad|
|[jzku4qcmz6H3dk9C.htm](pathfinder-bestiary-items/jzku4qcmz6H3dk9C.htm)|Torch|auto-trad|
|[jZxRFq5KrTWsA6Ut.htm](pathfinder-bestiary-items/jZxRFq5KrTWsA6Ut.htm)|Low-Light Vision|auto-trad|
|[jZXTqe1YmviCFvap.htm](pathfinder-bestiary-items/jZXTqe1YmviCFvap.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[K09n3CnAeuaSeD9k.htm](pathfinder-bestiary-items/K09n3CnAeuaSeD9k.htm)|Shake It Off|auto-trad|
|[k0b4P1F8sPglTEue.htm](pathfinder-bestiary-items/k0b4P1F8sPglTEue.htm)|Hurricane Blast|auto-trad|
|[k1kPrQ3Krhcipl4E.htm](pathfinder-bestiary-items/k1kPrQ3Krhcipl4E.htm)|Fist|auto-trad|
|[K2aOOR33x8FPHyVI.htm](pathfinder-bestiary-items/K2aOOR33x8FPHyVI.htm)|Darkvision|auto-trad|
|[K2pwGyKWkDm1x4Xh.htm](pathfinder-bestiary-items/K2pwGyKWkDm1x4Xh.htm)|Assume Form|auto-trad|
|[k2r2GtyzX2R9mQrE.htm](pathfinder-bestiary-items/k2r2GtyzX2R9mQrE.htm)|Darkvision|auto-trad|
|[K2x7jUEcU74IIJ3D.htm](pathfinder-bestiary-items/K2x7jUEcU74IIJ3D.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[K33vakNPDetkh1iP.htm](pathfinder-bestiary-items/K33vakNPDetkh1iP.htm)|Cold Iron Silver Flame Whip|auto-trad|
|[k3r3UHSeKp4tTrp0.htm](pathfinder-bestiary-items/k3r3UHSeKp4tTrp0.htm)|Divine Innate Spells|auto-trad|
|[k3uphptV5MjVwlbH.htm](pathfinder-bestiary-items/k3uphptV5MjVwlbH.htm)|Darkvision|auto-trad|
|[k45QQ0xBwRtwJCeF.htm](pathfinder-bestiary-items/k45QQ0xBwRtwJCeF.htm)|Darkvision|auto-trad|
|[K56mdzOHeveTvQ5D.htm](pathfinder-bestiary-items/K56mdzOHeveTvQ5D.htm)|Giant Wasp Venom|auto-trad|
|[k72C42LOvjUNSP6f.htm](pathfinder-bestiary-items/k72C42LOvjUNSP6f.htm)|Freezing Blood|auto-trad|
|[k8k6bZAdPTdhZfDp.htm](pathfinder-bestiary-items/k8k6bZAdPTdhZfDp.htm)|At-Will Spells|auto-trad|
|[k8mtTYrWrnHjBMCO.htm](pathfinder-bestiary-items/k8mtTYrWrnHjBMCO.htm)|Breach|auto-trad|
|[K8rZwgv45XvIe8vL.htm](pathfinder-bestiary-items/K8rZwgv45XvIe8vL.htm)|Ogre Hook|auto-trad|
|[k9T6UTS86VkzB0LP.htm](pathfinder-bestiary-items/k9T6UTS86VkzB0LP.htm)|At-Will Spells|auto-trad|
|[Ka4LviiNUhy4FNzo.htm](pathfinder-bestiary-items/Ka4LviiNUhy4FNzo.htm)|Talon|auto-trad|
|[kAUwyvDAhkgyFkme.htm](pathfinder-bestiary-items/kAUwyvDAhkgyFkme.htm)|Draconic Frenzy|auto-trad|
|[kayBhD4IXys5Ppt8.htm](pathfinder-bestiary-items/kayBhD4IXys5Ppt8.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[kB3jH0EObomim8bP.htm](pathfinder-bestiary-items/kB3jH0EObomim8bP.htm)|Tail Lash|auto-trad|
|[kBcZLCEv6TSReExJ.htm](pathfinder-bestiary-items/kBcZLCEv6TSReExJ.htm)|Claw|auto-trad|
|[kBOVK9j2yxJEBYt7.htm](pathfinder-bestiary-items/kBOVK9j2yxJEBYt7.htm)|Low-Light Vision|auto-trad|
|[kBr3AKRl4vlCwVK2.htm](pathfinder-bestiary-items/kBr3AKRl4vlCwVK2.htm)|Greatclub|auto-trad|
|[KcCw29PtlpuADI1x.htm](pathfinder-bestiary-items/KcCw29PtlpuADI1x.htm)|Pack Attack|auto-trad|
|[kCfoRidunIYcTXeZ.htm](pathfinder-bestiary-items/kCfoRidunIYcTXeZ.htm)|Divine Innate Spells|auto-trad|
|[KCUHGmNla57hO2ZK.htm](pathfinder-bestiary-items/KCUHGmNla57hO2ZK.htm)|Reshape Reality|auto-trad|
|[kcYtaqPWsMiKHYty.htm](pathfinder-bestiary-items/kcYtaqPWsMiKHYty.htm)|Haunting Melody|auto-trad|
|[KD8sQ1IJ6El9ImGR.htm](pathfinder-bestiary-items/KD8sQ1IJ6El9ImGR.htm)|Tail|auto-trad|
|[KddvbFPsHJAfqGAc.htm](pathfinder-bestiary-items/KddvbFPsHJAfqGAc.htm)|Wing Deflection|auto-trad|
|[kDtkieHx63QY56es.htm](pathfinder-bestiary-items/kDtkieHx63QY56es.htm)|Horn|auto-trad|
|[KDy693oZjUA79500.htm](pathfinder-bestiary-items/KDy693oZjUA79500.htm)|Darkvision|auto-trad|
|[KESgZ7EFPclSlLY8.htm](pathfinder-bestiary-items/KESgZ7EFPclSlLY8.htm)|Drink Blood|auto-trad|
|[KeTrwbKWih6HyuoM.htm](pathfinder-bestiary-items/KeTrwbKWih6HyuoM.htm)|Fast Healing 7|auto-trad|
|[kFeAtCbycBHyIGcR.htm](pathfinder-bestiary-items/kFeAtCbycBHyIGcR.htm)|Attack of Opportunity|auto-trad|
|[KfFb9f7ZnoNzfyNt.htm](pathfinder-bestiary-items/KfFb9f7ZnoNzfyNt.htm)|Extra Reaction|auto-trad|
|[kFgfAbXaJbI8otfZ.htm](pathfinder-bestiary-items/kFgfAbXaJbI8otfZ.htm)|Shortsword|auto-trad|
|[kfjRR8MHRHoxgCkF.htm](pathfinder-bestiary-items/kfjRR8MHRHoxgCkF.htm)|Grab|auto-trad|
|[KFObwL3fuLfRhdtm.htm](pathfinder-bestiary-items/KFObwL3fuLfRhdtm.htm)|Death Flame|auto-trad|
|[kGKrPttfeXrnHGWr.htm](pathfinder-bestiary-items/kGKrPttfeXrnHGWr.htm)|Mummy Rot|auto-trad|
|[kGQg9F0ZgW7fp4DB.htm](pathfinder-bestiary-items/kGQg9F0ZgW7fp4DB.htm)|Tongue Grab|auto-trad|
|[kgt9znbnxYGyxtvV.htm](pathfinder-bestiary-items/kgt9znbnxYGyxtvV.htm)|Fog Vision|auto-trad|
|[KhgTIBtP3uB7Ls0w.htm](pathfinder-bestiary-items/KhgTIBtP3uB7Ls0w.htm)|Fireball Breath|auto-trad|
|[KHmyhQ0lVwwKOLlC.htm](pathfinder-bestiary-items/KHmyhQ0lVwwKOLlC.htm)|At-Will Spells|auto-trad|
|[kHnahHvifJfCVS11.htm](pathfinder-bestiary-items/kHnahHvifJfCVS11.htm)|Telepathy 100 feet|auto-trad|
|[kHo1nNFrNvhFTdVm.htm](pathfinder-bestiary-items/kHo1nNFrNvhFTdVm.htm)|Gallop|auto-trad|
|[khvFVWc6itmsBv7T.htm](pathfinder-bestiary-items/khvFVWc6itmsBv7T.htm)|Darkvision|auto-trad|
|[ki3C4xNREH5G9Knv.htm](pathfinder-bestiary-items/ki3C4xNREH5G9Knv.htm)|Change Shape|auto-trad|
|[KiBIFxCCOgpsuMvK.htm](pathfinder-bestiary-items/KiBIFxCCOgpsuMvK.htm)|Sneak Attack|auto-trad|
|[KidhmTPJWxNcC0db.htm](pathfinder-bestiary-items/KidhmTPJWxNcC0db.htm)|Darkvision|auto-trad|
|[kiIWJXY9ATN5NdpV.htm](pathfinder-bestiary-items/kiIWJXY9ATN5NdpV.htm)|At-Will Spells|auto-trad|
|[kiLj66ryWPKBgZKh.htm](pathfinder-bestiary-items/kiLj66ryWPKBgZKh.htm)|Woodland Stride|auto-trad|
|[Kj1uqisygxPxuaC9.htm](pathfinder-bestiary-items/Kj1uqisygxPxuaC9.htm)|Spider Speak|auto-trad|
|[KjBveNLcC22szBCP.htm](pathfinder-bestiary-items/KjBveNLcC22szBCP.htm)|Sneak Attack|auto-trad|
|[Kjifw1jgDvfmrEGU.htm](pathfinder-bestiary-items/Kjifw1jgDvfmrEGU.htm)|Steal Shadow|auto-trad|
|[kjpNSY1WlZHMXIS6.htm](pathfinder-bestiary-items/kjpNSY1WlZHMXIS6.htm)|Powerful Charge|auto-trad|
|[KjSDkdNQbNUj0pJe.htm](pathfinder-bestiary-items/KjSDkdNQbNUj0pJe.htm)|Darkvision|auto-trad|
|[KkDrWaAN2q7y1azB.htm](pathfinder-bestiary-items/KkDrWaAN2q7y1azB.htm)|Jaws|auto-trad|
|[kkOWzP3DmckLh5vC.htm](pathfinder-bestiary-items/kkOWzP3DmckLh5vC.htm)|Divine Innate Spells|auto-trad|
|[KlVVVsJad72RahoI.htm](pathfinder-bestiary-items/KlVVVsJad72RahoI.htm)|Javelin|auto-trad|
|[KLX51VJomJOpNAgE.htm](pathfinder-bestiary-items/KLX51VJomJOpNAgE.htm)|Greater Darkvision|auto-trad|
|[kMmbg9uYz80oPVxp.htm](pathfinder-bestiary-items/kMmbg9uYz80oPVxp.htm)|Crossbow Precision|auto-trad|
|[kmUfrNR3jYGHG5co.htm](pathfinder-bestiary-items/kmUfrNR3jYGHG5co.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[kN3B1jnfTS7LsbUS.htm](pathfinder-bestiary-items/kN3B1jnfTS7LsbUS.htm)|Javelin|auto-trad|
|[kN3GBlB8fFVIQvRu.htm](pathfinder-bestiary-items/kN3GBlB8fFVIQvRu.htm)|Falchion|auto-trad|
|[kNOnudI7TvbeSrjS.htm](pathfinder-bestiary-items/kNOnudI7TvbeSrjS.htm)|Fist|auto-trad|
|[KNWnF8Mev9iFqlOl.htm](pathfinder-bestiary-items/KNWnF8Mev9iFqlOl.htm)|Jaws|auto-trad|
|[Ko6ZjYsY5A063K9J.htm](pathfinder-bestiary-items/Ko6ZjYsY5A063K9J.htm)|At-Will Spells|auto-trad|
|[KoI4JtfGIqzDY32e.htm](pathfinder-bestiary-items/KoI4JtfGIqzDY32e.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[koIcuQqgttVU1vlz.htm](pathfinder-bestiary-items/koIcuQqgttVU1vlz.htm)|Jaws|auto-trad|
|[kOpD7rFhEbxHb0Eb.htm](pathfinder-bestiary-items/kOpD7rFhEbxHb0Eb.htm)|Darkvision|auto-trad|
|[KpFmMp8bJz2ZkMQN.htm](pathfinder-bestiary-items/KpFmMp8bJz2ZkMQN.htm)|Darkvision|auto-trad|
|[kPkRH8AsC6BaXLPY.htm](pathfinder-bestiary-items/kPkRH8AsC6BaXLPY.htm)|Claw|auto-trad|
|[Kpl3lLjfHM0hWhT5.htm](pathfinder-bestiary-items/Kpl3lLjfHM0hWhT5.htm)|Final Spite|auto-trad|
|[kppTcx5TylWkpfF1.htm](pathfinder-bestiary-items/kppTcx5TylWkpfF1.htm)|Light Vulnerability|auto-trad|
|[kpuj3LrmPskjllD8.htm](pathfinder-bestiary-items/kpuj3LrmPskjllD8.htm)|Harmonize|auto-trad|
|[kq0boAEZsA01NsEH.htm](pathfinder-bestiary-items/kq0boAEZsA01NsEH.htm)|Powerful Dive|auto-trad|
|[KQARbveR4b7ZZ6yz.htm](pathfinder-bestiary-items/KQARbveR4b7ZZ6yz.htm)|Stunning Shock|auto-trad|
|[KqNNUdMvi8dUvAxl.htm](pathfinder-bestiary-items/KqNNUdMvi8dUvAxl.htm)|At-Will Spells|auto-trad|
|[KQrGNPdR20X4aQo8.htm](pathfinder-bestiary-items/KQrGNPdR20X4aQo8.htm)|Divine Prepared Spells|auto-trad|
|[KR3Cm5a8kO5abrJc.htm](pathfinder-bestiary-items/KR3Cm5a8kO5abrJc.htm)|Draconic Momentum|auto-trad|
|[krB0fRntUZ4twRdd.htm](pathfinder-bestiary-items/krB0fRntUZ4twRdd.htm)|Crumble|auto-trad|
|[krINP0tRSuTbpzys.htm](pathfinder-bestiary-items/krINP0tRSuTbpzys.htm)|Water-Bound|auto-trad|
|[KrjSsDuS9l6Y69eQ.htm](pathfinder-bestiary-items/KrjSsDuS9l6Y69eQ.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[ksFT2wzECyEtNKFL.htm](pathfinder-bestiary-items/ksFT2wzECyEtNKFL.htm)|At-Will Spells|auto-trad|
|[ksGHYkwsnnxc2Vmk.htm](pathfinder-bestiary-items/ksGHYkwsnnxc2Vmk.htm)|Darkvision|auto-trad|
|[kswB083sGau5ZaJ5.htm](pathfinder-bestiary-items/kswB083sGau5ZaJ5.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[Kt4KG0QuJD0Qf4kp.htm](pathfinder-bestiary-items/Kt4KG0QuJD0Qf4kp.htm)|Climb Stone|auto-trad|
|[ktAqGeswWwl10ORn.htm](pathfinder-bestiary-items/ktAqGeswWwl10ORn.htm)|Quick Stow|auto-trad|
|[kTCYz9Dnx9Zw1RHa.htm](pathfinder-bestiary-items/kTCYz9Dnx9Zw1RHa.htm)|Jaws|auto-trad|
|[kUNkiqNpajCyvKVy.htm](pathfinder-bestiary-items/kUNkiqNpajCyvKVy.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[KuURUnbzeAf0EaR1.htm](pathfinder-bestiary-items/KuURUnbzeAf0EaR1.htm)|Catch Rock|auto-trad|
|[kv1mRTVtv9mHLftL.htm](pathfinder-bestiary-items/kv1mRTVtv9mHLftL.htm)|Darkvision|auto-trad|
|[kvbp3xXviiIXnVhS.htm](pathfinder-bestiary-items/kvbp3xXviiIXnVhS.htm)|Predatory Grab|auto-trad|
|[kvPHtl3puURCTaGk.htm](pathfinder-bestiary-items/kvPHtl3puURCTaGk.htm)|Digestive Spew|auto-trad|
|[kwMV2uj1jCdvIm7J.htm](pathfinder-bestiary-items/kwMV2uj1jCdvIm7J.htm)|Beard|auto-trad|
|[KWrM4BkUQ8ldHG0I.htm](pathfinder-bestiary-items/KWrM4BkUQ8ldHG0I.htm)|Eagle Dive|auto-trad|
|[kXAwzMH9r670KlCp.htm](pathfinder-bestiary-items/kXAwzMH9r670KlCp.htm)|Glaive|auto-trad|
|[kXKhHfS3HFvs1QoO.htm](pathfinder-bestiary-items/kXKhHfS3HFvs1QoO.htm)|At-Will Spells|auto-trad|
|[KxNiF9bYl6kA6FRd.htm](pathfinder-bestiary-items/KxNiF9bYl6kA6FRd.htm)|Bellowing Command|auto-trad|
|[KXqIPpRkKZEFXqNo.htm](pathfinder-bestiary-items/KXqIPpRkKZEFXqNo.htm)|Divine Armament|auto-trad|
|[kxRli3zYiks3nckW.htm](pathfinder-bestiary-items/kxRli3zYiks3nckW.htm)|Grab|auto-trad|
|[KYLDzjAZn2ClEtPA.htm](pathfinder-bestiary-items/KYLDzjAZn2ClEtPA.htm)|Divine Innate Spells|auto-trad|
|[KyPmodj8yfI4cI99.htm](pathfinder-bestiary-items/KyPmodj8yfI4cI99.htm)|Devour Soul|auto-trad|
|[KYq91Bu5HDnQoSkG.htm](pathfinder-bestiary-items/KYq91Bu5HDnQoSkG.htm)|Club|auto-trad|
|[KyvMw6gZ8XlbMZI6.htm](pathfinder-bestiary-items/KyvMw6gZ8XlbMZI6.htm)|Quick Bomber|auto-trad|
|[kyw9lRkA8ktftjsi.htm](pathfinder-bestiary-items/kyw9lRkA8ktftjsi.htm)|Arcane Innate Spells|auto-trad|
|[KZ4sC23Z3r4PS4V6.htm](pathfinder-bestiary-items/KZ4sC23Z3r4PS4V6.htm)|Telekinetic Defense|auto-trad|
|[kZEJMwLRJaBvJMSU.htm](pathfinder-bestiary-items/kZEJMwLRJaBvJMSU.htm)|Change Shape|auto-trad|
|[KZgYdjuQuicrfTgT.htm](pathfinder-bestiary-items/KZgYdjuQuicrfTgT.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[KzNMNX33tOGWTxJY.htm](pathfinder-bestiary-items/KzNMNX33tOGWTxJY.htm)|Constrict|auto-trad|
|[kZqLmEhJGdtImTFg.htm](pathfinder-bestiary-items/kZqLmEhJGdtImTFg.htm)|Push or Pull 5 feet|auto-trad|
|[L0FrL7jDYSHuRsdS.htm](pathfinder-bestiary-items/L0FrL7jDYSHuRsdS.htm)|Woodland Stride|auto-trad|
|[L107mBB0Pcsz3T2q.htm](pathfinder-bestiary-items/L107mBB0Pcsz3T2q.htm)|Lurking Death|auto-trad|
|[L1NeMv3ZDcttKLw2.htm](pathfinder-bestiary-items/L1NeMv3ZDcttKLw2.htm)|Snatch|auto-trad|
|[l279FtexxY5UgeM3.htm](pathfinder-bestiary-items/l279FtexxY5UgeM3.htm)|Horn|auto-trad|
|[l2XpWL7iwCvRPkHe.htm](pathfinder-bestiary-items/l2XpWL7iwCvRPkHe.htm)|At-Will Spells|auto-trad|
|[L4dKqPtdmyspmZtd.htm](pathfinder-bestiary-items/L4dKqPtdmyspmZtd.htm)|Jaws|auto-trad|
|[l4eldMZrkZle4K3Y.htm](pathfinder-bestiary-items/l4eldMZrkZle4K3Y.htm)|Horns|auto-trad|
|[L6gROwj7mQVM2d4k.htm](pathfinder-bestiary-items/L6gROwj7mQVM2d4k.htm)|Occult Innate Spells|auto-trad|
|[l6OEi0DzuPlz6OeT.htm](pathfinder-bestiary-items/l6OEi0DzuPlz6OeT.htm)|Wrap in Coils|auto-trad|
|[l6oPZE6Gcn9lJVoU.htm](pathfinder-bestiary-items/l6oPZE6Gcn9lJVoU.htm)|Darkvision|auto-trad|
|[L7wqc9Ph3bPMKfRq.htm](pathfinder-bestiary-items/L7wqc9Ph3bPMKfRq.htm)|Low-Light Vision|auto-trad|
|[L8QPL1ttGhwzsJOz.htm](pathfinder-bestiary-items/L8QPL1ttGhwzsJOz.htm)|Weak Acid|auto-trad|
|[l8txqobaScKbGksB.htm](pathfinder-bestiary-items/l8txqobaScKbGksB.htm)|Constant Spells|auto-trad|
|[L8ZBoNle0ekONgCk.htm](pathfinder-bestiary-items/L8ZBoNle0ekONgCk.htm)|Sphere of Creation|auto-trad|
|[l9FK3RZpzd0GnoPT.htm](pathfinder-bestiary-items/l9FK3RZpzd0GnoPT.htm)|Divine Rituals|auto-trad|
|[L9mTxpzjmfkBOHto.htm](pathfinder-bestiary-items/L9mTxpzjmfkBOHto.htm)|Grab|auto-trad|
|[la6YsHRtpkwWWHV7.htm](pathfinder-bestiary-items/la6YsHRtpkwWWHV7.htm)|Tail|auto-trad|
|[LAE8pFe5hAYvWcTH.htm](pathfinder-bestiary-items/LAE8pFe5hAYvWcTH.htm)|Branch|auto-trad|
|[LAo739thLOTwv571.htm](pathfinder-bestiary-items/LAo739thLOTwv571.htm)|Scent (Imprecise) 100 feet|auto-trad|
|[laPA1RpDHNNksUzO.htm](pathfinder-bestiary-items/laPA1RpDHNNksUzO.htm)|Claw|auto-trad|
|[LaQ2wdVBDXDNbGSR.htm](pathfinder-bestiary-items/LaQ2wdVBDXDNbGSR.htm)|Sneak Attack|auto-trad|
|[LBCwTLpo7BDaFfpf.htm](pathfinder-bestiary-items/LBCwTLpo7BDaFfpf.htm)|+4 Status to All Saves vs. Fear|auto-trad|
|[LbGQE0qrzbORDYH9.htm](pathfinder-bestiary-items/LbGQE0qrzbORDYH9.htm)|Sudden Charge|auto-trad|
|[lBuNOTGbx6zBFceT.htm](pathfinder-bestiary-items/lBuNOTGbx6zBFceT.htm)|Desert Wind|auto-trad|
|[LCFMKpxw0iDxWGXu.htm](pathfinder-bestiary-items/LCFMKpxw0iDxWGXu.htm)|Spore Cloud|auto-trad|
|[LcpFmBryvzaDNDcL.htm](pathfinder-bestiary-items/LcpFmBryvzaDNDcL.htm)|Perfect Aim|auto-trad|
|[lcQfDBQrhLj0Evxf.htm](pathfinder-bestiary-items/lcQfDBQrhLj0Evxf.htm)|At-Will Spells|auto-trad|
|[LD8TK8mGL7ubLU4I.htm](pathfinder-bestiary-items/LD8TK8mGL7ubLU4I.htm)|Stinger|auto-trad|
|[LdDQZazfe2aGupQ2.htm](pathfinder-bestiary-items/LdDQZazfe2aGupQ2.htm)|Constant Spells|auto-trad|
|[LDExFIGLfarrbJQa.htm](pathfinder-bestiary-items/LDExFIGLfarrbJQa.htm)|Speed Surge|auto-trad|
|[LDLJlCLplbi1fPsU.htm](pathfinder-bestiary-items/LDLJlCLplbi1fPsU.htm)|Disperse|auto-trad|
|[LE4LVjvLH5qWmZJo.htm](pathfinder-bestiary-items/LE4LVjvLH5qWmZJo.htm)|Darkvision|auto-trad|
|[lEDcpTL7H8ayv9kT.htm](pathfinder-bestiary-items/lEDcpTL7H8ayv9kT.htm)|Divine Rituals|auto-trad|
|[lenb7f8O728YEnIr.htm](pathfinder-bestiary-items/lenb7f8O728YEnIr.htm)|Grab|auto-trad|
|[LfFpWQJ2gEIy0oiZ.htm](pathfinder-bestiary-items/LfFpWQJ2gEIy0oiZ.htm)|Fist|auto-trad|
|[LfhW8MrjdGdRlQSS.htm](pathfinder-bestiary-items/LfhW8MrjdGdRlQSS.htm)|Smoke Vision|auto-trad|
|[lFKVQJSD5lmRIdIT.htm](pathfinder-bestiary-items/lFKVQJSD5lmRIdIT.htm)|Illusory Retreat|auto-trad|
|[LfvR6qODwUM0OxAB.htm](pathfinder-bestiary-items/LfvR6qODwUM0OxAB.htm)|Occult Innate Spells|auto-trad|
|[LfYdkPPdY3ZvMyEk.htm](pathfinder-bestiary-items/LfYdkPPdY3ZvMyEk.htm)|Grab|auto-trad|
|[lg0ZOYopXDTR0Atj.htm](pathfinder-bestiary-items/lg0ZOYopXDTR0Atj.htm)|Fangs|auto-trad|
|[LG7PBXCgNj0UWIb8.htm](pathfinder-bestiary-items/LG7PBXCgNj0UWIb8.htm)|Root|auto-trad|
|[LGBzL3vv4QNzTFJ1.htm](pathfinder-bestiary-items/LGBzL3vv4QNzTFJ1.htm)|Telepathy 100 feet|auto-trad|
|[LGlJmmg8EvpeOhGW.htm](pathfinder-bestiary-items/LGlJmmg8EvpeOhGW.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[lgo6Chb44AWipQBi.htm](pathfinder-bestiary-items/lgo6Chb44AWipQBi.htm)|Constrict|auto-trad|
|[lhQcYifFNlqvbw4G.htm](pathfinder-bestiary-items/lhQcYifFNlqvbw4G.htm)|At-Will Spells|auto-trad|
|[LIdmD7VqztundfTh.htm](pathfinder-bestiary-items/LIdmD7VqztundfTh.htm)|Beak|auto-trad|
|[lieMSuY1PxRJK0CX.htm](pathfinder-bestiary-items/lieMSuY1PxRJK0CX.htm)|Fist|auto-trad|
|[LiSV4xTOMgWGk32H.htm](pathfinder-bestiary-items/LiSV4xTOMgWGk32H.htm)|Beak|auto-trad|
|[lizgWV0pNr6rV2SG.htm](pathfinder-bestiary-items/lizgWV0pNr6rV2SG.htm)|Tail|auto-trad|
|[Lj4RQQp6b9XUoSUb.htm](pathfinder-bestiary-items/Lj4RQQp6b9XUoSUb.htm)|Wavesense 30 feet|auto-trad|
|[lJGzVTdIOY1p14kj.htm](pathfinder-bestiary-items/lJGzVTdIOY1p14kj.htm)|All-Around Vision|auto-trad|
|[LJKNy957UOv4LqE5.htm](pathfinder-bestiary-items/LJKNy957UOv4LqE5.htm)|Change Shape|auto-trad|
|[lkaHyBENnIei6Qaf.htm](pathfinder-bestiary-items/lkaHyBENnIei6Qaf.htm)|At-Will Spells|auto-trad|
|[LkBIOwt4NW6BfYFE.htm](pathfinder-bestiary-items/LkBIOwt4NW6BfYFE.htm)|Psychic Static Aura|auto-trad|
|[lKC4So6E50bV5RRz.htm](pathfinder-bestiary-items/lKC4So6E50bV5RRz.htm)|Radiant Feathers|auto-trad|
|[Lkl0ZskZuJbD0vlP.htm](pathfinder-bestiary-items/Lkl0ZskZuJbD0vlP.htm)|Arcane Innate Spells|auto-trad|
|[lKQyAi6slANJZp0U.htm](pathfinder-bestiary-items/lKQyAi6slANJZp0U.htm)|Darkvision|auto-trad|
|[LkSajmBAzMTZ4Wk4.htm](pathfinder-bestiary-items/LkSajmBAzMTZ4Wk4.htm)|Beak|auto-trad|
|[lL38DigkATyHvflx.htm](pathfinder-bestiary-items/lL38DigkATyHvflx.htm)|Arcane Innate Spells|auto-trad|
|[lLANYeMvjjIbtdDB.htm](pathfinder-bestiary-items/lLANYeMvjjIbtdDB.htm)|At-Will Spells|auto-trad|
|[LLDqAQlq7w0YWyO0.htm](pathfinder-bestiary-items/LLDqAQlq7w0YWyO0.htm)|Talon|auto-trad|
|[LLFFYMBrnhFoDMRh.htm](pathfinder-bestiary-items/LLFFYMBrnhFoDMRh.htm)|Crumble|auto-trad|
|[LLJiTLsfaDzD1nZo.htm](pathfinder-bestiary-items/LLJiTLsfaDzD1nZo.htm)|Darkvision|auto-trad|
|[lLkkEbmwGunnyE2e.htm](pathfinder-bestiary-items/lLkkEbmwGunnyE2e.htm)|Draconic Frenzy|auto-trad|
|[lLsSuZ6Tev4yEFrU.htm](pathfinder-bestiary-items/lLsSuZ6Tev4yEFrU.htm)|Darkvision|auto-trad|
|[lLwtc5h1JK4M6Nzi.htm](pathfinder-bestiary-items/lLwtc5h1JK4M6Nzi.htm)|Knockdown|auto-trad|
|[LmcmQNgsnrAuSu2l.htm](pathfinder-bestiary-items/LmcmQNgsnrAuSu2l.htm)|Draconic Momentum|auto-trad|
|[LMiVMxKOuhUjGd45.htm](pathfinder-bestiary-items/LMiVMxKOuhUjGd45.htm)|Darkvision|auto-trad|
|[lmmVhyJxHrIHi4Je.htm](pathfinder-bestiary-items/lmmVhyJxHrIHi4Je.htm)|Antenna|auto-trad|
|[lmrBUipZFBXl2NgJ.htm](pathfinder-bestiary-items/lmrBUipZFBXl2NgJ.htm)|Claw|auto-trad|
|[LmRU9VN6HvnRCcJO.htm](pathfinder-bestiary-items/LmRU9VN6HvnRCcJO.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Ln5FLSMEoZNQuxUn.htm](pathfinder-bestiary-items/Ln5FLSMEoZNQuxUn.htm)|Darkvision|auto-trad|
|[LnCX5HAN8VUMv1f5.htm](pathfinder-bestiary-items/LnCX5HAN8VUMv1f5.htm)|Hop-Dodge|auto-trad|
|[LNJkhJSFpg8gvkwp.htm](pathfinder-bestiary-items/LNJkhJSFpg8gvkwp.htm)|Heatsight 60 feet|auto-trad|
|[lnlNJpjHNjw06Wzi.htm](pathfinder-bestiary-items/lnlNJpjHNjw06Wzi.htm)|Tusk|auto-trad|
|[lOGhyEufGuD2T8aX.htm](pathfinder-bestiary-items/lOGhyEufGuD2T8aX.htm)|Jaws|auto-trad|
|[lOUQcokkfOtGgPNG.htm](pathfinder-bestiary-items/lOUQcokkfOtGgPNG.htm)|Jaws|auto-trad|
|[LPgRXjxPg5sOcWEv.htm](pathfinder-bestiary-items/LPgRXjxPg5sOcWEv.htm)|Draconic Frenzy|auto-trad|
|[LpOv4VRyaN38UswN.htm](pathfinder-bestiary-items/LpOv4VRyaN38UswN.htm)|Sneak Attack|auto-trad|
|[lPRNx4hWnBppqbVp.htm](pathfinder-bestiary-items/lPRNx4hWnBppqbVp.htm)|Glaive|auto-trad|
|[lPUoJQI3bTRTV4ct.htm](pathfinder-bestiary-items/lPUoJQI3bTRTV4ct.htm)|Attack of Opportunity|auto-trad|
|[lQHT37uBw4LDc2OY.htm](pathfinder-bestiary-items/lQHT37uBw4LDc2OY.htm)|Inexorable March|auto-trad|
|[lQKrggSbzpvqq4YB.htm](pathfinder-bestiary-items/lQKrggSbzpvqq4YB.htm)|Inspiration|auto-trad|
|[LqM7JU0cOL7itoch.htm](pathfinder-bestiary-items/LqM7JU0cOL7itoch.htm)|Change Shape|auto-trad|
|[lqMgsu5wZAOsEwK2.htm](pathfinder-bestiary-items/lqMgsu5wZAOsEwK2.htm)|Giant Viper Venom|auto-trad|
|[lQO6RbVplN7WWnna.htm](pathfinder-bestiary-items/lQO6RbVplN7WWnna.htm)|Constant Spells|auto-trad|
|[LQPUOdcFtZk2FzJz.htm](pathfinder-bestiary-items/LQPUOdcFtZk2FzJz.htm)|Jaws|auto-trad|
|[lqWh4YqCJffKqcoX.htm](pathfinder-bestiary-items/lqWh4YqCJffKqcoX.htm)|Rapier|auto-trad|
|[lRaDhQKMgeR6IjAB.htm](pathfinder-bestiary-items/lRaDhQKMgeR6IjAB.htm)|Low-Light Vision|auto-trad|
|[LRAh1zPRfZ1jQ8a6.htm](pathfinder-bestiary-items/LRAh1zPRfZ1jQ8a6.htm)|Composite Shortbow|auto-trad|
|[LRoHaUgN8RugcqOi.htm](pathfinder-bestiary-items/LRoHaUgN8RugcqOi.htm)|Darkvision|auto-trad|
|[lrqS9oUNMEepLga2.htm](pathfinder-bestiary-items/lrqS9oUNMEepLga2.htm)|Divine Innate Spells|auto-trad|
|[LRT2YJWR1S18u4vp.htm](pathfinder-bestiary-items/LRT2YJWR1S18u4vp.htm)|Shortsword|auto-trad|
|[LRVsZzLZZ4VCKy1h.htm](pathfinder-bestiary-items/LRVsZzLZZ4VCKy1h.htm)|Darkvision|auto-trad|
|[LRxNywKFwiFUofE7.htm](pathfinder-bestiary-items/LRxNywKFwiFUofE7.htm)|Darkvision|auto-trad|
|[lRYUK2abQpyhIlbv.htm](pathfinder-bestiary-items/lRYUK2abQpyhIlbv.htm)|Darkvision|auto-trad|
|[lszUCBGYOxZzhPEU.htm](pathfinder-bestiary-items/lszUCBGYOxZzhPEU.htm)|Tremorsense (Imprecise) 90 feet|auto-trad|
|[Lt1Gndh1LAlY6Q2u.htm](pathfinder-bestiary-items/Lt1Gndh1LAlY6Q2u.htm)|Greataxe|auto-trad|
|[lT2fWWJ081ynQOOQ.htm](pathfinder-bestiary-items/lT2fWWJ081ynQOOQ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Lt7N9rIlyS76DtRj.htm](pathfinder-bestiary-items/Lt7N9rIlyS76DtRj.htm)|Tail|auto-trad|
|[lTAUfkDQ3VVnBakO.htm](pathfinder-bestiary-items/lTAUfkDQ3VVnBakO.htm)|Frightful Presence|auto-trad|
|[ltUht9L2rQGxrSt7.htm](pathfinder-bestiary-items/ltUht9L2rQGxrSt7.htm)|Divine Prepared Spells|auto-trad|
|[ltvMN7K0itu2l3WZ.htm](pathfinder-bestiary-items/ltvMN7K0itu2l3WZ.htm)|Snake Fangs|auto-trad|
|[LTwSnVSVWZeWrLAC.htm](pathfinder-bestiary-items/LTwSnVSVWZeWrLAC.htm)|Swarming Bites|auto-trad|
|[LUK2Wptf0GJyjbxC.htm](pathfinder-bestiary-items/LUK2Wptf0GJyjbxC.htm)|Staff|auto-trad|
|[LUPkzJMY0W8x0Vsd.htm](pathfinder-bestiary-items/LUPkzJMY0W8x0Vsd.htm)|Fist|auto-trad|
|[lUtqPs1qhhuLgmEE.htm](pathfinder-bestiary-items/lUtqPs1qhhuLgmEE.htm)|Attack of Opportunity|auto-trad|
|[luxLK4RgZT83aYTa.htm](pathfinder-bestiary-items/luxLK4RgZT83aYTa.htm)|Darkvision|auto-trad|
|[LuxUX599os0xTuFk.htm](pathfinder-bestiary-items/LuxUX599os0xTuFk.htm)|Mind Crush|auto-trad|
|[luyr4NvU4euUL7Dh.htm](pathfinder-bestiary-items/luyr4NvU4euUL7Dh.htm)|Darkvision|auto-trad|
|[LUZas9EK4qLmUsox.htm](pathfinder-bestiary-items/LUZas9EK4qLmUsox.htm)|Slowing Frost|auto-trad|
|[lV6uRtAWXQ1eYrO3.htm](pathfinder-bestiary-items/lV6uRtAWXQ1eYrO3.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[lvw0wlyhNfMvvQ6Q.htm](pathfinder-bestiary-items/lvw0wlyhNfMvvQ6Q.htm)|Darkvision|auto-trad|
|[LwPsr1H56GuAEnbm.htm](pathfinder-bestiary-items/LwPsr1H56GuAEnbm.htm)|Jaws|auto-trad|
|[LwT17u6xCyB2oygz.htm](pathfinder-bestiary-items/LwT17u6xCyB2oygz.htm)|At-Will Spells|auto-trad|
|[lwUnUfQ1EWHSs1xW.htm](pathfinder-bestiary-items/lwUnUfQ1EWHSs1xW.htm)|Gust|auto-trad|
|[LxhUmOCBKZi7FNfa.htm](pathfinder-bestiary-items/LxhUmOCBKZi7FNfa.htm)|Betraying Touch|auto-trad|
|[LxnGNDFd9yCEkOAt.htm](pathfinder-bestiary-items/LxnGNDFd9yCEkOAt.htm)|Pounce|auto-trad|
|[lxNScLMLEw8NbtpU.htm](pathfinder-bestiary-items/lxNScLMLEw8NbtpU.htm)|Knockdown (Wolf Form)|auto-trad|
|[lYeYZZa66BG9v3KY.htm](pathfinder-bestiary-items/lYeYZZa66BG9v3KY.htm)|Spectral Hand|auto-trad|
|[lYpvPmdtgwwrvYGd.htm](pathfinder-bestiary-items/lYpvPmdtgwwrvYGd.htm)|Tail|auto-trad|
|[lYUnEWGhnlYFvH6B.htm](pathfinder-bestiary-items/lYUnEWGhnlYFvH6B.htm)|Swallow Whole|auto-trad|
|[LZ3htI7JRHvcwAhU.htm](pathfinder-bestiary-items/LZ3htI7JRHvcwAhU.htm)|Double Opportunity|auto-trad|
|[LzMPMpH1MlWA2nAt.htm](pathfinder-bestiary-items/LzMPMpH1MlWA2nAt.htm)|Low-Light Vision|auto-trad|
|[lzmS1xCoWnHVa39l.htm](pathfinder-bestiary-items/lzmS1xCoWnHVa39l.htm)|Darkvision|auto-trad|
|[M05pu8jIHs0qEKzP.htm](pathfinder-bestiary-items/M05pu8jIHs0qEKzP.htm)|Hand|auto-trad|
|[M0BuH1pWhtWsVIYm.htm](pathfinder-bestiary-items/M0BuH1pWhtWsVIYm.htm)|Worry|auto-trad|
|[m0piXEdTBGvW1oIY.htm](pathfinder-bestiary-items/m0piXEdTBGvW1oIY.htm)|Push 10 feet|auto-trad|
|[M1Iy4WjF2HRmGv0e.htm](pathfinder-bestiary-items/M1Iy4WjF2HRmGv0e.htm)|Telepathy 100 feet|auto-trad|
|[M1Uh0E4VLZObTo3I.htm](pathfinder-bestiary-items/M1Uh0E4VLZObTo3I.htm)|Buck|auto-trad|
|[M203mlcBNFvAqyIJ.htm](pathfinder-bestiary-items/M203mlcBNFvAqyIJ.htm)|Burn Alive|auto-trad|
|[M22baNr3EaRn3JZr.htm](pathfinder-bestiary-items/M22baNr3EaRn3JZr.htm)|Arcane Spontaneous Spells|auto-trad|
|[m25t326n1Ovt0t5U.htm](pathfinder-bestiary-items/m25t326n1Ovt0t5U.htm)|Negative Healing|auto-trad|
|[m2aEAnMnepSKN57G.htm](pathfinder-bestiary-items/m2aEAnMnepSKN57G.htm)|Head Regrowth|auto-trad|
|[M2JcdJZTsdKY31Ed.htm](pathfinder-bestiary-items/M2JcdJZTsdKY31Ed.htm)|Constant Spells|auto-trad|
|[m2lc6stQx0fMmqPO.htm](pathfinder-bestiary-items/m2lc6stQx0fMmqPO.htm)|Knockdown|auto-trad|
|[M2ncaZFg1bVL9O6X.htm](pathfinder-bestiary-items/M2ncaZFg1bVL9O6X.htm)|Tail|auto-trad|
|[M2XLTTlSc1yU99ym.htm](pathfinder-bestiary-items/M2XLTTlSc1yU99ym.htm)|Darkvision|auto-trad|
|[m3iQuuvCSejqA1Ue.htm](pathfinder-bestiary-items/m3iQuuvCSejqA1Ue.htm)|Grab|auto-trad|
|[M3QwWIU2rzRHWndo.htm](pathfinder-bestiary-items/M3QwWIU2rzRHWndo.htm)|Mimic Object|auto-trad|
|[M4f06NOUIcSyQNr2.htm](pathfinder-bestiary-items/M4f06NOUIcSyQNr2.htm)|Regeneration 30 (Deactivated by Acid or Fire)|auto-trad|
|[m53HcXgUGgipalh0.htm](pathfinder-bestiary-items/m53HcXgUGgipalh0.htm)|Moon Frenzy|auto-trad|
|[m5FtOPWhFYV34o9q.htm](pathfinder-bestiary-items/m5FtOPWhFYV34o9q.htm)|Tail|auto-trad|
|[m5n5I6Z3Upfe9pSe.htm](pathfinder-bestiary-items/m5n5I6Z3Upfe9pSe.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[m6CbK52GoU471gez.htm](pathfinder-bestiary-items/m6CbK52GoU471gez.htm)|Drain Soul Cage|auto-trad|
|[m6E1gSnAhXp72yBC.htm](pathfinder-bestiary-items/m6E1gSnAhXp72yBC.htm)|Low-Light Vision|auto-trad|
|[M6l0kUINWUBkdXJ1.htm](pathfinder-bestiary-items/M6l0kUINWUBkdXJ1.htm)|Constant Spells|auto-trad|
|[M6LXusA9trhjoaTp.htm](pathfinder-bestiary-items/M6LXusA9trhjoaTp.htm)|Breath Weapon|auto-trad|
|[M6OcH3aIVdTWr7gq.htm](pathfinder-bestiary-items/M6OcH3aIVdTWr7gq.htm)|Tail|auto-trad|
|[m6WhfbsOdnetGUAE.htm](pathfinder-bestiary-items/m6WhfbsOdnetGUAE.htm)|Camouflage|auto-trad|
|[M6zs0ZmCJlyphS5N.htm](pathfinder-bestiary-items/M6zs0ZmCJlyphS5N.htm)|Fangs|auto-trad|
|[M7mGcRgzVr9C3wsW.htm](pathfinder-bestiary-items/M7mGcRgzVr9C3wsW.htm)|Telekinetic Assault|auto-trad|
|[M7sBCYj1yfPikRDu.htm](pathfinder-bestiary-items/M7sBCYj1yfPikRDu.htm)|Grab|auto-trad|
|[M8RNAgw0OgnUOpKP.htm](pathfinder-bestiary-items/M8RNAgw0OgnUOpKP.htm)|Polearm Critical Specialization|auto-trad|
|[M99t6H3so0fp0wTH.htm](pathfinder-bestiary-items/M99t6H3so0fp0wTH.htm)|Darkvision|auto-trad|
|[m9xKqsUED5sH6vqA.htm](pathfinder-bestiary-items/m9xKqsUED5sH6vqA.htm)|Change Shape|auto-trad|
|[mAzjVKaBRjpMwszN.htm](pathfinder-bestiary-items/mAzjVKaBRjpMwszN.htm)|Telepathy (Touch)|auto-trad|
|[mB1mKRbvNa6edeUB.htm](pathfinder-bestiary-items/mB1mKRbvNa6edeUB.htm)|Sandblast|auto-trad|
|[Mb8JbA884rcKgMkO.htm](pathfinder-bestiary-items/Mb8JbA884rcKgMkO.htm)|Telepathy 100 feet|auto-trad|
|[mB9tYATe35GSN6Tm.htm](pathfinder-bestiary-items/mB9tYATe35GSN6Tm.htm)|Starlight Blast|auto-trad|
|[mbDpJL0NCrFdM2bb.htm](pathfinder-bestiary-items/mbDpJL0NCrFdM2bb.htm)|Explosion|auto-trad|
|[MBIM9DzCh32Rrta8.htm](pathfinder-bestiary-items/MBIM9DzCh32Rrta8.htm)|Darkvision|auto-trad|
|[MbjqqpFVm0Xwv00X.htm](pathfinder-bestiary-items/MbjqqpFVm0Xwv00X.htm)|Tremorsense (Imprecise) 100 feet|auto-trad|
|[mbkNd6gFHEqUiVeO.htm](pathfinder-bestiary-items/mbkNd6gFHEqUiVeO.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[mbrIsofRTRy9BMDp.htm](pathfinder-bestiary-items/mbrIsofRTRy9BMDp.htm)|Fist|auto-trad|
|[MCe44ome2cf0lQ8W.htm](pathfinder-bestiary-items/MCe44ome2cf0lQ8W.htm)|Pull Apart|auto-trad|
|[MCgl5uKpwtTh48E4.htm](pathfinder-bestiary-items/MCgl5uKpwtTh48E4.htm)|Glaive|auto-trad|
|[Md6z01uI0caPW7Vl.htm](pathfinder-bestiary-items/Md6z01uI0caPW7Vl.htm)|Draconic Frenzy|auto-trad|
|[mDnVfeIEWGgfP7Xq.htm](pathfinder-bestiary-items/mDnVfeIEWGgfP7Xq.htm)|Repository of Lore|auto-trad|
|[mdqK7RuSjtezVyS8.htm](pathfinder-bestiary-items/mdqK7RuSjtezVyS8.htm)|Filth Wave|auto-trad|
|[mE1ez2SoxG8F6mVq.htm](pathfinder-bestiary-items/mE1ez2SoxG8F6mVq.htm)|Darkvision|auto-trad|
|[mE1G6otJjruqyHkD.htm](pathfinder-bestiary-items/mE1G6otJjruqyHkD.htm)|At-Will Spells|auto-trad|
|[MEAPfylvHHHbTqOf.htm](pathfinder-bestiary-items/MEAPfylvHHHbTqOf.htm)|Tongue Grab|auto-trad|
|[MEjtnkYdPhtWGyzF.htm](pathfinder-bestiary-items/MEjtnkYdPhtWGyzF.htm)|Coiled Opportunity (Special)|auto-trad|
|[MEMnTHEnlQDgPkfq.htm](pathfinder-bestiary-items/MEMnTHEnlQDgPkfq.htm)|Arcane Spontaneous Spells|auto-trad|
|[mf1G3pa8hN8LNJ4n.htm](pathfinder-bestiary-items/mf1G3pa8hN8LNJ4n.htm)|Leaf|auto-trad|
|[MF3wGJmHdgsg1PY3.htm](pathfinder-bestiary-items/MF3wGJmHdgsg1PY3.htm)|Beak|auto-trad|
|[Mf6oyfENXSeMgM7o.htm](pathfinder-bestiary-items/Mf6oyfENXSeMgM7o.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Mf7Y2TxYr2e5o2Ht.htm](pathfinder-bestiary-items/Mf7Y2TxYr2e5o2Ht.htm)|Buck|auto-trad|
|[MfBsuZaAMldux4Xb.htm](pathfinder-bestiary-items/MfBsuZaAMldux4Xb.htm)|Javelin|auto-trad|
|[mFf9kwHNM5V0Un7y.htm](pathfinder-bestiary-items/mFf9kwHNM5V0Un7y.htm)|Divine Prepared Spells|auto-trad|
|[mfPiHF5tCFHLTxDa.htm](pathfinder-bestiary-items/mfPiHF5tCFHLTxDa.htm)|Arcane Innate Spells|auto-trad|
|[MFz1cM1MqbifkyHH.htm](pathfinder-bestiary-items/MFz1cM1MqbifkyHH.htm)|Vulnerable to Smoke|auto-trad|
|[mgb2nO0WkJsnZogZ.htm](pathfinder-bestiary-items/mgb2nO0WkJsnZogZ.htm)|Lava Affinity|auto-trad|
|[mGL7tbh4lfdWIVfJ.htm](pathfinder-bestiary-items/mGL7tbh4lfdWIVfJ.htm)|Hidden Movement|auto-trad|
|[MGX1U8Kv59BVYAOI.htm](pathfinder-bestiary-items/MGX1U8Kv59BVYAOI.htm)|Push or Pull 10 feet|auto-trad|
|[mh9LAEOA50HTVL1T.htm](pathfinder-bestiary-items/mh9LAEOA50HTVL1T.htm)|Jaws|auto-trad|
|[mhBBm02GHgEZ30NR.htm](pathfinder-bestiary-items/mhBBm02GHgEZ30NR.htm)|Web Trap|auto-trad|
|[MHobLN5Z39cpkjWr.htm](pathfinder-bestiary-items/MHobLN5Z39cpkjWr.htm)|Darkvision|auto-trad|
|[MhpbBKngUDFRNsQF.htm](pathfinder-bestiary-items/MhpbBKngUDFRNsQF.htm)|Darkvision|auto-trad|
|[mHS7Uj0GKGKhQiNo.htm](pathfinder-bestiary-items/mHS7Uj0GKGKhQiNo.htm)|Orc Necksplitter|auto-trad|
|[MJ00yNypQKm6JLmB.htm](pathfinder-bestiary-items/MJ00yNypQKm6JLmB.htm)|Claw|auto-trad|
|[mJ4kL0oQk4xWhXtX.htm](pathfinder-bestiary-items/mJ4kL0oQk4xWhXtX.htm)|Darkvision|auto-trad|
|[Mj7eIolH2KmKRos4.htm](pathfinder-bestiary-items/Mj7eIolH2KmKRos4.htm)|Wyvern Venom|auto-trad|
|[mjClQEAEbQfY2Zfr.htm](pathfinder-bestiary-items/mjClQEAEbQfY2Zfr.htm)|Adamantine Claw|auto-trad|
|[Mjlek9OZ25Tga7Cg.htm](pathfinder-bestiary-items/Mjlek9OZ25Tga7Cg.htm)|Filth Fever|auto-trad|
|[MjvAem9y7U9Z9AnV.htm](pathfinder-bestiary-items/MjvAem9y7U9Z9AnV.htm)|Sneak Attack|auto-trad|
|[MJVKsTpwu0QDO6t7.htm](pathfinder-bestiary-items/MJVKsTpwu0QDO6t7.htm)|At-Will Spells|auto-trad|
|[mK3REH0Jcz0NP7Bh.htm](pathfinder-bestiary-items/mK3REH0Jcz0NP7Bh.htm)|Vampire Weaknesses|auto-trad|
|[mKaR7LWvVhRIbJCG.htm](pathfinder-bestiary-items/mKaR7LWvVhRIbJCG.htm)|Occult Innate Spells|auto-trad|
|[mkbavR53jwVXw293.htm](pathfinder-bestiary-items/mkbavR53jwVXw293.htm)|Claw|auto-trad|
|[mKMByZgGjo3KcHwj.htm](pathfinder-bestiary-items/mKMByZgGjo3KcHwj.htm)|Jaws|auto-trad|
|[MKqjvJ1d8GxOUuDF.htm](pathfinder-bestiary-items/MKqjvJ1d8GxOUuDF.htm)|Tail|auto-trad|
|[mkwN7Fgq3DRnThOV.htm](pathfinder-bestiary-items/mkwN7Fgq3DRnThOV.htm)|Consume Flesh|auto-trad|
|[mld7PgekazbX4cCh.htm](pathfinder-bestiary-items/mld7PgekazbX4cCh.htm)|Glaive|auto-trad|
|[MlLvVLQolivhuZFD.htm](pathfinder-bestiary-items/MlLvVLQolivhuZFD.htm)|Draconic Frenzy|auto-trad|
|[Mn9ThITOCOXSNkEv.htm](pathfinder-bestiary-items/Mn9ThITOCOXSNkEv.htm)|Regeneration 20 (Deactivated by Chaotic)|auto-trad|
|[MNeTs8gNujcsFNkv.htm](pathfinder-bestiary-items/MNeTs8gNujcsFNkv.htm)|Darkvision|auto-trad|
|[mNfAyKfUwSuMResA.htm](pathfinder-bestiary-items/mNfAyKfUwSuMResA.htm)|Low-Light Vision|auto-trad|
|[mnmPiXMTDMbeqRnI.htm](pathfinder-bestiary-items/mnmPiXMTDMbeqRnI.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[mNomS6NBwvlheidl.htm](pathfinder-bestiary-items/mNomS6NBwvlheidl.htm)|Cube Face|auto-trad|
|[MNTsZcEEgZLgHBMp.htm](pathfinder-bestiary-items/MNTsZcEEgZLgHBMp.htm)|Constrict|auto-trad|
|[mo2rgNZQlmzNFBze.htm](pathfinder-bestiary-items/mo2rgNZQlmzNFBze.htm)|Fist|auto-trad|
|[Mo3NNcPkDUwnRiIF.htm](pathfinder-bestiary-items/Mo3NNcPkDUwnRiIF.htm)|Leg|auto-trad|
|[MO9W8NpBALoHHtTS.htm](pathfinder-bestiary-items/MO9W8NpBALoHHtTS.htm)|Sneak Attack|auto-trad|
|[MOmmOyY0sUO8EEIX.htm](pathfinder-bestiary-items/MOmmOyY0sUO8EEIX.htm)|Attack of Opportunity|auto-trad|
|[momzHh7zAZRC7Hob.htm](pathfinder-bestiary-items/momzHh7zAZRC7Hob.htm)|Swamp Stride|auto-trad|
|[MPbClVjJK4l020IS.htm](pathfinder-bestiary-items/MPbClVjJK4l020IS.htm)|Low-Light Vision|auto-trad|
|[MPJDgS7lWLTOqNeT.htm](pathfinder-bestiary-items/MPJDgS7lWLTOqNeT.htm)|Darkvision|auto-trad|
|[MpNlJrdPvv9bECNE.htm](pathfinder-bestiary-items/MpNlJrdPvv9bECNE.htm)|Sinful Bite|auto-trad|
|[MQbK2a6aQPbnYNCw.htm](pathfinder-bestiary-items/MQbK2a6aQPbnYNCw.htm)|Negative Healing|auto-trad|
|[mqhNvxuxfh9K2nAg.htm](pathfinder-bestiary-items/mqhNvxuxfh9K2nAg.htm)|Low-Light Vision|auto-trad|
|[mrCcKL7iLIAFrAys.htm](pathfinder-bestiary-items/mrCcKL7iLIAFrAys.htm)|Low-Light Vision|auto-trad|
|[MrCFRuiA29CCYxgb.htm](pathfinder-bestiary-items/MrCFRuiA29CCYxgb.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[mRElwzY41P9cR0SN.htm](pathfinder-bestiary-items/mRElwzY41P9cR0SN.htm)|Jaws|auto-trad|
|[MRMj6jc7E8e5c7UF.htm](pathfinder-bestiary-items/MRMj6jc7E8e5c7UF.htm)|Grab|auto-trad|
|[mrrLoMpl5WnNGk4e.htm](pathfinder-bestiary-items/mrrLoMpl5WnNGk4e.htm)|Earthbound|auto-trad|
|[MrSMiZ1i5groiYX1.htm](pathfinder-bestiary-items/MrSMiZ1i5groiYX1.htm)|Regeneration 15 (Deactivated by Cold Iron)|auto-trad|
|[mS16mfAS5hbK048v.htm](pathfinder-bestiary-items/mS16mfAS5hbK048v.htm)|Divine Innate Spells|auto-trad|
|[mS77pmvBFQBdJrrS.htm](pathfinder-bestiary-items/mS77pmvBFQBdJrrS.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[MScqq1IasW1Ei33T.htm](pathfinder-bestiary-items/MScqq1IasW1Ei33T.htm)|Mandibles|auto-trad|
|[MsksF4NK5nEBIyq6.htm](pathfinder-bestiary-items/MsksF4NK5nEBIyq6.htm)|Tail|auto-trad|
|[Msmv4qRjU8FEJbcm.htm](pathfinder-bestiary-items/Msmv4qRjU8FEJbcm.htm)|Divine Innate Spells|auto-trad|
|[msrruiotI1gRnWN9.htm](pathfinder-bestiary-items/msrruiotI1gRnWN9.htm)|Darkvision|auto-trad|
|[MTJ6P2pU1320fO7f.htm](pathfinder-bestiary-items/MTJ6P2pU1320fO7f.htm)|Berserk|auto-trad|
|[mU73p7EYpTXgNAqN.htm](pathfinder-bestiary-items/mU73p7EYpTXgNAqN.htm)|Divine Innate Spells|auto-trad|
|[MuiyXm8KyQF57NaW.htm](pathfinder-bestiary-items/MuiyXm8KyQF57NaW.htm)|Jaws|auto-trad|
|[muQiaR4uAaAcFhJ0.htm](pathfinder-bestiary-items/muQiaR4uAaAcFhJ0.htm)|Darkvision|auto-trad|
|[MUtrSZzTqwA22XOa.htm](pathfinder-bestiary-items/MUtrSZzTqwA22XOa.htm)|Darkvision|auto-trad|
|[MVhuX4yQFrWGRGUi.htm](pathfinder-bestiary-items/MVhuX4yQFrWGRGUi.htm)|Telepathy 100 feet|auto-trad|
|[MVKA8eKi1YTMeHKK.htm](pathfinder-bestiary-items/MVKA8eKi1YTMeHKK.htm)|Powerful Charge|auto-trad|
|[mvKy4fox7C9GJNq4.htm](pathfinder-bestiary-items/mvKy4fox7C9GJNq4.htm)|Collect Brain|auto-trad|
|[mvM0UYDSareH0kWj.htm](pathfinder-bestiary-items/mvM0UYDSareH0kWj.htm)|Aklys|auto-trad|
|[MVOUIn7qGVbFyHG8.htm](pathfinder-bestiary-items/MVOUIn7qGVbFyHG8.htm)|Low-Light Vision|auto-trad|
|[MVS8VVm9BG19yQpr.htm](pathfinder-bestiary-items/MVS8VVm9BG19yQpr.htm)|Cheek Pouches|auto-trad|
|[mw30E878JJp7tVVh.htm](pathfinder-bestiary-items/mw30E878JJp7tVVh.htm)|Tongue|auto-trad|
|[MWKHdXGNx9JkkNqR.htm](pathfinder-bestiary-items/MWKHdXGNx9JkkNqR.htm)|Envisioning|auto-trad|
|[MWmyo46mgKFCREnk.htm](pathfinder-bestiary-items/MWmyo46mgKFCREnk.htm)|Claw|auto-trad|
|[MwNIK1P57HlN0q90.htm](pathfinder-bestiary-items/MwNIK1P57HlN0q90.htm)|Improved Grab|auto-trad|
|[MwXQ4IgNkQaCxTcf.htm](pathfinder-bestiary-items/MwXQ4IgNkQaCxTcf.htm)|Motion Sense 60 feet|auto-trad|
|[mxb42HlUBxVyw38F.htm](pathfinder-bestiary-items/mxb42HlUBxVyw38F.htm)|Rend|auto-trad|
|[MxiNEcQrKuXmCuWD.htm](pathfinder-bestiary-items/MxiNEcQrKuXmCuWD.htm)|Fangs|auto-trad|
|[My4dOJkuaFfSHA5r.htm](pathfinder-bestiary-items/My4dOJkuaFfSHA5r.htm)|Grab|auto-trad|
|[MY6hTtpdYnUlbipf.htm](pathfinder-bestiary-items/MY6hTtpdYnUlbipf.htm)|Jaws|auto-trad|
|[myHidV8LBPQYfgGg.htm](pathfinder-bestiary-items/myHidV8LBPQYfgGg.htm)|Constant Spells|auto-trad|
|[mYNDkS27YQRyy1F0.htm](pathfinder-bestiary-items/mYNDkS27YQRyy1F0.htm)|Tail|auto-trad|
|[myP1KQdadPQMg8sJ.htm](pathfinder-bestiary-items/myP1KQdadPQMg8sJ.htm)|Primal Innate Spells|auto-trad|
|[mZDSdkhKHanRDmIg.htm](pathfinder-bestiary-items/mZDSdkhKHanRDmIg.htm)|Low-Light Vision|auto-trad|
|[n06PGBhrUKKUVnsV.htm](pathfinder-bestiary-items/n06PGBhrUKKUVnsV.htm)|Knockdown|auto-trad|
|[N0g5xdIF3LC3JDxP.htm](pathfinder-bestiary-items/N0g5xdIF3LC3JDxP.htm)|Alchemical Rupture|auto-trad|
|[n1ci9JWzjxR4IICp.htm](pathfinder-bestiary-items/n1ci9JWzjxR4IICp.htm)|Headbutt|auto-trad|
|[n1wsbA3ExF7FB1bG.htm](pathfinder-bestiary-items/n1wsbA3ExF7FB1bG.htm)|Occult Innate Spells|auto-trad|
|[n2BHwPEdFYp07VvG.htm](pathfinder-bestiary-items/n2BHwPEdFYp07VvG.htm)|Pseudopod|auto-trad|
|[n4Cc3VF0F86FO0sG.htm](pathfinder-bestiary-items/n4Cc3VF0F86FO0sG.htm)|Cold Adaptation|auto-trad|
|[n4ny8F5wuAdt0WPM.htm](pathfinder-bestiary-items/n4ny8F5wuAdt0WPM.htm)|Divine Prepared Spells|auto-trad|
|[N5NccQ5JtdXMWjA9.htm](pathfinder-bestiary-items/N5NccQ5JtdXMWjA9.htm)|Engulf|auto-trad|
|[n7k0vUN2EEQBVEku.htm](pathfinder-bestiary-items/n7k0vUN2EEQBVEku.htm)|Frightful Presence|auto-trad|
|[n8B8DCAuDAVcZqQx.htm](pathfinder-bestiary-items/n8B8DCAuDAVcZqQx.htm)|Frightful Presence|auto-trad|
|[N9vcXhHw71tS2MZS.htm](pathfinder-bestiary-items/N9vcXhHw71tS2MZS.htm)|Darkvision|auto-trad|
|[nA5lgWKn3AryAuFs.htm](pathfinder-bestiary-items/nA5lgWKn3AryAuFs.htm)|Twisted Desires|auto-trad|
|[NaDfoZlbd0uAIh3t.htm](pathfinder-bestiary-items/NaDfoZlbd0uAIh3t.htm)|Trunk|auto-trad|
|[naotn6A029XPnJc1.htm](pathfinder-bestiary-items/naotn6A029XPnJc1.htm)|Mauler|auto-trad|
|[nbClUIeaboa77zlN.htm](pathfinder-bestiary-items/nbClUIeaboa77zlN.htm)|Swallow Whole|auto-trad|
|[nbUmQWTABz3t1MCW.htm](pathfinder-bestiary-items/nbUmQWTABz3t1MCW.htm)|Wing Deflection|auto-trad|
|[nbVDArIMsO5ljcOd.htm](pathfinder-bestiary-items/nbVDArIMsO5ljcOd.htm)|Claw|auto-trad|
|[NBwQlH8AscMgK3TC.htm](pathfinder-bestiary-items/NBwQlH8AscMgK3TC.htm)|Scimitar|auto-trad|
|[nbWXPfh7WyVCERN9.htm](pathfinder-bestiary-items/nbWXPfh7WyVCERN9.htm)|Smoke|auto-trad|
|[NBXY2jJ5QKUZlI60.htm](pathfinder-bestiary-items/NBXY2jJ5QKUZlI60.htm)|Rock Tunneler|auto-trad|
|[NC141aBluvAWkdH5.htm](pathfinder-bestiary-items/NC141aBluvAWkdH5.htm)|Grab|auto-trad|
|[NCdnViWXztATwZA5.htm](pathfinder-bestiary-items/NCdnViWXztATwZA5.htm)|Fog Vision|auto-trad|
|[nCvEdtYwtDcpBscl.htm](pathfinder-bestiary-items/nCvEdtYwtDcpBscl.htm)|Immunity to Magic|auto-trad|
|[NcZMn3pf8nbMeY8x.htm](pathfinder-bestiary-items/NcZMn3pf8nbMeY8x.htm)|Darkvision|auto-trad|
|[NDdkhcyKoaZMjFh6.htm](pathfinder-bestiary-items/NDdkhcyKoaZMjFh6.htm)|Constrict|auto-trad|
|[NDm80a7dNC5LKMIS.htm](pathfinder-bestiary-items/NDm80a7dNC5LKMIS.htm)|Low-Light Vision|auto-trad|
|[nE61lAuAYvElBffl.htm](pathfinder-bestiary-items/nE61lAuAYvElBffl.htm)|Divine Innate Spells|auto-trad|
|[NecBxUwfSaIlDEmH.htm](pathfinder-bestiary-items/NecBxUwfSaIlDEmH.htm)|Divine Rituals|auto-trad|
|[NeJvQpMPGMIfzdCG.htm](pathfinder-bestiary-items/NeJvQpMPGMIfzdCG.htm)|Tighten Coils|auto-trad|
|[NFeHIUnX3NMyZX5I.htm](pathfinder-bestiary-items/NFeHIUnX3NMyZX5I.htm)|Fangs|auto-trad|
|[NFnbEaaMOHmME3CN.htm](pathfinder-bestiary-items/NFnbEaaMOHmME3CN.htm)|Divine Innate Spells|auto-trad|
|[NFPV2IK6zYWCTJbC.htm](pathfinder-bestiary-items/NFPV2IK6zYWCTJbC.htm)|Passionate Kiss|auto-trad|
|[NfSCCIAzcvdGsuLs.htm](pathfinder-bestiary-items/NfSCCIAzcvdGsuLs.htm)|Keepsake|auto-trad|
|[nfw4zm2GbHBD2wum.htm](pathfinder-bestiary-items/nfw4zm2GbHBD2wum.htm)|Low-Light Vision|auto-trad|
|[NGiITXvvb7TI4B4y.htm](pathfinder-bestiary-items/NGiITXvvb7TI4B4y.htm)|At-Will Spells|auto-trad|
|[nGzfYxHKFp5jqc76.htm](pathfinder-bestiary-items/nGzfYxHKFp5jqc76.htm)|Smoke Vision|auto-trad|
|[nhAXBxeC21S71BiM.htm](pathfinder-bestiary-items/nhAXBxeC21S71BiM.htm)|Arcane Prepared Spells|auto-trad|
|[nHfBhYOOHdy8Wx7p.htm](pathfinder-bestiary-items/nHfBhYOOHdy8Wx7p.htm)|Whirlwind Form|auto-trad|
|[NhKBmdf3sh4wyA1u.htm](pathfinder-bestiary-items/NhKBmdf3sh4wyA1u.htm)|Trample|auto-trad|
|[NHmjLWLocmpqvoi9.htm](pathfinder-bestiary-items/NHmjLWLocmpqvoi9.htm)|Tail|auto-trad|
|[NHSqYE5ZeWiln3Vx.htm](pathfinder-bestiary-items/NHSqYE5ZeWiln3Vx.htm)|Hoof|auto-trad|
|[nHYtHMopSyGQg3WT.htm](pathfinder-bestiary-items/nHYtHMopSyGQg3WT.htm)|Regeneration 20 (Deactivated by Cold Iron)|auto-trad|
|[NIgj8uAlgosxMdVf.htm](pathfinder-bestiary-items/NIgj8uAlgosxMdVf.htm)|Spider Swarm Venom|auto-trad|
|[NiysQhQQKa7jp0YE.htm](pathfinder-bestiary-items/NiysQhQQKa7jp0YE.htm)|Fangs|auto-trad|
|[NJYQQMNbIh1ENrZb.htm](pathfinder-bestiary-items/NJYQQMNbIh1ENrZb.htm)|Arcane Prepared Spells|auto-trad|
|[NkG3QwP5kR1wTHv9.htm](pathfinder-bestiary-items/NkG3QwP5kR1wTHv9.htm)|Fist|auto-trad|
|[NKOHZ7Ua7fqQNsAy.htm](pathfinder-bestiary-items/NKOHZ7Ua7fqQNsAy.htm)|Disgusting Demise|auto-trad|
|[NKPJH9dvADsJ0zhQ.htm](pathfinder-bestiary-items/NKPJH9dvADsJ0zhQ.htm)|Great Despair|auto-trad|
|[NKs8hoGb4IyfFWCw.htm](pathfinder-bestiary-items/NKs8hoGb4IyfFWCw.htm)|Vengeful Spite|auto-trad|
|[Nl15bkRTLq869IPi.htm](pathfinder-bestiary-items/Nl15bkRTLq869IPi.htm)|Petrifying Glance|auto-trad|
|[nLB0CpyqlvJpfjnr.htm](pathfinder-bestiary-items/nLB0CpyqlvJpfjnr.htm)|Stunning Screech|auto-trad|
|[nlmR2shVVcAynbhT.htm](pathfinder-bestiary-items/nlmR2shVVcAynbhT.htm)|At-Will Spells|auto-trad|
|[nlnYY25ImmMf9WxI.htm](pathfinder-bestiary-items/nlnYY25ImmMf9WxI.htm)|Deep Breath|auto-trad|
|[NM1DnsbI55f4G1ix.htm](pathfinder-bestiary-items/NM1DnsbI55f4G1ix.htm)|Paralytic Venom|auto-trad|
|[nmSxnKrk324bG1w4.htm](pathfinder-bestiary-items/nmSxnKrk324bG1w4.htm)|Sudden Strike|auto-trad|
|[NN3qDwLQRgho4Tub.htm](pathfinder-bestiary-items/NN3qDwLQRgho4Tub.htm)|Constant Spells|auto-trad|
|[nNGqk0aQljFVuL8g.htm](pathfinder-bestiary-items/nNGqk0aQljFVuL8g.htm)|Rituals|auto-trad|
|[nnTlOr0YpS0mwSt7.htm](pathfinder-bestiary-items/nnTlOr0YpS0mwSt7.htm)|Telepathy 100 feet|auto-trad|
|[NoNrnnXaKdOmoBx6.htm](pathfinder-bestiary-items/NoNrnnXaKdOmoBx6.htm)|Hunted Fear|auto-trad|
|[npD19zlFQ72xXHfo.htm](pathfinder-bestiary-items/npD19zlFQ72xXHfo.htm)|Jaws|auto-trad|
|[NpgXzERVlP06NMCx.htm](pathfinder-bestiary-items/NpgXzERVlP06NMCx.htm)|Draconic Frenzy|auto-trad|
|[npH3O5fYofr6M5MP.htm](pathfinder-bestiary-items/npH3O5fYofr6M5MP.htm)|Low-Light Vision|auto-trad|
|[nPlH09DpLlqhUZvO.htm](pathfinder-bestiary-items/nPlH09DpLlqhUZvO.htm)|Darkvision|auto-trad|
|[nQAtVtNKfvmpm1T4.htm](pathfinder-bestiary-items/nQAtVtNKfvmpm1T4.htm)|Darkvision|auto-trad|
|[NQF0fD75864Ytav1.htm](pathfinder-bestiary-items/NQF0fD75864Ytav1.htm)|Captivating Song|auto-trad|
|[nQr3N8AIARUFBqx8.htm](pathfinder-bestiary-items/nQr3N8AIARUFBqx8.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[nQudJs5Bky0XvlDC.htm](pathfinder-bestiary-items/nQudJs5Bky0XvlDC.htm)|Reposition|auto-trad|
|[Ns2pok7e6UtuKUd6.htm](pathfinder-bestiary-items/Ns2pok7e6UtuKUd6.htm)|Object Lesson|auto-trad|
|[nS51JCooM2dV2JTC.htm](pathfinder-bestiary-items/nS51JCooM2dV2JTC.htm)|Wild Empathy|auto-trad|
|[nS74JTfKVbDhxOa2.htm](pathfinder-bestiary-items/nS74JTfKVbDhxOa2.htm)|Shield Block|auto-trad|
|[NS7yHszQCqQS0Bfj.htm](pathfinder-bestiary-items/NS7yHszQCqQS0Bfj.htm)|Sneak Attack|auto-trad|
|[NSJZXBcVLdrnmMBh.htm](pathfinder-bestiary-items/NSJZXBcVLdrnmMBh.htm)|Focused Assault|auto-trad|
|[NtYoVyYPS90gzKOB.htm](pathfinder-bestiary-items/NtYoVyYPS90gzKOB.htm)|Flaming Armament|auto-trad|
|[Nu3jevz0ikfeGuLs.htm](pathfinder-bestiary-items/Nu3jevz0ikfeGuLs.htm)|Holy Striking Spear|auto-trad|
|[nu6HxTKMW550KwDx.htm](pathfinder-bestiary-items/nu6HxTKMW550KwDx.htm)|Tree Meld|auto-trad|
|[NUE54wJoG2LAVMO1.htm](pathfinder-bestiary-items/NUE54wJoG2LAVMO1.htm)|Iron Mind|auto-trad|
|[NUQlBj6YHAwX4fEr.htm](pathfinder-bestiary-items/NUQlBj6YHAwX4fEr.htm)|Ghost Hunter|auto-trad|
|[NURzeRkXcWn2KGXj.htm](pathfinder-bestiary-items/NURzeRkXcWn2KGXj.htm)|Constant Spells|auto-trad|
|[NUuZmLjmz4NPx29q.htm](pathfinder-bestiary-items/NUuZmLjmz4NPx29q.htm)|Wing|auto-trad|
|[NV6YJ68cn3RFInT8.htm](pathfinder-bestiary-items/NV6YJ68cn3RFInT8.htm)|Constant Spells|auto-trad|
|[NvaOvaZnru4aSp7g.htm](pathfinder-bestiary-items/NvaOvaZnru4aSp7g.htm)|Tentacle|auto-trad|
|[nVHHHvNcvXZZYYnc.htm](pathfinder-bestiary-items/nVHHHvNcvXZZYYnc.htm)|Darkvision|auto-trad|
|[nvkbBLwKlX05vPo2.htm](pathfinder-bestiary-items/nvkbBLwKlX05vPo2.htm)|Breath Weapon|auto-trad|
|[nVzYnoOpZcD3KXg9.htm](pathfinder-bestiary-items/nVzYnoOpZcD3KXg9.htm)|Flame of Justice|auto-trad|
|[nW2h9FRerHtWsX41.htm](pathfinder-bestiary-items/nW2h9FRerHtWsX41.htm)|Rock|auto-trad|
|[NWBV7oqS64HKJSkz.htm](pathfinder-bestiary-items/NWBV7oqS64HKJSkz.htm)|Blackaxe - Link|auto-trad|
|[nwc3AiZwtHc7DnZV.htm](pathfinder-bestiary-items/nwc3AiZwtHc7DnZV.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[NWQ4r5yoAoqIoqvJ.htm](pathfinder-bestiary-items/NWQ4r5yoAoqIoqvJ.htm)|Darkvision|auto-trad|
|[nWtiD4ASp2FFsobD.htm](pathfinder-bestiary-items/nWtiD4ASp2FFsobD.htm)|Ghostly Hand|auto-trad|
|[NwuVG96DJON1Txro.htm](pathfinder-bestiary-items/NwuVG96DJON1Txro.htm)|Constant Spells|auto-trad|
|[NX0owCPUZbsVJ4az.htm](pathfinder-bestiary-items/NX0owCPUZbsVJ4az.htm)|Jaws|auto-trad|
|[NXmjfJq99Q2rTjVD.htm](pathfinder-bestiary-items/NXmjfJq99Q2rTjVD.htm)|Lifesense 60 feet|auto-trad|
|[nXo4IM1jEOhgvr4C.htm](pathfinder-bestiary-items/nXo4IM1jEOhgvr4C.htm)|At-Will Spells|auto-trad|
|[nXQUyl9JGuo2YBLG.htm](pathfinder-bestiary-items/nXQUyl9JGuo2YBLG.htm)|Transparent|auto-trad|
|[nxwLnaNtKUpRi5Rp.htm](pathfinder-bestiary-items/nxwLnaNtKUpRi5Rp.htm)|Constant Spells|auto-trad|
|[NxXkXhUBYYX8UsfS.htm](pathfinder-bestiary-items/NxXkXhUBYYX8UsfS.htm)|Rugged Travel|auto-trad|
|[nZfgUjjwZVrdR1bZ.htm](pathfinder-bestiary-items/nZfgUjjwZVrdR1bZ.htm)|Trident|auto-trad|
|[O01HSm8iaUsEoGok.htm](pathfinder-bestiary-items/O01HSm8iaUsEoGok.htm)|Arcane Spontaneous Spells|auto-trad|
|[o1Dh44Cl7px3KGea.htm](pathfinder-bestiary-items/o1Dh44Cl7px3KGea.htm)|Grab|auto-trad|
|[O1iPSxTdSnBAxGt1.htm](pathfinder-bestiary-items/O1iPSxTdSnBAxGt1.htm)|Constrict|auto-trad|
|[O1YTrDKri9e9hU6w.htm](pathfinder-bestiary-items/O1YTrDKri9e9hU6w.htm)|Darkvision|auto-trad|
|[O2U2on80bNw6GMEe.htm](pathfinder-bestiary-items/O2U2on80bNw6GMEe.htm)|Trample|auto-trad|
|[O40irN7ET0rqNTrO.htm](pathfinder-bestiary-items/O40irN7ET0rqNTrO.htm)|At-Will Spells|auto-trad|
|[O5gqolsMM0WkB0GZ.htm](pathfinder-bestiary-items/O5gqolsMM0WkB0GZ.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[O5Hb34r44xT4xQZB.htm](pathfinder-bestiary-items/O5Hb34r44xT4xQZB.htm)|Smoke Vision|auto-trad|
|[O5nC82tyENuQH3SJ.htm](pathfinder-bestiary-items/O5nC82tyENuQH3SJ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[O5vFGncnfOVbJhh7.htm](pathfinder-bestiary-items/O5vFGncnfOVbJhh7.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[O6armi7YSCPK5JDa.htm](pathfinder-bestiary-items/O6armi7YSCPK5JDa.htm)|Throw Rock|auto-trad|
|[o6Y376tDDVyc2Cf8.htm](pathfinder-bestiary-items/o6Y376tDDVyc2Cf8.htm)|Bonetaker|auto-trad|
|[o78IIdQ4zpSjDrZe.htm](pathfinder-bestiary-items/o78IIdQ4zpSjDrZe.htm)|Talon|auto-trad|
|[O7BM6p23R9nIMLOP.htm](pathfinder-bestiary-items/O7BM6p23R9nIMLOP.htm)|Low-Light Vision|auto-trad|
|[o7HPbnFCe1XPngQQ.htm](pathfinder-bestiary-items/o7HPbnFCe1XPngQQ.htm)|Change Shape|auto-trad|
|[O7YsO4EwyIY6RIDs.htm](pathfinder-bestiary-items/O7YsO4EwyIY6RIDs.htm)|Jaws|auto-trad|
|[o8i1KXLMvT5NmVxw.htm](pathfinder-bestiary-items/o8i1KXLMvT5NmVxw.htm)|Low-Light Vision|auto-trad|
|[oA3OqSUqWQPylGlB.htm](pathfinder-bestiary-items/oA3OqSUqWQPylGlB.htm)|Ranseur|auto-trad|
|[oa6JQCeL4B2WQMPI.htm](pathfinder-bestiary-items/oa6JQCeL4B2WQMPI.htm)|Ghoul Fever|auto-trad|
|[oAdazPfRl3wntiS1.htm](pathfinder-bestiary-items/oAdazPfRl3wntiS1.htm)|Attack of Opportunity (Fangs Only)|auto-trad|
|[OaKZ88EQ5UVii9xw.htm](pathfinder-bestiary-items/OaKZ88EQ5UVii9xw.htm)|Undead Mastery|auto-trad|
|[oAM6tsN2NTZwvnTf.htm](pathfinder-bestiary-items/oAM6tsN2NTZwvnTf.htm)|Greater Darkvision|auto-trad|
|[OAt8guQQNXMQRYHW.htm](pathfinder-bestiary-items/OAt8guQQNXMQRYHW.htm)|Pounce|auto-trad|
|[oB9Lh7ZFScaG3Z04.htm](pathfinder-bestiary-items/oB9Lh7ZFScaG3Z04.htm)|Hungry Flurry|auto-trad|
|[ObIC1c8HIIuC53Nb.htm](pathfinder-bestiary-items/ObIC1c8HIIuC53Nb.htm)|Darkvision|auto-trad|
|[oblGn3AB742xJUfT.htm](pathfinder-bestiary-items/oblGn3AB742xJUfT.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[ObX0PxwHHLMvaTJn.htm](pathfinder-bestiary-items/ObX0PxwHHLMvaTJn.htm)|At-Will Spells|auto-trad|
|[oBZiGvpEg2BfHRnx.htm](pathfinder-bestiary-items/oBZiGvpEg2BfHRnx.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[oc0bsYm11TxBi7AZ.htm](pathfinder-bestiary-items/oc0bsYm11TxBi7AZ.htm)|Attack of Opportunity|auto-trad|
|[oCe57tAkaWpqe976.htm](pathfinder-bestiary-items/oCe57tAkaWpqe976.htm)|Improved Grab|auto-trad|
|[ocNVtm84bTjxf93b.htm](pathfinder-bestiary-items/ocNVtm84bTjxf93b.htm)|Spit|auto-trad|
|[OcYo8NrM7fSjcymh.htm](pathfinder-bestiary-items/OcYo8NrM7fSjcymh.htm)|Claw|auto-trad|
|[ODGzZo0VrXZyNziZ.htm](pathfinder-bestiary-items/ODGzZo0VrXZyNziZ.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[oDkPJwQC7jVjhBRN.htm](pathfinder-bestiary-items/oDkPJwQC7jVjhBRN.htm)|Regeneration 20 (Deactivated by Acid or Fire)|auto-trad|
|[odPtrANIuNiSlgo5.htm](pathfinder-bestiary-items/odPtrANIuNiSlgo5.htm)|Infernal Wound|auto-trad|
|[oe1VqqJvUvIMKRRa.htm](pathfinder-bestiary-items/oe1VqqJvUvIMKRRa.htm)|Wing|auto-trad|
|[oeg1zG9t1NO1R9l3.htm](pathfinder-bestiary-items/oeg1zG9t1NO1R9l3.htm)|Draconic Frenzy|auto-trad|
|[oEHHDWURCYEb1t7W.htm](pathfinder-bestiary-items/oEHHDWURCYEb1t7W.htm)|Jaws|auto-trad|
|[oEnTmvBRzYBAWuou.htm](pathfinder-bestiary-items/oEnTmvBRzYBAWuou.htm)|Scratch|auto-trad|
|[oeOZCitfRoZXCLfy.htm](pathfinder-bestiary-items/oeOZCitfRoZXCLfy.htm)|Tor Linnorm Venom|auto-trad|
|[OFaZodP0QXaOQNqv.htm](pathfinder-bestiary-items/OFaZodP0QXaOQNqv.htm)|Divine Rituals|auto-trad|
|[OFEj9wltNrZpJ77C.htm](pathfinder-bestiary-items/OFEj9wltNrZpJ77C.htm)|Improved Grab|auto-trad|
|[oFnlUf41ozZQtvKs.htm](pathfinder-bestiary-items/oFnlUf41ozZQtvKs.htm)|Dagger|auto-trad|
|[OFtZ11CEYiceib4W.htm](pathfinder-bestiary-items/OFtZ11CEYiceib4W.htm)|Rend|auto-trad|
|[OG9zHmjCiir50sUt.htm](pathfinder-bestiary-items/OG9zHmjCiir50sUt.htm)|Fast Healing 2 (In Open Air)|auto-trad|
|[ogjALspcg7GVz79c.htm](pathfinder-bestiary-items/ogjALspcg7GVz79c.htm)|Buck|auto-trad|
|[OgkLVLhW0uOMdbGj.htm](pathfinder-bestiary-items/OgkLVLhW0uOMdbGj.htm)|Formation|auto-trad|
|[OgOkCo3ZpgcUbZr1.htm](pathfinder-bestiary-items/OgOkCo3ZpgcUbZr1.htm)|Vampire Weaknesses|auto-trad|
|[oGR3hLNhUlAR66Kw.htm](pathfinder-bestiary-items/oGR3hLNhUlAR66Kw.htm)|Claw|auto-trad|
|[OhbQTfhDygZDiO3p.htm](pathfinder-bestiary-items/OhbQTfhDygZDiO3p.htm)|Darkvision|auto-trad|
|[Ohby2hZPzWDoCkLo.htm](pathfinder-bestiary-items/Ohby2hZPzWDoCkLo.htm)|Aura of Misfortune|auto-trad|
|[ohDZuFtUD8Y2pTMN.htm](pathfinder-bestiary-items/ohDZuFtUD8Y2pTMN.htm)|Claw|auto-trad|
|[OHs6oHOrlsXUSGkS.htm](pathfinder-bestiary-items/OHs6oHOrlsXUSGkS.htm)|Golden Luck|auto-trad|
|[ohwAZH20CeE3uGMp.htm](pathfinder-bestiary-items/ohwAZH20CeE3uGMp.htm)|Gallop|auto-trad|
|[Oi2IinqJzQhrUE4R.htm](pathfinder-bestiary-items/Oi2IinqJzQhrUE4R.htm)|Mandibles|auto-trad|
|[oiDsSoQk3eAUnpKj.htm](pathfinder-bestiary-items/oiDsSoQk3eAUnpKj.htm)|Body|auto-trad|
|[oikg7iKnE0OwX6CV.htm](pathfinder-bestiary-items/oikg7iKnE0OwX6CV.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[OIpLEpHTRmAWKVGT.htm](pathfinder-bestiary-items/OIpLEpHTRmAWKVGT.htm)|At-Will Spells|auto-trad|
|[OjJQfX1hTXQZsKNH.htm](pathfinder-bestiary-items/OjJQfX1hTXQZsKNH.htm)|Jaws|auto-trad|
|[OkpY1zJx7qxKu6nk.htm](pathfinder-bestiary-items/OkpY1zJx7qxKu6nk.htm)|Jaws|auto-trad|
|[okSVMAHfKVBCEcfy.htm](pathfinder-bestiary-items/okSVMAHfKVBCEcfy.htm)|Tail|auto-trad|
|[oKuYToU9Nv9ALIIv.htm](pathfinder-bestiary-items/oKuYToU9Nv9ALIIv.htm)|Rejuvenation|auto-trad|
|[OKzqL9XBxnlVikDo.htm](pathfinder-bestiary-items/OKzqL9XBxnlVikDo.htm)|Trample|auto-trad|
|[oL6JyyShxXN8PigD.htm](pathfinder-bestiary-items/oL6JyyShxXN8PigD.htm)|Darkvision|auto-trad|
|[OLoz41FJFungrpr5.htm](pathfinder-bestiary-items/OLoz41FJFungrpr5.htm)|Gibbering|auto-trad|
|[omXkYKF6c76YYVUV.htm](pathfinder-bestiary-items/omXkYKF6c76YYVUV.htm)|Divine Prepared Spells|auto-trad|
|[oMykAWUXRiU2dRig.htm](pathfinder-bestiary-items/oMykAWUXRiU2dRig.htm)|At-Will Spells|auto-trad|
|[oN5KE37Rg66tMPMp.htm](pathfinder-bestiary-items/oN5KE37Rg66tMPMp.htm)|Beak|auto-trad|
|[oncBOpovIROpa0vf.htm](pathfinder-bestiary-items/oncBOpovIROpa0vf.htm)|Explosion|auto-trad|
|[ONfkRVRg1gkRXJAJ.htm](pathfinder-bestiary-items/ONfkRVRg1gkRXJAJ.htm)|Primal Innate Spells|auto-trad|
|[Onifi8y9OVLqK3Ea.htm](pathfinder-bestiary-items/Onifi8y9OVLqK3Ea.htm)|Ferocity|auto-trad|
|[ONIHyZ3rL5vCjJod.htm](pathfinder-bestiary-items/ONIHyZ3rL5vCjJod.htm)|Intimidating Display|auto-trad|
|[ONo2JspAa1GnoAlR.htm](pathfinder-bestiary-items/ONo2JspAa1GnoAlR.htm)|Pierce Armor|auto-trad|
|[onqCyoeB5jfvwrI0.htm](pathfinder-bestiary-items/onqCyoeB5jfvwrI0.htm)|Foot|auto-trad|
|[oObpBdGWzojxRdfS.htm](pathfinder-bestiary-items/oObpBdGWzojxRdfS.htm)|Sneak Attack|auto-trad|
|[OOhSHhrOf35DMGaw.htm](pathfinder-bestiary-items/OOhSHhrOf35DMGaw.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[oOto6dxiQZFhZzKY.htm](pathfinder-bestiary-items/oOto6dxiQZFhZzKY.htm)|Grab|auto-trad|
|[opGxn2Tkn6TRXGQh.htm](pathfinder-bestiary-items/opGxn2Tkn6TRXGQh.htm)|Draconic Momentum|auto-trad|
|[oPKgkFop9yTr0x9k.htm](pathfinder-bestiary-items/oPKgkFop9yTr0x9k.htm)|Dagger|auto-trad|
|[Oqdlw7Axr5oYYU4m.htm](pathfinder-bestiary-items/Oqdlw7Axr5oYYU4m.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[oqGxkGNFqdTqq3p0.htm](pathfinder-bestiary-items/oqGxkGNFqdTqq3p0.htm)|Darkvision|auto-trad|
|[OQICdYHqGHNlnuut.htm](pathfinder-bestiary-items/OQICdYHqGHNlnuut.htm)|Swallow Whole|auto-trad|
|[oQxGeLSQxmxGUQ1l.htm](pathfinder-bestiary-items/oQxGeLSQxmxGUQ1l.htm)|Darkvision|auto-trad|
|[OQyH3C1xWSTxRaOA.htm](pathfinder-bestiary-items/OQyH3C1xWSTxRaOA.htm)|Shortbow|auto-trad|
|[ORBRYCSr88txQyIM.htm](pathfinder-bestiary-items/ORBRYCSr88txQyIM.htm)|Keen Returning Hatchet|auto-trad|
|[orlHVFqe7EEW7ruO.htm](pathfinder-bestiary-items/orlHVFqe7EEW7ruO.htm)|Foot|auto-trad|
|[ORqdD8bp2Bs2WWlC.htm](pathfinder-bestiary-items/ORqdD8bp2Bs2WWlC.htm)|Constant Spells|auto-trad|
|[osaWxy5RrqJdKuni.htm](pathfinder-bestiary-items/osaWxy5RrqJdKuni.htm)|Armor of Flames|auto-trad|
|[oSoU8REx8w5hSHcD.htm](pathfinder-bestiary-items/oSoU8REx8w5hSHcD.htm)|Twisting Tail|auto-trad|
|[oSq2qYAln4B4sFY0.htm](pathfinder-bestiary-items/oSq2qYAln4B4sFY0.htm)|Branch|auto-trad|
|[osyjgXeJV4vv5i3F.htm](pathfinder-bestiary-items/osyjgXeJV4vv5i3F.htm)|Occult Innate Spells|auto-trad|
|[OTDwVMsK2PJ4wuY6.htm](pathfinder-bestiary-items/OTDwVMsK2PJ4wuY6.htm)|Jaws|auto-trad|
|[OTKM4OSjZGvMhEEJ.htm](pathfinder-bestiary-items/OTKM4OSjZGvMhEEJ.htm)|Darkvision|auto-trad|
|[otqsXx9gjh7rnOpU.htm](pathfinder-bestiary-items/otqsXx9gjh7rnOpU.htm)|Goblin Scuttle|auto-trad|
|[OTuZbxVdoEtjN7ap.htm](pathfinder-bestiary-items/OTuZbxVdoEtjN7ap.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[otWGnycqKoeQC7U9.htm](pathfinder-bestiary-items/otWGnycqKoeQC7U9.htm)|Inexorable March|auto-trad|
|[oTXMvXAgkbwzgGhM.htm](pathfinder-bestiary-items/oTXMvXAgkbwzgGhM.htm)|Jaws|auto-trad|
|[oU0YlVUVw2Jjoabk.htm](pathfinder-bestiary-items/oU0YlVUVw2Jjoabk.htm)|Claw|auto-trad|
|[ouaeCAenWwGPbpdb.htm](pathfinder-bestiary-items/ouaeCAenWwGPbpdb.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[OUrfzECO9Dy3ncIO.htm](pathfinder-bestiary-items/OUrfzECO9Dy3ncIO.htm)|Paralysis|auto-trad|
|[OuwilwDvrI1Rqfzi.htm](pathfinder-bestiary-items/OuwilwDvrI1Rqfzi.htm)|Spore Tendrils|auto-trad|
|[ovaI2nqsEYxoo4Ys.htm](pathfinder-bestiary-items/ovaI2nqsEYxoo4Ys.htm)|Shortsword|auto-trad|
|[OvbTCZLdp8qg7KX2.htm](pathfinder-bestiary-items/OvbTCZLdp8qg7KX2.htm)|Darkvision|auto-trad|
|[OVmDA2DweiDfmQwN.htm](pathfinder-bestiary-items/OVmDA2DweiDfmQwN.htm)|Light Blindness|auto-trad|
|[OvsLPiSD2whyqgU0.htm](pathfinder-bestiary-items/OvsLPiSD2whyqgU0.htm)|Lightning Blade|auto-trad|
|[OW9KKx3Offpslmah.htm](pathfinder-bestiary-items/OW9KKx3Offpslmah.htm)|Lumbering Charge|auto-trad|
|[OwDiH7gzfhf1i4hu.htm](pathfinder-bestiary-items/OwDiH7gzfhf1i4hu.htm)|Wasp Venom|auto-trad|
|[OWImE1DpRm2byzUX.htm](pathfinder-bestiary-items/OWImE1DpRm2byzUX.htm)|Juke|auto-trad|
|[oWIxJUR2edElUUod.htm](pathfinder-bestiary-items/oWIxJUR2edElUUod.htm)|Fist|auto-trad|
|[OWlPaR3WuuSoTaGl.htm](pathfinder-bestiary-items/OWlPaR3WuuSoTaGl.htm)|Low-Light Vision|auto-trad|
|[OwVFI2xNdo532d71.htm](pathfinder-bestiary-items/OwVFI2xNdo532d71.htm)|At-Will Spells|auto-trad|
|[oWvZ2SZzMcdxkRRj.htm](pathfinder-bestiary-items/oWvZ2SZzMcdxkRRj.htm)|Divine Innate Spells|auto-trad|
|[oXq7JKUPh72N7tPy.htm](pathfinder-bestiary-items/oXq7JKUPh72N7tPy.htm)|Change Shape|auto-trad|
|[oXQB3BHjejzv1QJD.htm](pathfinder-bestiary-items/oXQB3BHjejzv1QJD.htm)|Darkvision|auto-trad|
|[oXZ3YdyxyoVGRGJs.htm](pathfinder-bestiary-items/oXZ3YdyxyoVGRGJs.htm)|Web Sense|auto-trad|
|[oXZ9gBxLTC4PuQec.htm](pathfinder-bestiary-items/oXZ9gBxLTC4PuQec.htm)|Horns|auto-trad|
|[oyHWuTde1I33SKT6.htm](pathfinder-bestiary-items/oyHWuTde1I33SKT6.htm)|Lamia's Caress|auto-trad|
|[OZD3jO9gpPebw7bB.htm](pathfinder-bestiary-items/OZD3jO9gpPebw7bB.htm)|Jaws|auto-trad|
|[OzGatkJy0uyM9zZJ.htm](pathfinder-bestiary-items/OzGatkJy0uyM9zZJ.htm)|Grab|auto-trad|
|[OZmaS9RmTVLBAzbH.htm](pathfinder-bestiary-items/OZmaS9RmTVLBAzbH.htm)|Low-Light Vision|auto-trad|
|[P0MPVynVKmKMh3FI.htm](pathfinder-bestiary-items/P0MPVynVKmKMh3FI.htm)|Bloodletting|auto-trad|
|[P0Q4Hgs9e8hwRX5G.htm](pathfinder-bestiary-items/P0Q4Hgs9e8hwRX5G.htm)|Envelop|auto-trad|
|[p1LTV3WNbUGTsSUY.htm](pathfinder-bestiary-items/p1LTV3WNbUGTsSUY.htm)|Darkvision|auto-trad|
|[p1OayYNJswICCP7g.htm](pathfinder-bestiary-items/p1OayYNJswICCP7g.htm)|Jaws|auto-trad|
|[p1oWXCxvOv782fSp.htm](pathfinder-bestiary-items/p1oWXCxvOv782fSp.htm)|Javelin|auto-trad|
|[p1rNOTBScTO9LxBR.htm](pathfinder-bestiary-items/p1rNOTBScTO9LxBR.htm)|At-Will Spells|auto-trad|
|[p1roWrlYRw0ZNWUR.htm](pathfinder-bestiary-items/p1roWrlYRw0ZNWUR.htm)|Alchemical Formulas|auto-trad|
|[p1yl81yMK4bvehu5.htm](pathfinder-bestiary-items/p1yl81yMK4bvehu5.htm)|Scimitar|auto-trad|
|[p4gHmixTKN3Gb87J.htm](pathfinder-bestiary-items/p4gHmixTKN3Gb87J.htm)|Dragon Jaws|auto-trad|
|[p51DG64tQvgJwgyu.htm](pathfinder-bestiary-items/p51DG64tQvgJwgyu.htm)|Claw|auto-trad|
|[p5mFSmPHyGqg2ima.htm](pathfinder-bestiary-items/p5mFSmPHyGqg2ima.htm)|Innate Divine Spells|auto-trad|
|[p5WXqIz3tJJCdoiz.htm](pathfinder-bestiary-items/p5WXqIz3tJJCdoiz.htm)|Draconic Momentum|auto-trad|
|[P658sPuiyqAqTdkz.htm](pathfinder-bestiary-items/P658sPuiyqAqTdkz.htm)|Focus Beauty|auto-trad|
|[P7JMDrm2HB1QPV3N.htm](pathfinder-bestiary-items/P7JMDrm2HB1QPV3N.htm)|Web Lurker Venom|auto-trad|
|[P7S7nuGr7qWkygVT.htm](pathfinder-bestiary-items/P7S7nuGr7qWkygVT.htm)|Stomp|auto-trad|
|[p80DPc7fm2auFNQg.htm](pathfinder-bestiary-items/p80DPc7fm2auFNQg.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[P8ybqvwXM5UVHrfF.htm](pathfinder-bestiary-items/P8ybqvwXM5UVHrfF.htm)|Darkvision|auto-trad|
|[p90CXzyif81DD51u.htm](pathfinder-bestiary-items/p90CXzyif81DD51u.htm)|Spiral of Despair|auto-trad|
|[PAbtJjm0RNsZSr1R.htm](pathfinder-bestiary-items/PAbtJjm0RNsZSr1R.htm)|Dark Naga Venom|auto-trad|
|[PAN3R11XOGsaiIbK.htm](pathfinder-bestiary-items/PAN3R11XOGsaiIbK.htm)|Claw|auto-trad|
|[PAY2I7fJGT3XbkIX.htm](pathfinder-bestiary-items/PAY2I7fJGT3XbkIX.htm)|Hoof|auto-trad|
|[pB0p0rg2zJNouFjM.htm](pathfinder-bestiary-items/pB0p0rg2zJNouFjM.htm)|Talon|auto-trad|
|[PB7yguG7pz4ib0ps.htm](pathfinder-bestiary-items/PB7yguG7pz4ib0ps.htm)|Shark Commune 150 feet|auto-trad|
|[pbydRoHQnSpw50iV.htm](pathfinder-bestiary-items/pbydRoHQnSpw50iV.htm)|Belly Grease|auto-trad|
|[pChhLSd048EpUiL0.htm](pathfinder-bestiary-items/pChhLSd048EpUiL0.htm)|Fangs|auto-trad|
|[pcv6j9xM8Qcw0h9R.htm](pathfinder-bestiary-items/pcv6j9xM8Qcw0h9R.htm)|Low-Light Vision|auto-trad|
|[pCYQE7ydvnjQGmPi.htm](pathfinder-bestiary-items/pCYQE7ydvnjQGmPi.htm)|Breath Weapon|auto-trad|
|[pCZn7Y6OrzZU4XgW.htm](pathfinder-bestiary-items/pCZn7Y6OrzZU4XgW.htm)|Primal Innate Spells|auto-trad|
|[pD6a7tBQWhYfzUSt.htm](pathfinder-bestiary-items/pD6a7tBQWhYfzUSt.htm)|Jaws|auto-trad|
|[PDtsBEnskV921hyI.htm](pathfinder-bestiary-items/PDtsBEnskV921hyI.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[pdwlm8jY2MDoJUAN.htm](pathfinder-bestiary-items/pdwlm8jY2MDoJUAN.htm)|Arcane Spontaneous Spells|auto-trad|
|[PdyYBEM94DTjouMT.htm](pathfinder-bestiary-items/PdyYBEM94DTjouMT.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[PeH9Tlr3y5ABJu0g.htm](pathfinder-bestiary-items/PeH9Tlr3y5ABJu0g.htm)|Fast Healing 10|auto-trad|
|[peHpZeJ7VwkuSurw.htm](pathfinder-bestiary-items/peHpZeJ7VwkuSurw.htm)|Darkvision|auto-trad|
|[pEpgPRttHusG3elF.htm](pathfinder-bestiary-items/pEpgPRttHusG3elF.htm)|Three-Headed Strike|auto-trad|
|[PF9LIocpqPbxqnvR.htm](pathfinder-bestiary-items/PF9LIocpqPbxqnvR.htm)|Thrash|auto-trad|
|[PfK4YgueBXw7cjqo.htm](pathfinder-bestiary-items/PfK4YgueBXw7cjqo.htm)|Vine Lash|auto-trad|
|[pFP0EC770FgwQQ6K.htm](pathfinder-bestiary-items/pFP0EC770FgwQQ6K.htm)|Fangs|auto-trad|
|[pFruogrBBB4pvImp.htm](pathfinder-bestiary-items/pFruogrBBB4pvImp.htm)|Primal Prepared Spells|auto-trad|
|[pG17snAKOgqgqba1.htm](pathfinder-bestiary-items/pG17snAKOgqgqba1.htm)|Reap|auto-trad|
|[pg6Z939BkFzl6ZUZ.htm](pathfinder-bestiary-items/pg6Z939BkFzl6ZUZ.htm)|Darkvision|auto-trad|
|[Pgjjt3dWPTpOGD8c.htm](pathfinder-bestiary-items/Pgjjt3dWPTpOGD8c.htm)|Ambush|auto-trad|
|[PgvThxPk7tH5CoXV.htm](pathfinder-bestiary-items/PgvThxPk7tH5CoXV.htm)|Constrict|auto-trad|
|[ph3U8sPdi5HCaPC6.htm](pathfinder-bestiary-items/ph3U8sPdi5HCaPC6.htm)|Arcane Prepared Spells|auto-trad|
|[pHkzyTUC7cfSCoYP.htm](pathfinder-bestiary-items/pHkzyTUC7cfSCoYP.htm)|Tail|auto-trad|
|[phmie8CQGIpk0d0n.htm](pathfinder-bestiary-items/phmie8CQGIpk0d0n.htm)|Darkvision|auto-trad|
|[PHogXv9VGXwEm0k8.htm](pathfinder-bestiary-items/PHogXv9VGXwEm0k8.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[phoQ0yOkbf9saI2l.htm](pathfinder-bestiary-items/phoQ0yOkbf9saI2l.htm)|Jaws|auto-trad|
|[PHrmAQv1FUnGiNrw.htm](pathfinder-bestiary-items/PHrmAQv1FUnGiNrw.htm)|Engulf|auto-trad|
|[PHro3bLYo9Br5Q7R.htm](pathfinder-bestiary-items/PHro3bLYo9Br5Q7R.htm)|Claw|auto-trad|
|[PHSSOZNHO0AKJt1q.htm](pathfinder-bestiary-items/PHSSOZNHO0AKJt1q.htm)|Blood Nourishment|auto-trad|
|[piJXVVEPai5UFibA.htm](pathfinder-bestiary-items/piJXVVEPai5UFibA.htm)|Sporesight (Imprecise) 60 feet|auto-trad|
|[PIjZTZsbQlriq5X3.htm](pathfinder-bestiary-items/PIjZTZsbQlriq5X3.htm)|Giant Centipede Venom|auto-trad|
|[PIKiUX4h9vUWIM3Y.htm](pathfinder-bestiary-items/PIKiUX4h9vUWIM3Y.htm)|Armor-Rending Bite|auto-trad|
|[pIuRGB2pXEwdYSox.htm](pathfinder-bestiary-items/pIuRGB2pXEwdYSox.htm)|Constant Spells|auto-trad|
|[PIznHIEc3sGmoAUM.htm](pathfinder-bestiary-items/PIznHIEc3sGmoAUM.htm)|Darkvision|auto-trad|
|[pJ0MYG8bkvto7yHb.htm](pathfinder-bestiary-items/pJ0MYG8bkvto7yHb.htm)|Grab|auto-trad|
|[pJ0uK52j5EXZwNhb.htm](pathfinder-bestiary-items/pJ0uK52j5EXZwNhb.htm)|Shortsword|auto-trad|
|[pJ5VmBxu1TYtLUuZ.htm](pathfinder-bestiary-items/pJ5VmBxu1TYtLUuZ.htm)|Attack of Opportunity|auto-trad|
|[PJbq8TKDuFGf9n7l.htm](pathfinder-bestiary-items/PJbq8TKDuFGf9n7l.htm)|Quasit Venom|auto-trad|
|[pK7TW7mAgEFfJu0Q.htm](pathfinder-bestiary-items/pK7TW7mAgEFfJu0Q.htm)|Curse of the Wererat|auto-trad|
|[pkCKdy44WRzMrMB3.htm](pathfinder-bestiary-items/pkCKdy44WRzMrMB3.htm)|Change Shape|auto-trad|
|[pkJjWhKaHykBdCEO.htm](pathfinder-bestiary-items/pkJjWhKaHykBdCEO.htm)|Earth Glide|auto-trad|
|[pKPJ4u9g0HQyU7b6.htm](pathfinder-bestiary-items/pKPJ4u9g0HQyU7b6.htm)|Furious Fusillade|auto-trad|
|[pkTWnzYC0Yob3mzf.htm](pathfinder-bestiary-items/pkTWnzYC0Yob3mzf.htm)|Darkvision|auto-trad|
|[pl4CFdDU1X88olbS.htm](pathfinder-bestiary-items/pl4CFdDU1X88olbS.htm)|Captive Rake|auto-trad|
|[pLB0Pp01hzJSCr2W.htm](pathfinder-bestiary-items/pLB0Pp01hzJSCr2W.htm)|Darkvision|auto-trad|
|[plka6anhQ8CltSBM.htm](pathfinder-bestiary-items/plka6anhQ8CltSBM.htm)|Greater Constrict|auto-trad|
|[PLLmu0Mc4VizpZYv.htm](pathfinder-bestiary-items/PLLmu0Mc4VizpZYv.htm)|Jaws|auto-trad|
|[PlWNcIaHKORc6Khe.htm](pathfinder-bestiary-items/PlWNcIaHKORc6Khe.htm)|Darkvision|auto-trad|
|[pm2fdTcLLvGAYrvI.htm](pathfinder-bestiary-items/pm2fdTcLLvGAYrvI.htm)|Divine Innate Spells|auto-trad|
|[PMBSc3Ypq9HMYeKR.htm](pathfinder-bestiary-items/PMBSc3Ypq9HMYeKR.htm)|Water Mastery|auto-trad|
|[PmcZB53eH1gUzaBq.htm](pathfinder-bestiary-items/PmcZB53eH1gUzaBq.htm)|Water-Bound|auto-trad|
|[PmnlcdA8wcJdRNmR.htm](pathfinder-bestiary-items/PmnlcdA8wcJdRNmR.htm)|Darkvision|auto-trad|
|[pMqdrNgUYaFMO0PK.htm](pathfinder-bestiary-items/pMqdrNgUYaFMO0PK.htm)|Tail|auto-trad|
|[pmqefsGShf77p3Zd.htm](pathfinder-bestiary-items/pmqefsGShf77p3Zd.htm)|Jaws|auto-trad|
|[PMu8PzzMLndJzxnZ.htm](pathfinder-bestiary-items/PMu8PzzMLndJzxnZ.htm)|Draconic Frenzy|auto-trad|
|[PNc43N7H5LeJXDQj.htm](pathfinder-bestiary-items/PNc43N7H5LeJXDQj.htm)|Darkvision|auto-trad|
|[pO7Lt2BaAG6zgDvw.htm](pathfinder-bestiary-items/pO7Lt2BaAG6zgDvw.htm)|Scent (Imprecise) 100 feet|auto-trad|
|[PoFwl1bniXcbhkld.htm](pathfinder-bestiary-items/PoFwl1bniXcbhkld.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[pOtIHukgdOyL8rMp.htm](pathfinder-bestiary-items/pOtIHukgdOyL8rMp.htm)|Golem Antimagic|auto-trad|
|[PPtTcRr6xJGUyX3Q.htm](pathfinder-bestiary-items/PPtTcRr6xJGUyX3Q.htm)|Constant Spells|auto-trad|
|[pPuHLDfAI4W6qEr6.htm](pathfinder-bestiary-items/pPuHLDfAI4W6qEr6.htm)|Messenger's Amnesty|auto-trad|
|[pqbk6QDDIpUVuKRX.htm](pathfinder-bestiary-items/pqbk6QDDIpUVuKRX.htm)|Root|auto-trad|
|[PQMwXaG8Zqcd02Oe.htm](pathfinder-bestiary-items/PQMwXaG8Zqcd02Oe.htm)|Triple Opportunity|auto-trad|
|[pqQTzMV3UX2rq1M9.htm](pathfinder-bestiary-items/pqQTzMV3UX2rq1M9.htm)|Arcane Innate Spells|auto-trad|
|[pR70Cym5tQf7vIJt.htm](pathfinder-bestiary-items/pR70Cym5tQf7vIJt.htm)|Claw|auto-trad|
|[pR9Yyd3sYM5iPspf.htm](pathfinder-bestiary-items/pR9Yyd3sYM5iPspf.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[prLWsWvW54YcYyNa.htm](pathfinder-bestiary-items/prLWsWvW54YcYyNa.htm)|Guiding Angel|auto-trad|
|[pRRtkR99srCWUO1j.htm](pathfinder-bestiary-items/pRRtkR99srCWUO1j.htm)|Corrosive Mass|auto-trad|
|[PrsRCxi6j7SHJeMu.htm](pathfinder-bestiary-items/PrsRCxi6j7SHJeMu.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[PrtfnlNRqEPNsEDh.htm](pathfinder-bestiary-items/PrtfnlNRqEPNsEDh.htm)|Darkvision|auto-trad|
|[pSG95fJhQ8ufTKho.htm](pathfinder-bestiary-items/pSG95fJhQ8ufTKho.htm)|At-Will Spells|auto-trad|
|[PsH13WVcjnQPM2Iy.htm](pathfinder-bestiary-items/PsH13WVcjnQPM2Iy.htm)|Fangs|auto-trad|
|[pSI2QoLhvn9RpREJ.htm](pathfinder-bestiary-items/pSI2QoLhvn9RpREJ.htm)|Trample|auto-trad|
|[pSi2yAlBjhVTBwq8.htm](pathfinder-bestiary-items/pSi2yAlBjhVTBwq8.htm)|Dance of Ruin|auto-trad|
|[pSJ8UuUcEbstSlWE.htm](pathfinder-bestiary-items/pSJ8UuUcEbstSlWE.htm)|Breath Weapon|auto-trad|
|[pSKjTPdO8e9KO9LI.htm](pathfinder-bestiary-items/pSKjTPdO8e9KO9LI.htm)|Multiple Opportunities|auto-trad|
|[pSpabH5fc53i9K81.htm](pathfinder-bestiary-items/pSpabH5fc53i9K81.htm)|Frightful Presence|auto-trad|
|[Ptn3Zb1Syk7YdL4y.htm](pathfinder-bestiary-items/Ptn3Zb1Syk7YdL4y.htm)|Claw|auto-trad|
|[PtwaILORaUXLPA4E.htm](pathfinder-bestiary-items/PtwaILORaUXLPA4E.htm)|+2 Status to All Saves vs. Cold|auto-trad|
|[pu2uW299sl7fVAeT.htm](pathfinder-bestiary-items/pu2uW299sl7fVAeT.htm)|Hoof|auto-trad|
|[PuCVCBzPaKVJYf9n.htm](pathfinder-bestiary-items/PuCVCBzPaKVJYf9n.htm)|Jaws|auto-trad|
|[pugIMNhBHc9VHl1z.htm](pathfinder-bestiary-items/pugIMNhBHc9VHl1z.htm)|Divine Innate Spells|auto-trad|
|[puh52kwYDM4gZi1Z.htm](pathfinder-bestiary-items/puh52kwYDM4gZi1Z.htm)|Club|auto-trad|
|[pV2kE2wYrZjLyBiR.htm](pathfinder-bestiary-items/pV2kE2wYrZjLyBiR.htm)|Push 10 feet|auto-trad|
|[pv4cYBnhSh901lFA.htm](pathfinder-bestiary-items/pv4cYBnhSh901lFA.htm)|Scent (Imprecise) 120 feet|auto-trad|
|[pVCudVc1B7mQVjP2.htm](pathfinder-bestiary-items/pVCudVc1B7mQVjP2.htm)|Longspear|auto-trad|
|[pvdHMw62l3uanob8.htm](pathfinder-bestiary-items/pvdHMw62l3uanob8.htm)|Rejuvenation|auto-trad|
|[pVi8f6cZ7YOZDd1k.htm](pathfinder-bestiary-items/pVi8f6cZ7YOZDd1k.htm)|Fist|auto-trad|
|[pVOVd7IIOsuNw3fo.htm](pathfinder-bestiary-items/pVOVd7IIOsuNw3fo.htm)|Rock|auto-trad|
|[PVrWJHEarOi9LFbD.htm](pathfinder-bestiary-items/PVrWJHEarOi9LFbD.htm)|Rope Snare|auto-trad|
|[pWL13AdPcgKCTD9i.htm](pathfinder-bestiary-items/pWL13AdPcgKCTD9i.htm)|Occult Innate Spells|auto-trad|
|[pWm5TTC2umXroqjo.htm](pathfinder-bestiary-items/pWm5TTC2umXroqjo.htm)|Pack Attack|auto-trad|
|[pWMLYiykJTfDP0Lf.htm](pathfinder-bestiary-items/pWMLYiykJTfDP0Lf.htm)|Fire Ray|auto-trad|
|[PWV65LaMZJtQOg9d.htm](pathfinder-bestiary-items/PWV65LaMZJtQOg9d.htm)|Entropy Sense (Imprecise) 30 feet|auto-trad|
|[pwxCJBVZsNZOzHqr.htm](pathfinder-bestiary-items/pwxCJBVZsNZOzHqr.htm)|Swarming|auto-trad|
|[pxfevBUBi49C9LID.htm](pathfinder-bestiary-items/pxfevBUBi49C9LID.htm)|Occult Innate Spells|auto-trad|
|[PxvHno73t8NaLGmq.htm](pathfinder-bestiary-items/PxvHno73t8NaLGmq.htm)|Tail|auto-trad|
|[pXWT2pfQnO8lkVNs.htm](pathfinder-bestiary-items/pXWT2pfQnO8lkVNs.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[PycbJPVh46UXYc9R.htm](pathfinder-bestiary-items/PycbJPVh46UXYc9R.htm)|Matriarch's Caress|auto-trad|
|[PyCQ7wcdOzNcvfQN.htm](pathfinder-bestiary-items/PyCQ7wcdOzNcvfQN.htm)|Low-Light Vision|auto-trad|
|[pyD5mpeBbbBmJcvA.htm](pathfinder-bestiary-items/pyD5mpeBbbBmJcvA.htm)|Draconic Momentum|auto-trad|
|[pydkk6U2LVFlha3A.htm](pathfinder-bestiary-items/pydkk6U2LVFlha3A.htm)|Dragon Chill|auto-trad|
|[pYg2FGHWnoS7Le78.htm](pathfinder-bestiary-items/pYg2FGHWnoS7Le78.htm)|Pack Attack|auto-trad|
|[pyVnqo4FLO0RWxrX.htm](pathfinder-bestiary-items/pyVnqo4FLO0RWxrX.htm)|Improved Push 20 feet (40 feet on a Critical Hit)|auto-trad|
|[pzO0JwhFqVcRKzAN.htm](pathfinder-bestiary-items/pzO0JwhFqVcRKzAN.htm)|Giant Tarantula Venom|auto-trad|
|[pZO6hvTQCaz319Or.htm](pathfinder-bestiary-items/pZO6hvTQCaz319Or.htm)|Arcane Innate Spells|auto-trad|
|[PzpzmSnCSWrwxzDO.htm](pathfinder-bestiary-items/PzpzmSnCSWrwxzDO.htm)|Longsword|auto-trad|
|[PZQRsmRUDWR50Kiq.htm](pathfinder-bestiary-items/PZQRsmRUDWR50Kiq.htm)|Engulf|auto-trad|
|[pzRCNxN7IHDbV6Wv.htm](pathfinder-bestiary-items/pzRCNxN7IHDbV6Wv.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[pZX2G1Vh8XNn8DCw.htm](pathfinder-bestiary-items/pZX2G1Vh8XNn8DCw.htm)|Occult Innate Spells|auto-trad|
|[q09mldN5OwQ33WsU.htm](pathfinder-bestiary-items/q09mldN5OwQ33WsU.htm)|Divine Innate Spells|auto-trad|
|[q0Md652kB4awIFj1.htm](pathfinder-bestiary-items/q0Md652kB4awIFj1.htm)|Darkvision|auto-trad|
|[q0wjPcj5k9jWlgJc.htm](pathfinder-bestiary-items/q0wjPcj5k9jWlgJc.htm)|Alchemist's Fire (Lesser)|auto-trad|
|[Q163iaXFcQOEmvkG.htm](pathfinder-bestiary-items/Q163iaXFcQOEmvkG.htm)|Rock|auto-trad|
|[q1LhwW1syAxORXNt.htm](pathfinder-bestiary-items/q1LhwW1syAxORXNt.htm)|Staccato Strike|auto-trad|
|[q1OobVjFqRsc58KI.htm](pathfinder-bestiary-items/q1OobVjFqRsc58KI.htm)|Negative Healing|auto-trad|
|[Q1WWRiadk7sX7Gor.htm](pathfinder-bestiary-items/Q1WWRiadk7sX7Gor.htm)|Low-Light Vision|auto-trad|
|[Q2tEMGeuUtWJQfpV.htm](pathfinder-bestiary-items/Q2tEMGeuUtWJQfpV.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[Q3hmK1bo6Sh5wQEk.htm](pathfinder-bestiary-items/Q3hmK1bo6Sh5wQEk.htm)|Tail|auto-trad|
|[Q3WxbSH9pxgZ7vKf.htm](pathfinder-bestiary-items/Q3WxbSH9pxgZ7vKf.htm)|Arcane Prepared Spells|auto-trad|
|[q4wAc8vDoq72HKvR.htm](pathfinder-bestiary-items/q4wAc8vDoq72HKvR.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Q5WTbR9lUXHMagS8.htm](pathfinder-bestiary-items/Q5WTbR9lUXHMagS8.htm)|Darkvision|auto-trad|
|[Q7WZ4aNo40BCE8gQ.htm](pathfinder-bestiary-items/Q7WZ4aNo40BCE8gQ.htm)|Blue Flames|auto-trad|
|[q8ASqdiBd9SflYVU.htm](pathfinder-bestiary-items/q8ASqdiBd9SflYVU.htm)|Spectral Ripple|auto-trad|
|[Q8ir96PaHjj9G372.htm](pathfinder-bestiary-items/Q8ir96PaHjj9G372.htm)|Light Blindness|auto-trad|
|[q9A4hwlPK1sYF6p1.htm](pathfinder-bestiary-items/q9A4hwlPK1sYF6p1.htm)|Keen Scythe|auto-trad|
|[q9pwZOGUR5waARMo.htm](pathfinder-bestiary-items/q9pwZOGUR5waARMo.htm)|At-Will Spells|auto-trad|
|[Q9TQFtMzhVqywWrB.htm](pathfinder-bestiary-items/Q9TQFtMzhVqywWrB.htm)|Deadly Cleave|auto-trad|
|[q9WAG2gpmBXA8cfi.htm](pathfinder-bestiary-items/q9WAG2gpmBXA8cfi.htm)|Luminous Spark|auto-trad|
|[qa0M1ZbNy1NNa2x3.htm](pathfinder-bestiary-items/qa0M1ZbNy1NNa2x3.htm)|Improved Knockdown|auto-trad|
|[qaEGHfAlvrP6CGwS.htm](pathfinder-bestiary-items/qaEGHfAlvrP6CGwS.htm)|Arcane Innate Spells|auto-trad|
|[qaf6YFqcLoAJ5CM4.htm](pathfinder-bestiary-items/qaf6YFqcLoAJ5CM4.htm)|Snowblind|auto-trad|
|[qaGJzO6SddpAXZKg.htm](pathfinder-bestiary-items/qaGJzO6SddpAXZKg.htm)|Animated Weapons|auto-trad|
|[QaHD4bOypv7qPbfa.htm](pathfinder-bestiary-items/QaHD4bOypv7qPbfa.htm)|Occult Innate Spells|auto-trad|
|[QAKar9jh0ZBRftHz.htm](pathfinder-bestiary-items/QAKar9jh0ZBRftHz.htm)|Draconic Momentum|auto-trad|
|[Qb7bNn98jqqwe6S8.htm](pathfinder-bestiary-items/Qb7bNn98jqqwe6S8.htm)|Negative Healing|auto-trad|
|[QbCC0DUye0oVFVc4.htm](pathfinder-bestiary-items/QbCC0DUye0oVFVc4.htm)|Draconic Momentum|auto-trad|
|[QblhIHkvSph67WTI.htm](pathfinder-bestiary-items/QblhIHkvSph67WTI.htm)|Grab|auto-trad|
|[QBOHDvvqEsUhdk0Y.htm](pathfinder-bestiary-items/QBOHDvvqEsUhdk0Y.htm)|Darkvision|auto-trad|
|[qBULiNM4mOxrHx1F.htm](pathfinder-bestiary-items/qBULiNM4mOxrHx1F.htm)|At-Will Spells|auto-trad|
|[qbw4q1VE5jAxSzTj.htm](pathfinder-bestiary-items/qbw4q1VE5jAxSzTj.htm)|Light Flash|auto-trad|
|[Qc7duDVeN4puhd9w.htm](pathfinder-bestiary-items/Qc7duDVeN4puhd9w.htm)|Electrical Burst|auto-trad|
|[qcaLKbJ4n5L9k9Vc.htm](pathfinder-bestiary-items/qcaLKbJ4n5L9k9Vc.htm)|Wretched Weeps|auto-trad|
|[qCmKPruRObzVL1Dr.htm](pathfinder-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|auto-trad|
|[QcXlYliyTvqiEDHM.htm](pathfinder-bestiary-items/QcXlYliyTvqiEDHM.htm)|Throw Rock|auto-trad|
|[qd9QFtwsgr9c7yLU.htm](pathfinder-bestiary-items/qd9QFtwsgr9c7yLU.htm)|Water Healing|auto-trad|
|[qDVNiRd6lW8OACXz.htm](pathfinder-bestiary-items/qDVNiRd6lW8OACXz.htm)|Talon|auto-trad|
|[qDypnfBHeFgNsDjH.htm](pathfinder-bestiary-items/qDypnfBHeFgNsDjH.htm)|Wrappings Lash|auto-trad|
|[qe55DvvyEW3AC1KT.htm](pathfinder-bestiary-items/qe55DvvyEW3AC1KT.htm)|Thoughtsense (Imprecise) 60 feet|auto-trad|
|[QEdIkRZIUAbC6n2O.htm](pathfinder-bestiary-items/QEdIkRZIUAbC6n2O.htm)|Stunning Strike|auto-trad|
|[QEvRlTqwOEac04ov.htm](pathfinder-bestiary-items/QEvRlTqwOEac04ov.htm)|Attach|auto-trad|
|[QEvUj7nK4bGVmLIe.htm](pathfinder-bestiary-items/QEvUj7nK4bGVmLIe.htm)|At-Will Spells|auto-trad|
|[qeZfMzOwr8GL4Lbh.htm](pathfinder-bestiary-items/qeZfMzOwr8GL4Lbh.htm)|Jaws|auto-trad|
|[qf9VkXweMZaEf2DP.htm](pathfinder-bestiary-items/qf9VkXweMZaEf2DP.htm)|Breath Weapon|auto-trad|
|[QFbge8UGIYYOgoxR.htm](pathfinder-bestiary-items/QFbge8UGIYYOgoxR.htm)|Scimitar|auto-trad|
|[qfEnUMdNzlNIipCp.htm](pathfinder-bestiary-items/qfEnUMdNzlNIipCp.htm)|Tentacle|auto-trad|
|[qFvtSo4GpY09J6wr.htm](pathfinder-bestiary-items/qFvtSo4GpY09J6wr.htm)|Dagger|auto-trad|
|[qg3r6OKHjX8qHiNS.htm](pathfinder-bestiary-items/qg3r6OKHjX8qHiNS.htm)|Occult Innate Spells|auto-trad|
|[QG473vzOkJRCm8Sm.htm](pathfinder-bestiary-items/QG473vzOkJRCm8Sm.htm)|Divine Innate Spells|auto-trad|
|[qGnxEwt7F2yY8OvT.htm](pathfinder-bestiary-items/qGnxEwt7F2yY8OvT.htm)|Corpse|auto-trad|
|[qhS5ZEJcqNFytqWd.htm](pathfinder-bestiary-items/qhS5ZEJcqNFytqWd.htm)|Jaws|auto-trad|
|[QhyMp71NOcnUwizh.htm](pathfinder-bestiary-items/QhyMp71NOcnUwizh.htm)|Dagger|auto-trad|
|[QI8J1BJYyzaJqGgr.htm](pathfinder-bestiary-items/QI8J1BJYyzaJqGgr.htm)|Throw Rock|auto-trad|
|[qIe9DjokLrx7mSvK.htm](pathfinder-bestiary-items/qIe9DjokLrx7mSvK.htm)|Boulder|auto-trad|
|[qiGtT3oDu82IOdB6.htm](pathfinder-bestiary-items/qiGtT3oDu82IOdB6.htm)|At-Will Spells|auto-trad|
|[QIkp9jKMkUCmBsVW.htm](pathfinder-bestiary-items/QIkp9jKMkUCmBsVW.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[QIPvgEw3eXLxKT8d.htm](pathfinder-bestiary-items/QIPvgEw3eXLxKT8d.htm)|Self-Repair|auto-trad|
|[qIqgF3PIeW7W6Fft.htm](pathfinder-bestiary-items/qIqgF3PIeW7W6Fft.htm)|Draconic Momentum|auto-trad|
|[qiTNUKllqrwozDmi.htm](pathfinder-bestiary-items/qiTNUKllqrwozDmi.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[qiuAaS1AQOUYcYVF.htm](pathfinder-bestiary-items/qiuAaS1AQOUYcYVF.htm)|Divine Rituals|auto-trad|
|[Qj4erun9wbqBExS0.htm](pathfinder-bestiary-items/Qj4erun9wbqBExS0.htm)|Skittering Reposition|auto-trad|
|[qjRGAO0ktmv3fxHQ.htm](pathfinder-bestiary-items/qjRGAO0ktmv3fxHQ.htm)|Drain Bonded Item|auto-trad|
|[qJvFeMBK4WEJThI3.htm](pathfinder-bestiary-items/qJvFeMBK4WEJThI3.htm)|Darkvision|auto-trad|
|[qJWbB2v6gs9de4fv.htm](pathfinder-bestiary-items/qJWbB2v6gs9de4fv.htm)|Horn|auto-trad|
|[qjYI8SCJrXWZopn6.htm](pathfinder-bestiary-items/qjYI8SCJrXWZopn6.htm)|Ogre Hook|auto-trad|
|[Qk0N8XSgIpwlNKmf.htm](pathfinder-bestiary-items/Qk0N8XSgIpwlNKmf.htm)|Darkvision|auto-trad|
|[Qk0PYD1Is307XwmU.htm](pathfinder-bestiary-items/Qk0PYD1Is307XwmU.htm)|Darkvision|auto-trad|
|[QK1RgrbqCLvwpciW.htm](pathfinder-bestiary-items/QK1RgrbqCLvwpciW.htm)|Low-Light Vision|auto-trad|
|[QK3Y9sXcSwM0Bsm8.htm](pathfinder-bestiary-items/QK3Y9sXcSwM0Bsm8.htm)|Claw|auto-trad|
|[qkKWOWe1wAc9Jffg.htm](pathfinder-bestiary-items/qkKWOWe1wAc9Jffg.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[QkmDtTbsggbI0MQy.htm](pathfinder-bestiary-items/QkmDtTbsggbI0MQy.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Qknp3UNQSMjNTUmL.htm](pathfinder-bestiary-items/Qknp3UNQSMjNTUmL.htm)|Grab|auto-trad|
|[QKo6NtH208FCd8UB.htm](pathfinder-bestiary-items/QKo6NtH208FCd8UB.htm)|Gnashing Grip|auto-trad|
|[qKQ3f2hl6CPJXoKV.htm](pathfinder-bestiary-items/qKQ3f2hl6CPJXoKV.htm)|Jaws|auto-trad|
|[QKtfubIn7OYITmUL.htm](pathfinder-bestiary-items/QKtfubIn7OYITmUL.htm)|Animated Weapon|auto-trad|
|[QL3594a7048GPCem.htm](pathfinder-bestiary-items/QL3594a7048GPCem.htm)|Wild Empathy|auto-trad|
|[QllK5fKffk7mVWTW.htm](pathfinder-bestiary-items/QllK5fKffk7mVWTW.htm)|Ground Manipulation|auto-trad|
|[QMCJ4eMLrKIZq31c.htm](pathfinder-bestiary-items/QMCJ4eMLrKIZq31c.htm)|Infestation Aura|auto-trad|
|[qmffmrGzEQLV9DUw.htm](pathfinder-bestiary-items/qmffmrGzEQLV9DUw.htm)|Tail|auto-trad|
|[QmJkEvQgW1G9i7g9.htm](pathfinder-bestiary-items/QmJkEvQgW1G9i7g9.htm)|Draconic Frenzy|auto-trad|
|[qN64iiPewb1CIOTo.htm](pathfinder-bestiary-items/qN64iiPewb1CIOTo.htm)|Arcane Innate Spells|auto-trad|
|[qniHJ3WvDHkUKTJ3.htm](pathfinder-bestiary-items/qniHJ3WvDHkUKTJ3.htm)|Composite Longbow|auto-trad|
|[QNpSPASXEvpUWHYE.htm](pathfinder-bestiary-items/QNpSPASXEvpUWHYE.htm)|Primal Rituals|auto-trad|
|[QnuvWppYTYaOJTUu.htm](pathfinder-bestiary-items/QnuvWppYTYaOJTUu.htm)|Divine Innate Spells|auto-trad|
|[QnXBLcuv0HgvOjdo.htm](pathfinder-bestiary-items/QnXBLcuv0HgvOjdo.htm)|Darkvision|auto-trad|
|[qOhxTyF0N56kRCTR.htm](pathfinder-bestiary-items/qOhxTyF0N56kRCTR.htm)|All-Around Vision|auto-trad|
|[qoJNaA8ESyVt6CJS.htm](pathfinder-bestiary-items/qoJNaA8ESyVt6CJS.htm)|Claw|auto-trad|
|[QotCsUsiVM6VxMDP.htm](pathfinder-bestiary-items/QotCsUsiVM6VxMDP.htm)|Frightful Presence|auto-trad|
|[QphofWAqcVVsApgN.htm](pathfinder-bestiary-items/QphofWAqcVVsApgN.htm)|Calming Bioluminescence|auto-trad|
|[qpVAENrZzV6kOBCY.htm](pathfinder-bestiary-items/qpVAENrZzV6kOBCY.htm)|Divine Innate Spells|auto-trad|
|[QPywhw5X98V0NX5g.htm](pathfinder-bestiary-items/QPywhw5X98V0NX5g.htm)|High Winds|auto-trad|
|[QpzMidSalFaHGivw.htm](pathfinder-bestiary-items/QpzMidSalFaHGivw.htm)|Change Shape|auto-trad|
|[qqGsZZCFNTIdlk8l.htm](pathfinder-bestiary-items/qqGsZZCFNTIdlk8l.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[qqpgMalbjoAykhjP.htm](pathfinder-bestiary-items/qqpgMalbjoAykhjP.htm)|Keen Scythe|auto-trad|
|[QqqqheeizksJ9NkJ.htm](pathfinder-bestiary-items/QqqqheeizksJ9NkJ.htm)|Aura of Peace|auto-trad|
|[qqqYIHulqpSQmjAc.htm](pathfinder-bestiary-items/qqqYIHulqpSQmjAc.htm)|Primal Innate Spells|auto-trad|
|[qqY5IlOQecESUHZP.htm](pathfinder-bestiary-items/qqY5IlOQecESUHZP.htm)|Tail Lash|auto-trad|
|[qR2C9ASEA77YOzgK.htm](pathfinder-bestiary-items/qR2C9ASEA77YOzgK.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[QRArjmTp1QFxOrMG.htm](pathfinder-bestiary-items/QRArjmTp1QFxOrMG.htm)|Poison Tooth|auto-trad|
|[QReEjgBjoIXn5NnC.htm](pathfinder-bestiary-items/QReEjgBjoIXn5NnC.htm)|Bladestorm|auto-trad|
|[qrxSbCbHPvFU1sVs.htm](pathfinder-bestiary-items/qrxSbCbHPvFU1sVs.htm)|Stinger|auto-trad|
|[qSJwECMRJYL4xDUN.htm](pathfinder-bestiary-items/qSJwECMRJYL4xDUN.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[qssb4IVNo6C2zIwe.htm](pathfinder-bestiary-items/qssb4IVNo6C2zIwe.htm)|Self-Resurrection|auto-trad|
|[QtDv0dYBxOQuvIS5.htm](pathfinder-bestiary-items/QtDv0dYBxOQuvIS5.htm)|Rock|auto-trad|
|[QTipkgJAwLBZFcwu.htm](pathfinder-bestiary-items/QTipkgJAwLBZFcwu.htm)|Focused Assault|auto-trad|
|[QtiX9tEriHb92lTU.htm](pathfinder-bestiary-items/QtiX9tEriHb92lTU.htm)|Pounce|auto-trad|
|[qtsF75k2Zterahrt.htm](pathfinder-bestiary-items/qtsF75k2Zterahrt.htm)|Vulnerable to Sunlight|auto-trad|
|[qtXj0TB36n30L7my.htm](pathfinder-bestiary-items/qtXj0TB36n30L7my.htm)|Disarm|auto-trad|
|[qu3FtGya8YY0lQf6.htm](pathfinder-bestiary-items/qu3FtGya8YY0lQf6.htm)|Low-Light Vision|auto-trad|
|[qUaqzRZJuoid0Hha.htm](pathfinder-bestiary-items/qUaqzRZJuoid0Hha.htm)|Ferocity|auto-trad|
|[QUcIjJglRfQBcSRB.htm](pathfinder-bestiary-items/QUcIjJglRfQBcSRB.htm)|Jaws|auto-trad|
|[qUGNxVPDLyqLK3is.htm](pathfinder-bestiary-items/qUGNxVPDLyqLK3is.htm)|Fist|auto-trad|
|[qugqAWTLubz9fcj2.htm](pathfinder-bestiary-items/qugqAWTLubz9fcj2.htm)|Speed Surge|auto-trad|
|[QulwvQlkERm3i0aV.htm](pathfinder-bestiary-items/QulwvQlkERm3i0aV.htm)|Darkvision|auto-trad|
|[qV0AlDGF3k44wWNb.htm](pathfinder-bestiary-items/qV0AlDGF3k44wWNb.htm)|Arcane Innate Spells|auto-trad|
|[Qv3klzQNlAwTTmrJ.htm](pathfinder-bestiary-items/Qv3klzQNlAwTTmrJ.htm)|Tail|auto-trad|
|[QVSO6xmJIGdqfP0G.htm](pathfinder-bestiary-items/QVSO6xmJIGdqfP0G.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[qWBeJdRzUrGmE3Di.htm](pathfinder-bestiary-items/qWBeJdRzUrGmE3Di.htm)|Wendigo Torment|auto-trad|
|[QWBpk7SbuXW6RHU1.htm](pathfinder-bestiary-items/QWBpk7SbuXW6RHU1.htm)|Darkvision|auto-trad|
|[qwvZRNMyaVviFlN8.htm](pathfinder-bestiary-items/qwvZRNMyaVviFlN8.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[QX0Ta7fQzEhuiCCB.htm](pathfinder-bestiary-items/QX0Ta7fQzEhuiCCB.htm)|Dagger|auto-trad|
|[qX4zB0ycmWv3B6wH.htm](pathfinder-bestiary-items/qX4zB0ycmWv3B6wH.htm)|Trample|auto-trad|
|[QxEVY9qhM6ZZyiar.htm](pathfinder-bestiary-items/QxEVY9qhM6ZZyiar.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[qXMpQcdrpm3l454K.htm](pathfinder-bestiary-items/qXMpQcdrpm3l454K.htm)|Greataxe|auto-trad|
|[qY4Gqits5ZaytjDY.htm](pathfinder-bestiary-items/qY4Gqits5ZaytjDY.htm)|Pain Frenzy|auto-trad|
|[Qy7s2ZDFTvr7xR1m.htm](pathfinder-bestiary-items/Qy7s2ZDFTvr7xR1m.htm)|Web Trap|auto-trad|
|[qyAQlXz42QsRrsCo.htm](pathfinder-bestiary-items/qyAQlXz42QsRrsCo.htm)|Flaming Scimitar|auto-trad|
|[QySe64WqYlaY7p5g.htm](pathfinder-bestiary-items/QySe64WqYlaY7p5g.htm)|Cloud Walk|auto-trad|
|[Qz0ZCScbiX2aU9gz.htm](pathfinder-bestiary-items/Qz0ZCScbiX2aU9gz.htm)|Darkvision|auto-trad|
|[qZG63nOxzyyLxiMs.htm](pathfinder-bestiary-items/qZG63nOxzyyLxiMs.htm)|Trample|auto-trad|
|[Qzt1bWHKvD7X2ua8.htm](pathfinder-bestiary-items/Qzt1bWHKvD7X2ua8.htm)|Change Shape|auto-trad|
|[r035iPNnkvHhoCqt.htm](pathfinder-bestiary-items/r035iPNnkvHhoCqt.htm)|Flail|auto-trad|
|[r04hQrUajd4T3K6Z.htm](pathfinder-bestiary-items/r04hQrUajd4T3K6Z.htm)|Wing Deflection|auto-trad|
|[r0h2w4y8aEPKbrb1.htm](pathfinder-bestiary-items/r0h2w4y8aEPKbrb1.htm)|Telepathy 100 feet|auto-trad|
|[r0zMXxalwdgrXzf3.htm](pathfinder-bestiary-items/r0zMXxalwdgrXzf3.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[r1b2EOtm5LT3mHWK.htm](pathfinder-bestiary-items/r1b2EOtm5LT3mHWK.htm)|Attack of Opportunity|auto-trad|
|[R1hhANtemlX6gt4D.htm](pathfinder-bestiary-items/R1hhANtemlX6gt4D.htm)|Darkvision|auto-trad|
|[r1SlNgELge3BicQv.htm](pathfinder-bestiary-items/r1SlNgELge3BicQv.htm)|Stench|auto-trad|
|[R2OzRzNgkA2Ivtk0.htm](pathfinder-bestiary-items/R2OzRzNgkA2Ivtk0.htm)|Jaws|auto-trad|
|[R3u6nnIK33qXVJNK.htm](pathfinder-bestiary-items/R3u6nnIK33qXVJNK.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[r3WeFaPkohKTnc3H.htm](pathfinder-bestiary-items/r3WeFaPkohKTnc3H.htm)|At-Will Spells|auto-trad|
|[r4Qkdy83VOkw6r0p.htm](pathfinder-bestiary-items/r4Qkdy83VOkw6r0p.htm)|Boot|auto-trad|
|[R4yDJ8qRT5NaKyDL.htm](pathfinder-bestiary-items/R4yDJ8qRT5NaKyDL.htm)|At-Will Spells|auto-trad|
|[r5JyVLxepP2bpNAq.htm](pathfinder-bestiary-items/r5JyVLxepP2bpNAq.htm)|Draconic Frenzy|auto-trad|
|[R5LM2Ijl78zvdKkF.htm](pathfinder-bestiary-items/R5LM2Ijl78zvdKkF.htm)|Change Shape|auto-trad|
|[r5yruYZzaDx9MoHR.htm](pathfinder-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|auto-trad|
|[r6CoZc9HQihbtMvD.htm](pathfinder-bestiary-items/r6CoZc9HQihbtMvD.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[r7eZQc9QrFmYE1sw.htm](pathfinder-bestiary-items/r7eZQc9QrFmYE1sw.htm)|Occult Innate Spells|auto-trad|
|[r7vHhfdRSulBJtov.htm](pathfinder-bestiary-items/r7vHhfdRSulBJtov.htm)|Truth Vulnerability|auto-trad|
|[R7wSakgDTKzgP8Eo.htm](pathfinder-bestiary-items/R7wSakgDTKzgP8Eo.htm)|Lifedrinker|auto-trad|
|[r89JPcf2a0fUQOAP.htm](pathfinder-bestiary-items/r89JPcf2a0fUQOAP.htm)|Frightful Presence|auto-trad|
|[R8f6nx9jMnFB03qP.htm](pathfinder-bestiary-items/R8f6nx9jMnFB03qP.htm)|Savage|auto-trad|
|[r95YqiTkdXz73fSA.htm](pathfinder-bestiary-items/r95YqiTkdXz73fSA.htm)|Low-Light Vision|auto-trad|
|[r9cb3wi4i5kmkIVZ.htm](pathfinder-bestiary-items/r9cb3wi4i5kmkIVZ.htm)|Wing|auto-trad|
|[R9CYDurnZPOlDiFg.htm](pathfinder-bestiary-items/R9CYDurnZPOlDiFg.htm)|Attack of Opportunity|auto-trad|
|[R9oCMdMtlo1aed3A.htm](pathfinder-bestiary-items/R9oCMdMtlo1aed3A.htm)|Wing Deflection|auto-trad|
|[Ra5fO9dQyDnBzGeh.htm](pathfinder-bestiary-items/Ra5fO9dQyDnBzGeh.htm)|Horn|auto-trad|
|[rA69Ao7ZsTSnYPbJ.htm](pathfinder-bestiary-items/rA69Ao7ZsTSnYPbJ.htm)|Darkvision|auto-trad|
|[RAFn5P8tJAjf5eEe.htm](pathfinder-bestiary-items/RAFn5P8tJAjf5eEe.htm)|Retaliatory Strike|auto-trad|
|[RaYfxxlT031lUgCP.htm](pathfinder-bestiary-items/RaYfxxlT031lUgCP.htm)|Coffin Restoration|auto-trad|
|[RB4XdKuhVpQcNu9I.htm](pathfinder-bestiary-items/RB4XdKuhVpQcNu9I.htm)|Darkvision|auto-trad|
|[rbhTSJX35TItIjZX.htm](pathfinder-bestiary-items/rbhTSJX35TItIjZX.htm)|Arcane Prepared Spells|auto-trad|
|[rBKIlYFoV3vW1kEN.htm](pathfinder-bestiary-items/rBKIlYFoV3vW1kEN.htm)|Moon Frenzy|auto-trad|
|[rbRI99thbIthUP1M.htm](pathfinder-bestiary-items/rbRI99thbIthUP1M.htm)|Darkvision|auto-trad|
|[RBSzDryfenHBXU42.htm](pathfinder-bestiary-items/RBSzDryfenHBXU42.htm)|Claw|auto-trad|
|[rbYlQJlNI4Qk0aXW.htm](pathfinder-bestiary-items/rbYlQJlNI4Qk0aXW.htm)|At-Will Spells|auto-trad|
|[rc2RmYmto2nlnmbA.htm](pathfinder-bestiary-items/rc2RmYmto2nlnmbA.htm)|Occult Innate Spells|auto-trad|
|[RCcD66otbEEUJwke.htm](pathfinder-bestiary-items/RCcD66otbEEUJwke.htm)|Primal Innate Spells|auto-trad|
|[rcFKhekRBFACg0ED.htm](pathfinder-bestiary-items/rcFKhekRBFACg0ED.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[rCo3WVukDgsbPhLD.htm](pathfinder-bestiary-items/rCo3WVukDgsbPhLD.htm)|Spore Cloud|auto-trad|
|[RDAI0FQdtpHFrFa5.htm](pathfinder-bestiary-items/RDAI0FQdtpHFrFa5.htm)|Intense Heat|auto-trad|
|[RDDSANqANJv09ktQ.htm](pathfinder-bestiary-items/RDDSANqANJv09ktQ.htm)|Sneak Attack|auto-trad|
|[RdYqZ3Dt6i0ca94C.htm](pathfinder-bestiary-items/RdYqZ3Dt6i0ca94C.htm)|Negative Healing|auto-trad|
|[rE3Xk58S3o4oHxdo.htm](pathfinder-bestiary-items/rE3Xk58S3o4oHxdo.htm)|Darkvision|auto-trad|
|[Re5HgCWQJTyzGewL.htm](pathfinder-bestiary-items/Re5HgCWQJTyzGewL.htm)|Web Trap|auto-trad|
|[Re9Ws4fM67Zs7ZLW.htm](pathfinder-bestiary-items/Re9Ws4fM67Zs7ZLW.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[reBqKuVcCW8B0Kfe.htm](pathfinder-bestiary-items/reBqKuVcCW8B0Kfe.htm)|Knockdown|auto-trad|
|[REiNYp4cWbH6OcX2.htm](pathfinder-bestiary-items/REiNYp4cWbH6OcX2.htm)|+3 Status to All Saves vs. Divine Magic|auto-trad|
|[reoPbPNpXyLy56Ny.htm](pathfinder-bestiary-items/reoPbPNpXyLy56Ny.htm)|Shroud of Flame|auto-trad|
|[RFk0rK9Iw5uP4Q4K.htm](pathfinder-bestiary-items/RFk0rK9Iw5uP4Q4K.htm)|Tentacle|auto-trad|
|[rfNAJhXoI4vQWbVg.htm](pathfinder-bestiary-items/rfNAJhXoI4vQWbVg.htm)|Darkvision|auto-trad|
|[RG4odvzk9bSpL19C.htm](pathfinder-bestiary-items/RG4odvzk9bSpL19C.htm)|Flame Jet|auto-trad|
|[rGbtqEmUFQBjGvOd.htm](pathfinder-bestiary-items/rGbtqEmUFQBjGvOd.htm)|Hungersense (Imprecise) 30 feet|auto-trad|
|[rGgLU8CkJNTv7kLC.htm](pathfinder-bestiary-items/rGgLU8CkJNTv7kLC.htm)|Darkvision|auto-trad|
|[rGTmjeER3V1KY02w.htm](pathfinder-bestiary-items/rGTmjeER3V1KY02w.htm)|Fast Swallow|auto-trad|
|[RgUtXbdxY4ir0GNG.htm](pathfinder-bestiary-items/RgUtXbdxY4ir0GNG.htm)|Darkvision|auto-trad|
|[rGYOx7SHsrayrSsA.htm](pathfinder-bestiary-items/rGYOx7SHsrayrSsA.htm)|Jaws|auto-trad|
|[rh2quedjlo5wYnpP.htm](pathfinder-bestiary-items/rh2quedjlo5wYnpP.htm)|+1 Status to All Saves vs. Death Effects|auto-trad|
|[RhGXKyPEo0bWS5D1.htm](pathfinder-bestiary-items/RhGXKyPEo0bWS5D1.htm)|Zombie Rot|auto-trad|
|[RHN04hf0GJqs0PN9.htm](pathfinder-bestiary-items/RHN04hf0GJqs0PN9.htm)|Constant Spells|auto-trad|
|[RHsGeVOoFFoGYyZ1.htm](pathfinder-bestiary-items/RHsGeVOoFFoGYyZ1.htm)|Change Shape|auto-trad|
|[rHVSJCwiDlcfs7Ms.htm](pathfinder-bestiary-items/rHVSJCwiDlcfs7Ms.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[RI07SPUsr3EfIXQX.htm](pathfinder-bestiary-items/RI07SPUsr3EfIXQX.htm)|Tail|auto-trad|
|[RI1LAadtF6vq96g2.htm](pathfinder-bestiary-items/RI1LAadtF6vq96g2.htm)|Claw|auto-trad|
|[ri68Bk0TcV1k9lqS.htm](pathfinder-bestiary-items/ri68Bk0TcV1k9lqS.htm)|Altered Weather|auto-trad|
|[RibPgalaxUxyuwym.htm](pathfinder-bestiary-items/RibPgalaxUxyuwym.htm)|Pack Attack|auto-trad|
|[RiC8zTuKfW44G8Mp.htm](pathfinder-bestiary-items/RiC8zTuKfW44G8Mp.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[RiTHbEGvltSHb56S.htm](pathfinder-bestiary-items/RiTHbEGvltSHb56S.htm)|Draft Contract|auto-trad|
|[rjGVnVgJiMZZR6zk.htm](pathfinder-bestiary-items/rjGVnVgJiMZZR6zk.htm)|Scent (Imprecise) 100 feet|auto-trad|
|[RJHEA3Ugy5eo8bGd.htm](pathfinder-bestiary-items/RJHEA3Ugy5eo8bGd.htm)|Arcane Spontaneous Spells|auto-trad|
|[rJiIUBXia2sB40kR.htm](pathfinder-bestiary-items/rJiIUBXia2sB40kR.htm)|Woodland Stride|auto-trad|
|[RJlLjrzqdXREWyPR.htm](pathfinder-bestiary-items/RJlLjrzqdXREWyPR.htm)|Telekinetic Storm|auto-trad|
|[RjqqCquV2ghiNEdO.htm](pathfinder-bestiary-items/RjqqCquV2ghiNEdO.htm)|Arcane Innate Spells|auto-trad|
|[Rk71VDo2VsvkYsEO.htm](pathfinder-bestiary-items/Rk71VDo2VsvkYsEO.htm)|Darkvision|auto-trad|
|[rk9Qa3TRqh7vnR4r.htm](pathfinder-bestiary-items/rk9Qa3TRqh7vnR4r.htm)|Deep Breath|auto-trad|
|[RKCbcbBugZzIrxdw.htm](pathfinder-bestiary-items/RKCbcbBugZzIrxdw.htm)|Menacing Guardian|auto-trad|
|[rkM1yAFkUMEFUnsZ.htm](pathfinder-bestiary-items/rkM1yAFkUMEFUnsZ.htm)|Jaws|auto-trad|
|[RKyutDZZD9yAhzjX.htm](pathfinder-bestiary-items/RKyutDZZD9yAhzjX.htm)|Attack of Opportunity|auto-trad|
|[rL7A4RH2uX8GGbWM.htm](pathfinder-bestiary-items/rL7A4RH2uX8GGbWM.htm)|Wild Empathy|auto-trad|
|[RlajqjwuSiKB8NGC.htm](pathfinder-bestiary-items/RlajqjwuSiKB8NGC.htm)|Impose Paralysis|auto-trad|
|[rlBRYOIKCiyQfcQ8.htm](pathfinder-bestiary-items/rlBRYOIKCiyQfcQ8.htm)|Primal Prepared Spells|auto-trad|
|[rLFEG0L8t229JOcM.htm](pathfinder-bestiary-items/rLFEG0L8t229JOcM.htm)|Jaws|auto-trad|
|[RLggEzBamBn1M5sX.htm](pathfinder-bestiary-items/RLggEzBamBn1M5sX.htm)|Breath Weapon|auto-trad|
|[RLXxZqgZH5Ym4SGI.htm](pathfinder-bestiary-items/RLXxZqgZH5Ym4SGI.htm)|Blinding Aura|auto-trad|
|[RMbB0Cy1iBEXCD5A.htm](pathfinder-bestiary-items/RMbB0Cy1iBEXCD5A.htm)|Keen Longsword|auto-trad|
|[RmFwZAbN0gQEv92W.htm](pathfinder-bestiary-items/RmFwZAbN0gQEv92W.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[rMKHbVkOcGipVlzt.htm](pathfinder-bestiary-items/rMKHbVkOcGipVlzt.htm)|At-Will Spells|auto-trad|
|[RN3i79HbKaXreAiG.htm](pathfinder-bestiary-items/RN3i79HbKaXreAiG.htm)|Darkvision|auto-trad|
|[rn3k17gHgz0K9teg.htm](pathfinder-bestiary-items/rn3k17gHgz0K9teg.htm)|Claw|auto-trad|
|[rNMUpQjQil41A67R.htm](pathfinder-bestiary-items/rNMUpQjQil41A67R.htm)|Arcane Innate Spells|auto-trad|
|[RnOot5zAIUVb7mhI.htm](pathfinder-bestiary-items/RnOot5zAIUVb7mhI.htm)|Greataxe|auto-trad|
|[RNVvCjM6WpoASTKg.htm](pathfinder-bestiary-items/RNVvCjM6WpoASTKg.htm)|Constrict|auto-trad|
|[rog22HpLDkUnfolA.htm](pathfinder-bestiary-items/rog22HpLDkUnfolA.htm)|Goat Horns|auto-trad|
|[RoHDkOS2aYfoab02.htm](pathfinder-bestiary-items/RoHDkOS2aYfoab02.htm)|Reflect Spell|auto-trad|
|[ROihL0GMWnjLTGFx.htm](pathfinder-bestiary-items/ROihL0GMWnjLTGFx.htm)|Attack of Opportunity|auto-trad|
|[ROtcz4ACRnmedQua.htm](pathfinder-bestiary-items/ROtcz4ACRnmedQua.htm)|Darting Strike|auto-trad|
|[Rp6sCg2LppmsJwKC.htm](pathfinder-bestiary-items/Rp6sCg2LppmsJwKC.htm)|Jaws|auto-trad|
|[rpjuMiQRY2h0zJ1H.htm](pathfinder-bestiary-items/rpjuMiQRY2h0zJ1H.htm)|Arcane Spontaneous Spells|auto-trad|
|[RQ0SxaYgQ0WCzXyb.htm](pathfinder-bestiary-items/RQ0SxaYgQ0WCzXyb.htm)|Occult Innate Spells|auto-trad|
|[rQ1dAhk5hHaVicBP.htm](pathfinder-bestiary-items/rQ1dAhk5hHaVicBP.htm)|Grab|auto-trad|
|[rQa09Nb9LtSI9UYi.htm](pathfinder-bestiary-items/rQa09Nb9LtSI9UYi.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[rQT7KUvA4yzEm4PO.htm](pathfinder-bestiary-items/rQT7KUvA4yzEm4PO.htm)|Sunlight Powerlessness|auto-trad|
|[rQVfc5bR7Xpe25cT.htm](pathfinder-bestiary-items/rQVfc5bR7Xpe25cT.htm)|Fling|auto-trad|
|[RQyE6xYaEg8W7C38.htm](pathfinder-bestiary-items/RQyE6xYaEg8W7C38.htm)|Invigorating Passion|auto-trad|
|[RRFUmIxRsdfJqd8F.htm](pathfinder-bestiary-items/RRFUmIxRsdfJqd8F.htm)|Breath Weapon|auto-trad|
|[RRokxFBvuXGVzodp.htm](pathfinder-bestiary-items/RRokxFBvuXGVzodp.htm)|Darkvision|auto-trad|
|[RrUfNcdLaL1BhWLR.htm](pathfinder-bestiary-items/RrUfNcdLaL1BhWLR.htm)|Darkvision|auto-trad|
|[rs9VqajU6hTlLt5o.htm](pathfinder-bestiary-items/rs9VqajU6hTlLt5o.htm)|Darkvision|auto-trad|
|[RSzJjCjwpz8jIS3J.htm](pathfinder-bestiary-items/RSzJjCjwpz8jIS3J.htm)|Iron Golem Poison|auto-trad|
|[rtbwOcOPVCHoeTWz.htm](pathfinder-bestiary-items/rtbwOcOPVCHoeTWz.htm)|Constant Spells|auto-trad|
|[rTDme0840mDZkokU.htm](pathfinder-bestiary-items/rTDme0840mDZkokU.htm)|Swallow Whole|auto-trad|
|[rtgmVsdePPj1Qutv.htm](pathfinder-bestiary-items/rtgmVsdePPj1Qutv.htm)|Fist|auto-trad|
|[RUcX4XrsDpkjIiPy.htm](pathfinder-bestiary-items/RUcX4XrsDpkjIiPy.htm)|Primal Innate Spells|auto-trad|
|[rUzXVdDT4B94gK6i.htm](pathfinder-bestiary-items/rUzXVdDT4B94gK6i.htm)|Trident|auto-trad|
|[rvraBop9oOSwcjkq.htm](pathfinder-bestiary-items/rvraBop9oOSwcjkq.htm)|Swift Leap|auto-trad|
|[rw75XVfj9XlY7JGq.htm](pathfinder-bestiary-items/rw75XVfj9XlY7JGq.htm)|Draconic Frenzy|auto-trad|
|[rWeRpLhEFS7p5VCy.htm](pathfinder-bestiary-items/rWeRpLhEFS7p5VCy.htm)|Formation|auto-trad|
|[Rwgsw9DmxxBRN4NY.htm](pathfinder-bestiary-items/Rwgsw9DmxxBRN4NY.htm)|Constrict|auto-trad|
|[rwK2kJnSkkWTKSwP.htm](pathfinder-bestiary-items/rwK2kJnSkkWTKSwP.htm)|Mandibles|auto-trad|
|[RWLfly211783dq5i.htm](pathfinder-bestiary-items/RWLfly211783dq5i.htm)|Tactician of Cocytus|auto-trad|
|[RX0OsA2SH1cSF0rn.htm](pathfinder-bestiary-items/RX0OsA2SH1cSF0rn.htm)|Horn|auto-trad|
|[rXhfsoFPa9y2NdHs.htm](pathfinder-bestiary-items/rXhfsoFPa9y2NdHs.htm)|Grab|auto-trad|
|[rxoyBugXyIn6mRoC.htm](pathfinder-bestiary-items/rxoyBugXyIn6mRoC.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Rxv5Fh6XLsy8yyXk.htm](pathfinder-bestiary-items/Rxv5Fh6XLsy8yyXk.htm)|Divine Innate Spells|auto-trad|
|[rYCzNNJekAP07yuk.htm](pathfinder-bestiary-items/rYCzNNJekAP07yuk.htm)|Arcane Innate Spells|auto-trad|
|[RYEcc50Tn0g5GE6m.htm](pathfinder-bestiary-items/RYEcc50Tn0g5GE6m.htm)|Tail Trip|auto-trad|
|[ryo9HmpaThFAUxD6.htm](pathfinder-bestiary-items/ryo9HmpaThFAUxD6.htm)|Constrict|auto-trad|
|[RyRiVUoP2scafR6g.htm](pathfinder-bestiary-items/RyRiVUoP2scafR6g.htm)|Hateful Tide|auto-trad|
|[rYzzaXPAJ15uHtmD.htm](pathfinder-bestiary-items/rYzzaXPAJ15uHtmD.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[rz1sIDAc7CEOMR8R.htm](pathfinder-bestiary-items/rz1sIDAc7CEOMR8R.htm)|Claw|auto-trad|
|[rZ6DDkGMLvCQH70m.htm](pathfinder-bestiary-items/rZ6DDkGMLvCQH70m.htm)|Hungry Flurry|auto-trad|
|[RZBxog3E2iCbQY5r.htm](pathfinder-bestiary-items/RZBxog3E2iCbQY5r.htm)|Main-Gauche|auto-trad|
|[RZijNBizRW62zYQN.htm](pathfinder-bestiary-items/RZijNBizRW62zYQN.htm)|Flame Of Justice|auto-trad|
|[RZv15pm5vEmZAZhK.htm](pathfinder-bestiary-items/RZv15pm5vEmZAZhK.htm)|Infernal Temptation|auto-trad|
|[s1vorlaJQl2JXBN2.htm](pathfinder-bestiary-items/s1vorlaJQl2JXBN2.htm)|Baton|auto-trad|
|[s2CoilBTqkwO72Wu.htm](pathfinder-bestiary-items/s2CoilBTqkwO72Wu.htm)|Darkvision|auto-trad|
|[s2SFLAZVqryeGl0O.htm](pathfinder-bestiary-items/s2SFLAZVqryeGl0O.htm)|Trident|auto-trad|
|[S2UbYSN0YRKJbMS0.htm](pathfinder-bestiary-items/S2UbYSN0YRKJbMS0.htm)|Trample|auto-trad|
|[S2UO1rBCsuy3H5Ii.htm](pathfinder-bestiary-items/S2UO1rBCsuy3H5Ii.htm)|Darkvision|auto-trad|
|[S32QCqZ8XlolYIfE.htm](pathfinder-bestiary-items/S32QCqZ8XlolYIfE.htm)|Occult Spontaneous Spells|auto-trad|
|[S4BAB593hbKEobTN.htm](pathfinder-bestiary-items/S4BAB593hbKEobTN.htm)|Rituals|auto-trad|
|[s4VdQZkBvye1Qm6o.htm](pathfinder-bestiary-items/s4VdQZkBvye1Qm6o.htm)|Scent (Imprecise) 100 feet|auto-trad|
|[s5DYWIxSytUTy1x2.htm](pathfinder-bestiary-items/s5DYWIxSytUTy1x2.htm)|Fangs|auto-trad|
|[S5RuYzvwzPUK3wzT.htm](pathfinder-bestiary-items/S5RuYzvwzPUK3wzT.htm)|Throw Rock|auto-trad|
|[S5TEXAbFO5yNQ9JP.htm](pathfinder-bestiary-items/S5TEXAbFO5yNQ9JP.htm)|Occult Innate Spells|auto-trad|
|[S6DALcZPm4YWrvWR.htm](pathfinder-bestiary-items/S6DALcZPm4YWrvWR.htm)|Jaws|auto-trad|
|[S6FJYbxVlzTTwn0T.htm](pathfinder-bestiary-items/S6FJYbxVlzTTwn0T.htm)|Smoke Vision|auto-trad|
|[S7hKN8DRaICYTRDX.htm](pathfinder-bestiary-items/S7hKN8DRaICYTRDX.htm)|Darkvision|auto-trad|
|[S7Khn0wjpXjjyPAp.htm](pathfinder-bestiary-items/S7Khn0wjpXjjyPAp.htm)|Mist Escape|auto-trad|
|[s9Lpslvp7eJbAjWr.htm](pathfinder-bestiary-items/s9Lpslvp7eJbAjWr.htm)|Redirect Fire|auto-trad|
|[S9z7EwyzhGnCFZDI.htm](pathfinder-bestiary-items/S9z7EwyzhGnCFZDI.htm)|Enormous Inhalation|auto-trad|
|[sA9IvnRiBKdOegEF.htm](pathfinder-bestiary-items/sA9IvnRiBKdOegEF.htm)|Wing|auto-trad|
|[Saaz9FC4uuWqbzUs.htm](pathfinder-bestiary-items/Saaz9FC4uuWqbzUs.htm)|Swallow Whole|auto-trad|
|[SaMUZistsZa0Bskd.htm](pathfinder-bestiary-items/SaMUZistsZa0Bskd.htm)|All-Around Vision|auto-trad|
|[saoVl2MlopNbuTo8.htm](pathfinder-bestiary-items/saoVl2MlopNbuTo8.htm)|Aklys|auto-trad|
|[saqRnmZhtXKwqFed.htm](pathfinder-bestiary-items/saqRnmZhtXKwqFed.htm)|Demilich Eye Gems|auto-trad|
|[SbBp90Oz1LZfllYh.htm](pathfinder-bestiary-items/SbBp90Oz1LZfllYh.htm)|Spear|auto-trad|
|[sBRqXEDoaHJdg0SF.htm](pathfinder-bestiary-items/sBRqXEDoaHJdg0SF.htm)|Blackaxe|auto-trad|
|[SC5giI24l8gEnx5E.htm](pathfinder-bestiary-items/SC5giI24l8gEnx5E.htm)|Primal Innate Spells|auto-trad|
|[SCGxYeDTWb4Hznwu.htm](pathfinder-bestiary-items/SCGxYeDTWb4Hznwu.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[sCLiSoe59ubmpiPk.htm](pathfinder-bestiary-items/sCLiSoe59ubmpiPk.htm)|Change Shape|auto-trad|
|[sClpmySLDIh95u0m.htm](pathfinder-bestiary-items/sClpmySLDIh95u0m.htm)|Wavesense 30 feet|auto-trad|
|[scTXJJONiH5Y5Rpf.htm](pathfinder-bestiary-items/scTXJJONiH5Y5Rpf.htm)|Golem Antimagic|auto-trad|
|[scw9HnbCVwAyFzok.htm](pathfinder-bestiary-items/scw9HnbCVwAyFzok.htm)|Rend|auto-trad|
|[SDPanMrj394qVPdp.htm](pathfinder-bestiary-items/SDPanMrj394qVPdp.htm)|Mandibles|auto-trad|
|[SdRYcTTnrhfiMQBr.htm](pathfinder-bestiary-items/SdRYcTTnrhfiMQBr.htm)|Primal Prepared Spells|auto-trad|
|[SdvOPdD36kKFhSXc.htm](pathfinder-bestiary-items/SdvOPdD36kKFhSXc.htm)|Ward Contract|auto-trad|
|[Se0RGAQyLYAwETrV.htm](pathfinder-bestiary-items/Se0RGAQyLYAwETrV.htm)|Beak|auto-trad|
|[SE3QsBzRX2bmkrnR.htm](pathfinder-bestiary-items/SE3QsBzRX2bmkrnR.htm)|Brain Loss|auto-trad|
|[sE5xyEupWe7f3yot.htm](pathfinder-bestiary-items/sE5xyEupWe7f3yot.htm)|At-Will Spells|auto-trad|
|[sEBLtOOpyQCNzmBJ.htm](pathfinder-bestiary-items/sEBLtOOpyQCNzmBJ.htm)|Jaws|auto-trad|
|[SeJXe222cuooTJUx.htm](pathfinder-bestiary-items/SeJXe222cuooTJUx.htm)|Split|auto-trad|
|[sElxdOJLECdfNADx.htm](pathfinder-bestiary-items/sElxdOJLECdfNADx.htm)|Constant Spells|auto-trad|
|[SFhZjJoSIAYrPWoO.htm](pathfinder-bestiary-items/SFhZjJoSIAYrPWoO.htm)|Low-Light Vision|auto-trad|
|[SFJ6XAQzgQGC5zVV.htm](pathfinder-bestiary-items/SFJ6XAQzgQGC5zVV.htm)|Ground Slam|auto-trad|
|[SfTYucTXJbISe6Da.htm](pathfinder-bestiary-items/SfTYucTXJbISe6Da.htm)|All-Around Vision|auto-trad|
|[sG3tFge2ls3X5eek.htm](pathfinder-bestiary-items/sG3tFge2ls3X5eek.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[sGGqzFbYP70AiQpk.htm](pathfinder-bestiary-items/sGGqzFbYP70AiQpk.htm)|Jaws|auto-trad|
|[sgKONsx24qNmQXvo.htm](pathfinder-bestiary-items/sgKONsx24qNmQXvo.htm)|Breath Weapon|auto-trad|
|[sgLK6WwDNZXG3X0e.htm](pathfinder-bestiary-items/sgLK6WwDNZXG3X0e.htm)|Trample|auto-trad|
|[SHBduQHQxTGNFYyK.htm](pathfinder-bestiary-items/SHBduQHQxTGNFYyK.htm)|Constant Spells|auto-trad|
|[sHRh4SLqM1rBlpHb.htm](pathfinder-bestiary-items/sHRh4SLqM1rBlpHb.htm)|Attack of Opportunity|auto-trad|
|[siaeh3ZOXYXVsYlo.htm](pathfinder-bestiary-items/siaeh3ZOXYXVsYlo.htm)|Ether Spider Venom|auto-trad|
|[siaZvbqZwz1jMZvH.htm](pathfinder-bestiary-items/siaZvbqZwz1jMZvH.htm)|Arcane Innate Spells|auto-trad|
|[sIfVG6GaZyAaZ3xW.htm](pathfinder-bestiary-items/sIfVG6GaZyAaZ3xW.htm)|Pharyngeal Jaws|auto-trad|
|[Sj3qaBkh1exbNXJY.htm](pathfinder-bestiary-items/Sj3qaBkh1exbNXJY.htm)|Dagger|auto-trad|
|[sj5zKlTpK679kiLP.htm](pathfinder-bestiary-items/sj5zKlTpK679kiLP.htm)|Constant Spells|auto-trad|
|[sJEzuYy6exvJPlpq.htm](pathfinder-bestiary-items/sJEzuYy6exvJPlpq.htm)|Fangs|auto-trad|
|[Sjf1HpLuxn9U6lr2.htm](pathfinder-bestiary-items/Sjf1HpLuxn9U6lr2.htm)|Improved Grab|auto-trad|
|[SJraLZmz7x1C04ki.htm](pathfinder-bestiary-items/SJraLZmz7x1C04ki.htm)|Vulnerable to Rust|auto-trad|
|[SjtL4vaiiGNkeXDm.htm](pathfinder-bestiary-items/SjtL4vaiiGNkeXDm.htm)|Grab|auto-trad|
|[SksyTvUlilzyBVLq.htm](pathfinder-bestiary-items/SksyTvUlilzyBVLq.htm)|Tail|auto-trad|
|[slCMgd2stlwxBKdg.htm](pathfinder-bestiary-items/slCMgd2stlwxBKdg.htm)|Darkvision|auto-trad|
|[SlsCmJyWx3dgIo2X.htm](pathfinder-bestiary-items/SlsCmJyWx3dgIo2X.htm)|Knockdown|auto-trad|
|[sm8H18uHC5Td5p0Q.htm](pathfinder-bestiary-items/sm8H18uHC5Td5p0Q.htm)|At-Will Spells|auto-trad|
|[smEk0yh1Pe95Nvsc.htm](pathfinder-bestiary-items/smEk0yh1Pe95Nvsc.htm)|Natural Invisibility|auto-trad|
|[smLfVxCrxxL5qnzw.htm](pathfinder-bestiary-items/smLfVxCrxxL5qnzw.htm)|Divine Prepared Spells|auto-trad|
|[SmRyQEGPbRIR82tT.htm](pathfinder-bestiary-items/SmRyQEGPbRIR82tT.htm)|Tsunami Jet|auto-trad|
|[SNb64KWDBMrTorTI.htm](pathfinder-bestiary-items/SNb64KWDBMrTorTI.htm)|Jaws|auto-trad|
|[SnCGhLws82aS8xpS.htm](pathfinder-bestiary-items/SnCGhLws82aS8xpS.htm)|Jaws|auto-trad|
|[SNJ0RSCREyP9YYKY.htm](pathfinder-bestiary-items/SNJ0RSCREyP9YYKY.htm)|Infernal Wound|auto-trad|
|[snOrUVs5f8v8gtuP.htm](pathfinder-bestiary-items/snOrUVs5f8v8gtuP.htm)|Graveknight's Curse|auto-trad|
|[sNxY0Neqlt313iVL.htm](pathfinder-bestiary-items/sNxY0Neqlt313iVL.htm)|Change Shape|auto-trad|
|[so5dV8CQ2KVtaKsR.htm](pathfinder-bestiary-items/so5dV8CQ2KVtaKsR.htm)|Darkvision|auto-trad|
|[soOhuJxyNNP6AHYc.htm](pathfinder-bestiary-items/soOhuJxyNNP6AHYc.htm)|Flame Maw|auto-trad|
|[SoUCD4VSzt7y6NH9.htm](pathfinder-bestiary-items/SoUCD4VSzt7y6NH9.htm)|At-Will Spells|auto-trad|
|[spBsMqVOA8rbhE2e.htm](pathfinder-bestiary-items/spBsMqVOA8rbhE2e.htm)|Aura of Corruption|auto-trad|
|[sPdAzqQnoaixFizq.htm](pathfinder-bestiary-items/sPdAzqQnoaixFizq.htm)|Attack of Opportunity|auto-trad|
|[SPTwJf1wzeADk4sn.htm](pathfinder-bestiary-items/SPTwJf1wzeADk4sn.htm)|Stinger|auto-trad|
|[sqceAJWlpdKGnHvx.htm](pathfinder-bestiary-items/sqceAJWlpdKGnHvx.htm)|Talon|auto-trad|
|[sqpUsCajoWHMhg1o.htm](pathfinder-bestiary-items/sqpUsCajoWHMhg1o.htm)|Rush|auto-trad|
|[sqZIq7wiLeITXB6q.htm](pathfinder-bestiary-items/sqZIq7wiLeITXB6q.htm)|Azure Worm Venom|auto-trad|
|[sr3LyPHlpXyJIW1Z.htm](pathfinder-bestiary-items/sr3LyPHlpXyJIW1Z.htm)|Attack of Opportunity|auto-trad|
|[sRHkNxD3UkLsrrJp.htm](pathfinder-bestiary-items/sRHkNxD3UkLsrrJp.htm)|Darkvision|auto-trad|
|[SRv8Gjga6DHY4U8G.htm](pathfinder-bestiary-items/SRv8Gjga6DHY4U8G.htm)|Corrupt Water|auto-trad|
|[sS1tOJIttfEYCFaT.htm](pathfinder-bestiary-items/sS1tOJIttfEYCFaT.htm)|Disperse|auto-trad|
|[sSd80IXHhedyt6q5.htm](pathfinder-bestiary-items/sSd80IXHhedyt6q5.htm)|Dorsal Deflection|auto-trad|
|[SsJk8HtdVTC3HZUK.htm](pathfinder-bestiary-items/SsJk8HtdVTC3HZUK.htm)|Darkvision|auto-trad|
|[sslSR9PEr8cWdVop.htm](pathfinder-bestiary-items/sslSR9PEr8cWdVop.htm)|Steal Shadow|auto-trad|
|[ST2LDOW64VO0qAVl.htm](pathfinder-bestiary-items/ST2LDOW64VO0qAVl.htm)|Pounce|auto-trad|
|[sT4lUsg0seVtpUKQ.htm](pathfinder-bestiary-items/sT4lUsg0seVtpUKQ.htm)|Claw|auto-trad|
|[sTnyQQeOWiyHDhNt.htm](pathfinder-bestiary-items/sTnyQQeOWiyHDhNt.htm)|Guardian Naga Venom|auto-trad|
|[svdaEp5rN3vr0zjk.htm](pathfinder-bestiary-items/svdaEp5rN3vr0zjk.htm)|Attack of Opportunity|auto-trad|
|[svh3YWWgNYDh2wu5.htm](pathfinder-bestiary-items/svh3YWWgNYDh2wu5.htm)|Stone Longsword|auto-trad|
|[sW7nJD5OXK3Qshxd.htm](pathfinder-bestiary-items/sW7nJD5OXK3Qshxd.htm)|Low-Light Vision|auto-trad|
|[SWBrXQjy0PikJ1wd.htm](pathfinder-bestiary-items/SWBrXQjy0PikJ1wd.htm)|Wing|auto-trad|
|[swIJOeM7FXkgQV1H.htm](pathfinder-bestiary-items/swIJOeM7FXkgQV1H.htm)|Jaws|auto-trad|
|[SWo4EQT4exBUG1zV.htm](pathfinder-bestiary-items/SWo4EQT4exBUG1zV.htm)|Status Sight|auto-trad|
|[sX53J0I5Baihn9qO.htm](pathfinder-bestiary-items/sX53J0I5Baihn9qO.htm)|Claw|auto-trad|
|[sx9bfcjoYVEdb5Ra.htm](pathfinder-bestiary-items/sx9bfcjoYVEdb5Ra.htm)|Tail|auto-trad|
|[sylWtqbDKbIIyCtj.htm](pathfinder-bestiary-items/sylWtqbDKbIIyCtj.htm)|Fangs|auto-trad|
|[SyWmilYaLnrtDFVe.htm](pathfinder-bestiary-items/SyWmilYaLnrtDFVe.htm)|Darkvision|auto-trad|
|[sz9GGQOjyrluDIX3.htm](pathfinder-bestiary-items/sz9GGQOjyrluDIX3.htm)|Darkvision|auto-trad|
|[SZafYEjKmu9zC1XL.htm](pathfinder-bestiary-items/SZafYEjKmu9zC1XL.htm)|Spell Ambush|auto-trad|
|[SzaJgzszNSa3qiDA.htm](pathfinder-bestiary-items/SzaJgzszNSa3qiDA.htm)|Site Bound|auto-trad|
|[SzfxfFohnnwRyEeK.htm](pathfinder-bestiary-items/SzfxfFohnnwRyEeK.htm)|Glutton's Feast|auto-trad|
|[SzReeoNa9jYsialH.htm](pathfinder-bestiary-items/SzReeoNa9jYsialH.htm)|Inexorable|auto-trad|
|[T0L16oJLdZYthupK.htm](pathfinder-bestiary-items/T0L16oJLdZYthupK.htm)|Horns|auto-trad|
|[t0slPYAl6mytNXG7.htm](pathfinder-bestiary-items/t0slPYAl6mytNXG7.htm)|Vortex|auto-trad|
|[T108hwN1OhACYQdQ.htm](pathfinder-bestiary-items/T108hwN1OhACYQdQ.htm)|Attack of Opportunity|auto-trad|
|[t1G2FbWy88tYUk7w.htm](pathfinder-bestiary-items/t1G2FbWy88tYUk7w.htm)|Crimson Worm Venom|auto-trad|
|[t1tq9YiGmfqQQV5C.htm](pathfinder-bestiary-items/t1tq9YiGmfqQQV5C.htm)|Composite Shortbow|auto-trad|
|[T1WSPSyMbIh6gzFQ.htm](pathfinder-bestiary-items/T1WSPSyMbIh6gzFQ.htm)|Catch Rock|auto-trad|
|[T2hVNG1lQgAAAy70.htm](pathfinder-bestiary-items/T2hVNG1lQgAAAy70.htm)|Swallow Whole|auto-trad|
|[t2Yd0tHatTjeSUcB.htm](pathfinder-bestiary-items/t2Yd0tHatTjeSUcB.htm)|Dagger|auto-trad|
|[t3USaq2CKWLoaP9A.htm](pathfinder-bestiary-items/t3USaq2CKWLoaP9A.htm)|Regeneration 50 (Deactivated by Good)|auto-trad|
|[t4Gnn9gqqcKWerhM.htm](pathfinder-bestiary-items/t4Gnn9gqqcKWerhM.htm)|Thorny Mass|auto-trad|
|[T4pc6k7hdxLa8syv.htm](pathfinder-bestiary-items/T4pc6k7hdxLa8syv.htm)|Jaws|auto-trad|
|[T50Unm8nAUbNCLxu.htm](pathfinder-bestiary-items/T50Unm8nAUbNCLxu.htm)|Fist|auto-trad|
|[T56PSO8KBNKDvOCc.htm](pathfinder-bestiary-items/T56PSO8KBNKDvOCc.htm)|Light Ray|auto-trad|
|[T5f6H4ZAcl4EmG7i.htm](pathfinder-bestiary-items/T5f6H4ZAcl4EmG7i.htm)|Longsword|auto-trad|
|[t5Pe2D0CJm0NY3E4.htm](pathfinder-bestiary-items/t5Pe2D0CJm0NY3E4.htm)|Claw|auto-trad|
|[T6CjPHrd4XMeWpiE.htm](pathfinder-bestiary-items/T6CjPHrd4XMeWpiE.htm)|Darkvision|auto-trad|
|[t6KHJCiZXdNJnqxB.htm](pathfinder-bestiary-items/t6KHJCiZXdNJnqxB.htm)|Weapon Master|auto-trad|
|[T8jQXV4jxIhvfS14.htm](pathfinder-bestiary-items/T8jQXV4jxIhvfS14.htm)|Nimble Dodge|auto-trad|
|[t8uaryivfTb4jrjH.htm](pathfinder-bestiary-items/t8uaryivfTb4jrjH.htm)|Leaf|auto-trad|
|[t98B5FOZI1a1Ye46.htm](pathfinder-bestiary-items/t98B5FOZI1a1Ye46.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[TaJezoHLHAlyZmLU.htm](pathfinder-bestiary-items/TaJezoHLHAlyZmLU.htm)|Binding Contract|auto-trad|
|[TAr3uDoat4fR02qp.htm](pathfinder-bestiary-items/TAr3uDoat4fR02qp.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[tBCaXMwzwsRxvg1U.htm](pathfinder-bestiary-items/tBCaXMwzwsRxvg1U.htm)|Dagger|auto-trad|
|[TbFZxJr8aSgbgTZ6.htm](pathfinder-bestiary-items/TbFZxJr8aSgbgTZ6.htm)|Double Bite|auto-trad|
|[TBlBPLZKvFBHjjgU.htm](pathfinder-bestiary-items/TBlBPLZKvFBHjjgU.htm)|Slow|auto-trad|
|[TBlcp9L0tEkA3jyF.htm](pathfinder-bestiary-items/TBlcp9L0tEkA3jyF.htm)|Double Slice|auto-trad|
|[Tc806odnsrBWGupY.htm](pathfinder-bestiary-items/Tc806odnsrBWGupY.htm)|Claw|auto-trad|
|[tcjS7ZEu6Fppiy3z.htm](pathfinder-bestiary-items/tcjS7ZEu6Fppiy3z.htm)|Grab|auto-trad|
|[TCZQdv29obsz4Rie.htm](pathfinder-bestiary-items/TCZQdv29obsz4Rie.htm)|Low-Light Vision|auto-trad|
|[td7P2EcI7SbN65De.htm](pathfinder-bestiary-items/td7P2EcI7SbN65De.htm)|Tail|auto-trad|
|[Tdu73gvdkQpD4ZFK.htm](pathfinder-bestiary-items/Tdu73gvdkQpD4ZFK.htm)|Arcane Prepared Spells|auto-trad|
|[tDvcon2bK9xEGh90.htm](pathfinder-bestiary-items/tDvcon2bK9xEGh90.htm)|Deadly Throw|auto-trad|
|[tdX5F9iFFF0sboNA.htm](pathfinder-bestiary-items/tdX5F9iFFF0sboNA.htm)|Heavy Crossbow|auto-trad|
|[TEn7gmE6LHEqbMLJ.htm](pathfinder-bestiary-items/TEn7gmE6LHEqbMLJ.htm)|Daeodon Charge|auto-trad|
|[tEPc3Xc3Pufff6D5.htm](pathfinder-bestiary-items/tEPc3Xc3Pufff6D5.htm)|Drench|auto-trad|
|[Tf3oOPGPRrko1wvW.htm](pathfinder-bestiary-items/Tf3oOPGPRrko1wvW.htm)|Entangling Slime|auto-trad|
|[TfNCbBu8oaMSaeTO.htm](pathfinder-bestiary-items/TfNCbBu8oaMSaeTO.htm)|Claw|auto-trad|
|[tFRnP7KsOLmYb44P.htm](pathfinder-bestiary-items/tFRnP7KsOLmYb44P.htm)|Archon's Door|auto-trad|
|[TfSlL9Vupc8WC8jy.htm](pathfinder-bestiary-items/TfSlL9Vupc8WC8jy.htm)|Buck|auto-trad|
|[TfUyAj18NAaa1ZGV.htm](pathfinder-bestiary-items/TfUyAj18NAaa1ZGV.htm)|Ice Linnorm Venom|auto-trad|
|[tFYFPReEO8BIA94l.htm](pathfinder-bestiary-items/tFYFPReEO8BIA94l.htm)|Darkvision|auto-trad|
|[Tg3cDQ85HTqlUqQV.htm](pathfinder-bestiary-items/Tg3cDQ85HTqlUqQV.htm)|Claw|auto-trad|
|[tGAFUCC4Emx6CfMD.htm](pathfinder-bestiary-items/tGAFUCC4Emx6CfMD.htm)|Entropy Sense (Imprecise) 30 feet|auto-trad|
|[TGIpZcekBlTtMIW2.htm](pathfinder-bestiary-items/TGIpZcekBlTtMIW2.htm)|Massive Rush|auto-trad|
|[TGYOp1PUpIUNK6f9.htm](pathfinder-bestiary-items/TGYOp1PUpIUNK6f9.htm)|Darkvision|auto-trad|
|[tHBnPDgoj9trVlBk.htm](pathfinder-bestiary-items/tHBnPDgoj9trVlBk.htm)|Descend on a Web|auto-trad|
|[THputvyP8JOKYysA.htm](pathfinder-bestiary-items/THputvyP8JOKYysA.htm)|Dagger|auto-trad|
|[thy0OpcI6dFq3W3j.htm](pathfinder-bestiary-items/thy0OpcI6dFq3W3j.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[tiCwm2g4h6CfLncH.htm](pathfinder-bestiary-items/tiCwm2g4h6CfLncH.htm)|Jaws|auto-trad|
|[TIGvNHFixJK8sJCg.htm](pathfinder-bestiary-items/TIGvNHFixJK8sJCg.htm)|Low-Light Vision|auto-trad|
|[tiL9cpK0I2gzpQne.htm](pathfinder-bestiary-items/tiL9cpK0I2gzpQne.htm)|Attack of Opportunity|auto-trad|
|[Tj3MRsA6XVrZ9zQg.htm](pathfinder-bestiary-items/Tj3MRsA6XVrZ9zQg.htm)|Jaws|auto-trad|
|[tJF3MvlHvCalGkXk.htm](pathfinder-bestiary-items/tJF3MvlHvCalGkXk.htm)|Horns|auto-trad|
|[Tji1ev92INkjtI3U.htm](pathfinder-bestiary-items/Tji1ev92INkjtI3U.htm)|Smoke Vision|auto-trad|
|[TjOmyfilNvHKI58M.htm](pathfinder-bestiary-items/TjOmyfilNvHKI58M.htm)|Children of the Night|auto-trad|
|[tjX3QfENUkKVi1tn.htm](pathfinder-bestiary-items/tjX3QfENUkKVi1tn.htm)|Darkvision|auto-trad|
|[Tk7abWUpvDDV6Qg0.htm](pathfinder-bestiary-items/Tk7abWUpvDDV6Qg0.htm)|Succor Vulnerability|auto-trad|
|[TKaqHg2KqjNmg24t.htm](pathfinder-bestiary-items/TKaqHg2KqjNmg24t.htm)|Improved Grab|auto-trad|
|[TKC7njhX3a3mq4eY.htm](pathfinder-bestiary-items/TKC7njhX3a3mq4eY.htm)|Negative Healing|auto-trad|
|[TkDV0wPhRzYy8sii.htm](pathfinder-bestiary-items/TkDV0wPhRzYy8sii.htm)|Tremorsense (Imprecise) 120 feet|auto-trad|
|[tKTCH6a5g78miSHu.htm](pathfinder-bestiary-items/tKTCH6a5g78miSHu.htm)|Commander's Aura|auto-trad|
|[TLRgHlg3hxjekxoB.htm](pathfinder-bestiary-items/TLRgHlg3hxjekxoB.htm)|Claw|auto-trad|
|[tMGdCYYP6CA506om.htm](pathfinder-bestiary-items/tMGdCYYP6CA506om.htm)|Morphic Hands|auto-trad|
|[tMZ3S5BdHrl0iX7R.htm](pathfinder-bestiary-items/tMZ3S5BdHrl0iX7R.htm)|Fist|auto-trad|
|[TN0QOVJVRLV73CWR.htm](pathfinder-bestiary-items/TN0QOVJVRLV73CWR.htm)|Fist|auto-trad|
|[TnaXi3v3iBDjotwd.htm](pathfinder-bestiary-items/TnaXi3v3iBDjotwd.htm)|Viper Venom|auto-trad|
|[tNVPutTbdl3sPaGf.htm](pathfinder-bestiary-items/tNVPutTbdl3sPaGf.htm)|Death Flare|auto-trad|
|[tOCS9mkYR5K1LuOZ.htm](pathfinder-bestiary-items/tOCS9mkYR5K1LuOZ.htm)|Petrifying Gaze|auto-trad|
|[TOGa0vvktQjKmJ7K.htm](pathfinder-bestiary-items/TOGa0vvktQjKmJ7K.htm)|Darkvision|auto-trad|
|[TpEz65tv1dwJ91cy.htm](pathfinder-bestiary-items/TpEz65tv1dwJ91cy.htm)|Alchemical Chambers|auto-trad|
|[tphsLfLbrYV16pth.htm](pathfinder-bestiary-items/tphsLfLbrYV16pth.htm)|Inexorable|auto-trad|
|[TpHVJUZhgcIfmmBv.htm](pathfinder-bestiary-items/TpHVJUZhgcIfmmBv.htm)|Draconic Frenzy|auto-trad|
|[TQ0FOP3X7LrgaS6J.htm](pathfinder-bestiary-items/TQ0FOP3X7LrgaS6J.htm)|Ferocity|auto-trad|
|[tQ1m9uzdwC96pkCY.htm](pathfinder-bestiary-items/tQ1m9uzdwC96pkCY.htm)|Hydraulic Asphyxiation|auto-trad|
|[tq3HesFWx5P7xPWl.htm](pathfinder-bestiary-items/tq3HesFWx5P7xPWl.htm)|Vulnerable to Shatter|auto-trad|
|[TQcjKBbkOHwvxvhN.htm](pathfinder-bestiary-items/TQcjKBbkOHwvxvhN.htm)|Draconic Momentum|auto-trad|
|[tqL5vKXpeLAzq2JR.htm](pathfinder-bestiary-items/tqL5vKXpeLAzq2JR.htm)|Frightful Presence|auto-trad|
|[TRcRCZ4v7cyZtfSF.htm](pathfinder-bestiary-items/TRcRCZ4v7cyZtfSF.htm)|Sandstorm Wrath|auto-trad|
|[tRvUNo3EA7WVnZc5.htm](pathfinder-bestiary-items/tRvUNo3EA7WVnZc5.htm)|Frightful Presence|auto-trad|
|[TS7iVUZNmP0Szdfh.htm](pathfinder-bestiary-items/TS7iVUZNmP0Szdfh.htm)|Javelin|auto-trad|
|[TsCewvMqGGAfetXh.htm](pathfinder-bestiary-items/TsCewvMqGGAfetXh.htm)|Trident|auto-trad|
|[tsqJ5ZQfUx0W3mvO.htm](pathfinder-bestiary-items/tsqJ5ZQfUx0W3mvO.htm)|Darkvision|auto-trad|
|[TtcFIJZjrZ4Vaaml.htm](pathfinder-bestiary-items/TtcFIJZjrZ4Vaaml.htm)|Slow|auto-trad|
|[ttFxkO2qOIlHaSJc.htm](pathfinder-bestiary-items/ttFxkO2qOIlHaSJc.htm)|Arcane Innate Spells|auto-trad|
|[TTlpPh49DdSgaBxG.htm](pathfinder-bestiary-items/TTlpPh49DdSgaBxG.htm)|Horns|auto-trad|
|[TTpnkaK9B6Lsytya.htm](pathfinder-bestiary-items/TTpnkaK9B6Lsytya.htm)|Rat Empathy|auto-trad|
|[TuTut6BPwHgd3Xp7.htm](pathfinder-bestiary-items/TuTut6BPwHgd3Xp7.htm)|Emotional Focus|auto-trad|
|[tV18raweChPRFzPo.htm](pathfinder-bestiary-items/tV18raweChPRFzPo.htm)|Claw|auto-trad|
|[tvFxmbJKPxnCnTbb.htm](pathfinder-bestiary-items/tvFxmbJKPxnCnTbb.htm)|Vermin Empathy|auto-trad|
|[TVLAYoQrAgNovOAr.htm](pathfinder-bestiary-items/TVLAYoQrAgNovOAr.htm)|Debris|auto-trad|
|[tvnn3d6VCprLkoQ2.htm](pathfinder-bestiary-items/tvnn3d6VCprLkoQ2.htm)|Fist|auto-trad|
|[TvxcP8kfqmDOK1GX.htm](pathfinder-bestiary-items/TvxcP8kfqmDOK1GX.htm)|Low-Light Vision|auto-trad|
|[TWiy088AD8uOCDGQ.htm](pathfinder-bestiary-items/TWiy088AD8uOCDGQ.htm)|Speed Surge|auto-trad|
|[TwSJCzLZ4KcJKaWT.htm](pathfinder-bestiary-items/TwSJCzLZ4KcJKaWT.htm)|Breath Weapon|auto-trad|
|[TXy3zvlvlJECNrWn.htm](pathfinder-bestiary-items/TXy3zvlvlJECNrWn.htm)|Shell Block|auto-trad|
|[Tyan53wuqYSDd7rE.htm](pathfinder-bestiary-items/Tyan53wuqYSDd7rE.htm)|Tentacle|auto-trad|
|[TYcAOZDcSQlHGVZx.htm](pathfinder-bestiary-items/TYcAOZDcSQlHGVZx.htm)|Draconic Frenzy|auto-trad|
|[TyHmjC5wvxo29J5L.htm](pathfinder-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|auto-trad|
|[tyjUqs7S8mA70wiw.htm](pathfinder-bestiary-items/tyjUqs7S8mA70wiw.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[TyonmqwWHejYuBAN.htm](pathfinder-bestiary-items/TyonmqwWHejYuBAN.htm)|Dervish Strike|auto-trad|
|[tZ39PkV2oWLySkOs.htm](pathfinder-bestiary-items/tZ39PkV2oWLySkOs.htm)|Swarming Stings|auto-trad|
|[tz3Jv4IQgO8ygZjo.htm](pathfinder-bestiary-items/tz3Jv4IQgO8ygZjo.htm)|Spray Acid|auto-trad|
|[TzBbRiqiSDZj6VdK.htm](pathfinder-bestiary-items/TzBbRiqiSDZj6VdK.htm)|Death Strike|auto-trad|
|[tzGgSX4dpcTwYYag.htm](pathfinder-bestiary-items/tzGgSX4dpcTwYYag.htm)|Draconic Momentum|auto-trad|
|[TziDQnLmJx7Ty2df.htm](pathfinder-bestiary-items/TziDQnLmJx7Ty2df.htm)|Primal Innate Spells|auto-trad|
|[tzsc7ldZ2eR2z0XM.htm](pathfinder-bestiary-items/tzsc7ldZ2eR2z0XM.htm)|Regeneration 30 (Deactivated by Good)|auto-trad|
|[u0GbWrE4hwJyEA08.htm](pathfinder-bestiary-items/u0GbWrE4hwJyEA08.htm)|Hunk Of Meat|auto-trad|
|[u0WWqexz5TB2gEqn.htm](pathfinder-bestiary-items/u0WWqexz5TB2gEqn.htm)|Claws|auto-trad|
|[u1eOXRbLwSvouuEB.htm](pathfinder-bestiary-items/u1eOXRbLwSvouuEB.htm)|Occult Innate Spells|auto-trad|
|[U1ORlKZKu61bng75.htm](pathfinder-bestiary-items/U1ORlKZKu61bng75.htm)|Greater Darkvision|auto-trad|
|[U1y2WggaYWJc6YHr.htm](pathfinder-bestiary-items/U1y2WggaYWJc6YHr.htm)|Horns|auto-trad|
|[u3YnULJ9YqMBkD6D.htm](pathfinder-bestiary-items/u3YnULJ9YqMBkD6D.htm)|Wave|auto-trad|
|[u59sSnB7GuFn9ofv.htm](pathfinder-bestiary-items/u59sSnB7GuFn9ofv.htm)|Shark Commune 150 feet|auto-trad|
|[u5YlFZyly3oBsknm.htm](pathfinder-bestiary-items/u5YlFZyly3oBsknm.htm)|Darkvision|auto-trad|
|[u5ZGQOwAVraOXVuo.htm](pathfinder-bestiary-items/u5ZGQOwAVraOXVuo.htm)|Compression|auto-trad|
|[u71Wtb03wVLdrSUO.htm](pathfinder-bestiary-items/u71Wtb03wVLdrSUO.htm)|Truespeech|auto-trad|
|[U7gmP0tQ6eQor3xT.htm](pathfinder-bestiary-items/U7gmP0tQ6eQor3xT.htm)|Fist|auto-trad|
|[u9SS7QpWokulA8Af.htm](pathfinder-bestiary-items/u9SS7QpWokulA8Af.htm)|Jaws|auto-trad|
|[u9tY2pkbZxNzmxoe.htm](pathfinder-bestiary-items/u9tY2pkbZxNzmxoe.htm)|Claw|auto-trad|
|[UakPnn8jYjY9gCdo.htm](pathfinder-bestiary-items/UakPnn8jYjY9gCdo.htm)|Rope|auto-trad|
|[UB2ChrMM1B3cwlo2.htm](pathfinder-bestiary-items/UB2ChrMM1B3cwlo2.htm)|Darkvision|auto-trad|
|[uB2gaJscmmSbNNVZ.htm](pathfinder-bestiary-items/uB2gaJscmmSbNNVZ.htm)|Shortsword|auto-trad|
|[ubptAi7h2I8spy2V.htm](pathfinder-bestiary-items/ubptAi7h2I8spy2V.htm)|Darkvision|auto-trad|
|[uC55H6tJNTniSzuC.htm](pathfinder-bestiary-items/uC55H6tJNTniSzuC.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[UC8gJQCeqcEKOOfI.htm](pathfinder-bestiary-items/UC8gJQCeqcEKOOfI.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[uCBBqgflwHqvZeg5.htm](pathfinder-bestiary-items/uCBBqgflwHqvZeg5.htm)|At-Will Spells|auto-trad|
|[UCRM8G3tekG92tUj.htm](pathfinder-bestiary-items/UCRM8G3tekG92tUj.htm)|Hoof|auto-trad|
|[uCvZSYozUXxyyf1b.htm](pathfinder-bestiary-items/uCvZSYozUXxyyf1b.htm)|Death's Grace|auto-trad|
|[uD9MleR0PzjUXugC.htm](pathfinder-bestiary-items/uD9MleR0PzjUXugC.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[uDEIHmI6TZbQLzHX.htm](pathfinder-bestiary-items/uDEIHmI6TZbQLzHX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[UDSpeDfwP5gNIdMY.htm](pathfinder-bestiary-items/UDSpeDfwP5gNIdMY.htm)|Dagger|auto-trad|
|[uEjliLhQWBsVYCsE.htm](pathfinder-bestiary-items/uEjliLhQWBsVYCsE.htm)|Maul|auto-trad|
|[UemgQLIDasOGrvNC.htm](pathfinder-bestiary-items/UemgQLIDasOGrvNC.htm)|Archon's Door|auto-trad|
|[ufap97U4FyUOrJZF.htm](pathfinder-bestiary-items/ufap97U4FyUOrJZF.htm)|Light Blindness|auto-trad|
|[UfLxn8BgeR2BOqee.htm](pathfinder-bestiary-items/UfLxn8BgeR2BOqee.htm)|Fist|auto-trad|
|[UfQd4obnmoY6nVDf.htm](pathfinder-bestiary-items/UfQd4obnmoY6nVDf.htm)|Fist|auto-trad|
|[uFWe6jAjZ0J95ox9.htm](pathfinder-bestiary-items/uFWe6jAjZ0J95ox9.htm)|Battle Axe|auto-trad|
|[uGne7svJszeghy3d.htm](pathfinder-bestiary-items/uGne7svJszeghy3d.htm)|Plaguesense 60 feet|auto-trad|
|[uGSBKzZHNc69P1ub.htm](pathfinder-bestiary-items/uGSBKzZHNc69P1ub.htm)|Telepathy 1 mile|auto-trad|
|[ugytuxGqE0wsYOff.htm](pathfinder-bestiary-items/ugytuxGqE0wsYOff.htm)|Claw|auto-trad|
|[UgyxwE6dj48w1zhM.htm](pathfinder-bestiary-items/UgyxwE6dj48w1zhM.htm)|Retributive Strike|auto-trad|
|[uh1IYK2wwc48sFKE.htm](pathfinder-bestiary-items/uh1IYK2wwc48sFKE.htm)|Low-Light Vision|auto-trad|
|[Uhi5bxlJXwqkbpIT.htm](pathfinder-bestiary-items/Uhi5bxlJXwqkbpIT.htm)|Fist|auto-trad|
|[UhtLIPQpF9818xr2.htm](pathfinder-bestiary-items/UhtLIPQpF9818xr2.htm)|Darkvision|auto-trad|
|[UHxzFiyQ9sL32HBv.htm](pathfinder-bestiary-items/UHxzFiyQ9sL32HBv.htm)|Frightful Presence|auto-trad|
|[ui6XZl3S0T45fJFu.htm](pathfinder-bestiary-items/ui6XZl3S0T45fJFu.htm)|Claw|auto-trad|
|[uIMjt4HR6GpJ6PS1.htm](pathfinder-bestiary-items/uIMjt4HR6GpJ6PS1.htm)|Fangs|auto-trad|
|[UipChONJlSGScaQZ.htm](pathfinder-bestiary-items/UipChONJlSGScaQZ.htm)|Sneak Attack|auto-trad|
|[uj64Am9tI0929tfx.htm](pathfinder-bestiary-items/uj64Am9tI0929tfx.htm)|Arcane Innate Spells|auto-trad|
|[UJlQ2v4Mw6QsmvJY.htm](pathfinder-bestiary-items/UJlQ2v4Mw6QsmvJY.htm)|Holy Warhammer|auto-trad|
|[UJUSN0sChxOuRWw8.htm](pathfinder-bestiary-items/UJUSN0sChxOuRWw8.htm)|Infuse Weapon|auto-trad|
|[UK0INZ5G8eq5ytop.htm](pathfinder-bestiary-items/UK0INZ5G8eq5ytop.htm)|Darkvision|auto-trad|
|[uK5eIAdOWiayEgWr.htm](pathfinder-bestiary-items/uK5eIAdOWiayEgWr.htm)|Light Ray|auto-trad|
|[UKfFDeN7IDKW1okj.htm](pathfinder-bestiary-items/UKfFDeN7IDKW1okj.htm)|Dancer's Curse|auto-trad|
|[UKNPyXkarfzkXTHO.htm](pathfinder-bestiary-items/UKNPyXkarfzkXTHO.htm)|Longspear|auto-trad|
|[UkuRoGDf37QBLm8k.htm](pathfinder-bestiary-items/UkuRoGDf37QBLm8k.htm)|Ice Stride|auto-trad|
|[uldVZLVdwiLD1q6v.htm](pathfinder-bestiary-items/uldVZLVdwiLD1q6v.htm)|Vulnerable to Sunlight|auto-trad|
|[uLEPJWbbXAY2E4KJ.htm](pathfinder-bestiary-items/uLEPJWbbXAY2E4KJ.htm)|Push 5 feet|auto-trad|
|[uLf1dk5gMycZYjBy.htm](pathfinder-bestiary-items/uLf1dk5gMycZYjBy.htm)|+2 to All Saves vs. Grapple or Shove|auto-trad|
|[uLu0KGwcUUj5bvhl.htm](pathfinder-bestiary-items/uLu0KGwcUUj5bvhl.htm)|Darkvision|auto-trad|
|[UluC5rRvQl0yriAj.htm](pathfinder-bestiary-items/UluC5rRvQl0yriAj.htm)|Divine Innate Spells|auto-trad|
|[UmeNuzExQLQPFzTE.htm](pathfinder-bestiary-items/UmeNuzExQLQPFzTE.htm)|Guarded Thoughts|auto-trad|
|[Umi21NTMrg5JA3IQ.htm](pathfinder-bestiary-items/Umi21NTMrg5JA3IQ.htm)|Wight Spawn|auto-trad|
|[umShxoDTXECH3WCs.htm](pathfinder-bestiary-items/umShxoDTXECH3WCs.htm)|Snow Vision|auto-trad|
|[UMX8GkJ4L6G9UQGO.htm](pathfinder-bestiary-items/UMX8GkJ4L6G9UQGO.htm)|Darkvision|auto-trad|
|[UnFKmdjf3kYOmNvW.htm](pathfinder-bestiary-items/UnFKmdjf3kYOmNvW.htm)|Natural Cunning|auto-trad|
|[uoJMsbcXlm58cnei.htm](pathfinder-bestiary-items/uoJMsbcXlm58cnei.htm)|Longsword|auto-trad|
|[UoKQ5xYxwkzldwhR.htm](pathfinder-bestiary-items/UoKQ5xYxwkzldwhR.htm)|Constant Spells|auto-trad|
|[uOtLx35G2OFWHztX.htm](pathfinder-bestiary-items/uOtLx35G2OFWHztX.htm)|Tentacle Transfer|auto-trad|
|[UP6X4AexjR3Xwwq4.htm](pathfinder-bestiary-items/UP6X4AexjR3Xwwq4.htm)|Darkvision|auto-trad|
|[up8Xcig2FrwgbFfb.htm](pathfinder-bestiary-items/up8Xcig2FrwgbFfb.htm)|Terrifying Croak|auto-trad|
|[uPAAssuXj8CktZTI.htm](pathfinder-bestiary-items/uPAAssuXj8CktZTI.htm)|Push 40 feet|auto-trad|
|[upJaEvHTKGpsW57H.htm](pathfinder-bestiary-items/upJaEvHTKGpsW57H.htm)|Flaming Longsword|auto-trad|
|[uPjMU5WZPboQnmTu.htm](pathfinder-bestiary-items/uPjMU5WZPboQnmTu.htm)|Arcane Prepared Spells|auto-trad|
|[uPrkt0x7Qt0nbh7U.htm](pathfinder-bestiary-items/uPrkt0x7Qt0nbh7U.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[UpXcSbQhJBN069fE.htm](pathfinder-bestiary-items/UpXcSbQhJBN069fE.htm)|Horn|auto-trad|
|[UQ7yTknJYCFghTF4.htm](pathfinder-bestiary-items/UQ7yTknJYCFghTF4.htm)|+2 to Will Saves vs. Emotion|auto-trad|
|[uqfegosZFIHA1J7G.htm](pathfinder-bestiary-items/uqfegosZFIHA1J7G.htm)|Low-Light Vision|auto-trad|
|[uQywdK0j1lggScR1.htm](pathfinder-bestiary-items/uQywdK0j1lggScR1.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[uR0sxFBmOFXxYqZi.htm](pathfinder-bestiary-items/uR0sxFBmOFXxYqZi.htm)|Claw|auto-trad|
|[urDSEWopXr5z3krl.htm](pathfinder-bestiary-items/urDSEWopXr5z3krl.htm)|Attack of Opportunity (Stinger Only)|auto-trad|
|[uRZywk7870Tfgxnp.htm](pathfinder-bestiary-items/uRZywk7870Tfgxnp.htm)|Axe Vulnerability|auto-trad|
|[UsbbZtRGFg0fzukO.htm](pathfinder-bestiary-items/UsbbZtRGFg0fzukO.htm)|Composite Longbow|auto-trad|
|[USCoVo5LJNs12zB4.htm](pathfinder-bestiary-items/USCoVo5LJNs12zB4.htm)|Go Dark|auto-trad|
|[UsE3E2jD964UvWQw.htm](pathfinder-bestiary-items/UsE3E2jD964UvWQw.htm)|Arcane Innate Spells|auto-trad|
|[uss6H3otq5ggwFWb.htm](pathfinder-bestiary-items/uss6H3otq5ggwFWb.htm)|Tremorsense (Imprecise) 80 feet|auto-trad|
|[uT4KswZKvsTixIVs.htm](pathfinder-bestiary-items/uT4KswZKvsTixIVs.htm)|Improved Grab|auto-trad|
|[uT8zq93Vx5vOdzkD.htm](pathfinder-bestiary-items/uT8zq93Vx5vOdzkD.htm)|Breath Weapon|auto-trad|
|[uTobNtEZnb13t0PA.htm](pathfinder-bestiary-items/uTobNtEZnb13t0PA.htm)|Choose Weakness|auto-trad|
|[utu2oawY1G8K7q6g.htm](pathfinder-bestiary-items/utu2oawY1G8K7q6g.htm)|Darkvision|auto-trad|
|[UufKTkmVCcXkrBVb.htm](pathfinder-bestiary-items/UufKTkmVCcXkrBVb.htm)|Wing Thrash|auto-trad|
|[uuI4sId0T1tDVCla.htm](pathfinder-bestiary-items/uuI4sId0T1tDVCla.htm)|Darkvision|auto-trad|
|[uuMoWLbelNwKPIUM.htm](pathfinder-bestiary-items/uuMoWLbelNwKPIUM.htm)|Armor-Rending|auto-trad|
|[UUnzaMbka2DY7uDu.htm](pathfinder-bestiary-items/UUnzaMbka2DY7uDu.htm)|Attack of Opportunity|auto-trad|
|[UupBqSBXKanyWoA6.htm](pathfinder-bestiary-items/UupBqSBXKanyWoA6.htm)|Dispelling Strike|auto-trad|
|[uv0UDH4jDwetNZol.htm](pathfinder-bestiary-items/uv0UDH4jDwetNZol.htm)|Primal Innate Spells|auto-trad|
|[UVFdUW8UmQmQjvIs.htm](pathfinder-bestiary-items/UVFdUW8UmQmQjvIs.htm)|Low-Light Vision|auto-trad|
|[UvKqCH4Y10AWYyTl.htm](pathfinder-bestiary-items/UvKqCH4Y10AWYyTl.htm)|Fist|auto-trad|
|[UVREJWrU0MjhSxLs.htm](pathfinder-bestiary-items/UVREJWrU0MjhSxLs.htm)|Protean Anatomy|auto-trad|
|[UVwLwFSrhe9YIUEC.htm](pathfinder-bestiary-items/UVwLwFSrhe9YIUEC.htm)|Tighten Coils|auto-trad|
|[uVyonJirqXV46pFg.htm](pathfinder-bestiary-items/uVyonJirqXV46pFg.htm)|Breath Weapon|auto-trad|
|[UW8qbPcbWR2N4HcI.htm](pathfinder-bestiary-items/UW8qbPcbWR2N4HcI.htm)|Darkvision|auto-trad|
|[UwaLPxPywao4fbN3.htm](pathfinder-bestiary-items/UwaLPxPywao4fbN3.htm)|At-Will Spells|auto-trad|
|[uwBAscSuJG489hpI.htm](pathfinder-bestiary-items/uwBAscSuJG489hpI.htm)|Command Giants|auto-trad|
|[UWcKX7DmlsSP933a.htm](pathfinder-bestiary-items/UWcKX7DmlsSP933a.htm)|Arcane Innate Spells|auto-trad|
|[Ux1Z45Ai509H8jm7.htm](pathfinder-bestiary-items/Ux1Z45Ai509H8jm7.htm)|Darkvision|auto-trad|
|[uXbNGzD4guadWLp9.htm](pathfinder-bestiary-items/uXbNGzD4guadWLp9.htm)|Longsword|auto-trad|
|[uyDcXzwKNRuKIzY5.htm](pathfinder-bestiary-items/uyDcXzwKNRuKIzY5.htm)|Blackaxe - Restoration|auto-trad|
|[UYnNMvrWFg5VBhO2.htm](pathfinder-bestiary-items/UYnNMvrWFg5VBhO2.htm)|Invisible|auto-trad|
|[uYQrEbjjw3XIQ1cO.htm](pathfinder-bestiary-items/uYQrEbjjw3XIQ1cO.htm)|Frightful Moan|auto-trad|
|[UYwBYJoDNWHyjeIa.htm](pathfinder-bestiary-items/UYwBYJoDNWHyjeIa.htm)|Spear|auto-trad|
|[uyX0PXYhDtx37aB0.htm](pathfinder-bestiary-items/uyX0PXYhDtx37aB0.htm)|Darkvision|auto-trad|
|[uZBUxDTyFWbpARud.htm](pathfinder-bestiary-items/uZBUxDTyFWbpARud.htm)|Frightful Presence|auto-trad|
|[uZdepeycCi1DBYTW.htm](pathfinder-bestiary-items/uZdepeycCi1DBYTW.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[Uzinvon7QqcS48NH.htm](pathfinder-bestiary-items/Uzinvon7QqcS48NH.htm)|Improved Push 20 feet|auto-trad|
|[uznmLfSkP5j9qOEA.htm](pathfinder-bestiary-items/uznmLfSkP5j9qOEA.htm)|Quick Draw|auto-trad|
|[UZp9NX0EnCBPGns5.htm](pathfinder-bestiary-items/UZp9NX0EnCBPGns5.htm)|Draconic Momentum|auto-trad|
|[uzwUjI3gD8tO8Chh.htm](pathfinder-bestiary-items/uzwUjI3gD8tO8Chh.htm)|Vulnerable to Flesh to Stone|auto-trad|
|[V0DnCsH65jBlStiD.htm](pathfinder-bestiary-items/V0DnCsH65jBlStiD.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[v0n7ntTptPAp7TPz.htm](pathfinder-bestiary-items/v0n7ntTptPAp7TPz.htm)|Brazier|auto-trad|
|[V1nFWeg7OxNxkntU.htm](pathfinder-bestiary-items/V1nFWeg7OxNxkntU.htm)|Dagger|auto-trad|
|[v27XHQrEZwrrEcBP.htm](pathfinder-bestiary-items/v27XHQrEZwrrEcBP.htm)|Constant Spells|auto-trad|
|[v2Ifd6tb1IDRFqFO.htm](pathfinder-bestiary-items/v2Ifd6tb1IDRFqFO.htm)|Drain Life|auto-trad|
|[v2MAVdLprfTUs4jh.htm](pathfinder-bestiary-items/v2MAVdLprfTUs4jh.htm)|Fist|auto-trad|
|[v2Vcuxvy5icZyHkj.htm](pathfinder-bestiary-items/v2Vcuxvy5icZyHkj.htm)|All-Around Vision|auto-trad|
|[V45K2AmQDPCGIMET.htm](pathfinder-bestiary-items/V45K2AmQDPCGIMET.htm)|Blood Scent|auto-trad|
|[V6anmpeGZ7JM0rNx.htm](pathfinder-bestiary-items/V6anmpeGZ7JM0rNx.htm)|Darkvision|auto-trad|
|[V6eYuUdSaU2MbSi5.htm](pathfinder-bestiary-items/V6eYuUdSaU2MbSi5.htm)|At-Will Spells|auto-trad|
|[V6Y4yCrWba6tmSL9.htm](pathfinder-bestiary-items/V6Y4yCrWba6tmSL9.htm)|Coven|auto-trad|
|[v77PLs3ZMXpMZOWj.htm](pathfinder-bestiary-items/v77PLs3ZMXpMZOWj.htm)|Go for the Eyes|auto-trad|
|[v7vwMkZTAxAiZu1m.htm](pathfinder-bestiary-items/v7vwMkZTAxAiZu1m.htm)|Constant Spells|auto-trad|
|[v8RLawIxRUNc3rnR.htm](pathfinder-bestiary-items/v8RLawIxRUNc3rnR.htm)|Powerful Blows|auto-trad|
|[v8X1W0LmbkWRpOS6.htm](pathfinder-bestiary-items/v8X1W0LmbkWRpOS6.htm)|Frighten|auto-trad|
|[v8yr3bjEd1JafWFX.htm](pathfinder-bestiary-items/v8yr3bjEd1JafWFX.htm)|Breath Weapon|auto-trad|
|[v90WRjELLiKF57vr.htm](pathfinder-bestiary-items/v90WRjELLiKF57vr.htm)|Jaws|auto-trad|
|[v9Xt73Wj9wclc3ef.htm](pathfinder-bestiary-items/v9Xt73Wj9wclc3ef.htm)|Javelin|auto-trad|
|[V9zIFbIReO27zwGu.htm](pathfinder-bestiary-items/V9zIFbIReO27zwGu.htm)|Darkvision|auto-trad|
|[VB81WcGgMngyyh3E.htm](pathfinder-bestiary-items/VB81WcGgMngyyh3E.htm)|Slink in Shadows|auto-trad|
|[VbE9aK6V1gA4Fbv9.htm](pathfinder-bestiary-items/VbE9aK6V1gA4Fbv9.htm)|Foot|auto-trad|
|[vbhEyAI6chCq7BJL.htm](pathfinder-bestiary-items/vbhEyAI6chCq7BJL.htm)|Claw|auto-trad|
|[VBMoJbMPEmzIPu5X.htm](pathfinder-bestiary-items/VBMoJbMPEmzIPu5X.htm)|Chill Breath|auto-trad|
|[VBQJPk4ZTjKYg7E1.htm](pathfinder-bestiary-items/VBQJPk4ZTjKYg7E1.htm)|Strand|auto-trad|
|[vC0V00q445OAIDix.htm](pathfinder-bestiary-items/vC0V00q445OAIDix.htm)|Negative Healing|auto-trad|
|[vcD6GpR7alODPFzn.htm](pathfinder-bestiary-items/vcD6GpR7alODPFzn.htm)|Attack of Opportunity|auto-trad|
|[vCeTTDUcG0aI8C9H.htm](pathfinder-bestiary-items/vCeTTDUcG0aI8C9H.htm)|Earth Glide|auto-trad|
|[VcfZ8SToykQ7MnHU.htm](pathfinder-bestiary-items/VcfZ8SToykQ7MnHU.htm)|Goblin Scuttle|auto-trad|
|[VcMwjaxqor5ucwyE.htm](pathfinder-bestiary-items/VcMwjaxqor5ucwyE.htm)|Javelin|auto-trad|
|[VCXj9VT8SpqcHdZS.htm](pathfinder-bestiary-items/VCXj9VT8SpqcHdZS.htm)|Lifesense 120 feet|auto-trad|
|[VdbR3tZZovSqgWq8.htm](pathfinder-bestiary-items/VdbR3tZZovSqgWq8.htm)|Axe Swipe|auto-trad|
|[VdlfnhKj8BudHVMB.htm](pathfinder-bestiary-items/VdlfnhKj8BudHVMB.htm)|Trickster's Step|auto-trad|
|[Vdx4lOd9PGzzo18M.htm](pathfinder-bestiary-items/Vdx4lOd9PGzzo18M.htm)|Fling Offal|auto-trad|
|[vdYowEKgmhYMzYaA.htm](pathfinder-bestiary-items/vdYowEKgmhYMzYaA.htm)|Occult Innate Spells|auto-trad|
|[veNUDgghA30FdVNB.htm](pathfinder-bestiary-items/veNUDgghA30FdVNB.htm)|Dispelling Strike|auto-trad|
|[vEwagos32fLnf4Zc.htm](pathfinder-bestiary-items/vEwagos32fLnf4Zc.htm)|Scent 30 feet|auto-trad|
|[VEZWJVB54nkdpnVP.htm](pathfinder-bestiary-items/VEZWJVB54nkdpnVP.htm)|At-Will Spells|auto-trad|
|[VfbM5E2GxtRqzswt.htm](pathfinder-bestiary-items/VfbM5E2GxtRqzswt.htm)|Constant Spells|auto-trad|
|[VfC8rvpO3IN8NVBE.htm](pathfinder-bestiary-items/VfC8rvpO3IN8NVBE.htm)|Arcane Prepared Spells|auto-trad|
|[vfDqURmKtNWQI7IJ.htm](pathfinder-bestiary-items/vfDqURmKtNWQI7IJ.htm)|Lifesense 60 feet|auto-trad|
|[VfGIlE5o88e6ZuW6.htm](pathfinder-bestiary-items/VfGIlE5o88e6ZuW6.htm)|Pack Attack|auto-trad|
|[vgbSuZiK389nv7Tm.htm](pathfinder-bestiary-items/vgbSuZiK389nv7Tm.htm)|Profane Gift|auto-trad|
|[vGCCZICFjOj2u4Dh.htm](pathfinder-bestiary-items/vGCCZICFjOj2u4Dh.htm)|Wing|auto-trad|
|[VgEW660oBhpCgGPZ.htm](pathfinder-bestiary-items/VgEW660oBhpCgGPZ.htm)|Claw|auto-trad|
|[vgW6UmniZ0N09eeE.htm](pathfinder-bestiary-items/vgW6UmniZ0N09eeE.htm)|Arcane Innate Spells|auto-trad|
|[vh1w0Pk1NCyqaccM.htm](pathfinder-bestiary-items/vh1w0Pk1NCyqaccM.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[vhcrEIRKkfTwrwDs.htm](pathfinder-bestiary-items/vhcrEIRKkfTwrwDs.htm)|Fetid Fumes|auto-trad|
|[VhgpqA8362l8qc64.htm](pathfinder-bestiary-items/VhgpqA8362l8qc64.htm)|Wing|auto-trad|
|[vhMuwHfxhvT3a6e2.htm](pathfinder-bestiary-items/vhMuwHfxhvT3a6e2.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[VHQNRgFkCUkpPh9g.htm](pathfinder-bestiary-items/VHQNRgFkCUkpPh9g.htm)|Swift Tracker|auto-trad|
|[vHVrBdzxAjZ3RShK.htm](pathfinder-bestiary-items/vHVrBdzxAjZ3RShK.htm)|Darkvision|auto-trad|
|[VhwoW1Wd9SqC4MVO.htm](pathfinder-bestiary-items/VhwoW1Wd9SqC4MVO.htm)|Slither|auto-trad|
|[VhZJBu1NXWNgA9ey.htm](pathfinder-bestiary-items/VhZJBu1NXWNgA9ey.htm)|Grab|auto-trad|
|[Vib8DhTL418GaOhc.htm](pathfinder-bestiary-items/Vib8DhTL418GaOhc.htm)|At-Will Spells|auto-trad|
|[VidlqkBOGMdBlbUB.htm](pathfinder-bestiary-items/VidlqkBOGMdBlbUB.htm)|Tail|auto-trad|
|[vipJW7r7EquegImq.htm](pathfinder-bestiary-items/vipJW7r7EquegImq.htm)|Adamantine Fangs|auto-trad|
|[vius8NahqzVZYRSE.htm](pathfinder-bestiary-items/vius8NahqzVZYRSE.htm)|Play the Pipes|auto-trad|
|[vjCJtuDBXH0SYNN7.htm](pathfinder-bestiary-items/vjCJtuDBXH0SYNN7.htm)|Blood Frenzy|auto-trad|
|[vJdzS91Cmz3WZZXD.htm](pathfinder-bestiary-items/vJdzS91Cmz3WZZXD.htm)|Soul Siphon|auto-trad|
|[vJesB2gBXUdU1CMN.htm](pathfinder-bestiary-items/vJesB2gBXUdU1CMN.htm)|Flame Of Justice|auto-trad|
|[vJoGCQ4Ay2Cdm85A.htm](pathfinder-bestiary-items/vJoGCQ4Ay2Cdm85A.htm)|Swiftness|auto-trad|
|[VJqYJwxq4m0v6j2M.htm](pathfinder-bestiary-items/VJqYJwxq4m0v6j2M.htm)|Spores|auto-trad|
|[vJxCPyKdG9Aaedkx.htm](pathfinder-bestiary-items/vJxCPyKdG9Aaedkx.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[vKdhNyXTKHDMUfAc.htm](pathfinder-bestiary-items/vKdhNyXTKHDMUfAc.htm)|Flames of Fury|auto-trad|
|[vkEHgfqiTnQVm26w.htm](pathfinder-bestiary-items/vkEHgfqiTnQVm26w.htm)|Divine Rituals|auto-trad|
|[vkMvsL1nrlHkiJQQ.htm](pathfinder-bestiary-items/vkMvsL1nrlHkiJQQ.htm)|Embrace|auto-trad|
|[vknZjhozz53haMJZ.htm](pathfinder-bestiary-items/vknZjhozz53haMJZ.htm)|Arcane Innate Spells|auto-trad|
|[Vkvj7g0PCVVufjEu.htm](pathfinder-bestiary-items/Vkvj7g0PCVVufjEu.htm)|Darkvision|auto-trad|
|[VKxXRGAQaygpB7SH.htm](pathfinder-bestiary-items/VKxXRGAQaygpB7SH.htm)|Jaws|auto-trad|
|[vM8KFrCj03Tm1Odl.htm](pathfinder-bestiary-items/vM8KFrCj03Tm1Odl.htm)|Draconic Frenzy|auto-trad|
|[VMzhTqEkTw0QRQvq.htm](pathfinder-bestiary-items/VMzhTqEkTw0QRQvq.htm)|Uncanny Climber|auto-trad|
|[vNaUr8lkk7o1zXWJ.htm](pathfinder-bestiary-items/vNaUr8lkk7o1zXWJ.htm)|Ghost Touch|auto-trad|
|[VNCpyxuyRZEyA5HG.htm](pathfinder-bestiary-items/VNCpyxuyRZEyA5HG.htm)|Darkvision|auto-trad|
|[vNxkTMY4P5VmwDyM.htm](pathfinder-bestiary-items/vNxkTMY4P5VmwDyM.htm)|Primal Innate Spells|auto-trad|
|[vNXYpdzNtNydM0Yh.htm](pathfinder-bestiary-items/vNXYpdzNtNydM0Yh.htm)|Occult Innate Spells|auto-trad|
|[vOd0Oxhr36N6hj2R.htm](pathfinder-bestiary-items/vOd0Oxhr36N6hj2R.htm)|Darkvision|auto-trad|
|[VONgaGyF9aRjoaok.htm](pathfinder-bestiary-items/VONgaGyF9aRjoaok.htm)|Blizzard|auto-trad|
|[VPh2h7TJhQU8ugDu.htm](pathfinder-bestiary-items/VPh2h7TJhQU8ugDu.htm)|Low-Light Vision|auto-trad|
|[Vpr4X9t4ia5n8MLO.htm](pathfinder-bestiary-items/Vpr4X9t4ia5n8MLO.htm)|Jaws|auto-trad|
|[VpVNCwsAHOKuj0UI.htm](pathfinder-bestiary-items/VpVNCwsAHOKuj0UI.htm)|Occult Spontaneous Spells|auto-trad|
|[VQFM8xePgKsrXydY.htm](pathfinder-bestiary-items/VQFM8xePgKsrXydY.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[vQNY731fleWIGeZp.htm](pathfinder-bestiary-items/vQNY731fleWIGeZp.htm)|Darkvision|auto-trad|
|[vqr3Mssyf2Td2T1D.htm](pathfinder-bestiary-items/vqr3Mssyf2Td2T1D.htm)|Frightful Presence|auto-trad|
|[VQW9KJrLdz5lDjJ9.htm](pathfinder-bestiary-items/VQW9KJrLdz5lDjJ9.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[vr6e0VYbUOGnEU3M.htm](pathfinder-bestiary-items/vr6e0VYbUOGnEU3M.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[vRByig4Tnrtjds3I.htm](pathfinder-bestiary-items/vRByig4Tnrtjds3I.htm)|At-Will Spells|auto-trad|
|[VRVvTkZmbfIqx7uw.htm](pathfinder-bestiary-items/VRVvTkZmbfIqx7uw.htm)|Summon Steed|auto-trad|
|[VRZR3SXqGz9bOL7Z.htm](pathfinder-bestiary-items/VRZR3SXqGz9bOL7Z.htm)|Primal Innate Spells|auto-trad|
|[vskdCA5sskgL7g3S.htm](pathfinder-bestiary-items/vskdCA5sskgL7g3S.htm)|Dream Haunting|auto-trad|
|[Vst7qvKg3u5uhWKG.htm](pathfinder-bestiary-items/Vst7qvKg3u5uhWKG.htm)|Draconic Frenzy|auto-trad|
|[vt7bc5rxJY8WxTy0.htm](pathfinder-bestiary-items/vt7bc5rxJY8WxTy0.htm)|Acid Spit|auto-trad|
|[vTjkwNzpCJmsbg6R.htm](pathfinder-bestiary-items/vTjkwNzpCJmsbg6R.htm)|Shortsword|auto-trad|
|[vTXaShzYmcNbRjzk.htm](pathfinder-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|auto-trad|
|[vudcfpUT9un2pDOP.htm](pathfinder-bestiary-items/vudcfpUT9un2pDOP.htm)|Gnaw|auto-trad|
|[vUwGp8Kc7J2fDUxa.htm](pathfinder-bestiary-items/vUwGp8Kc7J2fDUxa.htm)|Shortsword|auto-trad|
|[VVvneLCIn09BPxX7.htm](pathfinder-bestiary-items/VVvneLCIn09BPxX7.htm)|Erosion Aura|auto-trad|
|[VWI6L5E7GsAtSrfF.htm](pathfinder-bestiary-items/VWI6L5E7GsAtSrfF.htm)|All-Around Vision|auto-trad|
|[vwO9JU83wAh6vmts.htm](pathfinder-bestiary-items/vwO9JU83wAh6vmts.htm)|Crag Linnorm Venom|auto-trad|
|[VwrlDdZczBGO5Ps4.htm](pathfinder-bestiary-items/VwrlDdZczBGO5Ps4.htm)|Motion Sense 60 feet|auto-trad|
|[VwuUYBwpcwv3BGFP.htm](pathfinder-bestiary-items/VwuUYBwpcwv3BGFP.htm)|Branch|auto-trad|
|[vwWsYBBoZjtVHTuT.htm](pathfinder-bestiary-items/vwWsYBBoZjtVHTuT.htm)|Snow Vision|auto-trad|
|[VxA7WerDGElBkdYj.htm](pathfinder-bestiary-items/VxA7WerDGElBkdYj.htm)|Swamp Stride|auto-trad|
|[VXAdFMEIJiPWyhRq.htm](pathfinder-bestiary-items/VXAdFMEIJiPWyhRq.htm)|Negative Healing|auto-trad|
|[VXiR6GGa6NxxxBMb.htm](pathfinder-bestiary-items/VXiR6GGa6NxxxBMb.htm)|Head Hunter|auto-trad|
|[vXxTQUxFEXV4Ldxm.htm](pathfinder-bestiary-items/vXxTQUxFEXV4Ldxm.htm)|Leaping Grab|auto-trad|
|[vXYYcxC29lm6Xqml.htm](pathfinder-bestiary-items/vXYYcxC29lm6Xqml.htm)|Earthbound|auto-trad|
|[Vy2pPgrGJ3SguoOn.htm](pathfinder-bestiary-items/Vy2pPgrGJ3SguoOn.htm)|Shadow Hand|auto-trad|
|[Vy7eGgsZwEtFCJod.htm](pathfinder-bestiary-items/Vy7eGgsZwEtFCJod.htm)|Improved Grab|auto-trad|
|[VYBvEnOj0TibKdci.htm](pathfinder-bestiary-items/VYBvEnOj0TibKdci.htm)|Breath Weapon|auto-trad|
|[Vyc7HceERBcZQuXT.htm](pathfinder-bestiary-items/Vyc7HceERBcZQuXT.htm)|Warding Glyph|auto-trad|
|[VYGyp2X6IJdSS0b2.htm](pathfinder-bestiary-items/VYGyp2X6IJdSS0b2.htm)|Monitor Lizard Venom|auto-trad|
|[VYK3ix2hBaIvUNXp.htm](pathfinder-bestiary-items/VYK3ix2hBaIvUNXp.htm)|Fist|auto-trad|
|[VyoEzllElTOu9fmG.htm](pathfinder-bestiary-items/VyoEzllElTOu9fmG.htm)|Change Size|auto-trad|
|[VyTDZWtEYeQGBrXk.htm](pathfinder-bestiary-items/VyTDZWtEYeQGBrXk.htm)|Darkvision|auto-trad|
|[vZOKYoSD979v70vq.htm](pathfinder-bestiary-items/vZOKYoSD979v70vq.htm)|Corpse Wave|auto-trad|
|[vZVJQzztFpSjzbNW.htm](pathfinder-bestiary-items/vZVJQzztFpSjzbNW.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[w14YL6ltyHxsjfDO.htm](pathfinder-bestiary-items/w14YL6ltyHxsjfDO.htm)|Reality Twist|auto-trad|
|[W1EiRfnWuUIq6KoC.htm](pathfinder-bestiary-items/W1EiRfnWuUIq6KoC.htm)|Speed Surge|auto-trad|
|[W1uyRdJuvABJ7epP.htm](pathfinder-bestiary-items/W1uyRdJuvABJ7epP.htm)|Arcane Innate Spells|auto-trad|
|[W3a4Bkc73B2bUZDE.htm](pathfinder-bestiary-items/W3a4Bkc73B2bUZDE.htm)|Energy Drain|auto-trad|
|[w4Cd5RsR9MjHTRDj.htm](pathfinder-bestiary-items/w4Cd5RsR9MjHTRDj.htm)|Darkvision|auto-trad|
|[w5FhuQgOnX0i92Y3.htm](pathfinder-bestiary-items/w5FhuQgOnX0i92Y3.htm)|Jaws|auto-trad|
|[w6BFsw2xw1ZKxz55.htm](pathfinder-bestiary-items/w6BFsw2xw1ZKxz55.htm)|Low-Light Vision|auto-trad|
|[W6dNim4KEjBzAlMj.htm](pathfinder-bestiary-items/W6dNim4KEjBzAlMj.htm)|Frightful Presence|auto-trad|
|[W8WLNath7obs8p6Z.htm](pathfinder-bestiary-items/W8WLNath7obs8p6Z.htm)|Darkvision|auto-trad|
|[W9vP5WtfdUoAWPDY.htm](pathfinder-bestiary-items/W9vP5WtfdUoAWPDY.htm)|Jaws|auto-trad|
|[Wa8dbVwSNL2tEHKt.htm](pathfinder-bestiary-items/Wa8dbVwSNL2tEHKt.htm)|Encircling Command|auto-trad|
|[waAgRDWjrJU73Mop.htm](pathfinder-bestiary-items/waAgRDWjrJU73Mop.htm)|Arcane Prepared Spells|auto-trad|
|[waCZVvDN6vAn7yCi.htm](pathfinder-bestiary-items/waCZVvDN6vAn7yCi.htm)|Spray Acid|auto-trad|
|[wAhBz8qI1QWAKLEv.htm](pathfinder-bestiary-items/wAhBz8qI1QWAKLEv.htm)|Beak|auto-trad|
|[wAitzoQtepFmPhqR.htm](pathfinder-bestiary-items/wAitzoQtepFmPhqR.htm)|Twisting Tail|auto-trad|
|[WAp1bEkhC2Ag8cwo.htm](pathfinder-bestiary-items/WAp1bEkhC2Ag8cwo.htm)|Darkvision|auto-trad|
|[wAPCN756UU6AV2AN.htm](pathfinder-bestiary-items/wAPCN756UU6AV2AN.htm)|Courageous Switch|auto-trad|
|[WaVlCwJbdjhIEPYG.htm](pathfinder-bestiary-items/WaVlCwJbdjhIEPYG.htm)|At-Will Spells|auto-trad|
|[wb9jgdwbjkdgmDPc.htm](pathfinder-bestiary-items/wb9jgdwbjkdgmDPc.htm)|At-Will Spells|auto-trad|
|[wbhIx2xtiqZn73im.htm](pathfinder-bestiary-items/wbhIx2xtiqZn73im.htm)|+3 Status to All Saves vs. Divine Magic|auto-trad|
|[WbJQr5XhkIENzNfG.htm](pathfinder-bestiary-items/WbJQr5XhkIENzNfG.htm)|Darkvision|auto-trad|
|[wbl4QeudXG1wsK2j.htm](pathfinder-bestiary-items/wbl4QeudXG1wsK2j.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[wBppFKxjdSadJQj2.htm](pathfinder-bestiary-items/wBppFKxjdSadJQj2.htm)|Shortbow|auto-trad|
|[wBs1NoPWhm38PmMP.htm](pathfinder-bestiary-items/wBs1NoPWhm38PmMP.htm)|Club|auto-trad|
|[WBvBXluaSwwxLaJ4.htm](pathfinder-bestiary-items/WBvBXluaSwwxLaJ4.htm)|Sneak Attack|auto-trad|
|[WC9rcwU4DaoOfejU.htm](pathfinder-bestiary-items/WC9rcwU4DaoOfejU.htm)|Arcane Prepared Spells|auto-trad|
|[wChn2PEw9GYziq2n.htm](pathfinder-bestiary-items/wChn2PEw9GYziq2n.htm)|Glow|auto-trad|
|[wcP5j8v9fGg91KoQ.htm](pathfinder-bestiary-items/wcP5j8v9fGg91KoQ.htm)|Darkvision|auto-trad|
|[WCpWLcuShftaYr4k.htm](pathfinder-bestiary-items/WCpWLcuShftaYr4k.htm)|Darkvision|auto-trad|
|[WCwa6PfOl2tfM35F.htm](pathfinder-bestiary-items/WCwa6PfOl2tfM35F.htm)|Darkvision|auto-trad|
|[WD2TqHcVPCybwkQT.htm](pathfinder-bestiary-items/WD2TqHcVPCybwkQT.htm)|Vortex|auto-trad|
|[wd9cXI5wKeM8sebH.htm](pathfinder-bestiary-items/wd9cXI5wKeM8sebH.htm)|Draconic Momentum|auto-trad|
|[wDlRAMig5HNlqg3q.htm](pathfinder-bestiary-items/wDlRAMig5HNlqg3q.htm)|Darkvision|auto-trad|
|[WdOsTqGjiU4ZnX5H.htm](pathfinder-bestiary-items/WdOsTqGjiU4ZnX5H.htm)|Darkvision|auto-trad|
|[wdsdNrRHG7tMUw5P.htm](pathfinder-bestiary-items/wdsdNrRHG7tMUw5P.htm)|Fist|auto-trad|
|[wDyI0HWtmrF7nCK9.htm](pathfinder-bestiary-items/wDyI0HWtmrF7nCK9.htm)|Low-Light Vision|auto-trad|
|[WDz6pEmszokFTO25.htm](pathfinder-bestiary-items/WDz6pEmszokFTO25.htm)|Draconic Momentum|auto-trad|
|[WE5R0TF08XSDSPjw.htm](pathfinder-bestiary-items/WE5R0TF08XSDSPjw.htm)|Telepathy 100 feet|auto-trad|
|[We6LladGR4C687q0.htm](pathfinder-bestiary-items/We6LladGR4C687q0.htm)|Reefclaw Venom|auto-trad|
|[WegG5uFcFYKrHbBF.htm](pathfinder-bestiary-items/WegG5uFcFYKrHbBF.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[wEHW7Aj6HAGWNLXA.htm](pathfinder-bestiary-items/wEHW7Aj6HAGWNLXA.htm)|At-Will Spells|auto-trad|
|[wEVWlNgGdVwq51Y9.htm](pathfinder-bestiary-items/wEVWlNgGdVwq51Y9.htm)|Claw|auto-trad|
|[WeXirhh6gD0DCTGB.htm](pathfinder-bestiary-items/WeXirhh6gD0DCTGB.htm)|Root|auto-trad|
|[Wf7eTH5SNYNO98tC.htm](pathfinder-bestiary-items/Wf7eTH5SNYNO98tC.htm)|Evasion|auto-trad|
|[wfIAFcaANDUUhMSm.htm](pathfinder-bestiary-items/wfIAFcaANDUUhMSm.htm)|Vanish|auto-trad|
|[wfKBAQHE6bHpnSve.htm](pathfinder-bestiary-items/wfKBAQHE6bHpnSve.htm)|Grab|auto-trad|
|[wfMs8RYiuhFeQMau.htm](pathfinder-bestiary-items/wfMs8RYiuhFeQMau.htm)|Mauler|auto-trad|
|[wGDHR7b7nFvTcaRz.htm](pathfinder-bestiary-items/wGDHR7b7nFvTcaRz.htm)|Sphere of Oblivion|auto-trad|
|[WGKxmcUB8pYq06Qv.htm](pathfinder-bestiary-items/WGKxmcUB8pYq06Qv.htm)|Constant Spells|auto-trad|
|[wgMagyELbs4gcxl2.htm](pathfinder-bestiary-items/wgMagyELbs4gcxl2.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[wgSeXMtV7TUrqAqJ.htm](pathfinder-bestiary-items/wgSeXMtV7TUrqAqJ.htm)|Darkvision|auto-trad|
|[WH7tGqprqYb1tk4C.htm](pathfinder-bestiary-items/WH7tGqprqYb1tk4C.htm)|Attack of Opportunity|auto-trad|
|[WhTbPOtlxlYvCpJ6.htm](pathfinder-bestiary-items/WhTbPOtlxlYvCpJ6.htm)|Homunculus Poison|auto-trad|
|[wHTcTY8XLxxGwUU9.htm](pathfinder-bestiary-items/wHTcTY8XLxxGwUU9.htm)|Darkvision|auto-trad|
|[WI20ZnQYqmSIaz3B.htm](pathfinder-bestiary-items/WI20ZnQYqmSIaz3B.htm)|Drawn to Service|auto-trad|
|[wi645CsJxPLHzqhq.htm](pathfinder-bestiary-items/wi645CsJxPLHzqhq.htm)|Darkvision|auto-trad|
|[WIHK77N13bnY09Iq.htm](pathfinder-bestiary-items/WIHK77N13bnY09Iq.htm)|Aqueous Fist|auto-trad|
|[Wir6Og85EjTj5jKg.htm](pathfinder-bestiary-items/Wir6Og85EjTj5jKg.htm)|At-Will Spells|auto-trad|
|[WjBhdxKcekKdof75.htm](pathfinder-bestiary-items/WjBhdxKcekKdof75.htm)|Darkvision|auto-trad|
|[wJnJT5Ecr1w6xix3.htm](pathfinder-bestiary-items/wJnJT5Ecr1w6xix3.htm)|Shortbow|auto-trad|
|[WJYznUjqpKPO2kQz.htm](pathfinder-bestiary-items/WJYznUjqpKPO2kQz.htm)|Grab|auto-trad|
|[WkB3ob6zOZXGPfnf.htm](pathfinder-bestiary-items/WkB3ob6zOZXGPfnf.htm)|Fog Vision|auto-trad|
|[WkPeg600zGONsuJz.htm](pathfinder-bestiary-items/WkPeg600zGONsuJz.htm)|Jaws|auto-trad|
|[WLCM7iPJWfoRusL5.htm](pathfinder-bestiary-items/WLCM7iPJWfoRusL5.htm)|Death Roll|auto-trad|
|[WLKYWXEiwmc9uCr5.htm](pathfinder-bestiary-items/WLKYWXEiwmc9uCr5.htm)|Master Link|auto-trad|
|[wLRqThGwPUufb2HR.htm](pathfinder-bestiary-items/wLRqThGwPUufb2HR.htm)|Raking Sand|auto-trad|
|[WLWmgPx0MfGb1F5z.htm](pathfinder-bestiary-items/WLWmgPx0MfGb1F5z.htm)|Light Pick|auto-trad|
|[WmmDzTWb78QbaymG.htm](pathfinder-bestiary-items/WmmDzTWb78QbaymG.htm)|Claw|auto-trad|
|[wmnFY6ylzPcsOlEb.htm](pathfinder-bestiary-items/wmnFY6ylzPcsOlEb.htm)|Divine Innate Spells|auto-trad|
|[WnbC97emFRGYOPR9.htm](pathfinder-bestiary-items/WnbC97emFRGYOPR9.htm)|Sphere Of Oblivion|auto-trad|
|[WnjXiMvqjEQAuxnZ.htm](pathfinder-bestiary-items/WnjXiMvqjEQAuxnZ.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[woCB9gbBdvxg3WX0.htm](pathfinder-bestiary-items/woCB9gbBdvxg3WX0.htm)|Leaping Charge|auto-trad|
|[wOkHQN8ELSDh7T1f.htm](pathfinder-bestiary-items/wOkHQN8ELSDh7T1f.htm)|Fist|auto-trad|
|[woTn67ogkZRQUfvH.htm](pathfinder-bestiary-items/woTn67ogkZRQUfvH.htm)|Claw|auto-trad|
|[Wp8326gStt6XrGVS.htm](pathfinder-bestiary-items/Wp8326gStt6XrGVS.htm)|Draconic Frenzy|auto-trad|
|[wPoobMAF8dEMbXzS.htm](pathfinder-bestiary-items/wPoobMAF8dEMbXzS.htm)|Tendril|auto-trad|
|[WPtFVQLHQ2T2WxY7.htm](pathfinder-bestiary-items/WPtFVQLHQ2T2WxY7.htm)|Deafening Blow|auto-trad|
|[WQED2R71AMrx1suT.htm](pathfinder-bestiary-items/WQED2R71AMrx1suT.htm)|Golem Antimagic|auto-trad|
|[wqhHoIIgE3jfAgCx.htm](pathfinder-bestiary-items/wqhHoIIgE3jfAgCx.htm)|Smoke Vision|auto-trad|
|[WQlpCiZyHiNu4LE0.htm](pathfinder-bestiary-items/WQlpCiZyHiNu4LE0.htm)|Grab|auto-trad|
|[WQqo16zlXzVAyGHX.htm](pathfinder-bestiary-items/WQqo16zlXzVAyGHX.htm)|Jungle Drake Venom|auto-trad|
|[WqyBNiwVl76DPQYF.htm](pathfinder-bestiary-items/WqyBNiwVl76DPQYF.htm)|Leg|auto-trad|
|[wR8rvUNTkSFlNhUv.htm](pathfinder-bestiary-items/wR8rvUNTkSFlNhUv.htm)|Fist|auto-trad|
|[wrhgHCC4FVBo4mrU.htm](pathfinder-bestiary-items/wrhgHCC4FVBo4mrU.htm)|Arcane Prepared Spells|auto-trad|
|[wrHyzkgKb8tdmukX.htm](pathfinder-bestiary-items/wrHyzkgKb8tdmukX.htm)|Claw|auto-trad|
|[wRiugNyM38JRtkeI.htm](pathfinder-bestiary-items/wRiugNyM38JRtkeI.htm)|Coven Spells|auto-trad|
|[Wsl0Qe07OtlNGmr0.htm](pathfinder-bestiary-items/Wsl0Qe07OtlNGmr0.htm)|Subservience|auto-trad|
|[wsuET1ePJr0p7cTd.htm](pathfinder-bestiary-items/wsuET1ePJr0p7cTd.htm)|Fist|auto-trad|
|[wTyVptTaqC4Su8lm.htm](pathfinder-bestiary-items/wTyVptTaqC4Su8lm.htm)|Energy Touch|auto-trad|
|[WU4cXLzhnoCczalO.htm](pathfinder-bestiary-items/WU4cXLzhnoCczalO.htm)|Irritating Dander|auto-trad|
|[Wu9GLINqkqLPEjJ7.htm](pathfinder-bestiary-items/Wu9GLINqkqLPEjJ7.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[WUmRR2ERjd5gTCCt.htm](pathfinder-bestiary-items/WUmRR2ERjd5gTCCt.htm)|Coven Ritual|auto-trad|
|[WUUFHNKmZ2orqvtO.htm](pathfinder-bestiary-items/WUUFHNKmZ2orqvtO.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[WuUxcHe2LEnb3BVQ.htm](pathfinder-bestiary-items/WuUxcHe2LEnb3BVQ.htm)|Giant Scorpion Venom|auto-trad|
|[Wvb6MKl733cfKJ64.htm](pathfinder-bestiary-items/Wvb6MKl733cfKJ64.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[wVxhMkTfMFAVDviB.htm](pathfinder-bestiary-items/wVxhMkTfMFAVDviB.htm)|Maul|auto-trad|
|[wW41T2Rqe1HsYWiK.htm](pathfinder-bestiary-items/wW41T2Rqe1HsYWiK.htm)|Repair Mode|auto-trad|
|[WW8hndG3qvu25VrH.htm](pathfinder-bestiary-items/WW8hndG3qvu25VrH.htm)|Cloud Walk|auto-trad|
|[WWnDflwvjpP5jAyW.htm](pathfinder-bestiary-items/WWnDflwvjpP5jAyW.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[wwQOKHAUPw4ltzVK.htm](pathfinder-bestiary-items/wwQOKHAUPw4ltzVK.htm)|Change Shape|auto-trad|
|[wWrvIQM5fOYBSMRQ.htm](pathfinder-bestiary-items/wWrvIQM5fOYBSMRQ.htm)|At-Will Spells|auto-trad|
|[WX7zOVpbKUqeICJ0.htm](pathfinder-bestiary-items/WX7zOVpbKUqeICJ0.htm)|Desert Wind|auto-trad|
|[wXAUYhVyZP3okwuu.htm](pathfinder-bestiary-items/wXAUYhVyZP3okwuu.htm)|Darkvision|auto-trad|
|[wya8y6adOGTfI5jX.htm](pathfinder-bestiary-items/wya8y6adOGTfI5jX.htm)|Change Shape|auto-trad|
|[wYcs9eIcJSh0thsP.htm](pathfinder-bestiary-items/wYcs9eIcJSh0thsP.htm)|Centipede Swarm Venom|auto-trad|
|[WYRi6VyNroGIrEk6.htm](pathfinder-bestiary-items/WYRi6VyNroGIrEk6.htm)|Crystalline Dust Form|auto-trad|
|[WzNNAcPz7ibjxcS8.htm](pathfinder-bestiary-items/WzNNAcPz7ibjxcS8.htm)|Claw|auto-trad|
|[WZrWbGf8dD3tbKfR.htm](pathfinder-bestiary-items/WZrWbGf8dD3tbKfR.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[x0anEXm5LMDe02cY.htm](pathfinder-bestiary-items/x0anEXm5LMDe02cY.htm)|Darkvision|auto-trad|
|[X0BfkEsL9YNikfyR.htm](pathfinder-bestiary-items/X0BfkEsL9YNikfyR.htm)|Wave|auto-trad|
|[x1hzvAiCUjfrEaZO.htm](pathfinder-bestiary-items/x1hzvAiCUjfrEaZO.htm)|Descend on a Web|auto-trad|
|[x3yCpybINerCQY3Q.htm](pathfinder-bestiary-items/x3yCpybINerCQY3Q.htm)|Corrosive Touch|auto-trad|
|[x47iDfIE9albveGl.htm](pathfinder-bestiary-items/x47iDfIE9albveGl.htm)|Shadow Hand|auto-trad|
|[X4qGxyWYxGqKvBsG.htm](pathfinder-bestiary-items/X4qGxyWYxGqKvBsG.htm)|Mandibles|auto-trad|
|[X4U013pt6H6xdUSt.htm](pathfinder-bestiary-items/X4U013pt6H6xdUSt.htm)|At-Will Spells|auto-trad|
|[X5DctZ32pPLyBZnM.htm](pathfinder-bestiary-items/X5DctZ32pPLyBZnM.htm)|Entropy Sense (Imprecise) 60 feet|auto-trad|
|[X7eyVZD1ZflrSoj8.htm](pathfinder-bestiary-items/X7eyVZD1ZflrSoj8.htm)|Mucus Cloud|auto-trad|
|[x7U8W2mn02rKkpEU.htm](pathfinder-bestiary-items/x7U8W2mn02rKkpEU.htm)|Scythe|auto-trad|
|[X8djlbeUDOZqxfAS.htm](pathfinder-bestiary-items/X8djlbeUDOZqxfAS.htm)|Arcane Innate Spells|auto-trad|
|[x8uwJOVGskq8pjOH.htm](pathfinder-bestiary-items/x8uwJOVGskq8pjOH.htm)|Circling Attack|auto-trad|
|[X9BvGRPY6jnbRCrG.htm](pathfinder-bestiary-items/X9BvGRPY6jnbRCrG.htm)|Throw Rock|auto-trad|
|[X9lnoAMRNp4d62Iu.htm](pathfinder-bestiary-items/X9lnoAMRNp4d62Iu.htm)|Rush|auto-trad|
|[xa1KRhMM6JnsIsgl.htm](pathfinder-bestiary-items/xa1KRhMM6JnsIsgl.htm)|Telepathy 100 feet|auto-trad|
|[xa72ZqFCCn9Ps5lD.htm](pathfinder-bestiary-items/xa72ZqFCCn9Ps5lD.htm)|Curse of Fire|auto-trad|
|[XAY63LFVYjJCN65F.htm](pathfinder-bestiary-items/XAY63LFVYjJCN65F.htm)|Greater Darkvision|auto-trad|
|[XbEOrTGzNN49cBsV.htm](pathfinder-bestiary-items/XbEOrTGzNN49cBsV.htm)|Jaws|auto-trad|
|[xBi3GMrwnT7AE4mq.htm](pathfinder-bestiary-items/xBi3GMrwnT7AE4mq.htm)|Grabbing Trunk|auto-trad|
|[xBIoPO0Yl00JwtXW.htm](pathfinder-bestiary-items/xBIoPO0Yl00JwtXW.htm)|Swamp Stride|auto-trad|
|[xBWfY8gxHjGavgBq.htm](pathfinder-bestiary-items/xBWfY8gxHjGavgBq.htm)|Divine Prepared Spells|auto-trad|
|[xby0iVqmufLqYCMA.htm](pathfinder-bestiary-items/xby0iVqmufLqYCMA.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[XbyGgxrnfchvOdm3.htm](pathfinder-bestiary-items/XbyGgxrnfchvOdm3.htm)|Pounce|auto-trad|
|[XCbPDCIugQCZ6Euc.htm](pathfinder-bestiary-items/XCbPDCIugQCZ6Euc.htm)|Trample|auto-trad|
|[XCfMmPXvB7LJznlU.htm](pathfinder-bestiary-items/XCfMmPXvB7LJznlU.htm)|Frost Composite Longbow|auto-trad|
|[xCmDwMnAF2ZdjWhU.htm](pathfinder-bestiary-items/xCmDwMnAF2ZdjWhU.htm)|Twisting Tail|auto-trad|
|[xCmuYVEvMqVlGxIw.htm](pathfinder-bestiary-items/xCmuYVEvMqVlGxIw.htm)|Claw|auto-trad|
|[xD2fbKZSefTlgsmQ.htm](pathfinder-bestiary-items/xD2fbKZSefTlgsmQ.htm)|Failure Vulnerability|auto-trad|
|[XD6GRQzVXAnrWid6.htm](pathfinder-bestiary-items/XD6GRQzVXAnrWid6.htm)|Tail|auto-trad|
|[Xd6tMydNtV3GyWPm.htm](pathfinder-bestiary-items/Xd6tMydNtV3GyWPm.htm)|Scrap Ball|auto-trad|
|[xdClMhhs1mXbf26g.htm](pathfinder-bestiary-items/xdClMhhs1mXbf26g.htm)|Wing Deflection|auto-trad|
|[XDq62TH6LQA4rgny.htm](pathfinder-bestiary-items/XDq62TH6LQA4rgny.htm)|Hand Crossbow|auto-trad|
|[xDUpwWQaZevQOkYQ.htm](pathfinder-bestiary-items/xDUpwWQaZevQOkYQ.htm)|Ferocity|auto-trad|
|[xDzg2v4rQ9wNAK3b.htm](pathfinder-bestiary-items/xDzg2v4rQ9wNAK3b.htm)|Death's Grace|auto-trad|
|[Xe4Ca7mHwdqwZ7wZ.htm](pathfinder-bestiary-items/Xe4Ca7mHwdqwZ7wZ.htm)|Occult Spontaneous Spells|auto-trad|
|[xeAR997MOrxz4cyA.htm](pathfinder-bestiary-items/xeAR997MOrxz4cyA.htm)|Gallop|auto-trad|
|[xECw5sCWsHZAUjFv.htm](pathfinder-bestiary-items/xECw5sCWsHZAUjFv.htm)|Longsword|auto-trad|
|[xEHY10Oxi5BJJCmQ.htm](pathfinder-bestiary-items/xEHY10Oxi5BJJCmQ.htm)|Wide Swing|auto-trad|
|[Xem818Dy2SdT8fjI.htm](pathfinder-bestiary-items/Xem818Dy2SdT8fjI.htm)|Glaring Ray|auto-trad|
|[XfQQjjOdt5sRiSb6.htm](pathfinder-bestiary-items/XfQQjjOdt5sRiSb6.htm)|Lion Jaws|auto-trad|
|[xFztWiaMY5iGr24T.htm](pathfinder-bestiary-items/xFztWiaMY5iGr24T.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Xg50UZe4CSts7Kvw.htm](pathfinder-bestiary-items/Xg50UZe4CSts7Kvw.htm)|Horns|auto-trad|
|[XGgKDNKukruETANT.htm](pathfinder-bestiary-items/XGgKDNKukruETANT.htm)|Darkvision|auto-trad|
|[XgjH4JAk9jbj9B8a.htm](pathfinder-bestiary-items/XgjH4JAk9jbj9B8a.htm)|Shamble|auto-trad|
|[XGl21E5BQZXRMicO.htm](pathfinder-bestiary-items/XGl21E5BQZXRMicO.htm)|Draconic Frenzy|auto-trad|
|[XGMdT1ANHdPUE8UO.htm](pathfinder-bestiary-items/XGMdT1ANHdPUE8UO.htm)|Flaming Stroke|auto-trad|
|[XgoaORovhBAB82vg.htm](pathfinder-bestiary-items/XgoaORovhBAB82vg.htm)|Constant Spells|auto-trad|
|[xGPdcgIZR0tAI9IY.htm](pathfinder-bestiary-items/xGPdcgIZR0tAI9IY.htm)|Hair Barrage|auto-trad|
|[XgwBNbRPKr9hX8NS.htm](pathfinder-bestiary-items/XgwBNbRPKr9hX8NS.htm)|Shield Block|auto-trad|
|[xgyBlDJ39BUcAINW.htm](pathfinder-bestiary-items/xgyBlDJ39BUcAINW.htm)|Sneak Attack|auto-trad|
|[XgycaDcxnUeoby8d.htm](pathfinder-bestiary-items/XgycaDcxnUeoby8d.htm)|Fist|auto-trad|
|[xH6NrBw4bYEHU7e3.htm](pathfinder-bestiary-items/xH6NrBw4bYEHU7e3.htm)|Sacrilegious Aura|auto-trad|
|[xH8E3TYe6R3G6KEv.htm](pathfinder-bestiary-items/xH8E3TYe6R3G6KEv.htm)|Locate Inevitable|auto-trad|
|[xheziaVMnj1iAjP5.htm](pathfinder-bestiary-items/xheziaVMnj1iAjP5.htm)|Caustic Mucus|auto-trad|
|[XHFapZJB0eCngkyn.htm](pathfinder-bestiary-items/XHFapZJB0eCngkyn.htm)|Dune|auto-trad|
|[xHg8hrfrTg35erxf.htm](pathfinder-bestiary-items/xHg8hrfrTg35erxf.htm)|Contingency|auto-trad|
|[XhN7A3RmBONMzZUF.htm](pathfinder-bestiary-items/XhN7A3RmBONMzZUF.htm)|Foot|auto-trad|
|[xhqKU8Bdnig3dLJs.htm](pathfinder-bestiary-items/xhqKU8Bdnig3dLJs.htm)|Water-Bound|auto-trad|
|[xHsqXGMqu6bMLRTB.htm](pathfinder-bestiary-items/xHsqXGMqu6bMLRTB.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[xHuOFLF02tZy7mwZ.htm](pathfinder-bestiary-items/xHuOFLF02tZy7mwZ.htm)|Claw|auto-trad|
|[XIYonpEs96Js86vZ.htm](pathfinder-bestiary-items/XIYonpEs96Js86vZ.htm)|Rapid Strikes|auto-trad|
|[XJ5wfV6LNdfMn7EH.htm](pathfinder-bestiary-items/XJ5wfV6LNdfMn7EH.htm)|Draconic Frenzy|auto-trad|
|[XJHXZOImLOORwInT.htm](pathfinder-bestiary-items/XJHXZOImLOORwInT.htm)|Jaws|auto-trad|
|[XJKtPma1TpZuVUUA.htm](pathfinder-bestiary-items/XJKtPma1TpZuVUUA.htm)|Beak|auto-trad|
|[XK9Va6QKy2g8z1dZ.htm](pathfinder-bestiary-items/XK9Va6QKy2g8z1dZ.htm)|Giant Octopus Venom|auto-trad|
|[xkIBsOSynEo9Ii6k.htm](pathfinder-bestiary-items/xkIBsOSynEo9Ii6k.htm)|Staggering Strike|auto-trad|
|[xkMpFGfoqM0o6IYm.htm](pathfinder-bestiary-items/xkMpFGfoqM0o6IYm.htm)|Telepathy 300 feet|auto-trad|
|[xkve6W8Rg8tqIPX0.htm](pathfinder-bestiary-items/xkve6W8Rg8tqIPX0.htm)|Tail|auto-trad|
|[xl5RfS1yyVh85doM.htm](pathfinder-bestiary-items/xl5RfS1yyVh85doM.htm)|Primal Innate Spells|auto-trad|
|[xlEh7Hz3HcY47r4R.htm](pathfinder-bestiary-items/xlEh7Hz3HcY47r4R.htm)|Tremorsense (Imprecise) 90 feet|auto-trad|
|[XLiAwm5JWDoSNIPF.htm](pathfinder-bestiary-items/XLiAwm5JWDoSNIPF.htm)|Beak|auto-trad|
|[xlUnQcjrxeMXu5Cd.htm](pathfinder-bestiary-items/xlUnQcjrxeMXu5Cd.htm)|Divine Innate Spells|auto-trad|
|[Xm0r5sx1GTiEAKc4.htm](pathfinder-bestiary-items/Xm0r5sx1GTiEAKc4.htm)|Cough Spores|auto-trad|
|[Xmaw18x2qDwDq7tB.htm](pathfinder-bestiary-items/Xmaw18x2qDwDq7tB.htm)|Curse of the Werewolf|auto-trad|
|[xMc6TnmwsR72HZR7.htm](pathfinder-bestiary-items/xMc6TnmwsR72HZR7.htm)|Dream Haunting|auto-trad|
|[XMcVGkoaO2Xaj18P.htm](pathfinder-bestiary-items/XMcVGkoaO2Xaj18P.htm)|Grab|auto-trad|
|[xoD1yB3vzy3xf2iD.htm](pathfinder-bestiary-items/xoD1yB3vzy3xf2iD.htm)|Primal Innate Spells|auto-trad|
|[xOGJ7uK7SF5xcrH2.htm](pathfinder-bestiary-items/xOGJ7uK7SF5xcrH2.htm)|Vulnerable to Disintegrate|auto-trad|
|[xolLzAgsgpzS6Nvn.htm](pathfinder-bestiary-items/xolLzAgsgpzS6Nvn.htm)|Shield Block|auto-trad|
|[XoQjkRouHO2yUuKc.htm](pathfinder-bestiary-items/XoQjkRouHO2yUuKc.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[XozXyBaOh9vAKQlv.htm](pathfinder-bestiary-items/XozXyBaOh9vAKQlv.htm)|Constant Spells|auto-trad|
|[xpcbUoWx3vMpIFDi.htm](pathfinder-bestiary-items/xpcbUoWx3vMpIFDi.htm)|Regeneration 25 (Deactivated by Cold)|auto-trad|
|[XpcWHnvqjcngSTyK.htm](pathfinder-bestiary-items/XpcWHnvqjcngSTyK.htm)|Jaws|auto-trad|
|[XPgz4pQn9aYVvddX.htm](pathfinder-bestiary-items/XPgz4pQn9aYVvddX.htm)|Hand Crossbow|auto-trad|
|[XPQEvEN2tEEPYRWA.htm](pathfinder-bestiary-items/XPQEvEN2tEEPYRWA.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[XQKbqAKqILs3dbvQ.htm](pathfinder-bestiary-items/XQKbqAKqILs3dbvQ.htm)|Water Dependent|auto-trad|
|[xqRgagTvArv2abwD.htm](pathfinder-bestiary-items/xqRgagTvArv2abwD.htm)|Constant Spells|auto-trad|
|[XqSpTVGRCbtpPfJU.htm](pathfinder-bestiary-items/XqSpTVGRCbtpPfJU.htm)|Darkvision|auto-trad|
|[xr7UgtUP8wXPdHah.htm](pathfinder-bestiary-items/xr7UgtUP8wXPdHah.htm)|Arcane Innate Spells|auto-trad|
|[xRQZegRo2JOZB7hU.htm](pathfinder-bestiary-items/xRQZegRo2JOZB7hU.htm)|Low-Light Vision|auto-trad|
|[xrTNYpA1abm4LPwG.htm](pathfinder-bestiary-items/xrTNYpA1abm4LPwG.htm)|Coven|auto-trad|
|[XsEpINoWybq0qnQN.htm](pathfinder-bestiary-items/XsEpINoWybq0qnQN.htm)|Jaws|auto-trad|
|[xsknvVvLR4sL1ugG.htm](pathfinder-bestiary-items/xsknvVvLR4sL1ugG.htm)|Darkvision|auto-trad|
|[XsMSamN905Cp8CUb.htm](pathfinder-bestiary-items/XsMSamN905Cp8CUb.htm)|Arcane Prepared Spells|auto-trad|
|[XSsZeFbx7x1xTI4H.htm](pathfinder-bestiary-items/XSsZeFbx7x1xTI4H.htm)|Horn|auto-trad|
|[xswRs14UWcUn7b5S.htm](pathfinder-bestiary-items/xswRs14UWcUn7b5S.htm)|Buck|auto-trad|
|[XTXRL6O388nfibXc.htm](pathfinder-bestiary-items/XTXRL6O388nfibXc.htm)|Grab|auto-trad|
|[XURG5dfn9vFXpZD1.htm](pathfinder-bestiary-items/XURG5dfn9vFXpZD1.htm)|Rituals|auto-trad|
|[xuSwO5OCFxK9XjiS.htm](pathfinder-bestiary-items/xuSwO5OCFxK9XjiS.htm)|Darkvision|auto-trad|
|[xUwxwWrzgPri548T.htm](pathfinder-bestiary-items/xUwxwWrzgPri548T.htm)|Darkvision|auto-trad|
|[Xv9vTEwIVBxtTfxZ.htm](pathfinder-bestiary-items/Xv9vTEwIVBxtTfxZ.htm)|Club|auto-trad|
|[xVw1eoXAtuPKjE3x.htm](pathfinder-bestiary-items/xVw1eoXAtuPKjE3x.htm)|Darkvision|auto-trad|
|[XwFOwvH7yRyD2Pn6.htm](pathfinder-bestiary-items/XwFOwvH7yRyD2Pn6.htm)|Regeneration 15 (Deactivated by Fire)|auto-trad|
|[xwHcXDjVTSiAD1kB.htm](pathfinder-bestiary-items/xwHcXDjVTSiAD1kB.htm)|Wing Rebuff|auto-trad|
|[xWNlGD6uekL83QTY.htm](pathfinder-bestiary-items/xWNlGD6uekL83QTY.htm)|Grab|auto-trad|
|[xX27SX9rrvhMXhIZ.htm](pathfinder-bestiary-items/xX27SX9rrvhMXhIZ.htm)|Change Shape|auto-trad|
|[xx2paGql0oW1VL3k.htm](pathfinder-bestiary-items/xx2paGql0oW1VL3k.htm)|Telepathy 100 feet|auto-trad|
|[xx3DbCuNgwUZWSha.htm](pathfinder-bestiary-items/xx3DbCuNgwUZWSha.htm)|Stinger|auto-trad|
|[xxGqls5RMZVYvmHf.htm](pathfinder-bestiary-items/xxGqls5RMZVYvmHf.htm)|Fast Healing 10|auto-trad|
|[XXiHxjwFvBeHoJUK.htm](pathfinder-bestiary-items/XXiHxjwFvBeHoJUK.htm)|Lifesense 60 feet|auto-trad|
|[XxKXmsn4wSfWyyXp.htm](pathfinder-bestiary-items/XxKXmsn4wSfWyyXp.htm)|Attack of Opportunity|auto-trad|
|[xxUW9YkqVCI57nCo.htm](pathfinder-bestiary-items/xxUW9YkqVCI57nCo.htm)|Aqueous Fist|auto-trad|
|[xxW4UJbrz9uwTSQs.htm](pathfinder-bestiary-items/xxW4UJbrz9uwTSQs.htm)|Fangs|auto-trad|
|[xYlf7HmaIqlHhW8O.htm](pathfinder-bestiary-items/xYlf7HmaIqlHhW8O.htm)|Alchemist's Fire|auto-trad|
|[XYmhvkJxbC5BrIJG.htm](pathfinder-bestiary-items/XYmhvkJxbC5BrIJG.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[xYOVclWnoxsqGBCY.htm](pathfinder-bestiary-items/xYOVclWnoxsqGBCY.htm)|Heavy Crossbow|auto-trad|
|[xYPB38g2HeJUfkGt.htm](pathfinder-bestiary-items/xYPB38g2HeJUfkGt.htm)|Darkvision|auto-trad|
|[XZcafYrLS6wcyUVG.htm](pathfinder-bestiary-items/XZcafYrLS6wcyUVG.htm)|Burn Eyes|auto-trad|
|[xzuGv08xYVhfHdXW.htm](pathfinder-bestiary-items/xzuGv08xYVhfHdXW.htm)|Tail|auto-trad|
|[y03xyojSlEvE4nQh.htm](pathfinder-bestiary-items/y03xyojSlEvE4nQh.htm)|Draconic Frenzy|auto-trad|
|[y0G6Hcirsc4uakIL.htm](pathfinder-bestiary-items/y0G6Hcirsc4uakIL.htm)|Shake It Off|auto-trad|
|[Y0hXaOxr6GsRFfoz.htm](pathfinder-bestiary-items/Y0hXaOxr6GsRFfoz.htm)|+1 Status to All Saves and AC vs. Evil Creatures|auto-trad|
|[y0ncwssgPJfLWMgt.htm](pathfinder-bestiary-items/y0ncwssgPJfLWMgt.htm)|Claw|auto-trad|
|[y0vt5wkSLNErceZF.htm](pathfinder-bestiary-items/y0vt5wkSLNErceZF.htm)|Freezing Blood|auto-trad|
|[Y198r9S2vz19SRbK.htm](pathfinder-bestiary-items/Y198r9S2vz19SRbK.htm)|Drag|auto-trad|
|[y1CWoANqqDtCbae7.htm](pathfinder-bestiary-items/y1CWoANqqDtCbae7.htm)|Frightful Presence|auto-trad|
|[Y1v59ZBvVAomH20U.htm](pathfinder-bestiary-items/Y1v59ZBvVAomH20U.htm)|Broad Swipe|auto-trad|
|[y22Jw3B6gV1YXd2p.htm](pathfinder-bestiary-items/y22Jw3B6gV1YXd2p.htm)|Moon Frenzy|auto-trad|
|[Y2DikEsO4R95MHFo.htm](pathfinder-bestiary-items/Y2DikEsO4R95MHFo.htm)|Verdant Burst|auto-trad|
|[Y2DYcAAQLTtolq5o.htm](pathfinder-bestiary-items/Y2DYcAAQLTtolq5o.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[Y2ewuQtDMN0O0bSp.htm](pathfinder-bestiary-items/Y2ewuQtDMN0O0bSp.htm)|Low-Light Vision|auto-trad|
|[y2ok0WY6i66HeQwf.htm](pathfinder-bestiary-items/y2ok0WY6i66HeQwf.htm)|Quick Bomber|auto-trad|
|[Y36eYYeuCCJ8eXM0.htm](pathfinder-bestiary-items/Y36eYYeuCCJ8eXM0.htm)|Electricity Aura|auto-trad|
|[Y3XpZKuJPVRIacbx.htm](pathfinder-bestiary-items/Y3XpZKuJPVRIacbx.htm)|Jaws|auto-trad|
|[Y44FxT7CY4R6SkdW.htm](pathfinder-bestiary-items/Y44FxT7CY4R6SkdW.htm)|Nymph's Beauty|auto-trad|
|[y4sYzueGiKy1QX4F.htm](pathfinder-bestiary-items/y4sYzueGiKy1QX4F.htm)|Divine Prepared Spells|auto-trad|
|[Y5momV4CWpuw5vhk.htm](pathfinder-bestiary-items/Y5momV4CWpuw5vhk.htm)|Shake It Off|auto-trad|
|[y5Na5QhVKW187D0K.htm](pathfinder-bestiary-items/y5Na5QhVKW187D0K.htm)|Mental Magic|auto-trad|
|[y6bETzshaXTYx4Wq.htm](pathfinder-bestiary-items/y6bETzshaXTYx4Wq.htm)|Shell Rake|auto-trad|
|[Y6FgWopPB5QwPfF4.htm](pathfinder-bestiary-items/Y6FgWopPB5QwPfF4.htm)|Jaws|auto-trad|
|[Y6X7el3YHox3syH1.htm](pathfinder-bestiary-items/Y6X7el3YHox3syH1.htm)|Disperse|auto-trad|
|[y7q6NFl7HXFqMYWW.htm](pathfinder-bestiary-items/y7q6NFl7HXFqMYWW.htm)|Shortsword|auto-trad|
|[y8SLwokMHGSHm0S9.htm](pathfinder-bestiary-items/y8SLwokMHGSHm0S9.htm)|Jaws|auto-trad|
|[y8ve2tORULDkXFSu.htm](pathfinder-bestiary-items/y8ve2tORULDkXFSu.htm)|Tail|auto-trad|
|[Y9YG23TX2Se8zYaw.htm](pathfinder-bestiary-items/Y9YG23TX2Se8zYaw.htm)|Push or Pull 10 feet|auto-trad|
|[yA3EfugwTMvPqKVg.htm](pathfinder-bestiary-items/yA3EfugwTMvPqKVg.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[ya7kmwsMyQI69FrG.htm](pathfinder-bestiary-items/ya7kmwsMyQI69FrG.htm)|Far Lobber|auto-trad|
|[YaAoatEoLAq9KaoR.htm](pathfinder-bestiary-items/YaAoatEoLAq9KaoR.htm)|Confounding Slam|auto-trad|
|[yAj0s7l7lPJLSE8C.htm](pathfinder-bestiary-items/yAj0s7l7lPJLSE8C.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[YAjAjrcDtGXjLBD7.htm](pathfinder-bestiary-items/YAjAjrcDtGXjLBD7.htm)|Draconic Frenzy|auto-trad|
|[yaVkTfBp829HhTYC.htm](pathfinder-bestiary-items/yaVkTfBp829HhTYC.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[yB58WUUckQC7pMxy.htm](pathfinder-bestiary-items/yB58WUUckQC7pMxy.htm)|Grab|auto-trad|
|[YBLnDuhexFtEMrfc.htm](pathfinder-bestiary-items/YBLnDuhexFtEMrfc.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[yBOjYe8hHHRkhUyZ.htm](pathfinder-bestiary-items/yBOjYe8hHHRkhUyZ.htm)|Sneak Attack|auto-trad|
|[yC4YZ8E2MfUoLnzI.htm](pathfinder-bestiary-items/yC4YZ8E2MfUoLnzI.htm)|Rapier|auto-trad|
|[YcE1Kg1c0njojA8b.htm](pathfinder-bestiary-items/YcE1Kg1c0njojA8b.htm)|Grab|auto-trad|
|[ydBVkEJ8CwcPFkPB.htm](pathfinder-bestiary-items/ydBVkEJ8CwcPFkPB.htm)|Blood Feast|auto-trad|
|[ydbvsWl1BoVazVZj.htm](pathfinder-bestiary-items/ydbvsWl1BoVazVZj.htm)|Sea Hag's Bargain|auto-trad|
|[YdD2kpKW2TNfcupL.htm](pathfinder-bestiary-items/YdD2kpKW2TNfcupL.htm)|Goblin Scuttle|auto-trad|
|[yDIuApGqKF4695Oq.htm](pathfinder-bestiary-items/yDIuApGqKF4695Oq.htm)|Spear|auto-trad|
|[ydL0czc2qFw4RnCW.htm](pathfinder-bestiary-items/ydL0czc2qFw4RnCW.htm)|Grab|auto-trad|
|[ydtRuz8pJkmMi4Uh.htm](pathfinder-bestiary-items/ydtRuz8pJkmMi4Uh.htm)|Telepathy 100 feet|auto-trad|
|[YdUFMjejXRVik0Ql.htm](pathfinder-bestiary-items/YdUFMjejXRVik0Ql.htm)|Low-Light Vision|auto-trad|
|[YdXTRnJYFdbi7JTh.htm](pathfinder-bestiary-items/YdXTRnJYFdbi7JTh.htm)|Assisted Mount|auto-trad|
|[YEfhznRCKL956Fzx.htm](pathfinder-bestiary-items/YEfhznRCKL956Fzx.htm)|Jaws|auto-trad|
|[yEhFyHlybvFRrc0k.htm](pathfinder-bestiary-items/yEhFyHlybvFRrc0k.htm)|Lunging Strike|auto-trad|
|[yeHohyJSYfiTwghO.htm](pathfinder-bestiary-items/yeHohyJSYfiTwghO.htm)|Battle Cry|auto-trad|
|[YeXnVtwRynKtKDVy.htm](pathfinder-bestiary-items/YeXnVtwRynKtKDVy.htm)|Defensive Assault|auto-trad|
|[yEzDmYi6aYTiIA8Y.htm](pathfinder-bestiary-items/yEzDmYi6aYTiIA8Y.htm)|Ride the Wind|auto-trad|
|[yfHpnY9G1GCp6VVb.htm](pathfinder-bestiary-items/yfHpnY9G1GCp6VVb.htm)|Greatsword|auto-trad|
|[YfPJcSHzuZ8KIdff.htm](pathfinder-bestiary-items/YfPJcSHzuZ8KIdff.htm)|Druid Order Spells|auto-trad|
|[yFqMLM1iygDqwiZC.htm](pathfinder-bestiary-items/yFqMLM1iygDqwiZC.htm)|Shift Form|auto-trad|
|[yfZjkf1LqlK3U7HM.htm](pathfinder-bestiary-items/yfZjkf1LqlK3U7HM.htm)|Tusk|auto-trad|
|[ygeIx11fATpKbQRh.htm](pathfinder-bestiary-items/ygeIx11fATpKbQRh.htm)|At-Will Spells|auto-trad|
|[yGiXa7MzVzw31w7B.htm](pathfinder-bestiary-items/yGiXa7MzVzw31w7B.htm)|Shield Block|auto-trad|
|[Yh9EKYQqgpz6wFJx.htm](pathfinder-bestiary-items/Yh9EKYQqgpz6wFJx.htm)|Tail|auto-trad|
|[yHE4jDWkyfO5E7mM.htm](pathfinder-bestiary-items/yHE4jDWkyfO5E7mM.htm)|Constrict|auto-trad|
|[YHERBUpSRZoR3Ox8.htm](pathfinder-bestiary-items/YHERBUpSRZoR3Ox8.htm)|Grab|auto-trad|
|[yhjUEa7dDonD0fSh.htm](pathfinder-bestiary-items/yhjUEa7dDonD0fSh.htm)|Grab|auto-trad|
|[Yi4K7PbKaZWlXxWj.htm](pathfinder-bestiary-items/Yi4K7PbKaZWlXxWj.htm)|Site Bound|auto-trad|
|[YIHoSZN2nTp139Dg.htm](pathfinder-bestiary-items/YIHoSZN2nTp139Dg.htm)|Slink|auto-trad|
|[yIloVqSs48TDwk9P.htm](pathfinder-bestiary-items/yIloVqSs48TDwk9P.htm)|Frost Longspear|auto-trad|
|[yk9pyF9iVrRoe307.htm](pathfinder-bestiary-items/yk9pyF9iVrRoe307.htm)|Telepathy 100 feet|auto-trad|
|[yKfR5JdxSC3YITNP.htm](pathfinder-bestiary-items/yKfR5JdxSC3YITNP.htm)|Improved Grab|auto-trad|
|[ykjhYVy331qKqLO7.htm](pathfinder-bestiary-items/ykjhYVy331qKqLO7.htm)|Darkvision|auto-trad|
|[ykw7UpzKukRpwEcU.htm](pathfinder-bestiary-items/ykw7UpzKukRpwEcU.htm)|Slow Aura|auto-trad|
|[yLFnxHxGno0eHAjr.htm](pathfinder-bestiary-items/yLFnxHxGno0eHAjr.htm)|Energy Drain|auto-trad|
|[yLnwKltbJHYpn8qC.htm](pathfinder-bestiary-items/yLnwKltbJHYpn8qC.htm)|Jaws|auto-trad|
|[yLq2DsGvZs0Z0aZN.htm](pathfinder-bestiary-items/yLq2DsGvZs0Z0aZN.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[yM2uUCjL6GUDVlF0.htm](pathfinder-bestiary-items/yM2uUCjL6GUDVlF0.htm)|Divine Innate Spells|auto-trad|
|[ymEZhbkIeDn7RKGI.htm](pathfinder-bestiary-items/ymEZhbkIeDn7RKGI.htm)|Trap Soul|auto-trad|
|[YMj2YteKtdg79DoF.htm](pathfinder-bestiary-items/YMj2YteKtdg79DoF.htm)|Sneak Attack|auto-trad|
|[YMwe0HIrPrtpRdsx.htm](pathfinder-bestiary-items/YMwe0HIrPrtpRdsx.htm)|Negative Healing|auto-trad|
|[YNmQSSXzIEUJ3yrh.htm](pathfinder-bestiary-items/YNmQSSXzIEUJ3yrh.htm)|Frightful Presence|auto-trad|
|[yntcM5gKBjUuNen1.htm](pathfinder-bestiary-items/yntcM5gKBjUuNen1.htm)|Javelin|auto-trad|
|[Yo23Art466hO6dEO.htm](pathfinder-bestiary-items/Yo23Art466hO6dEO.htm)|Bomb|auto-trad|
|[YoEkR6sBza4UBMYy.htm](pathfinder-bestiary-items/YoEkR6sBza4UBMYy.htm)|Essence Drain|auto-trad|
|[YOFEFZQFoVYO9alj.htm](pathfinder-bestiary-items/YOFEFZQFoVYO9alj.htm)|Catch Rock|auto-trad|
|[YoKSGUjwXmt7kxnq.htm](pathfinder-bestiary-items/YoKSGUjwXmt7kxnq.htm)|Curse of Death|auto-trad|
|[YOkyQZ6amn9r15G3.htm](pathfinder-bestiary-items/YOkyQZ6amn9r15G3.htm)|Maul|auto-trad|
|[yoSPlCHmFaXCr9Th.htm](pathfinder-bestiary-items/yoSPlCHmFaXCr9Th.htm)|At-Will Spells|auto-trad|
|[yOxa8PKiBE6PbRBA.htm](pathfinder-bestiary-items/yOxa8PKiBE6PbRBA.htm)|Body|auto-trad|
|[yp7FDjO7715BHNPw.htm](pathfinder-bestiary-items/yp7FDjO7715BHNPw.htm)|Darkvision|auto-trad|
|[YpgcfJpD1jMhcu1X.htm](pathfinder-bestiary-items/YpgcfJpD1jMhcu1X.htm)|Devil Shaping|auto-trad|
|[ypKdaBzYAIDgW2KT.htm](pathfinder-bestiary-items/ypKdaBzYAIDgW2KT.htm)|Earth Glide|auto-trad|
|[yPMc4avP8IHjveQY.htm](pathfinder-bestiary-items/yPMc4avP8IHjveQY.htm)|Constant Spells|auto-trad|
|[yQ0WKMlep02TPvLi.htm](pathfinder-bestiary-items/yQ0WKMlep02TPvLi.htm)|Change Shape|auto-trad|
|[YqWqB6EYv3yk0LPh.htm](pathfinder-bestiary-items/YqWqB6EYv3yk0LPh.htm)|Frightful Moan|auto-trad|
|[YR4Hu0QGXr4QHQyq.htm](pathfinder-bestiary-items/YR4Hu0QGXr4QHQyq.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[yrctbhF7nTGEPJSK.htm](pathfinder-bestiary-items/yrctbhF7nTGEPJSK.htm)|Longspear|auto-trad|
|[yrfY15KIhcsy07PS.htm](pathfinder-bestiary-items/yrfY15KIhcsy07PS.htm)|Pseudopod|auto-trad|
|[yrKXLmbaekza05cd.htm](pathfinder-bestiary-items/yrKXLmbaekza05cd.htm)|Improved Push 10 feet|auto-trad|
|[YS1lNceoORGTpa2L.htm](pathfinder-bestiary-items/YS1lNceoORGTpa2L.htm)|Infused Items|auto-trad|
|[ys1ZCX9tF6g75EH0.htm](pathfinder-bestiary-items/ys1ZCX9tF6g75EH0.htm)|Tail|auto-trad|
|[ySh7mxtzIvc9onNT.htm](pathfinder-bestiary-items/ySh7mxtzIvc9onNT.htm)|Tail|auto-trad|
|[YSjgR6s49HwnqEi6.htm](pathfinder-bestiary-items/YSjgR6s49HwnqEi6.htm)|Darkvision|auto-trad|
|[ysMN796BHlk39kv8.htm](pathfinder-bestiary-items/ysMN796BHlk39kv8.htm)|Frightful Presence|auto-trad|
|[ySnTknzJunTGUgYX.htm](pathfinder-bestiary-items/ySnTknzJunTGUgYX.htm)|Tail|auto-trad|
|[YsodmcYoo9fl86Bj.htm](pathfinder-bestiary-items/YsodmcYoo9fl86Bj.htm)|Claw|auto-trad|
|[YsSqsOvXgRemLPt6.htm](pathfinder-bestiary-items/YsSqsOvXgRemLPt6.htm)|Darkvision|auto-trad|
|[YTDggskczm13sYCy.htm](pathfinder-bestiary-items/YTDggskczm13sYCy.htm)|Scent (Imprecise) 80 feet|auto-trad|
|[Ytry4aEXlZIFroqA.htm](pathfinder-bestiary-items/Ytry4aEXlZIFroqA.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[YttgSizfr7L607th.htm](pathfinder-bestiary-items/YttgSizfr7L607th.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[yTXiHzmRavI1BmKJ.htm](pathfinder-bestiary-items/yTXiHzmRavI1BmKJ.htm)|Rend|auto-trad|
|[YuejnaqH1UUSFEtm.htm](pathfinder-bestiary-items/YuejnaqH1UUSFEtm.htm)|Occult Spontaneous Spells|auto-trad|
|[yUMh1Oa8g3zZ6utm.htm](pathfinder-bestiary-items/yUMh1Oa8g3zZ6utm.htm)|Wide Swing|auto-trad|
|[yuSihDZaTWtIZbzU.htm](pathfinder-bestiary-items/yuSihDZaTWtIZbzU.htm)|Arcane Innate Spells|auto-trad|
|[yuu0RBqGlI0PYbGU.htm](pathfinder-bestiary-items/yuu0RBqGlI0PYbGU.htm)|Darkvision|auto-trad|
|[YuzoCdKpCzawAPTJ.htm](pathfinder-bestiary-items/YuzoCdKpCzawAPTJ.htm)|Spike|auto-trad|
|[yvHOZTfOKMN7JNu7.htm](pathfinder-bestiary-items/yvHOZTfOKMN7JNu7.htm)|Hunt Prey|auto-trad|
|[YW6KUycT9oCKgOAo.htm](pathfinder-bestiary-items/YW6KUycT9oCKgOAo.htm)|Darkvision|auto-trad|
|[YWBW2zZbIggEH4rW.htm](pathfinder-bestiary-items/YWBW2zZbIggEH4rW.htm)|Knockdown|auto-trad|
|[YwidZ8QmOJ85iLcw.htm](pathfinder-bestiary-items/YwidZ8QmOJ85iLcw.htm)|Blood Drain|auto-trad|
|[yWipQYXVRovUBD3A.htm](pathfinder-bestiary-items/yWipQYXVRovUBD3A.htm)|At-Will Spells|auto-trad|
|[yWxsd7SGgC3z0IoN.htm](pathfinder-bestiary-items/yWxsd7SGgC3z0IoN.htm)|Twisting Tail|auto-trad|
|[YXiHAcR16OtpzApO.htm](pathfinder-bestiary-items/YXiHAcR16OtpzApO.htm)|Speed Surge|auto-trad|
|[yxQPTCjmsUlWSm7n.htm](pathfinder-bestiary-items/yxQPTCjmsUlWSm7n.htm)|Revert Form|auto-trad|
|[YXsM6AcguDWAwVIt.htm](pathfinder-bestiary-items/YXsM6AcguDWAwVIt.htm)|Pseudopod|auto-trad|
|[yYFISOz912MYdEDd.htm](pathfinder-bestiary-items/yYFISOz912MYdEDd.htm)|Breath Weapon|auto-trad|
|[yyKQP4J4yplKGLL0.htm](pathfinder-bestiary-items/yyKQP4J4yplKGLL0.htm)|Frightful Presence|auto-trad|
|[YYsOPt3KeLlEyyOR.htm](pathfinder-bestiary-items/YYsOPt3KeLlEyyOR.htm)|Pincer|auto-trad|
|[yYXID6QLMJVxA2xM.htm](pathfinder-bestiary-items/yYXID6QLMJVxA2xM.htm)|Negative Healing|auto-trad|
|[YyxKJRRXl2GdUfTt.htm](pathfinder-bestiary-items/YyxKJRRXl2GdUfTt.htm)|Darkvision|auto-trad|
|[yzX7FLSgvEvMeqQY.htm](pathfinder-bestiary-items/yzX7FLSgvEvMeqQY.htm)|Low-Light Vision|auto-trad|
|[YZxhBg9R5m8ydWm1.htm](pathfinder-bestiary-items/YZxhBg9R5m8ydWm1.htm)|Claw|auto-trad|
|[z07apLapCrVoCKOc.htm](pathfinder-bestiary-items/z07apLapCrVoCKOc.htm)|Dagger|auto-trad|
|[z0QT6nvmIHbeoGHa.htm](pathfinder-bestiary-items/z0QT6nvmIHbeoGHa.htm)|Jaws|auto-trad|
|[z0XDoZaPAzbT1buS.htm](pathfinder-bestiary-items/z0XDoZaPAzbT1buS.htm)|Tentacle|auto-trad|
|[Z0xjQ4vMpyymQS0z.htm](pathfinder-bestiary-items/Z0xjQ4vMpyymQS0z.htm)|Fist|auto-trad|
|[z1Wnid4gCPgKMoV8.htm](pathfinder-bestiary-items/z1Wnid4gCPgKMoV8.htm)|Water Healing|auto-trad|
|[z42GiliLLdEVPTiX.htm](pathfinder-bestiary-items/z42GiliLLdEVPTiX.htm)|Darkvision|auto-trad|
|[z4Mouf0sgt0pO2Vz.htm](pathfinder-bestiary-items/z4Mouf0sgt0pO2Vz.htm)|Avenging Bite|auto-trad|
|[Z4NC0zq5T9vj6Gu8.htm](pathfinder-bestiary-items/Z4NC0zq5T9vj6Gu8.htm)|Brand of the Impenitent|auto-trad|
|[z4WS8tZWux7BE3D7.htm](pathfinder-bestiary-items/z4WS8tZWux7BE3D7.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Z5lrTod89eL9ROFq.htm](pathfinder-bestiary-items/Z5lrTod89eL9ROFq.htm)|Primal Innate Spells|auto-trad|
|[Z6GLjJXLZ7y8eIDF.htm](pathfinder-bestiary-items/Z6GLjJXLZ7y8eIDF.htm)|Darkvision|auto-trad|
|[Z78ylUwriAT6RClH.htm](pathfinder-bestiary-items/Z78ylUwriAT6RClH.htm)|Darkvision|auto-trad|
|[z7GewXbjOdMCjk6w.htm](pathfinder-bestiary-items/z7GewXbjOdMCjk6w.htm)|Holy Greatsword|auto-trad|
|[z8bl09rqF4yCJDy4.htm](pathfinder-bestiary-items/z8bl09rqF4yCJDy4.htm)|Terrifying Croak|auto-trad|
|[z8Vh23jCIh7iroYs.htm](pathfinder-bestiary-items/z8Vh23jCIh7iroYs.htm)|Swarming Stings|auto-trad|
|[z9bTah65DoASEDhP.htm](pathfinder-bestiary-items/z9bTah65DoASEDhP.htm)|Shortsword|auto-trad|
|[z9EzpXZgqR4vRkyV.htm](pathfinder-bestiary-items/z9EzpXZgqR4vRkyV.htm)|Darkvision|auto-trad|
|[ZA1vPfnoLP9PkAG2.htm](pathfinder-bestiary-items/ZA1vPfnoLP9PkAG2.htm)|Tremorsense (Imprecise) 10 feet|auto-trad|
|[zadQdUq4eeiDSHSq.htm](pathfinder-bestiary-items/zadQdUq4eeiDSHSq.htm)|Shadow Spawn|auto-trad|
|[zb5Fwjfb2emTLKdL.htm](pathfinder-bestiary-items/zb5Fwjfb2emTLKdL.htm)|Grab|auto-trad|
|[zbg9eaOONakDkuma.htm](pathfinder-bestiary-items/zbg9eaOONakDkuma.htm)|Aquatic Opportunity (Special)|auto-trad|
|[Zc2k22zs45gbuybT.htm](pathfinder-bestiary-items/Zc2k22zs45gbuybT.htm)|Low-Light Vision|auto-trad|
|[Zc5P1zPH0GL9ZDWH.htm](pathfinder-bestiary-items/Zc5P1zPH0GL9ZDWH.htm)|Darkvision|auto-trad|
|[zCkhc1TIl4lLnx0Y.htm](pathfinder-bestiary-items/zCkhc1TIl4lLnx0Y.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[ZCRCrZQCOFtODRBG.htm](pathfinder-bestiary-items/ZCRCrZQCOFtODRBG.htm)|+2 Status to All Saves vs. Auditory and Visual|auto-trad|
|[Zcyxz8R468nruWI9.htm](pathfinder-bestiary-items/Zcyxz8R468nruWI9.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[zczHtXzGMeNhSQGF.htm](pathfinder-bestiary-items/zczHtXzGMeNhSQGF.htm)|Darkvision|auto-trad|
|[zD3l2lGfHmnHF8iq.htm](pathfinder-bestiary-items/zD3l2lGfHmnHF8iq.htm)|Drain Life|auto-trad|
|[zDDzvsB3OrJU9QXn.htm](pathfinder-bestiary-items/zDDzvsB3OrJU9QXn.htm)|Vigorous Shake|auto-trad|
|[ZDJkjnIor7qxsiSU.htm](pathfinder-bestiary-items/ZDJkjnIor7qxsiSU.htm)|Strangle|auto-trad|
|[ZeFESJTxEPFbLeiy.htm](pathfinder-bestiary-items/ZeFESJTxEPFbLeiy.htm)|Negative Healing|auto-trad|
|[zEMUKIwgy7ajyUpa.htm](pathfinder-bestiary-items/zEMUKIwgy7ajyUpa.htm)|Claw|auto-trad|
|[ZenMAKlPgjQVLQNC.htm](pathfinder-bestiary-items/ZenMAKlPgjQVLQNC.htm)|Rituals|auto-trad|
|[zeopy130t1G5DRUp.htm](pathfinder-bestiary-items/zeopy130t1G5DRUp.htm)|Draconic Momentum|auto-trad|
|[zeWfNg7lIyopCbmn.htm](pathfinder-bestiary-items/zeWfNg7lIyopCbmn.htm)|Fist|auto-trad|
|[zEz9gGlpNBUnyF2Y.htm](pathfinder-bestiary-items/zEz9gGlpNBUnyF2Y.htm)|Change Shape|auto-trad|
|[zEZW1atU8KQCSsNi.htm](pathfinder-bestiary-items/zEZW1atU8KQCSsNi.htm)|Thrash|auto-trad|
|[ZfDN0qrqMsIVdq7W.htm](pathfinder-bestiary-items/ZfDN0qrqMsIVdq7W.htm)|Improved Grab|auto-trad|
|[zFNrz9di2EnI53hz.htm](pathfinder-bestiary-items/zFNrz9di2EnI53hz.htm)|Pack Attack|auto-trad|
|[ZfNWabALFufsHCUi.htm](pathfinder-bestiary-items/ZfNWabALFufsHCUi.htm)|Draconic Frenzy|auto-trad|
|[ZfVwcTmrbWAVaOCX.htm](pathfinder-bestiary-items/ZfVwcTmrbWAVaOCX.htm)|Improved Grab|auto-trad|
|[zGEOz9aVguJujbSi.htm](pathfinder-bestiary-items/zGEOz9aVguJujbSi.htm)|Mutations|auto-trad|
|[ZgpdfTBdMYYRwcfP.htm](pathfinder-bestiary-items/ZgpdfTBdMYYRwcfP.htm)|Web|auto-trad|
|[zgR8Rwk8qwhkLVy6.htm](pathfinder-bestiary-items/zgR8Rwk8qwhkLVy6.htm)|Death Throes|auto-trad|
|[zGWjXyKd5cavgSYJ.htm](pathfinder-bestiary-items/zGWjXyKd5cavgSYJ.htm)|Primal Innate Spells|auto-trad|
|[zGXZKhk95VPsSdKN.htm](pathfinder-bestiary-items/zGXZKhk95VPsSdKN.htm)|Biting Snakes|auto-trad|
|[Zh4UT6i2zsYug3c7.htm](pathfinder-bestiary-items/Zh4UT6i2zsYug3c7.htm)|Darkvision|auto-trad|
|[zHIioB6mgFk3W6Uz.htm](pathfinder-bestiary-items/zHIioB6mgFk3W6Uz.htm)|Blood Frenzy|auto-trad|
|[zHL2E4bkH3vVvqSD.htm](pathfinder-bestiary-items/zHL2E4bkH3vVvqSD.htm)|Change Size|auto-trad|
|[zHzxiV7sS8c8LmWL.htm](pathfinder-bestiary-items/zHzxiV7sS8c8LmWL.htm)|Fist|auto-trad|
|[Zi3UHzrDTQBgI0SW.htm](pathfinder-bestiary-items/Zi3UHzrDTQBgI0SW.htm)|Telekinetic Object (Piercing)|auto-trad|
|[ziFuQqCKpWrgkyes.htm](pathfinder-bestiary-items/ziFuQqCKpWrgkyes.htm)|Rock|auto-trad|
|[zIgoZ6UuenqL0UvX.htm](pathfinder-bestiary-items/zIgoZ6UuenqL0UvX.htm)|Dragon Heat|auto-trad|
|[ziSz2omKZDCD4A0e.htm](pathfinder-bestiary-items/ziSz2omKZDCD4A0e.htm)|Claw|auto-trad|
|[zIyme0AGuphXNgSa.htm](pathfinder-bestiary-items/zIyme0AGuphXNgSa.htm)|Attack of Opportunity|auto-trad|
|[zK2aR7Xwx6ctIIxv.htm](pathfinder-bestiary-items/zK2aR7Xwx6ctIIxv.htm)|Improved Grab|auto-trad|
|[ZKFDBSkBr0B38G7d.htm](pathfinder-bestiary-items/ZKFDBSkBr0B38G7d.htm)|Constant Spells|auto-trad|
|[ZktMGhL2mkH1daBn.htm](pathfinder-bestiary-items/ZktMGhL2mkH1daBn.htm)|Ice Climb|auto-trad|
|[zKwj4CDPZ698YJci.htm](pathfinder-bestiary-items/zKwj4CDPZ698YJci.htm)|Hoof|auto-trad|
|[zl24K9E3qEAfVXrR.htm](pathfinder-bestiary-items/zl24K9E3qEAfVXrR.htm)|Turn to Mist|auto-trad|
|[zlPeMmaUBpOYNjEw.htm](pathfinder-bestiary-items/zlPeMmaUBpOYNjEw.htm)|Occult Spontaneous Spells|auto-trad|
|[zlRfRzTTm14EvZIG.htm](pathfinder-bestiary-items/zlRfRzTTm14EvZIG.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[zLUOcpj85OOnwvRh.htm](pathfinder-bestiary-items/zLUOcpj85OOnwvRh.htm)|Curse of Boiling Blood|auto-trad|
|[ZlxZ2pDpBDTkPZ6M.htm](pathfinder-bestiary-items/ZlxZ2pDpBDTkPZ6M.htm)|Sling|auto-trad|
|[zlYuXPKypj1updU4.htm](pathfinder-bestiary-items/zlYuXPKypj1updU4.htm)|Jaws|auto-trad|
|[zm5hc0YuNjjknTeP.htm](pathfinder-bestiary-items/zm5hc0YuNjjknTeP.htm)|Darkvision|auto-trad|
|[Zn0AZHlLWAhHykEg.htm](pathfinder-bestiary-items/Zn0AZHlLWAhHykEg.htm)|Claw|auto-trad|
|[zNcTKoBJlpFDMTec.htm](pathfinder-bestiary-items/zNcTKoBJlpFDMTec.htm)|Berserk Slam|auto-trad|
|[ZNWKdUJvc61evvBc.htm](pathfinder-bestiary-items/ZNWKdUJvc61evvBc.htm)|Wide Swing|auto-trad|
|[zoTuvqovtFZqbEp6.htm](pathfinder-bestiary-items/zoTuvqovtFZqbEp6.htm)|Adaptive Strike|auto-trad|
|[zOzr7HCfJlCH6I7z.htm](pathfinder-bestiary-items/zOzr7HCfJlCH6I7z.htm)|Draconic Momentum|auto-trad|
|[ZP1PiehJLtBoKijM.htm](pathfinder-bestiary-items/ZP1PiehJLtBoKijM.htm)|Shortbow|auto-trad|
|[ZP2IXAQnslTJ2Yfq.htm](pathfinder-bestiary-items/ZP2IXAQnslTJ2Yfq.htm)|Jaws|auto-trad|
|[Zp7ozjV5uBLnBo41.htm](pathfinder-bestiary-items/Zp7ozjV5uBLnBo41.htm)|Final Death|auto-trad|
|[ZpBdjMgBgOgkeGA8.htm](pathfinder-bestiary-items/ZpBdjMgBgOgkeGA8.htm)|Claw|auto-trad|
|[Zpn8XChRDGw65K4B.htm](pathfinder-bestiary-items/Zpn8XChRDGw65K4B.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[zpUaHpTkfVbbw7sX.htm](pathfinder-bestiary-items/zpUaHpTkfVbbw7sX.htm)|Overpowering Jaws|auto-trad|
|[ZpVuzT55hKotfiRz.htm](pathfinder-bestiary-items/ZpVuzT55hKotfiRz.htm)|Throw Rock|auto-trad|
|[zQRbKmxdzecknTDu.htm](pathfinder-bestiary-items/zQRbKmxdzecknTDu.htm)|Primal Innate Spells|auto-trad|
|[ZQs5C40PhQ6fLEOd.htm](pathfinder-bestiary-items/ZQs5C40PhQ6fLEOd.htm)|Jaws|auto-trad|
|[zQUhdB7kvudfGv5Z.htm](pathfinder-bestiary-items/zQUhdB7kvudfGv5Z.htm)|Reposition|auto-trad|
|[zqXzm6YAzfHGm8sq.htm](pathfinder-bestiary-items/zqXzm6YAzfHGm8sq.htm)|Goblin Song|auto-trad|
|[ZQY7WcFFZtddDBnh.htm](pathfinder-bestiary-items/ZQY7WcFFZtddDBnh.htm)|Precision Edge|auto-trad|
|[ZREK2bLfJEVr6kpv.htm](pathfinder-bestiary-items/ZREK2bLfJEVr6kpv.htm)|Hatchet|auto-trad|
|[zRI7pbJvbWWPBJQr.htm](pathfinder-bestiary-items/zRI7pbJvbWWPBJQr.htm)|Low-Light Vision|auto-trad|
|[ZrnTtQzYoMMf79j0.htm](pathfinder-bestiary-items/ZrnTtQzYoMMf79j0.htm)|Foot|auto-trad|
|[ZRU4JYMsdXENmG5Q.htm](pathfinder-bestiary-items/ZRU4JYMsdXENmG5Q.htm)|At-Will Spells|auto-trad|
|[ZrxAcxdGyfkm2kZf.htm](pathfinder-bestiary-items/ZrxAcxdGyfkm2kZf.htm)|Pseudopod|auto-trad|
|[ZSlJL6w6J5g6kFBN.htm](pathfinder-bestiary-items/ZSlJL6w6J5g6kFBN.htm)|Arcane Innate Spells|auto-trad|
|[zSN2IyI2qyW456x3.htm](pathfinder-bestiary-items/zSN2IyI2qyW456x3.htm)|Longspear|auto-trad|
|[zSXEX1BIW9Kopq1o.htm](pathfinder-bestiary-items/zSXEX1BIW9Kopq1o.htm)|At-Will Spells|auto-trad|
|[ztAIkCm5tn8PbtUr.htm](pathfinder-bestiary-items/ztAIkCm5tn8PbtUr.htm)|Claw|auto-trad|
|[ZTPzGQcaC9zUdjD6.htm](pathfinder-bestiary-items/ZTPzGQcaC9zUdjD6.htm)|Darkvision|auto-trad|
|[ztr88bB5WvLP8Ykt.htm](pathfinder-bestiary-items/ztr88bB5WvLP8Ykt.htm)|Earth Glide|auto-trad|
|[ZTtP9ifJTYHBXH8d.htm](pathfinder-bestiary-items/ZTtP9ifJTYHBXH8d.htm)|Hatchet|auto-trad|
|[ZtVK4wU2SnZ0H3zl.htm](pathfinder-bestiary-items/ZtVK4wU2SnZ0H3zl.htm)|Flaming Coal|auto-trad|
|[zTWAKdDMdVslc62l.htm](pathfinder-bestiary-items/zTWAKdDMdVslc62l.htm)|Grab|auto-trad|
|[ztyTHwNIKhCgnOc2.htm](pathfinder-bestiary-items/ztyTHwNIKhCgnOc2.htm)|Bone Javelin|auto-trad|
|[zu0tOUwHfZJlaGQq.htm](pathfinder-bestiary-items/zu0tOUwHfZJlaGQq.htm)|Seedpod|auto-trad|
|[ZUBqUDHdZhSOTiQD.htm](pathfinder-bestiary-items/ZUBqUDHdZhSOTiQD.htm)|Darkvision|auto-trad|
|[zucHNDwfKldM9TpE.htm](pathfinder-bestiary-items/zucHNDwfKldM9TpE.htm)|Jaws|auto-trad|
|[ZuFNPk5MmcRIzGRR.htm](pathfinder-bestiary-items/ZuFNPk5MmcRIzGRR.htm)|Draconic Frenzy|auto-trad|
|[zuuG6E7754pHBLsE.htm](pathfinder-bestiary-items/zuuG6E7754pHBLsE.htm)|Darkvision|auto-trad|
|[ZuxcOScsfkdklfi8.htm](pathfinder-bestiary-items/ZuxcOScsfkdklfi8.htm)|Breath of the Sea|auto-trad|
|[zUyuIoJBzk4LNvK2.htm](pathfinder-bestiary-items/zUyuIoJBzk4LNvK2.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[zwfFiuZdh5gkdUpi.htm](pathfinder-bestiary-items/zwfFiuZdh5gkdUpi.htm)|Arcane Prepared Spells|auto-trad|
|[ZWzIyAxYv8ApHu3K.htm](pathfinder-bestiary-items/ZWzIyAxYv8ApHu3K.htm)|Shield Push|auto-trad|
|[zxbJYDbwhz45mMSX.htm](pathfinder-bestiary-items/zxbJYDbwhz45mMSX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[zXDFVZKb5GlrVnod.htm](pathfinder-bestiary-items/zXDFVZKb5GlrVnod.htm)|Dust|auto-trad|
|[ZxIVdGLdVzJ1RVHi.htm](pathfinder-bestiary-items/ZxIVdGLdVzJ1RVHi.htm)|Mangling Rend|auto-trad|
|[zxL9Jy3Yap38BQBi.htm](pathfinder-bestiary-items/zxL9Jy3Yap38BQBi.htm)|Manipulate Flames|auto-trad|
|[ZXotvZV2fYBkhfUy.htm](pathfinder-bestiary-items/ZXotvZV2fYBkhfUy.htm)|Skewer|auto-trad|
|[zxrTBcJcqEnRKZyA.htm](pathfinder-bestiary-items/zxrTBcJcqEnRKZyA.htm)|Fist|auto-trad|
|[zYGuLe2x3OTlMc06.htm](pathfinder-bestiary-items/zYGuLe2x3OTlMc06.htm)|Jaunt|auto-trad|
|[zyK8S6224cjBrzpM.htm](pathfinder-bestiary-items/zyK8S6224cjBrzpM.htm)|Whip Reposition|auto-trad|
|[ZyTjl37PC8e0rtsy.htm](pathfinder-bestiary-items/ZyTjl37PC8e0rtsy.htm)|Rending Mandibles|auto-trad|
|[zyYihJ8eYGtYRh9u.htm](pathfinder-bestiary-items/zyYihJ8eYGtYRh9u.htm)|Bastard Sword|auto-trad|
|[zZ7YQvZHZDePNYfh.htm](pathfinder-bestiary-items/zZ7YQvZHZDePNYfh.htm)|Darkvision|auto-trad|
|[zzGNgcKnTWZFH3Jk.htm](pathfinder-bestiary-items/zzGNgcKnTWZFH3Jk.htm)|Warhammer|auto-trad|
|[ZZi8bWk8Oz1nqeYY.htm](pathfinder-bestiary-items/ZZi8bWk8Oz1nqeYY.htm)|Claw|auto-trad|
|[zzzLK4jcgGesR9Yt.htm](pathfinder-bestiary-items/zzzLK4jcgGesR9Yt.htm)|Darkvision|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
