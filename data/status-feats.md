# Estado de la traducción (feats)

 * **auto-trad**: 3890
 * **modificada**: 88
 * **ninguna**: 7


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[class-02-uBr7wHtOS6KIhHnd.htm](feats/class-02-uBr7wHtOS6KIhHnd.htm)|Twilight Speaker Dedication|
|[class-04-mnHUemd21MtmV9FV.htm](feats/class-04-mnHUemd21MtmV9FV.htm)|Empathic Envoy|
|[class-06-cXuycCR29sFLBjbo.htm](feats/class-06-cXuycCR29sFLBjbo.htm)|Betraying Shank|
|[class-06-ZGgFrQAQWk0keaFW.htm](feats/class-06-ZGgFrQAQWk0keaFW.htm)|Disarming Smile|
|[class-08-thNurw2OnN9jpBGV.htm](feats/class-08-thNurw2OnN9jpBGV.htm)|World-Wise Vigilance|
|[class-10-BV9k3nmVrWDLv8z6.htm](feats/class-10-BV9k3nmVrWDLv8z6.htm)|Emphatic Emissary|
|[skill-08-tjnL4wsriZugnHLn.htm](feats/skill-08-tjnL4wsriZugnHLn.htm)|Ilverani Purist|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[ancestry-01-0198lXWmDdrVWolN.htm](feats/ancestry-01-0198lXWmDdrVWolN.htm)|Wind Pillow|auto-trad|
|[ancestry-01-0DSCucjk9WZAw4xT.htm](feats/ancestry-01-0DSCucjk9WZAw4xT.htm)|Automaton Armament|auto-trad|
|[ancestry-01-0fTHqIqXCwGKiykR.htm](feats/ancestry-01-0fTHqIqXCwGKiykR.htm)|Sprite's Spark|auto-trad|
|[ancestry-01-0qeYP84FfQueggkx.htm](feats/ancestry-01-0qeYP84FfQueggkx.htm)|Hard Tail|auto-trad|
|[ancestry-01-0qshtd4mBcjFAxA8.htm](feats/ancestry-01-0qshtd4mBcjFAxA8.htm)|Cantorian Reinforcement|auto-trad|
|[ancestry-01-0YXRPqCaOQ3G73hh.htm](feats/ancestry-01-0YXRPqCaOQ3G73hh.htm)|Folksy Patter|auto-trad|
|[ancestry-01-13zaW8ZHRWnRe2pj.htm](feats/ancestry-01-13zaW8ZHRWnRe2pj.htm)|Hobgoblin Lore|auto-trad|
|[ancestry-01-1Doigqr1vBzg0tWU.htm](feats/ancestry-01-1Doigqr1vBzg0tWU.htm)|Silent Stone|auto-trad|
|[ancestry-01-1HsH8hE79MDsi8kK.htm](feats/ancestry-01-1HsH8hE79MDsi8kK.htm)|Orc Warmask|auto-trad|
|[ancestry-01-1MqvE1FL2mZRCnzo.htm](feats/ancestry-01-1MqvE1FL2mZRCnzo.htm)|Vestigial Wings|auto-trad|
|[ancestry-01-1newzNV5nkLvZ9KE.htm](feats/ancestry-01-1newzNV5nkLvZ9KE.htm)|Handy with your Paws|auto-trad|
|[ancestry-01-1r514EjD9YrZJ5rk.htm](feats/ancestry-01-1r514EjD9YrZJ5rk.htm)|Android Lore|auto-trad|
|[ancestry-01-1RygexXEjCKuR3Ps.htm](feats/ancestry-01-1RygexXEjCKuR3Ps.htm)|Undaunted|auto-trad|
|[ancestry-01-1tVC0mcxl8sTCg4U.htm](feats/ancestry-01-1tVC0mcxl8sTCg4U.htm)|Orc Weapon Familiarity|auto-trad|
|[ancestry-01-1xDMyQ8IHuhDHSXy.htm](feats/ancestry-01-1xDMyQ8IHuhDHSXy.htm)|Conrasu Weapon Familiarity|auto-trad|
|[ancestry-01-1XuytsxHDUMgyVH1.htm](feats/ancestry-01-1XuytsxHDUMgyVH1.htm)|Climbing Tail|auto-trad|
|[ancestry-01-23F2oqjL7SAMW3Ud.htm](feats/ancestry-01-23F2oqjL7SAMW3Ud.htm)|Ru-shi|auto-trad|
|[ancestry-01-254C4rqrE8APDfCf.htm](feats/ancestry-01-254C4rqrE8APDfCf.htm)|Orc Lore|auto-trad|
|[ancestry-01-2abNd2shBjF2kz2S.htm](feats/ancestry-01-2abNd2shBjF2kz2S.htm)|Undead Empathy|auto-trad|
|[ancestry-01-2bNd89jYmEO8wSay.htm](feats/ancestry-01-2bNd89jYmEO8wSay.htm)|Extra Squishy|auto-trad|
|[ancestry-01-2ebcYbg68pCZfAFQ.htm](feats/ancestry-01-2ebcYbg68pCZfAFQ.htm)|Halfling Weapon Familiarity|auto-trad|
|[ancestry-01-2XmdYW8OsAvjGDG3.htm](feats/ancestry-01-2XmdYW8OsAvjGDG3.htm)|Radiant Circuitry|auto-trad|
|[ancestry-01-30e9SdbmI3t5OJQE.htm](feats/ancestry-01-30e9SdbmI3t5OJQE.htm)|Deliberate Death|auto-trad|
|[ancestry-01-3J0NxeHcA1h9eToK.htm](feats/ancestry-01-3J0NxeHcA1h9eToK.htm)|Creative Prodigy|auto-trad|
|[ancestry-01-3zr5Gt5LgHsMNpSO.htm](feats/ancestry-01-3zr5Gt5LgHsMNpSO.htm)|Gnome Weapon Familiarity|auto-trad|
|[ancestry-01-48X3xSWxI20RPOr9.htm](feats/ancestry-01-48X3xSWxI20RPOr9.htm)|Burrow Elocutionist|auto-trad|
|[ancestry-01-49mBVNF4SK6iYdJm.htm](feats/ancestry-01-49mBVNF4SK6iYdJm.htm)|Unconventional Weaponry|auto-trad|
|[ancestry-01-4mLlHpD41101H0Iy.htm](feats/ancestry-01-4mLlHpD41101H0Iy.htm)|Changeling Lore|auto-trad|
|[ancestry-01-4NKyZVkmWjDyyIYZ.htm](feats/ancestry-01-4NKyZVkmWjDyyIYZ.htm)|Elven Weapon Familiarity|auto-trad|
|[ancestry-01-50CRpoP5XS1MVbu8.htm](feats/ancestry-01-50CRpoP5XS1MVbu8.htm)|Titan Slinger|auto-trad|
|[ancestry-01-5EpkOj9CFOjt8vsK.htm](feats/ancestry-01-5EpkOj9CFOjt8vsK.htm)|Eyes of the Night|auto-trad|
|[ancestry-01-5g7OFtvYQ7wPPJHC.htm](feats/ancestry-01-5g7OFtvYQ7wPPJHC.htm)|Fiendish Lore|auto-trad|
|[ancestry-01-5lZBacKOZOgIx4Pi.htm](feats/ancestry-01-5lZBacKOZOgIx4Pi.htm)|Eidetic Ear|auto-trad|
|[ancestry-01-5nPtGnXii1WTM5Wt.htm](feats/ancestry-01-5nPtGnXii1WTM5Wt.htm)|Faultspawn|auto-trad|
|[ancestry-01-5vipJDU5hGs0SejD.htm](feats/ancestry-01-5vipJDU5hGs0SejD.htm)|Hyena Familiar|auto-trad|
|[ancestry-01-5VkVWZq7ZqH6RaAW.htm](feats/ancestry-01-5VkVWZq7ZqH6RaAW.htm)|Voice Of The Night|auto-trad|
|[ancestry-01-5VWItcyt3Mx6mbSK.htm](feats/ancestry-01-5VWItcyt3Mx6mbSK.htm)|Grippli Lore|auto-trad|
|[ancestry-01-6dymMIqs3dAFqnyR.htm](feats/ancestry-01-6dymMIqs3dAFqnyR.htm)|Plumekith|auto-trad|
|[ancestry-01-6EgHimhCuS8L0xDE.htm](feats/ancestry-01-6EgHimhCuS8L0xDE.htm)|Explosive Savant|auto-trad|
|[ancestry-01-6ENHSzqg88J6dri6.htm](feats/ancestry-01-6ENHSzqg88J6dri6.htm)|Elemental Eyes|auto-trad|
|[ancestry-01-6oJ1EdANHgYCfCeF.htm](feats/ancestry-01-6oJ1EdANHgYCfCeF.htm)|Sharp Fangs|auto-trad|
|[ancestry-01-6ZNSMR0lRSLwBJBe.htm](feats/ancestry-01-6ZNSMR0lRSLwBJBe.htm)|Spelunker|auto-trad|
|[ancestry-01-7BVl9lFf0wuTjBgM.htm](feats/ancestry-01-7BVl9lFf0wuTjBgM.htm)|Elf Atavism|auto-trad|
|[ancestry-01-7lXCPeGMB3RrDVdS.htm](feats/ancestry-01-7lXCPeGMB3RrDVdS.htm)|Hard To Fool|auto-trad|
|[ancestry-01-7qBuzHY8kEG8SdEP.htm](feats/ancestry-01-7qBuzHY8kEG8SdEP.htm)|Cat's Luck|auto-trad|
|[ancestry-01-8DDwqrYHie33cf6d.htm](feats/ancestry-01-8DDwqrYHie33cf6d.htm)|Genie Weapon Familiarity|auto-trad|
|[ancestry-01-8nF3r3NHW2uSRgwb.htm](feats/ancestry-01-8nF3r3NHW2uSRgwb.htm)|Devil's Advocate|auto-trad|
|[ancestry-01-8nz6gvymeTvEfdo0.htm](feats/ancestry-01-8nz6gvymeTvEfdo0.htm)|Smashing Tail|auto-trad|
|[ancestry-01-8rKlptZ7ArtJ6cI8.htm](feats/ancestry-01-8rKlptZ7ArtJ6cI8.htm)|Vanth's Weapon Familiarity|auto-trad|
|[ancestry-01-95PmHIS041KiYIks.htm](feats/ancestry-01-95PmHIS041KiYIks.htm)|Reassuring Presence|auto-trad|
|[ancestry-01-9bnkx6VgcO5mOk5b.htm](feats/ancestry-01-9bnkx6VgcO5mOk5b.htm)|Dwarven Lore|auto-trad|
|[ancestry-01-9eDA2RLnrBBlCvce.htm](feats/ancestry-01-9eDA2RLnrBBlCvce.htm)|Catch the Details|auto-trad|
|[ancestry-01-9OdLdxvH5M9FyYSm.htm](feats/ancestry-01-9OdLdxvH5M9FyYSm.htm)|Corgi Mount|auto-trad|
|[ancestry-01-9Orkgjgfx8AILuqD.htm](feats/ancestry-01-9Orkgjgfx8AILuqD.htm)|Warren Navigator|auto-trad|
|[ancestry-01-9t4x8vRmQikEl4vP.htm](feats/ancestry-01-9t4x8vRmQikEl4vP.htm)|Well-Met Traveler|auto-trad|
|[ancestry-01-A1RzkLCj7mXJI1IY.htm](feats/ancestry-01-A1RzkLCj7mXJI1IY.htm)|Unassuming Dedication|auto-trad|
|[ancestry-01-A9HQ2bMAge2aGgWx.htm](feats/ancestry-01-A9HQ2bMAge2aGgWx.htm)|Prairie Rider|auto-trad|
|[ancestry-01-AAYN41WtyMZaLV1N.htm](feats/ancestry-01-AAYN41WtyMZaLV1N.htm)|Native Waters|auto-trad|
|[ancestry-01-af2fSePLvqMNNp0r.htm](feats/ancestry-01-af2fSePLvqMNNp0r.htm)|Wildborn Magic|auto-trad|
|[ancestry-01-AFXY5eM4IQuo5Ygj.htm](feats/ancestry-01-AFXY5eM4IQuo5Ygj.htm)|Speak With Bats|auto-trad|
|[ancestry-01-aGpH394T1xr2ey88.htm](feats/ancestry-01-aGpH394T1xr2ey88.htm)|Pack Hunter|auto-trad|
|[ancestry-01-AgR1OPBHDvwV5wKD.htm](feats/ancestry-01-AgR1OPBHDvwV5wKD.htm)|Unexpected Shift|auto-trad|
|[ancestry-01-AjdmoGFoSIyx1mxd.htm](feats/ancestry-01-AjdmoGFoSIyx1mxd.htm)|Intuitive Cooperation|auto-trad|
|[ancestry-01-akCO62yBCJLjjCZJ.htm](feats/ancestry-01-akCO62yBCJLjjCZJ.htm)|Celestial Eyes|auto-trad|
|[ancestry-01-akrj9t0lTrndmf0q.htm](feats/ancestry-01-akrj9t0lTrndmf0q.htm)|Canopy Sight|auto-trad|
|[ancestry-01-AKrxQ2JuDObM8coY.htm](feats/ancestry-01-AKrxQ2JuDObM8coY.htm)|Animal Senses|auto-trad|
|[ancestry-01-AL2KD88ddEl5AetZ.htm](feats/ancestry-01-AL2KD88ddEl5AetZ.htm)|Saber Teeth|auto-trad|
|[ancestry-01-AlhyF8LCW011w9kq.htm](feats/ancestry-01-AlhyF8LCW011w9kq.htm)|Bouncy Goblin|auto-trad|
|[ancestry-01-AMbYEv9rUt2fcR7F.htm](feats/ancestry-01-AMbYEv9rUt2fcR7F.htm)|Energy Beam|auto-trad|
|[ancestry-01-an8sq0RaJ8PW81EW.htm](feats/ancestry-01-an8sq0RaJ8PW81EW.htm)|Puncturing Horn|auto-trad|
|[ancestry-01-AogIo1gHLdz7DyHx.htm](feats/ancestry-01-AogIo1gHLdz7DyHx.htm)|Forlorn|auto-trad|
|[ancestry-01-aOhZUMCn0o2ZMkdW.htm](feats/ancestry-01-aOhZUMCn0o2ZMkdW.htm)|Crunch|auto-trad|
|[ancestry-01-AoibfCoLi2FaTvcH.htm](feats/ancestry-01-AoibfCoLi2FaTvcH.htm)|Hidden Thorn|auto-trad|
|[ancestry-01-ARBIhYAREcytOIaL.htm](feats/ancestry-01-ARBIhYAREcytOIaL.htm)|Helpful Poppet|auto-trad|
|[ancestry-01-arR15QoJH3xokw12.htm](feats/ancestry-01-arR15QoJH3xokw12.htm)|Wind Tempered|auto-trad|
|[ancestry-01-augNhQ51eSlZyead.htm](feats/ancestry-01-augNhQ51eSlZyead.htm)|Sneaky|auto-trad|
|[ancestry-01-b60ZCgKoaVDgMhBk.htm](feats/ancestry-01-b60ZCgKoaVDgMhBk.htm)|Kitsune Lore|auto-trad|
|[ancestry-01-bEh5qUgX5eFaQwzU.htm](feats/ancestry-01-bEh5qUgX5eFaQwzU.htm)|Catfolk Weapon Familiarity|auto-trad|
|[ancestry-01-BF5B2kDrSruQpqgS.htm](feats/ancestry-01-BF5B2kDrSruQpqgS.htm)|Pyrophilic Recovery|auto-trad|
|[ancestry-01-bLSd0y06QRjDYYMw.htm](feats/ancestry-01-bLSd0y06QRjDYYMw.htm)|Mental Sustenance|auto-trad|
|[ancestry-01-Bn9yK3FBndHkbVsu.htm](feats/ancestry-01-Bn9yK3FBndHkbVsu.htm)|Spine Stabber|auto-trad|
|[ancestry-01-Bni2NcuQn6Z546RE.htm](feats/ancestry-01-Bni2NcuQn6Z546RE.htm)|Twitchy|auto-trad|
|[ancestry-01-BNIAfaJPNCWKs3FN.htm](feats/ancestry-01-BNIAfaJPNCWKs3FN.htm)|Nocturnal Grippli|auto-trad|
|[ancestry-01-BoqMvGy1jXpsaBbo.htm](feats/ancestry-01-BoqMvGy1jXpsaBbo.htm)|Viking Shieldbearer|auto-trad|
|[ancestry-01-BRGdj5leyVFRHEUM.htm](feats/ancestry-01-BRGdj5leyVFRHEUM.htm)|Waxed Feathers|auto-trad|
|[ancestry-01-bWyY9NtLU5wXr03y.htm](feats/ancestry-01-bWyY9NtLU5wXr03y.htm)|Storm's Lash|auto-trad|
|[ancestry-01-C1R4wd6G46CAVIn7.htm](feats/ancestry-01-C1R4wd6G46CAVIn7.htm)|Unburdened Iron|auto-trad|
|[ancestry-01-Cb44J1g1nO43DEBd.htm](feats/ancestry-01-Cb44J1g1nO43DEBd.htm)|Ceremony of the Evened Hand|auto-trad|
|[ancestry-01-CCmiEmS7ZgyQUfhn.htm](feats/ancestry-01-CCmiEmS7ZgyQUfhn.htm)|Squawk!|auto-trad|
|[ancestry-01-ccWf2F5DqiqFwiQ1.htm](feats/ancestry-01-ccWf2F5DqiqFwiQ1.htm)|Gnome Polyglot|auto-trad|
|[ancestry-01-CF43oiymCFVVEkVS.htm](feats/ancestry-01-CF43oiymCFVVEkVS.htm)|Automaton Lore|auto-trad|
|[ancestry-01-ch3BWG3Z4kDEmuZW.htm](feats/ancestry-01-ch3BWG3Z4kDEmuZW.htm)|Alghollthu Bound|auto-trad|
|[ancestry-01-cilZUszwjSGB4p1W.htm](feats/ancestry-01-cilZUszwjSGB4p1W.htm)|Reinforced Chassis|auto-trad|
|[ancestry-01-cixf1uAQF2Y3w1Qx.htm](feats/ancestry-01-cixf1uAQF2Y3w1Qx.htm)|Nightvision Adaptation|auto-trad|
|[ancestry-01-ClVSyZxWk5L5KVLd.htm](feats/ancestry-01-ClVSyZxWk5L5KVLd.htm)|Aberration Kinship|auto-trad|
|[ancestry-01-csoGdGuWasEw3boD.htm](feats/ancestry-01-csoGdGuWasEw3boD.htm)|Saoc Astrology|auto-trad|
|[ancestry-01-CUrxG9CzT1hSfuhP.htm](feats/ancestry-01-CUrxG9CzT1hSfuhP.htm)|Web Weaver|auto-trad|
|[ancestry-01-CvtJ98EZvBGSCLOX.htm](feats/ancestry-01-CvtJ98EZvBGSCLOX.htm)|Grippli Weapon Familiarity|auto-trad|
|[ancestry-01-CXS0ryG2SODSobm9.htm](feats/ancestry-01-CXS0ryG2SODSobm9.htm)|Dwarven Weapon Familiarity|auto-trad|
|[ancestry-01-d1FTY5ai9KjpkX59.htm](feats/ancestry-01-d1FTY5ai9KjpkX59.htm)|Star Orb|auto-trad|
|[ancestry-01-D2tyUKQiDSq3JO1Z.htm](feats/ancestry-01-D2tyUKQiDSq3JO1Z.htm)|Vigorous Health|auto-trad|
|[ancestry-01-D3SuA5MaKucO1flE.htm](feats/ancestry-01-D3SuA5MaKucO1flE.htm)|General Training|auto-trad|
|[ancestry-01-d8SK0BQmTZiJ0VT7.htm](feats/ancestry-01-d8SK0BQmTZiJ0VT7.htm)|Community Knowledge|auto-trad|
|[ancestry-01-db0RHtFGhCfMx8vT.htm](feats/ancestry-01-db0RHtFGhCfMx8vT.htm)|Internal Cohesion|auto-trad|
|[ancestry-01-DGtUIMliflzGXc6E.htm](feats/ancestry-01-DGtUIMliflzGXc6E.htm)|Ghost Hunter|auto-trad|
|[ancestry-01-DmcJtpMBSh3R5MHI.htm](feats/ancestry-01-DmcJtpMBSh3R5MHI.htm)|Quick Shape|auto-trad|
|[ancestry-01-dmEMftRe8P5iPDKo.htm](feats/ancestry-01-dmEMftRe8P5iPDKo.htm)|Brinesoul|auto-trad|
|[ancestry-01-Dq4JSejEdCzGNeTc.htm](feats/ancestry-01-Dq4JSejEdCzGNeTc.htm)|Arcane Tattoos|auto-trad|
|[ancestry-01-Ds1waj86N4Z2gCMN.htm](feats/ancestry-01-Ds1waj86N4Z2gCMN.htm)|Winter Cat Senses|auto-trad|
|[ancestry-01-dWGa6cFSVrASTEfd.htm](feats/ancestry-01-dWGa6cFSVrASTEfd.htm)|Seedpod|auto-trad|
|[ancestry-01-DwnnmTNOvpLbp7jJ.htm](feats/ancestry-01-DwnnmTNOvpLbp7jJ.htm)|Courteous Comeback|auto-trad|
|[ancestry-01-e0Gz3tjd55A5ggYK.htm](feats/ancestry-01-e0Gz3tjd55A5ggYK.htm)|Alabaster Eyes|auto-trad|
|[ancestry-01-E1sjYRb4zsZVrzrN.htm](feats/ancestry-01-E1sjYRb4zsZVrzrN.htm)|Innocuous|auto-trad|
|[ancestry-01-E5TRKeB63rC910PC.htm](feats/ancestry-01-E5TRKeB63rC910PC.htm)|Nagaji Lore|auto-trad|
|[ancestry-01-ecJms2jtd6cZ5rQK.htm](feats/ancestry-01-ecJms2jtd6cZ5rQK.htm)|Round Ears|auto-trad|
|[ancestry-01-eCWQU16hRLfN1KaX.htm](feats/ancestry-01-eCWQU16hRLfN1KaX.htm)|Ancestral Linguistics|auto-trad|
|[ancestry-01-eD3KZJV8ACLt2xns.htm](feats/ancestry-01-eD3KZJV8ACLt2xns.htm)|Elven Verve|auto-trad|
|[ancestry-01-ED58GzldWb82yc2q.htm](feats/ancestry-01-ED58GzldWb82yc2q.htm)|Animal Accomplice|auto-trad|
|[ancestry-01-EEJKztPOpy5utha9.htm](feats/ancestry-01-EEJKztPOpy5utha9.htm)|Cleansing Subroutine|auto-trad|
|[ancestry-01-eEVZ19G2BAnHQZpa.htm](feats/ancestry-01-eEVZ19G2BAnHQZpa.htm)|Emotionless|auto-trad|
|[ancestry-01-EFhh3AenX7wtAmrs.htm](feats/ancestry-01-EFhh3AenX7wtAmrs.htm)|Orc Superstition|auto-trad|
|[ancestry-01-ehvHedIwPcxq9cRt.htm](feats/ancestry-01-ehvHedIwPcxq9cRt.htm)|Sure Feet|auto-trad|
|[ancestry-01-eMmdBpbMrpIuGowo.htm](feats/ancestry-01-eMmdBpbMrpIuGowo.htm)|Snare Setter|auto-trad|
|[ancestry-01-enRKfPyCLU5FMUOX.htm](feats/ancestry-01-enRKfPyCLU5FMUOX.htm)|Proximity Alert|auto-trad|
|[ancestry-01-eqBWab1J5Be24YAl.htm](feats/ancestry-01-eqBWab1J5Be24YAl.htm)|Catfolk Lore|auto-trad|
|[ancestry-01-eSP2938INGUG9b3w.htm](feats/ancestry-01-eSP2938INGUG9b3w.htm)|Musetouched|auto-trad|
|[ancestry-01-eUyZBi8vV5QDxOXD.htm](feats/ancestry-01-eUyZBi8vV5QDxOXD.htm)|Form Of The Fiend|auto-trad|
|[ancestry-01-eWUTE7Ln3MwX6uer.htm](feats/ancestry-01-eWUTE7Ln3MwX6uer.htm)|Axiomatic Lore|auto-trad|
|[ancestry-01-EXVHePH8alsTZ5TB.htm](feats/ancestry-01-EXVHePH8alsTZ5TB.htm)|Hellspawn|auto-trad|
|[ancestry-01-eZsMl6rx8Bv6Ccnp.htm](feats/ancestry-01-eZsMl6rx8Bv6Ccnp.htm)|Shiny Button Eyes|auto-trad|
|[ancestry-01-F6C25qZ9UNYPw7pj.htm](feats/ancestry-01-F6C25qZ9UNYPw7pj.htm)|Critter Shape|auto-trad|
|[ancestry-01-f6VPqhOPW9XfBKDr.htm](feats/ancestry-01-f6VPqhOPW9XfBKDr.htm)|Virga May|auto-trad|
|[ancestry-01-Ffi8L4EDO5OH5tpA.htm](feats/ancestry-01-Ffi8L4EDO5OH5tpA.htm)|Nimble Hooves|auto-trad|
|[ancestry-01-FhCsnHjdIUyKteCM.htm](feats/ancestry-01-FhCsnHjdIUyKteCM.htm)|Lizardfolk Lore|auto-trad|
|[ancestry-01-FHK4ZEl8SkGOpKdF.htm](feats/ancestry-01-FHK4ZEl8SkGOpKdF.htm)|Mirror-Risen|auto-trad|
|[ancestry-01-fLDhS0e6fBDjiCUA.htm](feats/ancestry-01-fLDhS0e6fBDjiCUA.htm)|Bone Magic|auto-trad|
|[ancestry-01-FnGOkNyLF4w3FyqZ.htm](feats/ancestry-01-FnGOkNyLF4w3FyqZ.htm)|Fang Sharpener|auto-trad|
|[ancestry-01-FWCULKnVXhSPL0ST.htm](feats/ancestry-01-FWCULKnVXhSPL0ST.htm)|Fey Fellowship|auto-trad|
|[ancestry-01-FwMTsYc87uU2q4Ox.htm](feats/ancestry-01-FwMTsYc87uU2q4Ox.htm)|Beastbrood|auto-trad|
|[ancestry-01-fYVFBnv9aVHv1UNg.htm](feats/ancestry-01-fYVFBnv9aVHv1UNg.htm)|Leshy Superstition|auto-trad|
|[ancestry-01-FZuQPFnQ5UkBWLU9.htm](feats/ancestry-01-FZuQPFnQ5UkBWLU9.htm)|Steady On Stone|auto-trad|
|[ancestry-01-g388ImzpenYBoiEF.htm](feats/ancestry-01-g388ImzpenYBoiEF.htm)|City Scavenger|auto-trad|
|[ancestry-01-g3oJlWGHc74qX2z5.htm](feats/ancestry-01-g3oJlWGHc74qX2z5.htm)|Lawbringer|auto-trad|
|[ancestry-01-G8WgbujrrnMQUQ8E.htm](feats/ancestry-01-G8WgbujrrnMQUQ8E.htm)|Share Thoughts|auto-trad|
|[ancestry-01-gC5EnlP38t1vTlWt.htm](feats/ancestry-01-gC5EnlP38t1vTlWt.htm)|Esteemed Visitor|auto-trad|
|[ancestry-01-GEvaoKgQteMrd4ub.htm](feats/ancestry-01-GEvaoKgQteMrd4ub.htm)|Reptile Speaker|auto-trad|
|[ancestry-01-GF9kkULUYowgjEWM.htm](feats/ancestry-01-GF9kkULUYowgjEWM.htm)|Rough Rider|auto-trad|
|[ancestry-01-gJDTS7eeIxZws5Lr.htm](feats/ancestry-01-gJDTS7eeIxZws5Lr.htm)|Adhyabhau|auto-trad|
|[ancestry-01-GjQZMmw2sz8OyLxj.htm](feats/ancestry-01-GjQZMmw2sz8OyLxj.htm)|Tengu Lore|auto-trad|
|[ancestry-01-gKDRnsBPBdhJB0FI.htm](feats/ancestry-01-gKDRnsBPBdhJB0FI.htm)|Avenge in Glory|auto-trad|
|[ancestry-01-gozOYxLVx7PQvOSj.htm](feats/ancestry-01-gozOYxLVx7PQvOSj.htm)|Fey Cantrips|auto-trad|
|[ancestry-01-GPsYtPdkAkk710F3.htm](feats/ancestry-01-GPsYtPdkAkk710F3.htm)|Suli-jann|auto-trad|
|[ancestry-01-gsDDw5KAb7qlPkms.htm](feats/ancestry-01-gsDDw5KAb7qlPkms.htm)|Shrouded Magic|auto-trad|
|[ancestry-01-gxQheZ4xuDWwyzy4.htm](feats/ancestry-01-gxQheZ4xuDWwyzy4.htm)|Pitborn|auto-trad|
|[ancestry-01-GYdLf1LhvSUeu95Y.htm](feats/ancestry-01-GYdLf1LhvSUeu95Y.htm)|Harmlessly Cute|auto-trad|
|[ancestry-01-H95Gh2nKUp9NKFuR.htm](feats/ancestry-01-H95Gh2nKUp9NKFuR.htm)|Shadow Blending|auto-trad|
|[ancestry-01-HAMy8UiUqCGFdhrf.htm](feats/ancestry-01-HAMy8UiUqCGFdhrf.htm)|Lemma Of Vision|auto-trad|
|[ancestry-01-HauCZuLvIthFe2we.htm](feats/ancestry-01-HauCZuLvIthFe2we.htm)|Draconic Sycophant|auto-trad|
|[ancestry-01-hc4lWhWekIVb0wjL.htm](feats/ancestry-01-hc4lWhWekIVb0wjL.htm)|Pack Rat|auto-trad|
|[ancestry-01-Hey5rucsel7apOOi.htm](feats/ancestry-01-Hey5rucsel7apOOi.htm)|Goloma Courage|auto-trad|
|[ancestry-01-hkMQGiTJCLVAWHy0.htm](feats/ancestry-01-hkMQGiTJCLVAWHy0.htm)|Surface Culture|auto-trad|
|[ancestry-01-Hmgy0GJKIawAiqHE.htm](feats/ancestry-01-Hmgy0GJKIawAiqHE.htm)|Maiden's Mending|auto-trad|
|[ancestry-01-HOUHs5rahwIsQoBf.htm](feats/ancestry-01-HOUHs5rahwIsQoBf.htm)|Stonecunning|auto-trad|
|[ancestry-01-HOXxEa7sAeAxpKHb.htm](feats/ancestry-01-HOXxEa7sAeAxpKHb.htm)|Strix Lore|auto-trad|
|[ancestry-01-hSzNtRNwrma81Eeq.htm](feats/ancestry-01-hSzNtRNwrma81Eeq.htm)|Life-Giving Magic|auto-trad|
|[ancestry-01-HTy9bQsVkKnS8bLT.htm](feats/ancestry-01-HTy9bQsVkKnS8bLT.htm)|As in Life, So in Death|auto-trad|
|[ancestry-01-hxCqQPjlyVI57vQt.htm](feats/ancestry-01-hxCqQPjlyVI57vQt.htm)|Gravesight|auto-trad|
|[ancestry-01-Hy8SPadSjukKq078.htm](feats/ancestry-01-Hy8SPadSjukKq078.htm)|Sinister Appearance|auto-trad|
|[ancestry-01-HZJqMESaEHTfefz3.htm](feats/ancestry-01-HZJqMESaEHTfefz3.htm)|Cindersoul|auto-trad|
|[ancestry-01-i0iyW1I7TylEgpV6.htm](feats/ancestry-01-i0iyW1I7TylEgpV6.htm)|Vampire Lore|auto-trad|
|[ancestry-01-i5W5aGWEiyo1vt2d.htm](feats/ancestry-01-i5W5aGWEiyo1vt2d.htm)|Leshy Lore|auto-trad|
|[ancestry-01-IAXWdFXwjFChojeb.htm](feats/ancestry-01-IAXWdFXwjFChojeb.htm)|Harmless Doll|auto-trad|
|[ancestry-01-ICLUKJc9P0LgwVyt.htm](feats/ancestry-01-ICLUKJc9P0LgwVyt.htm)|Brightsoul|auto-trad|
|[ancestry-01-IElFaS5i10MFYIvq.htm](feats/ancestry-01-IElFaS5i10MFYIvq.htm)|Kobold Lore|auto-trad|
|[ancestry-01-IFvjnLMw3ht8f84U.htm](feats/ancestry-01-IFvjnLMw3ht8f84U.htm)|Callow May|auto-trad|
|[ancestry-01-Iqa96LHn3Bs2xEJA.htm](feats/ancestry-01-Iqa96LHn3Bs2xEJA.htm)|Startling Appearance (Fleshwarp)|auto-trad|
|[ancestry-01-Iqv8qj7ym63YjexN.htm](feats/ancestry-01-Iqv8qj7ym63YjexN.htm)|Catfolk Dance|auto-trad|
|[ancestry-01-iRztLGEK5OfjZTPg.htm](feats/ancestry-01-iRztLGEK5OfjZTPg.htm)|Remorseless Lash|auto-trad|
|[ancestry-01-itjTPh76mZfJCBxQ.htm](feats/ancestry-01-itjTPh76mZfJCBxQ.htm)|Marsh Runner|auto-trad|
|[ancestry-01-izuErgHfh90KctAL.htm](feats/ancestry-01-izuErgHfh90KctAL.htm)|Fetchling Lore|auto-trad|
|[ancestry-01-j54VJmwwAQZBlS6J.htm](feats/ancestry-01-j54VJmwwAQZBlS6J.htm)|Anadi Lore|auto-trad|
|[ancestry-01-J7BtszszVxpETMD7.htm](feats/ancestry-01-J7BtszszVxpETMD7.htm)|Retractable Claws|auto-trad|
|[ancestry-01-jEJ6AWCctirMT7p0.htm](feats/ancestry-01-jEJ6AWCctirMT7p0.htm)|Brine May|auto-trad|
|[ancestry-01-jmW8aZ5JGH3m6dL6.htm](feats/ancestry-01-jmW8aZ5JGH3m6dL6.htm)|Warren Friend|auto-trad|
|[ancestry-01-jNDjLsqpq13RqzD4.htm](feats/ancestry-01-jNDjLsqpq13RqzD4.htm)|Studious Magic|auto-trad|
|[ancestry-01-JoeepCWheQChcQ9s.htm](feats/ancestry-01-JoeepCWheQChcQ9s.htm)|Lavasoul|auto-trad|
|[ancestry-01-jOwZMv5jtwdKSfLS.htm](feats/ancestry-01-jOwZMv5jtwdKSfLS.htm)|Sensitive Nose|auto-trad|
|[ancestry-01-JP5pptkl1Fx1JK4m.htm](feats/ancestry-01-JP5pptkl1Fx1JK4m.htm)|Iron Fists|auto-trad|
|[ancestry-01-JQTP0XdI1XVAvBIn.htm](feats/ancestry-01-JQTP0XdI1XVAvBIn.htm)|Ghoran Lore|auto-trad|
|[ancestry-01-JS24EjgLYcHB9E3T.htm](feats/ancestry-01-JS24EjgLYcHB9E3T.htm)|Nanite Surge|auto-trad|
|[ancestry-01-jxUPlkB2kFuZKXef.htm](feats/ancestry-01-jxUPlkB2kFuZKXef.htm)|Lesser Enhance Venom|auto-trad|
|[ancestry-01-k31sg0xBIwvkfWyg.htm](feats/ancestry-01-k31sg0xBIwvkfWyg.htm)|Reptile Rider|auto-trad|
|[ancestry-01-kBxgo589ctJsBwJj.htm](feats/ancestry-01-kBxgo589ctJsBwJj.htm)|Conrasu Lore|auto-trad|
|[ancestry-01-kCO4r8NOm8E2T3eH.htm](feats/ancestry-01-kCO4r8NOm8E2T3eH.htm)|Nimble Elf|auto-trad|
|[ancestry-01-kDklfrprKTuTpcEE.htm](feats/ancestry-01-kDklfrprKTuTpcEE.htm)|Shadow of the Wilds|auto-trad|
|[ancestry-01-keMP6xVg4fMloczj.htm](feats/ancestry-01-keMP6xVg4fMloczj.htm)|Cel Rau|auto-trad|
|[ancestry-01-kJfLBPtiVi5LQu0v.htm](feats/ancestry-01-kJfLBPtiVi5LQu0v.htm)|Nocturnal Charm|auto-trad|
|[ancestry-01-KKUDZUbX3nDdME4K.htm](feats/ancestry-01-KKUDZUbX3nDdME4K.htm)|Watchful Gaze|auto-trad|
|[ancestry-01-kqnFdIhToKTnOpMl.htm](feats/ancestry-01-kqnFdIhToKTnOpMl.htm)|Dream May|auto-trad|
|[ancestry-01-kqRFoXfErUFEndIs.htm](feats/ancestry-01-kqRFoXfErUFEndIs.htm)|Hydraulic Deflection|auto-trad|
|[ancestry-01-KQVE4FsDd9RFpWRz.htm](feats/ancestry-01-KQVE4FsDd9RFpWRz.htm)|Improvisational Defender|auto-trad|
|[ancestry-01-KRMdfPcNCE7AVsEo.htm](feats/ancestry-01-KRMdfPcNCE7AVsEo.htm)|Scholar's Inheritance|auto-trad|
|[ancestry-01-KuD4Yplwcdolhjsu.htm](feats/ancestry-01-KuD4Yplwcdolhjsu.htm)|Overlooked Mastermind|auto-trad|
|[ancestry-01-KXUVAI6SDtxwjO7t.htm](feats/ancestry-01-KXUVAI6SDtxwjO7t.htm)|Web Walker|auto-trad|
|[ancestry-01-KYTSvAEqK7KAyVwi.htm](feats/ancestry-01-KYTSvAEqK7KAyVwi.htm)|Hunter's Defense|auto-trad|
|[ancestry-01-Kz4MLcrfXFrdKhyS.htm](feats/ancestry-01-Kz4MLcrfXFrdKhyS.htm)|Aquatic Eyes|auto-trad|
|[ancestry-01-l0VBbymCVT1Qz9t9.htm](feats/ancestry-01-l0VBbymCVT1Qz9t9.htm)|Swift|auto-trad|
|[ancestry-01-L7fhh2RTCq4FlVSN.htm](feats/ancestry-01-L7fhh2RTCq4FlVSN.htm)|Strix Defender|auto-trad|
|[ancestry-01-Lb8mrOF3W2VGSOpA.htm](feats/ancestry-01-Lb8mrOF3W2VGSOpA.htm)|Arcane Communication|auto-trad|
|[ancestry-01-LbyNDCxFEkjp0iG7.htm](feats/ancestry-01-LbyNDCxFEkjp0iG7.htm)|Slink|auto-trad|
|[ancestry-01-liqDAd5xk2b3xzeE.htm](feats/ancestry-01-liqDAd5xk2b3xzeE.htm)|Quah Bond|auto-trad|
|[ancestry-01-lLO0WSxE3tO3Ovsq.htm](feats/ancestry-01-lLO0WSxE3tO3Ovsq.htm)|Whitecape|auto-trad|
|[ancestry-01-LLQ5SO2c44uXDFJk.htm](feats/ancestry-01-LLQ5SO2c44uXDFJk.htm)|Tail Whip|auto-trad|
|[ancestry-01-lqhhvDJAjPw6LZy5.htm](feats/ancestry-01-lqhhvDJAjPw6LZy5.htm)|Smokesoul|auto-trad|
|[ancestry-01-Lug2p9E065L05Rhi.htm](feats/ancestry-01-Lug2p9E065L05Rhi.htm)|Story Crooner|auto-trad|
|[ancestry-01-LUXBuTAuK1glHOkJ.htm](feats/ancestry-01-LUXBuTAuK1glHOkJ.htm)|Fiendish Eyes|auto-trad|
|[ancestry-01-LvVg83ZDj8mabcWF.htm](feats/ancestry-01-LvVg83ZDj8mabcWF.htm)|Clan Pistol|auto-trad|
|[ancestry-01-lwLcUHQMOqfaNND4.htm](feats/ancestry-01-lwLcUHQMOqfaNND4.htm)|Cooperative Nature|auto-trad|
|[ancestry-01-lwzspz4dktZLgqlY.htm](feats/ancestry-01-lwzspz4dktZLgqlY.htm)|Wash Out|auto-trad|
|[ancestry-01-M41PGiFlLE2tByUL.htm](feats/ancestry-01-M41PGiFlLE2tByUL.htm)|Jungle Strider|auto-trad|
|[ancestry-01-M5195FvfCN3X7gi9.htm](feats/ancestry-01-M5195FvfCN3X7gi9.htm)|Tusks (Orc)|auto-trad|
|[ancestry-01-m9tKNsZQQjHdsmEN.htm](feats/ancestry-01-m9tKNsZQQjHdsmEN.htm)|Tupilaq Carver|auto-trad|
|[ancestry-01-mAAlean6DuWd3wDT.htm](feats/ancestry-01-mAAlean6DuWd3wDT.htm)|Open Mind|auto-trad|
|[ancestry-01-MbvPuMVy2VhftJgd.htm](feats/ancestry-01-MbvPuMVy2VhftJgd.htm)|Perfect Dive|auto-trad|
|[ancestry-01-mmYAiK3x0UMcgiNv.htm](feats/ancestry-01-mmYAiK3x0UMcgiNv.htm)|Dualborn|auto-trad|
|[ancestry-01-MqDLaBypDyp1VQKg.htm](feats/ancestry-01-MqDLaBypDyp1VQKg.htm)|Shisk Lore|auto-trad|
|[ancestry-01-mQYO501xyMgtIQ3W.htm](feats/ancestry-01-mQYO501xyMgtIQ3W.htm)|Ratfolk Lore|auto-trad|
|[ancestry-01-MS53Ds75BT379ZFm.htm](feats/ancestry-01-MS53Ds75BT379ZFm.htm)|Empathetic Plea|auto-trad|
|[ancestry-01-mW9uhe3RorEJg6Mn.htm](feats/ancestry-01-mW9uhe3RorEJg6Mn.htm)|Nagaji Spell Familiarity|auto-trad|
|[ancestry-01-mxhGH4FVYXJwb0BC.htm](feats/ancestry-01-mxhGH4FVYXJwb0BC.htm)|Gnome Obsession|auto-trad|
|[ancestry-01-MXkklchuimVSHZyd.htm](feats/ancestry-01-MXkklchuimVSHZyd.htm)|Theoretical Acumen|auto-trad|
|[ancestry-01-N3xyK9keDm00oUY6.htm](feats/ancestry-01-N3xyK9keDm00oUY6.htm)|Inner Fire (Ifrit)|auto-trad|
|[ancestry-01-n7JXquVGxQOfrCsf.htm](feats/ancestry-01-n7JXquVGxQOfrCsf.htm)|Ancestral Insight|auto-trad|
|[ancestry-01-n9CBjyiB17srkyr4.htm](feats/ancestry-01-n9CBjyiB17srkyr4.htm)|Hobgoblin Weapon Familiarity|auto-trad|
|[ancestry-01-nB8BD9rIg9hfFGns.htm](feats/ancestry-01-nB8BD9rIg9hfFGns.htm)|Eye For Treasure|auto-trad|
|[ancestry-01-nbWkPLiUxagX8dCw.htm](feats/ancestry-01-nbWkPLiUxagX8dCw.htm)|Shapechanger's Intuition|auto-trad|
|[ancestry-01-nfERPRCITBp970HO.htm](feats/ancestry-01-nfERPRCITBp970HO.htm)|Earned Glory|auto-trad|
|[ancestry-01-ng9H9flz4H6agiiV.htm](feats/ancestry-01-ng9H9flz4H6agiiV.htm)|Demonbane Warrior|auto-trad|
|[ancestry-01-ngEd8PgsyAARuTQ9.htm](feats/ancestry-01-ngEd8PgsyAARuTQ9.htm)|Vengeful Hatred|auto-trad|
|[ancestry-01-NIUSBGMmdqhkYtmo.htm](feats/ancestry-01-NIUSBGMmdqhkYtmo.htm)|Rock Runner|auto-trad|
|[ancestry-01-NN1U8ifiURc0h4Fx.htm](feats/ancestry-01-NN1U8ifiURc0h4Fx.htm)|Pierce the Darkness|auto-trad|
|[ancestry-01-Nn80wBZYJZxcuKsJ.htm](feats/ancestry-01-Nn80wBZYJZxcuKsJ.htm)|Stone Face|auto-trad|
|[ancestry-01-nnlzI9LSvhl94U16.htm](feats/ancestry-01-nnlzI9LSvhl94U16.htm)|Scuttle Up|auto-trad|
|[ancestry-01-nqtO5dNnxT4nZDbB.htm](feats/ancestry-01-nqtO5dNnxT4nZDbB.htm)|Surface Skimmer|auto-trad|
|[ancestry-01-NYcgVwO4xLerJ9lO.htm](feats/ancestry-01-NYcgVwO4xLerJ9lO.htm)|Progenitor Lore|auto-trad|
|[ancestry-01-nyhQ9xB0rkoAoNbf.htm](feats/ancestry-01-nyhQ9xB0rkoAoNbf.htm)|Grimspawn|auto-trad|
|[ancestry-01-NySPRgD0FjZY2QGs.htm](feats/ancestry-01-NySPRgD0FjZY2QGs.htm)|Consult The Stars|auto-trad|
|[ancestry-01-o4LycqplO14zn6It.htm](feats/ancestry-01-o4LycqplO14zn6It.htm)|Know Your Own|auto-trad|
|[ancestry-01-O5v8yaeCbjKeXfyi.htm](feats/ancestry-01-O5v8yaeCbjKeXfyi.htm)|Duskwalker Lore|auto-trad|
|[ancestry-01-o6MXyjSgavTzU5AS.htm](feats/ancestry-01-o6MXyjSgavTzU5AS.htm)|Gemsoul|auto-trad|
|[ancestry-01-oeGowXO2P6rHbZfY.htm](feats/ancestry-01-oeGowXO2P6rHbZfY.htm)|Very Sneaky|auto-trad|
|[ancestry-01-oeVRwtBlQjsSVtXV.htm](feats/ancestry-01-oeVRwtBlQjsSVtXV.htm)|Ghoran Weapon Familiarity|auto-trad|
|[ancestry-01-ojp39fVYqFBGAw38.htm](feats/ancestry-01-ojp39fVYqFBGAw38.htm)|Natural Performer|auto-trad|
|[ancestry-01-ojykhaQkT8IU7ouc.htm](feats/ancestry-01-ojykhaQkT8IU7ouc.htm)|Emotional Partitions|auto-trad|
|[ancestry-01-OKSsFlHY5UKc4dKu.htm](feats/ancestry-01-OKSsFlHY5UKc4dKu.htm)|Elemental Assault|auto-trad|
|[ancestry-01-OqGNcUTqaZTp2YND.htm](feats/ancestry-01-OqGNcUTqaZTp2YND.htm)|Molten Wit|auto-trad|
|[ancestry-01-oQzyeSUlKx6eHcZp.htm](feats/ancestry-01-oQzyeSUlKx6eHcZp.htm)|Vanara Weapon Familiarity|auto-trad|
|[ancestry-01-OTjRxyCtwPoqNFP2.htm](feats/ancestry-01-OTjRxyCtwPoqNFP2.htm)|Kobold Weapon Familiarity|auto-trad|
|[ancestry-01-OYjzfTeWU7RJBT7v.htm](feats/ancestry-01-OYjzfTeWU7RJBT7v.htm)|Goblin Weapon Familiarity|auto-trad|
|[ancestry-01-OZtoTusMmCJymObT.htm](feats/ancestry-01-OZtoTusMmCJymObT.htm)|Leech-Clipper|auto-trad|
|[ancestry-01-P1dk0LTWkQ1LT1ai.htm](feats/ancestry-01-P1dk0LTWkQ1LT1ai.htm)|Svetocher|auto-trad|
|[ancestry-01-Pat4H0VbmApblZxc.htm](feats/ancestry-01-Pat4H0VbmApblZxc.htm)|Otherworldly Magic|auto-trad|
|[ancestry-01-pCck5GgKIiIPGGdy.htm](feats/ancestry-01-pCck5GgKIiIPGGdy.htm)|Veil May|auto-trad|
|[ancestry-01-pgsSUJehsKZXlRp7.htm](feats/ancestry-01-pgsSUJehsKZXlRp7.htm)|Vishkanya Lore|auto-trad|
|[ancestry-01-PGVXjbAi1Fa4uTmD.htm](feats/ancestry-01-PGVXjbAi1Fa4uTmD.htm)|Halo|auto-trad|
|[ancestry-01-PlhPpdwIV0rIAJ8K.htm](feats/ancestry-01-PlhPpdwIV0rIAJ8K.htm)|Orc Ferocity|auto-trad|
|[ancestry-01-PMRfunXzC9YizHNZ.htm](feats/ancestry-01-PMRfunXzC9YizHNZ.htm)|Riftmarked|auto-trad|
|[ancestry-01-PodajLVxqYSAqVox.htm](feats/ancestry-01-PodajLVxqYSAqVox.htm)|Natural Ambition|auto-trad|
|[ancestry-01-Pox93XMBaFmeLIDM.htm](feats/ancestry-01-Pox93XMBaFmeLIDM.htm)|Cheek Pouches|auto-trad|
|[ancestry-01-pP4pbFBg5GAgcOE9.htm](feats/ancestry-01-pP4pbFBg5GAgcOE9.htm)|Snow May|auto-trad|
|[ancestry-01-pQQhrcj1u6hUUc8L.htm](feats/ancestry-01-pQQhrcj1u6hUUc8L.htm)|Keep Up Appearances|auto-trad|
|[ancestry-01-PsLne80WUsD4IFa6.htm](feats/ancestry-01-PsLne80WUsD4IFa6.htm)|Dig Quickly|auto-trad|
|[ancestry-01-ptEOt3lqjxUnAW62.htm](feats/ancestry-01-ptEOt3lqjxUnAW62.htm)|Ancient Memories|auto-trad|
|[ancestry-01-PVkAEBlRSJHe3JCz.htm](feats/ancestry-01-PVkAEBlRSJHe3JCz.htm)|Straveika|auto-trad|
|[ancestry-01-Qb25uu1gT5CDMSWb.htm](feats/ancestry-01-Qb25uu1gT5CDMSWb.htm)|Hag Claws|auto-trad|
|[ancestry-01-qCV04rZMty2TJBrX.htm](feats/ancestry-01-qCV04rZMty2TJBrX.htm)|Foxfire|auto-trad|
|[ancestry-01-QHwajD5n8P3oS9Wb.htm](feats/ancestry-01-QHwajD5n8P3oS9Wb.htm)|Vicious Incisors|auto-trad|
|[ancestry-01-qm1lIMLVdsQtVFT0.htm](feats/ancestry-01-qm1lIMLVdsQtVFT0.htm)|Stormsoul|auto-trad|
|[ancestry-01-qpVB9F9DURW4Lti1.htm](feats/ancestry-01-qpVB9F9DURW4Lti1.htm)|Uncanny Agility|auto-trad|
|[ancestry-01-QsPvKvt4S1PR7kr7.htm](feats/ancestry-01-QsPvKvt4S1PR7kr7.htm)|Elemental Trade|auto-trad|
|[ancestry-01-qT68NpouO667sUMA.htm](feats/ancestry-01-qT68NpouO667sUMA.htm)|Sudden Mindfulness|auto-trad|
|[ancestry-01-qVk9htKv47niPmXS.htm](feats/ancestry-01-qVk9htKv47niPmXS.htm)|Deep Vision|auto-trad|
|[ancestry-01-qvxHGcOUYVG7Tqt4.htm](feats/ancestry-01-qvxHGcOUYVG7Tqt4.htm)|Dongun Education|auto-trad|
|[ancestry-01-qxQf0HiYBh26mCMT.htm](feats/ancestry-01-qxQf0HiYBh26mCMT.htm)|Shrouded Mien|auto-trad|
|[ancestry-01-QygsOzswFQEYONO3.htm](feats/ancestry-01-QygsOzswFQEYONO3.htm)|Shackleborn|auto-trad|
|[ancestry-01-qzalwa2Ze3dIqIrA.htm](feats/ancestry-01-qzalwa2Ze3dIqIrA.htm)|Lightning Tongue|auto-trad|
|[ancestry-01-QZb0Utg0WFPf2Qg0.htm](feats/ancestry-01-QZb0Utg0WFPf2Qg0.htm)|Goblin Song|auto-trad|
|[ancestry-01-qZStZXyAj6kSAKQo.htm](feats/ancestry-01-qZStZXyAj6kSAKQo.htm)|Vanara Lore|auto-trad|
|[ancestry-01-Rfyhlyql1GSoDkI2.htm](feats/ancestry-01-Rfyhlyql1GSoDkI2.htm)|Natural Skill|auto-trad|
|[ancestry-01-RJClD7YYsAFutxNs.htm](feats/ancestry-01-RJClD7YYsAFutxNs.htm)|Elemental Lore|auto-trad|
|[ancestry-01-RmiMUZlae6yGUyXY.htm](feats/ancestry-01-RmiMUZlae6yGUyXY.htm)|Haughty Obstinacy|auto-trad|
|[ancestry-01-roC6o0xJ8hDzHIWY.htm](feats/ancestry-01-roC6o0xJ8hDzHIWY.htm)|Serpent's Tongue|auto-trad|
|[ancestry-01-rswkPvZvEBXISH96.htm](feats/ancestry-01-rswkPvZvEBXISH96.htm)|Rimesoul|auto-trad|
|[ancestry-01-rWiddU8fHl0TraoN.htm](feats/ancestry-01-rWiddU8fHl0TraoN.htm)|Gnoll Lore|auto-trad|
|[ancestry-01-ryRpqixE7cN8svwB.htm](feats/ancestry-01-ryRpqixE7cN8svwB.htm)|Evanescent Wings|auto-trad|
|[ancestry-01-SCgqIo8VQZZKZ1Ws.htm](feats/ancestry-01-SCgqIo8VQZZKZ1Ws.htm)|Orc Sight|auto-trad|
|[ancestry-01-shp63QZvw9xvkVvC.htm](feats/ancestry-01-shp63QZvw9xvkVvC.htm)|Know Oneself|auto-trad|
|[ancestry-01-sJ7WTLDwAbIA9Elc.htm](feats/ancestry-01-sJ7WTLDwAbIA9Elc.htm)|Burn It!|auto-trad|
|[ancestry-01-SJbokJKkFnA6rKkJ.htm](feats/ancestry-01-SJbokJKkFnA6rKkJ.htm)|Nestling Fall|auto-trad|
|[ancestry-01-sKDCoxMz2yKWLGRJ.htm](feats/ancestry-01-sKDCoxMz2yKWLGRJ.htm)|Celestial Lore|auto-trad|
|[ancestry-01-sL2GmYve5NXJ0wc1.htm](feats/ancestry-01-sL2GmYve5NXJ0wc1.htm)|Gloomseer|auto-trad|
|[ancestry-01-sm6Y3fTcltxE8N0p.htm](feats/ancestry-01-sm6Y3fTcltxE8N0p.htm)|Goblin Lore|auto-trad|
|[ancestry-01-sMZBH6ROL44EDpXB.htm](feats/ancestry-01-sMZBH6ROL44EDpXB.htm)|Vishkanya Weapon Familiarity|auto-trad|
|[ancestry-01-SPyvwsiSghySIEw2.htm](feats/ancestry-01-SPyvwsiSghySIEw2.htm)|Dragon's Presence|auto-trad|
|[ancestry-01-SrSYEHqOLXWuj65e.htm](feats/ancestry-01-SrSYEHqOLXWuj65e.htm)|Inventive Offensive|auto-trad|
|[ancestry-01-StsFnks3lQU9YYpB.htm](feats/ancestry-01-StsFnks3lQU9YYpB.htm)|Clone-Risen|auto-trad|
|[ancestry-01-T4ulOYkFh8gq2kY9.htm](feats/ancestry-01-T4ulOYkFh8gq2kY9.htm)|Tide-hardened|auto-trad|
|[ancestry-01-TcUpt0KaDnoYheX8.htm](feats/ancestry-01-TcUpt0KaDnoYheX8.htm)|Tough Skin|auto-trad|
|[ancestry-01-tFgsBRsEo9ZEA5fU.htm](feats/ancestry-01-tFgsBRsEo9ZEA5fU.htm)|Illusion Sense|auto-trad|
|[ancestry-01-TGlb3gmSKkJBZt5q.htm](feats/ancestry-01-TGlb3gmSKkJBZt5q.htm)|Striking Retribution|auto-trad|
|[ancestry-01-TLuFqQwvnlJNeEsv.htm](feats/ancestry-01-TLuFqQwvnlJNeEsv.htm)|Crystal Luminescence|auto-trad|
|[ancestry-01-TmaxLWraJrvSQOkY.htm](feats/ancestry-01-TmaxLWraJrvSQOkY.htm)|Mariner's Fire|auto-trad|
|[ancestry-01-tn7K5lbnF87rZ659.htm](feats/ancestry-01-tn7K5lbnF87rZ659.htm)|Sociable|auto-trad|
|[ancestry-01-tP8pws78OOmobWjB.htm](feats/ancestry-01-tP8pws78OOmobWjB.htm)|Willing Death|auto-trad|
|[ancestry-01-tQ0FWNYJEXbmPCt7.htm](feats/ancestry-01-tQ0FWNYJEXbmPCt7.htm)|Emberkin|auto-trad|
|[ancestry-01-ttgv3PPCJLeI5pUL.htm](feats/ancestry-01-ttgv3PPCJLeI5pUL.htm)|Social Camouflage|auto-trad|
|[ancestry-01-TtlbpGchHOoWc4HN.htm](feats/ancestry-01-TtlbpGchHOoWc4HN.htm)|Collapse|auto-trad|
|[ancestry-01-tVPxq2qYCcOqvIYZ.htm](feats/ancestry-01-tVPxq2qYCcOqvIYZ.htm)|Ganzi Gaze|auto-trad|
|[ancestry-01-TW5TY7kSf50uaX71.htm](feats/ancestry-01-TW5TY7kSf50uaX71.htm)|Slither|auto-trad|
|[ancestry-01-txgCXcNMDe9kO7N8.htm](feats/ancestry-01-txgCXcNMDe9kO7N8.htm)|Vibrant Display|auto-trad|
|[ancestry-01-TYoE0GDF0URwQqZI.htm](feats/ancestry-01-TYoE0GDF0URwQqZI.htm)|Mistsoul|auto-trad|
|[ancestry-01-U09VfgU7alL0acWv.htm](feats/ancestry-01-U09VfgU7alL0acWv.htm)|Water Nagaji|auto-trad|
|[ancestry-01-U12Sh43QuY85Prdm.htm](feats/ancestry-01-U12Sh43QuY85Prdm.htm)|Parthenogenic Hatchling|auto-trad|
|[ancestry-01-u7FPgOjbSOmCTSD9.htm](feats/ancestry-01-u7FPgOjbSOmCTSD9.htm)|Flexible Form|auto-trad|
|[ancestry-01-u8gmBNHgb880vN3S.htm](feats/ancestry-01-u8gmBNHgb880vN3S.htm)|Witch Warden|auto-trad|
|[ancestry-01-Ugw1zZStQhg6iz8h.htm](feats/ancestry-01-Ugw1zZStQhg6iz8h.htm)|Adroit Manipulation|auto-trad|
|[ancestry-01-uiNRrdnJe0GOzy6Q.htm](feats/ancestry-01-uiNRrdnJe0GOzy6Q.htm)|Chance Death|auto-trad|
|[ancestry-01-UJ8AqzkkDqRCMNFW.htm](feats/ancestry-01-UJ8AqzkkDqRCMNFW.htm)|Dwarven Doughtiness|auto-trad|
|[ancestry-01-upMcjxPDgNOLuu7N.htm](feats/ancestry-01-upMcjxPDgNOLuu7N.htm)|Internal Compartment|auto-trad|
|[ancestry-01-UsJtnrqWOs7puRZa.htm](feats/ancestry-01-UsJtnrqWOs7puRZa.htm)|Dragon Spit|auto-trad|
|[ancestry-01-uuD8Z9jUG61GmenX.htm](feats/ancestry-01-uuD8Z9jUG61GmenX.htm)|Unfettered Halfling|auto-trad|
|[ancestry-01-uW0VSyy75YrsvtWz.htm](feats/ancestry-01-uW0VSyy75YrsvtWz.htm)|Unwavering Mien|auto-trad|
|[ancestry-01-UwH0sGIthv8kiPUt.htm](feats/ancestry-01-UwH0sGIthv8kiPUt.htm)|Goblin Scuttle|auto-trad|
|[ancestry-01-VatkAzfBYjA6z6OP.htm](feats/ancestry-01-VatkAzfBYjA6z6OP.htm)|Razor Claws|auto-trad|
|[ancestry-01-vcfeHDoaWEZtEcfz.htm](feats/ancestry-01-vcfeHDoaWEZtEcfz.htm)|Distracting Shadows|auto-trad|
|[ancestry-01-VcnOEAM3UR7oS0D5.htm](feats/ancestry-01-VcnOEAM3UR7oS0D5.htm)|Razzle-Dazzle|auto-trad|
|[ancestry-01-VDiMapgJoFI3CCol.htm](feats/ancestry-01-VDiMapgJoFI3CCol.htm)|Ratspeak|auto-trad|
|[ancestry-01-viFTJfZusRPx0G2q.htm](feats/ancestry-01-viFTJfZusRPx0G2q.htm)|Scamper Underfoot|auto-trad|
|[ancestry-01-vNDCXpumBONpk5JO.htm](feats/ancestry-01-vNDCXpumBONpk5JO.htm)|Goloma Lore|auto-trad|
|[ancestry-01-VSINzKESKAw2zA20.htm](feats/ancestry-01-VSINzKESKAw2zA20.htm)|Ceremony of Protection|auto-trad|
|[ancestry-01-VU07hybqzUXIJ6l2.htm](feats/ancestry-01-VU07hybqzUXIJ6l2.htm)|Halfling Lore|auto-trad|
|[ancestry-01-VV7vbDzcO8vdD3OO.htm](feats/ancestry-01-VV7vbDzcO8vdD3OO.htm)|Skull Creeper|auto-trad|
|[ancestry-01-vWf6uykXkQp1Li0r.htm](feats/ancestry-01-vWf6uykXkQp1Li0r.htm)|Fumesoul|auto-trad|
|[ancestry-01-Vxs9btOk61C7KpP2.htm](feats/ancestry-01-Vxs9btOk61C7KpP2.htm)|Cynical|auto-trad|
|[ancestry-01-VZDJgGm5LGb1Pdck.htm](feats/ancestry-01-VZDJgGm5LGb1Pdck.htm)|Elemental Embellish|auto-trad|
|[ancestry-01-vzKvihYNModB7sJ7.htm](feats/ancestry-01-vzKvihYNModB7sJ7.htm)|Play Dead|auto-trad|
|[ancestry-01-w1UrOqEYVuJDgRZZ.htm](feats/ancestry-01-w1UrOqEYVuJDgRZZ.htm)|Cold Minded|auto-trad|
|[ancestry-01-W2LmEXJH75tyeCSn.htm](feats/ancestry-01-W2LmEXJH75tyeCSn.htm)|Rat Familiar|auto-trad|
|[ancestry-01-W5hgaJvaCrTKfYbC.htm](feats/ancestry-01-W5hgaJvaCrTKfYbC.htm)|Fire Savvy|auto-trad|
|[ancestry-01-wAeI18wyWVwfUrIP.htm](feats/ancestry-01-wAeI18wyWVwfUrIP.htm)|Tusks (Half-Orc)|auto-trad|
|[ancestry-01-wEtN2NkqW4z4GUON.htm](feats/ancestry-01-wEtN2NkqW4z4GUON.htm)|Nosoi's Mask|auto-trad|
|[ancestry-01-weYZzyMmlCIC2TZt.htm](feats/ancestry-01-weYZzyMmlCIC2TZt.htm)|Grasping Reach|auto-trad|
|[ancestry-01-WoKiYMCXV27szBdy.htm](feats/ancestry-01-WoKiYMCXV27szBdy.htm)|Cringe|auto-trad|
|[ancestry-01-WoLh16gyDp8y9WOZ.htm](feats/ancestry-01-WoLh16gyDp8y9WOZ.htm)|Ancestral Longevity|auto-trad|
|[ancestry-01-wTT8ieLvfWsZZaWT.htm](feats/ancestry-01-wTT8ieLvfWsZZaWT.htm)|Tinkering Fingers|auto-trad|
|[ancestry-01-wtTye8OrC9cuK7YP.htm](feats/ancestry-01-wtTye8OrC9cuK7YP.htm)|Beast Trainer|auto-trad|
|[ancestry-01-WUMnFrehmFbj2PIm.htm](feats/ancestry-01-WUMnFrehmFbj2PIm.htm)|Azarketi Weapon Familiarity|auto-trad|
|[ancestry-01-WYEjiUZQJP3uMk4Z.htm](feats/ancestry-01-WYEjiUZQJP3uMk4Z.htm)|Irrepressible (Ganzi)|auto-trad|
|[ancestry-01-wylnETwIz32Au46y.htm](feats/ancestry-01-wylnETwIz32Au46y.htm)|Ageless Spirit|auto-trad|
|[ancestry-01-WzJaArukCUf9hpeP.htm](feats/ancestry-01-WzJaArukCUf9hpeP.htm)|Hag's Sight|auto-trad|
|[ancestry-01-X7wFUWjYjYhzpejU.htm](feats/ancestry-01-X7wFUWjYjYhzpejU.htm)|Clan's Edge|auto-trad|
|[ancestry-01-X9tKWtQrAcmn26Nv.htm](feats/ancestry-01-X9tKWtQrAcmn26Nv.htm)|Adapted Cantrip|auto-trad|
|[ancestry-01-xjwI1DlJvb7Rg6TG.htm](feats/ancestry-01-xjwI1DlJvb7Rg6TG.htm)|Azarketi Lore|auto-trad|
|[ancestry-01-XKDFgBEtFdEzCz8X.htm](feats/ancestry-01-XKDFgBEtFdEzCz8X.htm)|Intuitive Crafting|auto-trad|
|[ancestry-01-xOCmeskMkd8nCmba.htm](feats/ancestry-01-xOCmeskMkd8nCmba.htm)|Moon May|auto-trad|
|[ancestry-01-xruboxaTF6nw8l7M.htm](feats/ancestry-01-xruboxaTF6nw8l7M.htm)|Miresoul|auto-trad|
|[ancestry-01-xTe8lNBp76jsrhYh.htm](feats/ancestry-01-xTe8lNBp76jsrhYh.htm)|Slag May|auto-trad|
|[ancestry-01-XXAqMjml33jnQiDO.htm](feats/ancestry-01-XXAqMjml33jnQiDO.htm)|Old Soul|auto-trad|
|[ancestry-01-xzpMQ2ZRn9zC23XG.htm](feats/ancestry-01-xzpMQ2ZRn9zC23XG.htm)|Scamper|auto-trad|
|[ancestry-01-Y1bEisU8jVCsIYk3.htm](feats/ancestry-01-Y1bEisU8jVCsIYk3.htm)|Woodcraft|auto-trad|
|[ancestry-01-y7Or0CbcQBDdS9yG.htm](feats/ancestry-01-y7Or0CbcQBDdS9yG.htm)|Elven Aloofness|auto-trad|
|[ancestry-01-Y8c8hAyjNAiqgxAO.htm](feats/ancestry-01-Y8c8hAyjNAiqgxAO.htm)|Pelagic Aptitude|auto-trad|
|[ancestry-01-Y8sKn8NH1wC7Mrui.htm](feats/ancestry-01-Y8sKn8NH1wC7Mrui.htm)|Fangs|auto-trad|
|[ancestry-01-yaacOmfmBuGDcNOs.htm](feats/ancestry-01-yaacOmfmBuGDcNOs.htm)|Skittertalk|auto-trad|
|[ancestry-01-yaoekizcgPIlqVcC.htm](feats/ancestry-01-yaoekizcgPIlqVcC.htm)|Warped Reflection|auto-trad|
|[ancestry-01-yCaWcKlpbAfebqlO.htm](feats/ancestry-01-yCaWcKlpbAfebqlO.htm)|Shoony Lore|auto-trad|
|[ancestry-01-ydgCsYsgqSkFWEDK.htm](feats/ancestry-01-ydgCsYsgqSkFWEDK.htm)|Angelkin|auto-trad|
|[ancestry-01-yEbXxbD317IZgtsN.htm](feats/ancestry-01-yEbXxbD317IZgtsN.htm)|Dustsoul|auto-trad|
|[ancestry-01-YG6OCTbbqZwqRTr3.htm](feats/ancestry-01-YG6OCTbbqZwqRTr3.htm)|Arcane Eye|auto-trad|
|[ancestry-01-yJ8Ez5dEscIk1xr5.htm](feats/ancestry-01-yJ8Ez5dEscIk1xr5.htm)|First World Magic|auto-trad|
|[ancestry-01-yMfZulJcoSomQ6dO.htm](feats/ancestry-01-yMfZulJcoSomQ6dO.htm)|Elemental Wrath|auto-trad|
|[ancestry-01-YMKtEoNwHKA713Cx.htm](feats/ancestry-01-YMKtEoNwHKA713Cx.htm)|Junk Tinker|auto-trad|
|[ancestry-01-yQZ6naE8AP6JYTSH.htm](feats/ancestry-01-yQZ6naE8AP6JYTSH.htm)|Ember's Eyes|auto-trad|
|[ancestry-01-z92LsdpE98QdwILa.htm](feats/ancestry-01-z92LsdpE98QdwILa.htm)|Adaptive Vision|auto-trad|
|[ancestry-01-zaGD9Og2p8Opa0oJ.htm](feats/ancestry-01-zaGD9Og2p8Opa0oJ.htm)|Scavenger's Search|auto-trad|
|[ancestry-01-zAZJpgeEf5TWvdq4.htm](feats/ancestry-01-zAZJpgeEf5TWvdq4.htm)|Watchful Halfling|auto-trad|
|[ancestry-01-zb7F0M3H8PN3XsdX.htm](feats/ancestry-01-zb7F0M3H8PN3XsdX.htm)|Gnoll Weapon Familiarity|auto-trad|
|[ancestry-01-ZbRVqf14RTJJIZXG.htm](feats/ancestry-01-ZbRVqf14RTJJIZXG.htm)|Halfling Luck|auto-trad|
|[ancestry-01-ZDO7foRCMd9niGsK.htm](feats/ancestry-01-ZDO7foRCMd9niGsK.htm)|Quadruped|auto-trad|
|[ancestry-01-zi84Xt4dTsLeJ3uD.htm](feats/ancestry-01-zi84Xt4dTsLeJ3uD.htm)|Living Weapon|auto-trad|
|[ancestry-01-zki5qdM5IQcAiscM.htm](feats/ancestry-01-zki5qdM5IQcAiscM.htm)|Elven Lore|auto-trad|
|[ancestry-01-znX4u20IFE7TPi9Y.htm](feats/ancestry-01-znX4u20IFE7TPi9Y.htm)|Morph-Risen|auto-trad|
|[ancestry-01-Zr1sspa9Q16V8uZV.htm](feats/ancestry-01-Zr1sspa9Q16V8uZV.htm)|Dig Up Secrets|auto-trad|
|[ancestry-01-zs2FFGI88zB7EaBT.htm](feats/ancestry-01-zs2FFGI88zB7EaBT.htm)|Grim Insight|auto-trad|
|[ancestry-01-ZUjjUj9lVGOKrJbp.htm](feats/ancestry-01-ZUjjUj9lVGOKrJbp.htm)|Idyllkin|auto-trad|
|[ancestry-01-zvtYsz9jk8tPvlpS.htm](feats/ancestry-01-zvtYsz9jk8tPvlpS.htm)|Cat Nap|auto-trad|
|[ancestry-01-ZxiAMposVPDNPwxI.htm](feats/ancestry-01-ZxiAMposVPDNPwxI.htm)|Forge-Day's Rest|auto-trad|
|[ancestry-01-Zz7isE8Td2xDWqR8.htm](feats/ancestry-01-Zz7isE8Td2xDWqR8.htm)|Kitsune Spell Familiarity|auto-trad|
|[ancestry-01-ZZzgqFTOtUgnzSLZ.htm](feats/ancestry-01-ZZzgqFTOtUgnzSLZ.htm)|Monstrous Peacemaker|auto-trad|
|[ancestry-05-0FqbyC5tR2DC0DOk.htm](feats/ancestry-05-0FqbyC5tR2DC0DOk.htm)|Pack Stalker|auto-trad|
|[ancestry-05-0ihK3qYItmi8eVZs.htm](feats/ancestry-05-0ihK3qYItmi8eVZs.htm)|Eclectic Obsession|auto-trad|
|[ancestry-05-0Iv3VbR1DMPbZIjD.htm](feats/ancestry-05-0Iv3VbR1DMPbZIjD.htm)|Drag Down|auto-trad|
|[ancestry-05-0u1SZ1c6gDo6l0hS.htm](feats/ancestry-05-0u1SZ1c6gDo6l0hS.htm)|Climate Adaptation|auto-trad|
|[ancestry-05-14u6604yUXvoiIf3.htm](feats/ancestry-05-14u6604yUXvoiIf3.htm)|Malicious Bane|auto-trad|
|[ancestry-05-1gAehVvstGY885kJ.htm](feats/ancestry-05-1gAehVvstGY885kJ.htm)|Bristle|auto-trad|
|[ancestry-05-1LFbIRhb3Fgk4203.htm](feats/ancestry-05-1LFbIRhb3Fgk4203.htm)|Sealed Poppet|auto-trad|
|[ancestry-05-1miLVRtvsnZU6TTk.htm](feats/ancestry-05-1miLVRtvsnZU6TTk.htm)|Anchoring Roots|auto-trad|
|[ancestry-05-1muxLx8Vacn2SLHc.htm](feats/ancestry-05-1muxLx8Vacn2SLHc.htm)|Kneecap|auto-trad|
|[ancestry-05-2IO2W09IvwGHvatH.htm](feats/ancestry-05-2IO2W09IvwGHvatH.htm)|Graceful Guidance|auto-trad|
|[ancestry-05-2kwLzw618QaIHOap.htm](feats/ancestry-05-2kwLzw618QaIHOap.htm)|Genie Weapon Flourish|auto-trad|
|[ancestry-05-2RmsQrLySNYQ4uIn.htm](feats/ancestry-05-2RmsQrLySNYQ4uIn.htm)|Spirit Soother|auto-trad|
|[ancestry-05-2vlQ09QIBli5u9Gz.htm](feats/ancestry-05-2vlQ09QIBli5u9Gz.htm)|Called|auto-trad|
|[ancestry-05-3fLfHrJbMy2ayLMQ.htm](feats/ancestry-05-3fLfHrJbMy2ayLMQ.htm)|Blast Resistance|auto-trad|
|[ancestry-05-3HuiLoQuJKLAh5rV.htm](feats/ancestry-05-3HuiLoQuJKLAh5rV.htm)|Springing Leaper|auto-trad|
|[ancestry-05-3NZSRyoulnzsi3sn.htm](feats/ancestry-05-3NZSRyoulnzsi3sn.htm)|Wildborn Adept|auto-trad|
|[ancestry-05-47ZB8mYBtBt1C7zh.htm](feats/ancestry-05-47ZB8mYBtBt1C7zh.htm)|Histrionic Injury|auto-trad|
|[ancestry-05-48crF8lpg78fRdhJ.htm](feats/ancestry-05-48crF8lpg78fRdhJ.htm)|Feed On Pain|auto-trad|
|[ancestry-05-4M36jGbeSDfZFM38.htm](feats/ancestry-05-4M36jGbeSDfZFM38.htm)|Elven Instincts|auto-trad|
|[ancestry-05-4RUTJC42ZaENYh9T.htm](feats/ancestry-05-4RUTJC42ZaENYh9T.htm)|Embodied Dreadnought Subjectivity|auto-trad|
|[ancestry-05-4tZbjb1ote8Nij8I.htm](feats/ancestry-05-4tZbjb1ote8Nij8I.htm)|Darting Monkey|auto-trad|
|[ancestry-05-5AnJTAZnuWrkL8fa.htm](feats/ancestry-05-5AnJTAZnuWrkL8fa.htm)|Defiance Unto Death|auto-trad|
|[ancestry-05-5iULRBRDOryuWX6t.htm](feats/ancestry-05-5iULRBRDOryuWX6t.htm)|Iruxi Unarmed Cunning|auto-trad|
|[ancestry-05-69h9D3syUYLgIPr7.htm](feats/ancestry-05-69h9D3syUYLgIPr7.htm)|Expanded Luck|auto-trad|
|[ancestry-05-6kC0OuuWHyaqR3UQ.htm](feats/ancestry-05-6kC0OuuWHyaqR3UQ.htm)|Venom Spit|auto-trad|
|[ancestry-05-73JyUrJnH3nOQJM5.htm](feats/ancestry-05-73JyUrJnH3nOQJM5.htm)|Ceremony of Knowledge|auto-trad|
|[ancestry-05-7AuOURsTkkjrRekp.htm](feats/ancestry-05-7AuOURsTkkjrRekp.htm)|Loud Singer|auto-trad|
|[ancestry-05-7hwTeZNq6Jmzmtz4.htm](feats/ancestry-05-7hwTeZNq6Jmzmtz4.htm)|Agonizing Rebuke|auto-trad|
|[ancestry-05-7tNyXrMXclURtvUY.htm](feats/ancestry-05-7tNyXrMXclURtvUY.htm)|Skillful Tail (Ganzi)|auto-trad|
|[ancestry-05-7w8O3g3KM1HCDBSL.htm](feats/ancestry-05-7w8O3g3KM1HCDBSL.htm)|Ambush Awareness|auto-trad|
|[ancestry-05-7ZwXUlnqjj9zEYbO.htm](feats/ancestry-05-7ZwXUlnqjj9zEYbO.htm)|Vandal|auto-trad|
|[ancestry-05-80TAzC8XeFKRl7t5.htm](feats/ancestry-05-80TAzC8XeFKRl7t5.htm)|Warp Likeness|auto-trad|
|[ancestry-05-8CSewrCVEmL8sjnk.htm](feats/ancestry-05-8CSewrCVEmL8sjnk.htm)|Celestial Resistance|auto-trad|
|[ancestry-05-8DIzXO1YpsU3DpJw.htm](feats/ancestry-05-8DIzXO1YpsU3DpJw.htm)|Snare Genius|auto-trad|
|[ancestry-05-8MMYFIudSDkdo8cx.htm](feats/ancestry-05-8MMYFIudSDkdo8cx.htm)|Rat Magic|auto-trad|
|[ancestry-05-8Mo0fKRAz1TVbrr1.htm](feats/ancestry-05-8Mo0fKRAz1TVbrr1.htm)|Catfolk Weapon Rake|auto-trad|
|[ancestry-05-8NnYg8RUY4DQ8Wkf.htm](feats/ancestry-05-8NnYg8RUY4DQ8Wkf.htm)|Ghoran Weapon Practice|auto-trad|
|[ancestry-05-8RZ4VKXJgtl1aN27.htm](feats/ancestry-05-8RZ4VKXJgtl1aN27.htm)|Ornate Tattoo|auto-trad|
|[ancestry-05-969mPoiYANzV2261.htm](feats/ancestry-05-969mPoiYANzV2261.htm)|Ratfolk Roll|auto-trad|
|[ancestry-05-9CIy4cJJTcRM30Vz.htm](feats/ancestry-05-9CIy4cJJTcRM30Vz.htm)|Tough Tumbler|auto-trad|
|[ancestry-05-9WsYVbe47aSADs1Q.htm](feats/ancestry-05-9WsYVbe47aSADs1Q.htm)|Halfling Weapon Trickster|auto-trad|
|[ancestry-05-a0DM4VV48EFrga30.htm](feats/ancestry-05-a0DM4VV48EFrga30.htm)|Reveal Hidden Self|auto-trad|
|[ancestry-05-a3eKpbAQnxGrlnGq.htm](feats/ancestry-05-a3eKpbAQnxGrlnGq.htm)|Restoring Blood|auto-trad|
|[ancestry-05-a5H6dJSQgWfviUHU.htm](feats/ancestry-05-a5H6dJSQgWfviUHU.htm)|Nothing But Fluff|auto-trad|
|[ancestry-05-ab7xlmUzPUOFnAl2.htm](feats/ancestry-05-ab7xlmUzPUOFnAl2.htm)|Water Conjuration|auto-trad|
|[ancestry-05-AcgxKcA5I3dNTWLr.htm](feats/ancestry-05-AcgxKcA5I3dNTWLr.htm)|Wavetouched Paragon|auto-trad|
|[ancestry-05-ACXWB7a38ETc32Qj.htm](feats/ancestry-05-ACXWB7a38ETc32Qj.htm)|Catchy Tune|auto-trad|
|[ancestry-05-aLgb1x3azE4IKz0o.htm](feats/ancestry-05-aLgb1x3azE4IKz0o.htm)|Gnoll Weapon Practicality|auto-trad|
|[ancestry-05-ALR9knJdktuMWzr4.htm](feats/ancestry-05-ALR9knJdktuMWzr4.htm)|Azarketi Weapon Aptitude|auto-trad|
|[ancestry-05-aoz9dCq2NynyUYEf.htm](feats/ancestry-05-aoz9dCq2NynyUYEf.htm)|Formation Training|auto-trad|
|[ancestry-05-aQNsD2t0Tb4vToA4.htm](feats/ancestry-05-aQNsD2t0Tb4vToA4.htm)|Hold Mark|auto-trad|
|[ancestry-05-aVRuchEAJITim82y.htm](feats/ancestry-05-aVRuchEAJITim82y.htm)|Skillful Tail (Geniekin)|auto-trad|
|[ancestry-05-aVRuchEAJIvnd70k.htm](feats/ancestry-05-aVRuchEAJIvnd70k.htm)|Skillful Tail (Tiefling)|auto-trad|
|[ancestry-05-aZIdjtIYlLtJJP3g.htm](feats/ancestry-05-aZIdjtIYlLtJJP3g.htm)|Amorphous Aspect|auto-trad|
|[ancestry-05-B4e8V5nExlScjojY.htm](feats/ancestry-05-B4e8V5nExlScjojY.htm)|Shoki's Argument|auto-trad|
|[ancestry-05-B6eoSmowSFNlhj9h.htm](feats/ancestry-05-B6eoSmowSFNlhj9h.htm)|Hobgoblin Weapon Discipline|auto-trad|
|[ancestry-05-BaSl8PmfQwESIiY6.htm](feats/ancestry-05-BaSl8PmfQwESIiY6.htm)|Hypnotic Lure|auto-trad|
|[ancestry-05-BiPQAcvaEO0P2snr.htm](feats/ancestry-05-BiPQAcvaEO0P2snr.htm)|Healer's Halo|auto-trad|
|[ancestry-05-bJzANqEGTkho1bv6.htm](feats/ancestry-05-bJzANqEGTkho1bv6.htm)|Arcane Safeguards|auto-trad|
|[ancestry-05-bKc8MMFEOpOJJihb.htm](feats/ancestry-05-bKc8MMFEOpOJJihb.htm)|Heat Wave|auto-trad|
|[ancestry-05-BL2nLeClO30QoQGs.htm](feats/ancestry-05-BL2nLeClO30QoQGs.htm)|Spark Fist|auto-trad|
|[ancestry-05-BuaOQzmRNgIhGsfN.htm](feats/ancestry-05-BuaOQzmRNgIhGsfN.htm)|Blessed Blood (Aasimar)|auto-trad|
|[ancestry-05-bZGCOKLtCzMnrVwk.htm](feats/ancestry-05-bZGCOKLtCzMnrVwk.htm)|Replicate|auto-trad|
|[ancestry-05-CdSWMqPOVWIEzyUA.htm](feats/ancestry-05-CdSWMqPOVWIEzyUA.htm)|Fluid Contortionist|auto-trad|
|[ancestry-05-CnKc1kBejFV5gWtN.htm](feats/ancestry-05-CnKc1kBejFV5gWtN.htm)|Animalistic Resistance|auto-trad|
|[ancestry-05-Crcz9cW0To2pkfSy.htm](feats/ancestry-05-Crcz9cW0To2pkfSy.htm)|Murderous Thorns|auto-trad|
|[ancestry-05-CXgTn2sE7Do11rlv.htm](feats/ancestry-05-CXgTn2sE7Do11rlv.htm)|Mist Child|auto-trad|
|[ancestry-05-DBWfPOYZaupwo3rz.htm](feats/ancestry-05-DBWfPOYZaupwo3rz.htm)|Adaptive Adept|auto-trad|
|[ancestry-05-DidzozerKLZ2UYLx.htm](feats/ancestry-05-DidzozerKLZ2UYLx.htm)|Fortify Shield|auto-trad|
|[ancestry-05-dIIqejy4JAVuF0I8.htm](feats/ancestry-05-dIIqejy4JAVuF0I8.htm)|Fey Influence|auto-trad|
|[ancestry-05-DmYbGBC2ukp8tYD4.htm](feats/ancestry-05-DmYbGBC2ukp8tYD4.htm)|Firesight|auto-trad|
|[ancestry-05-DP5VVZHERQlYuYTa.htm](feats/ancestry-05-DP5VVZHERQlYuYTa.htm)|Protective Subroutine|auto-trad|
|[ancestry-05-DSyLFBi2LcUxeORh.htm](feats/ancestry-05-DSyLFBi2LcUxeORh.htm)|Devil In Plain Sight|auto-trad|
|[ancestry-05-DtazfQJaRRe46Sej.htm](feats/ancestry-05-DtazfQJaRRe46Sej.htm)|Tranquil Sanctuary|auto-trad|
|[ancestry-05-e3oaaoMZPzyP26QV.htm](feats/ancestry-05-e3oaaoMZPzyP26QV.htm)|Noble Resolve|auto-trad|
|[ancestry-05-EdVMwFRNV1LX1VWh.htm](feats/ancestry-05-EdVMwFRNV1LX1VWh.htm)|Light Paws|auto-trad|
|[ancestry-05-ehPnY1PuPK7EXkYc.htm](feats/ancestry-05-ehPnY1PuPK7EXkYc.htm)|Demonblood Frenzy|auto-trad|
|[ancestry-05-EIbppwlEu11ltC7n.htm](feats/ancestry-05-EIbppwlEu11ltC7n.htm)|Reflective Pocket|auto-trad|
|[ancestry-05-eJ6CytjWx2sGnKnC.htm](feats/ancestry-05-eJ6CytjWx2sGnKnC.htm)|Fiendish Resistance|auto-trad|
|[ancestry-05-espST21gwaZQFxpw.htm](feats/ancestry-05-espST21gwaZQFxpw.htm)|Undead Companion|auto-trad|
|[ancestry-05-eVn6hBjNjTB4liKw.htm](feats/ancestry-05-eVn6hBjNjTB4liKw.htm)|Chosen of Lamashtu|auto-trad|
|[ancestry-05-evwCimenReYvcruj.htm](feats/ancestry-05-evwCimenReYvcruj.htm)|Animal Elocutionist|auto-trad|
|[ancestry-05-Ewk7h9aQpKvy1RJo.htm](feats/ancestry-05-Ewk7h9aQpKvy1RJo.htm)|Crystalline Dust|auto-trad|
|[ancestry-05-exJmNR2XH1i6PGw3.htm](feats/ancestry-05-exJmNR2XH1i6PGw3.htm)|Skilled Climber|auto-trad|
|[ancestry-05-F4W5a2vkhUP7Lr4j.htm](feats/ancestry-05-F4W5a2vkhUP7Lr4j.htm)|Powerful Guts|auto-trad|
|[ancestry-05-f5Vkk2rM6tCe2zQn.htm](feats/ancestry-05-f5Vkk2rM6tCe2zQn.htm)|Shared Luck (Halfling)|auto-trad|
|[ancestry-05-f9YlMYWMjd0zoyy0.htm](feats/ancestry-05-f9YlMYWMjd0zoyy0.htm)|Protective Claws|auto-trad|
|[ancestry-05-FAUqvzfZDpRG44q0.htm](feats/ancestry-05-FAUqvzfZDpRG44q0.htm)|Renewing Quills|auto-trad|
|[ancestry-05-FayzcoNaiIdyPS2j.htm](feats/ancestry-05-FayzcoNaiIdyPS2j.htm)|Greater Animal Senses|auto-trad|
|[ancestry-05-FBtm9rZzk0tCQu9H.htm](feats/ancestry-05-FBtm9rZzk0tCQu9H.htm)|Spark of Independence|auto-trad|
|[ancestry-05-fD3RSV9nIkJsW6lD.htm](feats/ancestry-05-fD3RSV9nIkJsW6lD.htm)|One-Toed Hop|auto-trad|
|[ancestry-05-FGrIFDobGRFBPOuM.htm](feats/ancestry-05-FGrIFDobGRFBPOuM.htm)|Dwarven Reinforcement|auto-trad|
|[ancestry-05-Fj3ufCawOM6fZB24.htm](feats/ancestry-05-Fj3ufCawOM6fZB24.htm)|Expert Drill Sergeant|auto-trad|
|[ancestry-05-FLuv8uI1KoodNgY4.htm](feats/ancestry-05-FLuv8uI1KoodNgY4.htm)|Goblin Weapon Frenzy|auto-trad|
|[ancestry-05-g0hHetVM9UmmIDKU.htm](feats/ancestry-05-g0hHetVM9UmmIDKU.htm)|Fighting Horn|auto-trad|
|[ancestry-05-G1BPXzTzrUE4IndV.htm](feats/ancestry-05-G1BPXzTzrUE4IndV.htm)|Defy The Darkness|auto-trad|
|[ancestry-05-GAh4CwCMS3Gsl8WH.htm](feats/ancestry-05-GAh4CwCMS3Gsl8WH.htm)|Tengu Feather Fan|auto-trad|
|[ancestry-05-GgerQCCsGibaGWq0.htm](feats/ancestry-05-GgerQCCsGibaGWq0.htm)|Necromantic Physiology|auto-trad|
|[ancestry-05-gS9FYlD0Vt8yyZkP.htm](feats/ancestry-05-gS9FYlD0Vt8yyZkP.htm)|Grovel|auto-trad|
|[ancestry-05-GTA6QM7cH40L8H5Q.htm](feats/ancestry-05-GTA6QM7cH40L8H5Q.htm)|Fey Disguise|auto-trad|
|[ancestry-05-GZrvQo5FcoP5qocX.htm](feats/ancestry-05-GZrvQo5FcoP5qocX.htm)|Gaping Flesh|auto-trad|
|[ancestry-05-HCxmS8QGHL5O7LUf.htm](feats/ancestry-05-HCxmS8QGHL5O7LUf.htm)|Offensive Analysis|auto-trad|
|[ancestry-05-HEvk4ja8nJ3RVEqi.htm](feats/ancestry-05-HEvk4ja8nJ3RVEqi.htm)|Skin Split|auto-trad|
|[ancestry-05-HKGyFj2w5dzkf3SW.htm](feats/ancestry-05-HKGyFj2w5dzkf3SW.htm)|Natural Illusionist|auto-trad|
|[ancestry-05-hrITlxkBqHvaiiRS.htm](feats/ancestry-05-hrITlxkBqHvaiiRS.htm)|Right-Hand Blood|auto-trad|
|[ancestry-05-I3EMC4pqkEWrodpq.htm](feats/ancestry-05-I3EMC4pqkEWrodpq.htm)|Practiced Paddler|auto-trad|
|[ancestry-05-iCsHR5tdSXDHGjCv.htm](feats/ancestry-05-iCsHR5tdSXDHGjCv.htm)|Plague Sniffer|auto-trad|
|[ancestry-05-iCzJRywykSFwLto0.htm](feats/ancestry-05-iCzJRywykSFwLto0.htm)|Vanth's Weapon Execution|auto-trad|
|[ancestry-05-iliy0ONIb8Hw6muA.htm](feats/ancestry-05-iliy0ONIb8Hw6muA.htm)|Swift Swimmer|auto-trad|
|[ancestry-05-iNrR4WOB1UO9iiNE.htm](feats/ancestry-05-iNrR4WOB1UO9iiNE.htm)|Lifesense|auto-trad|
|[ancestry-05-iS7MvHewCeQRT78d.htm](feats/ancestry-05-iS7MvHewCeQRT78d.htm)|Skillful Climber|auto-trad|
|[ancestry-05-IsNs6hXQm9pJcXB1.htm](feats/ancestry-05-IsNs6hXQm9pJcXB1.htm)|Clever Shadow|auto-trad|
|[ancestry-05-IUVzB39pRVyBFOEx.htm](feats/ancestry-05-IUVzB39pRVyBFOEx.htm)|Elemental Bulwark|auto-trad|
|[ancestry-05-IY86Kvopp4ACuQsw.htm](feats/ancestry-05-IY86Kvopp4ACuQsw.htm)|Envenom Fangs|auto-trad|
|[ancestry-05-j0mlvJcuYGFuMG2S.htm](feats/ancestry-05-j0mlvJcuYGFuMG2S.htm)|Ally's Shelter|auto-trad|
|[ancestry-05-j1620BT5t0RLVB0C.htm](feats/ancestry-05-j1620BT5t0RLVB0C.htm)|Grippli Weapon Innovator|auto-trad|
|[ancestry-05-jNrpvEqfncdGZPak.htm](feats/ancestry-05-jNrpvEqfncdGZPak.htm)|Halfling Ingenuity|auto-trad|
|[ancestry-05-juLtvliSzYPCmMa3.htm](feats/ancestry-05-juLtvliSzYPCmMa3.htm)|Winglets|auto-trad|
|[ancestry-05-k8nWKHLYvAKMuwLd.htm](feats/ancestry-05-k8nWKHLYvAKMuwLd.htm)|Ageless Patience|auto-trad|
|[ancestry-05-KcbSxOPYC5CUqbZQ.htm](feats/ancestry-05-KcbSxOPYC5CUqbZQ.htm)|Clever Improviser|auto-trad|
|[ancestry-05-KeE8Ky38dCX8XaTg.htm](feats/ancestry-05-KeE8Ky38dCX8XaTg.htm)|Tree's Ward|auto-trad|
|[ancestry-05-kGed3uQbZ7x5SBB8.htm](feats/ancestry-05-kGed3uQbZ7x5SBB8.htm)|Gecko's Grip|auto-trad|
|[ancestry-05-ko399VJY2VsKE7iM.htm](feats/ancestry-05-ko399VJY2VsKE7iM.htm)|Unlock Secret|auto-trad|
|[ancestry-05-kpn4R65YlD38iAIS.htm](feats/ancestry-05-kpn4R65YlD38iAIS.htm)|Ritual Reversion|auto-trad|
|[ancestry-05-krgoR0Ykqw59MDbg.htm](feats/ancestry-05-krgoR0Ykqw59MDbg.htm)|Uncanny Awareness|auto-trad|
|[ancestry-05-KU488rIt9bOdTNMA.htm](feats/ancestry-05-KU488rIt9bOdTNMA.htm)|Well of Potential|auto-trad|
|[ancestry-05-KYKK1vLqGIxXH5Tu.htm](feats/ancestry-05-KYKK1vLqGIxXH5Tu.htm)|Speak with Kindred|auto-trad|
|[ancestry-05-l2JaSC3NA9S6Qq46.htm](feats/ancestry-05-l2JaSC3NA9S6Qq46.htm)|Myriad Forms|auto-trad|
|[ancestry-05-l3PosipTLXANeoT8.htm](feats/ancestry-05-l3PosipTLXANeoT8.htm)|Step Lively|auto-trad|
|[ancestry-05-LAArv2uv6TOkTzQO.htm](feats/ancestry-05-LAArv2uv6TOkTzQO.htm)|Quick Stow (Ratfolk)|auto-trad|
|[ancestry-05-LC2EnXQ4MkhDLViM.htm](feats/ancestry-05-LC2EnXQ4MkhDLViM.htm)|Finned Ridges|auto-trad|
|[ancestry-05-lF5B49zbDG61sxXa.htm](feats/ancestry-05-lF5B49zbDG61sxXa.htm)|Lucky Break|auto-trad|
|[ancestry-05-llWKddRiyUHouaZx.htm](feats/ancestry-05-llWKddRiyUHouaZx.htm)|Empathic Calm|auto-trad|
|[ancestry-05-LNyTKhIZlM17026W.htm](feats/ancestry-05-LNyTKhIZlM17026W.htm)|Gnaw|auto-trad|
|[ancestry-05-M0B0rt6rk5MkHiBN.htm](feats/ancestry-05-M0B0rt6rk5MkHiBN.htm)|Hybrid Shape|auto-trad|
|[ancestry-05-m5JYglEObpWC3dhP.htm](feats/ancestry-05-m5JYglEObpWC3dhP.htm)|Sense Allies|auto-trad|
|[ancestry-05-m64PtRsaiOkylVVk.htm](feats/ancestry-05-m64PtRsaiOkylVVk.htm)|Cunning Tinker|auto-trad|
|[ancestry-05-mfy0nasMIiMLUm3f.htm](feats/ancestry-05-mfy0nasMIiMLUm3f.htm)|Slip With The Breeze|auto-trad|
|[ancestry-05-MwjnRpVn3br88Caj.htm](feats/ancestry-05-MwjnRpVn3br88Caj.htm)|Intuitive Illusions|auto-trad|
|[ancestry-05-n3CbbtK4fgBznIMf.htm](feats/ancestry-05-n3CbbtK4fgBznIMf.htm)|Empyreal Blessing|auto-trad|
|[ancestry-05-NBDwiz1NDioc2eMP.htm](feats/ancestry-05-NBDwiz1NDioc2eMP.htm)|Energized Font|auto-trad|
|[ancestry-05-NBwH9wEeUfKfOg8R.htm](feats/ancestry-05-NBwH9wEeUfKfOg8R.htm)|Magpie Snatch|auto-trad|
|[ancestry-05-NFsQOa3ynthYLVj6.htm](feats/ancestry-05-NFsQOa3ynthYLVj6.htm)|Ward Against Corruption|auto-trad|
|[ancestry-05-NHlCta7B1Lmt3S7w.htm](feats/ancestry-05-NHlCta7B1Lmt3S7w.htm)|Jungle Runner|auto-trad|
|[ancestry-05-NPATYCDg2cryH0Ya.htm](feats/ancestry-05-NPATYCDg2cryH0Ya.htm)|Conrasu Weapon Understanding|auto-trad|
|[ancestry-05-Nv9hNKVSioDw5DHC.htm](feats/ancestry-05-Nv9hNKVSioDw5DHC.htm)|Ragdya's Revelry|auto-trad|
|[ancestry-05-O89mwKPRg0up0J0I.htm](feats/ancestry-05-O89mwKPRg0up0J0I.htm)|Focused Cat Nap|auto-trad|
|[ancestry-05-OCANjuCQ1wcCRBsn.htm](feats/ancestry-05-OCANjuCQ1wcCRBsn.htm)|Vishkanya Weapon Arts|auto-trad|
|[ancestry-05-oEvcWFvBnsZ03OGW.htm](feats/ancestry-05-oEvcWFvBnsZ03OGW.htm)|Thrown Voice|auto-trad|
|[ancestry-05-OhyN9pBHdMpC126F.htm](feats/ancestry-05-OhyN9pBHdMpC126F.htm)|Hybrid Form|auto-trad|
|[ancestry-05-OjUfwxMcM91CHLHP.htm](feats/ancestry-05-OjUfwxMcM91CHLHP.htm)|Well-Armed|auto-trad|
|[ancestry-05-OK8Fm7EG8wjoYZBX.htm](feats/ancestry-05-OK8Fm7EG8wjoYZBX.htm)|Favorable Winds|auto-trad|
|[ancestry-05-OK8X6QFRuRxpBMcZ.htm](feats/ancestry-05-OK8X6QFRuRxpBMcZ.htm)|Long Tongue|auto-trad|
|[ancestry-05-oL6f86dy4yyx5N54.htm](feats/ancestry-05-oL6f86dy4yyx5N54.htm)|Tomb-Watcher's Glare|auto-trad|
|[ancestry-05-OOBSMpdAfYuiiQqo.htm](feats/ancestry-05-OOBSMpdAfYuiiQqo.htm)|Recognize Ambush|auto-trad|
|[ancestry-05-OqJMjuK61uST7AlM.htm](feats/ancestry-05-OqJMjuK61uST7AlM.htm)|Gnome Weapon Innovator|auto-trad|
|[ancestry-05-oqoftwLB6tvoLjnL.htm](feats/ancestry-05-oqoftwLB6tvoLjnL.htm)|Lightless Litheness|auto-trad|
|[ancestry-05-oSA1Ii6gTRGobeSO.htm](feats/ancestry-05-oSA1Ii6gTRGobeSO.htm)|Shadowy Disguise|auto-trad|
|[ancestry-05-otr60veuPNygNDFY.htm](feats/ancestry-05-otr60veuPNygNDFY.htm)|Cunning Hair|auto-trad|
|[ancestry-05-OXhrwQgnZdOi81Yi.htm](feats/ancestry-05-OXhrwQgnZdOi81Yi.htm)|Defy Death|auto-trad|
|[ancestry-05-P19AnciwNcSqxU7z.htm](feats/ancestry-05-P19AnciwNcSqxU7z.htm)|Distant Cackle|auto-trad|
|[ancestry-05-pc00hoz4ILmqUwSC.htm](feats/ancestry-05-pc00hoz4ILmqUwSC.htm)|Devilish Wiles|auto-trad|
|[ancestry-05-PdIN91xKsZ4z7p17.htm](feats/ancestry-05-PdIN91xKsZ4z7p17.htm)|Transposable Compliance|auto-trad|
|[ancestry-05-PncXj46fgSwTWRl6.htm](feats/ancestry-05-PncXj46fgSwTWRl6.htm)|Ankle Bite|auto-trad|
|[ancestry-05-pqezW5dqha1I32Ld.htm](feats/ancestry-05-pqezW5dqha1I32Ld.htm)|Inspire Imitation|auto-trad|
|[ancestry-05-PRlN77xL6Bm2gUIp.htm](feats/ancestry-05-PRlN77xL6Bm2gUIp.htm)|Grippli Glide|auto-trad|
|[ancestry-05-psgPbKsbqfz6Qt4P.htm](feats/ancestry-05-psgPbKsbqfz6Qt4P.htm)|Speak With Flowers|auto-trad|
|[ancestry-05-PWKFdaNghx1YMKlA.htm](feats/ancestry-05-PWKFdaNghx1YMKlA.htm)|Hunter's Fangs|auto-trad|
|[ancestry-05-QE8asCPqyrdenll0.htm](feats/ancestry-05-QE8asCPqyrdenll0.htm)|Lab Rat|auto-trad|
|[ancestry-05-qKNo7Sr1dtVqhhAa.htm](feats/ancestry-05-qKNo7Sr1dtVqhhAa.htm)|Inured to the Heat|auto-trad|
|[ancestry-05-qkXRucQCLLS3VoMa.htm](feats/ancestry-05-qkXRucQCLLS3VoMa.htm)|Friendform|auto-trad|
|[ancestry-05-qUZnrueC2a0zf95N.htm](feats/ancestry-05-qUZnrueC2a0zf95N.htm)|Runtsage|auto-trad|
|[ancestry-05-qXl2cOh3wL3QszCy.htm](feats/ancestry-05-qXl2cOh3wL3QszCy.htm)|Martial Experience|auto-trad|
|[ancestry-05-qZUdGd2khS9cq4hJ.htm](feats/ancestry-05-qZUdGd2khS9cq4hJ.htm)|Shifting Faces|auto-trad|
|[ancestry-05-rFmJVDdB313EibTs.htm](feats/ancestry-05-rFmJVDdB313EibTs.htm)|Eat Fortune|auto-trad|
|[ancestry-05-RkEvEqwc8pCBcusz.htm](feats/ancestry-05-RkEvEqwc8pCBcusz.htm)|Hopping Stride|auto-trad|
|[ancestry-05-rnEfO5eyRw7Fywzb.htm](feats/ancestry-05-rnEfO5eyRw7Fywzb.htm)|Tail Spin|auto-trad|
|[ancestry-05-rp3mjgFXBVZYCleU.htm](feats/ancestry-05-rp3mjgFXBVZYCleU.htm)|Inoculation Subroutine|auto-trad|
|[ancestry-05-RP7TlsqNuge0dltp.htm](feats/ancestry-05-RP7TlsqNuge0dltp.htm)|Kitsune Spell Mysteries|auto-trad|
|[ancestry-05-Rr7GdUjXhGhV04Pe.htm](feats/ancestry-05-Rr7GdUjXhGhV04Pe.htm)|Supernatural Charm|auto-trad|
|[ancestry-05-RrtqD8WmoUumauJD.htm](feats/ancestry-05-RrtqD8WmoUumauJD.htm)|Mask Of Power|auto-trad|
|[ancestry-05-rTjGshtUMHnlBUfH.htm](feats/ancestry-05-rTjGshtUMHnlBUfH.htm)|Marine Ally|auto-trad|
|[ancestry-05-s1swBWSqtfrXTJHK.htm](feats/ancestry-05-s1swBWSqtfrXTJHK.htm)|Guided by the Stars|auto-trad|
|[ancestry-05-SBhzem6n3buoxlG5.htm](feats/ancestry-05-SBhzem6n3buoxlG5.htm)|Extinguish Light|auto-trad|
|[ancestry-05-scA2b141swxUPw8M.htm](feats/ancestry-05-scA2b141swxUPw8M.htm)|Garuda's Squall|auto-trad|
|[ancestry-05-scNrNhnGTgPIzoj7.htm](feats/ancestry-05-scNrNhnGTgPIzoj7.htm)|Ancestral Suspicion|auto-trad|
|[ancestry-05-sGSf5BdopT0zWOWs.htm](feats/ancestry-05-sGSf5BdopT0zWOWs.htm)|Sheltering Slab|auto-trad|
|[ancestry-05-ShH7Wl7xfJL07DZC.htm](feats/ancestry-05-ShH7Wl7xfJL07DZC.htm)|Leshy Glide|auto-trad|
|[ancestry-05-sQLgEcqoQ0SYtbTg.htm](feats/ancestry-05-sQLgEcqoQ0SYtbTg.htm)|Tail Snatch|auto-trad|
|[ancestry-05-sTqdFqWVL9yxi5wt.htm](feats/ancestry-05-sTqdFqWVL9yxi5wt.htm)|Protective Sheath|auto-trad|
|[ancestry-05-SWNmYaj0OSPhhIqO.htm](feats/ancestry-05-SWNmYaj0OSPhhIqO.htm)|Mutate Weapon|auto-trad|
|[ancestry-05-SYcTallEKEaJeNGw.htm](feats/ancestry-05-SYcTallEKEaJeNGw.htm)|Swimming Poppet|auto-trad|
|[ancestry-05-SzZFmcfLoHMAS0gt.htm](feats/ancestry-05-SzZFmcfLoHMAS0gt.htm)|Cloud Gazer|auto-trad|
|[ancestry-05-t3IzY8uSyFj3aGmh.htm](feats/ancestry-05-t3IzY8uSyFj3aGmh.htm)|Shed Tail|auto-trad|
|[ancestry-05-tB6V6rWv8vAFsKsX.htm](feats/ancestry-05-tB6V6rWv8vAFsKsX.htm)|Nanite Shroud|auto-trad|
|[ancestry-05-ThqMksZRxNB18ivs.htm](feats/ancestry-05-ThqMksZRxNB18ivs.htm)|Advanced Targeting System|auto-trad|
|[ancestry-05-TlMSleKR8Bh0EuSW.htm](feats/ancestry-05-TlMSleKR8Bh0EuSW.htm)|Climbing Claws|auto-trad|
|[ancestry-05-TvVqZHp7qvkPakKf.htm](feats/ancestry-05-TvVqZHp7qvkPakKf.htm)|Mistaken Identity|auto-trad|
|[ancestry-05-twnZopGlB392hmqH.htm](feats/ancestry-05-twnZopGlB392hmqH.htm)|Project Persona|auto-trad|
|[ancestry-05-U5FcfRvveTKtgebq.htm](feats/ancestry-05-U5FcfRvveTKtgebq.htm)|Torch Goblin|auto-trad|
|[ancestry-05-ulrGnvF0KAgEaifX.htm](feats/ancestry-05-ulrGnvF0KAgEaifX.htm)|Cornered Fury|auto-trad|
|[ancestry-05-uQGo33E4haaFNg6u.htm](feats/ancestry-05-uQGo33E4haaFNg6u.htm)|Pride Hunter|auto-trad|
|[ancestry-05-UuVz1QY7QXD5cnLu.htm](feats/ancestry-05-UuVz1QY7QXD5cnLu.htm)|Fledgling Flight|auto-trad|
|[ancestry-05-v0ovKn4ZKmtXfXnu.htm](feats/ancestry-05-v0ovKn4ZKmtXfXnu.htm)|Vanara Weapon Trickery|auto-trad|
|[ancestry-05-V4W2hTr5lm5vS8Dq.htm](feats/ancestry-05-V4W2hTr5lm5vS8Dq.htm)|Nagaji Spell Mysteries|auto-trad|
|[ancestry-05-vdowlFFknihiz5pm.htm](feats/ancestry-05-vdowlFFknihiz5pm.htm)|Intercorporate|auto-trad|
|[ancestry-05-VFBQ1MskOascFDNf.htm](feats/ancestry-05-VFBQ1MskOascFDNf.htm)|Steam Spell|auto-trad|
|[ancestry-05-VK2vAl1SfI4Qrtkt.htm](feats/ancestry-05-VK2vAl1SfI4Qrtkt.htm)|Loyal Empath|auto-trad|
|[ancestry-05-VLCAFIXzUtiv1VuE.htm](feats/ancestry-05-VLCAFIXzUtiv1VuE.htm)|Flexible Tail|auto-trad|
|[ancestry-05-VNmmHrMAVk3Tiw1d.htm](feats/ancestry-05-VNmmHrMAVk3Tiw1d.htm)|Kobold Weapon Innovator|auto-trad|
|[ancestry-05-VQmKNh0QPHzYMmee.htm](feats/ancestry-05-VQmKNh0QPHzYMmee.htm)|Treacherous Earth|auto-trad|
|[ancestry-05-vW7tWi3jK7z2Clen.htm](feats/ancestry-05-vW7tWi3jK7z2Clen.htm)|Dwarven Weapon Cunning|auto-trad|
|[ancestry-05-vxE9hBKB6F2ctOX3.htm](feats/ancestry-05-vxE9hBKB6F2ctOX3.htm)|Mask Of Rejection|auto-trad|
|[ancestry-05-wigOSPSxXnapFxeh.htm](feats/ancestry-05-wigOSPSxXnapFxeh.htm)|Dogfang Bite|auto-trad|
|[ancestry-05-wjhhlh82MABhfxCO.htm](feats/ancestry-05-wjhhlh82MABhfxCO.htm)|Strix Vengeance|auto-trad|
|[ancestry-05-WPz97m5FNlbLIQ6p.htm](feats/ancestry-05-WPz97m5FNlbLIQ6p.htm)|Long-Nosed Form|auto-trad|
|[ancestry-05-Wwr8VilSybQgVtin.htm](feats/ancestry-05-Wwr8VilSybQgVtin.htm)|Athletic Might|auto-trad|
|[ancestry-05-WWSwcvIjGUQOKKuD.htm](feats/ancestry-05-WWSwcvIjGUQOKKuD.htm)|Taste Blood|auto-trad|
|[ancestry-05-X6tVQ5S7H7GuCZux.htm](feats/ancestry-05-X6tVQ5S7H7GuCZux.htm)|Tengu Weapon Study|auto-trad|
|[ancestry-05-XM4zR0q9rBxjR3lG.htm](feats/ancestry-05-XM4zR0q9rBxjR3lG.htm)|Darkseer|auto-trad|
|[ancestry-05-xoB4RDYkdAALt0U4.htm](feats/ancestry-05-xoB4RDYkdAALt0U4.htm)|Magical Resistance|auto-trad|
|[ancestry-05-XRLtXJXGswe20QKY.htm](feats/ancestry-05-XRLtXJXGswe20QKY.htm)|Towering Presence|auto-trad|
|[ancestry-05-xruqgywumtExg8me.htm](feats/ancestry-05-xruqgywumtExg8me.htm)|Debilitating Venom|auto-trad|
|[ancestry-05-XYtnVNKt6uPcRrdH.htm](feats/ancestry-05-XYtnVNKt6uPcRrdH.htm)|Tree Climber (Goblin)|auto-trad|
|[ancestry-05-y0Ru2hRmoRwkRsxZ.htm](feats/ancestry-05-y0Ru2hRmoRwkRsxZ.htm)|Tongue Disarm|auto-trad|
|[ancestry-05-Y3IUIsopvN13vEvZ.htm](feats/ancestry-05-Y3IUIsopvN13vEvZ.htm)|Orc Weapon Carnage|auto-trad|
|[ancestry-05-YeVDITJplindA27l.htm](feats/ancestry-05-YeVDITJplindA27l.htm)|Victorious Vigor|auto-trad|
|[ancestry-05-YndH82FX7KLawNBW.htm](feats/ancestry-05-YndH82FX7KLawNBW.htm)|Tenacious Net|auto-trad|
|[ancestry-05-yqtaAZR9jfen6gEW.htm](feats/ancestry-05-yqtaAZR9jfen6gEW.htm)|Iruxi Glide|auto-trad|
|[ancestry-05-z0Bwy7lxU3DIugpo.htm](feats/ancestry-05-z0Bwy7lxU3DIugpo.htm)|Animal Speaker|auto-trad|
|[ancestry-05-z2z3UTayfUhcGazv.htm](feats/ancestry-05-z2z3UTayfUhcGazv.htm)|Scar-Thick Skin|auto-trad|
|[ancestry-05-Z30nSkai5UmZCyKu.htm](feats/ancestry-05-Z30nSkai5UmZCyKu.htm)|Past Life|auto-trad|
|[ancestry-05-Z56DtGBd3AcZOCeG.htm](feats/ancestry-05-Z56DtGBd3AcZOCeG.htm)|Cultural Adaptability|auto-trad|
|[ancestry-05-zc32MIxa1E6357D6.htm](feats/ancestry-05-zc32MIxa1E6357D6.htm)|Elven Weapon Elegance|auto-trad|
|[ancestry-05-zgriBCYR4TmBoDqO.htm](feats/ancestry-05-zgriBCYR4TmBoDqO.htm)|Forest Stealth|auto-trad|
|[ancestry-05-ZOJsLNziWQ7Ri3x9.htm](feats/ancestry-05-ZOJsLNziWQ7Ri3x9.htm)|Undead Slayer|auto-trad|
|[ancestry-05-ZOOP2RzRJnpVqnCr.htm](feats/ancestry-05-ZOOP2RzRJnpVqnCr.htm)|Aquatic Conversationalist|auto-trad|
|[ancestry-05-ZRI1OXaaa4ZC6EK1.htm](feats/ancestry-05-ZRI1OXaaa4ZC6EK1.htm)|Easily Dismissed|auto-trad|
|[ancestry-05-zwqawaXccARDV0jL.htm](feats/ancestry-05-zwqawaXccARDV0jL.htm)|Feathered Cloak|auto-trad|
|[ancestry-05-ZwvSiuFFsyNGJiB3.htm](feats/ancestry-05-ZwvSiuFFsyNGJiB3.htm)|Ceremony of Sunlight|auto-trad|
|[ancestry-05-Zz5A8Yg0jGSK8GNu.htm](feats/ancestry-05-Zz5A8Yg0jGSK8GNu.htm)|Enthralling Allure|auto-trad|
|[ancestry-05-ZZxePfQkBPuTHkt1.htm](feats/ancestry-05-ZZxePfQkBPuTHkt1.htm)|Boulder Roll|auto-trad|
|[ancestry-09-0BUSnsCKOeFCJKEp.htm](feats/ancestry-09-0BUSnsCKOeFCJKEp.htm)|Moderate Enhance Venom|auto-trad|
|[ancestry-09-0R15gtdXoXbrD8As.htm](feats/ancestry-09-0R15gtdXoXbrD8As.htm)|Fortified Mind|auto-trad|
|[ancestry-09-0X4HZk036A5meZbo.htm](feats/ancestry-09-0X4HZk036A5meZbo.htm)|Serpentine Swimmer|auto-trad|
|[ancestry-09-1WheVs50iwMBi6KC.htm](feats/ancestry-09-1WheVs50iwMBi6KC.htm)|Archon Magic|auto-trad|
|[ancestry-09-2GrlSP1xhKIz4G8B.htm](feats/ancestry-09-2GrlSP1xhKIz4G8B.htm)|Glory And Valor!|auto-trad|
|[ancestry-09-2i82PgD1BEtAVeZt.htm](feats/ancestry-09-2i82PgD1BEtAVeZt.htm)|Call of Elysium|auto-trad|
|[ancestry-09-2kAyZ3LB28BDhXKa.htm](feats/ancestry-09-2kAyZ3LB28BDhXKa.htm)|Bark and Tendril|auto-trad|
|[ancestry-09-3N6T7AQETVKaAwiR.htm](feats/ancestry-09-3N6T7AQETVKaAwiR.htm)|Demon Magic|auto-trad|
|[ancestry-09-3TilQ1l97NV0okph.htm](feats/ancestry-09-3TilQ1l97NV0okph.htm)|Invoke The Elements (Veil May)|auto-trad|
|[ancestry-09-3Y1k2eAMqCdcCGmK.htm](feats/ancestry-09-3Y1k2eAMqCdcCGmK.htm)|Stone Bones|auto-trad|
|[ancestry-09-3yLdRzh16sT8RgFV.htm](feats/ancestry-09-3yLdRzh16sT8RgFV.htm)|Wheedle and Jig|auto-trad|
|[ancestry-09-4Ai8aNHYw7oEj7eE.htm](feats/ancestry-09-4Ai8aNHYw7oEj7eE.htm)|Gripping Limbs|auto-trad|
|[ancestry-09-4cH2RoWsgXYaWIPa.htm](feats/ancestry-09-4cH2RoWsgXYaWIPa.htm)|Eerie Compression|auto-trad|
|[ancestry-09-4EgueMqUm0ebKV4J.htm](feats/ancestry-09-4EgueMqUm0ebKV4J.htm)|Dragonblood Paragon|auto-trad|
|[ancestry-09-4hFeaF4MlqIHb6gb.htm](feats/ancestry-09-4hFeaF4MlqIHb6gb.htm)|Azata Magic|auto-trad|
|[ancestry-09-4Jwtl2FvxskruHQv.htm](feats/ancestry-09-4Jwtl2FvxskruHQv.htm)|Invoke The Elements|auto-trad|
|[ancestry-09-56HvICglqH7uR3AY.htm](feats/ancestry-09-56HvICglqH7uR3AY.htm)|Cantorian Rejuvenation|auto-trad|
|[ancestry-09-5j3nLvicUVmPqzA7.htm](feats/ancestry-09-5j3nLvicUVmPqzA7.htm)|Tongue Tether|auto-trad|
|[ancestry-09-5S1nPoxHTgu9MGGV.htm](feats/ancestry-09-5S1nPoxHTgu9MGGV.htm)|Ricocheting Leap|auto-trad|
|[ancestry-09-67UXSHUUH0K36xyB.htm](feats/ancestry-09-67UXSHUUH0K36xyB.htm)|Smoke Sight|auto-trad|
|[ancestry-09-6eXffyvqxpIzig2O.htm](feats/ancestry-09-6eXffyvqxpIzig2O.htm)|Offensive Subroutine|auto-trad|
|[ancestry-09-6px0s2nE8fHVWswz.htm](feats/ancestry-09-6px0s2nE8fHVWswz.htm)|Envenom Strike|auto-trad|
|[ancestry-09-6WwxTbX8KvU3Xxak.htm](feats/ancestry-09-6WwxTbX8KvU3Xxak.htm)|Janni Hospitality|auto-trad|
|[ancestry-09-7cM7uKRKQDWz5eeu.htm](feats/ancestry-09-7cM7uKRKQDWz5eeu.htm)|Heroes' Call|auto-trad|
|[ancestry-09-7eyy63BkMv3enk5r.htm](feats/ancestry-09-7eyy63BkMv3enk5r.htm)|Internal Respirator|auto-trad|
|[ancestry-09-7RFu7EwwvjHMP0dq.htm](feats/ancestry-09-7RFu7EwwvjHMP0dq.htm)|Rakshasa Magic|auto-trad|
|[ancestry-09-7vUOlVqjheZV0Nmc.htm](feats/ancestry-09-7vUOlVqjheZV0Nmc.htm)|Vivacious Conduit|auto-trad|
|[ancestry-09-8buK32r3i3aGyyOR.htm](feats/ancestry-09-8buK32r3i3aGyyOR.htm)|Shaitan Magic|auto-trad|
|[ancestry-09-8ukixWL8MBJOhPbW.htm](feats/ancestry-09-8ukixWL8MBJOhPbW.htm)|Pinch Time|auto-trad|
|[ancestry-09-8znlUoKgr8mmLPe1.htm](feats/ancestry-09-8znlUoKgr8mmLPe1.htm)|Wing Step|auto-trad|
|[ancestry-09-9LTBRvuQgXKZFiZc.htm](feats/ancestry-09-9LTBRvuQgXKZFiZc.htm)|Pack Tactics|auto-trad|
|[ancestry-09-a32r2n9j36khV0Cp.htm](feats/ancestry-09-a32r2n9j36khV0Cp.htm)|Spore Cloud|auto-trad|
|[ancestry-09-AGznaQpn2cE6jz9H.htm](feats/ancestry-09-AGznaQpn2cE6jz9H.htm)|Dangle|auto-trad|
|[ancestry-09-aIm2qi4JZerthZmF.htm](feats/ancestry-09-aIm2qi4JZerthZmF.htm)|Elf Step|auto-trad|
|[ancestry-09-AJVx3sHm6f6i8ZQW.htm](feats/ancestry-09-AJVx3sHm6f6i8ZQW.htm)|Efreeti Magic|auto-trad|
|[ancestry-09-AKMx0GYKLjy7jTXl.htm](feats/ancestry-09-AKMx0GYKLjy7jTXl.htm)|No Evidence|auto-trad|
|[ancestry-09-aOIZvx5fx5jVHHOO.htm](feats/ancestry-09-aOIZvx5fx5jVHHOO.htm)|Life Leap|auto-trad|
|[ancestry-09-AQvtqj2h2n5n8YYg.htm](feats/ancestry-09-AQvtqj2h2n5n8YYg.htm)|Lesser Augmentation|auto-trad|
|[ancestry-09-aSgLhr8mM53DWbFc.htm](feats/ancestry-09-aSgLhr8mM53DWbFc.htm)|Endless Memories|auto-trad|
|[ancestry-09-ATiQDz27aiBTAt17.htm](feats/ancestry-09-ATiQDz27aiBTAt17.htm)|Roll with It|auto-trad|
|[ancestry-09-AYXqqeLSqCYhgUY7.htm](feats/ancestry-09-AYXqqeLSqCYhgUY7.htm)|Core Attunement|auto-trad|
|[ancestry-09-aznyI5mfMdEFSDr8.htm](feats/ancestry-09-aznyI5mfMdEFSDr8.htm)|Hardy Traveler|auto-trad|
|[ancestry-09-B53sWzd5irAoLn2U.htm](feats/ancestry-09-B53sWzd5irAoLn2U.htm)|Bone Caller|auto-trad|
|[ancestry-09-B5HiNholWMwYdHTC.htm](feats/ancestry-09-B5HiNholWMwYdHTC.htm)|Energy Blessed|auto-trad|
|[ancestry-09-bJTcHDqHOI6xD4AT.htm](feats/ancestry-09-bJTcHDqHOI6xD4AT.htm)|Unyielding Disguise|auto-trad|
|[ancestry-09-bo4JG09pkoS7ywSZ.htm](feats/ancestry-09-bo4JG09pkoS7ywSZ.htm)|Cave Climber|auto-trad|
|[ancestry-09-bpNpqxtsBJIPYSX9.htm](feats/ancestry-09-bpNpqxtsBJIPYSX9.htm)|Ceremony of Strengthened Hand|auto-trad|
|[ancestry-09-C2PmqTxQgWGTHluf.htm](feats/ancestry-09-C2PmqTxQgWGTHluf.htm)|Studious Adept|auto-trad|
|[ancestry-09-C80vQCKQBGRaqcmq.htm](feats/ancestry-09-C80vQCKQBGRaqcmq.htm)|Multitalented|auto-trad|
|[ancestry-09-cCv9pgyXzZ0TQmZg.htm](feats/ancestry-09-cCv9pgyXzZ0TQmZg.htm)|Big Mouth|auto-trad|
|[ancestry-09-CCY6VsGjp5fdmM6K.htm](feats/ancestry-09-CCY6VsGjp5fdmM6K.htm)|Pride in Arms|auto-trad|
|[ancestry-09-chM3Pya6H8QGnEGo.htm](feats/ancestry-09-chM3Pya6H8QGnEGo.htm)|Water Dancer|auto-trad|
|[ancestry-09-cmhfYMEM4uzrNIiV.htm](feats/ancestry-09-cmhfYMEM4uzrNIiV.htm)|Energize Wings|auto-trad|
|[ancestry-09-COP89tjrNhEucuRW.htm](feats/ancestry-09-COP89tjrNhEucuRW.htm)|Mountain's Stoutness|auto-trad|
|[ancestry-09-cRMUYCnkkE9lSEhh.htm](feats/ancestry-09-cRMUYCnkkE9lSEhh.htm)|Irrepressible (Halfling)|auto-trad|
|[ancestry-09-d022Gp8PjS4Q0ZAC.htm](feats/ancestry-09-d022Gp8PjS4Q0ZAC.htm)|Shared Luck (Catfolk)|auto-trad|
|[ancestry-09-DhqsKns2SaGcOKO9.htm](feats/ancestry-09-DhqsKns2SaGcOKO9.htm)|Read The Stars|auto-trad|
|[ancestry-09-DLE2rr5I1TBAk0I3.htm](feats/ancestry-09-DLE2rr5I1TBAk0I3.htm)|Disorienting Venom|auto-trad|
|[ancestry-09-DnfVQGLk3PAl8UWh.htm](feats/ancestry-09-DnfVQGLk3PAl8UWh.htm)|Hungry Goblin|auto-trad|
|[ancestry-09-dtmLxbSy2H8h8e4N.htm](feats/ancestry-09-dtmLxbSy2H8h8e4N.htm)|Skittering Scuttle|auto-trad|
|[ancestry-09-DTU4jrz9YNr7e62e.htm](feats/ancestry-09-DTU4jrz9YNr7e62e.htm)|Larcenous Tail|auto-trad|
|[ancestry-09-DunjA0vLZeozcNxu.htm](feats/ancestry-09-DunjA0vLZeozcNxu.htm)|Devil Magic|auto-trad|
|[ancestry-09-Eoy0zhpf8tYrTHN4.htm](feats/ancestry-09-Eoy0zhpf8tYrTHN4.htm)|Shory Aeromancer|auto-trad|
|[ancestry-09-esKk5XrnlqRayDPG.htm](feats/ancestry-09-esKk5XrnlqRayDPG.htm)|Angelic Magic|auto-trad|
|[ancestry-09-Ezk3OgfPaRlEEyAD.htm](feats/ancestry-09-Ezk3OgfPaRlEEyAD.htm)|Virtue-Forged Tattoos|auto-trad|
|[ancestry-09-fcSzCdQ9y800BdOv.htm](feats/ancestry-09-fcSzCdQ9y800BdOv.htm)|Velstrac Magic|auto-trad|
|[ancestry-09-fEzxwMCNyvooYqdn.htm](feats/ancestry-09-fEzxwMCNyvooYqdn.htm)|Miraculous Repair|auto-trad|
|[ancestry-09-FGbrxFmNeOfPHFOG.htm](feats/ancestry-09-FGbrxFmNeOfPHFOG.htm)|Bone Missile|auto-trad|
|[ancestry-09-fiA3rfqPcCKFCI83.htm](feats/ancestry-09-fiA3rfqPcCKFCI83.htm)|Towering Growth|auto-trad|
|[ancestry-09-ftntYtiKGoPBpvcv.htm](feats/ancestry-09-ftntYtiKGoPBpvcv.htm)|Qlippoth Magic|auto-trad|
|[ancestry-09-fu1cTh93zgGweduf.htm](feats/ancestry-09-fu1cTh93zgGweduf.htm)|Wary Skulker|auto-trad|
|[ancestry-09-FUOUyhHufNH4ri7H.htm](feats/ancestry-09-FUOUyhHufNH4ri7H.htm)|Wings Of Air|auto-trad|
|[ancestry-09-FZZXgmVMAmvKHWIo.htm](feats/ancestry-09-FZZXgmVMAmvKHWIo.htm)|Breath Like Honey|auto-trad|
|[ancestry-09-g9AOSz4QDAlUee9M.htm](feats/ancestry-09-g9AOSz4QDAlUee9M.htm)|Fox Trick|auto-trad|
|[ancestry-09-gWyCNTWUhxneOBne.htm](feats/ancestry-09-gWyCNTWUhxneOBne.htm)|Helpful Halfling|auto-trad|
|[ancestry-09-h11M3QrIHKLj2ezy.htm](feats/ancestry-09-h11M3QrIHKLj2ezy.htm)|Well-Groomed|auto-trad|
|[ancestry-09-HfebybiUNW8mXOfP.htm](feats/ancestry-09-HfebybiUNW8mXOfP.htm)|Returning Throw|auto-trad|
|[ancestry-09-hgfRHSS5BsoyQ9Fj.htm](feats/ancestry-09-hgfRHSS5BsoyQ9Fj.htm)|Marid Magic|auto-trad|
|[ancestry-09-HPW7Qi02lkVprW6V.htm](feats/ancestry-09-HPW7Qi02lkVprW6V.htm)|Silent Step|auto-trad|
|[ancestry-09-Ht21JJ95wiHOgZoT.htm](feats/ancestry-09-Ht21JJ95wiHOgZoT.htm)|Telekinetic Slip|auto-trad|
|[ancestry-09-ieFjiZSlT9J4boqP.htm](feats/ancestry-09-ieFjiZSlT9J4boqP.htm)|Dangle (Vanara)|auto-trad|
|[ancestry-09-IHFqUHxNChCPZPml.htm](feats/ancestry-09-IHFqUHxNChCPZPml.htm)|Garuda Magic|auto-trad|
|[ancestry-09-IlzUi1viWgeRolU8.htm](feats/ancestry-09-IlzUi1viWgeRolU8.htm)|Legendary Size|auto-trad|
|[ancestry-09-InTchkd50pzQok3f.htm](feats/ancestry-09-InTchkd50pzQok3f.htm)|Web Hunter|auto-trad|
|[ancestry-09-IQ9C7glCXVgYecz1.htm](feats/ancestry-09-IQ9C7glCXVgYecz1.htm)|Light From Darkness|auto-trad|
|[ancestry-09-iRpVW1DPKVxlzIzt.htm](feats/ancestry-09-iRpVW1DPKVxlzIzt.htm)|Night Magic|auto-trad|
|[ancestry-09-iXhMlT5rlLngybxX.htm](feats/ancestry-09-iXhMlT5rlLngybxX.htm)|Slip Into Shadow|auto-trad|
|[ancestry-09-IzNyByUBNH94MDOr.htm](feats/ancestry-09-IzNyByUBNH94MDOr.htm)|Fey Ascension|auto-trad|
|[ancestry-09-JeMGDAD5s9AXCZ2G.htm](feats/ancestry-09-JeMGDAD5s9AXCZ2G.htm)|Animal Magic|auto-trad|
|[ancestry-09-jNemcS5GqH8mnjV6.htm](feats/ancestry-09-jNemcS5GqH8mnjV6.htm)|Fiendish Wings|auto-trad|
|[ancestry-09-jpRVp1INDAlYWvlI.htm](feats/ancestry-09-jpRVp1INDAlYWvlI.htm)|Rakshasa Ravaged|auto-trad|
|[ancestry-09-jzfV4lJ0721Hfzq1.htm](feats/ancestry-09-jzfV4lJ0721Hfzq1.htm)|Squad Tactics|auto-trad|
|[ancestry-09-k0sXAn4PPq5nW9al.htm](feats/ancestry-09-k0sXAn4PPq5nW9al.htm)|Guiding Luck|auto-trad|
|[ancestry-09-K9ixUl7PrNbBHGdA.htm](feats/ancestry-09-K9ixUl7PrNbBHGdA.htm)|Sodbuster|auto-trad|
|[ancestry-09-KgiYiDz1lWoBMRlF.htm](feats/ancestry-09-KgiYiDz1lWoBMRlF.htm)|Otherworldly Acumen|auto-trad|
|[ancestry-09-kIIcgcc5SWFkyiBj.htm](feats/ancestry-09-kIIcgcc5SWFkyiBj.htm)|Transcendent Realization|auto-trad|
|[ancestry-09-kjRMXN95lQmIi2hP.htm](feats/ancestry-09-kjRMXN95lQmIi2hP.htm)|Earthsense|auto-trad|
|[ancestry-09-kPyyZGD5L6b2Kl8C.htm](feats/ancestry-09-kPyyZGD5L6b2Kl8C.htm)|Constant Gaze|auto-trad|
|[ancestry-09-KRgzuwwjT30KKvV4.htm](feats/ancestry-09-KRgzuwwjT30KKvV4.htm)|Eclectic Sword Training|auto-trad|
|[ancestry-09-KTLdk65OOAVixqtY.htm](feats/ancestry-09-KTLdk65OOAVixqtY.htm)|Stonewalker|auto-trad|
|[ancestry-09-Kuv9P0NdtpE8yPJL.htm](feats/ancestry-09-Kuv9P0NdtpE8yPJL.htm)|Solar Rejuvenation (Ghoran)|auto-trad|
|[ancestry-09-l4ux6Mn2fklB2cXM.htm](feats/ancestry-09-l4ux6Mn2fklB2cXM.htm)|Evade Doom|auto-trad|
|[ancestry-09-lbiFj4At5BxotaNY.htm](feats/ancestry-09-lbiFj4At5BxotaNY.htm)|Dracomancer|auto-trad|
|[ancestry-09-lBVzIet6IpufLXZg.htm](feats/ancestry-09-lBVzIet6IpufLXZg.htm)|Swift Application|auto-trad|
|[ancestry-09-lHcDb9oXUdFupRdi.htm](feats/ancestry-09-lHcDb9oXUdFupRdi.htm)|Incredible Improvisation|auto-trad|
|[ancestry-09-LhpE0NsfNwYP6MOz.htm](feats/ancestry-09-LhpE0NsfNwYP6MOz.htm)|Freeze It!|auto-trad|
|[ancestry-09-LK6niBb38mEraYRS.htm](feats/ancestry-09-LK6niBb38mEraYRS.htm)|Overcrowd|auto-trad|
|[ancestry-09-llWnSLYALh88iRGQ.htm](feats/ancestry-09-llWnSLYALh88iRGQ.htm)|Rain of Bolts|auto-trad|
|[ancestry-09-LRBzEzpS19z3Eghd.htm](feats/ancestry-09-LRBzEzpS19z3Eghd.htm)|Sense Thoughts|auto-trad|
|[ancestry-09-lT2sOwC6Pi5P4Yq0.htm](feats/ancestry-09-lT2sOwC6Pi5P4Yq0.htm)|Ancillary Motes|auto-trad|
|[ancestry-09-m25EfSMmEjJPSyJj.htm](feats/ancestry-09-m25EfSMmEjJPSyJj.htm)|Perfume Cloud|auto-trad|
|[ancestry-09-m7aW56wExv2ieMFL.htm](feats/ancestry-09-m7aW56wExv2ieMFL.htm)|Invoke The Elements (Virga May)|auto-trad|
|[ancestry-09-mnH68QcFRtkMbNE0.htm](feats/ancestry-09-mnH68QcFRtkMbNE0.htm)|Battleforger|auto-trad|
|[ancestry-09-MTp2j4N4H4wj07pH.htm](feats/ancestry-09-MTp2j4N4H4wj07pH.htm)|Heir of the Saoc|auto-trad|
|[ancestry-09-mU8vTzrWX9fIlG0d.htm](feats/ancestry-09-mU8vTzrWX9fIlG0d.htm)|Grandmother's Wisdom|auto-trad|
|[ancestry-09-mXNwdRSM9kZrT2Um.htm](feats/ancestry-09-mXNwdRSM9kZrT2Um.htm)|Fell Rider|auto-trad|
|[ancestry-09-N8Ci3w5gQ68rj6a6.htm](feats/ancestry-09-N8Ci3w5gQ68rj6a6.htm)|Arcane Camouflage|auto-trad|
|[ancestry-09-Nb8iLgQeuHU73hQM.htm](feats/ancestry-09-Nb8iLgQeuHU73hQM.htm)|Mirror Refuge|auto-trad|
|[ancestry-09-NEl8G3SlwxOR9Zx1.htm](feats/ancestry-09-NEl8G3SlwxOR9Zx1.htm)|Preemptive Reconfiguration|auto-trad|
|[ancestry-09-ngNzsvIpnj1iLfSC.htm](feats/ancestry-09-ngNzsvIpnj1iLfSC.htm)|Anarchic Arcana|auto-trad|
|[ancestry-09-nHoRM1gLL7MtIiCS.htm](feats/ancestry-09-nHoRM1gLL7MtIiCS.htm)|Djinni Magic|auto-trad|
|[ancestry-09-NjPZbQjJJIygS1ru.htm](feats/ancestry-09-NjPZbQjJJIygS1ru.htm)|Shadow Sight|auto-trad|
|[ancestry-09-nKkbEKbE9vfKWKdd.htm](feats/ancestry-09-nKkbEKbE9vfKWKdd.htm)|Echoes In Stone|auto-trad|
|[ancestry-09-NnpUhj7d4RmfOKTE.htm](feats/ancestry-09-NnpUhj7d4RmfOKTE.htm)|Scorching Disarm|auto-trad|
|[ancestry-09-nqDhQdkgsnvebUMr.htm](feats/ancestry-09-nqDhQdkgsnvebUMr.htm)|Snare Commando|auto-trad|
|[ancestry-09-nYSSdczKdRj7pdW6.htm](feats/ancestry-09-nYSSdczKdRj7pdW6.htm)|Ceremony of Aeon's Guidance|auto-trad|
|[ancestry-09-o1LhDmwymrpEy1u2.htm](feats/ancestry-09-o1LhDmwymrpEy1u2.htm)|Riptide|auto-trad|
|[ancestry-09-O2iFrAt7hyELJlIR.htm](feats/ancestry-09-O2iFrAt7hyELJlIR.htm)|Slip The Grasp|auto-trad|
|[ancestry-09-O80QCYMqz4VZsfxT.htm](feats/ancestry-09-O80QCYMqz4VZsfxT.htm)|Hefting Shadow|auto-trad|
|[ancestry-09-O9z1MJpEaf6Y1Acd.htm](feats/ancestry-09-O9z1MJpEaf6Y1Acd.htm)|Dance Underfoot|auto-trad|
|[ancestry-09-oGPX29AOsyHj18mK.htm](feats/ancestry-09-oGPX29AOsyHj18mK.htm)|Flower Magic|auto-trad|
|[ancestry-09-ovMIxhiStlPE7tty.htm](feats/ancestry-09-ovMIxhiStlPE7tty.htm)|Divine Countermeasures|auto-trad|
|[ancestry-09-p5dLNQ5HtM9Fq8SN.htm](feats/ancestry-09-p5dLNQ5HtM9Fq8SN.htm)|Invoke The Elements (Brine May)|auto-trad|
|[ancestry-09-pe6cHJzzMm5Tr25G.htm](feats/ancestry-09-pe6cHJzzMm5Tr25G.htm)|Juvenile Flight|auto-trad|
|[ancestry-09-pr7e8xzrl8OLp6U9.htm](feats/ancestry-09-pr7e8xzrl8OLp6U9.htm)|Coating Of Slime|auto-trad|
|[ancestry-09-pt4oMtjAVGFQHtVw.htm](feats/ancestry-09-pt4oMtjAVGFQHtVw.htm)|Catrina's Presence|auto-trad|
|[ancestry-09-PxWdf4HAhZ8fUB3R.htm](feats/ancestry-09-PxWdf4HAhZ8fUB3R.htm)|Duskwalker Magic|auto-trad|
|[ancestry-09-Q1lGguNI4SqPwgVn.htm](feats/ancestry-09-Q1lGguNI4SqPwgVn.htm)|Dragon Prince|auto-trad|
|[ancestry-09-Q8fdMjZ2Wv3FawOI.htm](feats/ancestry-09-Q8fdMjZ2Wv3FawOI.htm)|Water Strider|auto-trad|
|[ancestry-09-Q8XZYvatnYo4VlAQ.htm](feats/ancestry-09-Q8XZYvatnYo4VlAQ.htm)|Agathion Magic|auto-trad|
|[ancestry-09-qAdlJvAHkWBisDJ0.htm](feats/ancestry-09-qAdlJvAHkWBisDJ0.htm)|Ceremony of Aeon's Shield|auto-trad|
|[ancestry-09-Qe9IUDVo7gsowMHq.htm](feats/ancestry-09-Qe9IUDVo7gsowMHq.htm)|Fey Magic|auto-trad|
|[ancestry-09-QNgnwkKZmJR5jT7K.htm](feats/ancestry-09-QNgnwkKZmJR5jT7K.htm)|Laughing Gnoll|auto-trad|
|[ancestry-09-QpvmQTvYaiT6iDJR.htm](feats/ancestry-09-QpvmQTvYaiT6iDJR.htm)|Guarded Thoughts|auto-trad|
|[ancestry-09-qr1E37Tla555tvIO.htm](feats/ancestry-09-qr1E37Tla555tvIO.htm)|Fade Away|auto-trad|
|[ancestry-09-qS2VcFLez4PLOpIS.htm](feats/ancestry-09-qS2VcFLez4PLOpIS.htm)|Tree Climber (Elf)|auto-trad|
|[ancestry-09-QUaSGUmRnlMbzw1P.htm](feats/ancestry-09-QUaSGUmRnlMbzw1P.htm)|Wind God's Fan|auto-trad|
|[ancestry-09-RdnzuBhkEv7TKsNi.htm](feats/ancestry-09-RdnzuBhkEv7TKsNi.htm)|Asura Magic|auto-trad|
|[ancestry-09-RFXzsfEgz7WbDCQO.htm](feats/ancestry-09-RFXzsfEgz7WbDCQO.htm)|Unhampered Passage|auto-trad|
|[ancestry-09-RN5aZCzEnmlYmBf5.htm](feats/ancestry-09-RN5aZCzEnmlYmBf5.htm)|Crystalline Cloud|auto-trad|
|[ancestry-09-RTxPL1reRcJhhYeG.htm](feats/ancestry-09-RTxPL1reRcJhhYeG.htm)|Analyze Information|auto-trad|
|[ancestry-09-rU9Aw05FFLVq0MTV.htm](feats/ancestry-09-rU9Aw05FFLVq0MTV.htm)|Close Quarters|auto-trad|
|[ancestry-09-rw4qq4qmCbO0hRfH.htm](feats/ancestry-09-rw4qq4qmCbO0hRfH.htm)|Peri Magic|auto-trad|
|[ancestry-09-RxA1PdgGbijkieJD.htm](feats/ancestry-09-RxA1PdgGbijkieJD.htm)|Defensive Instincts|auto-trad|
|[ancestry-09-rzBC5bHAWWpjHMEw.htm](feats/ancestry-09-rzBC5bHAWWpjHMEw.htm)|Ceremony of Fortification|auto-trad|
|[ancestry-09-S7z1LbnSRlBep8rO.htm](feats/ancestry-09-S7z1LbnSRlBep8rO.htm)|Spirit Strikes|auto-trad|
|[ancestry-09-Sav50NxWdLnbaDWQ.htm](feats/ancestry-09-Sav50NxWdLnbaDWQ.htm)|Ferocious Gust|auto-trad|
|[ancestry-09-sbAb2a6BzZpcYv8y.htm](feats/ancestry-09-sbAb2a6BzZpcYv8y.htm)|Mother's Mindfulness|auto-trad|
|[ancestry-09-sGdREpnSJDzEacub.htm](feats/ancestry-09-sGdREpnSJDzEacub.htm)|Group Aid|auto-trad|
|[ancestry-09-SiedJ6hnDLEGeeBj.htm](feats/ancestry-09-SiedJ6hnDLEGeeBj.htm)|Cling|auto-trad|
|[ancestry-09-SjwISllgvlKEcjSv.htm](feats/ancestry-09-SjwISllgvlKEcjSv.htm)|Two Truths|auto-trad|
|[ancestry-09-sTaeqBJCH1HQy96W.htm](feats/ancestry-09-sTaeqBJCH1HQy96W.htm)|Invoke The Elements (Snow May)|auto-trad|
|[ancestry-09-SuEmijj909yxmYOO.htm](feats/ancestry-09-SuEmijj909yxmYOO.htm)|Rat Form|auto-trad|
|[ancestry-09-swsMURQBMXZpjWl8.htm](feats/ancestry-09-swsMURQBMXZpjWl8.htm)|Cunning Climber|auto-trad|
|[ancestry-09-SxRmlDYhYEkq10Ak.htm](feats/ancestry-09-SxRmlDYhYEkq10Ak.htm)|Serpentcoil Slam|auto-trad|
|[ancestry-09-T4OSMMEvbymMzlIJ.htm](feats/ancestry-09-T4OSMMEvbymMzlIJ.htm)|Rejuvenation Token|auto-trad|
|[ancestry-09-T8cBEhuHWkh3MqgO.htm](feats/ancestry-09-T8cBEhuHWkh3MqgO.htm)|Sense For Trouble|auto-trad|
|[ancestry-09-tEzwhF3uFIi825xj.htm](feats/ancestry-09-tEzwhF3uFIi825xj.htm)|Scaling Poppet|auto-trad|
|[ancestry-09-Thhcli0PR7HBBcPX.htm](feats/ancestry-09-Thhcli0PR7HBBcPX.htm)|Aboleth Transmutation|auto-trad|
|[ancestry-09-tkY5jXELipWjC8k2.htm](feats/ancestry-09-tkY5jXELipWjC8k2.htm)|Viper Strike|auto-trad|
|[ancestry-09-Tl8yckXeTCHnwrlM.htm](feats/ancestry-09-Tl8yckXeTCHnwrlM.htm)|Soaring Flight|auto-trad|
|[ancestry-09-TPX0fm9wxndpIqpk.htm](feats/ancestry-09-TPX0fm9wxndpIqpk.htm)|Morrigna's Spider Affinity|auto-trad|
|[ancestry-09-TRC4DgVq07cZO65B.htm](feats/ancestry-09-TRC4DgVq07cZO65B.htm)|Thorned Seedpod|auto-trad|
|[ancestry-09-TwAps3ewk7KFHKDv.htm](feats/ancestry-09-TwAps3ewk7KFHKDv.htm)|Sculpt Shadows|auto-trad|
|[ancestry-09-Twhkz2FfzaZezVnG.htm](feats/ancestry-09-Twhkz2FfzaZezVnG.htm)|Fortuitous Shift|auto-trad|
|[ancestry-09-tyae2vpOiAMxXvQH.htm](feats/ancestry-09-tyae2vpOiAMxXvQH.htm)|Inner Breath|auto-trad|
|[ancestry-09-u94fcPT5Oukqzql5.htm](feats/ancestry-09-u94fcPT5Oukqzql5.htm)|Azarketi Purification|auto-trad|
|[ancestry-09-UdwXT24zrLzg2ZIV.htm](feats/ancestry-09-UdwXT24zrLzg2ZIV.htm)|Strong Swimmer|auto-trad|
|[ancestry-09-UKYO5kiOnCY1hgCD.htm](feats/ancestry-09-UKYO5kiOnCY1hgCD.htm)|Daemon Magic|auto-trad|
|[ancestry-09-ulQzdBOnZH9LQu8M.htm](feats/ancestry-09-ulQzdBOnZH9LQu8M.htm)|Scalding Spit|auto-trad|
|[ancestry-09-uOFzs058hy144rzm.htm](feats/ancestry-09-uOFzs058hy144rzm.htm)|Captivating Curiosity|auto-trad|
|[ancestry-09-UojXcKf98oAFJUE0.htm](feats/ancestry-09-UojXcKf98oAFJUE0.htm)|Skeletal Resistance|auto-trad|
|[ancestry-09-VazbV1s93eZqUZIu.htm](feats/ancestry-09-VazbV1s93eZqUZIu.htm)|Improvisational Warrior|auto-trad|
|[ancestry-09-vB7xdAJiZ1gWW2Yj.htm](feats/ancestry-09-vB7xdAJiZ1gWW2Yj.htm)|Jalmeri Rakshasa Magic|auto-trad|
|[ancestry-09-velPTcpjLXPnaYrm.htm](feats/ancestry-09-velPTcpjLXPnaYrm.htm)|Solar Rejuvenation|auto-trad|
|[ancestry-09-vfuHVSuExvtyajkW.htm](feats/ancestry-09-vfuHVSuExvtyajkW.htm)|Expert Longevity|auto-trad|
|[ancestry-09-VphmK5JRR35SyEhV.htm](feats/ancestry-09-VphmK5JRR35SyEhV.htm)|Pervasive Superstition|auto-trad|
|[ancestry-09-vtCrMziYxNyj8kP7.htm](feats/ancestry-09-vtCrMziYxNyj8kP7.htm)|Celestial Wings|auto-trad|
|[ancestry-09-VtFuhfIIw8OoLF6v.htm](feats/ancestry-09-VtFuhfIIw8OoLF6v.htm)|Alluring Performance|auto-trad|
|[ancestry-09-vyb3BYDiIl2MiZp4.htm](feats/ancestry-09-vyb3BYDiIl2MiZp4.htm)|Absorb Toxin|auto-trad|
|[ancestry-09-wGzKq2cx1b2Ycl4u.htm](feats/ancestry-09-wGzKq2cx1b2Ycl4u.htm)|Kneel for No God|auto-trad|
|[ancestry-09-WldISqAE5Rw3Ewzn.htm](feats/ancestry-09-WldISqAE5Rw3Ewzn.htm)|Demolitionist|auto-trad|
|[ancestry-09-WQa6PxkOgyvRpaaM.htm](feats/ancestry-09-WQa6PxkOgyvRpaaM.htm)|Predator's Growl|auto-trad|
|[ancestry-09-WSxHCapuTn8uRdLI.htm](feats/ancestry-09-WSxHCapuTn8uRdLI.htm)|Dragon Grip|auto-trad|
|[ancestry-09-WUVNZoIZvr9XFv2x.htm](feats/ancestry-09-WUVNZoIZvr9XFv2x.htm)|First World Adept|auto-trad|
|[ancestry-09-xDCtfNtbnaG166cy.htm](feats/ancestry-09-xDCtfNtbnaG166cy.htm)|Janni Magic|auto-trad|
|[ancestry-09-xfbPSP9tl1N95xDF.htm](feats/ancestry-09-xfbPSP9tl1N95xDF.htm)|Arcane Slam|auto-trad|
|[ancestry-09-xM8IWff5yfbQGk4s.htm](feats/ancestry-09-xM8IWff5yfbQGk4s.htm)|Briar Battler|auto-trad|
|[ancestry-09-YgytD4HGjWNFwiev.htm](feats/ancestry-09-YgytD4HGjWNFwiev.htm)|Arcane Propulsion|auto-trad|
|[ancestry-09-YSEqqNx3McbS7k4n.htm](feats/ancestry-09-YSEqqNx3McbS7k4n.htm)|Embodied Dragoon Subjectivity|auto-trad|
|[ancestry-09-yYIyAbwpdkySiCMU.htm](feats/ancestry-09-yYIyAbwpdkySiCMU.htm)|Uncanny Cheeks|auto-trad|
|[ancestry-09-Z2EiYMtGoNnwW6Tk.htm](feats/ancestry-09-Z2EiYMtGoNnwW6Tk.htm)|Cooperative Soul|auto-trad|
|[ancestry-09-z54Jl3KRZpA2UaZV.htm](feats/ancestry-09-z54Jl3KRZpA2UaZV.htm)|Tetraelemental Assault|auto-trad|
|[ancestry-09-ZcKW5n7F0oAqRw5o.htm](feats/ancestry-09-ZcKW5n7F0oAqRw5o.htm)|Death's Drums|auto-trad|
|[ancestry-09-ZGnmky2B3v1pbPDA.htm](feats/ancestry-09-ZGnmky2B3v1pbPDA.htm)|Strand Strider|auto-trad|
|[ancestry-09-ZGT7NLpcvREkJsMd.htm](feats/ancestry-09-ZGT7NLpcvREkJsMd.htm)|Repair Module|auto-trad|
|[ancestry-09-zL6zUrt44tZYyuh4.htm](feats/ancestry-09-zL6zUrt44tZYyuh4.htm)|Between The Scales|auto-trad|
|[ancestry-09-zPJ2NyMv97AfkN3P.htm](feats/ancestry-09-zPJ2NyMv97AfkN3P.htm)|Occult Resistance|auto-trad|
|[ancestry-09-ZPJbjH5XCp39TVu7.htm](feats/ancestry-09-ZPJbjH5XCp39TVu7.htm)|Ragdya's Dance|auto-trad|
|[ancestry-09-ZPK2Un5ChzeNc9Dx.htm](feats/ancestry-09-ZPK2Un5ChzeNc9Dx.htm)|Cautious Curiosity|auto-trad|
|[ancestry-09-zpQEOqMDoujexNfA.htm](feats/ancestry-09-zpQEOqMDoujexNfA.htm)|Replenishing Hydration|auto-trad|
|[ancestry-09-zsubK6PaY58fOYCb.htm](feats/ancestry-09-zsubK6PaY58fOYCb.htm)|Lucky Keepsake|auto-trad|
|[ancestry-09-zsWIZQeVhIihNw6M.htm](feats/ancestry-09-zsWIZQeVhIihNw6M.htm)|Charred Remains|auto-trad|
|[ancestry-09-ZvrzK8NM390k139E.htm](feats/ancestry-09-ZvrzK8NM390k139E.htm)|Drain Emotion|auto-trad|
|[ancestry-09-zZOycr00XDVTKuXa.htm](feats/ancestry-09-zZOycr00XDVTKuXa.htm)|Undying Ferocity|auto-trad|
|[ancestry-13-05wQPB1Z20DOy4rH.htm](feats/ancestry-13-05wQPB1Z20DOy4rH.htm)|Summon Fiendish Kin|auto-trad|
|[ancestry-13-0jJ5FG72lydY3HHR.htm](feats/ancestry-13-0jJ5FG72lydY3HHR.htm)|Hydraulic Maneuvers|auto-trad|
|[ancestry-13-14dFcInubWcPnFzR.htm](feats/ancestry-13-14dFcInubWcPnFzR.htm)|Eternal Memories|auto-trad|
|[ancestry-13-1jZ7f4TJqiFH8Ied.htm](feats/ancestry-13-1jZ7f4TJqiFH8Ied.htm)|Thunder God's Fan|auto-trad|
|[ancestry-13-1lpygRsa487Jto4L.htm](feats/ancestry-13-1lpygRsa487Jto4L.htm)|Revivification Protocol|auto-trad|
|[ancestry-13-1WPKV2sInXtiUaeG.htm](feats/ancestry-13-1WPKV2sInXtiUaeG.htm)|Flame Jump|auto-trad|
|[ancestry-13-2jy4uh04yz0ezz6Q.htm](feats/ancestry-13-2jy4uh04yz0ezz6Q.htm)|Reanimating Spark|auto-trad|
|[ancestry-13-3X8BkDZEDWp90U4u.htm](feats/ancestry-13-3X8BkDZEDWp90U4u.htm)|Ancestor's Transformation|auto-trad|
|[ancestry-13-3YByhZJi93ie5F45.htm](feats/ancestry-13-3YByhZJi93ie5F45.htm)|Shaitan Skin|auto-trad|
|[ancestry-13-48z7BPYIZJIgj3x5.htm](feats/ancestry-13-48z7BPYIZJIgj3x5.htm)|We March On|auto-trad|
|[ancestry-13-55XNy1TVETEMc0vf.htm](feats/ancestry-13-55XNy1TVETEMc0vf.htm)|Stubborn Persistence|auto-trad|
|[ancestry-13-569SxRBvTCDiHlbW.htm](feats/ancestry-13-569SxRBvTCDiHlbW.htm)|Summon Air Elemental|auto-trad|
|[ancestry-13-5BEY7VzDVuiAg4PX.htm](feats/ancestry-13-5BEY7VzDVuiAg4PX.htm)|Impossible Gossip|auto-trad|
|[ancestry-13-5H2KmhiIGuPxKwBK.htm](feats/ancestry-13-5H2KmhiIGuPxKwBK.htm)|Nagaji Spell expertise|auto-trad|
|[ancestry-13-5iHB5ZFJ25XrZHye.htm](feats/ancestry-13-5iHB5ZFJ25XrZHye.htm)|Inspirit Hazard|auto-trad|
|[ancestry-13-5q8a36QyYAslgnsk.htm](feats/ancestry-13-5q8a36QyYAslgnsk.htm)|Eldritch Calm|auto-trad|
|[ancestry-13-607fIUg4JjG0aIRx.htm](feats/ancestry-13-607fIUg4JjG0aIRx.htm)|Goblin Weapon Expertise|auto-trad|
|[ancestry-13-6GGPTYwljGeGL2B3.htm](feats/ancestry-13-6GGPTYwljGeGL2B3.htm)|Grippli Weapon Expertise|auto-trad|
|[ancestry-13-7bVJEZt2vwAAnILV.htm](feats/ancestry-13-7bVJEZt2vwAAnILV.htm)|Suli Amir|auto-trad|
|[ancestry-13-7Lx8rsEbBJkg6C17.htm](feats/ancestry-13-7Lx8rsEbBJkg6C17.htm)|Gift Of The Moon|auto-trad|
|[ancestry-13-7NsqI50oBIJ4bFwb.htm](feats/ancestry-13-7NsqI50oBIJ4bFwb.htm)|Monkey Spirits|auto-trad|
|[ancestry-13-8H72RC7QI1i8wjJ1.htm](feats/ancestry-13-8H72RC7QI1i8wjJ1.htm)|Warren Digger|auto-trad|
|[ancestry-13-8JOLT1UX5BVG4kVY.htm](feats/ancestry-13-8JOLT1UX5BVG4kVY.htm)|Tengu Weapon Expertise|auto-trad|
|[ancestry-13-8Qn80RunXaChOM5p.htm](feats/ancestry-13-8Qn80RunXaChOM5p.htm)|Shadow's Assault|auto-trad|
|[ancestry-13-8VXYwHE5LqAGRGTB.htm](feats/ancestry-13-8VXYwHE5LqAGRGTB.htm)|Kashrishi Revivification|auto-trad|
|[ancestry-13-93vQcuKBESXUKoH5.htm](feats/ancestry-13-93vQcuKBESXUKoH5.htm)|Killing Stone|auto-trad|
|[ancestry-13-99WRahrMC91D6MMe.htm](feats/ancestry-13-99WRahrMC91D6MMe.htm)|Unbreakable-er Goblin|auto-trad|
|[ancestry-13-9eL7W4rvs4sjhWFT.htm](feats/ancestry-13-9eL7W4rvs4sjhWFT.htm)|Radiant Burst|auto-trad|
|[ancestry-13-ACeqRSWr4CEwLZgO.htm](feats/ancestry-13-ACeqRSWr4CEwLZgO.htm)|Shadow Pact|auto-trad|
|[ancestry-13-ADDOwO0QWOXAV85x.htm](feats/ancestry-13-ADDOwO0QWOXAV85x.htm)|Telluric Power|auto-trad|
|[ancestry-13-aEhUX2c1xj57CMw5.htm](feats/ancestry-13-aEhUX2c1xj57CMw5.htm)|Vishkanya Weapon Expertise|auto-trad|
|[ancestry-13-AP9PXXaP4a2sMdt2.htm](feats/ancestry-13-AP9PXXaP4a2sMdt2.htm)|Enforced Order|auto-trad|
|[ancestry-13-aQHB5LhxLaFRkzSt.htm](feats/ancestry-13-aQHB5LhxLaFRkzSt.htm)|Finest Trick|auto-trad|
|[ancestry-13-aqTlEPtd5gzs1Lxg.htm](feats/ancestry-13-aqTlEPtd5gzs1Lxg.htm)|Unconventional Expertise|auto-trad|
|[ancestry-13-arlZTqfppOAXBhdw.htm](feats/ancestry-13-arlZTqfppOAXBhdw.htm)|Irriseni Ice-Witch|auto-trad|
|[ancestry-13-aRqXpCBWic6a3DQH.htm](feats/ancestry-13-aRqXpCBWic6a3DQH.htm)|Venom Purge|auto-trad|
|[ancestry-13-AxqnIMh5WbSah5OS.htm](feats/ancestry-13-AxqnIMh5WbSah5OS.htm)|Mischievous Tail|auto-trad|
|[ancestry-13-b5j3boj1iEcXSB9f.htm](feats/ancestry-13-b5j3boj1iEcXSB9f.htm)|Reimagine|auto-trad|
|[ancestry-13-bi77bT9uyAmJXVed.htm](feats/ancestry-13-bi77bT9uyAmJXVed.htm)|Truespeech|auto-trad|
|[ancestry-13-bj0y7JnIboNfCuC8.htm](feats/ancestry-13-bj0y7JnIboNfCuC8.htm)|Gnoll Weapon Expertise|auto-trad|
|[ancestry-13-BTQj2N5erpJDWNFA.htm](feats/ancestry-13-BTQj2N5erpJDWNFA.htm)|Astral Blink|auto-trad|
|[ancestry-13-c5xHL6CDFwBqXx2a.htm](feats/ancestry-13-c5xHL6CDFwBqXx2a.htm)|Spiteful Rake|auto-trad|
|[ancestry-13-CAfrSLaDM0OBaNtp.htm](feats/ancestry-13-CAfrSLaDM0OBaNtp.htm)|See the Unseen|auto-trad|
|[ancestry-13-Cf0CDTZvGaYDAXUN.htm](feats/ancestry-13-Cf0CDTZvGaYDAXUN.htm)|Calaca's Showstopper|auto-trad|
|[ancestry-13-cIJfKlbfKezdDbwK.htm](feats/ancestry-13-cIJfKlbfKezdDbwK.htm)|Summon Fire Elemental|auto-trad|
|[ancestry-13-cpsyKarYRHiOF0Nd.htm](feats/ancestry-13-cpsyKarYRHiOF0Nd.htm)|None Shall Know|auto-trad|
|[ancestry-13-d3J74jmGOrPBWUm9.htm](feats/ancestry-13-d3J74jmGOrPBWUm9.htm)|Disruptive Stare|auto-trad|
|[ancestry-13-dgpoTae18H4zc9fH.htm](feats/ancestry-13-dgpoTae18H4zc9fH.htm)|Catfolk Weapon Expertise|auto-trad|
|[ancestry-13-DIjpbE2dh5MRGiYO.htm](feats/ancestry-13-DIjpbE2dh5MRGiYO.htm)|Impose Order|auto-trad|
|[ancestry-13-DNZlWe2V28KoajoN.htm](feats/ancestry-13-DNZlWe2V28KoajoN.htm)|Eclectic Sword Mastery|auto-trad|
|[ancestry-13-dor2F3g8gL2KVwX6.htm](feats/ancestry-13-dor2F3g8gL2KVwX6.htm)|Skirt The Light|auto-trad|
|[ancestry-13-E0EARGd5iryHJGJD.htm](feats/ancestry-13-E0EARGd5iryHJGJD.htm)|Unbound Freedom|auto-trad|
|[ancestry-13-ECcFHAeAmh5F3mxg.htm](feats/ancestry-13-ECcFHAeAmh5F3mxg.htm)|Azarketi Weapon Expertise|auto-trad|
|[ancestry-13-eGfYZoWC6cDa2XWd.htm](feats/ancestry-13-eGfYZoWC6cDa2XWd.htm)|Jinx Glutton|auto-trad|
|[ancestry-13-EiiCCJqWnN5RYMV4.htm](feats/ancestry-13-EiiCCJqWnN5RYMV4.htm)|Universal Longevity|auto-trad|
|[ancestry-13-EIyazsXwM7Zc2XGO.htm](feats/ancestry-13-EIyazsXwM7Zc2XGO.htm)|Arcane Sight|auto-trad|
|[ancestry-13-ewwhgHZbxHcpMdLn.htm](feats/ancestry-13-ewwhgHZbxHcpMdLn.htm)|Glamour|auto-trad|
|[ancestry-13-FI4MnH0KQfIKJRNT.htm](feats/ancestry-13-FI4MnH0KQfIKJRNT.htm)|Arcane Locomotion|auto-trad|
|[ancestry-13-Fn8dEIcUZVxuWJgN.htm](feats/ancestry-13-Fn8dEIcUZVxuWJgN.htm)|Summon Water Elemental|auto-trad|
|[ancestry-13-fnMT0AsZXFW9Ppyp.htm](feats/ancestry-13-fnMT0AsZXFW9Ppyp.htm)|Cloak Of Poison|auto-trad|
|[ancestry-13-FqZKSSBU7M4zhsXM.htm](feats/ancestry-13-FqZKSSBU7M4zhsXM.htm)|Resist Ruin|auto-trad|
|[ancestry-13-fX5FybM93HIQRRd1.htm](feats/ancestry-13-fX5FybM93HIQRRd1.htm)|Black Cat Curse|auto-trad|
|[ancestry-13-g6M5mapOXVf0g9BG.htm](feats/ancestry-13-g6M5mapOXVf0g9BG.htm)|Orc Weapon Expertise|auto-trad|
|[ancestry-13-GELhS1k0vHEi1PK3.htm](feats/ancestry-13-GELhS1k0vHEi1PK3.htm)|Incredible Ferocity|auto-trad|
|[ancestry-13-GfpHBiOX02PQz0Pm.htm](feats/ancestry-13-GfpHBiOX02PQz0Pm.htm)|Spew Tentacles|auto-trad|
|[ancestry-13-Gh0rJNsdxBacK9b8.htm](feats/ancestry-13-Gh0rJNsdxBacK9b8.htm)|Conrasu Weapon Expertise|auto-trad|
|[ancestry-13-GndRoEYD3uWjJji3.htm](feats/ancestry-13-GndRoEYD3uWjJji3.htm)|Purge Sins|auto-trad|
|[ancestry-13-gsW6mCMBWquLM3bj.htm](feats/ancestry-13-gsW6mCMBWquLM3bj.htm)|Halfling Weapon Expertise|auto-trad|
|[ancestry-13-gwzjvKQQ6zmgVVmS.htm](feats/ancestry-13-gwzjvKQQ6zmgVVmS.htm)|Daywalker|auto-trad|
|[ancestry-13-H965m1koFvY4FQkF.htm](feats/ancestry-13-H965m1koFvY4FQkF.htm)|Form Of The Bat|auto-trad|
|[ancestry-13-hOD9de1ftfYRSEKn.htm](feats/ancestry-13-hOD9de1ftfYRSEKn.htm)|Airy Step|auto-trad|
|[ancestry-13-HpAUTAzEAK7mGsJ8.htm](feats/ancestry-13-HpAUTAzEAK7mGsJ8.htm)|Scrutinizing Gaze|auto-trad|
|[ancestry-13-hSHteYr1g1gKHcwM.htm](feats/ancestry-13-hSHteYr1g1gKHcwM.htm)|Fey Skin|auto-trad|
|[ancestry-13-Ht6b8H9DpA9lWzAg.htm](feats/ancestry-13-Ht6b8H9DpA9lWzAg.htm)|Advanced General Training|auto-trad|
|[ancestry-13-HWLD3zGFBIjCDATo.htm](feats/ancestry-13-HWLD3zGFBIjCDATo.htm)|Planar Sidestep|auto-trad|
|[ancestry-13-hXfyIWMtvB2kAEvA.htm](feats/ancestry-13-hXfyIWMtvB2kAEvA.htm)|Aquatic Camouflage|auto-trad|
|[ancestry-13-I3Md8XYhuMVSJqKI.htm](feats/ancestry-13-I3Md8XYhuMVSJqKI.htm)|Continuous Assault|auto-trad|
|[ancestry-13-i3wSkeU7CSyHEi4Y.htm](feats/ancestry-13-i3wSkeU7CSyHEi4Y.htm)|Genie Weapon Expertise|auto-trad|
|[ancestry-13-I9cJYC7anz7HmpcJ.htm](feats/ancestry-13-I9cJYC7anz7HmpcJ.htm)|Elite Dracomancer|auto-trad|
|[ancestry-13-Ih85PWZSVTwU0xkI.htm](feats/ancestry-13-Ih85PWZSVTwU0xkI.htm)|Lifeblood's Call|auto-trad|
|[ancestry-13-iTsLr3zEaGZ45zez.htm](feats/ancestry-13-iTsLr3zEaGZ45zez.htm)|Incredible Luck (Halfling)|auto-trad|
|[ancestry-13-j0fgquODHTyyekyO.htm](feats/ancestry-13-j0fgquODHTyyekyO.htm)|Kobold Weapon Expertise|auto-trad|
|[ancestry-13-J2CPfHKPvu6RGfY6.htm](feats/ancestry-13-J2CPfHKPvu6RGfY6.htm)|Fully Flighted|auto-trad|
|[ancestry-13-j49fEU2TWJUaxD30.htm](feats/ancestry-13-j49fEU2TWJUaxD30.htm)|Gnome Weapon Expertise|auto-trad|
|[ancestry-13-j8tukQK5FT1Vfx2G.htm](feats/ancestry-13-j8tukQK5FT1Vfx2G.htm)|Crafter's Instinct|auto-trad|
|[ancestry-13-JAl4t2KINE8nI8KY.htm](feats/ancestry-13-JAl4t2KINE8nI8KY.htm)|Ceremony of Growth|auto-trad|
|[ancestry-13-jatfexNkXaTs9s5Q.htm](feats/ancestry-13-jatfexNkXaTs9s5Q.htm)|Instinctive Obfuscation|auto-trad|
|[ancestry-13-JlkkYedPyWLShuka.htm](feats/ancestry-13-JlkkYedPyWLShuka.htm)|Kitsune Spell Expertise|auto-trad|
|[ancestry-13-JmVuumcmqSrAmxdm.htm](feats/ancestry-13-JmVuumcmqSrAmxdm.htm)|Ghoran Weapon Expertise|auto-trad|
|[ancestry-13-JQxFvMHu0ffo56RT.htm](feats/ancestry-13-JQxFvMHu0ffo56RT.htm)|Shinstabber|auto-trad|
|[ancestry-13-k2L9p4cc8RrHufut.htm](feats/ancestry-13-k2L9p4cc8RrHufut.htm)|Look But Don't Touch|auto-trad|
|[ancestry-13-kBnsLuh3eBLjwGpN.htm](feats/ancestry-13-kBnsLuh3eBLjwGpN.htm)|Hatchling Flight|auto-trad|
|[ancestry-13-KboQgBhYXI2WdCt8.htm](feats/ancestry-13-KboQgBhYXI2WdCt8.htm)|Mask Of Fear|auto-trad|
|[ancestry-13-KQ5xOAqZ1KPN3FgA.htm](feats/ancestry-13-KQ5xOAqZ1KPN3FgA.htm)|Stronger Debilitating Venom|auto-trad|
|[ancestry-13-KRfZcToCc5nvcQRa.htm](feats/ancestry-13-KRfZcToCc5nvcQRa.htm)|Harbinger's Caw|auto-trad|
|[ancestry-13-l11OWMFUixCcRYGm.htm](feats/ancestry-13-l11OWMFUixCcRYGm.htm)|Aquatic Adaptation|auto-trad|
|[ancestry-13-L231BR4815B6hwKT.htm](feats/ancestry-13-L231BR4815B6hwKT.htm)|Summon Celestial Kin|auto-trad|
|[ancestry-13-l78KwojlT0AWvS4l.htm](feats/ancestry-13-l78KwojlT0AWvS4l.htm)|Toppling Dance|auto-trad|
|[ancestry-13-lIzsj8XlcL0tqQcm.htm](feats/ancestry-13-lIzsj8XlcL0tqQcm.htm)|Cannibalize Magic|auto-trad|
|[ancestry-13-lrTRELs8uDpVpsk0.htm](feats/ancestry-13-lrTRELs8uDpVpsk0.htm)|Summon Earth Elemental|auto-trad|
|[ancestry-13-LTcpBbnngfuYTdB0.htm](feats/ancestry-13-LTcpBbnngfuYTdB0.htm)|Avenge Ally|auto-trad|
|[ancestry-13-LxAEF9VPwUVttA0F.htm](feats/ancestry-13-LxAEF9VPwUVttA0F.htm)|Delver|auto-trad|
|[ancestry-13-M8pDuRo6tO0Kok9z.htm](feats/ancestry-13-M8pDuRo6tO0Kok9z.htm)|Secret Eyes|auto-trad|
|[ancestry-13-MII0ybm76DkDxsId.htm](feats/ancestry-13-MII0ybm76DkDxsId.htm)|Cobble Dancer|auto-trad|
|[ancestry-13-MR4X38qgBj5tmkMw.htm](feats/ancestry-13-MR4X38qgBj5tmkMw.htm)|Bounce Back|auto-trad|
|[ancestry-13-n82gTpAENgr6XjCM.htm](feats/ancestry-13-n82gTpAENgr6XjCM.htm)|Translucent Skin|auto-trad|
|[ancestry-13-NaY9HXeQ0yhoIorO.htm](feats/ancestry-13-NaY9HXeQ0yhoIorO.htm)|Skittering Sneak|auto-trad|
|[ancestry-13-ne7nVluvvVXMvuB1.htm](feats/ancestry-13-ne7nVluvvVXMvuB1.htm)|Wandering Heart|auto-trad|
|[ancestry-13-nePEcAp7lTL35uyx.htm](feats/ancestry-13-nePEcAp7lTL35uyx.htm)|Caterwaul|auto-trad|
|[ancestry-13-NN9sKez7Z9R8q1i9.htm](feats/ancestry-13-NN9sKez7Z9R8q1i9.htm)|Vanara Battle Clarity|auto-trad|
|[ancestry-13-O65q744snXtC5Uc4.htm](feats/ancestry-13-O65q744snXtC5Uc4.htm)|Consistent Surge|auto-trad|
|[ancestry-13-ODy9tq7NPlD5fxzZ.htm](feats/ancestry-13-ODy9tq7NPlD5fxzZ.htm)|Pit of Snakes|auto-trad|
|[ancestry-13-okJjCvQ6hcCk8FOC.htm](feats/ancestry-13-okJjCvQ6hcCk8FOC.htm)|Malleable Form|auto-trad|
|[ancestry-13-Ot1jI91ccnB2ayfk.htm](feats/ancestry-13-Ot1jI91ccnB2ayfk.htm)|Celestial Strikes|auto-trad|
|[ancestry-13-OwJRuy4EW8vW09AI.htm](feats/ancestry-13-OwJRuy4EW8vW09AI.htm)|Metal-veined Strikes|auto-trad|
|[ancestry-13-pedHIDAVLFzzjGO1.htm](feats/ancestry-13-pedHIDAVLFzzjGO1.htm)|Spell Devourer|auto-trad|
|[ancestry-13-PfHvTR0L9ShjhWiP.htm](feats/ancestry-13-PfHvTR0L9ShjhWiP.htm)|Ceaseless Shadows|auto-trad|
|[ancestry-13-pivX2SxPQjKDyvHU.htm](feats/ancestry-13-pivX2SxPQjKDyvHU.htm)|Iruxi Spirit Strike|auto-trad|
|[ancestry-13-plhQDES7yb6xDAXL.htm](feats/ancestry-13-plhQDES7yb6xDAXL.htm)|Shory Aerialist|auto-trad|
|[ancestry-13-PvODcTsmFtXffaZn.htm](feats/ancestry-13-PvODcTsmFtXffaZn.htm)|Idol Threat|auto-trad|
|[ancestry-13-QHPtlFvTpgx1GE7S.htm](feats/ancestry-13-QHPtlFvTpgx1GE7S.htm)|Formation Master|auto-trad|
|[ancestry-13-QjHZN1uAxl2hSbej.htm](feats/ancestry-13-QjHZN1uAxl2hSbej.htm)|Vicious Snares|auto-trad|
|[ancestry-13-rCajlx2KjGxzabAJ.htm](feats/ancestry-13-rCajlx2KjGxzabAJ.htm)|Fiend's Door|auto-trad|
|[ancestry-13-rHBmZi95n0pcQqxs.htm](feats/ancestry-13-rHBmZi95n0pcQqxs.htm)|Skeleton Commander|auto-trad|
|[ancestry-13-s0OqtQOeYOGkBMYG.htm](feats/ancestry-13-s0OqtQOeYOGkBMYG.htm)|Steadfast Ally|auto-trad|
|[ancestry-13-S1Z5dFAkMKkFSofk.htm](feats/ancestry-13-S1Z5dFAkMKkFSofk.htm)|War Conditioning|auto-trad|
|[ancestry-13-SzkpazSZ0pi1WZsH.htm](feats/ancestry-13-SzkpazSZ0pi1WZsH.htm)|Squirm Free|auto-trad|
|[ancestry-13-T3cwVrRT0VMZIwpT.htm](feats/ancestry-13-T3cwVrRT0VMZIwpT.htm)|One With Earth|auto-trad|
|[ancestry-13-t6GBBIwX7hvvxYyV.htm](feats/ancestry-13-t6GBBIwX7hvvxYyV.htm)|Hag Magic|auto-trad|
|[ancestry-13-TGDy2R6Dq8hwZir4.htm](feats/ancestry-13-TGDy2R6Dq8hwZir4.htm)|Redirect Attention|auto-trad|
|[ancestry-13-tpkih32Ch2wCA9R5.htm](feats/ancestry-13-tpkih32Ch2wCA9R5.htm)|Dwarven Weapon Expertise|auto-trad|
|[ancestry-13-TtSLIMiNw1hqMui5.htm](feats/ancestry-13-TtSLIMiNw1hqMui5.htm)|Skeletal Transformation|auto-trad|
|[ancestry-13-TYe3dsNlks91YAks.htm](feats/ancestry-13-TYe3dsNlks91YAks.htm)|Explosive Expert|auto-trad|
|[ancestry-13-u0DA0gkrZxXb0Hle.htm](feats/ancestry-13-u0DA0gkrZxXb0Hle.htm)|Very, Very Sneaky|auto-trad|
|[ancestry-13-uclbKFsrqCW6tQmB.htm](feats/ancestry-13-uclbKFsrqCW6tQmB.htm)|Core Rejuvenation|auto-trad|
|[ancestry-13-uDFgL6uCwJsaTHi3.htm](feats/ancestry-13-uDFgL6uCwJsaTHi3.htm)|Call Of The Green Man|auto-trad|
|[ancestry-13-uQpAPkJ4ygzFZcIt.htm](feats/ancestry-13-uQpAPkJ4ygzFZcIt.htm)|Vanth's Weapon Expertise|auto-trad|
|[ancestry-13-V6iJ4v15Qc8awVCH.htm](feats/ancestry-13-V6iJ4v15Qc8awVCH.htm)|Aasimar's Mercy|auto-trad|
|[ancestry-13-vWUPSvEBHr5AAPUU.htm](feats/ancestry-13-vWUPSvEBHr5AAPUU.htm)|Violent Vines|auto-trad|
|[ancestry-13-VXAIElMlMnVvz3x5.htm](feats/ancestry-13-VXAIElMlMnVvz3x5.htm)|Webslinger|auto-trad|
|[ancestry-13-wHxXVknciaD4X8Ch.htm](feats/ancestry-13-wHxXVknciaD4X8Ch.htm)|Unrivaled Builder|auto-trad|
|[ancestry-13-wruf4sMh3P7o8k1P.htm](feats/ancestry-13-wruf4sMh3P7o8k1P.htm)|Elven Weapon Expertise|auto-trad|
|[ancestry-13-wteuGNhOXLvBudRQ.htm](feats/ancestry-13-wteuGNhOXLvBudRQ.htm)|Ferocious Beasts|auto-trad|
|[ancestry-13-WWCZef1PBliiot9Q.htm](feats/ancestry-13-WWCZef1PBliiot9Q.htm)|Enlarged Chassis|auto-trad|
|[ancestry-13-X1SPiYJqWzqwzPTs.htm](feats/ancestry-13-X1SPiYJqWzqwzPTs.htm)|Primal Rampage|auto-trad|
|[ancestry-13-X5NRdQ1W0Y5t6MFi.htm](feats/ancestry-13-X5NRdQ1W0Y5t6MFi.htm)|Dire Form|auto-trad|
|[ancestry-13-xjE123p2rPTsz1eV.htm](feats/ancestry-13-xjE123p2rPTsz1eV.htm)|Fiendish Strikes|auto-trad|
|[ancestry-13-XJfUj2o4HdL5waZL.htm](feats/ancestry-13-XJfUj2o4HdL5waZL.htm)|Can't Fall Here|auto-trad|
|[ancestry-13-XOudHBESJ192FBwF.htm](feats/ancestry-13-XOudHBESJ192FBwF.htm)|Rehydration|auto-trad|
|[ancestry-13-XPVqOtHLN6jduHYs.htm](feats/ancestry-13-XPVqOtHLN6jduHYs.htm)|Hobgoblin Weapon Expertise|auto-trad|
|[ancestry-13-xTLy2AyVW90TrQk6.htm](feats/ancestry-13-xTLy2AyVW90TrQk6.htm)|Improved Elemental Bulwark|auto-trad|
|[ancestry-13-XuL3g2ExgadFtWbb.htm](feats/ancestry-13-XuL3g2ExgadFtWbb.htm)|Bone Investiture|auto-trad|
|[ancestry-13-XzTklOArNQle7rQP.htm](feats/ancestry-13-XzTklOArNQle7rQP.htm)|Invisible Trickster|auto-trad|
|[ancestry-13-Y5ompgSd1JGSHt0C.htm](feats/ancestry-13-Y5ompgSd1JGSHt0C.htm)|Augment Senses|auto-trad|
|[ancestry-13-YDfcwp0NVbmpgxej.htm](feats/ancestry-13-YDfcwp0NVbmpgxej.htm)|Vanara Weapoin Expertise|auto-trad|
|[ancestry-13-YryDlMt3zARg6j7T.htm](feats/ancestry-13-YryDlMt3zARg6j7T.htm)|Arise, Ye Worthy!|auto-trad|
|[ancestry-13-ytgmam8COVq8B7Do.htm](feats/ancestry-13-ytgmam8COVq8B7Do.htm)|Ancestor's Rage|auto-trad|
|[ancestry-13-Z15QFC45psi0txWg.htm](feats/ancestry-13-Z15QFC45psi0txWg.htm)|Alter Resistance|auto-trad|
|[ancestry-13-ZlPCQ3Qh1cgS9r87.htm](feats/ancestry-13-ZlPCQ3Qh1cgS9r87.htm)|Mist Strider|auto-trad|
|[ancestry-17-2pYCGPCQDHD3o7Jz.htm](feats/ancestry-17-2pYCGPCQDHD3o7Jz.htm)|Fiendish Word|auto-trad|
|[ancestry-17-4TndDyCjnF7pc1GC.htm](feats/ancestry-17-4TndDyCjnF7pc1GC.htm)|Ghoran's Wrath|auto-trad|
|[ancestry-17-5FHLom2tpC0X3nbf.htm](feats/ancestry-17-5FHLom2tpC0X3nbf.htm)|Greater Augmentation|auto-trad|
|[ancestry-17-5qVCl5MwPcx0sS7T.htm](feats/ancestry-17-5qVCl5MwPcx0sS7T.htm)|Elude Trouble|auto-trad|
|[ancestry-17-5Unc9AhGAAw1klFN.htm](feats/ancestry-17-5Unc9AhGAAw1klFN.htm)|Animal Swiftness|auto-trad|
|[ancestry-17-6NXlg11EeCqjOmSg.htm](feats/ancestry-17-6NXlg11EeCqjOmSg.htm)|Twist Healing|auto-trad|
|[ancestry-17-715iDDXFPguSO9Or.htm](feats/ancestry-17-715iDDXFPguSO9Or.htm)|Bend Space|auto-trad|
|[ancestry-17-7iB1yacjF9fG6Rvn.htm](feats/ancestry-17-7iB1yacjF9fG6Rvn.htm)|Yamaraj's Grandeur|auto-trad|
|[ancestry-17-7YvOqcdp9Z0RALMp.htm](feats/ancestry-17-7YvOqcdp9Z0RALMp.htm)|Shadow Self|auto-trad|
|[ancestry-17-8l3qDZrfhxUGijjB.htm](feats/ancestry-17-8l3qDZrfhxUGijjB.htm)|Rallying Cry|auto-trad|
|[ancestry-17-8ZWMJYNgyH5zh1yH.htm](feats/ancestry-17-8ZWMJYNgyH5zh1yH.htm)|Eternal Wings (Sylph)|auto-trad|
|[ancestry-17-9cq4j15EbVyrHKFX.htm](feats/ancestry-17-9cq4j15EbVyrHKFX.htm)|Forge-Blessed Shot|auto-trad|
|[ancestry-17-9eaUS0jJCpxuNXO5.htm](feats/ancestry-17-9eaUS0jJCpxuNXO5.htm)|Rampaging Ferocity|auto-trad|
|[ancestry-17-ABXUfGoeBgCyFasg.htm](feats/ancestry-17-ABXUfGoeBgCyFasg.htm)|Great Tengu Form|auto-trad|
|[ancestry-17-bPPcPUZMlJ7m5lYq.htm](feats/ancestry-17-bPPcPUZMlJ7m5lYq.htm)|Tidal Shield|auto-trad|
|[ancestry-17-BS6UoGLyJ12xVD9P.htm](feats/ancestry-17-BS6UoGLyJ12xVD9P.htm)|Ratfolk Growth|auto-trad|
|[ancestry-17-cayKGa2yK7Gwvk6m.htm](feats/ancestry-17-cayKGa2yK7Gwvk6m.htm)|Olethros's Decree|auto-trad|
|[ancestry-17-CuyuwFht0rVbGMca.htm](feats/ancestry-17-CuyuwFht0rVbGMca.htm)|Relentless Wings|auto-trad|
|[ancestry-17-dftzUTOrj9dQyN3q.htm](feats/ancestry-17-dftzUTOrj9dQyN3q.htm)|Restitch|auto-trad|
|[ancestry-17-Dxac0mgye7EFFVou.htm](feats/ancestry-17-Dxac0mgye7EFFVou.htm)|Radiate Glory|auto-trad|
|[ancestry-17-DYFOlJlMoDuHjrZx.htm](feats/ancestry-17-DYFOlJlMoDuHjrZx.htm)|Channel The Godmind|auto-trad|
|[ancestry-17-EEnL4zyCDo4HD6Rn.htm](feats/ancestry-17-EEnL4zyCDo4HD6Rn.htm)|Fey Transcendence|auto-trad|
|[ancestry-17-ERCd44zQMYI4rgqM.htm](feats/ancestry-17-ERCd44zQMYI4rgqM.htm)|Animal Swiftness (Land)|auto-trad|
|[ancestry-17-FBhBnyGX0Uje0tbJ.htm](feats/ancestry-17-FBhBnyGX0Uje0tbJ.htm)|Storm Form|auto-trad|
|[ancestry-17-ffjoPXtAoIR2Drwe.htm](feats/ancestry-17-ffjoPXtAoIR2Drwe.htm)|Regrowth|auto-trad|
|[ancestry-17-fqw1ELaqavuKLHIj.htm](feats/ancestry-17-fqw1ELaqavuKLHIj.htm)|Reckless Abandon (Goblin)|auto-trad|
|[ancestry-17-FWfKbAxTct6q3AMp.htm](feats/ancestry-17-FWfKbAxTct6q3AMp.htm)|Vicious Venom|auto-trad|
|[ancestry-17-gXAAJCnjfCDK7YV2.htm](feats/ancestry-17-gXAAJCnjfCDK7YV2.htm)|Cleansing Light|auto-trad|
|[ancestry-17-GYvaR6ZD8ZKdQWrF.htm](feats/ancestry-17-GYvaR6ZD8ZKdQWrF.htm)|Stone Form|auto-trad|
|[ancestry-17-h9b5CId7S7gV7j2t.htm](feats/ancestry-17-h9b5CId7S7gV7j2t.htm)|Axial Recall|auto-trad|
|[ancestry-17-hkU92nqUYBQLQSMt.htm](feats/ancestry-17-hkU92nqUYBQLQSMt.htm)|Blazing Aura|auto-trad|
|[ancestry-17-hNr4OrMdCMhQLbtB.htm](feats/ancestry-17-hNr4OrMdCMhQLbtB.htm)|Wyrmling Flight|auto-trad|
|[ancestry-17-hZUFP1NPwdGMzs1y.htm](feats/ancestry-17-hZUFP1NPwdGMzs1y.htm)|Ceremony of Sun's Gift|auto-trad|
|[ancestry-17-I7S8Snq8FlrBHmbf.htm](feats/ancestry-17-I7S8Snq8FlrBHmbf.htm)|Ten Lives|auto-trad|
|[ancestry-17-iJrjzLnLJkvQgrbS.htm](feats/ancestry-17-iJrjzLnLJkvQgrbS.htm)|Hurricane Swing|auto-trad|
|[ancestry-17-IOlvpmeIwlk0IACr.htm](feats/ancestry-17-IOlvpmeIwlk0IACr.htm)|Bone Rider|auto-trad|
|[ancestry-17-iVwsLYjOJbfvL0Pe.htm](feats/ancestry-17-iVwsLYjOJbfvL0Pe.htm)|Heroic Presence|auto-trad|
|[ancestry-17-IyFz7kVF8wzPdrfJ.htm](feats/ancestry-17-IyFz7kVF8wzPdrfJ.htm)|Unfettering Prankster|auto-trad|
|[ancestry-17-IzJ76OXe1gl2hfbd.htm](feats/ancestry-17-IzJ76OXe1gl2hfbd.htm)|Stormy Heart|auto-trad|
|[ancestry-17-JgGsWppVh4q4HtSy.htm](feats/ancestry-17-JgGsWppVh4q4HtSy.htm)|Dominion Aura|auto-trad|
|[ancestry-17-JrBqOxHZX20b8gTT.htm](feats/ancestry-17-JrBqOxHZX20b8gTT.htm)|Soaring Poppet|auto-trad|
|[ancestry-17-KL3Pk10ReItAHTw9.htm](feats/ancestry-17-KL3Pk10ReItAHTw9.htm)|Celestial Word|auto-trad|
|[ancestry-17-kr537ZL3f8tCSgDK.htm](feats/ancestry-17-kr537ZL3f8tCSgDK.htm)|True Gaze|auto-trad|
|[ancestry-17-kt5UJEfTzO3LiInN.htm](feats/ancestry-17-kt5UJEfTzO3LiInN.htm)|Crone's Cruelty|auto-trad|
|[ancestry-17-LIEbgqgSXkGIli8u.htm](feats/ancestry-17-LIEbgqgSXkGIli8u.htm)|Animal Swiftness (Fly)|auto-trad|
|[ancestry-17-LoZY48txEgHZxewj.htm](feats/ancestry-17-LoZY48txEgHZxewj.htm)|Azaersi's Roads|auto-trad|
|[ancestry-17-LTIARSoQP8WYE33A.htm](feats/ancestry-17-LTIARSoQP8WYE33A.htm)|Necromantic Heir|auto-trad|
|[ancestry-17-MzdoJWpqyMmMDI1F.htm](feats/ancestry-17-MzdoJWpqyMmMDI1F.htm)|Fountain of Secrets|auto-trad|
|[ancestry-17-Nd7C7X9LYyp0TaET.htm](feats/ancestry-17-Nd7C7X9LYyp0TaET.htm)|Animal Swiftness (Swim)|auto-trad|
|[ancestry-17-nJRcbt72wRk5Rmc4.htm](feats/ancestry-17-nJRcbt72wRk5Rmc4.htm)|Symphony Of Blood|auto-trad|
|[ancestry-17-oItG5dqlPEi8wNnR.htm](feats/ancestry-17-oItG5dqlPEi8wNnR.htm)|Trickster Tengu|auto-trad|
|[ancestry-17-OpSHTPkoZDrqR0Mq.htm](feats/ancestry-17-OpSHTPkoZDrqR0Mq.htm)|Animal Shape|auto-trad|
|[ancestry-17-OWL6ZNVWMU0AFqvZ.htm](feats/ancestry-17-OWL6ZNVWMU0AFqvZ.htm)|Eternal Wings (Aasimar)|auto-trad|
|[ancestry-17-Qc9MH7wT182qasSV.htm](feats/ancestry-17-Qc9MH7wT182qasSV.htm)|Cantorian Restoration|auto-trad|
|[ancestry-17-QgNo1s6nVbKPU4St.htm](feats/ancestry-17-QgNo1s6nVbKPU4St.htm)|Reliable Luck|auto-trad|
|[ancestry-17-QXZFvsZDduxwTJYM.htm](feats/ancestry-17-QXZFvsZDduxwTJYM.htm)|Magic Rider|auto-trad|
|[ancestry-17-rq0lnwZ51AtJ0aux.htm](feats/ancestry-17-rq0lnwZ51AtJ0aux.htm)|Breath of Calamity|auto-trad|
|[ancestry-17-SFnKHPrd65RORTgN.htm](feats/ancestry-17-SFnKHPrd65RORTgN.htm)|Prismatic Scales|auto-trad|
|[ancestry-17-U9nJy5suyYDE0b20.htm](feats/ancestry-17-U9nJy5suyYDE0b20.htm)|Flourish And Ruin|auto-trad|
|[ancestry-17-UKHIKipBvOGhzcSQ.htm](feats/ancestry-17-UKHIKipBvOGhzcSQ.htm)|Homeward Bound|auto-trad|
|[ancestry-17-UOL44Qm1rOVD5TFg.htm](feats/ancestry-17-UOL44Qm1rOVD5TFg.htm)|Underwater Volcano|auto-trad|
|[ancestry-17-UquGIEFKhutgJsEz.htm](feats/ancestry-17-UquGIEFKhutgJsEz.htm)|Stonegate|auto-trad|
|[ancestry-17-VbHuOvF1MM9ls6Tg.htm](feats/ancestry-17-VbHuOvF1MM9ls6Tg.htm)|Favor Of Heaven|auto-trad|
|[ancestry-17-VPKBjEy8frqDMxyO.htm](feats/ancestry-17-VPKBjEy8frqDMxyO.htm)|Greater Enhance Venom|auto-trad|
|[ancestry-17-wfOGyLnuVMFo7Rwy.htm](feats/ancestry-17-wfOGyLnuVMFo7Rwy.htm)|Boneyard's Call|auto-trad|
|[ancestry-17-wFtJlamwRc6rSQmj.htm](feats/ancestry-17-wFtJlamwRc6rSQmj.htm)|Scion Transformation|auto-trad|
|[ancestry-17-WZC2XXQo6Pu2GQeo.htm](feats/ancestry-17-WZC2XXQo6Pu2GQeo.htm)|Soaring Form|auto-trad|
|[ancestry-17-WZLw9UrIyCz6Eiqo.htm](feats/ancestry-17-WZLw9UrIyCz6Eiqo.htm)|Animal Swiftness (Climb)|auto-trad|
|[ancestry-17-zeyrLJr6b7hPdx4w.htm](feats/ancestry-17-zeyrLJr6b7hPdx4w.htm)|Core Cannon|auto-trad|
|[ancestry-17-Zl6vDXJEM7Lbp8JQ.htm](feats/ancestry-17-Zl6vDXJEM7Lbp8JQ.htm)|Rampaging Form|auto-trad|
|[ancestry-17-ZOlwVqWa4hfv44xX.htm](feats/ancestry-17-ZOlwVqWa4hfv44xX.htm)|Hero's Wings|auto-trad|
|[ancestry-17-zSJ0Ke8HWzG1krlq.htm](feats/ancestry-17-zSJ0Ke8HWzG1krlq.htm)|Reflect Foe|auto-trad|
|[archetype-02-0icxuITlxUBfeomX.htm](feats/archetype-02-0icxuITlxUBfeomX.htm)|Turpin Rowe Lumberjack Dedication|auto-trad|
|[archetype-02-0ogbBzSD7fmiWp9d.htm](feats/archetype-02-0ogbBzSD7fmiWp9d.htm)|Living Vessel Dedication|auto-trad|
|[archetype-02-0YjetnLVIUz9zQt5.htm](feats/archetype-02-0YjetnLVIUz9zQt5.htm)|Champion Dedication|auto-trad|
|[archetype-02-1hyC4aiLMBSwM9Z1.htm](feats/archetype-02-1hyC4aiLMBSwM9Z1.htm)|Trick Driver Dedication|auto-trad|
|[archetype-02-1t5479E6bdvFs4E7.htm](feats/archetype-02-1t5479E6bdvFs4E7.htm)|Talisman Dabbler Dedication|auto-trad|
|[archetype-02-20jNBiIIxaiOVyi0.htm](feats/archetype-02-20jNBiIIxaiOVyi0.htm)|Oozemorph Dedication|auto-trad|
|[archetype-02-3r4v6W4abJnLg7dv.htm](feats/archetype-02-3r4v6W4abJnLg7dv.htm)|Alter Ego Dedication|auto-trad|
|[archetype-02-3SGS0chf3SKosG5H.htm](feats/archetype-02-3SGS0chf3SKosG5H.htm)|Shieldmarshal Dedication|auto-trad|
|[archetype-02-4M5zIlJ8EvjQDtg9.htm](feats/archetype-02-4M5zIlJ8EvjQDtg9.htm)|Dandy Dedication|auto-trad|
|[archetype-02-4MUbwilvb9dI0X59.htm](feats/archetype-02-4MUbwilvb9dI0X59.htm)|Snarecrafter Dedication|auto-trad|
|[archetype-02-4QFElZoWjg1X0vsg.htm](feats/archetype-02-4QFElZoWjg1X0vsg.htm)|Archer Dedication|auto-trad|
|[archetype-02-4zaU3GlTGMNqLFS8.htm](feats/archetype-02-4zaU3GlTGMNqLFS8.htm)|Familiar Master Dedication|auto-trad|
|[archetype-02-52nzskpmUt9AjUf8.htm](feats/archetype-02-52nzskpmUt9AjUf8.htm)|Ghost Dedication|auto-trad|
|[archetype-02-57K9il2Jxs8PmsYL.htm](feats/archetype-02-57K9il2Jxs8PmsYL.htm)|Nantambu Chime-Ringer Dedication|auto-trad|
|[archetype-02-5Cm2TYHzoO2DbQNM.htm](feats/archetype-02-5Cm2TYHzoO2DbQNM.htm)|Reanimator Dedication|auto-trad|
|[archetype-02-5CRt5Dy9eLv5LpRF.htm](feats/archetype-02-5CRt5Dy9eLv5LpRF.htm)|Herbalist Dedication|auto-trad|
|[archetype-02-5FOqVC9Q0eEKEf3w.htm](feats/archetype-02-5FOqVC9Q0eEKEf3w.htm)|Magic Warrior Dedication|auto-trad|
|[archetype-02-5npovgLMUlvtot2J.htm](feats/archetype-02-5npovgLMUlvtot2J.htm)|Wrestler Dedication|auto-trad|
|[archetype-02-5Q69PI8jdQVkb1ZT.htm](feats/archetype-02-5Q69PI8jdQVkb1ZT.htm)|Cavalier Dedication|auto-trad|
|[archetype-02-5UQagWB13Z8xR5Z6.htm](feats/archetype-02-5UQagWB13Z8xR5Z6.htm)|Sorcerer Dedication|auto-trad|
|[archetype-02-63L2iSAxkHyq6qEt.htm](feats/archetype-02-63L2iSAxkHyq6qEt.htm)|Hallowed Necromancer Dedication|auto-trad|
|[archetype-02-6b8DiNx4c1zPZ7RP.htm](feats/archetype-02-6b8DiNx4c1zPZ7RP.htm)|Edgewatch Detective Dedication|auto-trad|
|[archetype-02-6PP57Aa5HmSr0qIZ.htm](feats/archetype-02-6PP57Aa5HmSr0qIZ.htm)|Sniping Duo Dedication|auto-trad|
|[archetype-02-8foxmfC6FFT3oYpV.htm](feats/archetype-02-8foxmfC6FFT3oYpV.htm)|Sentinel Dedication|auto-trad|
|[archetype-02-8qawcaQAZMD7pC6Y.htm](feats/archetype-02-8qawcaQAZMD7pC6Y.htm)|Alkenstar Agent Dedication|auto-trad|
|[archetype-02-aFygWxgSv82WyCsl.htm](feats/archetype-02-aFygWxgSv82WyCsl.htm)|Acrobat Dedication|auto-trad|
|[archetype-02-AimSmPyiMhJZVZRq.htm](feats/archetype-02-AimSmPyiMhJZVZRq.htm)|Mauler Dedication|auto-trad|
|[archetype-02-bCWieNDC1CD35tin.htm](feats/archetype-02-bCWieNDC1CD35tin.htm)|Rogue Dedication|auto-trad|
|[archetype-02-BVxcuJ5TWfN1px4L.htm](feats/archetype-02-BVxcuJ5TWfN1px4L.htm)|Pactbound Dedication|auto-trad|
|[archetype-02-BwDIwjHasZwcd61Z.htm](feats/archetype-02-BwDIwjHasZwcd61Z.htm)|Spellshot Dedication|auto-trad|
|[archetype-02-bWYnjNa2RknMoibg.htm](feats/archetype-02-bWYnjNa2RknMoibg.htm)|Lastwall Sentry Dedication|auto-trad|
|[archetype-02-c4ao7b2T92fiM8jR.htm](feats/archetype-02-c4ao7b2T92fiM8jR.htm)|Living Monolith Dedication|auto-trad|
|[archetype-02-CaeCSWFWytWv8Fgu.htm](feats/archetype-02-CaeCSWFWytWv8Fgu.htm)|Scrounger Dedication|auto-trad|
|[archetype-02-CbqaiI0TKsyDywDr.htm](feats/archetype-02-CbqaiI0TKsyDywDr.htm)|Spell Trickster Dedication|auto-trad|
|[archetype-02-CJMkxlxHiHZQYDCz.htm](feats/archetype-02-CJMkxlxHiHZQYDCz.htm)|Alchemist Dedication|auto-trad|
|[archetype-02-CowQy8gXP9POjuxq.htm](feats/archetype-02-CowQy8gXP9POjuxq.htm)|Game Hunter Dedication|auto-trad|
|[archetype-02-CrS0iDwHmjKqvKti.htm](feats/archetype-02-CrS0iDwHmjKqvKti.htm)|Flexible Spellcaster Dedication|auto-trad|
|[archetype-02-CZXhJS55rG5H6PpB.htm](feats/archetype-02-CZXhJS55rG5H6PpB.htm)|Investigator Dedication|auto-trad|
|[archetype-02-dIH771mt4PcVTyAs.htm](feats/archetype-02-dIH771mt4PcVTyAs.htm)|Bard Dedication|auto-trad|
|[archetype-02-dkuY22d3yLUBcqhq.htm](feats/archetype-02-dkuY22d3yLUBcqhq.htm)|Cathartic Mage Dedication|auto-trad|
|[archetype-02-Dm0YMEvSY0qg0jA0.htm](feats/archetype-02-Dm0YMEvSY0qg0jA0.htm)|Gladiator Dedication|auto-trad|
|[archetype-02-E8SDesgMD6Zbye2j.htm](feats/archetype-02-E8SDesgMD6Zbye2j.htm)|Geomancer Dedication|auto-trad|
|[archetype-02-eAlrvPVb8qt8Lruw.htm](feats/archetype-02-eAlrvPVb8qt8Lruw.htm)|Swashbuckler Dedication|auto-trad|
|[archetype-02-eHjqNXgylSuvA887.htm](feats/archetype-02-eHjqNXgylSuvA887.htm)|Student of Perfection Dedication|auto-trad|
|[archetype-02-EHJRMCBRL4YeBN6l.htm](feats/archetype-02-EHJRMCBRL4YeBN6l.htm)|Ghoul Dedication|auto-trad|
|[archetype-02-ePKe13FzKWaQqLo6.htm](feats/archetype-02-ePKe13FzKWaQqLo6.htm)|Basic Psychic Spellcasting|auto-trad|
|[archetype-02-fdSbB0EQVxFgLoEd.htm](feats/archetype-02-fdSbB0EQVxFgLoEd.htm)|Sterling Dynamo Dedication|auto-trad|
|[archetype-02-fL5lO5Odp7iJPkan.htm](feats/archetype-02-fL5lO5Odp7iJPkan.htm)|Vehicle Mechanic Dedication|auto-trad|
|[archetype-02-gQAQRHxpFKEkNQFs.htm](feats/archetype-02-gQAQRHxpFKEkNQFs.htm)|Thaumaturge Dedication|auto-trad|
|[archetype-02-I5hzE0XT2BsjatS3.htm](feats/archetype-02-I5hzE0XT2BsjatS3.htm)|Vampire Dedication|auto-trad|
|[archetype-02-iBmqKjsq4iTtoqvl.htm](feats/archetype-02-iBmqKjsq4iTtoqvl.htm)|Assassin Dedication|auto-trad|
|[archetype-02-IUmN2WC55LxPNSBB.htm](feats/archetype-02-IUmN2WC55LxPNSBB.htm)|Mummy Dedication|auto-trad|
|[archetype-02-j1hhTLOq7o80XCyV.htm](feats/archetype-02-j1hhTLOq7o80XCyV.htm)|Bullet Dancer Dedication|auto-trad|
|[archetype-02-j2XKVhDuhdcI15kD.htm](feats/archetype-02-j2XKVhDuhdcI15kD.htm)|First Frost|auto-trad|
|[archetype-02-JIpjpZ8VneQomkZw.htm](feats/archetype-02-JIpjpZ8VneQomkZw.htm)|Inventor Dedication|auto-trad|
|[archetype-02-jm8kchZRznjxRy0C.htm](feats/archetype-02-jm8kchZRznjxRy0C.htm)|Clockwork Reanimator Dedication|auto-trad|
|[archetype-02-JuEUNrvsxOYdXeYL.htm](feats/archetype-02-JuEUNrvsxOYdXeYL.htm)|Viking Dedication|auto-trad|
|[archetype-02-juikoiIA0Jy8PboY.htm](feats/archetype-02-juikoiIA0Jy8PboY.htm)|Mind Smith Dedication|auto-trad|
|[archetype-02-l1eCHNahsBb7rUkT.htm](feats/archetype-02-l1eCHNahsBb7rUkT.htm)|Pirate Dedication|auto-trad|
|[archetype-02-LJzk4Xxxs9IlbWz6.htm](feats/archetype-02-LJzk4Xxxs9IlbWz6.htm)|Curse Maelstrom Dedication|auto-trad|
|[archetype-02-LoeoiYOpxSaEkWKv.htm](feats/archetype-02-LoeoiYOpxSaEkWKv.htm)|Staff Acrobat Dedication|auto-trad|
|[archetype-02-lotpVdFhxNsNP0Ru.htm](feats/archetype-02-lotpVdFhxNsNP0Ru.htm)|Unexpected Sharpshooter Dedication|auto-trad|
|[archetype-02-lslR2UfP3ze7TFYu.htm](feats/archetype-02-lslR2UfP3ze7TFYu.htm)|Eldritch Researcher Dedication|auto-trad|
|[archetype-02-luKAFJAvdbAgEwV7.htm](feats/archetype-02-luKAFJAvdbAgEwV7.htm)|Runescarred Dedication|auto-trad|
|[archetype-02-lyD6eXl4wc7Pq61q.htm](feats/archetype-02-lyD6eXl4wc7Pq61q.htm)|Loremaster Dedication|auto-trad|
|[archetype-02-M0D2CQgASNfdZBrl.htm](feats/archetype-02-M0D2CQgASNfdZBrl.htm)|Demolitionist Dedication|auto-trad|
|[archetype-02-m3ANSHYfBrFyFUvo.htm](feats/archetype-02-m3ANSHYfBrFyFUvo.htm)|Animal Trainer Dedication|auto-trad|
|[archetype-02-M6cWLywN0aWnX5Gl.htm](feats/archetype-02-M6cWLywN0aWnX5Gl.htm)|Undead Master Dedication|auto-trad|
|[archetype-02-MJg24e9fJd7OASvF.htm](feats/archetype-02-MJg24e9fJd7OASvF.htm)|Medic Dedication|auto-trad|
|[archetype-02-mkp6lhBbTASEmKwY.htm](feats/archetype-02-mkp6lhBbTASEmKwY.htm)|Ghost Hunter Dedication|auto-trad|
|[archetype-02-MNArjo5Z5LUYITQm.htm](feats/archetype-02-MNArjo5Z5LUYITQm.htm)|Celebrity Dedication|auto-trad|
|[archetype-02-MndcBMz6I7e6SRqx.htm](feats/archetype-02-MndcBMz6I7e6SRqx.htm)|Weapon Improviser Dedication|auto-trad|
|[archetype-02-mNsNl6w3l5rXx8dL.htm](feats/archetype-02-mNsNl6w3l5rXx8dL.htm)|Juggler Dedication|auto-trad|
|[archetype-02-mrxAX7h5ya9Z7QV9.htm](feats/archetype-02-mrxAX7h5ya9Z7QV9.htm)|Oatia Skysage Dedication|auto-trad|
|[archetype-02-MVbNnjqQOK9d8Ki3.htm](feats/archetype-02-MVbNnjqQOK9d8Ki3.htm)|Firework Technician Dedication|auto-trad|
|[archetype-02-mvNa9KfQooHYEXoA.htm](feats/archetype-02-mvNa9KfQooHYEXoA.htm)|Fighter Dedication|auto-trad|
|[archetype-02-MYzRfNExDYp25rro.htm](feats/archetype-02-MYzRfNExDYp25rro.htm)|Marshal Dedication|auto-trad|
|[archetype-02-mz2x4HFrWT8usbEL.htm](feats/archetype-02-mz2x4HFrWT8usbEL.htm)|Runelord Dedication|auto-trad|
|[archetype-02-ncrNQcwm4gOQRAA3.htm](feats/archetype-02-ncrNQcwm4gOQRAA3.htm)|Shadowcaster Dedication|auto-trad|
|[archetype-02-NwMiszlcqNZWtezq.htm](feats/archetype-02-NwMiszlcqNZWtezq.htm)|Drow Shootist Dedication|auto-trad|
|[archetype-02-oo34CloLefFRi72w.htm](feats/archetype-02-oo34CloLefFRi72w.htm)|Dragon Disciple Dedication|auto-trad|
|[archetype-02-oQ6EZRFXbHWCx08C.htm](feats/archetype-02-oQ6EZRFXbHWCx08C.htm)|Wizard Dedication|auto-trad|
|[archetype-02-OUNj8nXTHwGcEdlh.htm](feats/archetype-02-OUNj8nXTHwGcEdlh.htm)|Oracle Dedication|auto-trad|
|[archetype-02-OW3W2vMARiojda7e.htm](feats/archetype-02-OW3W2vMARiojda7e.htm)|Psychic Dedication|auto-trad|
|[archetype-02-PFMsj4LtdAIuGn4R.htm](feats/archetype-02-PFMsj4LtdAIuGn4R.htm)|Pistol Phenom Dedication|auto-trad|
|[archetype-02-pI97a5xSg4LbBY1g.htm](feats/archetype-02-pI97a5xSg4LbBY1g.htm)|Aldori Duelist Dedication|auto-trad|
|[archetype-02-pPA2YF6Dal0PClWA.htm](feats/archetype-02-pPA2YF6Dal0PClWA.htm)|Undead Slayer Dedication|auto-trad|
|[archetype-02-PRKe5rWYZMZgEpFU.htm](feats/archetype-02-PRKe5rWYZMZgEpFU.htm)|Archaeologist Dedication|auto-trad|
|[archetype-02-pZMBq7gn66SEEA0n.htm](feats/archetype-02-pZMBq7gn66SEEA0n.htm)|Horizon Walker Dedication|auto-trad|
|[archetype-02-qhs0mWbaLKsdckZX.htm](feats/archetype-02-qhs0mWbaLKsdckZX.htm)|Hellknight Armiger Dedication|auto-trad|
|[archetype-02-qMa2fIP2nqrFzHrq.htm](feats/archetype-02-qMa2fIP2nqrFzHrq.htm)|Scout Dedication|auto-trad|
|[archetype-02-QnB5ArlO4wlRXQ2E.htm](feats/archetype-02-QnB5ArlO4wlRXQ2E.htm)|Pathfinder Agent Dedication|auto-trad|
|[archetype-02-R00qiDE5pBctgtyU.htm](feats/archetype-02-R00qiDE5pBctgtyU.htm)|Ranger Dedication|auto-trad|
|[archetype-02-r1um0cn7MRIMLqcq.htm](feats/archetype-02-r1um0cn7MRIMLqcq.htm)|Ghost Eater Dedication|auto-trad|
|[archetype-02-r72qcTupGzyRDiGe.htm](feats/archetype-02-r72qcTupGzyRDiGe.htm)|Duelist Dedication|auto-trad|
|[archetype-02-renUpSO6MJXPSXow.htm](feats/archetype-02-renUpSO6MJXPSXow.htm)|Monk Dedication|auto-trad|
|[archetype-02-rJ1XxUEACaQA9Lyo.htm](feats/archetype-02-rJ1XxUEACaQA9Lyo.htm)|Bright Lion Dedication|auto-trad|
|[archetype-02-s6wXcQYmHbew14gC.htm](feats/archetype-02-s6wXcQYmHbew14gC.htm)|Bastion Dedication|auto-trad|
|[archetype-02-sG9fPQk4w9y6MmnY.htm](feats/archetype-02-sG9fPQk4w9y6MmnY.htm)|Folklorist Dedication|auto-trad|
|[archetype-02-smCDaPlpRDA47xjK.htm](feats/archetype-02-smCDaPlpRDA47xjK.htm)|Cleric Dedication|auto-trad|
|[archetype-02-SNhhx0hPWlERpQRr.htm](feats/archetype-02-SNhhx0hPWlERpQRr.htm)|Corpse Tender Dedication|auto-trad|
|[archetype-02-SoocjFrWNOpchTVb.htm](feats/archetype-02-SoocjFrWNOpchTVb.htm)|Soulforger Dedication|auto-trad|
|[archetype-02-SwzPqEsLzZpNufvm.htm](feats/archetype-02-SwzPqEsLzZpNufvm.htm)|Summoner Dedication|auto-trad|
|[archetype-02-t03Tc5I4B8RHsyqs.htm](feats/archetype-02-t03Tc5I4B8RHsyqs.htm)|Overwatch Dedication|auto-trad|
|[archetype-02-tKaesXO5nlZ2sspx.htm](feats/archetype-02-tKaesXO5nlZ2sspx.htm)|Bounty Hunter Dedication|auto-trad|
|[archetype-02-tx9pkrpmtqe4FnvS.htm](feats/archetype-02-tx9pkrpmtqe4FnvS.htm)|Elementalist Dedication|auto-trad|
|[archetype-02-TxKWgxor49xntDlU.htm](feats/archetype-02-TxKWgxor49xntDlU.htm)|Magus Dedication|auto-trad|
|[archetype-02-txWKzKuJus7UBebX.htm](feats/archetype-02-txWKzKuJus7UBebX.htm)|Artillerist Dedication|auto-trad|
|[archetype-02-u6SDVWbzHnBBPNMo.htm](feats/archetype-02-u6SDVWbzHnBBPNMo.htm)|Linguist Dedication|auto-trad|
|[archetype-02-uD8J9wAE3KB2w0Cf.htm](feats/archetype-02-uD8J9wAE3KB2w0Cf.htm)|Pactbinder Dedication|auto-trad|
|[archetype-02-uqIiSDbv80TbRwTQ.htm](feats/archetype-02-uqIiSDbv80TbRwTQ.htm)|Vigilante Dedication|auto-trad|
|[archetype-02-USt0jwK8XI5loG4E.htm](feats/archetype-02-USt0jwK8XI5loG4E.htm)|Gunslinger Dedication|auto-trad|
|[archetype-02-UuPZ7drPBnSmI8Eo.htm](feats/archetype-02-UuPZ7drPBnSmI8Eo.htm)|Red Mantis Assassin Dedication|auto-trad|
|[archetype-02-uxHWqFbYD0ZvkeF8.htm](feats/archetype-02-uxHWqFbYD0ZvkeF8.htm)|Scroll Trickster Dedication|auto-trad|
|[archetype-02-UyyrFtPWOo0qvXOv.htm](feats/archetype-02-UyyrFtPWOo0qvXOv.htm)|Zephyr Guard Dedication|auto-trad|
|[archetype-02-vhw3n86TEs6laopA.htm](feats/archetype-02-vhw3n86TEs6laopA.htm)|Druid Dedication|auto-trad|
|[archetype-02-wdmUH6hdat7jpEWt.htm](feats/archetype-02-wdmUH6hdat7jpEWt.htm)|Beastmaster Dedication|auto-trad|
|[archetype-02-WVU0c8rgcpGSRqSi.htm](feats/archetype-02-WVU0c8rgcpGSRqSi.htm)|Barbarian Dedication|auto-trad|
|[archetype-02-y0vdu6DGhKKElmE6.htm](feats/archetype-02-y0vdu6DGhKKElmE6.htm)|Witch Dedication|auto-trad|
|[archetype-02-y7DDs03GtDnmhxFp.htm](feats/archetype-02-y7DDs03GtDnmhxFp.htm)|Poisoner Dedication|auto-trad|
|[archetype-02-ygdbkfPPgSoWxaBa.htm](feats/archetype-02-ygdbkfPPgSoWxaBa.htm)|Chronoskimmer Dedication|auto-trad|
|[archetype-02-YhnCjiHNlS3nCeoC.htm](feats/archetype-02-YhnCjiHNlS3nCeoC.htm)|Dual-Weapon Warrior Dedication|auto-trad|
|[archetype-02-yWawboNWFoJMVl0g.htm](feats/archetype-02-yWawboNWFoJMVl0g.htm)|Soul Warden Dedication|auto-trad|
|[archetype-02-ZhToff996PnTESwb.htm](feats/archetype-02-ZhToff996PnTESwb.htm)|Magaambyan Attendant Dedication|auto-trad|
|[archetype-02-zn7arorE3VJLNYsb.htm](feats/archetype-02-zn7arorE3VJLNYsb.htm)|Martial Artist Dedication|auto-trad|
|[archetype-02-zNxbsYgQgPVxdTLV.htm](feats/archetype-02-zNxbsYgQgPVxdTLV.htm)|Lion Blade Dedication|auto-trad|
|[archetype-02-zV4vzcl7eoVewz5p.htm](feats/archetype-02-zV4vzcl7eoVewz5p.htm)|Ursine Avenger Form|auto-trad|
|[archetype-02-Zyb3RMGyhsKfTjEG.htm](feats/archetype-02-Zyb3RMGyhsKfTjEG.htm)|Wellspring Mage Dedication|auto-trad|
|[archetype-04-0evTnx67DYsxWtg3.htm](feats/archetype-04-0evTnx67DYsxWtg3.htm)|Ice Crafter|auto-trad|
|[archetype-04-0VsrXyQYdluGRfsY.htm](feats/archetype-04-0VsrXyQYdluGRfsY.htm)|Pact of Fey Glamour|auto-trad|
|[archetype-04-16QBDinolgG2M66d.htm](feats/archetype-04-16QBDinolgG2M66d.htm)|Spellstriker|auto-trad|
|[archetype-04-1fBHZpM3Z3MQtzvi.htm](feats/archetype-04-1fBHZpM3Z3MQtzvi.htm)|Doctor's Visitation|auto-trad|
|[archetype-04-1FnZhf5UwSb7Lo3t.htm](feats/archetype-04-1FnZhf5UwSb7Lo3t.htm)|Entity's Strike|auto-trad|
|[archetype-04-1HvSkbUjqrIMXLiY.htm](feats/archetype-04-1HvSkbUjqrIMXLiY.htm)|Hijack Undead|auto-trad|
|[archetype-04-1j9QCzHHxi5MbIY3.htm](feats/archetype-04-1j9QCzHHxi5MbIY3.htm)|Big Game Trapper|auto-trad|
|[archetype-04-1VrV24hhPSEvuKgT.htm](feats/archetype-04-1VrV24hhPSEvuKgT.htm)|Basic Shooting|auto-trad|
|[archetype-04-1wTXeBrYU6BVEEOn.htm](feats/archetype-04-1wTXeBrYU6BVEEOn.htm)|Stage Fighting|auto-trad|
|[archetype-04-2UKf5IiUbpUbOC9a.htm](feats/archetype-04-2UKf5IiUbpUbOC9a.htm)|Draconic Scent|auto-trad|
|[archetype-04-37uOb0iaWCfTCvBZ.htm](feats/archetype-04-37uOb0iaWCfTCvBZ.htm)|Harsh Judgement|auto-trad|
|[archetype-04-3aG0gkHulBIHqqGE.htm](feats/archetype-04-3aG0gkHulBIHqqGE.htm)|Turn Back the Clock|auto-trad|
|[archetype-04-3QLWe5oS9jGJ0Oq4.htm](feats/archetype-04-3QLWe5oS9jGJ0Oq4.htm)|Observant Explorer|auto-trad|
|[archetype-04-4BbfHsGPRHPHfIGY.htm](feats/archetype-04-4BbfHsGPRHPHfIGY.htm)|Poultice Preparation|auto-trad|
|[archetype-04-4HHw2DjTxdv1jBZd.htm](feats/archetype-04-4HHw2DjTxdv1jBZd.htm)|Decry Thief|auto-trad|
|[archetype-04-4Y7wKFogGB0LZ5ZA.htm](feats/archetype-04-4Y7wKFogGB0LZ5ZA.htm)|Steel Yourself!|auto-trad|
|[archetype-04-5KXJkEs39y1gaPEm.htm](feats/archetype-04-5KXJkEs39y1gaPEm.htm)|Stargazer's Eyes|auto-trad|
|[archetype-04-5op3m0gwZjL4udit.htm](feats/archetype-04-5op3m0gwZjL4udit.htm)|Duelist's Challenge|auto-trad|
|[archetype-04-6CE1nVGxt192AUGk.htm](feats/archetype-04-6CE1nVGxt192AUGk.htm)|Scout's Charge|auto-trad|
|[archetype-04-6RjilN93bgy34y3H.htm](feats/archetype-04-6RjilN93bgy34y3H.htm)|Basic Red Mantis Magic|auto-trad|
|[archetype-04-7Hx0i4VchvwrIOV5.htm](feats/archetype-04-7Hx0i4VchvwrIOV5.htm)|Gear Gnash|auto-trad|
|[archetype-04-7jqBwXq9jVsghCva.htm](feats/archetype-04-7jqBwXq9jVsghCva.htm)|Gossip Lore|auto-trad|
|[archetype-04-7nnEtLdUKmDljdhm.htm](feats/archetype-04-7nnEtLdUKmDljdhm.htm)|Wellspring Control|auto-trad|
|[archetype-04-7Pb1WL8abrPBTPrH.htm](feats/archetype-04-7Pb1WL8abrPBTPrH.htm)|Basic Dogma|auto-trad|
|[archetype-04-7XcQ8Ygz5cubGxdC.htm](feats/archetype-04-7XcQ8Ygz5cubGxdC.htm)|Investigator's Stratagem|auto-trad|
|[archetype-04-7ycF0fgSw1ovUPit.htm](feats/archetype-04-7ycF0fgSw1ovUPit.htm)|Basic Oracle Spellcasting|auto-trad|
|[archetype-04-81pD03pQup9sPzXv.htm](feats/archetype-04-81pD03pQup9sPzXv.htm)|Snowcaster|auto-trad|
|[archetype-04-8bDwJieEVCjrceM7.htm](feats/archetype-04-8bDwJieEVCjrceM7.htm)|Nocturnal Kindred|auto-trad|
|[archetype-04-8pmd9gMl3TamFx3u.htm](feats/archetype-04-8pmd9gMl3TamFx3u.htm)|Deft Cooperation|auto-trad|
|[archetype-04-9yko78REsaw7i2gr.htm](feats/archetype-04-9yko78REsaw7i2gr.htm)|Suplex|auto-trad|
|[archetype-04-aLJsBBZzlUK3G8MW.htm](feats/archetype-04-aLJsBBZzlUK3G8MW.htm)|Quick Study|auto-trad|
|[archetype-04-asPQEfYCcsbYxx6K.htm](feats/archetype-04-asPQEfYCcsbYxx6K.htm)|Basic Breakthrough|auto-trad|
|[archetype-04-AuIE19F9rY3YvXf6.htm](feats/archetype-04-AuIE19F9rY3YvXf6.htm)|Magical Scholastics|auto-trad|
|[archetype-04-AvfswILmh3BwNbyR.htm](feats/archetype-04-AvfswILmh3BwNbyR.htm)|Basic Sorcerer Spellcasting|auto-trad|
|[archetype-04-baD02AcIpU7xUBlD.htm](feats/archetype-04-baD02AcIpU7xUBlD.htm)|Basic Maneuver|auto-trad|
|[archetype-04-bCAvo59b5RyW12iI.htm](feats/archetype-04-bCAvo59b5RyW12iI.htm)|Sneak Attacker|auto-trad|
|[archetype-04-bgnf7USFkqsNh8j1.htm](feats/archetype-04-bgnf7USFkqsNh8j1.htm)|Investigate Haunting|auto-trad|
|[archetype-04-bj5QmGyrt7OYORjo.htm](feats/archetype-04-bj5QmGyrt7OYORjo.htm)|Senses of the Bear|auto-trad|
|[archetype-04-bJbWM6zcOeDvCOiZ.htm](feats/archetype-04-bJbWM6zcOeDvCOiZ.htm)|Sacred Spells|auto-trad|
|[archetype-04-Bk07joho2dUG3lVw.htm](feats/archetype-04-Bk07joho2dUG3lVw.htm)|Duelist's Edge|auto-trad|
|[archetype-04-BQkqUrlUVNFp8BEq.htm](feats/archetype-04-BQkqUrlUVNFp8BEq.htm)|Wild Lights|auto-trad|
|[archetype-04-BridkNEysTuSOOLM.htm](feats/archetype-04-BridkNEysTuSOOLM.htm)|Basic Kata|auto-trad|
|[archetype-04-Btu0tA6SEBS6K1hE.htm](feats/archetype-04-Btu0tA6SEBS6K1hE.htm)|Never Tire|auto-trad|
|[archetype-04-BtZJJClWCpc31Ven.htm](feats/archetype-04-BtZJJClWCpc31Ven.htm)|Push Back the Dead!|auto-trad|
|[archetype-04-bvOsJNeI0ewvQsFa.htm](feats/archetype-04-bvOsJNeI0ewvQsFa.htm)|Inspiring Marshal Stance|auto-trad|
|[archetype-04-BxO5l9tH9y1xNzzi.htm](feats/archetype-04-BxO5l9tH9y1xNzzi.htm)|Advanced Thoughtform|auto-trad|
|[archetype-04-CAaXGhHDMRM3Pt4J.htm](feats/archetype-04-CAaXGhHDMRM3Pt4J.htm)|Cavalier's Banner|auto-trad|
|[archetype-04-cB6K0wkiDhduAjtL.htm](feats/archetype-04-cB6K0wkiDhduAjtL.htm)|Just the Tool|auto-trad|
|[archetype-04-chmQgMamyaZX90Tc.htm](feats/archetype-04-chmQgMamyaZX90Tc.htm)|Magical Edification|auto-trad|
|[archetype-04-CX4ISbBwndRWhP55.htm](feats/archetype-04-CX4ISbBwndRWhP55.htm)|Summon Ensemble|auto-trad|
|[archetype-04-CZQgH17ZxoBiVXLa.htm](feats/archetype-04-CZQgH17ZxoBiVXLa.htm)|Ka Stone Ritual|auto-trad|
|[archetype-04-d8RfatiK9UOQANLz.htm](feats/archetype-04-d8RfatiK9UOQANLz.htm)|Lost in the Crowd|auto-trad|
|[archetype-04-Daap4ugeDZQWoPCx.htm](feats/archetype-04-Daap4ugeDZQWoPCx.htm)|Spirit Spells|auto-trad|
|[archetype-04-DBsqWivnSaEo8jz5.htm](feats/archetype-04-DBsqWivnSaEo8jz5.htm)|Basic Arcana|auto-trad|
|[archetype-04-dSSwRyuhKTq1VubX.htm](feats/archetype-04-dSSwRyuhKTq1VubX.htm)|Disarming Block|auto-trad|
|[archetype-04-dtt6xTOSF8PuoStg.htm](feats/archetype-04-dtt6xTOSF8PuoStg.htm)|Tracing Sigil|auto-trad|
|[archetype-04-ELdUj5ihdivlgb3H.htm](feats/archetype-04-ELdUj5ihdivlgb3H.htm)|Crystal Keeper Dedication|auto-trad|
|[archetype-04-ePKe13FzKWaQqLo6.htm](feats/archetype-04-ePKe13FzKWaQqLo6.htm)|Basic Psychic Spellcasting|auto-trad|
|[archetype-04-EsA2F4R3UwUdI8Px.htm](feats/archetype-04-EsA2F4R3UwUdI8Px.htm)|Cycle Spell|auto-trad|
|[archetype-04-f69C05QokaBrDFjn.htm](feats/archetype-04-f69C05QokaBrDFjn.htm)|Shadow Spells|auto-trad|
|[archetype-04-FbzbZc4LGUTcz9tA.htm](feats/archetype-04-FbzbZc4LGUTcz9tA.htm)|Gunpowder Gauntlet|auto-trad|
|[archetype-04-fGBb3VsJwf7osKaL.htm](feats/archetype-04-fGBb3VsJwf7osKaL.htm)|One With the Land|auto-trad|
|[archetype-04-FItD6HmjasjbLdgS.htm](feats/archetype-04-FItD6HmjasjbLdgS.htm)|Spell Runes|auto-trad|
|[archetype-04-FSxugo3zTgRhW7Og.htm](feats/archetype-04-FSxugo3zTgRhW7Og.htm)|Improvised Pummel|auto-trad|
|[archetype-04-fT37dtsByEIc3glC.htm](feats/archetype-04-fT37dtsByEIc3glC.htm)|Scales Of The Dragon|auto-trad|
|[archetype-04-FVDozRTuCQQzD97D.htm](feats/archetype-04-FVDozRTuCQQzD97D.htm)|Eye of Ozem|auto-trad|
|[archetype-04-fVP8jX7yRUpyrcVO.htm](feats/archetype-04-fVP8jX7yRUpyrcVO.htm)|Basic Cathartic Spellcasting|auto-trad|
|[archetype-04-FWQSyjjYg8h0KpHq.htm](feats/archetype-04-FWQSyjjYg8h0KpHq.htm)|Initial Eidolon Ability|auto-trad|
|[archetype-04-G43fiFtxFR24CWRs.htm](feats/archetype-04-G43fiFtxFR24CWRs.htm)|Magic Warrior Aspect|auto-trad|
|[archetype-04-GByIIkeHN3JWkZIZ.htm](feats/archetype-04-GByIIkeHN3JWkZIZ.htm)|Mask Familiar|auto-trad|
|[archetype-04-gc24C5CyWgqn1Lbl.htm](feats/archetype-04-gc24C5CyWgqn1Lbl.htm)|Basic Cleric Spellcasting|auto-trad|
|[archetype-04-geESDWQVvwScyPph.htm](feats/archetype-04-geESDWQVvwScyPph.htm)|Basic Witchcraft|auto-trad|
|[archetype-04-GkPbsX5RZWyI0qFj.htm](feats/archetype-04-GkPbsX5RZWyI0qFj.htm)|Psychopomp Familiar|auto-trad|
|[archetype-04-gMPnLWLlHoNU9Lqv.htm](feats/archetype-04-gMPnLWLlHoNU9Lqv.htm)|Dragon Arcana|auto-trad|
|[archetype-04-gqQK7dDrGPkQDfQQ.htm](feats/archetype-04-gqQK7dDrGPkQDfQQ.htm)|Mortification|auto-trad|
|[archetype-04-GV0lOcVgcetsUlLO.htm](feats/archetype-04-GV0lOcVgcetsUlLO.htm)|Brilliant Crafter|auto-trad|
|[archetype-04-gZrlze9HlRYEQNBG.htm](feats/archetype-04-gZrlze9HlRYEQNBG.htm)|Soul Flare|auto-trad|
|[archetype-04-H2GlPobLyqgBYbd9.htm](feats/archetype-04-H2GlPobLyqgBYbd9.htm)|Hybrid Study Spell|auto-trad|
|[archetype-04-h5ZT9i79BFVJ0VfE.htm](feats/archetype-04-h5ZT9i79BFVJ0VfE.htm)|Basic Concoction|auto-trad|
|[archetype-04-HdhnAm9SNfDqxRSN.htm](feats/archetype-04-HdhnAm9SNfDqxRSN.htm)|First Revelation|auto-trad|
|[archetype-04-hENWnoZNljeJnZBR.htm](feats/archetype-04-hENWnoZNljeJnZBR.htm)|Elbow Breaker|auto-trad|
|[archetype-04-HpT1GlcnkCBnDnVF.htm](feats/archetype-04-HpT1GlcnkCBnDnVF.htm)|Barbarian Resiliency|auto-trad|
|[archetype-04-HVP3ZGIOlxlFy0ni.htm](feats/archetype-04-HVP3ZGIOlxlFy0ni.htm)|Hunter's Sanctum|auto-trad|
|[archetype-04-HvPvyeXM2iMK4OYf.htm](feats/archetype-04-HvPvyeXM2iMK4OYf.htm)|Repeating Hand Crossbow Training|auto-trad|
|[archetype-04-Hwvrive2vBIqZUcE.htm](feats/archetype-04-Hwvrive2vBIqZUcE.htm)|Familiar Mascot|auto-trad|
|[archetype-04-Hzq8FOtaYWpur2BL.htm](feats/archetype-04-Hzq8FOtaYWpur2BL.htm)|Safety Measures|auto-trad|
|[archetype-04-i86JRWsFRpfEJnZP.htm](feats/archetype-04-i86JRWsFRpfEJnZP.htm)|Pirate Weapon Training|auto-trad|
|[archetype-04-iAziUKdxgy4k4ypY.htm](feats/archetype-04-iAziUKdxgy4k4ypY.htm)|Shining Arms|auto-trad|
|[archetype-04-IeRX0tGbeOF0ev08.htm](feats/archetype-04-IeRX0tGbeOF0ev08.htm)|Shorthanded|auto-trad|
|[archetype-04-ifiaewasPbc091BQ.htm](feats/archetype-04-ifiaewasPbc091BQ.htm)|Ankle Biter|auto-trad|
|[archetype-04-iKOcwFCbNX1a2OFT.htm](feats/archetype-04-iKOcwFCbNX1a2OFT.htm)|Ardent Armiger|auto-trad|
|[archetype-04-iMh9Ve8Kt8ZcdKU0.htm](feats/archetype-04-iMh9Ve8Kt8ZcdKU0.htm)|Bullying Staff|auto-trad|
|[archetype-04-io6eJGrw701WbmYe.htm](feats/archetype-04-io6eJGrw701WbmYe.htm)|Surprise Attack|auto-trad|
|[archetype-04-IqTtpxZ48rApy4BN.htm](feats/archetype-04-IqTtpxZ48rApy4BN.htm)|Executioner Weapon Training|auto-trad|
|[archetype-04-irDFSzKCaF4ux3bx.htm](feats/archetype-04-irDFSzKCaF4ux3bx.htm)|Reflexive Catch|auto-trad|
|[archetype-04-IrxtH1BFlUOS0DnQ.htm](feats/archetype-04-IrxtH1BFlUOS0DnQ.htm)|Captivator Dedication|auto-trad|
|[archetype-04-IYt6pMqiTocTrixA.htm](feats/archetype-04-IYt6pMqiTocTrixA.htm)|Familiar Conduit|auto-trad|
|[archetype-04-iyXw5PnevT2jT8kU.htm](feats/archetype-04-iyXw5PnevT2jT8kU.htm)|Sense Alignment|auto-trad|
|[archetype-04-J1MURfqf0kbxrKG9.htm](feats/archetype-04-J1MURfqf0kbxrKG9.htm)|Warding Light|auto-trad|
|[archetype-04-J8BSHRkmP3QLknwF.htm](feats/archetype-04-J8BSHRkmP3QLknwF.htm)|Beast Speaker|auto-trad|
|[archetype-04-j9Rp4fOZIxizyvYy.htm](feats/archetype-04-j9Rp4fOZIxizyvYy.htm)|Viking Weapon Familiarity|auto-trad|
|[archetype-04-Jk6gZzXEABiX5A0S.htm](feats/archetype-04-Jk6gZzXEABiX5A0S.htm)|Loose Cannon|auto-trad|
|[archetype-04-JKvP4pFHzwWsHu2n.htm](feats/archetype-04-JKvP4pFHzwWsHu2n.htm)|Ranger Resiliency|auto-trad|
|[archetype-04-jlLWz8e7PpLFt0Ed.htm](feats/archetype-04-jlLWz8e7PpLFt0Ed.htm)|Healing Touch|auto-trad|
|[archetype-04-JM0umAUx30mAkuTz.htm](feats/archetype-04-JM0umAUx30mAkuTz.htm)|Basic Muse's Whispers|auto-trad|
|[archetype-04-JO3mcFvxjRp1V8XK.htm](feats/archetype-04-JO3mcFvxjRp1V8XK.htm)|Contortionist|auto-trad|
|[archetype-04-jr2LYiMvjnTNhfMM.htm](feats/archetype-04-jr2LYiMvjnTNhfMM.htm)|Slayer's Strike|auto-trad|
|[archetype-04-JStTmD3W8R41WvPg.htm](feats/archetype-04-JStTmD3W8R41WvPg.htm)|Sleepwalker Dedication|auto-trad|
|[archetype-04-jsXDbQAAH3yMchPU.htm](feats/archetype-04-jsXDbQAAH3yMchPU.htm)|Spot Translate|auto-trad|
|[archetype-04-juTBSRs2jzJzLoth.htm](feats/archetype-04-juTBSRs2jzJzLoth.htm)|Fake It Till You Make It|auto-trad|
|[archetype-04-jZbhRBR2yawntSmd.htm](feats/archetype-04-jZbhRBR2yawntSmd.htm)|Agile Hand|auto-trad|
|[archetype-04-JZurhROfi2JfmLfb.htm](feats/archetype-04-JZurhROfi2JfmLfb.htm)|Unnerving Expansion|auto-trad|
|[archetype-04-k5C1WNuYQ4u7nSHt.htm](feats/archetype-04-k5C1WNuYQ4u7nSHt.htm)|Nonlethal Takedown|auto-trad|
|[archetype-04-kmIgbWLbaarqmMaY.htm](feats/archetype-04-kmIgbWLbaarqmMaY.htm)|Basic Captivator Spellcasting|auto-trad|
|[archetype-04-KOf6kmwAZaUJSDW9.htm](feats/archetype-04-KOf6kmwAZaUJSDW9.htm)|Terrain Scout|auto-trad|
|[archetype-04-kPhym38UCLJpjnJD.htm](feats/archetype-04-kPhym38UCLJpjnJD.htm)|Angel of Vindication|auto-trad|
|[archetype-04-KrYvJ5n06yHCipCZ.htm](feats/archetype-04-KrYvJ5n06yHCipCZ.htm)|Play To The Crowd|auto-trad|
|[archetype-04-kU4K9jj9qnktoAaQ.htm](feats/archetype-04-kU4K9jj9qnktoAaQ.htm)|Ritualist Dedication|auto-trad|
|[archetype-04-kUa8PBjKmBk04zmc.htm](feats/archetype-04-kUa8PBjKmBk04zmc.htm)|Basic Wilding|auto-trad|
|[archetype-04-KZJJ0WxcRd4RZKJR.htm](feats/archetype-04-KZJJ0WxcRd4RZKJR.htm)|Elemental Familiar|auto-trad|
|[archetype-04-lkLwEfc3ZhLJSkVr.htm](feats/archetype-04-lkLwEfc3ZhLJSkVr.htm)|Manipulative Charm|auto-trad|
|[archetype-04-LQ5YW01UD9hGKk0l.htm](feats/archetype-04-LQ5YW01UD9hGKk0l.htm)|Corpse Tender's Font|auto-trad|
|[archetype-04-lyJ3pPvZAr9nRco6.htm](feats/archetype-04-lyJ3pPvZAr9nRco6.htm)|Hallowed Initiate|auto-trad|
|[archetype-04-lYVAGuHU47Ixyuxy.htm](feats/archetype-04-lYVAGuHU47Ixyuxy.htm)|Focused Juggler|auto-trad|
|[archetype-04-M9WIUEPY6IoRtgWN.htm](feats/archetype-04-M9WIUEPY6IoRtgWN.htm)|Gravelands Herbalist|auto-trad|
|[archetype-04-MkLujXEplJUt6Arv.htm](feats/archetype-04-MkLujXEplJUt6Arv.htm)|Necromantic Resistance (Undead Slayer)|auto-trad|
|[archetype-04-MnqDDUekqd40HTZc.htm](feats/archetype-04-MnqDDUekqd40HTZc.htm)|Necromantic Resistance|auto-trad|
|[archetype-04-MprbkBZUouqvbKGo.htm](feats/archetype-04-MprbkBZUouqvbKGo.htm)|Folktales Lore|auto-trad|
|[archetype-04-mRHkGMLecd5aaj2R.htm](feats/archetype-04-mRHkGMLecd5aaj2R.htm)|Tools Of The Trade|auto-trad|
|[archetype-04-mV911W6MTGMvHPbE.htm](feats/archetype-04-mV911W6MTGMvHPbE.htm)|Basic Hunter's Trick|auto-trad|
|[archetype-04-N0YcU8mIJnQ4C2N6.htm](feats/archetype-04-N0YcU8mIJnQ4C2N6.htm)|Posse|auto-trad|
|[archetype-04-n5T4ChWJqDUblYfR.htm](feats/archetype-04-n5T4ChWJqDUblYfR.htm)|Bless Shield|auto-trad|
|[archetype-04-nAgAICjPd4BSQlAj.htm](feats/archetype-04-nAgAICjPd4BSQlAj.htm)|Axe Climber|auto-trad|
|[archetype-04-NagTqSLK8bAlo2nY.htm](feats/archetype-04-NagTqSLK8bAlo2nY.htm)|Firebrand Braggart Dedication|auto-trad|
|[archetype-04-nc5G99d20PI9JKCK.htm](feats/archetype-04-nc5G99d20PI9JKCK.htm)|Monk Resiliency|auto-trad|
|[archetype-04-NdgMxlz5I1ddT0Zi.htm](feats/archetype-04-NdgMxlz5I1ddT0Zi.htm)|Basic Mysteries|auto-trad|
|[archetype-04-NlEZ0piDxg9buXCL.htm](feats/archetype-04-NlEZ0piDxg9buXCL.htm)|Quick Alchemy|auto-trad|
|[archetype-04-NQtIhIowH1tVywZI.htm](feats/archetype-04-NQtIhIowH1tVywZI.htm)|Replenishing Consumption|auto-trad|
|[archetype-04-NrCloSukzSUCprsM.htm](feats/archetype-04-NrCloSukzSUCprsM.htm)|Blessed Sacrifice|auto-trad|
|[archetype-04-nroOy0PBeEUGdUXD.htm](feats/archetype-04-nroOy0PBeEUGdUXD.htm)|Dousing Spell|auto-trad|
|[archetype-04-NrZt98r47sPSQ06j.htm](feats/archetype-04-NrZt98r47sPSQ06j.htm)|Crystal Ward Spells|auto-trad|
|[archetype-04-NSyzjkDdQU2A75mX.htm](feats/archetype-04-NSyzjkDdQU2A75mX.htm)|Basic Bard Spellcasting|auto-trad|
|[archetype-04-nT9Z4OPDTOg2AGYc.htm](feats/archetype-04-nT9Z4OPDTOg2AGYc.htm)|Basic Blood Potency|auto-trad|
|[archetype-04-nU5Pow4HMzoDHa8Z.htm](feats/archetype-04-nU5Pow4HMzoDHa8Z.htm)|Basic Witch Spellcasting|auto-trad|
|[archetype-04-nU8W1QoA9hl3h6nR.htm](feats/archetype-04-nU8W1QoA9hl3h6nR.htm)|Watch and Learn|auto-trad|
|[archetype-04-Nxke8WzifQafSa4I.htm](feats/archetype-04-Nxke8WzifQafSa4I.htm)|Impressive Mount|auto-trad|
|[archetype-04-NZgjqVV2HYzLCvvA.htm](feats/archetype-04-NZgjqVV2HYzLCvvA.htm)|Aldori Parry|auto-trad|
|[archetype-04-OJxEF5FONTtEdbpP.htm](feats/archetype-04-OJxEF5FONTtEdbpP.htm)|Heal Animal|auto-trad|
|[archetype-04-OQAo3Us0ODGYdNNn.htm](feats/archetype-04-OQAo3Us0ODGYdNNn.htm)|Expert Backstabber|auto-trad|
|[archetype-04-OqU6QXkMrZqToEEi.htm](feats/archetype-04-OqU6QXkMrZqToEEi.htm)|Opportunist|auto-trad|
|[archetype-04-otLs7XzMXR1cZKGe.htm](feats/archetype-04-otLs7XzMXR1cZKGe.htm)|Quick Juggler|auto-trad|
|[archetype-04-OxvZSFGOjGfcSCv8.htm](feats/archetype-04-OxvZSFGOjGfcSCv8.htm)|Aeon Resonance|auto-trad|
|[archetype-04-p4OllLWJ2rV4sjxe.htm](feats/archetype-04-p4OllLWJ2rV4sjxe.htm)|Semblance of Life|auto-trad|
|[archetype-04-pfBVx5xBfwKd1iVL.htm](feats/archetype-04-pfBVx5xBfwKd1iVL.htm)|Spiritual Explorer|auto-trad|
|[archetype-04-pfF5UzT1MLW3KwHd.htm](feats/archetype-04-pfF5UzT1MLW3KwHd.htm)|Shared Attunement|auto-trad|
|[archetype-04-pKoW1X95LjmWn5Jq.htm](feats/archetype-04-pKoW1X95LjmWn5Jq.htm)|Poisoner's Twist|auto-trad|
|[archetype-04-PLz1oIEGJojVUBsW.htm](feats/archetype-04-PLz1oIEGJojVUBsW.htm)|Fighter Resiliency|auto-trad|
|[archetype-04-PNG7e39mEhq1MorG.htm](feats/archetype-04-PNG7e39mEhq1MorG.htm)|Basic Druid Spellcasting|auto-trad|
|[archetype-04-pwM4RGwCTLiVSic0.htm](feats/archetype-04-pwM4RGwCTLiVSic0.htm)|Ghostly Resistance|auto-trad|
|[archetype-04-Q6EkzXkbMuuk8f7c.htm](feats/archetype-04-Q6EkzXkbMuuk8f7c.htm)|Lion's Might|auto-trad|
|[archetype-04-qAVM65grmny3f8DP.htm](feats/archetype-04-qAVM65grmny3f8DP.htm)|Coughing Dragon Display|auto-trad|
|[archetype-04-QMycbf2StuPcUbzO.htm](feats/archetype-04-QMycbf2StuPcUbzO.htm)|Reach Beyond|auto-trad|
|[archetype-04-qpoE2KhsPbF1ZDsx.htm](feats/archetype-04-qpoE2KhsPbF1ZDsx.htm)|Ravenous Charge|auto-trad|
|[archetype-04-qQh8wnslOagixxD1.htm](feats/archetype-04-qQh8wnslOagixxD1.htm)|Careful Explorer|auto-trad|
|[archetype-04-QrShJGrvmWPBj4oN.htm](feats/archetype-04-QrShJGrvmWPBj4oN.htm)|Arcane School Spell|auto-trad|
|[archetype-04-qV6EuOI3UJYIL6xa.htm](feats/archetype-04-qV6EuOI3UJYIL6xa.htm)|Consolidated Overlay Panopticon|auto-trad|
|[archetype-04-qWb5IxkBUpJWKSLf.htm](feats/archetype-04-qWb5IxkBUpJWKSLf.htm)|Champion Resiliency|auto-trad|
|[archetype-04-R56DLSqoBG8dX4Zv.htm](feats/archetype-04-R56DLSqoBG8dX4Zv.htm)|Scion of Domora Dedication|auto-trad|
|[archetype-04-R7c4PyTNkZb0yvoT.htm](feats/archetype-04-R7c4PyTNkZb0yvoT.htm)|Dread Marshal Stance|auto-trad|
|[archetype-04-RAaUb9MPSDv1CGmF.htm](feats/archetype-04-RAaUb9MPSDv1CGmF.htm)|Perfect Weaponry|auto-trad|
|[archetype-04-RL4GrJ2vTrdJuzW1.htm](feats/archetype-04-RL4GrJ2vTrdJuzW1.htm)|Clinging Climber|auto-trad|
|[archetype-04-RlhvppSmQRqL2RUe.htm](feats/archetype-04-RlhvppSmQRqL2RUe.htm)|Quick Fix|auto-trad|
|[archetype-04-rNPeOwFZE5Ma18JJ.htm](feats/archetype-04-rNPeOwFZE5Ma18JJ.htm)|Social Purview|auto-trad|
|[archetype-04-SASOvOG2Nqs2ekdA.htm](feats/archetype-04-SASOvOG2Nqs2ekdA.htm)|Forceful Push|auto-trad|
|[archetype-04-sE7x3QAel4VGdkgn.htm](feats/archetype-04-sE7x3QAel4VGdkgn.htm)|Basic Flair|auto-trad|
|[archetype-04-shDyS87L0eiabyHw.htm](feats/archetype-04-shDyS87L0eiabyHw.htm)|Grave's Voice|auto-trad|
|[archetype-04-sHS5LQfBHdCsv6vZ.htm](feats/archetype-04-sHS5LQfBHdCsv6vZ.htm)|Basic Thoughtform|auto-trad|
|[archetype-04-SHUfHzElHZPXJFiP.htm](feats/archetype-04-SHUfHzElHZPXJFiP.htm)|Basic Wizard Spellcasting|auto-trad|
|[archetype-04-SjJ8BOy5sc8p2H5E.htm](feats/archetype-04-SjJ8BOy5sc8p2H5E.htm)|Log Roll|auto-trad|
|[archetype-04-Sr6CcCXceV8ALAmB.htm](feats/archetype-04-Sr6CcCXceV8ALAmB.htm)|Basic Fury|auto-trad|
|[archetype-04-SVdYJW5JsOMhAYd0.htm](feats/archetype-04-SVdYJW5JsOMhAYd0.htm)|Fleet Tempo|auto-trad|
|[archetype-04-sVEF7j9Wh1KNGPUm.htm](feats/archetype-04-sVEF7j9Wh1KNGPUm.htm)|Modular Dynamo|auto-trad|
|[archetype-04-SVhyfYNAphQPxFjd.htm](feats/archetype-04-SVhyfYNAphQPxFjd.htm)|Mammoth Lord Dedication|auto-trad|
|[archetype-04-TaoV7ArAuZjxpeQB.htm](feats/archetype-04-TaoV7ArAuZjxpeQB.htm)|Order Spell|auto-trad|
|[archetype-04-TC6zELq2BOqVfgMh.htm](feats/archetype-04-TC6zELq2BOqVfgMh.htm)|Basic Synergy|auto-trad|
|[archetype-04-Tln47zk8F8nswrEI.htm](feats/archetype-04-Tln47zk8F8nswrEI.htm)|Feast|auto-trad|
|[archetype-04-TltRTR1e5KGly64k.htm](feats/archetype-04-TltRTR1e5KGly64k.htm)|Basic Martial Magic|auto-trad|
|[archetype-04-tor4lzY0wpNcJd2U.htm](feats/archetype-04-tor4lzY0wpNcJd2U.htm)|Ghost Blade|auto-trad|
|[archetype-04-TUIQBw9miDowhezw.htm](feats/archetype-04-TUIQBw9miDowhezw.htm)|Exorcist Dedication|auto-trad|
|[archetype-04-txo0xFYUfAjdmyjt.htm](feats/archetype-04-txo0xFYUfAjdmyjt.htm)|Butterfly Blade Dedication|auto-trad|
|[archetype-04-U1I87PGViGpGaP7D.htm](feats/archetype-04-U1I87PGViGpGaP7D.htm)|Attunement Shift|auto-trad|
|[archetype-04-U7dntZ2dAklzsqw8.htm](feats/archetype-04-U7dntZ2dAklzsqw8.htm)|Blessing of the Sun Gods|auto-trad|
|[archetype-04-uCElsebJ45ltmZMT.htm](feats/archetype-04-uCElsebJ45ltmZMT.htm)|Mature Beastmaster Companion|auto-trad|
|[archetype-04-uhU0KajD09h5bw4e.htm](feats/archetype-04-uhU0KajD09h5bw4e.htm)|Acclimatization|auto-trad|
|[archetype-04-uKBT0D9gxdwMcwNl.htm](feats/archetype-04-uKBT0D9gxdwMcwNl.htm)|Basic Devotion|auto-trad|
|[archetype-04-uMjczqcXuteoP7lf.htm](feats/archetype-04-uMjczqcXuteoP7lf.htm)|Claws Of The Dragon|auto-trad|
|[archetype-04-UsEGem9s9ElaTS0d.htm](feats/archetype-04-UsEGem9s9ElaTS0d.htm)|Voice of Authority|auto-trad|
|[archetype-04-uT9RnHLgIIcm7Hhs.htm](feats/archetype-04-uT9RnHLgIIcm7Hhs.htm)|Trapsmith Dedication|auto-trad|
|[archetype-04-UyMQ1X8KLSZvm7AT.htm](feats/archetype-04-UyMQ1X8KLSZvm7AT.htm)|Wayfinder Resonance Tinkerer|auto-trad|
|[archetype-04-v6wYNnkoVDquzRpw.htm](feats/archetype-04-v6wYNnkoVDquzRpw.htm)|Spyglass Modification|auto-trad|
|[archetype-04-Vab3XIirjs3KQh3t.htm](feats/archetype-04-Vab3XIirjs3KQh3t.htm)|Cavalier's Charge|auto-trad|
|[archetype-04-vbHF6HEC9jQorFGl.htm](feats/archetype-04-vbHF6HEC9jQorFGl.htm)|Environmental Explorer|auto-trad|
|[archetype-04-vCsvT7xqBIolF7zH.htm](feats/archetype-04-vCsvT7xqBIolF7zH.htm)|Command Corpse|auto-trad|
|[archetype-04-vKzAIJuyr9SU2JzU.htm](feats/archetype-04-vKzAIJuyr9SU2JzU.htm)|Safe House|auto-trad|
|[archetype-04-VP2CWUTZ9Edg82uz.htm](feats/archetype-04-VP2CWUTZ9Edg82uz.htm)|Swift Leap|auto-trad|
|[archetype-04-vsSbYfsRTHveef24.htm](feats/archetype-04-vsSbYfsRTHveef24.htm)|Duel Spell Advantage|auto-trad|
|[archetype-04-vUQ3XwCT0i3ydX1U.htm](feats/archetype-04-vUQ3XwCT0i3ydX1U.htm)|Always Ready|auto-trad|
|[archetype-04-vVqOBzWTpWirPbrK.htm](feats/archetype-04-vVqOBzWTpWirPbrK.htm)|Seeker of Truths|auto-trad|
|[archetype-04-W7IfFi6MkTDfO2hb.htm](feats/archetype-04-W7IfFi6MkTDfO2hb.htm)|Disengaging Twist|auto-trad|
|[archetype-04-WBp5ybj3kLcsGdVr.htm](feats/archetype-04-WBp5ybj3kLcsGdVr.htm)|Claws Of The Dragon (Draconic Bloodline)|auto-trad|
|[archetype-04-wBqQsXzqObrZM9Va.htm](feats/archetype-04-wBqQsXzqObrZM9Va.htm)|Basic Bloodline Spell|auto-trad|
|[archetype-04-WPHq7MWHlGWpTnme.htm](feats/archetype-04-WPHq7MWHlGWpTnme.htm)|Strange Script|auto-trad|
|[archetype-04-wqrOVv9gnqF4nlLR.htm](feats/archetype-04-wqrOVv9gnqF4nlLR.htm)|Surprise Snare|auto-trad|
|[archetype-04-wscmghwNCXvZtKsz.htm](feats/archetype-04-wscmghwNCXvZtKsz.htm)|Rescuer's Press|auto-trad|
|[archetype-04-X0NFLIn1bqj6bnd0.htm](feats/archetype-04-X0NFLIn1bqj6bnd0.htm)|Basic Deduction|auto-trad|
|[archetype-04-X5gNhaYNx1xu6NoH.htm](feats/archetype-04-X5gNhaYNx1xu6NoH.htm)|Finishing Precision|auto-trad|
|[archetype-04-x62kd1CYJNqO2ZsS.htm](feats/archetype-04-x62kd1CYJNqO2ZsS.htm)|Magic Warrior Transformation|auto-trad|
|[archetype-04-Xb8CyW9sYS27ElcC.htm](feats/archetype-04-Xb8CyW9sYS27ElcC.htm)|Lucky Escape|auto-trad|
|[archetype-04-XgMV11C8W72SX9Yg.htm](feats/archetype-04-XgMV11C8W72SX9Yg.htm)|Patch Job|auto-trad|
|[archetype-04-Xjn4iYzJ6rLIpLV3.htm](feats/archetype-04-Xjn4iYzJ6rLIpLV3.htm)|Barrier Shield|auto-trad|
|[archetype-04-xKKyS9nEQqolJ0SF.htm](feats/archetype-04-xKKyS9nEQqolJ0SF.htm)|Psychic Duelist Dedication|auto-trad|
|[archetype-04-xMTC8UOerwGyCtaN.htm](feats/archetype-04-xMTC8UOerwGyCtaN.htm)|Basic Trickery|auto-trad|
|[archetype-04-xRTlbvvBzURgC6M2.htm](feats/archetype-04-xRTlbvvBzURgC6M2.htm)|Quick Shot|auto-trad|
|[archetype-04-xtRcWpprFBiXeCOB.htm](feats/archetype-04-xtRcWpprFBiXeCOB.htm)|Triangulate|auto-trad|
|[archetype-04-xxvKWQa2olCHoJ03.htm](feats/archetype-04-xxvKWQa2olCHoJ03.htm)|Basic Skysage Divination|auto-trad|
|[archetype-04-Y4gYfrxjSir2Enui.htm](feats/archetype-04-Y4gYfrxjSir2Enui.htm)|Perfect Strike|auto-trad|
|[archetype-04-yantkiDNmsT5ONZX.htm](feats/archetype-04-yantkiDNmsT5ONZX.htm)|Mental Forge|auto-trad|
|[archetype-04-YehcIyxY5KnhPlx5.htm](feats/archetype-04-YehcIyxY5KnhPlx5.htm)|Basic Thaumaturgy|auto-trad|
|[archetype-04-YhclkX1nfyUU8RtO.htm](feats/archetype-04-YhclkX1nfyUU8RtO.htm)|Remember Your Training|auto-trad|
|[archetype-04-yqMBLt0GUTwiqnZT.htm](feats/archetype-04-yqMBLt0GUTwiqnZT.htm)|Cathartic Focus Spell|auto-trad|
|[archetype-04-yUZXOEGqqXKhkGNE.htm](feats/archetype-04-yUZXOEGqqXKhkGNE.htm)|Advanced Reanimated Companion|auto-trad|
|[archetype-04-z3ycxqT1XvjfQ0Oq.htm](feats/archetype-04-z3ycxqT1XvjfQ0Oq.htm)|Take the Wheel|auto-trad|
|[archetype-04-zBLrZE5aCkpaTK2N.htm](feats/archetype-04-zBLrZE5aCkpaTK2N.htm)|Additional Companion|auto-trad|
|[archetype-04-zbnL5OP4zVaNFcq8.htm](feats/archetype-04-zbnL5OP4zVaNFcq8.htm)|Snap Out Of It! (Marshal)|auto-trad|
|[archetype-04-Ze9vb0wlRmWWqnXC.htm](feats/archetype-04-Ze9vb0wlRmWWqnXC.htm)|Jalmeri Heavenseeker Dedication|auto-trad|
|[archetype-04-zfTmb78yGZzNpgU3.htm](feats/archetype-04-zfTmb78yGZzNpgU3.htm)|Dual Thrower|auto-trad|
|[archetype-04-zWEu9xuAxBnPoSrv.htm](feats/archetype-04-zWEu9xuAxBnPoSrv.htm)|Disturbing Defense|auto-trad|
|[archetype-06-00uTUJPgJ6kuGR8O.htm](feats/archetype-06-00uTUJPgJ6kuGR8O.htm)|Crown of the Saumen Kar|auto-trad|
|[archetype-06-03mVGvudDLyGEpTZ.htm](feats/archetype-06-03mVGvudDLyGEpTZ.htm)|Spirit's Absolution|auto-trad|
|[archetype-06-04RgXKFVC2A6Ryn6.htm](feats/archetype-06-04RgXKFVC2A6Ryn6.htm)|Surprise Strike|auto-trad|
|[archetype-06-0Gao1ez4dGH6dIZ2.htm](feats/archetype-06-0Gao1ez4dGH6dIZ2.htm)|Expeditious Advance|auto-trad|
|[archetype-06-0idzh4dww7B7cbnW.htm](feats/archetype-06-0idzh4dww7B7cbnW.htm)|Reloading Trick|auto-trad|
|[archetype-06-0qGLCpggCcOVkbtT.htm](feats/archetype-06-0qGLCpggCcOVkbtT.htm)|Dodge Away|auto-trad|
|[archetype-06-0SbHdwYumvmzwWw3.htm](feats/archetype-06-0SbHdwYumvmzwWw3.htm)|Piston Punch|auto-trad|
|[archetype-06-18UQefmhcNq6tFav.htm](feats/archetype-06-18UQefmhcNq6tFav.htm)|Rapid Manifestation|auto-trad|
|[archetype-06-1YFrl8I6ZGo7BIM9.htm](feats/archetype-06-1YFrl8I6ZGo7BIM9.htm)|Knight Vigilant Dedication|auto-trad|
|[archetype-06-2nSk6oOLBXCEbAhc.htm](feats/archetype-06-2nSk6oOLBXCEbAhc.htm)|Vacate Vision|auto-trad|
|[archetype-06-37CUdnWwJCfOCC2H.htm](feats/archetype-06-37CUdnWwJCfOCC2H.htm)|Thunder Clap|auto-trad|
|[archetype-06-4T9HHOdTk3yVbeoO.htm](feats/archetype-06-4T9HHOdTk3yVbeoO.htm)|Instinct Ability|auto-trad|
|[archetype-06-62hpJOuvYYSa4X7u.htm](feats/archetype-06-62hpJOuvYYSa4X7u.htm)|Hellknight Signifer Dedication|auto-trad|
|[archetype-06-64DgI5CHP7K9kbbg.htm](feats/archetype-06-64DgI5CHP7K9kbbg.htm)|Practiced Opposition|auto-trad|
|[archetype-06-6vpgAMj87J6cWN0j.htm](feats/archetype-06-6vpgAMj87J6cWN0j.htm)|Drenching Mist|auto-trad|
|[archetype-06-74PGpEAVNS5xUDA3.htm](feats/archetype-06-74PGpEAVNS5xUDA3.htm)|Clinch Strike|auto-trad|
|[archetype-06-7fU6e3HIT4NvwLYa.htm](feats/archetype-06-7fU6e3HIT4NvwLYa.htm)|Halcyon Speaker Dedication|auto-trad|
|[archetype-06-7GrACprIxZuarGDs.htm](feats/archetype-06-7GrACprIxZuarGDs.htm)|Grave Strength|auto-trad|
|[archetype-06-7JjNWSuutkjrMrd0.htm](feats/archetype-06-7JjNWSuutkjrMrd0.htm)|Advanced Devotion|auto-trad|
|[archetype-06-7NdMHszAiiveihoW.htm](feats/archetype-06-7NdMHszAiiveihoW.htm)|Armor Specialist|auto-trad|
|[archetype-06-7O0PrMoXd5L8dRfg.htm](feats/archetype-06-7O0PrMoXd5L8dRfg.htm)|Archaeologist's Warning|auto-trad|
|[archetype-06-7RcCvziQBLL7Bumu.htm](feats/archetype-06-7RcCvziQBLL7Bumu.htm)|Advanced Bow Training|auto-trad|
|[archetype-06-7wk6yVM3OJdc4LEU.htm](feats/archetype-06-7wk6yVM3OJdc4LEU.htm)|Champion's Reaction|auto-trad|
|[archetype-06-8rKzUpDxAi8tMk7I.htm](feats/archetype-06-8rKzUpDxAi8tMk7I.htm)|Liberate Soul|auto-trad|
|[archetype-06-8RppI1i4LfI0CYsX.htm](feats/archetype-06-8RppI1i4LfI0CYsX.htm)|Aldori Riposte|auto-trad|
|[archetype-06-8x3wqCZgYzJiSyR1.htm](feats/archetype-06-8x3wqCZgYzJiSyR1.htm)|Dazzling Bullet|auto-trad|
|[archetype-06-9C6a6FXuPqWjXy8K.htm](feats/archetype-06-9C6a6FXuPqWjXy8K.htm)|Improved Familiar (Familiar Master)|auto-trad|
|[archetype-06-9DWxzEavOeymc6Ql.htm](feats/archetype-06-9DWxzEavOeymc6Ql.htm)|Spiritual Strike|auto-trad|
|[archetype-06-9E1FLGp4CNBEwiZE.htm](feats/archetype-06-9E1FLGp4CNBEwiZE.htm)|Relentless Disarm|auto-trad|
|[archetype-06-a9zzu4kb7vstq0HQ.htm](feats/archetype-06-a9zzu4kb7vstq0HQ.htm)|Cadence Call|auto-trad|
|[archetype-06-adr9buwVuxgZV2B3.htm](feats/archetype-06-adr9buwVuxgZV2B3.htm)|Share Burden|auto-trad|
|[archetype-06-aQjWHbjK1pk8HESM.htm](feats/archetype-06-aQjWHbjK1pk8HESM.htm)|Perfect Ki Adept|auto-trad|
|[archetype-06-bi9w4Z9MQY8VWLZF.htm](feats/archetype-06-bi9w4Z9MQY8VWLZF.htm)|Animate Net|auto-trad|
|[archetype-06-bmWvMfYxZbZtigDp.htm](feats/archetype-06-bmWvMfYxZbZtigDp.htm)|Flexible Ritualist|auto-trad|
|[archetype-06-BQzExsEZrwGsJD66.htm](feats/archetype-06-BQzExsEZrwGsJD66.htm)|Endemic Herbs|auto-trad|
|[archetype-06-bTQVvkEuPj9QAiAi.htm](feats/archetype-06-bTQVvkEuPj9QAiAi.htm)|Time Mage Dedication|auto-trad|
|[archetype-06-bVng7Mkrj4UnQzLo.htm](feats/archetype-06-bVng7Mkrj4UnQzLo.htm)|Burning Spell|auto-trad|
|[archetype-06-BxO5l9tH9y1xNzzi.htm](feats/archetype-06-BxO5l9tH9y1xNzzi.htm)|Advanced Thoughtform|auto-trad|
|[archetype-06-bzKBUK6CH8tuLCfo.htm](feats/archetype-06-bzKBUK6CH8tuLCfo.htm)|Advanced Red Mantis Magic|auto-trad|
|[archetype-06-c5SfaSn6OEHkHxII.htm](feats/archetype-06-c5SfaSn6OEHkHxII.htm)|Feverish Enzymes|auto-trad|
|[archetype-06-cBYTVYqw1EFVEuzs.htm](feats/archetype-06-cBYTVYqw1EFVEuzs.htm)|Bolera's Interrogation|auto-trad|
|[archetype-06-Ckglzh4dXcGWPNS3.htm](feats/archetype-06-Ckglzh4dXcGWPNS3.htm)|Beastmaster's Trance|auto-trad|
|[archetype-06-ckukkEJj4Lc3ENjr.htm](feats/archetype-06-ckukkEJj4Lc3ENjr.htm)|Tap Vitality|auto-trad|
|[archetype-06-CmA8t7MRMOzLTeUj.htm](feats/archetype-06-CmA8t7MRMOzLTeUj.htm)|Advanced Breakthrough|auto-trad|
|[archetype-06-cny7ouhsoiNsWJ7X.htm](feats/archetype-06-cny7ouhsoiNsWJ7X.htm)|Daring Act|auto-trad|
|[archetype-06-CoVBrlyMToANvt2v.htm](feats/archetype-06-CoVBrlyMToANvt2v.htm)|Stone Blood|auto-trad|
|[archetype-06-dCCVqIcYrhCp3Bzl.htm](feats/archetype-06-dCCVqIcYrhCp3Bzl.htm)|Sound Mirror|auto-trad|
|[archetype-06-dDFQJem5K9Jzxgda.htm](feats/archetype-06-dDFQJem5K9Jzxgda.htm)|Expert Fireworks Crafter|auto-trad|
|[archetype-06-df4cBV3qZn3qNUmP.htm](feats/archetype-06-df4cBV3qZn3qNUmP.htm)|Rallying Charge|auto-trad|
|[archetype-06-DpmdmRNMg6LZpNB0.htm](feats/archetype-06-DpmdmRNMg6LZpNB0.htm)|Simple Crystal Magic|auto-trad|
|[archetype-06-dPmJ91qawZW2U8K3.htm](feats/archetype-06-dPmJ91qawZW2U8K3.htm)|Staff Sweep|auto-trad|
|[archetype-06-DR6vessIpXTLM6Xa.htm](feats/archetype-06-DR6vessIpXTLM6Xa.htm)|Crowd Mastery|auto-trad|
|[archetype-06-dVzPTpZoGSi5NR6y.htm](feats/archetype-06-dVzPTpZoGSi5NR6y.htm)|Cast Out|auto-trad|
|[archetype-06-dwL0Y0P2x4pn2cft.htm](feats/archetype-06-dwL0Y0P2x4pn2cft.htm)|Bellflower Dedication|auto-trad|
|[archetype-06-ef57tIj30IaPnSgC.htm](feats/archetype-06-ef57tIj30IaPnSgC.htm)|Spiral Sworn|auto-trad|
|[archetype-06-eHkMcQQ4ejRAFJAt.htm](feats/archetype-06-eHkMcQQ4ejRAFJAt.htm)|Poison Coat|auto-trad|
|[archetype-06-eHvJqBHYqx7UjpPg.htm](feats/archetype-06-eHvJqBHYqx7UjpPg.htm)|Perpetual Scout|auto-trad|
|[archetype-06-Ek1CoyGKxsozDsaD.htm](feats/archetype-06-Ek1CoyGKxsozDsaD.htm)|Guide the Timeline|auto-trad|
|[archetype-06-elbj75qsUerbM725.htm](feats/archetype-06-elbj75qsUerbM725.htm)|Scout's Speed|auto-trad|
|[archetype-06-em3glccrJ5ZIf8Uq.htm](feats/archetype-06-em3glccrJ5ZIf8Uq.htm)|Keep Pace (Game Hunter)|auto-trad|
|[archetype-06-eTqWgfojpvuigdvx.htm](feats/archetype-06-eTqWgfojpvuigdvx.htm)|Advanced Muse's Whispers|auto-trad|
|[archetype-06-euWpgjPNcDjeXAWQ.htm](feats/archetype-06-euWpgjPNcDjeXAWQ.htm)|Advanced Maneuver|auto-trad|
|[archetype-06-eXNkcM7gtCGC7udi.htm](feats/archetype-06-eXNkcM7gtCGC7udi.htm)|Grave Sense|auto-trad|
|[archetype-06-f754txt1ZyhVWXHk.htm](feats/archetype-06-f754txt1ZyhVWXHk.htm)|Advanced Hunter's Trick|auto-trad|
|[archetype-06-FGdKV40UiS6jvBmI.htm](feats/archetype-06-FGdKV40UiS6jvBmI.htm)|Exploit Opening|auto-trad|
|[archetype-06-FIVuc2TRaLXiCGkn.htm](feats/archetype-06-FIVuc2TRaLXiCGkn.htm)|Advanced Thaumaturgy|auto-trad|
|[archetype-06-FJdcQU6yjDVDBC4r.htm](feats/archetype-06-FJdcQU6yjDVDBC4r.htm)|Tempest-Sun Redirection|auto-trad|
|[archetype-06-FkOtiB52wIOi7SP7.htm](feats/archetype-06-FkOtiB52wIOi7SP7.htm)|Adaptive Mask Familiar|auto-trad|
|[archetype-06-fngPLUD4Sltho2kn.htm](feats/archetype-06-fngPLUD4Sltho2kn.htm)|Staggering Fire|auto-trad|
|[archetype-06-Fpsd8Y8qJ3Tdbiyz.htm](feats/archetype-06-Fpsd8Y8qJ3Tdbiyz.htm)|Brains!|auto-trad|
|[archetype-06-fSNgVtt8y5uTCYvf.htm](feats/archetype-06-fSNgVtt8y5uTCYvf.htm)|Advanced Martial Magic|auto-trad|
|[archetype-06-g8ZMeg1YFg9WZj3I.htm](feats/archetype-06-g8ZMeg1YFg9WZj3I.htm)|Second Shield|auto-trad|
|[archetype-06-goFxIDlbWd8GN0kj.htm](feats/archetype-06-goFxIDlbWd8GN0kj.htm)|Clear The Way|auto-trad|
|[archetype-06-gOHBzx5Rqa6TZcrm.htm](feats/archetype-06-gOHBzx5Rqa6TZcrm.htm)|Mesmerizing Gaze|auto-trad|
|[archetype-06-GvQ9FQ02i7GYuRRh.htm](feats/archetype-06-GvQ9FQ02i7GYuRRh.htm)|Pact of Draconic Fury|auto-trad|
|[archetype-06-H86VNC6cNWhBI8Ed.htm](feats/archetype-06-H86VNC6cNWhBI8Ed.htm)|Knight Reclaimant Dedication|auto-trad|
|[archetype-06-hCEoINqJ75ConqqA.htm](feats/archetype-06-hCEoINqJ75ConqqA.htm)|Mammoth Charge|auto-trad|
|[archetype-06-HCoDbriaPwnurcNP.htm](feats/archetype-06-HCoDbriaPwnurcNP.htm)|Gear Up|auto-trad|
|[archetype-06-hE6fchGuHuPIeKlO.htm](feats/archetype-06-hE6fchGuHuPIeKlO.htm)|Fleeting Shadow|auto-trad|
|[archetype-06-hiABcPXvcYa9QccF.htm](feats/archetype-06-hiABcPXvcYa9QccF.htm)|Death Warden|auto-trad|
|[archetype-06-HkbP5zbN2meRiP7w.htm](feats/archetype-06-HkbP5zbN2meRiP7w.htm)|Psi Development|auto-trad|
|[archetype-06-HtH8MONAzx4eYuJY.htm](feats/archetype-06-HtH8MONAzx4eYuJY.htm)|Advanced Concoction|auto-trad|
|[archetype-06-i5LtFOpsUR5S74pC.htm](feats/archetype-06-i5LtFOpsUR5S74pC.htm)|Butterfly's Sting|auto-trad|
|[archetype-06-IeAbL6fkRsd1hL6r.htm](feats/archetype-06-IeAbL6fkRsd1hL6r.htm)|Repulse the Wicked|auto-trad|
|[archetype-06-Ig431EeRy3FKMmMq.htm](feats/archetype-06-Ig431EeRy3FKMmMq.htm)|Keen Recollection|auto-trad|
|[archetype-06-J06CgMzMDGOahXjf.htm](feats/archetype-06-J06CgMzMDGOahXjf.htm)|Uneasy Rest|auto-trad|
|[archetype-06-jRJqKkm6NnHcL8HO.htm](feats/archetype-06-jRJqKkm6NnHcL8HO.htm)|Rain-Scribe Sustenance|auto-trad|
|[archetype-06-JxSxCTJOOayIdO4B.htm](feats/archetype-06-JxSxCTJOOayIdO4B.htm)|Slinger's Readiness|auto-trad|
|[archetype-06-K9SpdinLWc7YRVHP.htm](feats/archetype-06-K9SpdinLWc7YRVHP.htm)|Captivating Intensity|auto-trad|
|[archetype-06-KWZqHwI82ae8fMML.htm](feats/archetype-06-KWZqHwI82ae8fMML.htm)|Grave Mummification|auto-trad|
|[archetype-06-L1gD3VMD5X9JNJzE.htm](feats/archetype-06-L1gD3VMD5X9JNJzE.htm)|Beast Gunner Dedication|auto-trad|
|[archetype-06-L1rCuwsCKWd9zlS3.htm](feats/archetype-06-L1rCuwsCKWd9zlS3.htm)|Advanced Deduction|auto-trad|
|[archetype-06-l60Ua5Ugv85GnF9b.htm](feats/archetype-06-l60Ua5Ugv85GnF9b.htm)|Nameless Anonymity|auto-trad|
|[archetype-06-l9K62T7qMCvJXUoY.htm](feats/archetype-06-l9K62T7qMCvJXUoY.htm)|Advanced Wilding|auto-trad|
|[archetype-06-l9PYldtyPr7Q8Xow.htm](feats/archetype-06-l9PYldtyPr7Q8Xow.htm)|Daywalker (Vampire)|auto-trad|
|[archetype-06-LgpATqbuTIfB4o6G.htm](feats/archetype-06-LgpATqbuTIfB4o6G.htm)|No Hard Feelings|auto-trad|
|[archetype-06-lix0Utu4g8mQ0ZtI.htm](feats/archetype-06-lix0Utu4g8mQ0ZtI.htm)|Divine Ally|auto-trad|
|[archetype-06-lknYlp0ekVyBWQK9.htm](feats/archetype-06-lknYlp0ekVyBWQK9.htm)|Discerning Gaze|auto-trad|
|[archetype-06-lqs4MIlO1N1YglOi.htm](feats/archetype-06-lqs4MIlO1N1YglOi.htm)|Advanced Synergy|auto-trad|
|[archetype-06-lt9bQDI7ZXPA7wPw.htm](feats/archetype-06-lt9bQDI7ZXPA7wPw.htm)|Advanced Fury|auto-trad|
|[archetype-06-lZ0swL9EEUgbAuaZ.htm](feats/archetype-06-lZ0swL9EEUgbAuaZ.htm)|Swashbuckler's Riposte|auto-trad|
|[archetype-06-m0ot9Qydb9SYWHis.htm](feats/archetype-06-m0ot9Qydb9SYWHis.htm)|Follow-up Strike|auto-trad|
|[archetype-06-M2V1vqAziOkWV30B.htm](feats/archetype-06-M2V1vqAziOkWV30B.htm)|Crimson Shroud|auto-trad|
|[archetype-06-MM9NcRyXWX2LFiuF.htm](feats/archetype-06-MM9NcRyXWX2LFiuF.htm)|Living Rune|auto-trad|
|[archetype-06-MOAThpfl92zO5p08.htm](feats/archetype-06-MOAThpfl92zO5p08.htm)|Sacred Armaments|auto-trad|
|[archetype-06-moXYfz806x6uXIW9.htm](feats/archetype-06-moXYfz806x6uXIW9.htm)|Field Artillery|auto-trad|
|[archetype-06-mrM07U3MyElcLEx4.htm](feats/archetype-06-mrM07U3MyElcLEx4.htm)|Implement Initiate|auto-trad|
|[archetype-06-mwBb8MlAmpbYH9T4.htm](feats/archetype-06-mwBb8MlAmpbYH9T4.htm)|It's Alive!|auto-trad|
|[archetype-06-n3eOMWQd4kdR3A0l.htm](feats/archetype-06-n3eOMWQd4kdR3A0l.htm)|Mature Trained Companion|auto-trad|
|[archetype-06-NIaNTFlPwi2ng1rZ.htm](feats/archetype-06-NIaNTFlPwi2ng1rZ.htm)|Superior Propulsion|auto-trad|
|[archetype-06-nluD4gFLWePrBK5f.htm](feats/archetype-06-nluD4gFLWePrBK5f.htm)|Explosion|auto-trad|
|[archetype-06-Nm8n3urzpDqXni1i.htm](feats/archetype-06-Nm8n3urzpDqXni1i.htm)|Disrupting Strikes|auto-trad|
|[archetype-06-NQTpuGhB7Rq9zJkt.htm](feats/archetype-06-NQTpuGhB7Rq9zJkt.htm)|Polearm Tricks|auto-trad|
|[archetype-06-nuBY8Ek4JBFQJzoh.htm](feats/archetype-06-nuBY8Ek4JBFQJzoh.htm)|Mastermind's Eye|auto-trad|
|[archetype-06-o3J79hnr00ztcwtT.htm](feats/archetype-06-o3J79hnr00ztcwtT.htm)|Fulminating Shot|auto-trad|
|[archetype-06-of3G33qoA8oJZ0Le.htm](feats/archetype-06-of3G33qoA8oJZ0Le.htm)|Infiltrate Dream|auto-trad|
|[archetype-06-oGOxrqT7DHI43SVk.htm](feats/archetype-06-oGOxrqT7DHI43SVk.htm)|Mounted Shield|auto-trad|
|[archetype-06-oIfCpkpH0Jb1mzj9.htm](feats/archetype-06-oIfCpkpH0Jb1mzj9.htm)|Basic Magus Spellcasting|auto-trad|
|[archetype-06-OjvE7gaQgWiBqOhY.htm](feats/archetype-06-OjvE7gaQgWiBqOhY.htm)|Discerning Strike|auto-trad|
|[archetype-06-oNh2vedij8xbDbph.htm](feats/archetype-06-oNh2vedij8xbDbph.htm)|Hellknight Dedication|auto-trad|
|[archetype-06-opP9j7RP7JPyt8Zj.htm](feats/archetype-06-opP9j7RP7JPyt8Zj.htm)|Mature Megafauna Companion|auto-trad|
|[archetype-06-owJorCBZmUi5lIV0.htm](feats/archetype-06-owJorCBZmUi5lIV0.htm)|Expert Herbalism|auto-trad|
|[archetype-06-oyLkqhDGwGGj40ME.htm](feats/archetype-06-oyLkqhDGwGGj40ME.htm)|Eldritch Archer Dedication|auto-trad|
|[archetype-06-Ozm0xy2lrOq6GiWU.htm](feats/archetype-06-Ozm0xy2lrOq6GiWU.htm)|Ghostly Grasp|auto-trad|
|[archetype-06-OzvvsyjAWWij4mmm.htm](feats/archetype-06-OzvvsyjAWWij4mmm.htm)|Keep Pace (Bounty Hunter)|auto-trad|
|[archetype-06-p6j4nLPRwksjfwPW.htm](feats/archetype-06-p6j4nLPRwksjfwPW.htm)|Snow Step|auto-trad|
|[archetype-06-pewPAMlURmTqBqJx.htm](feats/archetype-06-pewPAMlURmTqBqJx.htm)|Axe Thrower|auto-trad|
|[archetype-06-pIG5hWjZtzZJ3VOZ.htm](feats/archetype-06-pIG5hWjZtzZJ3VOZ.htm)|Advanced Witchcraft|auto-trad|
|[archetype-06-PjsIiVM7yI0XgaFi.htm](feats/archetype-06-PjsIiVM7yI0XgaFi.htm)|Deathly Secrets|auto-trad|
|[archetype-06-pkH4DPmMWcimMov7.htm](feats/archetype-06-pkH4DPmMWcimMov7.htm)|Westyr's Wayfinder Repository|auto-trad|
|[archetype-06-pM0g4ColXTiQ3gTa.htm](feats/archetype-06-pM0g4ColXTiQ3gTa.htm)|Ghost Strike|auto-trad|
|[archetype-06-pph43ZrfvEnQjJXE.htm](feats/archetype-06-pph43ZrfvEnQjJXE.htm)|Cannon Corner Shot|auto-trad|
|[archetype-06-PxTRE0mFEO3tyt8h.htm](feats/archetype-06-PxTRE0mFEO3tyt8h.htm)|Advanced Mysteries|auto-trad|
|[archetype-06-qg8TlLJRgvjzW9YK.htm](feats/archetype-06-qg8TlLJRgvjzW9YK.htm)|Startling Appearance (Vigilante)|auto-trad|
|[archetype-06-qJdbK8vgIqeHU7bu.htm](feats/archetype-06-qJdbK8vgIqeHU7bu.htm)|Heaven's Thunder|auto-trad|
|[archetype-06-RCjMbLyRnG70R7cO.htm](feats/archetype-06-RCjMbLyRnG70R7cO.htm)|Frighten Undead|auto-trad|
|[archetype-06-reoylyGMDPl7H6L5.htm](feats/archetype-06-reoylyGMDPl7H6L5.htm)|Macabre Virtuoso|auto-trad|
|[archetype-06-rFlBoYGI5OmfMvaO.htm](feats/archetype-06-rFlBoYGI5OmfMvaO.htm)|Scholarly Defense|auto-trad|
|[archetype-06-Rh3KSd7BUfV12GBT.htm](feats/archetype-06-Rh3KSd7BUfV12GBT.htm)|Swift Intervention|auto-trad|
|[archetype-06-ROAUR1GhC19Pjk9C.htm](feats/archetype-06-ROAUR1GhC19Pjk9C.htm)|Basic Scroll Cache|auto-trad|
|[archetype-06-rS8uNb0C5GBHnKHH.htm](feats/archetype-06-rS8uNb0C5GBHnKHH.htm)|Viking Weapon Specialist|auto-trad|
|[archetype-06-rXY2fhyteYhaQnMl.htm](feats/archetype-06-rXY2fhyteYhaQnMl.htm)|Advanced Trickery|auto-trad|
|[archetype-06-S25iRw2X9hmGRMyO.htm](feats/archetype-06-S25iRw2X9hmGRMyO.htm)|Advanced Blood Potency|auto-trad|
|[archetype-06-sflJhnFzYfqZ2tDy.htm](feats/archetype-06-sflJhnFzYfqZ2tDy.htm)|Defend Mount|auto-trad|
|[archetype-06-SHhiLn0OSILEXNOj.htm](feats/archetype-06-SHhiLn0OSILEXNOj.htm)|Advanced Flair|auto-trad|
|[archetype-06-SHpVHkPxtQggD9Cf.htm](feats/archetype-06-SHpVHkPxtQggD9Cf.htm)|Swordmaster Dedication|auto-trad|
|[archetype-06-sk5HspGGnLW8b6bX.htm](feats/archetype-06-sk5HspGGnLW8b6bX.htm)|Remote Trigger|auto-trad|
|[archetype-06-sKuhYCfCbXeRWivv.htm](feats/archetype-06-sKuhYCfCbXeRWivv.htm)|Nimble Shield Hand|auto-trad|
|[archetype-06-SOG0yVNtiDsaNbIO.htm](feats/archetype-06-SOG0yVNtiDsaNbIO.htm)|Performative Weapons Training|auto-trad|
|[archetype-06-soHLtpMM9h3AE7PD.htm](feats/archetype-06-soHLtpMM9h3AE7PD.htm)|Expert Alchemy|auto-trad|
|[archetype-06-SUNLm99CgsS5M3Eq.htm](feats/archetype-06-SUNLm99CgsS5M3Eq.htm)|Frightful Condemnation|auto-trad|
|[archetype-06-t5zeg3m9rEnWnYXY.htm](feats/archetype-06-t5zeg3m9rEnWnYXY.htm)|Counter Perform|auto-trad|
|[archetype-06-TCLDccG80M5GeqGw.htm](feats/archetype-06-TCLDccG80M5GeqGw.htm)|Urgent Upwelling|auto-trad|
|[archetype-06-Tg3Iyq55xW9PTSW9.htm](feats/archetype-06-Tg3Iyq55xW9PTSW9.htm)|Advanced Arcana|auto-trad|
|[archetype-06-TjNkvawfWCqb1alg.htm](feats/archetype-06-TjNkvawfWCqb1alg.htm)|Night's Glow|auto-trad|
|[archetype-06-tkMEdh0gM07teWkx.htm](feats/archetype-06-tkMEdh0gM07teWkx.htm)|Advanced Dogma|auto-trad|
|[archetype-06-Tlqqim5TmijoPRRT.htm](feats/archetype-06-Tlqqim5TmijoPRRT.htm)|Soul Arsenal|auto-trad|
|[archetype-06-TszXKspPffCzCD0X.htm](feats/archetype-06-TszXKspPffCzCD0X.htm)|Disciple of Shade|auto-trad|
|[archetype-06-TYP0Ee4o3p9LDodd.htm](feats/archetype-06-TYP0Ee4o3p9LDodd.htm)|Advanced Kata|auto-trad|
|[archetype-06-U3A0kqJ2HKBYiu7X.htm](feats/archetype-06-U3A0kqJ2HKBYiu7X.htm)|Bear Hug|auto-trad|
|[archetype-06-u4idmXH5dd2gU9uA.htm](feats/archetype-06-u4idmXH5dd2gU9uA.htm)|Warding Rune|auto-trad|
|[archetype-06-uc6JcNrI31wzSC2h.htm](feats/archetype-06-uc6JcNrI31wzSC2h.htm)|Bullet Dancer Burn|auto-trad|
|[archetype-06-UEbBOljjXvKsGKFu.htm](feats/archetype-06-UEbBOljjXvKsGKFu.htm)|Disk Rider|auto-trad|
|[archetype-06-uhfZtjbfJ8pZIWrF.htm](feats/archetype-06-uhfZtjbfJ8pZIWrF.htm)|Scrollmaster Dedication|auto-trad|
|[archetype-06-UjEeHamC2C8JfgJz.htm](feats/archetype-06-UjEeHamC2C8JfgJz.htm)|Sky and Heaven Stance|auto-trad|
|[archetype-06-UtUT6JngJbQRHySX.htm](feats/archetype-06-UtUT6JngJbQRHySX.htm)|Boaster's Challenge|auto-trad|
|[archetype-06-V3lFDAQr3PfnAxMC.htm](feats/archetype-06-V3lFDAQr3PfnAxMC.htm)|Confounding Image|auto-trad|
|[archetype-06-V7bwuYADV8huWeF7.htm](feats/archetype-06-V7bwuYADV8huWeF7.htm)|Unnerving Prowess|auto-trad|
|[archetype-06-vKFg7HMNu4cCDD8b.htm](feats/archetype-06-vKFg7HMNu4cCDD8b.htm)|Volatile Grease|auto-trad|
|[archetype-06-vqLt1qjdrflTmdsw.htm](feats/archetype-06-vqLt1qjdrflTmdsw.htm)|Sun's Fury|auto-trad|
|[archetype-06-VruIzuysxw4tY6rk.htm](feats/archetype-06-VruIzuysxw4tY6rk.htm)|Expert Poisoner|auto-trad|
|[archetype-06-vsSbYfsRTHveef24.htm](feats/archetype-06-vsSbYfsRTHveef24.htm)|Duel Spell Advantage|auto-trad|
|[archetype-06-vuApM8xHOZs4o6oS.htm](feats/archetype-06-vuApM8xHOZs4o6oS.htm)|Spellmaster Dedication|auto-trad|
|[archetype-06-vW8dGtOD3rZVOJoq.htm](feats/archetype-06-vW8dGtOD3rZVOJoq.htm)|High-quality Scrounger|auto-trad|
|[archetype-06-wHwjoK3E1Ot9kkV0.htm](feats/archetype-06-wHwjoK3E1Ot9kkV0.htm)|Obscured Terrain|auto-trad|
|[archetype-06-x7EGJYZQuxHbP50X.htm](feats/archetype-06-x7EGJYZQuxHbP50X.htm)|Guided Skill|auto-trad|
|[archetype-06-xaSlCFYUXlu5f0zw.htm](feats/archetype-06-xaSlCFYUXlu5f0zw.htm)|Mind Shards|auto-trad|
|[archetype-06-Xhphe5Lsa4kuU4RG.htm](feats/archetype-06-Xhphe5Lsa4kuU4RG.htm)|Crossbow Terror|auto-trad|
|[archetype-06-XkemuXgSQtxFAhZ8.htm](feats/archetype-06-XkemuXgSQtxFAhZ8.htm)|Advanced Shooting|auto-trad|
|[archetype-06-XLIJXGJ1JdJJZQHG.htm](feats/archetype-06-XLIJXGJ1JdJJZQHG.htm)|Vision of Foresight|auto-trad|
|[archetype-06-xqyN4Nk5mLgUm09l.htm](feats/archetype-06-xqyN4Nk5mLgUm09l.htm)|Targeted Redirection|auto-trad|
|[archetype-06-XusuxqmXWPYc0JYA.htm](feats/archetype-06-XusuxqmXWPYc0JYA.htm)|Narrative Conduit|auto-trad|
|[archetype-06-YOU5eCD5S4cS6Qeu.htm](feats/archetype-06-YOU5eCD5S4cS6Qeu.htm)|Cascade Bearer's Flexibility|auto-trad|
|[archetype-06-YY7wmYQ9ccAO8fut.htm](feats/archetype-06-YY7wmYQ9ccAO8fut.htm)|Basic Summoner Spellcasting|auto-trad|
|[archetype-06-z8bozNJvUjBoKLPA.htm](feats/archetype-06-z8bozNJvUjBoKLPA.htm)|Arcane Sensitivity|auto-trad|
|[archetype-06-ZhhITE2ZMX7UZUge.htm](feats/archetype-06-ZhhITE2ZMX7UZUge.htm)|Predatory Claws|auto-trad|
|[archetype-06-zMT3etCcdPdtAdOn.htm](feats/archetype-06-zMT3etCcdPdtAdOn.htm)|Butterfly's Kiss|auto-trad|
|[archetype-07-i6eWwJ67qBIPJZoK.htm](feats/archetype-07-i6eWwJ67qBIPJZoK.htm)|Explosive Entry|auto-trad|
|[archetype-07-v1PtAazmEFhTp6fZ.htm](feats/archetype-07-v1PtAazmEFhTp6fZ.htm)|Quick Change|auto-trad|
|[archetype-08-0Bu48kn3Deq9gHQE.htm](feats/archetype-08-0Bu48kn3Deq9gHQE.htm)|Practiced Guidance|auto-trad|
|[archetype-08-1AgirzUGkyDdmENy.htm](feats/archetype-08-1AgirzUGkyDdmENy.htm)|Shadowdancer Dedication|auto-trad|
|[archetype-08-1NS6AM2oVNKb4LhX.htm](feats/archetype-08-1NS6AM2oVNKb4LhX.htm)|Flashing Shield|auto-trad|
|[archetype-08-2mww0DmokYJXUEoA.htm](feats/archetype-08-2mww0DmokYJXUEoA.htm)|Ooze Empathy|auto-trad|
|[archetype-08-31ozQ8lwNtiQi2N0.htm](feats/archetype-08-31ozQ8lwNtiQi2N0.htm)|Guardian Ghosts|auto-trad|
|[archetype-08-3XFKXB3ffeIkrQYe.htm](feats/archetype-08-3XFKXB3ffeIkrQYe.htm)|Sense Chaos|auto-trad|
|[archetype-08-3xiWBDSR8miAotpa.htm](feats/archetype-08-3xiWBDSR8miAotpa.htm)|Viking Vindicator|auto-trad|
|[archetype-08-3xkFb2qlAdgLmdSf.htm](feats/archetype-08-3xkFb2qlAdgLmdSf.htm)|Counter Curse|auto-trad|
|[archetype-08-49TMxb8OA1Pp7AiF.htm](feats/archetype-08-49TMxb8OA1Pp7AiF.htm)|Scholar's Hunch|auto-trad|
|[archetype-08-4Bum44iMs6tQz90v.htm](feats/archetype-08-4Bum44iMs6tQz90v.htm)|Waking Dream|auto-trad|
|[archetype-08-4Ek3Kyle2DsCPQQm.htm](feats/archetype-08-4Ek3Kyle2DsCPQQm.htm)|Great Boaster|auto-trad|
|[archetype-08-4HqPkJeSDpqYeGNn.htm](feats/archetype-08-4HqPkJeSDpqYeGNn.htm)|Enhanced Psychopomp Familiar|auto-trad|
|[archetype-08-4jaCuX2JSjTSJ3wp.htm](feats/archetype-08-4jaCuX2JSjTSJ3wp.htm)|Phalanx Formation|auto-trad|
|[archetype-08-4nFsGmPdWvFrwjgF.htm](feats/archetype-08-4nFsGmPdWvFrwjgF.htm)|Advanced Hallowed Spell|auto-trad|
|[archetype-08-4xeDjt8eARzAGARP.htm](feats/archetype-08-4xeDjt8eARzAGARP.htm)|Masked Casting|auto-trad|
|[archetype-08-4YeHOPzo3zOhZQCh.htm](feats/archetype-08-4YeHOPzo3zOhZQCh.htm)|Mantis Form|auto-trad|
|[archetype-08-505CCsBRft1P53gP.htm](feats/archetype-08-505CCsBRft1P53gP.htm)|Lingering Flames|auto-trad|
|[archetype-08-5MG4dBTsFZVbHcX7.htm](feats/archetype-08-5MG4dBTsFZVbHcX7.htm)|Basic Eldritch Archer Spellcasting|auto-trad|
|[archetype-08-5O6G488xu1p8ZHsS.htm](feats/archetype-08-5O6G488xu1p8ZHsS.htm)|Slayer's Blessing|auto-trad|
|[archetype-08-5Pj6pQ7N1qXCQLal.htm](feats/archetype-08-5Pj6pQ7N1qXCQLal.htm)|Accursed Touch|auto-trad|
|[archetype-08-60o2Pf4IunqP6J0Z.htm](feats/archetype-08-60o2Pf4IunqP6J0Z.htm)|Equitable Defense|auto-trad|
|[archetype-08-6qHoUiEudUgUB1Uq.htm](feats/archetype-08-6qHoUiEudUgUB1Uq.htm)|Divine Healing|auto-trad|
|[archetype-08-7p2tNqYHsg6u05cU.htm](feats/archetype-08-7p2tNqYHsg6u05cU.htm)|Shoving Sweep|auto-trad|
|[archetype-08-7vOVPzsVuyE5a3Rp.htm](feats/archetype-08-7vOVPzsVuyE5a3Rp.htm)|Fortified Flesh|auto-trad|
|[archetype-08-8cq6NO087Te3P9yw.htm](feats/archetype-08-8cq6NO087Te3P9yw.htm)|Bonds of Death|auto-trad|
|[archetype-08-8Sdw31YToKhBJ4v4.htm](feats/archetype-08-8Sdw31YToKhBJ4v4.htm)|Physical Training|auto-trad|
|[archetype-08-9EqUTnbV8WHE2aKm.htm](feats/archetype-08-9EqUTnbV8WHE2aKm.htm)|Dualistic Synergy|auto-trad|
|[archetype-08-9LvJo3K2AjKcVvTc.htm](feats/archetype-08-9LvJo3K2AjKcVvTc.htm)|Exude Abyssal Corruption|auto-trad|
|[archetype-08-9LwOCcutlLxd4bfS.htm](feats/archetype-08-9LwOCcutlLxd4bfS.htm)|Reversing Charge|auto-trad|
|[archetype-08-9WMLIcHpwkQpUQfz.htm](feats/archetype-08-9WMLIcHpwkQpUQfz.htm)|Magic Finder|auto-trad|
|[archetype-08-A0keRhzNlcB1u4gD.htm](feats/archetype-08-A0keRhzNlcB1u4gD.htm)|Stasian Smash|auto-trad|
|[archetype-08-A981119DMdqE9Pg1.htm](feats/archetype-08-A981119DMdqE9Pg1.htm)|Running Tackle|auto-trad|
|[archetype-08-ADgFB8hjUgcXS4bF.htm](feats/archetype-08-ADgFB8hjUgcXS4bF.htm)|Mutable Familiar|auto-trad|
|[archetype-08-aEws1NR19Lbu1Kio.htm](feats/archetype-08-aEws1NR19Lbu1Kio.htm)|Incredible Beastmaster Companion|auto-trad|
|[archetype-08-AgMFfp6fdNZ1mAxn.htm](feats/archetype-08-AgMFfp6fdNZ1mAxn.htm)|Order Training|auto-trad|
|[archetype-08-agWNAYqgyV58jlxm.htm](feats/archetype-08-agWNAYqgyV58jlxm.htm)|Foolproof Instructions|auto-trad|
|[archetype-08-aKFYL4b5Bi7qla2j.htm](feats/archetype-08-aKFYL4b5Bi7qla2j.htm)|Peer Beyond|auto-trad|
|[archetype-08-AqVyKiIjISDBFRA0.htm](feats/archetype-08-AqVyKiIjISDBFRA0.htm)|Unkillable|auto-trad|
|[archetype-08-ASWqQ6RB7cfCsUo0.htm](feats/archetype-08-ASWqQ6RB7cfCsUo0.htm)|Protect Ally|auto-trad|
|[archetype-08-Aww98EQXcigRhY3v.htm](feats/archetype-08-Aww98EQXcigRhY3v.htm)|Sickening Bite|auto-trad|
|[archetype-08-AYBfwGImT28lUdue.htm](feats/archetype-08-AYBfwGImT28lUdue.htm)|Incredible Mount|auto-trad|
|[archetype-08-BASCKOPvNGgoHGid.htm](feats/archetype-08-BASCKOPvNGgoHGid.htm)|Achaekek's Grip|auto-trad|
|[archetype-08-BBvmmULFPLlHCeIK.htm](feats/archetype-08-BBvmmULFPLlHCeIK.htm)|Heightened Captivation|auto-trad|
|[archetype-08-BFgrPAK2v3GSKQ5e.htm](feats/archetype-08-BFgrPAK2v3GSKQ5e.htm)|Lore Seeker|auto-trad|
|[archetype-08-bkX8v744C62W8hol.htm](feats/archetype-08-bkX8v744C62W8hol.htm)|Attuned Stride|auto-trad|
|[archetype-08-BVHDkBa4JMmmj5Sn.htm](feats/archetype-08-BVHDkBa4JMmmj5Sn.htm)|Opportunistic Grapple|auto-trad|
|[archetype-08-bXoGskH0SYfdcEtJ.htm](feats/archetype-08-bXoGskH0SYfdcEtJ.htm)|Garden Path|auto-trad|
|[archetype-08-C3zKTQecexSbezhT.htm](feats/archetype-08-C3zKTQecexSbezhT.htm)|Grievous Blow|auto-trad|
|[archetype-08-C4ugUzUuQ4UznNhl.htm](feats/archetype-08-C4ugUzUuQ4UznNhl.htm)|Basic Modification|auto-trad|
|[archetype-08-c9rhGmKft1BVT4JO.htm](feats/archetype-08-c9rhGmKft1BVT4JO.htm)|Skill Mastery (Rogue)|auto-trad|
|[archetype-08-cdeVm8fTDzrP71Sw.htm](feats/archetype-08-cdeVm8fTDzrP71Sw.htm)|Lorefinder|auto-trad|
|[archetype-08-cDNxHLIgvyD7yBfM.htm](feats/archetype-08-cDNxHLIgvyD7yBfM.htm)|Scattered Fire|auto-trad|
|[archetype-08-CIRseixX8dr36ZQK.htm](feats/archetype-08-CIRseixX8dr36ZQK.htm)|Inspirational Performance|auto-trad|
|[archetype-08-cTeRwExCGhfwJbkl.htm](feats/archetype-08-cTeRwExCGhfwJbkl.htm)|Sixth Pillar Dedication|auto-trad|
|[archetype-08-cU5NdcwnMkFQNPjh.htm](feats/archetype-08-cU5NdcwnMkFQNPjh.htm)|To Battle!|auto-trad|
|[archetype-08-d81PT3w97SkyAiXQ.htm](feats/archetype-08-d81PT3w97SkyAiXQ.htm)|Lion's Fury|auto-trad|
|[archetype-08-dCoX0TfsasMfwYnz.htm](feats/archetype-08-dCoX0TfsasMfwYnz.htm)|Larcenous Hand|auto-trad|
|[archetype-08-dHJw5Yv0gY3suBZo.htm](feats/archetype-08-dHJw5Yv0gY3suBZo.htm)|Reminder of the Greater Fear|auto-trad|
|[archetype-08-dmXd68ilbuGR6eUP.htm](feats/archetype-08-dmXd68ilbuGR6eUP.htm)|Archaeologist's Luck|auto-trad|
|[archetype-08-dtPXuS8MiWrz5UNK.htm](feats/archetype-08-dtPXuS8MiWrz5UNK.htm)|Hot Foot|auto-trad|
|[archetype-08-DwU6CmK4KsH8A3hu.htm](feats/archetype-08-DwU6CmK4KsH8A3hu.htm)|Arcane Breadth|auto-trad|
|[archetype-08-E3NsAdbp7kkOvfnr.htm](feats/archetype-08-E3NsAdbp7kkOvfnr.htm)|Quick Positioning|auto-trad|
|[archetype-08-E7eceezD3NDmBVBb.htm](feats/archetype-08-E7eceezD3NDmBVBb.htm)|Back To Back|auto-trad|
|[archetype-08-EG27noJj9KzyB2i4.htm](feats/archetype-08-EG27noJj9KzyB2i4.htm)|Out of Hand|auto-trad|
|[archetype-08-eGgSGU1LOaRNRYR7.htm](feats/archetype-08-eGgSGU1LOaRNRYR7.htm)|Shadow Spell|auto-trad|
|[archetype-08-egmb8p3ZIYtx5aQN.htm](feats/archetype-08-egmb8p3ZIYtx5aQN.htm)|Archer's Aim|auto-trad|
|[archetype-08-EGtuOZ3E9y0qZ1oJ.htm](feats/archetype-08-EGtuOZ3E9y0qZ1oJ.htm)|Familiar Form|auto-trad|
|[archetype-08-EoKgJXfNfHwsy2sk.htm](feats/archetype-08-EoKgJXfNfHwsy2sk.htm)|Superimpose Time Duplicates|auto-trad|
|[archetype-08-fhwO3N3VoZ0ZpfjU.htm](feats/archetype-08-fhwO3N3VoZ0ZpfjU.htm)|Accursed Magic|auto-trad|
|[archetype-08-fHWYICk6cSePr30c.htm](feats/archetype-08-fHWYICk6cSePr30c.htm)|Surreptitious Spellcaster|auto-trad|
|[archetype-08-fJxIdcg7kWPwlULY.htm](feats/archetype-08-fJxIdcg7kWPwlULY.htm)|Guardian's Embrace|auto-trad|
|[archetype-08-FNW66GfWXaiBSexb.htm](feats/archetype-08-FNW66GfWXaiBSexb.htm)|Impervious Vehicle|auto-trad|
|[archetype-08-gepQGtV8Ftr0JJ6O.htm](feats/archetype-08-gepQGtV8Ftr0JJ6O.htm)|Skyseeker|auto-trad|
|[archetype-08-gHcVxdMksOaGaiCx.htm](feats/archetype-08-gHcVxdMksOaGaiCx.htm)|Chronomancer's Secrets|auto-trad|
|[archetype-08-gTg3D9Dv9L4NEVhV.htm](feats/archetype-08-gTg3D9Dv9L4NEVhV.htm)|Gardener's Resolve|auto-trad|
|[archetype-08-gtHTr77BkK4CckEH.htm](feats/archetype-08-gtHTr77BkK4CckEH.htm)|Basic Beast Gunner Spellcasting|auto-trad|
|[archetype-08-HIyuVIh2XSDz3h2j.htm](feats/archetype-08-HIyuVIh2XSDz3h2j.htm)|Skim Scroll|auto-trad|
|[archetype-08-hLMsARkiwRf4SwqZ.htm](feats/archetype-08-hLMsARkiwRf4SwqZ.htm)|Improved Command Corpse|auto-trad|
|[archetype-08-HlSwpxreIfsglTJ8.htm](feats/archetype-08-HlSwpxreIfsglTJ8.htm)|Lethargy Poisoner|auto-trad|
|[archetype-08-i7hNUqiJsB8hgIET.htm](feats/archetype-08-i7hNUqiJsB8hgIET.htm)|Quick Stow (Swordmaster)|auto-trad|
|[archetype-08-ivSf8wSIUkK8gwej.htm](feats/archetype-08-ivSf8wSIUkK8gwej.htm)|Live Ammunition|auto-trad|
|[archetype-08-jaekke9HomT4PZ9b.htm](feats/archetype-08-jaekke9HomT4PZ9b.htm)|Reflexive Grip|auto-trad|
|[archetype-08-jfsmtOx4QgXB19BL.htm](feats/archetype-08-jfsmtOx4QgXB19BL.htm)|Tumbling Strike|auto-trad|
|[archetype-08-jIMeialR9CBo1bx9.htm](feats/archetype-08-jIMeialR9CBo1bx9.htm)|Levering Strike|auto-trad|
|[archetype-08-jTOxxy19o2VRJTbH.htm](feats/archetype-08-jTOxxy19o2VRJTbH.htm)|Golden League Xun Dedication|auto-trad|
|[archetype-08-jwcNyPDVw313KXZU.htm](feats/archetype-08-jwcNyPDVw313KXZU.htm)|Armored Rebuff|auto-trad|
|[archetype-08-jYKKnr41OqQrf7hv.htm](feats/archetype-08-jYKKnr41OqQrf7hv.htm)|Efficient Rituals|auto-trad|
|[archetype-08-jZalt2bFGjK8XXcP.htm](feats/archetype-08-jZalt2bFGjK8XXcP.htm)|Many Guises|auto-trad|
|[archetype-08-kQfqJLpKOPORq7I6.htm](feats/archetype-08-kQfqJLpKOPORq7I6.htm)|Work Yourself Up|auto-trad|
|[archetype-08-kYYB7ziQZjlgQWWu.htm](feats/archetype-08-kYYB7ziQZjlgQWWu.htm)|Call Ursine Ally|auto-trad|
|[archetype-08-L9aBLPG2veBkVow9.htm](feats/archetype-08-L9aBLPG2veBkVow9.htm)|Persistent Creation|auto-trad|
|[archetype-08-lH8FwUM9bX9rpVsa.htm](feats/archetype-08-lH8FwUM9bX9rpVsa.htm)|Dream Magic|auto-trad|
|[archetype-08-lMA3F9SGzGV79P5C.htm](feats/archetype-08-lMA3F9SGzGV79P5C.htm)|Strangle|auto-trad|
|[archetype-08-lUjWE40fjAjHecNQ.htm](feats/archetype-08-lUjWE40fjAjHecNQ.htm)|Incredible Megafauna Companion|auto-trad|
|[archetype-08-lvxzgLNzmScMY0uC.htm](feats/archetype-08-lvxzgLNzmScMY0uC.htm)|Stalwart Mind|auto-trad|
|[archetype-08-m8iP2OCzit9WUrMD.htm](feats/archetype-08-m8iP2OCzit9WUrMD.htm)|Enchanting Arrow|auto-trad|
|[archetype-08-MjPqgBQU9W4kelfz.htm](feats/archetype-08-MjPqgBQU9W4kelfz.htm)|Interrupt Charge|auto-trad|
|[archetype-08-MlxfNv98LZKfYl64.htm](feats/archetype-08-MlxfNv98LZKfYl64.htm)|Precious Arrow|auto-trad|
|[archetype-08-MSqgBffcAXTg700A.htm](feats/archetype-08-MSqgBffcAXTg700A.htm)|Preventative Treatment|auto-trad|
|[archetype-08-mUL0C7O4HSnPSXea.htm](feats/archetype-08-mUL0C7O4HSnPSXea.htm)|Warped Constriction|auto-trad|
|[archetype-08-MxNb97qr1yMhbjiP.htm](feats/archetype-08-MxNb97qr1yMhbjiP.htm)|Golem Grafter Dedication|auto-trad|
|[archetype-08-n7LTwKUSATLaQ9FD.htm](feats/archetype-08-n7LTwKUSATLaQ9FD.htm)|Survivor of Desolation|auto-trad|
|[archetype-08-nAVLB5MLYWUu8N71.htm](feats/archetype-08-nAVLB5MLYWUu8N71.htm)|Storm Shroud|auto-trad|
|[archetype-08-niKCBA11zzgGT1PU.htm](feats/archetype-08-niKCBA11zzgGT1PU.htm)|Primal Breadth|auto-trad|
|[archetype-08-nx2nB10rypAuspAa.htm](feats/archetype-08-nx2nB10rypAuspAa.htm)|Acquired Tolerance|auto-trad|
|[archetype-08-o98nJriWE95xgweg.htm](feats/archetype-08-o98nJriWE95xgweg.htm)|Wayfinder Resonance Infiltrator|auto-trad|
|[archetype-08-OeUP3ASaS5deBsWw.htm](feats/archetype-08-OeUP3ASaS5deBsWw.htm)|Accurate Swing|auto-trad|
|[archetype-08-Olx796SgBbHUFeHc.htm](feats/archetype-08-Olx796SgBbHUFeHc.htm)|Armiger's Mobility|auto-trad|
|[archetype-08-oSi9mUtYWYekxX0B.htm](feats/archetype-08-oSi9mUtYWYekxX0B.htm)|Able Ritualist|auto-trad|
|[archetype-08-ossGwqEvC6ifPwgT.htm](feats/archetype-08-ossGwqEvC6ifPwgT.htm)|Swap Reflections|auto-trad|
|[archetype-08-OtM4pvPeIeEEdpuS.htm](feats/archetype-08-OtM4pvPeIeEEdpuS.htm)|Rule of Three|auto-trad|
|[archetype-08-P5Zu46vfO4ptBBoj.htm](feats/archetype-08-P5Zu46vfO4ptBBoj.htm)|Mind Projectiles|auto-trad|
|[archetype-08-PaUpesy5lyDLlwud.htm](feats/archetype-08-PaUpesy5lyDLlwud.htm)|Transcribe Moment|auto-trad|
|[archetype-08-PccekOihIbRWdDky.htm](feats/archetype-08-PccekOihIbRWdDky.htm)|Malleable Mental Forge|auto-trad|
|[archetype-08-PcDvw2vWTzBEIX7k.htm](feats/archetype-08-PcDvw2vWTzBEIX7k.htm)|Necromantic Tenacity|auto-trad|
|[archetype-08-PTXZ2C3AV8tZf0iX.htm](feats/archetype-08-PTXZ2C3AV8tZf0iX.htm)|Deeper Dabbler|auto-trad|
|[archetype-08-pu8Mg2FIrhBYTNnB.htm](feats/archetype-08-pu8Mg2FIrhBYTNnB.htm)|Duo's Aim|auto-trad|
|[archetype-08-QN55ToyJIjxgPmhs.htm](feats/archetype-08-QN55ToyJIjxgPmhs.htm)|Perfect Resistance|auto-trad|
|[archetype-08-QQCxSQaX8UEZHfUz.htm](feats/archetype-08-QQCxSQaX8UEZHfUz.htm)|Unbelievable Luck|auto-trad|
|[archetype-08-qUBr1YsQw3BSNy9c.htm](feats/archetype-08-qUBr1YsQw3BSNy9c.htm)|Glyph Expert|auto-trad|
|[archetype-08-qXWcmyHLkMlzRffC.htm](feats/archetype-08-qXWcmyHLkMlzRffC.htm)|Vantage Shot|auto-trad|
|[archetype-08-QYZovCDjOhBb1u01.htm](feats/archetype-08-QYZovCDjOhBb1u01.htm)|Innate Magical Intuition|auto-trad|
|[archetype-08-R337W7VveJxlEddE.htm](feats/archetype-08-R337W7VveJxlEddE.htm)|Necromantic Bulwark|auto-trad|
|[archetype-08-r7srDh7Iz94vfwwN.htm](feats/archetype-08-r7srDh7Iz94vfwwN.htm)|Countercharm|auto-trad|
|[archetype-08-RHEdvEFz3QKiRKlr.htm](feats/archetype-08-RHEdvEFz3QKiRKlr.htm)|Topple Giants|auto-trad|
|[archetype-08-RivbJYEBUyfLwPh7.htm](feats/archetype-08-RivbJYEBUyfLwPh7.htm)|Scarecrow|auto-trad|
|[archetype-08-Rqj1kmrnF9M1E6pv.htm](feats/archetype-08-Rqj1kmrnF9M1E6pv.htm)|Supreme Psychic Center|auto-trad|
|[archetype-08-RU86cGTryRAdaEqx.htm](feats/archetype-08-RU86cGTryRAdaEqx.htm)|Swashbuckler's Speed|auto-trad|
|[archetype-08-sbYDDDUWYN6Qx71k.htm](feats/archetype-08-sbYDDDUWYN6Qx71k.htm)|Crude Communication|auto-trad|
|[archetype-08-sCkzwTBLyE8FdzWI.htm](feats/archetype-08-sCkzwTBLyE8FdzWI.htm)|Smoldering Explosion|auto-trad|
|[archetype-08-SE38R6zpv2XelzZk.htm](feats/archetype-08-SE38R6zpv2XelzZk.htm)|Call Gun|auto-trad|
|[archetype-08-SfBXPmADMFiZIBQt.htm](feats/archetype-08-SfBXPmADMFiZIBQt.htm)|Call Your Shot|auto-trad|
|[archetype-08-Sr75bQtqmCM6dyAM.htm](feats/archetype-08-Sr75bQtqmCM6dyAM.htm)|Breath Of The Dragon|auto-trad|
|[archetype-08-T6Twdesb5niBhHuZ.htm](feats/archetype-08-T6Twdesb5niBhHuZ.htm)|Jumping Jenny Display|auto-trad|
|[archetype-08-tBg3FZST3nX5TfLf.htm](feats/archetype-08-tBg3FZST3nX5TfLf.htm)|Safeguard Soul|auto-trad|
|[archetype-08-TDaf3DbWymxPmHrO.htm](feats/archetype-08-TDaf3DbWymxPmHrO.htm)|What Could Have Been|auto-trad|
|[archetype-08-TGFbFyv0AUi5gAGO.htm](feats/archetype-08-TGFbFyv0AUi5gAGO.htm)|Monk Moves|auto-trad|
|[archetype-08-tH5w8vIfFCFNVP7T.htm](feats/archetype-08-tH5w8vIfFCFNVP7T.htm)|Invoke the Crimson Oath|auto-trad|
|[archetype-08-TzBP8yiZQHNhei1V.htm](feats/archetype-08-TzBP8yiZQHNhei1V.htm)|Walk The Plank|auto-trad|
|[archetype-08-u8hKEb71pI45XP1f.htm](feats/archetype-08-u8hKEb71pI45XP1f.htm)|Minor Omen|auto-trad|
|[archetype-08-UcIyf7bTDf6RwydU.htm](feats/archetype-08-UcIyf7bTDf6RwydU.htm)|Makeshift Strike|auto-trad|
|[archetype-08-UfuYHdozZD586RWd.htm](feats/archetype-08-UfuYHdozZD586RWd.htm)|Bloodline Breadth|auto-trad|
|[archetype-08-URtcR1BU2OgfKHfm.htm](feats/archetype-08-URtcR1BU2OgfKHfm.htm)|Metabolize Element|auto-trad|
|[archetype-08-V3nNlrdU2OxYJAjx.htm](feats/archetype-08-V3nNlrdU2OxYJAjx.htm)|Blessed Spell|auto-trad|
|[archetype-08-v4O6eDiSOkzQZHmT.htm](feats/archetype-08-v4O6eDiSOkzQZHmT.htm)|Skill Mastery (Investigator)|auto-trad|
|[archetype-08-VqzXfdbQQ2e8Hndr.htm](feats/archetype-08-VqzXfdbQQ2e8Hndr.htm)|Unshakable Idealism|auto-trad|
|[archetype-08-vUFvcrvszXlHvz2Y.htm](feats/archetype-08-vUFvcrvszXlHvz2Y.htm)|Magic Arrow|auto-trad|
|[archetype-08-vVyX9IG8flxg81mc.htm](feats/archetype-08-vVyX9IG8flxg81mc.htm)|Elude the Divine|auto-trad|
|[archetype-08-wIAiDaNztd52ltT2.htm](feats/archetype-08-wIAiDaNztd52ltT2.htm)|Positive Luminance|auto-trad|
|[archetype-08-WmSggxmf4iAe2aRD.htm](feats/archetype-08-WmSggxmf4iAe2aRD.htm)|Recycled Cogwheels|auto-trad|
|[archetype-08-Wz0LLKjEi8GfKloV.htm](feats/archetype-08-Wz0LLKjEi8GfKloV.htm)|Patron's Breadth|auto-trad|
|[archetype-08-X6nwiA2uJ2UQIOKB.htm](feats/archetype-08-X6nwiA2uJ2UQIOKB.htm)|Drive Back|auto-trad|
|[archetype-08-X71qmyGpKN1XAoT6.htm](feats/archetype-08-X71qmyGpKN1XAoT6.htm)|Shamble|auto-trad|
|[archetype-08-xhFsHEsMTAvzwJhr.htm](feats/archetype-08-xhFsHEsMTAvzwJhr.htm)|Incredible Reanimated Companion|auto-trad|
|[archetype-08-XkmrLhyAoxQTLnza.htm](feats/archetype-08-XkmrLhyAoxQTLnza.htm)|Lobbed Attack|auto-trad|
|[archetype-08-XoxawV3Fmn61VJcS.htm](feats/archetype-08-XoxawV3Fmn61VJcS.htm)|Steal Vitality|auto-trad|
|[archetype-08-XRIWWrXfghsQce4S.htm](feats/archetype-08-XRIWWrXfghsQce4S.htm)|Divine Breadth|auto-trad|
|[archetype-08-XY2uS7pMKRLVNQKG.htm](feats/archetype-08-XY2uS7pMKRLVNQKG.htm)|Fey's Trickery|auto-trad|
|[archetype-08-YDpSnLmnSLsItP45.htm](feats/archetype-08-YDpSnLmnSLsItP45.htm)|Improvised Critical|auto-trad|
|[archetype-08-yg1ZTjHuSiQJFO0i.htm](feats/archetype-08-yg1ZTjHuSiQJFO0i.htm)|Future Spell Learning|auto-trad|
|[archetype-08-yIDJTe4kimRoZN0L.htm](feats/archetype-08-yIDJTe4kimRoZN0L.htm)|Controlled Blast|auto-trad|
|[archetype-08-Yj0IKXM49Ver5Smc.htm](feats/archetype-08-Yj0IKXM49Ver5Smc.htm)|Siphoning Touch|auto-trad|
|[archetype-08-YMrcnO2l3mA71ohM.htm](feats/archetype-08-YMrcnO2l3mA71ohM.htm)|Frozen Breadth|auto-trad|
|[archetype-08-yOybxBkebeeuHWuy.htm](feats/archetype-08-yOybxBkebeeuHWuy.htm)|Magical Knowledge|auto-trad|
|[archetype-08-YrRlbIzjsFlGGmVN.htm](feats/archetype-08-YrRlbIzjsFlGGmVN.htm)|Black Powder Blaze|auto-trad|
|[archetype-08-YTZhLWtrEnV9Pjf2.htm](feats/archetype-08-YTZhLWtrEnV9Pjf2.htm)|Bravo's Determination|auto-trad|
|[archetype-08-Yvxr1Q2TslWiKqqi.htm](feats/archetype-08-Yvxr1Q2TslWiKqqi.htm)|Occult Breadth|auto-trad|
|[archetype-08-Yy9LXo2GaLT7eCOL.htm](feats/archetype-08-Yy9LXo2GaLT7eCOL.htm)|Deadly Butterfly|auto-trad|
|[archetype-08-YYPjndZ7tqRQLtAH.htm](feats/archetype-08-YYPjndZ7tqRQLtAH.htm)|Beacon Mark|auto-trad|
|[archetype-08-zAJY8rLvZjzOK0Mt.htm](feats/archetype-08-zAJY8rLvZjzOK0Mt.htm)|Sin Reservoir|auto-trad|
|[archetype-08-zJXsGF61lH0WHw5v.htm](feats/archetype-08-zJXsGF61lH0WHw5v.htm)|Ghost Flight|auto-trad|
|[archetype-08-ztxbGySxIEeWvsAT.htm](feats/archetype-08-ztxbGySxIEeWvsAT.htm)|Pact of Infernal Prowess|auto-trad|
|[archetype-08-ZXaDS4OJvsQYvhBZ.htm](feats/archetype-08-ZXaDS4OJvsQYvhBZ.htm)|Submission Hold|auto-trad|
|[archetype-08-ZyQYP7i26DWhMNux.htm](feats/archetype-08-ZyQYP7i26DWhMNux.htm)|Unfazed Assessment|auto-trad|
|[archetype-08-zytTsipimVTmPc5U.htm](feats/archetype-08-zytTsipimVTmPc5U.htm)|Selfless Parry|auto-trad|
|[archetype-10-04Smj4ZsEBD8WIXv.htm](feats/archetype-10-04Smj4ZsEBD8WIXv.htm)|Dual-Weapon Blitz|auto-trad|
|[archetype-10-1F4FD5OdDCEyiEvk.htm](feats/archetype-10-1F4FD5OdDCEyiEvk.htm)|Deny the Songs of War|auto-trad|
|[archetype-10-23QgyEYjoslBvkra.htm](feats/archetype-10-23QgyEYjoslBvkra.htm)|Invigorating Breath|auto-trad|
|[archetype-10-2uQbQgz1AbjzcFSp.htm](feats/archetype-10-2uQbQgz1AbjzcFSp.htm)|Runic Mind Smithing|auto-trad|
|[archetype-10-2xxFg9yRuCDpME3z.htm](feats/archetype-10-2xxFg9yRuCDpME3z.htm)|Spy's Countermeasures|auto-trad|
|[archetype-10-39RJF47FLYr5gZ8p.htm](feats/archetype-10-39RJF47FLYr5gZ8p.htm)|Unseat|auto-trad|
|[archetype-10-3R09Hl6IDMgPcSs0.htm](feats/archetype-10-3R09Hl6IDMgPcSs0.htm)|Beastmaster Bond|auto-trad|
|[archetype-10-4AezsqaQRFtX024w.htm](feats/archetype-10-4AezsqaQRFtX024w.htm)|Shall not Falter, Shall not Rout|auto-trad|
|[archetype-10-4Cc0gQauzUqcYdLw.htm](feats/archetype-10-4Cc0gQauzUqcYdLw.htm)|Uncanny Dodge|auto-trad|
|[archetype-10-4w73cxvqhGRiozsK.htm](feats/archetype-10-4w73cxvqhGRiozsK.htm)|Bear Empathy|auto-trad|
|[archetype-10-4YqmeJQwnrG1Lg07.htm](feats/archetype-10-4YqmeJQwnrG1Lg07.htm)|Phenom's Verve|auto-trad|
|[archetype-10-5GC2iGtVp3UAH2nm.htm](feats/archetype-10-5GC2iGtVp3UAH2nm.htm)|Glutton For Flesh|auto-trad|
|[archetype-10-5nzeyuvzKL4T8eLt.htm](feats/archetype-10-5nzeyuvzKL4T8eLt.htm)|Weapon-Rune Shifter|auto-trad|
|[archetype-10-5TPKikTyN7lrCvzY.htm](feats/archetype-10-5TPKikTyN7lrCvzY.htm)|Mighty Bulwark|auto-trad|
|[archetype-10-6dJokwhIMvjAHL52.htm](feats/archetype-10-6dJokwhIMvjAHL52.htm)|Practiced Reloads|auto-trad|
|[archetype-10-6Hv4kpHj8IexNUey.htm](feats/archetype-10-6Hv4kpHj8IexNUey.htm)|Tiller's Aid|auto-trad|
|[archetype-10-6IrNSD0G5AAXPTJq.htm](feats/archetype-10-6IrNSD0G5AAXPTJq.htm)|Borrow Time|auto-trad|
|[archetype-10-7fyWKASX6uNByJF5.htm](feats/archetype-10-7fyWKASX6uNByJF5.htm)|Pass Through|auto-trad|
|[archetype-10-7nAoHfB7nsRwWDmF.htm](feats/archetype-10-7nAoHfB7nsRwWDmF.htm)|Expert Skysage Divination|auto-trad|
|[archetype-10-7x1r2w7C7A4Uy7wG.htm](feats/archetype-10-7x1r2w7C7A4Uy7wG.htm)|Topple Foe|auto-trad|
|[archetype-10-81xlcIFJIAtsmXmd.htm](feats/archetype-10-81xlcIFJIAtsmXmd.htm)|Armor Rune Shifter|auto-trad|
|[archetype-10-8d3QxoKmqSkB9Mcj.htm](feats/archetype-10-8d3QxoKmqSkB9Mcj.htm)|Endure Death's Touch|auto-trad|
|[archetype-10-8NQaqtHheeMUNGYr.htm](feats/archetype-10-8NQaqtHheeMUNGYr.htm)|Shared Synergy|auto-trad|
|[archetype-10-9Qn5E7Ujye9KdxOj.htm](feats/archetype-10-9Qn5E7Ujye9KdxOj.htm)|Recover Spell|auto-trad|
|[archetype-10-ADgQzThbtGKvp6hy.htm](feats/archetype-10-ADgQzThbtGKvp6hy.htm)|Bestiary Scholar|auto-trad|
|[archetype-10-AGovcdAzmUqxgiOq.htm](feats/archetype-10-AGovcdAzmUqxgiOq.htm)|Fused Polearm|auto-trad|
|[archetype-10-amf2zZuW299eiHAZ.htm](feats/archetype-10-amf2zZuW299eiHAZ.htm)|Everyone Duck!|auto-trad|
|[archetype-10-AMgTG6TwfoNmseWm.htm](feats/archetype-10-AMgTG6TwfoNmseWm.htm)|Channel Rot|auto-trad|
|[archetype-10-aVzkG1UhNjQr22pE.htm](feats/archetype-10-aVzkG1UhNjQr22pE.htm)|I Meant to do That|auto-trad|
|[archetype-10-AYByEEgPXk3QbCiF.htm](feats/archetype-10-AYByEEgPXk3QbCiF.htm)|Silent Sting|auto-trad|
|[archetype-10-B7VMXObJSNVI0ZGJ.htm](feats/archetype-10-B7VMXObJSNVI0ZGJ.htm)|Shattering Strike (Weapon Improviser)|auto-trad|
|[archetype-10-bBORql3kKsSyXllk.htm](feats/archetype-10-bBORql3kKsSyXllk.htm)|Monk's Flurry|auto-trad|
|[archetype-10-BBPrlPncXg86I42D.htm](feats/archetype-10-BBPrlPncXg86I42D.htm)|Counterclockwork Focus|auto-trad|
|[archetype-10-bJc477EbUYW2TlBx.htm](feats/archetype-10-bJc477EbUYW2TlBx.htm)|Drain Vitality|auto-trad|
|[archetype-10-bqBCatllklPceA34.htm](feats/archetype-10-bqBCatllklPceA34.htm)|Turn to Mist|auto-trad|
|[archetype-10-CMDCWPC8m2b6HvEN.htm](feats/archetype-10-CMDCWPC8m2b6HvEN.htm)|Spiritual Flurry|auto-trad|
|[archetype-10-cQ2jy4Utoxmk3MkG.htm](feats/archetype-10-cQ2jy4Utoxmk3MkG.htm)|Reactive Charm|auto-trad|
|[archetype-10-cxAvcFiZRT3ZVhie.htm](feats/archetype-10-cxAvcFiZRT3ZVhie.htm)|Emerald Boughs Hideaway|auto-trad|
|[archetype-10-D6bncWAOvFYX7hCE.htm](feats/archetype-10-D6bncWAOvFYX7hCE.htm)|Strident Command|auto-trad|
|[archetype-10-DFtbxytrOrmkQRfm.htm](feats/archetype-10-DFtbxytrOrmkQRfm.htm)|Steal the Sky|auto-trad|
|[archetype-10-DPaZurhC9uyIWPcu.htm](feats/archetype-10-DPaZurhC9uyIWPcu.htm)|Magical Adaptation|auto-trad|
|[archetype-10-dY0jhJOEj6DHc0ud.htm](feats/archetype-10-dY0jhJOEj6DHc0ud.htm)|Destructive Block|auto-trad|
|[archetype-10-EBqnbiuVL0ULFJWX.htm](feats/archetype-10-EBqnbiuVL0ULFJWX.htm)|Know It All (Eldritch Researcher)|auto-trad|
|[archetype-10-eceQmgvmoPplFKId.htm](feats/archetype-10-eceQmgvmoPplFKId.htm)|Rope Mastery|auto-trad|
|[archetype-10-EpWgrMznGbm8gceW.htm](feats/archetype-10-EpWgrMznGbm8gceW.htm)|Cautious Delver|auto-trad|
|[archetype-10-ErKwliHplziJY2BW.htm](feats/archetype-10-ErKwliHplziJY2BW.htm)|Greater Magical Scholastics|auto-trad|
|[archetype-10-F3ZBkDEWZ24NOR2j.htm](feats/archetype-10-F3ZBkDEWZ24NOR2j.htm)|Saving Slash|auto-trad|
|[archetype-10-fCsIyglmpb7NYwiy.htm](feats/archetype-10-fCsIyglmpb7NYwiy.htm)|Harrying Strike|auto-trad|
|[archetype-10-FOk8xTCHcHYyENu2.htm](feats/archetype-10-FOk8xTCHcHYyENu2.htm)|Bat Form|auto-trad|
|[archetype-10-gxzTEt37M0z1WY1M.htm](feats/archetype-10-gxzTEt37M0z1WY1M.htm)|Spinebreaker|auto-trad|
|[archetype-10-HgBksiMTUibPK36M.htm](feats/archetype-10-HgBksiMTUibPK36M.htm)|Halcyon Spellcasting Initiate|auto-trad|
|[archetype-10-HlqAdfxmcd9gdgHa.htm](feats/archetype-10-HlqAdfxmcd9gdgHa.htm)|Draw From the Land|auto-trad|
|[archetype-10-hRJV7byfPUHx1b9P.htm](feats/archetype-10-hRJV7byfPUHx1b9P.htm)|Greater Spell Runes|auto-trad|
|[archetype-10-hsMcKK92ho39djgI.htm](feats/archetype-10-hsMcKK92ho39djgI.htm)|Communal Tale|auto-trad|
|[archetype-10-hT0pVPqFuiEsmRb8.htm](feats/archetype-10-hT0pVPqFuiEsmRb8.htm)|Six Pillars Stance|auto-trad|
|[archetype-10-i8MnyasCDo3j65Xd.htm](feats/archetype-10-i8MnyasCDo3j65Xd.htm)|Quicken Heartbeat|auto-trad|
|[archetype-10-jBHDVsTVOBeoMoO4.htm](feats/archetype-10-jBHDVsTVOBeoMoO4.htm)|Greater Magical Edification|auto-trad|
|[archetype-10-Jnhkl2BOhxxRCTpp.htm](feats/archetype-10-Jnhkl2BOhxxRCTpp.htm)|Vigil's Walls Rise Anew|auto-trad|
|[archetype-10-Jv24QkykqdPB7brL.htm](feats/archetype-10-Jv24QkykqdPB7brL.htm)|Unbreakable Bond|auto-trad|
|[archetype-10-jvQoupE76OeUpjZp.htm](feats/archetype-10-jvQoupE76OeUpjZp.htm)|Scout's Pounce|auto-trad|
|[archetype-10-JwosaDYoqfPiFMYa.htm](feats/archetype-10-JwosaDYoqfPiFMYa.htm)|Eidetic Memorization|auto-trad|
|[archetype-10-K0x90HNHi5hONdDl.htm](feats/archetype-10-K0x90HNHi5hONdDl.htm)|Beast Dynamo Howl|auto-trad|
|[archetype-10-KcNXoSvULnuQjC9a.htm](feats/archetype-10-KcNXoSvULnuQjC9a.htm)|Incredible Familiar (Familiar Master)|auto-trad|
|[archetype-10-KNtnb9HwPnPDY2Mv.htm](feats/archetype-10-KNtnb9HwPnPDY2Mv.htm)|Wide Overwatch|auto-trad|
|[archetype-10-L7hs5XOCbJmh0H0e.htm](feats/archetype-10-L7hs5XOCbJmh0H0e.htm)|Expand Spiral|auto-trad|
|[archetype-10-lKeJruYQutWlNXyZ.htm](feats/archetype-10-lKeJruYQutWlNXyZ.htm)|Martial Exercise|auto-trad|
|[archetype-10-lszcn7eO3olp5vEt.htm](feats/archetype-10-lszcn7eO3olp5vEt.htm)|Grave Sight|auto-trad|
|[archetype-10-MgLUbsvAkIA4fsZW.htm](feats/archetype-10-MgLUbsvAkIA4fsZW.htm)|Gaze of Veracity|auto-trad|
|[archetype-10-MLiEMjyZXE43wmrq.htm](feats/archetype-10-MLiEMjyZXE43wmrq.htm)|Instant Armor|auto-trad|
|[archetype-10-mnhsG4l53YJkJIeY.htm](feats/archetype-10-mnhsG4l53YJkJIeY.htm)|Widen the Gap|auto-trad|
|[archetype-10-muDbZAyrE1ObyuTL.htm](feats/archetype-10-muDbZAyrE1ObyuTL.htm)|Beneath Notice|auto-trad|
|[archetype-10-muyEI60L0FmCHuWb.htm](feats/archetype-10-muyEI60L0FmCHuWb.htm)|Ensnaring Wrappings|auto-trad|
|[archetype-10-n0693gmx3k9wvb1N.htm](feats/archetype-10-n0693gmx3k9wvb1N.htm)|Demanding Challenge|auto-trad|
|[archetype-10-N0gJ4Q69nslbdXHg.htm](feats/archetype-10-N0gJ4Q69nslbdXHg.htm)|Command Attention|auto-trad|
|[archetype-10-N16lctDPZpvk9Khq.htm](feats/archetype-10-N16lctDPZpvk9Khq.htm)|Shadow Sneak Attack|auto-trad|
|[archetype-10-nDNmqez9McmrjBAV.htm](feats/archetype-10-nDNmqez9McmrjBAV.htm)|Attunement to Stone|auto-trad|
|[archetype-10-NLjxAO9Mhx3k7gpc.htm](feats/archetype-10-NLjxAO9Mhx3k7gpc.htm)|Winter's Embrace|auto-trad|
|[archetype-10-nobsCgNmsDX6aKR5.htm](feats/archetype-10-nobsCgNmsDX6aKR5.htm)|Rain-Scribe Mobility|auto-trad|
|[archetype-10-nZK5oGakaSZcSOs1.htm](feats/archetype-10-nZK5oGakaSZcSOs1.htm)|Unbelievable Escape|auto-trad|
|[archetype-10-O1C6pMTOxIrWBO3G.htm](feats/archetype-10-O1C6pMTOxIrWBO3G.htm)|Daring Flourish|auto-trad|
|[archetype-10-o8ogNJ53l1JDIJud.htm](feats/archetype-10-o8ogNJ53l1JDIJud.htm)|Font of Knowledge|auto-trad|
|[archetype-10-OCvgmAFz4qgj2Scf.htm](feats/archetype-10-OCvgmAFz4qgj2Scf.htm)|Stalwart Standard|auto-trad|
|[archetype-10-OmjTt8eR1Q3SmkPp.htm](feats/archetype-10-OmjTt8eR1Q3SmkPp.htm)|Armored Rest|auto-trad|
|[archetype-10-orTWiRwIQEc9FGJQ.htm](feats/archetype-10-orTWiRwIQEc9FGJQ.htm)|Fading|auto-trad|
|[archetype-10-phFRWFgeHdBzio2V.htm](feats/archetype-10-phFRWFgeHdBzio2V.htm)|Terrain Form|auto-trad|
|[archetype-10-pI5cG6x49ZQLchuy.htm](feats/archetype-10-pI5cG6x49ZQLchuy.htm)|Goblin Jubilee Display|auto-trad|
|[archetype-10-PlRAfeNof3xgJttZ.htm](feats/archetype-10-PlRAfeNof3xgJttZ.htm)|Surrounding Flames|auto-trad|
|[archetype-10-pT6r5BVJjALJUmb3.htm](feats/archetype-10-pT6r5BVJjALJUmb3.htm)|Tag Team|auto-trad|
|[archetype-10-qbFRPTHP96Q9kGpk.htm](feats/archetype-10-qbFRPTHP96Q9kGpk.htm)|Flash Your Badge|auto-trad|
|[archetype-10-QHh442n2CEtJr23B.htm](feats/archetype-10-QHh442n2CEtJr23B.htm)|Greater Deathly Secrets|auto-trad|
|[archetype-10-QhKLXGJPLZaX1qDp.htm](feats/archetype-10-QhKLXGJPLZaX1qDp.htm)|Expert Captivator Spellcasting|auto-trad|
|[archetype-10-Qjn9ErWY6wuIK9z6.htm](feats/archetype-10-Qjn9ErWY6wuIK9z6.htm)|Ward Casting|auto-trad|
|[archetype-10-qo6oKL8mE32hSjSC.htm](feats/archetype-10-qo6oKL8mE32hSjSC.htm)|Slayer's Presence|auto-trad|
|[archetype-10-RLWbx2itF1jmW0OL.htm](feats/archetype-10-RLWbx2itF1jmW0OL.htm)|Shadow Reservoir|auto-trad|
|[archetype-10-RtYOsmb9R71J9ce2.htm](feats/archetype-10-RtYOsmb9R71J9ce2.htm)|Hilt Hammer|auto-trad|
|[archetype-10-s0P4JXagA3085wLW.htm](feats/archetype-10-s0P4JXagA3085wLW.htm)|Toppling Tentacles|auto-trad|
|[archetype-10-S36WQ8o2MvPxQp0p.htm](feats/archetype-10-S36WQ8o2MvPxQp0p.htm)|Menacing Prowess|auto-trad|
|[archetype-10-SGBLDcT4wI5VUDCZ.htm](feats/archetype-10-SGBLDcT4wI5VUDCZ.htm)|Emissary of Peace|auto-trad|
|[archetype-10-SgXvw6rzk2lhTpXL.htm](feats/archetype-10-SgXvw6rzk2lhTpXL.htm)|Giant Snare|auto-trad|
|[archetype-10-sTUicpQkhiFVtMK1.htm](feats/archetype-10-sTUicpQkhiFVtMK1.htm)|Tempest-Sun Shielding|auto-trad|
|[archetype-10-SVfD887ocfUFikVA.htm](feats/archetype-10-SVfD887ocfUFikVA.htm)|Starlit Spells|auto-trad|
|[archetype-10-t6sey3cyV8n7a78l.htm](feats/archetype-10-t6sey3cyV8n7a78l.htm)|Trampling Charge|auto-trad|
|[archetype-10-tMsAj0H0B9XZQjtH.htm](feats/archetype-10-tMsAj0H0B9XZQjtH.htm)|Accursed Clay Fist|auto-trad|
|[archetype-10-ts2A1XeuPRzaCZgA.htm](feats/archetype-10-ts2A1XeuPRzaCZgA.htm)|Roadkill|auto-trad|
|[archetype-10-TW0fUdqqB69rIbRx.htm](feats/archetype-10-TW0fUdqqB69rIbRx.htm)|Lead the Way|auto-trad|
|[archetype-10-TyjW9VGtlH0Zkm0I.htm](feats/archetype-10-TyjW9VGtlH0Zkm0I.htm)|Signifer's Sight|auto-trad|
|[archetype-10-U9SrWgLJ8JoAKIy0.htm](feats/archetype-10-U9SrWgLJ8JoAKIy0.htm)|Rockslide Spell|auto-trad|
|[archetype-10-UCFGUbmDnzRGmN9I.htm](feats/archetype-10-UCFGUbmDnzRGmN9I.htm)|Steal Time|auto-trad|
|[archetype-10-UJcQgQpZRqXBw0Nb.htm](feats/archetype-10-UJcQgQpZRqXBw0Nb.htm)|Repurposed Parts|auto-trad|
|[archetype-10-v482u7QboZEbhgvv.htm](feats/archetype-10-v482u7QboZEbhgvv.htm)|Assured Ritualist|auto-trad|
|[archetype-10-V5FKAmcYQF4KRELG.htm](feats/archetype-10-V5FKAmcYQF4KRELG.htm)|Cascade Bearer's Spellcasting|auto-trad|
|[archetype-10-V5Y9r31BCTaCiNzi.htm](feats/archetype-10-V5Y9r31BCTaCiNzi.htm)|Lastwall Warden|auto-trad|
|[archetype-10-V7n3kVSl0G6a29KG.htm](feats/archetype-10-V7n3kVSl0G6a29KG.htm)|Flailtongue|auto-trad|
|[archetype-10-VJ3wIsfmabK02SNg.htm](feats/archetype-10-VJ3wIsfmabK02SNg.htm)|Provocator Dedication|auto-trad|
|[archetype-10-VsGdiAuVbYmDTR82.htm](feats/archetype-10-VsGdiAuVbYmDTR82.htm)|Black Powder Flash|auto-trad|
|[archetype-10-vu0R9VfRZ6RWTczZ.htm](feats/archetype-10-vu0R9VfRZ6RWTczZ.htm)|Practiced Defender|auto-trad|
|[archetype-10-waZzVgfyr1Vcxfqk.htm](feats/archetype-10-waZzVgfyr1Vcxfqk.htm)|Staggering Blow|auto-trad|
|[archetype-10-Wo0HNBh6CCgPN3Co.htm](feats/archetype-10-Wo0HNBh6CCgPN3Co.htm)|Tiller's Drive|auto-trad|
|[archetype-10-wUHnauB3atxO1RIo.htm](feats/archetype-10-wUHnauB3atxO1RIo.htm)|Rubbery Skin|auto-trad|
|[archetype-10-wuMa6iJyZ83LYJXH.htm](feats/archetype-10-wuMa6iJyZ83LYJXH.htm)|Into the Future|auto-trad|
|[archetype-10-wxIOuPFjVBADNYDP.htm](feats/archetype-10-wxIOuPFjVBADNYDP.htm)|Torrential Backlash|auto-trad|
|[archetype-10-xCuPob9TuSBKz6Gn.htm](feats/archetype-10-xCuPob9TuSBKz6Gn.htm)|Perfect Ki Expert|auto-trad|
|[archetype-10-XeFMWjJkZy2O7lou.htm](feats/archetype-10-XeFMWjJkZy2O7lou.htm)|Cloud Walk|auto-trad|
|[archetype-10-Yazy4gex46FLwsph.htm](feats/archetype-10-Yazy4gex46FLwsph.htm)|Tumbling Opportunist|auto-trad|
|[archetype-10-YdGHQjhUrNvP18AA.htm](feats/archetype-10-YdGHQjhUrNvP18AA.htm)|Spellmaster's Ward|auto-trad|
|[archetype-12-07YYaQBknzDTcbFz.htm](feats/archetype-12-07YYaQBknzDTcbFz.htm)|Directed Poison|auto-trad|
|[archetype-12-0Qr7Y1IOxny0vIQf.htm](feats/archetype-12-0Qr7Y1IOxny0vIQf.htm)|Tunneling Claws|auto-trad|
|[archetype-12-0RB3f3J7gOEv3fni.htm](feats/archetype-12-0RB3f3J7gOEv3fni.htm)|Expert Bard Spellcasting|auto-trad|
|[archetype-12-178rqdgJBF9Rl0Gi.htm](feats/archetype-12-178rqdgJBF9Rl0Gi.htm)|Master of the Dead|auto-trad|
|[archetype-12-1k5PZAYth8u4Fqyr.htm](feats/archetype-12-1k5PZAYth8u4Fqyr.htm)|Expert Sorcerer Spellcasting|auto-trad|
|[archetype-12-1ZbLYY5m2YALrrgA.htm](feats/archetype-12-1ZbLYY5m2YALrrgA.htm)|Perfection's Path (Fortitude)|auto-trad|
|[archetype-12-4bB3N36DmqllGJNx.htm](feats/archetype-12-4bB3N36DmqllGJNx.htm)|Shield Salvation|auto-trad|
|[archetype-12-5kua1Kf5Ca85lbzb.htm](feats/archetype-12-5kua1Kf5Ca85lbzb.htm)|Space-Time Shift|auto-trad|
|[archetype-12-5kUQuIP8N57MXhuz.htm](feats/archetype-12-5kUQuIP8N57MXhuz.htm)|Beastmaster's Call|auto-trad|
|[archetype-12-5ZX4btrw5yjBr8IM.htm](feats/archetype-12-5ZX4btrw5yjBr8IM.htm)|Bullet Dancer Reload|auto-trad|
|[archetype-12-6DkylvU5RF1O6DTT.htm](feats/archetype-12-6DkylvU5RF1O6DTT.htm)|Daredevil's Gambit|auto-trad|
|[archetype-12-7aHYM9fawA3PQwtM.htm](feats/archetype-12-7aHYM9fawA3PQwtM.htm)|Self Destruct|auto-trad|
|[archetype-12-7IrZIPP5ePtzzKcI.htm](feats/archetype-12-7IrZIPP5ePtzzKcI.htm)|Oneiric Influence|auto-trad|
|[archetype-12-8KUvJuAWCoxWg5FH.htm](feats/archetype-12-8KUvJuAWCoxWg5FH.htm)|Metallic Envisionment|auto-trad|
|[archetype-12-a2Owk9WI4pjSPuHf.htm](feats/archetype-12-a2Owk9WI4pjSPuHf.htm)|Educated Assessment|auto-trad|
|[archetype-12-a2wXdQHiIoj3lHoe.htm](feats/archetype-12-a2wXdQHiIoj3lHoe.htm)|Communal Sustain|auto-trad|
|[archetype-12-ADVrDjp75fr2BYMa.htm](feats/archetype-12-ADVrDjp75fr2BYMa.htm)|Charged Creation|auto-trad|
|[archetype-12-AmV13b9ncALtWJFt.htm](feats/archetype-12-AmV13b9ncALtWJFt.htm)|Redirect Elements|auto-trad|
|[archetype-12-Ano4tRq88V39eyPq.htm](feats/archetype-12-Ano4tRq88V39eyPq.htm)|Frightening Appearance|auto-trad|
|[archetype-12-AtidbY3lU49taaUR.htm](feats/archetype-12-AtidbY3lU49taaUR.htm)|Rejuvenation|auto-trad|
|[archetype-12-AvN95M5eVLLEu2qk.htm](feats/archetype-12-AvN95M5eVLLEu2qk.htm)|Splendid Companion|auto-trad|
|[archetype-12-aWHOcGLA7AhX4xpm.htm](feats/archetype-12-aWHOcGLA7AhX4xpm.htm)|Interfering Surge|auto-trad|
|[archetype-12-BJfIGuUMItalNYet.htm](feats/archetype-12-BJfIGuUMItalNYet.htm)|Evasiveness (Swashbuckler)|auto-trad|
|[archetype-12-bTNgHDqzfoqOLWu3.htm](feats/archetype-12-bTNgHDqzfoqOLWu3.htm)|Speaking Sky|auto-trad|
|[archetype-12-C4m59yjuDmZLnTqu.htm](feats/archetype-12-C4m59yjuDmZLnTqu.htm)|Shadow Illusion|auto-trad|
|[archetype-12-cppQZwnuoXqX8mgF.htm](feats/archetype-12-cppQZwnuoXqX8mgF.htm)|Vernai Training|auto-trad|
|[archetype-12-CQfxxsRxf1BuUH4o.htm](feats/archetype-12-CQfxxsRxf1BuUH4o.htm)|Janatimo's Lessons|auto-trad|
|[archetype-12-cV2CZxD6HB7HFeFv.htm](feats/archetype-12-cV2CZxD6HB7HFeFv.htm)|Sing to the Steel|auto-trad|
|[archetype-12-D3ypq5kAmLbwrgjq.htm](feats/archetype-12-D3ypq5kAmLbwrgjq.htm)|No Stranger to Death|auto-trad|
|[archetype-12-dWkf6LhYBfBkeyOA.htm](feats/archetype-12-dWkf6LhYBfBkeyOA.htm)|Spellmaster's Resilience|auto-trad|
|[archetype-12-E7PFg8xua1nGIqHQ.htm](feats/archetype-12-E7PFg8xua1nGIqHQ.htm)|Competitive Eater|auto-trad|
|[archetype-12-Eg7YZBmeNJeY9wkD.htm](feats/archetype-12-Eg7YZBmeNJeY9wkD.htm)|Drive-By Attack|auto-trad|
|[archetype-12-eoEYZJNdmvA5GfyK.htm](feats/archetype-12-eoEYZJNdmvA5GfyK.htm)|Expert Eldritch Archer Spellcasting|auto-trad|
|[archetype-12-etaixAdHNlHnLH0i.htm](feats/archetype-12-etaixAdHNlHnLH0i.htm)|Guarded Advance|auto-trad|
|[archetype-12-f6k9lIrIS4SfnCnG.htm](feats/archetype-12-f6k9lIrIS4SfnCnG.htm)|Master Alchemy|auto-trad|
|[archetype-12-FA1s2jjZqoBy58Xx.htm](feats/archetype-12-FA1s2jjZqoBy58Xx.htm)|Secrets of Shadow|auto-trad|
|[archetype-12-FtO8DjjMLBtWiRhZ.htm](feats/archetype-12-FtO8DjjMLBtWiRhZ.htm)|Expert Oracle Spellcasting|auto-trad|
|[archetype-12-gHHnMCBi1gvG5wTL.htm](feats/archetype-12-gHHnMCBi1gvG5wTL.htm)|Student Of The Dueling Arts|auto-trad|
|[archetype-12-gipC9eWrBDFRpNkQ.htm](feats/archetype-12-gipC9eWrBDFRpNkQ.htm)|Frostbite Runes|auto-trad|
|[archetype-12-Gu8IAYxwNdQM603P.htm](feats/archetype-12-Gu8IAYxwNdQM603P.htm)|Read the Land|auto-trad|
|[archetype-12-HbBNuT1bqdcCU9hM.htm](feats/archetype-12-HbBNuT1bqdcCU9hM.htm)|Greater Sun Blessing|auto-trad|
|[archetype-12-HJBDFHIaJ3lfxcbs.htm](feats/archetype-12-HJBDFHIaJ3lfxcbs.htm)|Maneuvering Spell|auto-trad|
|[archetype-12-hpCBELEKGA4ynYv4.htm](feats/archetype-12-hpCBELEKGA4ynYv4.htm)|Expert Wizard Spellcasting|auto-trad|
|[archetype-12-HxTqYtXRqFkkLfDQ.htm](feats/archetype-12-HxTqYtXRqFkkLfDQ.htm)|Gigantic Megafauna Companion|auto-trad|
|[archetype-12-hZueHQeYR5bgDh5W.htm](feats/archetype-12-hZueHQeYR5bgDh5W.htm)|Secret Eater|auto-trad|
|[archetype-12-I6su6Z9sJTgIrhQV.htm](feats/archetype-12-I6su6Z9sJTgIrhQV.htm)|Spiritual Aid|auto-trad|
|[archetype-12-iFEecf9o6uhJxWcG.htm](feats/archetype-12-iFEecf9o6uhJxWcG.htm)|Hellknight Order Cross-Training|auto-trad|
|[archetype-12-iJxbrXAdxhLqdT5E.htm](feats/archetype-12-iJxbrXAdxhLqdT5E.htm)|Assassinate|auto-trad|
|[archetype-12-INqBWbbDHF4DIg8i.htm](feats/archetype-12-INqBWbbDHF4DIg8i.htm)|Escape Timeline|auto-trad|
|[archetype-12-isdTXU8bV7ZVOAuQ.htm](feats/archetype-12-isdTXU8bV7ZVOAuQ.htm)|Flourishing Finish|auto-trad|
|[archetype-12-IZFw3Do0kBdgwZX0.htm](feats/archetype-12-IZFw3Do0kBdgwZX0.htm)|Expert Cathartic Spellcasting|auto-trad|
|[archetype-12-J5s7NeFHYuFSdhrX.htm](feats/archetype-12-J5s7NeFHYuFSdhrX.htm)|Hell's Armaments|auto-trad|
|[archetype-12-JJPoMcxUf3QoKA6h.htm](feats/archetype-12-JJPoMcxUf3QoKA6h.htm)|Combat Premonition|auto-trad|
|[archetype-12-JmHHdGC1p53BBedu.htm](feats/archetype-12-JmHHdGC1p53BBedu.htm)|Expert Magus Spellcasting|auto-trad|
|[archetype-12-jPBqvEH2jLlvDr6M.htm](feats/archetype-12-jPBqvEH2jLlvDr6M.htm)|Juggernaut's Fortitude|auto-trad|
|[archetype-12-jsbe2d9lYGJ2MksT.htm](feats/archetype-12-jsbe2d9lYGJ2MksT.htm)|Banshee Cry Display|auto-trad|
|[archetype-12-K5pXeeJLmdE8XuvM.htm](feats/archetype-12-K5pXeeJLmdE8XuvM.htm)|Tense Negotiator|auto-trad|
|[archetype-12-k9B7gDit3pXbq2XF.htm](feats/archetype-12-k9B7gDit3pXbq2XF.htm)|Finessed Features|auto-trad|
|[archetype-12-kKoqqXOTdRYROmVV.htm](feats/archetype-12-kKoqqXOTdRYROmVV.htm)|Blessed Denial|auto-trad|
|[archetype-12-KOslKkFO9N1kZY87.htm](feats/archetype-12-KOslKkFO9N1kZY87.htm)|Night's Shine|auto-trad|
|[archetype-12-KvKg9pBOpk2oLeO1.htm](feats/archetype-12-KvKg9pBOpk2oLeO1.htm)|Advanced Order Training|auto-trad|
|[archetype-12-KYF9e4oeSjHKgbwM.htm](feats/archetype-12-KYF9e4oeSjHKgbwM.htm)|Coordinated Charge|auto-trad|
|[archetype-12-lIpfj1JeFFS7Zn6D.htm](feats/archetype-12-lIpfj1JeFFS7Zn6D.htm)|Vigilant Benediction|auto-trad|
|[archetype-12-llDw5DqnB4LbIUCV.htm](feats/archetype-12-llDw5DqnB4LbIUCV.htm)|Expert Snowcasting|auto-trad|
|[archetype-12-lmAuoHPxzQdaSUmN.htm](feats/archetype-12-lmAuoHPxzQdaSUmN.htm)|Cartwheel Dodge|auto-trad|
|[archetype-12-lx8Wt813qavwLISv.htm](feats/archetype-12-lx8Wt813qavwLISv.htm)|Expert Beast Gunner Spellcasting|auto-trad|
|[archetype-12-MD33E76f2olLnDZb.htm](feats/archetype-12-MD33E76f2olLnDZb.htm)|Uncanny Suction|auto-trad|
|[archetype-12-MsieJK35tAP6gFGv.htm](feats/archetype-12-MsieJK35tAP6gFGv.htm)|Coffin Bound|auto-trad|
|[archetype-12-n1t6foOyrN48OVPK.htm](feats/archetype-12-n1t6foOyrN48OVPK.htm)|Collapse Wall|auto-trad|
|[archetype-12-NIzuFBqmHURIy2oI.htm](feats/archetype-12-NIzuFBqmHURIy2oI.htm)|Ward Slumber|auto-trad|
|[archetype-12-NM5a1tF0OW5mVYdR.htm](feats/archetype-12-NM5a1tF0OW5mVYdR.htm)|Additional Shadow Magic|auto-trad|
|[archetype-12-nPwzElPvV29eM5as.htm](feats/archetype-12-nPwzElPvV29eM5as.htm)|Chain Reaction|auto-trad|
|[archetype-12-O0QrBJfiMCTR0n0z.htm](feats/archetype-12-O0QrBJfiMCTR0n0z.htm)|Greater Despair|auto-trad|
|[archetype-12-Oa41bfBRO36lf1aE.htm](feats/archetype-12-Oa41bfBRO36lf1aE.htm)|Shoulder Catastrophe|auto-trad|
|[archetype-12-OyOpHPOXC08bffVR.htm](feats/archetype-12-OyOpHPOXC08bffVR.htm)|Determined Lore Seeker|auto-trad|
|[archetype-12-PeBz8f9h8Y4OFdws.htm](feats/archetype-12-PeBz8f9h8Y4OFdws.htm)|Legs of Stone|auto-trad|
|[archetype-12-peUBtdoG5LyVaf5g.htm](feats/archetype-12-peUBtdoG5LyVaf5g.htm)|Lich Dedication|auto-trad|
|[archetype-12-pKttKO84dxzRLBIC.htm](feats/archetype-12-pKttKO84dxzRLBIC.htm)|Expert Combat Eidolon|auto-trad|
|[archetype-12-PLTOCEAvqBS05pZu.htm](feats/archetype-12-PLTOCEAvqBS05pZu.htm)|Expert Cleric Spellcasting|auto-trad|
|[archetype-12-pu1U9ZWVVG1Lc94t.htm](feats/archetype-12-pu1U9ZWVVG1Lc94t.htm)|Diverse Weapon Expert|auto-trad|
|[archetype-12-pZUmoCqxBJfZmeqm.htm](feats/archetype-12-pZUmoCqxBJfZmeqm.htm)|Festering Wounds|auto-trad|
|[archetype-12-qgahXDIjevUmk5Ci.htm](feats/archetype-12-qgahXDIjevUmk5Ci.htm)|Pact of the Final Breath|auto-trad|
|[archetype-12-qmORiUubF2CVgIva.htm](feats/archetype-12-qmORiUubF2CVgIva.htm)|Reclaimant Plea|auto-trad|
|[archetype-12-Qss6GKzSYGqKgY4b.htm](feats/archetype-12-Qss6GKzSYGqKgY4b.htm)|Resolute|auto-trad|
|[archetype-12-r0twuF5nxXN5lkLk.htm](feats/archetype-12-r0twuF5nxXN5lkLk.htm)|Cut the Bonds|auto-trad|
|[archetype-12-RJmnFuaGLXfhX3It.htm](feats/archetype-12-RJmnFuaGLXfhX3It.htm)|Expert Psychic Spellcasting|auto-trad|
|[archetype-12-rm8NgrdZNjqpGlC1.htm](feats/archetype-12-rm8NgrdZNjqpGlC1.htm)|Instigate Psychic Duel|auto-trad|
|[archetype-12-RSwrDo9i0RKoAI6D.htm](feats/archetype-12-RSwrDo9i0RKoAI6D.htm)|Shepherd of Desolation|auto-trad|
|[archetype-12-rW1q7x5CMf9Rh1bi.htm](feats/archetype-12-rW1q7x5CMf9Rh1bi.htm)|Master Spotter (Ranger)|auto-trad|
|[archetype-12-s6y6JzPW2K8k4m8k.htm](feats/archetype-12-s6y6JzPW2K8k4m8k.htm)|Expert Summoner Spellcasting|auto-trad|
|[archetype-12-saEwTvJuiemEIfLm.htm](feats/archetype-12-saEwTvJuiemEIfLm.htm)|Expert Druid Spellcasting|auto-trad|
|[archetype-12-sjqnusj5Py7AiofF.htm](feats/archetype-12-sjqnusj5Py7AiofF.htm)|Perfection's Path (Will)|auto-trad|
|[archetype-12-ssrublnppwFSvVcb.htm](feats/archetype-12-ssrublnppwFSvVcb.htm)|Reach for the Sky|auto-trad|
|[archetype-12-SyxXnSk2R0AM9HSn.htm](feats/archetype-12-SyxXnSk2R0AM9HSn.htm)|Flicker|auto-trad|
|[archetype-12-t8CAK8ylu23PUxbn.htm](feats/archetype-12-t8CAK8ylu23PUxbn.htm)|Master Spotter (Investigator)|auto-trad|
|[archetype-12-tBfalnbUZLkG9gs1.htm](feats/archetype-12-tBfalnbUZLkG9gs1.htm)|Blade of Law|auto-trad|
|[archetype-12-UH8pDxrq1xJq4Sid.htm](feats/archetype-12-UH8pDxrq1xJq4Sid.htm)|Master Spotter|auto-trad|
|[archetype-12-UhmPekgw2HO40zKC.htm](feats/archetype-12-UhmPekgw2HO40zKC.htm)|Eagle Eyes|auto-trad|
|[archetype-12-UotFrqA7zAxtpJdE.htm](feats/archetype-12-UotFrqA7zAxtpJdE.htm)|Firearm Expert|auto-trad|
|[archetype-12-UqA9GdO2pGQwg9cd.htm](feats/archetype-12-UqA9GdO2pGQwg9cd.htm)|Flexible Halcyon Spellcasting|auto-trad|
|[archetype-12-uQExVrBNPJeS66sO.htm](feats/archetype-12-uQExVrBNPJeS66sO.htm)|For Love, For Lightning|auto-trad|
|[archetype-12-UrOj9TROtn8nuxPf.htm](feats/archetype-12-UrOj9TROtn8nuxPf.htm)|Expert Scroll Cache|auto-trad|
|[archetype-12-uvbPZR6dsQBimIUo.htm](feats/archetype-12-uvbPZR6dsQBimIUo.htm)|Imbue Mindlessness|auto-trad|
|[archetype-12-UxjAszOYAhUvCDt2.htm](feats/archetype-12-UxjAszOYAhUvCDt2.htm)|Inescapable Grasp|auto-trad|
|[archetype-12-UZDiqc5bBJzOTxUQ.htm](feats/archetype-12-UZDiqc5bBJzOTxUQ.htm)|Advanced Seeker of Truths|auto-trad|
|[archetype-12-Uzz7oZit3FNO9FxO.htm](feats/archetype-12-Uzz7oZit3FNO9FxO.htm)|Wings Of The Dragon|auto-trad|
|[archetype-12-VD446AflrQ3kO1al.htm](feats/archetype-12-VD446AflrQ3kO1al.htm)|Evasiveness (Rogue)|auto-trad|
|[archetype-12-VN3OHDYcnLaw0nW1.htm](feats/archetype-12-VN3OHDYcnLaw0nW1.htm)|Wild Strider|auto-trad|
|[archetype-12-vPA0EGXBaNzGxlxM.htm](feats/archetype-12-vPA0EGXBaNzGxlxM.htm)|Forewarn|auto-trad|
|[archetype-12-vqHF9U2RkL2NVgMF.htm](feats/archetype-12-vqHF9U2RkL2NVgMF.htm)|Enticing Dwelling|auto-trad|
|[archetype-12-VS28L98uFhr3j1HC.htm](feats/archetype-12-VS28L98uFhr3j1HC.htm)|Their Master's Call|auto-trad|
|[archetype-12-W7Rkw1L5QxHvgeUW.htm](feats/archetype-12-W7Rkw1L5QxHvgeUW.htm)|Eagle Eye|auto-trad|
|[archetype-12-wDo5dsSmyJqfmPgj.htm](feats/archetype-12-wDo5dsSmyJqfmPgj.htm)|Signifer Armor Expertise|auto-trad|
|[archetype-12-wGaxWwJhIXbMJft1.htm](feats/archetype-12-wGaxWwJhIXbMJft1.htm)|Plentiful Snares|auto-trad|
|[archetype-12-wkJ6EjtXUztOqTwH.htm](feats/archetype-12-wkJ6EjtXUztOqTwH.htm)|Perfection's Path (Reflex)|auto-trad|
|[archetype-12-WO23FvCo8IYVB5RF.htm](feats/archetype-12-WO23FvCo8IYVB5RF.htm)|Blade of the Crimson Oath|auto-trad|
|[archetype-12-WyGeBz9U6Hovozdl.htm](feats/archetype-12-WyGeBz9U6Hovozdl.htm)|Reverse Curse|auto-trad|
|[archetype-12-x7vMKBSrxXmfs5C2.htm](feats/archetype-12-x7vMKBSrxXmfs5C2.htm)|Expert Witch Spellcasting|auto-trad|
|[archetype-12-XgUQ6Tm9LKxcZGHW.htm](feats/archetype-12-XgUQ6Tm9LKxcZGHW.htm)|Knight in Shining Armor|auto-trad|
|[archetype-12-xmccXo6U7P0IMM3z.htm](feats/archetype-12-xmccXo6U7P0IMM3z.htm)|Reaper of Repose|auto-trad|
|[archetype-12-xNejAvuRXKYq2D6A.htm](feats/archetype-12-xNejAvuRXKYq2D6A.htm)|Swap Investment|auto-trad|
|[archetype-12-XWmlGnFNfxyJWw9V.htm](feats/archetype-12-XWmlGnFNfxyJWw9V.htm)|Choking Smoke|auto-trad|
|[archetype-12-xWWcxZUgQTaHZHkY.htm](feats/archetype-12-xWWcxZUgQTaHZHkY.htm)|School Counterspell|auto-trad|
|[archetype-12-xzesXSIXTqsVxm1e.htm](feats/archetype-12-xzesXSIXTqsVxm1e.htm)|Golem Dynamo|auto-trad|
|[archetype-12-YidYY7k2gvny9eSY.htm](feats/archetype-12-YidYY7k2gvny9eSY.htm)|Judgement of the Monolith|auto-trad|
|[archetype-12-ysXXa8K9fa383HAD.htm](feats/archetype-12-ysXXa8K9fa383HAD.htm)|Great Bear|auto-trad|
|[archetype-12-Zn2ySapQ2gtgyWgW.htm](feats/archetype-12-Zn2ySapQ2gtgyWgW.htm)|Aegis of Arnisant|auto-trad|
|[archetype-12-zybYSfeM0PLPpAaa.htm](feats/archetype-12-zybYSfeM0PLPpAaa.htm)|Infectious Emotions|auto-trad|
|[archetype-14-0EY2WQC3Hb6Mitgz.htm](feats/archetype-14-0EY2WQC3Hb6Mitgz.htm)|Form Lock|auto-trad|
|[archetype-14-0qL4a3CarG1e0pfB.htm](feats/archetype-14-0qL4a3CarG1e0pfB.htm)|Forceful Shot|auto-trad|
|[archetype-14-0S92aZtljjTAwLdO.htm](feats/archetype-14-0S92aZtljjTAwLdO.htm)|Drain Soul Cage|auto-trad|
|[archetype-14-0xh9ISHFUFHqngK0.htm](feats/archetype-14-0xh9ISHFUFHqngK0.htm)|Execution|auto-trad|
|[archetype-14-1fVKWWYjlVtOECku.htm](feats/archetype-14-1fVKWWYjlVtOECku.htm)|Shape of the Cloud Dragon|auto-trad|
|[archetype-14-2FJwXMTJycSZY80Q.htm](feats/archetype-14-2FJwXMTJycSZY80Q.htm)|Target Of Opportunity|auto-trad|
|[archetype-14-2l15VfVHMw3ttgJ3.htm](feats/archetype-14-2l15VfVHMw3ttgJ3.htm)|Incredible Recollection|auto-trad|
|[archetype-14-45F4nNN8gxoBdSnk.htm](feats/archetype-14-45F4nNN8gxoBdSnk.htm)|Enshroud Soul Cage|auto-trad|
|[archetype-14-4dQcLroKQ13QYIT3.htm](feats/archetype-14-4dQcLroKQ13QYIT3.htm)|Tactical Cadence|auto-trad|
|[archetype-14-5kua1Kf5Ca85lbzb.htm](feats/archetype-14-5kua1Kf5Ca85lbzb.htm)|Space-Time Shift|auto-trad|
|[archetype-14-8nCxI2SZ64UmMuRZ.htm](feats/archetype-14-8nCxI2SZ64UmMuRZ.htm)|Entities From Afar|auto-trad|
|[archetype-14-8PMxl8o5YXET58Pn.htm](feats/archetype-14-8PMxl8o5YXET58Pn.htm)|Iron Lung|auto-trad|
|[archetype-14-9DECwTTiVpHJc4B6.htm](feats/archetype-14-9DECwTTiVpHJc4B6.htm)|Specialized Megafauna Companion|auto-trad|
|[archetype-14-9zfcIXDG2mDpiypp.htm](feats/archetype-14-9zfcIXDG2mDpiypp.htm)|Armored Exercise|auto-trad|
|[archetype-14-AiDelwfA0uVT2lN3.htm](feats/archetype-14-AiDelwfA0uVT2lN3.htm)|Signature Synergy|auto-trad|
|[archetype-14-aOYnK0xe9DKFtx7d.htm](feats/archetype-14-aOYnK0xe9DKFtx7d.htm)|Innocent Butterfly|auto-trad|
|[archetype-14-As8cRK5jVzf62fEd.htm](feats/archetype-14-As8cRK5jVzf62fEd.htm)|Peculiar Anatomy|auto-trad|
|[archetype-14-bEdFgywri7fABhBT.htm](feats/archetype-14-bEdFgywri7fABhBT.htm)|Absorb Spell|auto-trad|
|[archetype-14-Bi4rdz48MgkSY7su.htm](feats/archetype-14-Bi4rdz48MgkSY7su.htm)|Crimson Oath Devotion|auto-trad|
|[archetype-14-bki36RiEM5FR4aiT.htm](feats/archetype-14-bki36RiEM5FR4aiT.htm)|Specialized Beastmaster Companion|auto-trad|
|[archetype-14-CR9NcAIPTT4oWSEy.htm](feats/archetype-14-CR9NcAIPTT4oWSEy.htm)|Hammer Quake|auto-trad|
|[archetype-14-dBB71rps7dbat4Vo.htm](feats/archetype-14-dBB71rps7dbat4Vo.htm)|Dream Logic|auto-trad|
|[archetype-14-dloGUhZYG1xUPVE4.htm](feats/archetype-14-dloGUhZYG1xUPVE4.htm)|Whirling Knockdown|auto-trad|
|[archetype-14-Ec9Q8cvOKFgeezx6.htm](feats/archetype-14-Ec9Q8cvOKFgeezx6.htm)|Terrible Transformation|auto-trad|
|[archetype-14-eR0sifECG27CC4do.htm](feats/archetype-14-eR0sifECG27CC4do.htm)|Shape Of The Dragon|auto-trad|
|[archetype-14-EVNd9hZs49b1pScR.htm](feats/archetype-14-EVNd9hZs49b1pScR.htm)|Dual Onslaught|auto-trad|
|[archetype-14-F7Ao9p17ocf3JVvy.htm](feats/archetype-14-F7Ao9p17ocf3JVvy.htm)|Shadow Power|auto-trad|
|[archetype-14-gQKPKSS5KyK3uUfs.htm](feats/archetype-14-gQKPKSS5KyK3uUfs.htm)|Resolute Defender|auto-trad|
|[archetype-14-h7KZXNRm1gLV1yTt.htm](feats/archetype-14-h7KZXNRm1gLV1yTt.htm)|Mist Escape|auto-trad|
|[archetype-14-ibaxh77mm8ttObdk.htm](feats/archetype-14-ibaxh77mm8ttObdk.htm)|Thwart Evil|auto-trad|
|[archetype-14-IdKfWg48qMieuzl7.htm](feats/archetype-14-IdKfWg48qMieuzl7.htm)|Murderer's Circle|auto-trad|
|[archetype-14-j4QSlswoBCVrPYa8.htm](feats/archetype-14-j4QSlswoBCVrPYa8.htm)|Consecrated Aura|auto-trad|
|[archetype-14-jEq4JcZb0LpKOZy1.htm](feats/archetype-14-jEq4JcZb0LpKOZy1.htm)|Diverse Armor Expert|auto-trad|
|[archetype-14-JgidFws6bOoGQcti.htm](feats/archetype-14-JgidFws6bOoGQcti.htm)|Words of Unraveling|auto-trad|
|[archetype-14-jK0hjx4qsRyKnpBd.htm](feats/archetype-14-jK0hjx4qsRyKnpBd.htm)|Starlight Armor|auto-trad|
|[archetype-14-k0h4jvjxK0fYFvOU.htm](feats/archetype-14-k0h4jvjxK0fYFvOU.htm)|Pact of Eldritch Eyes|auto-trad|
|[archetype-14-kC92uxCvrxDkrCpO.htm](feats/archetype-14-kC92uxCvrxDkrCpO.htm)|Borrow Memories|auto-trad|
|[archetype-14-LDv6RVuDXJ9nOfhj.htm](feats/archetype-14-LDv6RVuDXJ9nOfhj.htm)|Halcyon Spellcasting Adept|auto-trad|
|[archetype-14-LhqWrgeBH2sdRxND.htm](feats/archetype-14-LhqWrgeBH2sdRxND.htm)|Unlimited Ghost Flight|auto-trad|
|[archetype-14-llawV63qzdynbOkx.htm](feats/archetype-14-llawV63qzdynbOkx.htm)|Stone Communion|auto-trad|
|[archetype-14-lPoP5TFfq266kR6g.htm](feats/archetype-14-lPoP5TFfq266kR6g.htm)|Spellmaster's Tenacity|auto-trad|
|[archetype-14-lZA7SYBmSPFlKNr2.htm](feats/archetype-14-lZA7SYBmSPFlKNr2.htm)|Effortless Captivation|auto-trad|
|[archetype-14-matJDIUDvgaJqyiF.htm](feats/archetype-14-matJDIUDvgaJqyiF.htm)|Hand of the Lich|auto-trad|
|[archetype-14-mprVzno2BR2VhRSJ.htm](feats/archetype-14-mprVzno2BR2VhRSJ.htm)|Winter's Kiss|auto-trad|
|[archetype-14-MyDluIbnX2sjm3pB.htm](feats/archetype-14-MyDluIbnX2sjm3pB.htm)|Ranged Disarm|auto-trad|
|[archetype-14-NCsjkjEp6kHFS07h.htm](feats/archetype-14-NCsjkjEp6kHFS07h.htm)|Concentrated Assault|auto-trad|
|[archetype-14-nY8HtHNJMqP0hz3v.htm](feats/archetype-14-nY8HtHNJMqP0hz3v.htm)|Phase Bullet|auto-trad|
|[archetype-14-Pk6qGMlef5SMhhwE.htm](feats/archetype-14-Pk6qGMlef5SMhhwE.htm)|Night's Warning|auto-trad|
|[archetype-14-PSpwdvuddC9kXONz.htm](feats/archetype-14-PSpwdvuddC9kXONz.htm)|Death's Door|auto-trad|
|[archetype-14-q0DGOuLuJNLzgM8d.htm](feats/archetype-14-q0DGOuLuJNLzgM8d.htm)|Unending Emptiness|auto-trad|
|[archetype-14-RHdY0xczRYzkIdJt.htm](feats/archetype-14-RHdY0xczRYzkIdJt.htm)|Instinctual Interception|auto-trad|
|[archetype-14-RnxullWsNdbU7fuH.htm](feats/archetype-14-RnxullWsNdbU7fuH.htm)|Pivot Strike|auto-trad|
|[archetype-14-roItUHUbBqhHfwJr.htm](feats/archetype-14-roItUHUbBqhHfwJr.htm)|Seeker Arrow|auto-trad|
|[archetype-14-S450JMWfF90oOcv9.htm](feats/archetype-14-S450JMWfF90oOcv9.htm)|Path Of Iron|auto-trad|
|[archetype-14-SA8rnheHFtjkATrJ.htm](feats/archetype-14-SA8rnheHFtjkATrJ.htm)|Shifting Terrain|auto-trad|
|[archetype-14-ST6AhCbEDSMxXf20.htm](feats/archetype-14-ST6AhCbEDSMxXf20.htm)|Perfect Ki Exemplar|auto-trad|
|[archetype-14-TdA3oVj79KxOm2Kd.htm](feats/archetype-14-TdA3oVj79KxOm2Kd.htm)|Wind-Tossed Spell|auto-trad|
|[archetype-14-TNRB8IY6Wtk9BoMp.htm](feats/archetype-14-TNRB8IY6Wtk9BoMp.htm)|Terrain Shield|auto-trad|
|[archetype-14-TQCpXi1hwYX6VIhp.htm](feats/archetype-14-TQCpXi1hwYX6VIhp.htm)|Speedy Rituals|auto-trad|
|[archetype-14-ubF6nGJrnfW7ocSg.htm](feats/archetype-14-ubF6nGJrnfW7ocSg.htm)|Graveshift|auto-trad|
|[archetype-14-UJcuACMlspc1raL1.htm](feats/archetype-14-UJcuACMlspc1raL1.htm)|Curse of the Saumen Kar|auto-trad|
|[archetype-14-uPikeCzrTrgzEJT8.htm](feats/archetype-14-uPikeCzrTrgzEJT8.htm)|Talismanic Sage|auto-trad|
|[archetype-14-UxurtbOOvCkngsKN.htm](feats/archetype-14-UxurtbOOvCkngsKN.htm)|Paragon Reanimated Companion|auto-trad|
|[archetype-14-Vzbu8tclrsS2IBYU.htm](feats/archetype-14-Vzbu8tclrsS2IBYU.htm)|Seize|auto-trad|
|[archetype-14-w1GFspPIQ9f5MbFL.htm](feats/archetype-14-w1GFspPIQ9f5MbFL.htm)|Touch Focus|auto-trad|
|[archetype-14-wBL5h0hTmVD4EJLw.htm](feats/archetype-14-wBL5h0hTmVD4EJLw.htm)|Control Tower|auto-trad|
|[archetype-14-wijzB1FDUT7SC86a.htm](feats/archetype-14-wijzB1FDUT7SC86a.htm)|Specialized Mount|auto-trad|
|[archetype-14-WKRSBu7H9miAwUaR.htm](feats/archetype-14-WKRSBu7H9miAwUaR.htm)|Spirit Guide Form|auto-trad|
|[archetype-14-xdv75qkv5TOFlHmM.htm](feats/archetype-14-xdv75qkv5TOFlHmM.htm)|Keep up the Good Fight|auto-trad|
|[archetype-14-zK0Dr1FZDOq2DMn8.htm](feats/archetype-14-zK0Dr1FZDOq2DMn8.htm)|Improved Hijack Undead|auto-trad|
|[archetype-14-ZNq7Qgubi9gUqR0L.htm](feats/archetype-14-ZNq7Qgubi9gUqR0L.htm)|Bodysnatcher|auto-trad|
|[archetype-14-ZTxiM8NExDmxHJDf.htm](feats/archetype-14-ZTxiM8NExDmxHJDf.htm)|Pin to the Spot|auto-trad|
|[archetype-16-2LdncNMDNJW5Oeyu.htm](feats/archetype-16-2LdncNMDNJW5Oeyu.htm)|Opportune Throw|auto-trad|
|[archetype-16-Aeea44xyJqYArFun.htm](feats/archetype-16-Aeea44xyJqYArFun.htm)|Lead The Pack|auto-trad|
|[archetype-16-aoZYZm2PrTKEK0Ji.htm](feats/archetype-16-aoZYZm2PrTKEK0Ji.htm)|Converge|auto-trad|
|[archetype-16-AV9NkSLNvv0UcdcP.htm](feats/archetype-16-AV9NkSLNvv0UcdcP.htm)|Breath of Hungry Death|auto-trad|
|[archetype-16-AyP23H3WJkEAIEKd.htm](feats/archetype-16-AyP23H3WJkEAIEKd.htm)|Mobile Magical Combat|auto-trad|
|[archetype-16-AYVf9MU8oo1QWbGv.htm](feats/archetype-16-AYVf9MU8oo1QWbGv.htm)|Wave the Flag|auto-trad|
|[archetype-16-cHOALlKY1XsCj3Fe.htm](feats/archetype-16-cHOALlKY1XsCj3Fe.htm)|Disciple's Breath|auto-trad|
|[archetype-16-CtC7DM1poHDwwu52.htm](feats/archetype-16-CtC7DM1poHDwwu52.htm)|Fearsome Fangs|auto-trad|
|[archetype-16-EDdJFwarNIJIkP2E.htm](feats/archetype-16-EDdJFwarNIJIkP2E.htm)|Master's Counterspell|auto-trad|
|[archetype-16-EwCDbWg8yPxDWF4a.htm](feats/archetype-16-EwCDbWg8yPxDWF4a.htm)|Stunning Appearance|auto-trad|
|[archetype-16-fgnfXwFcn9jZlXGD.htm](feats/archetype-16-fgnfXwFcn9jZlXGD.htm)|Advanced Runic Mind Smithing|auto-trad|
|[archetype-16-giupBd3dyOwdeoFl.htm](feats/archetype-16-giupBd3dyOwdeoFl.htm)|Performance Weapon Expert|auto-trad|
|[archetype-16-gqISxjbTtZSYndZ4.htm](feats/archetype-16-gqISxjbTtZSYndZ4.htm)|Quickened Attunement|auto-trad|
|[archetype-16-hKemfam7Hvkllsgp.htm](feats/archetype-16-hKemfam7Hvkllsgp.htm)|Purge of Moments|auto-trad|
|[archetype-16-JezNf3xbCi8h2qKe.htm](feats/archetype-16-JezNf3xbCi8h2qKe.htm)|Vessel's Form|auto-trad|
|[archetype-16-jI4Eoi6m0ogjXkGK.htm](feats/archetype-16-jI4Eoi6m0ogjXkGK.htm)|Master Captivator Spellcasting|auto-trad|
|[archetype-16-kVSZQvsz1cYjOwxL.htm](feats/archetype-16-kVSZQvsz1cYjOwxL.htm)|Master Siege Engineer|auto-trad|
|[archetype-16-LiLnDkoFcwW1RxqZ.htm](feats/archetype-16-LiLnDkoFcwW1RxqZ.htm)|Withstand Death|auto-trad|
|[archetype-16-lleedxE6fTDSK6og.htm](feats/archetype-16-lleedxE6fTDSK6og.htm)|Resuscitate|auto-trad|
|[archetype-16-LRSTjjBNNlD0XZX8.htm](feats/archetype-16-LRSTjjBNNlD0XZX8.htm)|Phase Arrow|auto-trad|
|[archetype-16-MGtlUc6cy4nCM4Lk.htm](feats/archetype-16-MGtlUc6cy4nCM4Lk.htm)|Bound in Ice|auto-trad|
|[archetype-16-Mix4IwZRuCz7JS3T.htm](feats/archetype-16-Mix4IwZRuCz7JS3T.htm)|Greater Snow Step|auto-trad|
|[archetype-16-qZzRXAa9mNQPUXoW.htm](feats/archetype-16-qZzRXAa9mNQPUXoW.htm)|Controlled Bullet|auto-trad|
|[archetype-16-Ruee0G3oZG5n5Auk.htm](feats/archetype-16-Ruee0G3oZG5n5Auk.htm)|Master Skysage Divination|auto-trad|
|[archetype-16-S8Hda5OtajS9gpqM.htm](feats/archetype-16-S8Hda5OtajS9gpqM.htm)|Ward Mind|auto-trad|
|[archetype-16-sFAqKrzqXQhtrfqN.htm](feats/archetype-16-sFAqKrzqXQhtrfqN.htm)|Sixth Pillar Mastery|auto-trad|
|[archetype-16-SGgK4BoUooA0HhTj.htm](feats/archetype-16-SGgK4BoUooA0HhTj.htm)|Avalanche Strike|auto-trad|
|[archetype-16-VYdZmTifZRkRF7ey.htm](feats/archetype-16-VYdZmTifZRkRF7ey.htm)|Deflecting Cloud|auto-trad|
|[archetype-16-WoUwDqhA6i6abwen.htm](feats/archetype-16-WoUwDqhA6i6abwen.htm)|Song of Grace and Speed|auto-trad|
|[archetype-16-X5LxcrZbjHfmX58a.htm](feats/archetype-16-X5LxcrZbjHfmX58a.htm)|Bolster Soul Cage|auto-trad|
|[archetype-16-XpZkzUV9PwUHvmyq.htm](feats/archetype-16-XpZkzUV9PwUHvmyq.htm)|Fulminating Synergy|auto-trad|
|[archetype-16-XRCqj74dG27MHNxQ.htm](feats/archetype-16-XRCqj74dG27MHNxQ.htm)|Spell Gem|auto-trad|
|[archetype-16-XXOc3MxbkEBMgFeT.htm](feats/archetype-16-XXOc3MxbkEBMgFeT.htm)|Cross the Threshold|auto-trad|
|[archetype-16-xYNMWGEmpbtrtWXQ.htm](feats/archetype-16-xYNMWGEmpbtrtWXQ.htm)|Unwind Death|auto-trad|
|[archetype-16-yqIorA6QGWmbKoOz.htm](feats/archetype-16-yqIorA6QGWmbKoOz.htm)|Dominating Gaze|auto-trad|
|[archetype-16-zodscjgydZRUSOLO.htm](feats/archetype-16-zodscjgydZRUSOLO.htm)|Focus Ally|auto-trad|
|[archetype-16-ZXLbIgL3NNplgnuB.htm](feats/archetype-16-ZXLbIgL3NNplgnuB.htm)|Shared Dream|auto-trad|
|[archetype-18-218WeRqUqdnguRdS.htm](feats/archetype-18-218WeRqUqdnguRdS.htm)|Master Druid Spellcasting|auto-trad|
|[archetype-18-5NWZct5OvSiVvMn8.htm](feats/archetype-18-5NWZct5OvSiVvMn8.htm)|Master Cathartic Spellcasting|auto-trad|
|[archetype-18-6iOLxitjqHujH1Tj.htm](feats/archetype-18-6iOLxitjqHujH1Tj.htm)|Arrow of Death|auto-trad|
|[archetype-18-6kz7FPdxDrsPiNti.htm](feats/archetype-18-6kz7FPdxDrsPiNti.htm)|Master Eldritch Archer Spellcasting|auto-trad|
|[archetype-18-6qNRVKwbnX381jVj.htm](feats/archetype-18-6qNRVKwbnX381jVj.htm)|Master Beast Gunner Spellcasting|auto-trad|
|[archetype-18-7uofkNynjFy3ofk2.htm](feats/archetype-18-7uofkNynjFy3ofk2.htm)|Ever Dreaming|auto-trad|
|[archetype-18-9v3wpgvEEiZHO0SJ.htm](feats/archetype-18-9v3wpgvEEiZHO0SJ.htm)|Perfect Ki Grandmaster|auto-trad|
|[archetype-18-bc6tcXSzakyCbQsS.htm](feats/archetype-18-bc6tcXSzakyCbQsS.htm)|Master Cleric Spellcasting|auto-trad|
|[archetype-18-blOiU4LPlBjVHcgR.htm](feats/archetype-18-blOiU4LPlBjVHcgR.htm)|Look Again|auto-trad|
|[archetype-18-BqcAwmNjDlKEI84X.htm](feats/archetype-18-BqcAwmNjDlKEI84X.htm)|Shadow Master|auto-trad|
|[archetype-18-DFY3X7Mgl9rESQuu.htm](feats/archetype-18-DFY3X7Mgl9rESQuu.htm)|Mighty Dragon Shape|auto-trad|
|[archetype-18-eJNzP21lFPV3zWkm.htm](feats/archetype-18-eJNzP21lFPV3zWkm.htm)|Master Bard Spellcasting|auto-trad|
|[archetype-18-fyJ2slL98hnQH3On.htm](feats/archetype-18-fyJ2slL98hnQH3On.htm)|Soaring Dynamo|auto-trad|
|[archetype-18-hdt3RHZljLrO49kq.htm](feats/archetype-18-hdt3RHZljLrO49kq.htm)|Specialized Companion (Animal Trainer)|auto-trad|
|[archetype-18-i7ibUqJCl1GXRFEa.htm](feats/archetype-18-i7ibUqJCl1GXRFEa.htm)|Master Witch Spellcasting|auto-trad|
|[archetype-18-iuLLw1X5RjRcR4rH.htm](feats/archetype-18-iuLLw1X5RjRcR4rH.htm)|Halcyon Spellcasting Sage|auto-trad|
|[archetype-18-kPsJIz19viZy8YjO.htm](feats/archetype-18-kPsJIz19viZy8YjO.htm)|Master Summoning Spellcasting|auto-trad|
|[archetype-18-Ky7ZEtt4TchQNmFc.htm](feats/archetype-18-Ky7ZEtt4TchQNmFc.htm)|Timeline-Splitting Spell|auto-trad|
|[archetype-18-lIg5Gzz7W70jfbk1.htm](feats/archetype-18-lIg5Gzz7W70jfbk1.htm)|Master Scroll Cache|auto-trad|
|[archetype-18-phD0PbElkEeldZ2U.htm](feats/archetype-18-phD0PbElkEeldZ2U.htm)|Master Sorcerer Spellcasting|auto-trad|
|[archetype-18-PMckhnGYMyiwUNiL.htm](feats/archetype-18-PMckhnGYMyiwUNiL.htm)|Master Wizard Spellcasting|auto-trad|
|[archetype-18-pWbNhfBiskV4n58a.htm](feats/archetype-18-pWbNhfBiskV4n58a.htm)|Black Powder Embodiment|auto-trad|
|[archetype-18-PzbfpOQXOjpUWKkL.htm](feats/archetype-18-PzbfpOQXOjpUWKkL.htm)|Master Psychic Spellcasting|auto-trad|
|[archetype-18-Rbp08BSSzwpkWVjh.htm](feats/archetype-18-Rbp08BSSzwpkWVjh.htm)|Terrifying Countenance|auto-trad|
|[archetype-18-rmO7FP410nvCjFBB.htm](feats/archetype-18-rmO7FP410nvCjFBB.htm)|Stave off Catastrophe|auto-trad|
|[archetype-18-S3hN7qK7aiCDTrpM.htm](feats/archetype-18-S3hN7qK7aiCDTrpM.htm)|Retain Absorbed Spell|auto-trad|
|[archetype-18-SlXLsuxBHeUyUPII.htm](feats/archetype-18-SlXLsuxBHeUyUPII.htm)|Mighty Wings|auto-trad|
|[archetype-18-syxJQ48bxE8NY91a.htm](feats/archetype-18-syxJQ48bxE8NY91a.htm)|Master Snowcasting|auto-trad|
|[archetype-18-v22FQuw17Dlr1b3x.htm](feats/archetype-18-v22FQuw17Dlr1b3x.htm)|Miraculous Flight|auto-trad|
|[archetype-18-YaJ1JTBMhpu1RWXV.htm](feats/archetype-18-YaJ1JTBMhpu1RWXV.htm)|Mighty Bear|auto-trad|
|[archetype-18-ZB2WMbhALgQTGY3c.htm](feats/archetype-18-ZB2WMbhALgQTGY3c.htm)|School Spell Redirection|auto-trad|
|[archetype-18-ZkGhDEpqe7fzJuSr.htm](feats/archetype-18-ZkGhDEpqe7fzJuSr.htm)|Master Oracle Spellcasting|auto-trad|
|[archetype-20-20Yax5lEqjftKBHZ.htm](feats/archetype-20-20Yax5lEqjftKBHZ.htm)|Vigilant Mask|auto-trad|
|[archetype-20-5f0EbNl7DHkiKCIr.htm](feats/archetype-20-5f0EbNl7DHkiKCIr.htm)|Empyreal Aura|auto-trad|
|[archetype-20-5uhKRkYDzLP7v3XY.htm](feats/archetype-20-5uhKRkYDzLP7v3XY.htm)|Zombie Horde|auto-trad|
|[archetype-20-6PCNYExygF5890Fl.htm](feats/archetype-20-6PCNYExygF5890Fl.htm)|Thick Hide Mask|auto-trad|
|[archetype-20-8YSwzLNlmBLoEyUj.htm](feats/archetype-20-8YSwzLNlmBLoEyUj.htm)|Cross the Final Horizon|auto-trad|
|[archetype-20-aZ5JdFKA7L8NYl4o.htm](feats/archetype-20-aZ5JdFKA7L8NYl4o.htm)|Tower Shield Mastery|auto-trad|
|[archetype-20-bwbPuv4JsilmMnPz.htm](feats/archetype-20-bwbPuv4JsilmMnPz.htm)|Immortal Bear|auto-trad|
|[archetype-20-csjkzb5dsyZPeOtY.htm](feats/archetype-20-csjkzb5dsyZPeOtY.htm)|Wrath of the First Ghoul|auto-trad|
|[archetype-20-CuJzCxGwd1EZDift.htm](feats/archetype-20-CuJzCxGwd1EZDift.htm)|Cunning Trickster Mask|auto-trad|
|[archetype-20-dj0aOPPcDOqmptpX.htm](feats/archetype-20-dj0aOPPcDOqmptpX.htm)|Storyteller's Mask|auto-trad|
|[archetype-20-epNrbgmjZDjJe7Ry.htm](feats/archetype-20-epNrbgmjZDjJe7Ry.htm)|Legendary Rider|auto-trad|
|[archetype-20-f4k5ripShCn5orZB.htm](feats/archetype-20-f4k5ripShCn5orZB.htm)|Grand Medic's Mask|auto-trad|
|[archetype-20-H63SJLkenhLDkVnN.htm](feats/archetype-20-H63SJLkenhLDkVnN.htm)|The Tyrant Falls!|auto-trad|
|[archetype-20-HxO0Sh3hNFMoSsTB.htm](feats/archetype-20-HxO0Sh3hNFMoSsTB.htm)|Emancipator's Mask|auto-trad|
|[archetype-20-kOqW5ZtnOiWxKl9M.htm](feats/archetype-20-kOqW5ZtnOiWxKl9M.htm)|Icy Apotheosis|auto-trad|
|[archetype-20-nn7DiYYWinsSYrZy.htm](feats/archetype-20-nn7DiYYWinsSYrZy.htm)|Protective Spirit Mask|auto-trad|
|[archetype-20-SS8JSDB2P0SFX1KH.htm](feats/archetype-20-SS8JSDB2P0SFX1KH.htm)|Sky Master Mask|auto-trad|
|[archetype-20-tFUo2tsdreWBxMfs.htm](feats/archetype-20-tFUo2tsdreWBxMfs.htm)|Stalking Feline Mask|auto-trad|
|[archetype-20-WAWaew53zpTFVwQM.htm](feats/archetype-20-WAWaew53zpTFVwQM.htm)|Synergistic Spell|auto-trad|
|[archetype-20-WVm2dxPOI4tK2wsJ.htm](feats/archetype-20-WVm2dxPOI4tK2wsJ.htm)|Impossible Volley (Eldritch Archer)|auto-trad|
|[archetype-20-XHOhAHv2HLdJQz2q.htm](feats/archetype-20-XHOhAHv2HLdJQz2q.htm)|Tireless Guide's Mask|auto-trad|
|[bonus-00-1xpqOpguTUAeFsdO.htm](feats/bonus-00-1xpqOpguTUAeFsdO.htm)|Seek Injustice|auto-trad|
|[bonus-00-AGiIjgLOFuahmwiT.htm](feats/bonus-00-AGiIjgLOFuahmwiT.htm)|Shackles of Law|auto-trad|
|[bonus-00-AxjjAoU1IZaNUGGS.htm](feats/bonus-00-AxjjAoU1IZaNUGGS.htm)|Fear No Law, Fear No One|auto-trad|
|[bonus-00-cMkpLgadlLCzDOv0.htm](feats/bonus-00-cMkpLgadlLCzDOv0.htm)|Blessings of the Five|auto-trad|
|[bonus-00-FlsAYAGEiZg1gg7D.htm](feats/bonus-00-FlsAYAGEiZg1gg7D.htm)|Spiritual Disruption|auto-trad|
|[bonus-00-jrDCPvkruz8y740Z.htm](feats/bonus-00-jrDCPvkruz8y740Z.htm)|Locate Lawbreakers|auto-trad|
|[bonus-00-Kz4E8heU12IGcYoi.htm](feats/bonus-00-Kz4E8heU12IGcYoi.htm)|Righteous Resistance|auto-trad|
|[bonus-00-PpJURSJFEHzhutdp.htm](feats/bonus-00-PpJURSJFEHzhutdp.htm)|Trailblazing Stride|auto-trad|
|[bonus-00-PPSH5vdf90KC95jJ.htm](feats/bonus-00-PPSH5vdf90KC95jJ.htm)|Reveal Beasts|auto-trad|
|[bonus-00-RkvAuikI0kIZlQTU.htm](feats/bonus-00-RkvAuikI0kIZlQTU.htm)|Disillusionment|auto-trad|
|[bonus-00-tPhhaCbaQqwenkzx.htm](feats/bonus-00-tPhhaCbaQqwenkzx.htm)|Silence Heresy|auto-trad|
|[bonus-00-UHejQS4uCNGRI45t.htm](feats/bonus-00-UHejQS4uCNGRI45t.htm)|Dedication to the Five|auto-trad|
|[bonus-00-w7dOgJvqAqyqSeSQ.htm](feats/bonus-00-w7dOgJvqAqyqSeSQ.htm)|Devil Allies|auto-trad|
|[bonus-00-XOAtP64xiSRIofsY.htm](feats/bonus-00-XOAtP64xiSRIofsY.htm)|Sturdy Bindings|auto-trad|
|[bonus-10-q8c0LINVNcJrdK91.htm](feats/bonus-10-q8c0LINVNcJrdK91.htm)|Propulsive Leap|auto-trad|
|[class-01-0BR61rW4JFOfO7T7.htm](feats/class-01-0BR61rW4JFOfO7T7.htm)|Cackle|auto-trad|
|[class-01-0PCDkVnRxVqxsp9j.htm](feats/class-01-0PCDkVnRxVqxsp9j.htm)|Energy Heart|auto-trad|
|[class-01-0yPbPVEESwB6Bdfw.htm](feats/class-01-0yPbPVEESwB6Bdfw.htm)|Magus's Analysis|auto-trad|
|[class-01-142QRyK2aPIrJu48.htm](feats/class-01-142QRyK2aPIrJu48.htm)|Holy Castigation|auto-trad|
|[class-01-1JnERVwnPtX620f2.htm](feats/class-01-1JnERVwnPtX620f2.htm)|Animal Companion (Ranger)|auto-trad|
|[class-01-1MWL2uEmyiOfYtJn.htm](feats/class-01-1MWL2uEmyiOfYtJn.htm)|Disarming Flair|auto-trad|
|[class-01-1W0a6YCyoYv8dm4e.htm](feats/class-01-1W0a6YCyoYv8dm4e.htm)|Harming Hands|auto-trad|
|[class-01-2bCbfQ0KCSSYpg2q.htm](feats/class-01-2bCbfQ0KCSSYpg2q.htm)|Unfetter Eidolon|auto-trad|
|[class-01-2c9awqDem5OLK47S.htm](feats/class-01-2c9awqDem5OLK47S.htm)|Weight of Guilt|auto-trad|
|[class-01-2Lcwqwq8CF40TYHd.htm](feats/class-01-2Lcwqwq8CF40TYHd.htm)|Dual Studies|auto-trad|
|[class-01-2xk4jdwcCfmasYfT.htm](feats/class-01-2xk4jdwcCfmasYfT.htm)|Power Attack|auto-trad|
|[class-01-3a07jkWezSuORSpS.htm](feats/class-01-3a07jkWezSuORSpS.htm)|Ki Strike|auto-trad|
|[class-01-4HgKmqTrXYZJ4fyj.htm](feats/class-01-4HgKmqTrXYZJ4fyj.htm)|Animal Companion (Druid)|auto-trad|
|[class-01-4KkzwOu2OKYLKSXQ.htm](feats/class-01-4KkzwOu2OKYLKSXQ.htm)|Variable Core|auto-trad|
|[class-01-5FyvwI24mnROzh61.htm](feats/class-01-5FyvwI24mnROzh61.htm)|Combat Assessment|auto-trad|
|[class-01-5GqjEM22n78Vmdpe.htm](feats/class-01-5GqjEM22n78Vmdpe.htm)|Focused Fascination|auto-trad|
|[class-01-5pGMffGKkBTZKvjw.htm](feats/class-01-5pGMffGKkBTZKvjw.htm)|Raging Thrower|auto-trad|
|[class-01-6GN1zh3RcnZhrzxP.htm](feats/class-01-6GN1zh3RcnZhrzxP.htm)|Everstand Stance|auto-trad|
|[class-01-6LFBPpPPJjDq07fg.htm](feats/class-01-6LFBPpPPJjDq07fg.htm)|Hit the Dirt!|auto-trad|
|[class-01-6thEFvU7aMPmLrly.htm](feats/class-01-6thEFvU7aMPmLrly.htm)|False Faith|auto-trad|
|[class-01-7FRYyKXDKjGoANYj.htm](feats/class-01-7FRYyKXDKjGoANYj.htm)|Stumbling Stance|auto-trad|
|[class-01-7y1BCJLrdk9mKXlc.htm](feats/class-01-7y1BCJLrdk9mKXlc.htm)|Leshy Familiar|auto-trad|
|[class-01-82ATEfDMPkZDxV5H.htm](feats/class-01-82ATEfDMPkZDxV5H.htm)|Blessed Blood (Sorcerer)|auto-trad|
|[class-01-8sy3sHwOHS4ImwvJ.htm](feats/class-01-8sy3sHwOHS4ImwvJ.htm)|Dragon Stance|auto-trad|
|[class-01-8tS5NzytLmgbq5hF.htm](feats/class-01-8tS5NzytLmgbq5hF.htm)|Exacting Strike|auto-trad|
|[class-01-8zHsIubGREIrGfAA.htm](feats/class-01-8zHsIubGREIrGfAA.htm)|Prototype Companion|auto-trad|
|[class-01-9bgl6qYWKHzqWZj0.htm](feats/class-01-9bgl6qYWKHzqWZj0.htm)|Flexible Studies|auto-trad|
|[class-01-9CaaU5szcf3mtJXD.htm](feats/class-01-9CaaU5szcf3mtJXD.htm)|Overextending Feint|auto-trad|
|[class-01-9oKtrUzXixj58hOg.htm](feats/class-01-9oKtrUzXixj58hOg.htm)|Psychic Rapport|auto-trad|
|[class-01-9XALeVNcmlIxf3tJ.htm](feats/class-01-9XALeVNcmlIxf3tJ.htm)|Twin Feint|auto-trad|
|[class-01-AD2eQu6SjLhUGD6Z.htm](feats/class-01-AD2eQu6SjLhUGD6Z.htm)|Eschew Materials|auto-trad|
|[class-01-AN9jY1JVcU20Qdw6.htm](feats/class-01-AN9jY1JVcU20Qdw6.htm)|Wolf Stance|auto-trad|
|[class-01-avCmIwmwJH7d7gri.htm](feats/class-01-avCmIwmwJH7d7gri.htm)|Ancestral Blood Magic|auto-trad|
|[class-01-b09B0XLNAiFP3gFT.htm](feats/class-01-b09B0XLNAiFP3gFT.htm)|Counterspell (Sorcerer)|auto-trad|
|[class-01-BBj6jrdyff7QOgjH.htm](feats/class-01-BBj6jrdyff7QOgjH.htm)|Adrenaline Rush|auto-trad|
|[class-01-bCizH4ByTwbLcYA1.htm](feats/class-01-bCizH4ByTwbLcYA1.htm)|One For All|auto-trad|
|[class-01-bcxIg7wi8ZAhvhOD.htm](feats/class-01-bcxIg7wi8ZAhvhOD.htm)|Familiar|auto-trad|
|[class-01-bf7NCeKqDClaqhTR.htm](feats/class-01-bf7NCeKqDClaqhTR.htm)|Crane Stance|auto-trad|
|[class-01-Bj0GvPgyPiC2kDH1.htm](feats/class-01-Bj0GvPgyPiC2kDH1.htm)|Glider Form|auto-trad|
|[class-01-BJHsCiBLdjgJo6zM.htm](feats/class-01-BJHsCiBLdjgJo6zM.htm)|Far Lobber|auto-trad|
|[class-01-BWomK7EVY0WXxWgh.htm](feats/class-01-BWomK7EVY0WXxWgh.htm)|Reach Spell|auto-trad|
|[class-01-c3b7DhnDBC7YEgRG.htm](feats/class-01-c3b7DhnDBC7YEgRG.htm)|Hunted Shot|auto-trad|
|[class-01-C5tSxOwDILefw4zq.htm](feats/class-01-C5tSxOwDILefw4zq.htm)|Buckler Expertise|auto-trad|
|[class-01-CpjN7v1QN8TQFcvI.htm](feats/class-01-CpjN7v1QN8TQFcvI.htm)|Crossbow Ace|auto-trad|
|[class-01-deoHKUzpzT7iwWhL.htm](feats/class-01-deoHKUzpzT7iwWhL.htm)|Counterspell (Spontaneous)|auto-trad|
|[class-01-DfLkIIg2reyYW3a8.htm](feats/class-01-DfLkIIg2reyYW3a8.htm)|Deadly Simplicity|auto-trad|
|[class-01-dFQj6gLrbQi7APiA.htm](feats/class-01-dFQj6gLrbQi7APiA.htm)|Agile Shield Grip|auto-trad|
|[class-01-dGFQvkDRmyvvf4IQ.htm](feats/class-01-dGFQvkDRmyvvf4IQ.htm)|Splinter Faith|auto-trad|
|[class-01-dNH8OHEvx3vI9NBQ.htm](feats/class-01-dNH8OHEvx3vI9NBQ.htm)|Nimble Dodge|auto-trad|
|[class-01-DqD7htz8Sd1dh3BT.htm](feats/class-01-DqD7htz8Sd1dh3BT.htm)|Gorilla Stance|auto-trad|
|[class-01-dWbISC0di0r4oPCi.htm](feats/class-01-dWbISC0di0r4oPCi.htm)|Sword and Pistol|auto-trad|
|[class-01-emRfSVvU8ZAH9UdK.htm](feats/class-01-emRfSVvU8ZAH9UdK.htm)|Dangerous Sorcery|auto-trad|
|[class-01-EOmTf95t03y4IGdp.htm](feats/class-01-EOmTf95t03y4IGdp.htm)|Draconic Arrogance|auto-trad|
|[class-01-EpBG4CFMNSZQx7vI.htm](feats/class-01-EpBG4CFMNSZQx7vI.htm)|Counterspell (Prepared)|auto-trad|
|[class-01-epfQ2PwNDLwyY31t.htm](feats/class-01-epfQ2PwNDLwyY31t.htm)|Unimpeded Step|auto-trad|
|[class-01-eww9tuiXPnZFZ3DU.htm](feats/class-01-eww9tuiXPnZFZ3DU.htm)|Plant Evidence|auto-trad|
|[class-01-f2Pl5dWEL9ZvEyI1.htm](feats/class-01-f2Pl5dWEL9ZvEyI1.htm)|Animal Companion|auto-trad|
|[class-01-FCzfh8QHMo7QJpAM.htm](feats/class-01-FCzfh8QHMo7QJpAM.htm)|Spellbook Prodigy|auto-trad|
|[class-01-FhxkU6OftQeecpQW.htm](feats/class-01-FhxkU6OftQeecpQW.htm)|Heal Companion|auto-trad|
|[class-01-FoWO4RnHRwfEIC7Q.htm](feats/class-01-FoWO4RnHRwfEIC7Q.htm)|Widen Spell|auto-trad|
|[class-01-fUR72e3t7p2IcqqG.htm](feats/class-01-fUR72e3t7p2IcqqG.htm)|Divine Disharmony|auto-trad|
|[class-01-FXKIALDXAzEBfj5A.htm](feats/class-01-FXKIALDXAzEBfj5A.htm)|Deity's Domain|auto-trad|
|[class-01-gFQFgREm3HaFx1mf.htm](feats/class-01-gFQFgREm3HaFx1mf.htm)|Built-In Tools|auto-trad|
|[class-01-gjec0ts3wkFbjvHN.htm](feats/class-01-gjec0ts3wkFbjvHN.htm)|You're Next|auto-trad|
|[class-01-GuEdTz1VMEptQnOd.htm](feats/class-01-GuEdTz1VMEptQnOd.htm)|Stoked Flame Stance|auto-trad|
|[class-01-Gw0wGXikhAhiGoud.htm](feats/class-01-Gw0wGXikhAhiGoud.htm)|Twin Takedown|auto-trad|
|[class-01-hT4INKGtly4QY8KN.htm](feats/class-01-hT4INKGtly4QY8KN.htm)|Domain Initiate|auto-trad|
|[class-01-hXYnwpi95E77qfAu.htm](feats/class-01-hXYnwpi95E77qfAu.htm)|Goading Feint|auto-trad|
|[class-01-iP01b6eyUm4m6KQp.htm](feats/class-01-iP01b6eyUm4m6KQp.htm)|Haunt Ingenuity|auto-trad|
|[class-01-iT39wlCEC1aWaSx7.htm](feats/class-01-iT39wlCEC1aWaSx7.htm)|Gravity Weapon|auto-trad|
|[class-01-iTdbkP07UFMOo1rI.htm](feats/class-01-iTdbkP07UFMOo1rI.htm)|Ranged Reprisal|auto-trad|
|[class-01-iWvpq3uDZcXvBJj8.htm](feats/class-01-iWvpq3uDZcXvBJj8.htm)|Known Weaknesses|auto-trad|
|[class-01-iX5HEqRImhKzfPR2.htm](feats/class-01-iX5HEqRImhKzfPR2.htm)|Well-Versed|auto-trad|
|[class-01-IzkL60LlKzbKIhY1.htm](feats/class-01-IzkL60LlKzbKIhY1.htm)|Cover Fire|auto-trad|
|[class-01-jBp91q4uzwd4FeSX.htm](feats/class-01-jBp91q4uzwd4FeSX.htm)|Versatile Performance|auto-trad|
|[class-01-jGTRRCqxn1FIBxE2.htm](feats/class-01-jGTRRCqxn1FIBxE2.htm)|Hymn Of Healing|auto-trad|
|[class-01-JoxEspM0kbCop7og.htm](feats/class-01-JoxEspM0kbCop7og.htm)|Raise a Tome|auto-trad|
|[class-01-KaCpXuYuho3nnDUy.htm](feats/class-01-KaCpXuYuho3nnDUy.htm)|Vicious Vengeance|auto-trad|
|[class-01-kFpVgcqREAfDmjXp.htm](feats/class-01-kFpVgcqREAfDmjXp.htm)|Advanced Weaponry|auto-trad|
|[class-01-KlqKpeq5OmTRxVHb.htm](feats/class-01-KlqKpeq5OmTRxVHb.htm)|Diverse Lore|auto-trad|
|[class-01-knZUN4sYExIyRC4F.htm](feats/class-01-knZUN4sYExIyRC4F.htm)|Reflective Ripple Stance|auto-trad|
|[class-01-kS4wxSKrZxcOvSNK.htm](feats/class-01-kS4wxSKrZxcOvSNK.htm)|Shore Step|auto-trad|
|[class-01-KYl1rWFOHe0e6VqJ.htm](feats/class-01-KYl1rWFOHe0e6VqJ.htm)|Premonition Of Avoidance|auto-trad|
|[class-01-L5nSP8CkHWSLp2vV.htm](feats/class-01-L5nSP8CkHWSLp2vV.htm)|Arcane Fists|auto-trad|
|[class-01-lFVqejlf52cdYrZy.htm](feats/class-01-lFVqejlf52cdYrZy.htm)|Munitions Crafter|auto-trad|
|[class-01-loC0wIyIrsG43Zrd.htm](feats/class-01-loC0wIyIrsG43Zrd.htm)|Alchemical Familiar|auto-trad|
|[class-01-MJW4VP7PjVAX131C.htm](feats/class-01-MJW4VP7PjVAX131C.htm)|Glean Lore|auto-trad|
|[class-01-MRMW0EiuOO20pzG2.htm](feats/class-01-MRMW0EiuOO20pzG2.htm)|Ongoing Selfishness|auto-trad|
|[class-01-mWCiu9Hl1WxajSLa.htm](feats/class-01-mWCiu9Hl1WxajSLa.htm)|Snagging Strike|auto-trad|
|[class-01-oA866uVEFu1OrAX0.htm](feats/class-01-oA866uVEFu1OrAX0.htm)|Trap Finder|auto-trad|
|[class-01-onde0SxLoxLBTnvm.htm](feats/class-01-onde0SxLoxLBTnvm.htm)|Double Slice|auto-trad|
|[class-01-oQVp2UhXVBcELma5.htm](feats/class-01-oQVp2UhXVBcELma5.htm)|Root to Life|auto-trad|
|[class-01-pD1oDbUDkNtHadGY.htm](feats/class-01-pD1oDbUDkNtHadGY.htm)|Hand of the Apprentice|auto-trad|
|[class-01-PGi1EtcZMolnNA1M.htm](feats/class-01-PGi1EtcZMolnNA1M.htm)|Ancestral Mind|auto-trad|
|[class-01-pgIrr8xrlCXbRAeo.htm](feats/class-01-pgIrr8xrlCXbRAeo.htm)|Underworld Investigator|auto-trad|
|[class-01-pIHjH1x0AVtiX5Tv.htm](feats/class-01-pIHjH1x0AVtiX5Tv.htm)|That's Odd|auto-trad|
|[class-01-PqImBJ2JrPo5oFxc.htm](feats/class-01-PqImBJ2JrPo5oFxc.htm)|Vile Desecration|auto-trad|
|[class-01-q1iP3SjAF5uceI0M.htm](feats/class-01-q1iP3SjAF5uceI0M.htm)|Martial Performance|auto-trad|
|[class-01-QEOe9AhQzdqIx1ei.htm](feats/class-01-QEOe9AhQzdqIx1ei.htm)|Steadying Stone|auto-trad|
|[class-01-qgNc5XwjsaWET0Op.htm](feats/class-01-qgNc5XwjsaWET0Op.htm)|Flying Blade|auto-trad|
|[class-01-QKC9iiR4Epj1Lyc7.htm](feats/class-01-QKC9iiR4Epj1Lyc7.htm)|Counter Thought|auto-trad|
|[class-01-qPFWEyihvbWsCcUv.htm](feats/class-01-qPFWEyihvbWsCcUv.htm)|Verdant Weapon|auto-trad|
|[class-01-qQt3CMrhLkUV1wCv.htm](feats/class-01-qQt3CMrhLkUV1wCv.htm)|Sudden Charge|auto-trad|
|[class-01-QZ00D2xdJnbWFzml.htm](feats/class-01-QZ00D2xdJnbWFzml.htm)|Subtle Delivery|auto-trad|
|[class-01-rbiMK71SvGZGRLJ1.htm](feats/class-01-rbiMK71SvGZGRLJ1.htm)|Rain of Embers Stance|auto-trad|
|[class-01-rPzEnABFjkbOARiB.htm](feats/class-01-rPzEnABFjkbOARiB.htm)|Expanded Senses|auto-trad|
|[class-01-s6h0xkdKf3gecLk6.htm](feats/class-01-s6h0xkdKf3gecLk6.htm)|Crossbow Crack Shot|auto-trad|
|[class-01-s97nKj9Uye1KXr7A.htm](feats/class-01-s97nKj9Uye1KXr7A.htm)|Meld into Eidolon|auto-trad|
|[class-01-sf73K6f8xlfhHS1n.htm](feats/class-01-sf73K6f8xlfhHS1n.htm)|Scroll Thaumaturgy|auto-trad|
|[class-01-sjChYEuEWPqndCSK.htm](feats/class-01-sjChYEuEWPqndCSK.htm)|Dual-Weapon Reload|auto-trad|
|[class-01-sVjATEo8eqkAosNp.htm](feats/class-01-sVjATEo8eqkAosNp.htm)|Lingering Composition|auto-trad|
|[class-01-tn1PajS2DrHeBlGg.htm](feats/class-01-tn1PajS2DrHeBlGg.htm)|Ammunition Thaumaturgy|auto-trad|
|[class-01-u5cPwhuCd3xTAlWl.htm](feats/class-01-u5cPwhuCd3xTAlWl.htm)|Extend Boost|auto-trad|
|[class-01-UpEjRfQkCJCruAfb.htm](feats/class-01-UpEjRfQkCJCruAfb.htm)|Storm Born|auto-trad|
|[class-01-UtRW3hYBTFG8HQLh.htm](feats/class-01-UtRW3hYBTFG8HQLh.htm)|Mental Buffer|auto-trad|
|[class-01-UVsMJwHpQHjVZLTK.htm](feats/class-01-UVsMJwHpQHjVZLTK.htm)|Explosive Leap|auto-trad|
|[class-01-uVXEZblPRuCyPRua.htm](feats/class-01-uVXEZblPRuCyPRua.htm)|Bardic Lore|auto-trad|
|[class-01-VaIHQzOE5ibmbtqU.htm](feats/class-01-VaIHQzOE5ibmbtqU.htm)|Wild Shape|auto-trad|
|[class-01-vRXPsMbyuvoPvbAQ.htm](feats/class-01-vRXPsMbyuvoPvbAQ.htm)|Moment of Clarity|auto-trad|
|[class-01-w8Ycgeq2zfyshtoS.htm](feats/class-01-w8Ycgeq2zfyshtoS.htm)|Reactive Shield|auto-trad|
|[class-01-WcY7H7mRiK2VDquV.htm](feats/class-01-WcY7H7mRiK2VDquV.htm)|Dueling Parry (Swashbuckler)|auto-trad|
|[class-01-WYaKRREZUSH0jel5.htm](feats/class-01-WYaKRREZUSH0jel5.htm)|Desperate Prayer|auto-trad|
|[class-01-xbg1scOIT7RI9Fij.htm](feats/class-01-xbg1scOIT7RI9Fij.htm)|Quick Bomber|auto-trad|
|[class-01-XHy9inDi3oGZNd43.htm](feats/class-01-XHy9inDi3oGZNd43.htm)|No! No! I Created You!|auto-trad|
|[class-01-XLoRtMn48f2KKvsu.htm](feats/class-01-XLoRtMn48f2KKvsu.htm)|Blast Lock|auto-trad|
|[class-01-XseJI9XhKNtZN8pv.htm](feats/class-01-XseJI9XhKNtZN8pv.htm)|Raging Intimidation|auto-trad|
|[class-01-XttSGDuAsRDTuvgS.htm](feats/class-01-XttSGDuAsRDTuvgS.htm)|Iron Repercussions|auto-trad|
|[class-01-yA9tsWT9uzL2REnw.htm](feats/class-01-yA9tsWT9uzL2REnw.htm)|Ki Rush|auto-trad|
|[class-01-YdpGPLN0QFTZIhbk.htm](feats/class-01-YdpGPLN0QFTZIhbk.htm)|Acute Vision|auto-trad|
|[class-01-YG2RxXE9SMfwo6wP.htm](feats/class-01-YG2RxXE9SMfwo6wP.htm)|Monastic Archer Stance|auto-trad|
|[class-01-YKqMuuC8j35NFh92.htm](feats/class-01-YKqMuuC8j35NFh92.htm)|Tumble Behind (Rogue)|auto-trad|
|[class-01-Yl2wYv24v5kw95aS.htm](feats/class-01-Yl2wYv24v5kw95aS.htm)|Point-Blank Shot|auto-trad|
|[class-01-yOloZIGkulrrPYG4.htm](feats/class-01-yOloZIGkulrrPYG4.htm)|Fire Lung|auto-trad|
|[class-01-ypir96PNY6LBSIWT.htm](feats/class-01-ypir96PNY6LBSIWT.htm)|Alchemical Savant|auto-trad|
|[class-01-yPWNbTqOIKdkwaVq.htm](feats/class-01-yPWNbTqOIKdkwaVq.htm)|Takedown Expert|auto-trad|
|[class-01-YTTJqRKH8QZl6al2.htm](feats/class-01-YTTJqRKH8QZl6al2.htm)|Monster Hunter|auto-trad|
|[class-01-yVC5pVZaWczYWGTa.htm](feats/class-01-yVC5pVZaWczYWGTa.htm)|Syncretism|auto-trad|
|[class-01-YW4FdWgzTwRFneFF.htm](feats/class-01-YW4FdWgzTwRFneFF.htm)|Monastic Weaponry|auto-trad|
|[class-01-ZL5UU9quCTvcWzfY.htm](feats/class-01-ZL5UU9quCTvcWzfY.htm)|Mountain Stance|auto-trad|
|[class-01-zUtdBd3IbM7UX0AD.htm](feats/class-01-zUtdBd3IbM7UX0AD.htm)|Cauldron|auto-trad|
|[class-01-zxaE7C1NCGqZR8aU.htm](feats/class-01-zxaE7C1NCGqZR8aU.htm)|Haphazard Repair|auto-trad|
|[class-01-zY6y802bOouMYYFV.htm](feats/class-01-zY6y802bOouMYYFV.htm)|Wortwitch|auto-trad|
|[class-01-zYhcEX4JnrZ08HfV.htm](feats/class-01-zYhcEX4JnrZ08HfV.htm)|Healing Hands|auto-trad|
|[class-02-0Bm4WMi9u0EOHbkp.htm](feats/class-02-0Bm4WMi9u0EOHbkp.htm)|Resilient Mind|auto-trad|
|[class-02-0lieFKdwgNVXep7u.htm](feats/class-02-0lieFKdwgNVXep7u.htm)|Poison Resistance|auto-trad|
|[class-02-0NRIP8cDv9Opl5Ls.htm](feats/class-02-0NRIP8cDv9Opl5Ls.htm)|Fiendsbane Oath (Paladin)|auto-trad|
|[class-02-17jjQPpnBkHRkzPE.htm](feats/class-02-17jjQPpnBkHRkzPE.htm)|Turn Away Misfortune|auto-trad|
|[class-02-19OLlTrxvEtDAOHc.htm](feats/class-02-19OLlTrxvEtDAOHc.htm)|Aggressive Block|auto-trad|
|[class-02-2AeGnjk4EAqFDvK8.htm](feats/class-02-2AeGnjk4EAqFDvK8.htm)|Spirit Sheath|auto-trad|
|[class-02-2rJP5TqvfazNeNmY.htm](feats/class-02-2rJP5TqvfazNeNmY.htm)|Revivifying Mutagen|auto-trad|
|[class-02-2yq3l8e2Vzz34yRV.htm](feats/class-02-2yq3l8e2Vzz34yRV.htm)|Monster Warden|auto-trad|
|[class-02-38L5yCVNgRAFFMsZ.htm](feats/class-02-38L5yCVNgRAFFMsZ.htm)|Cantrip Expansion|auto-trad|
|[class-02-3kH0fGOIoYvPNQsq.htm](feats/class-02-3kH0fGOIoYvPNQsq.htm)|After You|auto-trad|
|[class-02-4bvoePh1p3ZGgqhP.htm](feats/class-02-4bvoePh1p3ZGgqhP.htm)|Second Wind|auto-trad|
|[class-02-4f65nrWVzTG8Njj8.htm](feats/class-02-4f65nrWVzTG8Njj8.htm)|Unbalancing Blow|auto-trad|
|[class-02-4HZTLPKPteEFsa7n.htm](feats/class-02-4HZTLPKPteEFsa7n.htm)|Esoteric Polymath|auto-trad|
|[class-02-54BhwBHyp2jp3e26.htm](feats/class-02-54BhwBHyp2jp3e26.htm)|Wild Empathy|auto-trad|
|[class-02-5gnBhockV7O32jTR.htm](feats/class-02-5gnBhockV7O32jTR.htm)|Furious Finish|auto-trad|
|[class-02-5SBFayX7JqKYANwa.htm](feats/class-02-5SBFayX7JqKYANwa.htm)|Rebounding Toss|auto-trad|
|[class-02-6cQSPqXoAO6oJl0i.htm](feats/class-02-6cQSPqXoAO6oJl0i.htm)|Shooting Stars Stance|auto-trad|
|[class-02-6QOcQ8ooP1vjQACX.htm](feats/class-02-6QOcQ8ooP1vjQACX.htm)|Nonlethal Spell|auto-trad|
|[class-02-7RFjTTzznsdPcaYB.htm](feats/class-02-7RFjTTzznsdPcaYB.htm)|Emblazon Armament|auto-trad|
|[class-02-84n92oBsRVCxjifM.htm](feats/class-02-84n92oBsRVCxjifM.htm)|Expansive Spellstrike|auto-trad|
|[class-02-85eUthbjE9TnF4fo.htm](feats/class-02-85eUthbjE9TnF4fo.htm)|Dragonslayer Oath (Redeemer)|auto-trad|
|[class-02-8bMmGBO6gsMfWZk3.htm](feats/class-02-8bMmGBO6gsMfWZk3.htm)|Stunning Fist|auto-trad|
|[class-02-9Jbl71C5C6MnOqxV.htm](feats/class-02-9Jbl71C5C6MnOqxV.htm)|Consume Energy|auto-trad|
|[class-02-9onf3uGvZRnNLPR6.htm](feats/class-02-9onf3uGvZRnNLPR6.htm)|Order Explorer|auto-trad|
|[class-02-9WzZc110jCNnjjRz.htm](feats/class-02-9WzZc110jCNnjjRz.htm)|Lightslayer Oath|auto-trad|
|[class-02-A2EBy2L4acrehiAA.htm](feats/class-02-A2EBy2L4acrehiAA.htm)|Pistol Twirl|auto-trad|
|[class-02-A4sV0cRU9I8ztbHY.htm](feats/class-02-A4sV0cRU9I8ztbHY.htm)|Bashing Charge|auto-trad|
|[class-02-A7ofsoPva0UtjqrX.htm](feats/class-02-A7ofsoPva0UtjqrX.htm)|Cantrip Expansion (Prepared Caster)|auto-trad|
|[class-02-a898miJnjgD93ZsX.htm](feats/class-02-a898miJnjgD93ZsX.htm)|Multifarious Muse|auto-trad|
|[class-02-auv1lss6LxM0q3gz.htm](feats/class-02-auv1lss6LxM0q3gz.htm)|Shake it Off|auto-trad|
|[class-02-aXdY2wgn0ItWwTr0.htm](feats/class-02-aXdY2wgn0ItWwTr0.htm)|Conceited Mindset|auto-trad|
|[class-02-BmAk6o14CutgnIOG.htm](feats/class-02-BmAk6o14CutgnIOG.htm)|Risky Reload|auto-trad|
|[class-02-bPMYOiiqhrb098s6.htm](feats/class-02-bPMYOiiqhrb098s6.htm)|Call Implement|auto-trad|
|[class-02-bRftzbFvSF1pilIo.htm](feats/class-02-bRftzbFvSF1pilIo.htm)|Anoint Ally|auto-trad|
|[class-02-BU4NBIBkVZxdWLLH.htm](feats/class-02-BU4NBIBkVZxdWLLH.htm)|Elemental Fist|auto-trad|
|[class-02-c2h9Z8exSFhraJ8j.htm](feats/class-02-c2h9Z8exSFhraJ8j.htm)|Esoteric Oath (Paladin)|auto-trad|
|[class-02-C3ukUk4Js10o5IBA.htm](feats/class-02-C3ukUk4Js10o5IBA.htm)|Ghostly Grasp (Deviant)|auto-trad|
|[class-02-CcQISNjNdD0Fsj9n.htm](feats/class-02-CcQISNjNdD0Fsj9n.htm)|Shining Oath (Paladin)|auto-trad|
|[class-02-corjPSTUKB02gkqN.htm](feats/class-02-corjPSTUKB02gkqN.htm)|Defensive Armaments|auto-trad|
|[class-02-cT18roxM5P5MWDRT.htm](feats/class-02-cT18roxM5P5MWDRT.htm)|Reliable Squire|auto-trad|
|[class-02-cy9jqlHz75GTjg7l.htm](feats/class-02-cy9jqlHz75GTjg7l.htm)|Bone Spikes|auto-trad|
|[class-02-D1o7GUraoFFzjaub.htm](feats/class-02-D1o7GUraoFFzjaub.htm)|Clever Gambit|auto-trad|
|[class-02-dF1HR6TqyMz03o6F.htm](feats/class-02-dF1HR6TqyMz03o6F.htm)|Combat Grab|auto-trad|
|[class-02-DFusBl7CyNkuDTRa.htm](feats/class-02-DFusBl7CyNkuDTRa.htm)|Collapse Armor|auto-trad|
|[class-02-dgmjiToPHC3Yf5I3.htm](feats/class-02-dgmjiToPHC3Yf5I3.htm)|Acute Scent|auto-trad|
|[class-02-DkoxNw9tsFFXrfJY.htm](feats/class-02-DkoxNw9tsFFXrfJY.htm)|Charmed Life|auto-trad|
|[class-02-DxcjEcxTbjlOA2C0.htm](feats/class-02-DxcjEcxTbjlOA2C0.htm)|Reinforce Eidolon|auto-trad|
|[class-02-DY6pNO3GzHeKSxmQ.htm](feats/class-02-DY6pNO3GzHeKSxmQ.htm)|Finishing Follow-through|auto-trad|
|[class-02-DYUlMxh7TDgEh9xB.htm](feats/class-02-DYUlMxh7TDgEh9xB.htm)|Brutal Beating|auto-trad|
|[class-02-e9NcDk6ds5YebGvb.htm](feats/class-02-e9NcDk6ds5YebGvb.htm)|Relentless Stalker|auto-trad|
|[class-02-EaIczkGVI5DUo3c9.htm](feats/class-02-EaIczkGVI5DUo3c9.htm)|Entreat With Forebears|auto-trad|
|[class-02-eCzIiTjI4mQFYe9D.htm](feats/class-02-eCzIiTjI4mQFYe9D.htm)|Steed Form|auto-trad|
|[class-02-eJPu2Sj7XYCM0h0R.htm](feats/class-02-eJPu2Sj7XYCM0h0R.htm)|Warp Space|auto-trad|
|[class-02-eoDDjIGAA67Z7rQt.htm](feats/class-02-eoDDjIGAA67Z7rQt.htm)|Underhanded Assault|auto-trad|
|[class-02-eUXgY8W3fShlW7pd.htm](feats/class-02-eUXgY8W3fShlW7pd.htm)|Inspire Competence|auto-trad|
|[class-02-FkN9QX1W2Iv56bkn.htm](feats/class-02-FkN9QX1W2Iv56bkn.htm)|Song Of Strength|auto-trad|
|[class-02-FrlhErsjR6fEn6kX.htm](feats/class-02-FrlhErsjR6fEn6kX.htm)|Minor Magic|auto-trad|
|[class-02-Fs9ZMHZNCBo7B5Zc.htm](feats/class-02-Fs9ZMHZNCBo7B5Zc.htm)|Vengeful Oath|auto-trad|
|[class-02-fU7d5P6WrfAirgip.htm](feats/class-02-fU7d5P6WrfAirgip.htm)|Tumble Behind (Swashbuckler)|auto-trad|
|[class-02-GFtNQvpzuqtsdOTG.htm](feats/class-02-GFtNQvpzuqtsdOTG.htm)|Hunter's Aim|auto-trad|
|[class-02-gKoNWXem1ikEqE2d.htm](feats/class-02-gKoNWXem1ikEqE2d.htm)|Familiar's Language|auto-trad|
|[class-02-gt9pvIicabSOe6pB.htm](feats/class-02-gt9pvIicabSOe6pB.htm)|Brutish Shove|auto-trad|
|[class-02-hGjmbTNBUiJqYvsE.htm](feats/class-02-hGjmbTNBUiJqYvsE.htm)|Crushing Grab|auto-trad|
|[class-02-hLYoIjH8gPEVXyWG.htm](feats/class-02-hLYoIjH8gPEVXyWG.htm)|Distracting Feint|auto-trad|
|[class-02-I2DGiMcof703Cnjc.htm](feats/class-02-I2DGiMcof703Cnjc.htm)|Cantrip Casting|auto-trad|
|[class-02-iS4Vc2zv7vgL5mnX.htm](feats/class-02-iS4Vc2zv7vgL5mnX.htm)|Energy Ablation|auto-trad|
|[class-02-J3R0vx1lszU3CLp5.htm](feats/class-02-J3R0vx1lszU3CLp5.htm)|Fiendsbane Oath (Redeemer)|auto-trad|
|[class-02-jPWF8Xe0UqVvbeyv.htm](feats/class-02-jPWF8Xe0UqVvbeyv.htm)|Eerie Flicker|auto-trad|
|[class-02-K3QkcNWY8qpNEJrk.htm](feats/class-02-K3QkcNWY8qpNEJrk.htm)|Dragonslayer Oath (Liberator)|auto-trad|
|[class-02-KCwXj3y7Nm4e3NbI.htm](feats/class-02-KCwXj3y7Nm4e3NbI.htm)|Demolition Charge|auto-trad|
|[class-02-lDfOzhKJoTCkLPtn.htm](feats/class-02-lDfOzhKJoTCkLPtn.htm)|Shining Oath (Liberator)|auto-trad|
|[class-02-lIrPwGpJk9TldZ4c.htm](feats/class-02-lIrPwGpJk9TldZ4c.htm)|Snare Hopping|auto-trad|
|[class-02-lkcM4V3VDAtjlR9P.htm](feats/class-02-lkcM4V3VDAtjlR9P.htm)|Intimidating Strike|auto-trad|
|[class-02-lT8XlX1Ig900BblS.htm](feats/class-02-lT8XlX1Ig900BblS.htm)|No Escape|auto-trad|
|[class-02-LZsTt6zPZfNyjIZl.htm](feats/class-02-LZsTt6zPZfNyjIZl.htm)|Dragonslayer Oath (Paladin)|auto-trad|
|[class-02-mzpLIuRQ81DCYdKU.htm](feats/class-02-mzpLIuRQ81DCYdKU.htm)|Force Fang|auto-trad|
|[class-02-N4wCUZH2KG6FoGqh.htm](feats/class-02-N4wCUZH2KG6FoGqh.htm)|Shared Stratagem|auto-trad|
|[class-02-N7dTFxpjXGn4ddq8.htm](feats/class-02-N7dTFxpjXGn4ddq8.htm)|Enhanced Familiar|auto-trad|
|[class-02-n7nQQR940OvFbw7T.htm](feats/class-02-n7nQQR940OvFbw7T.htm)|Dueling Parry (Fighter)|auto-trad|
|[class-02-Na1qfHd6AFAXoN1A.htm](feats/class-02-Na1qfHd6AFAXoN1A.htm)|Reverse Engineer|auto-trad|
|[class-02-OcBaEnGdDm6CuSnr.htm](feats/class-02-OcBaEnGdDm6CuSnr.htm)|Rapid Response|auto-trad|
|[class-02-ODnXQHvFoK7tIVpu.htm](feats/class-02-ODnXQHvFoK7tIVpu.htm)|Smoke Bomb|auto-trad|
|[class-02-OiY0L3WvjwlQQ4iG.htm](feats/class-02-OiY0L3WvjwlQQ4iG.htm)|Strong Arm|auto-trad|
|[class-02-ot0uyFtnC1Whz5bp.htm](feats/class-02-ot0uyFtnC1Whz5bp.htm)|Directed Audience|auto-trad|
|[class-02-oVSlTmdmho8ZQo2k.htm](feats/class-02-oVSlTmdmho8ZQo2k.htm)|Ranged Combatant|auto-trad|
|[class-02-OWhTAowdMvBZnCrT.htm](feats/class-02-OWhTAowdMvBZnCrT.htm)|Dancing Leaf|auto-trad|
|[class-02-P6cTyDMTXcC8HBDr.htm](feats/class-02-P6cTyDMTXcC8HBDr.htm)|Esoteric Warden|auto-trad|
|[class-02-pEFcWRYiWLSjxvkW.htm](feats/class-02-pEFcWRYiWLSjxvkW.htm)|Mental Balm|auto-trad|
|[class-02-PGyzFBwuTgypU8cD.htm](feats/class-02-PGyzFBwuTgypU8cD.htm)|Turn Undead|auto-trad|
|[class-02-QFFbmQ5yrBSjbAj3.htm](feats/class-02-QFFbmQ5yrBSjbAj3.htm)|Esoteric Oath (Redeemer)|auto-trad|
|[class-02-qFt6zyWVX1njJf1l.htm](feats/class-02-qFt6zyWVX1njJf1l.htm)|Quick Draw|auto-trad|
|[class-02-QkP007ESTWtw7UQG.htm](feats/class-02-QkP007ESTWtw7UQG.htm)|Cantrip Expansion (Spontaneous Caster)|auto-trad|
|[class-02-qmFWCHOuubEl7VpX.htm](feats/class-02-qmFWCHOuubEl7VpX.htm)|Domain Acumen|auto-trad|
|[class-02-QN7y2CQeiJ2iPioM.htm](feats/class-02-QN7y2CQeiJ2iPioM.htm)|Titan Swing|auto-trad|
|[class-02-RAymYRO2SLNNzKVk.htm](feats/class-02-RAymYRO2SLNNzKVk.htm)|Mobility|auto-trad|
|[class-02-rTkr1EqpAN6YtnAh.htm](feats/class-02-rTkr1EqpAN6YtnAh.htm)|Lunge|auto-trad|
|[class-02-ryBj7phZqASQSEjV.htm](feats/class-02-ryBj7phZqASQSEjV.htm)|Psi Burst|auto-trad|
|[class-02-sgo7J9BVofBqwlsF.htm](feats/class-02-sgo7J9BVofBqwlsF.htm)|Favored Terrain|auto-trad|
|[class-02-sIeuPW0j39fTZm08.htm](feats/class-02-sIeuPW0j39fTZm08.htm)|Conceal Spell|auto-trad|
|[class-02-Stydu9VtrhQZFZxt.htm](feats/class-02-Stydu9VtrhQZFZxt.htm)|Fake Out|auto-trad|
|[class-02-t2Uvf6W8Z116DYvo.htm](feats/class-02-t2Uvf6W8Z116DYvo.htm)|Cantrip Expansion (Magus)|auto-trad|
|[class-02-tdtCwmYoBMKzfhEp.htm](feats/class-02-tdtCwmYoBMKzfhEp.htm)|Shining Oath (Redeemer)|auto-trad|
|[class-02-TiNlehXIDEnIl95M.htm](feats/class-02-TiNlehXIDEnIl95M.htm)|Sap Life|auto-trad|
|[class-02-tJduF6N83l5khRow.htm](feats/class-02-tJduF6N83l5khRow.htm)|Divine Grace|auto-trad|
|[class-02-Tu1hOEr6Ko9Df54L.htm](feats/class-02-Tu1hOEr6Ko9Df54L.htm)|Athletic Strategist|auto-trad|
|[class-02-U6lS758rtYGR6aw9.htm](feats/class-02-U6lS758rtYGR6aw9.htm)|Amphibious Form|auto-trad|
|[class-02-UiQbjeqBUFjUtgUR.htm](feats/class-02-UiQbjeqBUFjUtgUR.htm)|Assisting Shot|auto-trad|
|[class-02-uv235v6hXCITAFej.htm](feats/class-02-uv235v6hXCITAFej.htm)|Fiendsbane Oath (Liberator)|auto-trad|
|[class-02-Veaf8vm2M9w8bcBI.htm](feats/class-02-Veaf8vm2M9w8bcBI.htm)|Alacritous Action|auto-trad|
|[class-02-VeVHWTrzE3aDm3rx.htm](feats/class-02-VeVHWTrzE3aDm3rx.htm)|Brawling Focus|auto-trad|
|[class-02-VQz5VypVRLCloapa.htm](feats/class-02-VQz5VypVRLCloapa.htm)|Ancestral Weaponry|auto-trad|
|[class-02-VToVEOxiyy53AcEp.htm](feats/class-02-VToVEOxiyy53AcEp.htm)|Loremaster's Etude|auto-trad|
|[class-02-VU7aZC7L08Mk1GVA.htm](feats/class-02-VU7aZC7L08Mk1GVA.htm)|Esoteric Oath (Liberator)|auto-trad|
|[class-02-VXA50vhIRCBt4vvP.htm](feats/class-02-VXA50vhIRCBt4vvP.htm)|Unbalancing Finisher|auto-trad|
|[class-02-WfbmFsRxbVyzMmCz.htm](feats/class-02-WfbmFsRxbVyzMmCz.htm)|Magical Understudy|auto-trad|
|[class-02-wKFQreilUASJkKzV.htm](feats/class-02-wKFQreilUASJkKzV.htm)|Collapse Construct|auto-trad|
|[class-02-wNr02jsG5nRF23YO.htm](feats/class-02-wNr02jsG5nRF23YO.htm)|Red Herring|auto-trad|
|[class-02-wsq8nncD25Q1fRn2.htm](feats/class-02-wsq8nncD25Q1fRn2.htm)|Basic Lesson|auto-trad|
|[class-02-Wx12NUjqTOjFrEoW.htm](feats/class-02-Wx12NUjqTOjFrEoW.htm)|Antagonize|auto-trad|
|[class-02-WZWSaAwuDgne7Z0c.htm](feats/class-02-WZWSaAwuDgne7Z0c.htm)|Solid Lead|auto-trad|
|[class-02-X9AkydrMdFwg7qIn.htm](feats/class-02-X9AkydrMdFwg7qIn.htm)|Spell Parry|auto-trad|
|[class-02-XHaxSBOaFMnBbBKt.htm](feats/class-02-XHaxSBOaFMnBbBKt.htm)|Living Hair|auto-trad|
|[class-02-xoIxiRtBVHV27Rvd.htm](feats/class-02-xoIxiRtBVHV27Rvd.htm)|Divine Aegis|auto-trad|
|[class-02-XQEoKoFtq8n3wgA3.htm](feats/class-02-XQEoKoFtq8n3wgA3.htm)|Warning Shot|auto-trad|
|[class-02-y8VecqdECqyH1h6o.htm](feats/class-02-y8VecqdECqyH1h6o.htm)|Magic Hide|auto-trad|
|[class-02-yAgFDUU8HfVK4KTy.htm](feats/class-02-yAgFDUU8HfVK4KTy.htm)|Dragging Strike|auto-trad|
|[class-02-yExxOkHN1PN37hUa.htm](feats/class-02-yExxOkHN1PN37hUa.htm)|Communal Healing|auto-trad|
|[class-02-ygCLN0brunmBYtJR.htm](feats/class-02-ygCLN0brunmBYtJR.htm)|Talisman Esoterica|auto-trad|
|[class-02-YwVdaszwpDJd6kf9.htm](feats/class-02-YwVdaszwpDJd6kf9.htm)|Devoted Guardian|auto-trad|
|[class-02-yYLVGhedXD7lFQMn.htm](feats/class-02-yYLVGhedXD7lFQMn.htm)|United Assault|auto-trad|
|[class-02-yZ7EcM9CLddZz8Hl.htm](feats/class-02-yZ7EcM9CLddZz8Hl.htm)|Versatile Font|auto-trad|
|[class-02-ZvPgibovxwiN8Wse.htm](feats/class-02-ZvPgibovxwiN8Wse.htm)|Call of the Wild|auto-trad|
|[class-04-00OnDt8UEMwfoYWH.htm](feats/class-04-00OnDt8UEMwfoYWH.htm)|Ghost Wrangler|auto-trad|
|[class-04-011wnYpjIwCEzFtl.htm](feats/class-04-011wnYpjIwCEzFtl.htm)|Elemental Summons|auto-trad|
|[class-04-0FNLI8APwj9NsBDa.htm](feats/class-04-0FNLI8APwj9NsBDa.htm)|Alchemical Discoveries|auto-trad|
|[class-04-0haS0qXR9xTYKoTG.htm](feats/class-04-0haS0qXR9xTYKoTG.htm)|Snare Specialist|auto-trad|
|[class-04-2pglnWX8q5p3XcqR.htm](feats/class-04-2pglnWX8q5p3XcqR.htm)|Syu Tak-nwa's Skillful Tresses|auto-trad|
|[class-04-2qR4QAgJVArv63Z2.htm](feats/class-04-2qR4QAgJVArv63Z2.htm)|Prayer-Touched Weapon|auto-trad|
|[class-04-2VKV7jLRTxWyVjGa.htm](feats/class-04-2VKV7jLRTxWyVjGa.htm)|Improved Familiar (Witch)|auto-trad|
|[class-04-39CqlOzlHjEhh0E4.htm](feats/class-04-39CqlOzlHjEhh0E4.htm)|Knockdown|auto-trad|
|[class-04-3EtlXNK4vc44R3Gm.htm](feats/class-04-3EtlXNK4vc44R3Gm.htm)|Reactive Dismissal|auto-trad|
|[class-04-3R9l2t1ycN8iwmdU.htm](feats/class-04-3R9l2t1ycN8iwmdU.htm)|Student of the Staff|auto-trad|
|[class-04-3y459uK2qfWtS9q4.htm](feats/class-04-3y459uK2qfWtS9q4.htm)|Everstand Strike|auto-trad|
|[class-04-4L5pj2W7Zyf8B3kg.htm](feats/class-04-4L5pj2W7Zyf8B3kg.htm)|Diving Armor|auto-trad|
|[class-04-4PvmSnyp3URIqJUM.htm](feats/class-04-4PvmSnyp3URIqJUM.htm)|Parting Shot|auto-trad|
|[class-04-5cYFHKQK6OZCwavI.htm](feats/class-04-5cYFHKQK6OZCwavI.htm)|Detective's Readiness|auto-trad|
|[class-04-5EzJVhiHQvr3v72n.htm](feats/class-04-5EzJVhiHQvr3v72n.htm)|Breached Defenses|auto-trad|
|[class-04-6erzXaxxvcXYnL9H.htm](feats/class-04-6erzXaxxvcXYnL9H.htm)|Necrotic Infusion|auto-trad|
|[class-04-6oObLoUn3MjmwbaW.htm](feats/class-04-6oObLoUn3MjmwbaW.htm)|Cryptic Spell|auto-trad|
|[class-04-6TlBZSPr18Y8WiNk.htm](feats/class-04-6TlBZSPr18Y8WiNk.htm)|Instructive Strike|auto-trad|
|[class-04-8EbIznFgkG7PHqlE.htm](feats/class-04-8EbIznFgkG7PHqlE.htm)|Divine Access|auto-trad|
|[class-04-9j90iE61ZToFR8cu.htm](feats/class-04-9j90iE61ZToFR8cu.htm)|Call Bonded Item|auto-trad|
|[class-04-9SYnbjFgyucjhN5e.htm](feats/class-04-9SYnbjFgyucjhN5e.htm)|Dread Striker|auto-trad|
|[class-04-AbsqV1P8OAhChcRl.htm](feats/class-04-AbsqV1P8OAhChcRl.htm)|Inspiring Resilience|auto-trad|
|[class-04-agfosPInBLQXNQfa.htm](feats/class-04-agfosPInBLQXNQfa.htm)|Head Stomp|auto-trad|
|[class-04-aiHbS8FGNYAQBF62.htm](feats/class-04-aiHbS8FGNYAQBF62.htm)|Accelerating Touch|auto-trad|
|[class-04-AkV4Jyllo6nlK2Sl.htm](feats/class-04-AkV4Jyllo6nlK2Sl.htm)|Cobra Stance|auto-trad|
|[class-04-ALbosSUygdq4T1Yk.htm](feats/class-04-ALbosSUygdq4T1Yk.htm)|Poison Weapon|auto-trad|
|[class-04-amPQHO9O86G6AC4P.htm](feats/class-04-amPQHO9O86G6AC4P.htm)|Devastating Spellstrike|auto-trad|
|[class-04-B0T6p3kcrOfSvLqQ.htm](feats/class-04-B0T6p3kcrOfSvLqQ.htm)|Triple Time|auto-trad|
|[class-04-B5f7eCAC3ZEmlR9h.htm](feats/class-04-B5f7eCAC3ZEmlR9h.htm)|Basic Spellcasting|auto-trad|
|[class-04-B6jXVgfPuPWWLx2K.htm](feats/class-04-B6jXVgfPuPWWLx2K.htm)|Battle Assessment|auto-trad|
|[class-04-baz18CdB13DVMHV9.htm](feats/class-04-baz18CdB13DVMHV9.htm)|Leshy Familiar Secrets|auto-trad|
|[class-04-BEqXsP6UqARzpEFD.htm](feats/class-04-BEqXsP6UqARzpEFD.htm)|Megaton Strike|auto-trad|
|[class-04-bLj5ufeOdVWDx8Aw.htm](feats/class-04-bLj5ufeOdVWDx8Aw.htm)|Starlit Eyes|auto-trad|
|[class-04-C3MgEkPNaIhTddbr.htm](feats/class-04-C3MgEkPNaIhTddbr.htm)|Peafowl Stance|auto-trad|
|[class-04-c7jNms3ZQ8eaMUqv.htm](feats/class-04-c7jNms3ZQ8eaMUqv.htm)|Efficient Alchemy (Alchemist)|auto-trad|
|[class-04-cErltcAC7OVnIyO1.htm](feats/class-04-cErltcAC7OVnIyO1.htm)|Predictable!|auto-trad|
|[class-04-cEu8BUS41dlPyPGW.htm](feats/class-04-cEu8BUS41dlPyPGW.htm)|Spiritual Guides|auto-trad|
|[class-04-CLKlavik0540j5bl.htm](feats/class-04-CLKlavik0540j5bl.htm)|Wounded Rage|auto-trad|
|[class-04-cTR67ZKDD1EKntXw.htm](feats/class-04-cTR67ZKDD1EKntXw.htm)|Advanced Construct Companion|auto-trad|
|[class-04-cxwDj2gZ7kJdP4hs.htm](feats/class-04-cxwDj2gZ7kJdP4hs.htm)|Thousand Faces|auto-trad|
|[class-04-dC14a0DZqDBA9B8g.htm](feats/class-04-dC14a0DZqDBA9B8g.htm)|Instant Backup|auto-trad|
|[class-04-douVMHDuQQv8U8aq.htm](feats/class-04-douVMHDuQQv8U8aq.htm)|Lingering Chill|auto-trad|
|[class-04-DQN7YC7s7T0pL6Aa.htm](feats/class-04-DQN7YC7s7T0pL6Aa.htm)|Gadget Specialist|auto-trad|
|[class-04-DS0XlHfi3ztb3ET7.htm](feats/class-04-DS0XlHfi3ztb3ET7.htm)|Split Shot|auto-trad|
|[class-04-DUuCOQ9FiZf7vS5b.htm](feats/class-04-DUuCOQ9FiZf7vS5b.htm)|Ritual Researcher|auto-trad|
|[class-04-epBOnfEDOd4V9mQ0.htm](feats/class-04-epBOnfEDOd4V9mQ0.htm)|Natural Swimmer|auto-trad|
|[class-04-eXuCYDzj0UJOxNu9.htm](feats/class-04-eXuCYDzj0UJOxNu9.htm)|Twin Distraction|auto-trad|
|[class-04-FdP21jbjHHGpHut1.htm](feats/class-04-FdP21jbjHHGpHut1.htm)|Tenacious Toxins|auto-trad|
|[class-04-fO1vRDEfl9pysfLU.htm](feats/class-04-fO1vRDEfl9pysfLU.htm)|Guarded Movement|auto-trad|
|[class-04-FoJd0oiEQ9mF5KS9.htm](feats/class-04-FoJd0oiEQ9mF5KS9.htm)|Steady Spellcasting (Magus)|auto-trad|
|[class-04-gL7QZsSMldjwE6te.htm](feats/class-04-gL7QZsSMldjwE6te.htm)|Inured to Alchemy|auto-trad|
|[class-04-GNMy7NYfF3AQwHpN.htm](feats/class-04-GNMy7NYfF3AQwHpN.htm)|Divine Health|auto-trad|
|[class-04-gVXefUaMzg7S1vpm.htm](feats/class-04-gVXefUaMzg7S1vpm.htm)|Knock Sense|auto-trad|
|[class-04-Gz9NQBjSDRQP8YTY.htm](feats/class-04-Gz9NQBjSDRQP8YTY.htm)|Striker's Scroll|auto-trad|
|[class-04-HHAGiBYVv8nyUEsd.htm](feats/class-04-HHAGiBYVv8nyUEsd.htm)|Dual-Handed Assault|auto-trad|
|[class-04-htOsE4hnSj2gzKdi.htm](feats/class-04-htOsE4hnSj2gzKdi.htm)|Mug|auto-trad|
|[class-04-I10dkdvL6kAnqZWA.htm](feats/class-04-I10dkdvL6kAnqZWA.htm)|Lie Detector|auto-trad|
|[class-04-IOdk7bOJ4dgYVh9I.htm](feats/class-04-IOdk7bOJ4dgYVh9I.htm)|Improved Communal Healing|auto-trad|
|[class-04-iojlXjVdbzi1fZGt.htm](feats/class-04-iojlXjVdbzi1fZGt.htm)|Directed Channel|auto-trad|
|[class-04-it2i6OXfGIizokpg.htm](feats/class-04-it2i6OXfGIizokpg.htm)|Animal Feature|auto-trad|
|[class-04-IvgLDtXmZwzbVJj1.htm](feats/class-04-IvgLDtXmZwzbVJj1.htm)|Strategic Assessment|auto-trad|
|[class-04-j01dM0ZAC7KzShx0.htm](feats/class-04-j01dM0ZAC7KzShx0.htm)|Rites Of Convocation|auto-trad|
|[class-04-jBeuyq0Aged45YAc.htm](feats/class-04-jBeuyq0Aged45YAc.htm)|Enduring Alchemy|auto-trad|
|[class-04-JbrVcOf82oFXk3mY.htm](feats/class-04-JbrVcOf82oFXk3mY.htm)|Swipe|auto-trad|
|[class-04-JcXzKwrdMkNszrJQ.htm](feats/class-04-JcXzKwrdMkNszrJQ.htm)|Radiant Infusion|auto-trad|
|[class-04-JGfJPx6xkx11zHlW.htm](feats/class-04-JGfJPx6xkx11zHlW.htm)|Linked Focus|auto-trad|
|[class-04-JgfZOEOgnbKNYth4.htm](feats/class-04-JgfZOEOgnbKNYth4.htm)|Irezoko Tattoo|auto-trad|
|[class-04-JlGZFW3mCNxWPKvX.htm](feats/class-04-JlGZFW3mCNxWPKvX.htm)|Silent Spell|auto-trad|
|[class-04-jQ8JntOdBFHPw5S4.htm](feats/class-04-jQ8JntOdBFHPw5S4.htm)|Light of Revelation|auto-trad|
|[class-04-K3TasgeZLJQ84qtZ.htm](feats/class-04-K3TasgeZLJQ84qtZ.htm)|Primal Evolution|auto-trad|
|[class-04-k4QU2edqSoB23foo.htm](feats/class-04-k4QU2edqSoB23foo.htm)|The Harder They Fall|auto-trad|
|[class-04-kh4bTBgi3C9CjwHK.htm](feats/class-04-kh4bTBgi3C9CjwHK.htm)|Melodious Spell|auto-trad|
|[class-04-KMVXUgFArcftg1jQ.htm](feats/class-04-KMVXUgFArcftg1jQ.htm)|Masquerade of Seasons Stance|auto-trad|
|[class-04-kqW6d3Dfk4nApd7y.htm](feats/class-04-kqW6d3Dfk4nApd7y.htm)|Combat Reading|auto-trad|
|[class-04-KWXoo738KuddWMOB.htm](feats/class-04-KWXoo738KuddWMOB.htm)|Ongoing Investigation|auto-trad|
|[class-04-Kxckxf4G9URXNc07.htm](feats/class-04-Kxckxf4G9URXNc07.htm)|Wolf in Sheep's Clothing|auto-trad|
|[class-04-LJw5tRrX0dMnm9Vq.htm](feats/class-04-LJw5tRrX0dMnm9Vq.htm)|Soothing Mist|auto-trad|
|[class-04-lKgpe2JKaeLjgnYF.htm](feats/class-04-lKgpe2JKaeLjgnYF.htm)|Woodland Stride|auto-trad|
|[class-04-lL2fQJ2oRyBgga8Q.htm](feats/class-04-lL2fQJ2oRyBgga8Q.htm)|Occult Evolution|auto-trad|
|[class-04-m4FOYkzuqNjU0ETq.htm](feats/class-04-m4FOYkzuqNjU0ETq.htm)|Eldritch Nails|auto-trad|
|[class-04-Mj1KTiAwwovm7K9f.htm](feats/class-04-Mj1KTiAwwovm7K9f.htm)|Stand Still|auto-trad|
|[class-04-MU9qS0QuBdcyLkza.htm](feats/class-04-MU9qS0QuBdcyLkza.htm)|Homing Beacon|auto-trad|
|[class-04-mxO7u59ms58q7zyj.htm](feats/class-04-mxO7u59ms58q7zyj.htm)|Magical Trickster|auto-trad|
|[class-04-mXp6G4YWCXGvp7Qd.htm](feats/class-04-mXp6G4YWCXGvp7Qd.htm)|Awakened Power|auto-trad|
|[class-04-N4TpzEzuFbInSgvz.htm](feats/class-04-N4TpzEzuFbInSgvz.htm)|Hunter's Luck|auto-trad|
|[class-04-N8mhYbr5xBI8jydb.htm](feats/class-04-N8mhYbr5xBI8jydb.htm)|Violent Unleash|auto-trad|
|[class-04-ncXK0Cc8dZ9TilSC.htm](feats/class-04-ncXK0Cc8dZ9TilSC.htm)|Devil's Eye|auto-trad|
|[class-04-NHheDmNB7L4REmlr.htm](feats/class-04-NHheDmNB7L4REmlr.htm)|Swaggering Initiative|auto-trad|
|[class-04-o5q9FBrPsAYqEl5w.htm](feats/class-04-o5q9FBrPsAYqEl5w.htm)|Channel Smite|auto-trad|
|[class-04-oGu9AtUAx0SpRXy8.htm](feats/class-04-oGu9AtUAx0SpRXy8.htm)|Favored Enemy|auto-trad|
|[class-04-oHdUwzUUblg3neCT.htm](feats/class-04-oHdUwzUUblg3neCT.htm)|Order Magic|auto-trad|
|[class-04-oIE88rIDEFNm83Mr.htm](feats/class-04-oIE88rIDEFNm83Mr.htm)|Powerful Shove|auto-trad|
|[class-04-pCVegyXxNibF4ulp.htm](feats/class-04-pCVegyXxNibF4ulp.htm)|Elaborate Flourish|auto-trad|
|[class-04-PH5b61x3iJSKP3Xi.htm](feats/class-04-PH5b61x3iJSKP3Xi.htm)|Farabellus Flip|auto-trad|
|[class-04-pqVG9mRKcXg5Rsjc.htm](feats/class-04-pqVG9mRKcXg5Rsjc.htm)|Thoughtform Summoning|auto-trad|
|[class-04-pvFRMbIazwAO0fjH.htm](feats/class-04-pvFRMbIazwAO0fjH.htm)|Echo of the Fallen|auto-trad|
|[class-04-PwcmmJOLY8C9JHau.htm](feats/class-04-PwcmmJOLY8C9JHau.htm)|Double Shot|auto-trad|
|[class-04-Q1O4P1YIkCfeedHH.htm](feats/class-04-Q1O4P1YIkCfeedHH.htm)|Alchemical Shot|auto-trad|
|[class-04-qav9ec9cR4lFcz3C.htm](feats/class-04-qav9ec9cR4lFcz3C.htm)|Disrupt Prey|auto-trad|
|[class-04-qbH9ns3HMYBxIvEQ.htm](feats/class-04-qbH9ns3HMYBxIvEQ.htm)|Fire Resistance|auto-trad|
|[class-04-Qfn7lmOeXfBtpG4O.htm](feats/class-04-Qfn7lmOeXfBtpG4O.htm)|Impaling Finisher|auto-trad|
|[class-04-qFR5OddDBmhZe6nl.htm](feats/class-04-qFR5OddDBmhZe6nl.htm)|Chemical Purification|auto-trad|
|[class-04-qmx8QwVepqI9FqiD.htm](feats/class-04-qmx8QwVepqI9FqiD.htm)|Tandem Movement|auto-trad|
|[class-04-qxh4evekG28Gt1vj.htm](feats/class-04-qxh4evekG28Gt1vj.htm)|Arcane Evolution|auto-trad|
|[class-04-r6dvGxru3FWNLVE2.htm](feats/class-04-r6dvGxru3FWNLVE2.htm)|Far Shot|auto-trad|
|[class-04-RonS3ZJs4poFTckH.htm](feats/class-04-RonS3ZJs4poFTckH.htm)|Shrink Down|auto-trad|
|[class-04-RsluSLtSWq1vN8Hc.htm](feats/class-04-RsluSLtSWq1vN8Hc.htm)|Form Control|auto-trad|
|[class-04-RzElsoBGTjKWjPgY.htm](feats/class-04-RzElsoBGTjKWjPgY.htm)|Sheltering Cave|auto-trad|
|[class-04-S5vOQ7J8DKR8sEj0.htm](feats/class-04-S5vOQ7J8DKR8sEj0.htm)|Paired Link|auto-trad|
|[class-04-sahJHnojXO9eEXVE.htm](feats/class-04-sahJHnojXO9eEXVE.htm)|Inspire Defense|auto-trad|
|[class-04-SCDSgeJU7vULvHmo.htm](feats/class-04-SCDSgeJU7vULvHmo.htm)|Fast Movement|auto-trad|
|[class-04-sEWYOllJ6rYoXK4K.htm](feats/class-04-sEWYOllJ6rYoXK4K.htm)|Raging Athlete|auto-trad|
|[class-04-sgaqlDFTVC7Ryurt.htm](feats/class-04-sgaqlDFTVC7Ryurt.htm)|Deflect Arrow|auto-trad|
|[class-04-sM3nmDi3PHWI64SH.htm](feats/class-04-sM3nmDi3PHWI64SH.htm)|Astral Tether|auto-trad|
|[class-04-SnIx3FhBuuq6AZD0.htm](feats/class-04-SnIx3FhBuuq6AZD0.htm)|Quick Reversal|auto-trad|
|[class-04-so4v9xjBFaoJ8EQs.htm](feats/class-04-so4v9xjBFaoJ8EQs.htm)|Supernatural Senses|auto-trad|
|[class-04-sv3ywEHaab9oZ3Nj.htm](feats/class-04-sv3ywEHaab9oZ3Nj.htm)|Courageous Advance|auto-trad|
|[class-04-TgYs2m9scSyEJwdr.htm](feats/class-04-TgYs2m9scSyEJwdr.htm)|Vibration Sense|auto-trad|
|[class-04-tHqlcgcxHXzqLHPZ.htm](feats/class-04-tHqlcgcxHXzqLHPZ.htm)|Shielded Stride|auto-trad|
|[class-04-tIE2umG4rQOxm8D8.htm](feats/class-04-tIE2umG4rQOxm8D8.htm)|Oversized Throw|auto-trad|
|[class-04-tIeVe9jOmxW7NgCK.htm](feats/class-04-tIeVe9jOmxW7NgCK.htm)|Distracting Spellstrike|auto-trad|
|[class-04-TIwk07T0OxSbcOpJ.htm](feats/class-04-TIwk07T0OxSbcOpJ.htm)|Aura of Courage|auto-trad|
|[class-04-TNpoEG0cUEAuSju7.htm](feats/class-04-TNpoEG0cUEAuSju7.htm)|Sun Blade|auto-trad|
|[class-04-to6s7QanfhHukW5r.htm](feats/class-04-to6s7QanfhHukW5r.htm)|Barreling Charge|auto-trad|
|[class-04-toFhkS9QbObxg6cp.htm](feats/class-04-toFhkS9QbObxg6cp.htm)|Versatile Signature|auto-trad|
|[class-04-TORYSZMLMAGgsSEW.htm](feats/class-04-TORYSZMLMAGgsSEW.htm)|Companion's Cry|auto-trad|
|[class-04-Tpcq3Lk7qEOZ3LDP.htm](feats/class-04-Tpcq3Lk7qEOZ3LDP.htm)|Scout's Warning|auto-trad|
|[class-04-tWBK7Zbt80JlPryC.htm](feats/class-04-tWBK7Zbt80JlPryC.htm)|Bespell Weapon|auto-trad|
|[class-04-uisI6b7Ua5zSHDwj.htm](feats/class-04-uisI6b7Ua5zSHDwj.htm)|Psi Strikes|auto-trad|
|[class-04-UjWLK86BgHxz3Itd.htm](feats/class-04-UjWLK86BgHxz3Itd.htm)|Calculated Splash|auto-trad|
|[class-04-UWG1USE0L2ZxEPiO.htm](feats/class-04-UWG1USE0L2ZxEPiO.htm)|Wholeness of Body|auto-trad|
|[class-04-Ux0DSklFlIlcvnhO.htm](feats/class-04-Ux0DSklFlIlcvnhO.htm)|Sacred Ki|auto-trad|
|[class-04-uY03kVQBA81gbTj9.htm](feats/class-04-uY03kVQBA81gbTj9.htm)|Emergency Targe|auto-trad|
|[class-04-vDeG0N4kzTBDTC2b.htm](feats/class-04-vDeG0N4kzTBDTC2b.htm)|Sabotage|auto-trad|
|[class-04-vPZxFpq7XkRmE3Uc.htm](feats/class-04-vPZxFpq7XkRmE3Uc.htm)|Black Powder Boost|auto-trad|
|[class-04-vQ4DNfpktmaqdgdM.htm](feats/class-04-vQ4DNfpktmaqdgdM.htm)|Expanded Domain Initiate|auto-trad|
|[class-04-VVsYBmVi2E1u9E5Z.htm](feats/class-04-VVsYBmVi2E1u9E5Z.htm)|Reactive Pursuit|auto-trad|
|[class-04-vxA0VRN10OwUkGAr.htm](feats/class-04-vxA0VRN10OwUkGAr.htm)|Cruelty|auto-trad|
|[class-04-wB1ONG2uO7RnD1iC.htm](feats/class-04-wB1ONG2uO7RnD1iC.htm)|Flying Kick|auto-trad|
|[class-04-wbS8f7R7KqHkwOzM.htm](feats/class-04-wbS8f7R7KqHkwOzM.htm)|Sacral Lord|auto-trad|
|[class-04-WHOCaVobY7N3UTtA.htm](feats/class-04-WHOCaVobY7N3UTtA.htm)|Divine Evolution|auto-trad|
|[class-04-wjnfdh6WzN7HbmeE.htm](feats/class-04-wjnfdh6WzN7HbmeE.htm)|Aura Of Despair|auto-trad|
|[class-04-ww5AM2yFs0lqQhmD.htm](feats/class-04-ww5AM2yFs0lqQhmD.htm)|Vision Of Weakness|auto-trad|
|[class-04-wz2edbLFnDKDNWWZ.htm](feats/class-04-wz2edbLFnDKDNWWZ.htm)|Flurry of Maneuvers|auto-trad|
|[class-04-XGZUjc9I3sjfniDg.htm](feats/class-04-XGZUjc9I3sjfniDg.htm)|Dual Energy Heart|auto-trad|
|[class-04-XkYNCFdZMjZTw6nn.htm](feats/class-04-XkYNCFdZMjZTw6nn.htm)|Giant Hunter|auto-trad|
|[class-04-xlparPCGhkgjdhx2.htm](feats/class-04-xlparPCGhkgjdhx2.htm)|Defend Summoner|auto-trad|
|[class-04-XWtNGOkMHcrdrRw8.htm](feats/class-04-XWtNGOkMHcrdrRw8.htm)|Dual-Form Weapon|auto-trad|
|[class-04-xXHoyccK5ZG2AJKg.htm](feats/class-04-xXHoyccK5ZG2AJKg.htm)|Skilled Partner|auto-trad|
|[class-04-xXHwktc9SymSY8d6.htm](feats/class-04-xXHwktc9SymSY8d6.htm)|Healing Bomb|auto-trad|
|[class-04-xYakFeP6olBsxpZN.htm](feats/class-04-xYakFeP6olBsxpZN.htm)|Command Undead|auto-trad|
|[class-04-Y8LHfkzGyOhPlUou.htm](feats/class-04-Y8LHfkzGyOhPlUou.htm)|Twin Parry|auto-trad|
|[class-04-YJIzE2RhGRGfbt9j.htm](feats/class-04-YJIzE2RhGRGfbt9j.htm)|Guardian's Deflection (Swashbuckler)|auto-trad|
|[class-04-yMj9WfPctWQC7be2.htm](feats/class-04-yMj9WfPctWQC7be2.htm)|Thaumaturgic Ritualist|auto-trad|
|[class-04-yozSCfdLFHVBbTxj.htm](feats/class-04-yozSCfdLFHVBbTxj.htm)|Mature Animal Companion (Druid)|auto-trad|
|[class-04-YYTZFBT2WZMU14om.htm](feats/class-04-YYTZFBT2WZMU14om.htm)|Lifelink Surge|auto-trad|
|[class-04-zgljg4gVI6i1Fpb5.htm](feats/class-04-zgljg4gVI6i1Fpb5.htm)|Paired Shots|auto-trad|
|[class-04-ZHPSASbvbbshq1zG.htm](feats/class-04-ZHPSASbvbbshq1zG.htm)|Leading Dance|auto-trad|
|[class-04-zilwynzk8lIujgZo.htm](feats/class-04-zilwynzk8lIujgZo.htm)|Mercy|auto-trad|
|[class-06-0UdHPOv3DX8TY9yb.htm](feats/class-06-0UdHPOv3DX8TY9yb.htm)|Snap Shot|auto-trad|
|[class-06-0zSoSPwC4cpqRewj.htm](feats/class-06-0zSoSPwC4cpqRewj.htm)|Song Of Marching|auto-trad|
|[class-06-10DbphslCihq8mxQ.htm](feats/class-06-10DbphslCihq8mxQ.htm)|Ki Blast|auto-trad|
|[class-06-1p5ErCp33nGOzEsk.htm](feats/class-06-1p5ErCp33nGOzEsk.htm)|Disarming Stance|auto-trad|
|[class-06-21YWBdoXGmj60vdI.htm](feats/class-06-21YWBdoXGmj60vdI.htm)|Harmonize|auto-trad|
|[class-06-2HoDwBAmPIAoKUVF.htm](feats/class-06-2HoDwBAmPIAoKUVF.htm)|Dazing Blow|auto-trad|
|[class-06-3PHHiZjX16Dwyt65.htm](feats/class-06-3PHHiZjX16Dwyt65.htm)|Analyze Weakness|auto-trad|
|[class-06-3uavnVbCsqTvzpgt.htm](feats/class-06-3uavnVbCsqTvzpgt.htm)|Dragon's Rage Breath|auto-trad|
|[class-06-3vjOXL9ZD4ibaJL6.htm](feats/class-06-3vjOXL9ZD4ibaJL6.htm)|Green Empathy|auto-trad|
|[class-06-4I1Kq53Qfzrrmg2E.htm](feats/class-06-4I1Kq53Qfzrrmg2E.htm)|Skirmish Strike|auto-trad|
|[class-06-4o9g5g12yyrfZ3Xd.htm](feats/class-06-4o9g5g12yyrfZ3Xd.htm)|Light Step|auto-trad|
|[class-06-515N9nl9ChZwLWKR.htm](feats/class-06-515N9nl9ChZwLWKR.htm)|Defensive Coordination|auto-trad|
|[class-06-52cygjzHfSD0YhEA.htm](feats/class-06-52cygjzHfSD0YhEA.htm)|Spell Penetration|auto-trad|
|[class-06-52QyoGaysrfBOy5H.htm](feats/class-06-52QyoGaysrfBOy5H.htm)|Witch's Charge|auto-trad|
|[class-06-588O3jurogttvqgm.htm](feats/class-06-588O3jurogttvqgm.htm)|Eidolon's Opportunity|auto-trad|
|[class-06-5Jc2ySGLVi053qpz.htm](feats/class-06-5Jc2ySGLVi053qpz.htm)|Twist the Knife|auto-trad|
|[class-06-5N6rLz4mdJg0NrQH.htm](feats/class-06-5N6rLz4mdJg0NrQH.htm)|Anticipate Ambush|auto-trad|
|[class-06-5vXc2s2siR1ihpBT.htm](feats/class-06-5vXc2s2siR1ihpBT.htm)|Shield Warden (Fighter)|auto-trad|
|[class-06-65gjc5KE4ZRoocbi.htm](feats/class-06-65gjc5KE4ZRoocbi.htm)|Abundant Step|auto-trad|
|[class-06-6gLWr3xghsHSFwxc.htm](feats/class-06-6gLWr3xghsHSFwxc.htm)|Water Step|auto-trad|
|[class-06-6iDd7CTzxkvMp6lB.htm](feats/class-06-6iDd7CTzxkvMp6lB.htm)|Align Ki|auto-trad|
|[class-06-6J2ZSGNsXPKPcJGV.htm](feats/class-06-6J2ZSGNsXPKPcJGV.htm)|Tiger Slash|auto-trad|
|[class-06-7KT4huf0iPaBGD7R.htm](feats/class-06-7KT4huf0iPaBGD7R.htm)|Combination Finisher|auto-trad|
|[class-06-9cHQua33V35JPE3U.htm](feats/class-06-9cHQua33V35JPE3U.htm)|Educate Allies|auto-trad|
|[class-06-9CXQhg4YprPhqzoL.htm](feats/class-06-9CXQhg4YprPhqzoL.htm)|Vexing Tumble|auto-trad|
|[class-06-9p4oFIn791VAmzUn.htm](feats/class-06-9p4oFIn791VAmzUn.htm)|Cauterize|auto-trad|
|[class-06-ajesR7y0jWzqjAgc.htm](feats/class-06-ajesR7y0jWzqjAgc.htm)|Current Spell|auto-trad|
|[class-06-AWd7uZBfba5mNXeT.htm](feats/class-06-AWd7uZBfba5mNXeT.htm)|Syu Tak-nwa's Deadly Hair|auto-trad|
|[class-06-axGS5bBJ9vl5AePc.htm](feats/class-06-axGS5bBJ9vl5AePc.htm)|Phase Out|auto-trad|
|[class-06-axvGLVoAvuD9jU78.htm](feats/class-06-axvGLVoAvuD9jU78.htm)|Loyal Warhorse|auto-trad|
|[class-06-b532qrlTUqWxLd1j.htm](feats/class-06-b532qrlTUqWxLd1j.htm)|Shield Warden (Champion)|auto-trad|
|[class-06-b7isszc8C75V3okn.htm](feats/class-06-b7isszc8C75V3okn.htm)|Sticky Poison|auto-trad|
|[class-06-Ba6SLqAghsZgqhua.htm](feats/class-06-Ba6SLqAghsZgqhua.htm)|Agile Maneuvers|auto-trad|
|[class-06-BBN5G6epRWXGwZHv.htm](feats/class-06-BBN5G6epRWXGwZHv.htm)|Ephemeral Tracking|auto-trad|
|[class-06-BkEOwv3SRtefczpO.htm](feats/class-06-BkEOwv3SRtefczpO.htm)|Phalanx Breaker|auto-trad|
|[class-06-bSXcyu7ExWq9qUzG.htm](feats/class-06-bSXcyu7ExWq9qUzG.htm)|Convincing Illusion|auto-trad|
|[class-06-c5ns35FLvvxjimuH.htm](feats/class-06-c5ns35FLvvxjimuH.htm)|Swift Tracker|auto-trad|
|[class-06-c6CS97Zs0DPmInaI.htm](feats/class-06-c6CS97Zs0DPmInaI.htm)|Assured Knowledge|auto-trad|
|[class-06-cTQMtd2IVlvgJwAn.htm](feats/class-06-cTQMtd2IVlvgJwAn.htm)|Slice and Swipe|auto-trad|
|[class-06-CvMCw6JqvgMPE5uk.htm](feats/class-06-CvMCw6JqvgMPE5uk.htm)|Blood Frenzy|auto-trad|
|[class-06-d00Ip4YHVmk2tecD.htm](feats/class-06-d00Ip4YHVmk2tecD.htm)|Sympathetic Vulnerabilities|auto-trad|
|[class-06-dHUoQVzDa9Cf4QCG.htm](feats/class-06-dHUoQVzDa9Cf4QCG.htm)|Reflexive Shield|auto-trad|
|[class-06-dMKx0T629hpJCN8T.htm](feats/class-06-dMKx0T629hpJCN8T.htm)|Sixth Sense|auto-trad|
|[class-06-DPk7a0cdFOjDOdn5.htm](feats/class-06-DPk7a0cdFOjDOdn5.htm)|Advanced Elemental Spell|auto-trad|
|[class-06-dxUbA1dUEcVHnU5s.htm](feats/class-06-dxUbA1dUEcVHnU5s.htm)|Clockwork Celerity|auto-trad|
|[class-06-E5ewlRE6Mh9ZqUMu.htm](feats/class-06-E5ewlRE6Mh9ZqUMu.htm)|Ostentatious Arrival|auto-trad|
|[class-06-Ea6Z5cxeBCCtPD5R.htm](feats/class-06-Ea6Z5cxeBCCtPD5R.htm)|Ranger's Bramble|auto-trad|
|[class-06-EBmZyzDWhFSLydlM.htm](feats/class-06-EBmZyzDWhFSLydlM.htm)|Storming Breath|auto-trad|
|[class-06-eNeSl5UNaqDwyNkp.htm](feats/class-06-eNeSl5UNaqDwyNkp.htm)|Eidolon's Wrath|auto-trad|
|[class-06-exVjUkpAdbzJsxxg.htm](feats/class-06-exVjUkpAdbzJsxxg.htm)|Tandem Strike|auto-trad|
|[class-06-eZrftEihfuJBldG5.htm](feats/class-06-eZrftEihfuJBldG5.htm)|Advanced Bloodline|auto-trad|
|[class-06-F1DVDJRARfdb1Kjz.htm](feats/class-06-F1DVDJRARfdb1Kjz.htm)|Storm Retribution|auto-trad|
|[class-06-FNO2hfGmxqJngD4A.htm](feats/class-06-FNO2hfGmxqJngD4A.htm)|Drifter's Juke|auto-trad|
|[class-06-FPVe3o7YctBicSQa.htm](feats/class-06-FPVe3o7YctBicSQa.htm)|Advanced Revelation|auto-trad|
|[class-06-FqrfyUtoBWJNnSi6.htm](feats/class-06-FqrfyUtoBWJNnSi6.htm)|Pain Tolerance|auto-trad|
|[class-06-fx50Ivl1ERxTijpT.htm](feats/class-06-fx50Ivl1ERxTijpT.htm)|Invigorating Mercy|auto-trad|
|[class-06-GE96a0UGPYM74qjI.htm](feats/class-06-GE96a0UGPYM74qjI.htm)|Cleave|auto-trad|
|[class-06-grPtqWYbdXXo7yhP.htm](feats/class-06-grPtqWYbdXXo7yhP.htm)|Fey Tracker|auto-trad|
|[class-06-gv7PJVrnODu3qYB0.htm](feats/class-06-gv7PJVrnODu3qYB0.htm)|Shielded Tome|auto-trad|
|[class-06-gwIgB6bMh0sruyX7.htm](feats/class-06-gwIgB6bMh0sruyX7.htm)|Connect The Dots|auto-trad|
|[class-06-HB0jvWCdim1p91q1.htm](feats/class-06-HB0jvWCdim1p91q1.htm)|Spiritual Sense|auto-trad|
|[class-06-HBZSP3ABqmsV9FXH.htm](feats/class-06-HBZSP3ABqmsV9FXH.htm)|Advanced Shooter|auto-trad|
|[class-06-hjApw8AvYVuqQk2W.htm](feats/class-06-hjApw8AvYVuqQk2W.htm)|Spirits' Interference|auto-trad|
|[class-06-hJlLmjW0NNeV1Ous.htm](feats/class-06-hJlLmjW0NNeV1Ous.htm)|Cascade Countermeasure|auto-trad|
|[class-06-hkdP5tsTAoqJDR8v.htm](feats/class-06-hkdP5tsTAoqJDR8v.htm)|Triple Shot|auto-trad|
|[class-06-hlX7jYoS1s6srZC2.htm](feats/class-06-hlX7jYoS1s6srZC2.htm)|Megavolt|auto-trad|
|[class-06-IEbnal1VJySrhxFR.htm](feats/class-06-IEbnal1VJySrhxFR.htm)|Stella's Stab and Snag|auto-trad|
|[class-06-iJrHJKNGxV4z4Qi7.htm](feats/class-06-iJrHJKNGxV4z4Qi7.htm)|Artokus's Fire|auto-trad|
|[class-06-jaAnxfXVmUQy0IKU.htm](feats/class-06-jaAnxfXVmUQy0IKU.htm)|One-Inch Punch|auto-trad|
|[class-06-JdCRxwgtdQkJ1Ha6.htm](feats/class-06-JdCRxwgtdQkJ1Ha6.htm)|Guardian's Deflection (Fighter)|auto-trad|
|[class-06-JHcvySfCM9uYNb9N.htm](feats/class-06-JHcvySfCM9uYNb9N.htm)|Revealing Stab|auto-trad|
|[class-06-jSkJIWPfSZZzvYzq.htm](feats/class-06-jSkJIWPfSZZzvYzq.htm)|Combine Elixirs|auto-trad|
|[class-06-Jwq5o13uZF3ooln1.htm](feats/class-06-Jwq5o13uZF3ooln1.htm)|Jellyfish Stance|auto-trad|
|[class-06-k0NNa5Ko4XhDdBYB.htm](feats/class-06-k0NNa5Ko4XhDdBYB.htm)|Nocturnal Sense|auto-trad|
|[class-06-K5ZONljq5XzS8MQc.htm](feats/class-06-K5ZONljq5XzS8MQc.htm)|Detonating Spell|auto-trad|
|[class-06-KgD26HpSrKyciB8f.htm](feats/class-06-KgD26HpSrKyciB8f.htm)|Parallel Breakthrough|auto-trad|
|[class-06-KW6K5Zv4J7ClWkKA.htm](feats/class-06-KW6K5Zv4J7ClWkKA.htm)|Quick Snares|auto-trad|
|[class-06-kYA6LkDw4AzKI156.htm](feats/class-06-kYA6LkDw4AzKI156.htm)|Stumbling Feint|auto-trad|
|[class-06-L9sRaFl0tHT5AFIQ.htm](feats/class-06-L9sRaFl0tHT5AFIQ.htm)|Scatter Blast|auto-trad|
|[class-06-lGCFVYjL9Lp5m9Ex.htm](feats/class-06-lGCFVYjL9Lp5m9Ex.htm)|Cast Down|auto-trad|
|[class-06-lh3STEvbGnP7jVMr.htm](feats/class-06-lh3STEvbGnP7jVMr.htm)|Munitions Machinist|auto-trad|
|[class-06-lkl5QaDb1mlSD7SC.htm](feats/class-06-lkl5QaDb1mlSD7SC.htm)|Visual Fidelity|auto-trad|
|[class-06-Ls3MiZ5RcAWaiQ7f.htm](feats/class-06-Ls3MiZ5RcAWaiQ7f.htm)|Construct Shell|auto-trad|
|[class-06-mqLPCNdCSNyY7gyI.htm](feats/class-06-mqLPCNdCSNyY7gyI.htm)|Mage Hunter|auto-trad|
|[class-06-MROG87PQmuBTdCaB.htm](feats/class-06-MROG87PQmuBTdCaB.htm)|Jelly Body|auto-trad|
|[class-06-n2hawNmzW7DBn1Lm.htm](feats/class-06-n2hawNmzW7DBn1Lm.htm)|Mountain Stronghold|auto-trad|
|[class-06-nDjTJq7PEbvRktnb.htm](feats/class-06-nDjTJq7PEbvRktnb.htm)|Advanced Weapon Training|auto-trad|
|[class-06-Nm4yeoJVXRy0Wyth.htm](feats/class-06-Nm4yeoJVXRy0Wyth.htm)|Inertial Barrier|auto-trad|
|[class-06-NMWXHGWUcZGoLDKb.htm](feats/class-06-NMWXHGWUcZGoLDKb.htm)|Attack of Opportunity|auto-trad|
|[class-06-nRjyyDulHnP5OewA.htm](feats/class-06-nRjyyDulHnP5OewA.htm)|Gorilla Pound|auto-trad|
|[class-06-nU0r77AZXMXIlti6.htm](feats/class-06-nU0r77AZXMXIlti6.htm)|Additional Recollection|auto-trad|
|[class-06-OINfbwNZGnlyMqPR.htm](feats/class-06-OINfbwNZGnlyMqPR.htm)|Return Fire|auto-trad|
|[class-06-OqObuRB8oVSAEKFR.htm](feats/class-06-OqObuRB8oVSAEKFR.htm)|Scroll Esoterica|auto-trad|
|[class-06-oUhwrijg4rClCplO.htm](feats/class-06-oUhwrijg4rClCplO.htm)|Brutal Bully|auto-trad|
|[class-06-OWedlrKGsVZVkSnT.htm](feats/class-06-OWedlrKGsVZVkSnT.htm)|Insect Shape|auto-trad|
|[class-06-OY1Ewg0dbCp52Hl5.htm](feats/class-06-OY1Ewg0dbCp52Hl5.htm)|Inner Strength|auto-trad|
|[class-06-ozvYhY4hG1deXly8.htm](feats/class-06-ozvYhY4hG1deXly8.htm)|Directional Bombs|auto-trad|
|[class-06-PdQAAKtCW5dS9IYj.htm](feats/class-06-PdQAAKtCW5dS9IYj.htm)|Inner Fire (Monk)|auto-trad|
|[class-06-PiBXXCeDNQGfQVoJ.htm](feats/class-06-PiBXXCeDNQGfQVoJ.htm)|Divine Emissary|auto-trad|
|[class-06-PqZZSo06BH5N7x7C.htm](feats/class-06-PqZZSo06BH5N7x7C.htm)|Diverting Vortex|auto-trad|
|[class-06-qDfTqetM9UEpp8ty.htm](feats/class-06-qDfTqetM9UEpp8ty.htm)|Greater Lesson|auto-trad|
|[class-06-qeLpqH2cMSmIrILV.htm](feats/class-06-qeLpqH2cMSmIrILV.htm)|Precise Finisher|auto-trad|
|[class-06-QpRzvfWdj6YH9TyE.htm](feats/class-06-QpRzvfWdj6YH9TyE.htm)|Shield Wall|auto-trad|
|[class-06-QSuwyX84U26OLzZI.htm](feats/class-06-QSuwyX84U26OLzZI.htm)|Predictive Purchase (Investigator)|auto-trad|
|[class-06-QUnN2wUfvOD6Tz69.htm](feats/class-06-QUnN2wUfvOD6Tz69.htm)|Defy Fey|auto-trad|
|[class-06-RlKGaxQWWLa7xJSc.htm](feats/class-06-RlKGaxQWWLa7xJSc.htm)|Pirouette|auto-trad|
|[class-06-S14S52HjszTgIy4l.htm](feats/class-06-S14S52HjszTgIy4l.htm)|Crane Flutter|auto-trad|
|[class-06-SD3RlgQMVL6aWjtW.htm](feats/class-06-SD3RlgQMVL6aWjtW.htm)|Triggerbrand Salvo|auto-trad|
|[class-06-SELSj1vvVLx5cP72.htm](feats/class-06-SELSj1vvVLx5cP72.htm)|Dragon Roar|auto-trad|
|[class-06-Su4nbNnR0mjgusTT.htm](feats/class-06-Su4nbNnR0mjgusTT.htm)|Magic Hands|auto-trad|
|[class-06-sv4LeEbkOJyLen10.htm](feats/class-06-sv4LeEbkOJyLen10.htm)|Debilitating Bomb|auto-trad|
|[class-06-T5xFirAE8VLL5Lbu.htm](feats/class-06-T5xFirAE8VLL5Lbu.htm)|Smite Good|auto-trad|
|[class-06-TfDvkTNaC1DmsB2C.htm](feats/class-06-TfDvkTNaC1DmsB2C.htm)|Elysium's Cadence|auto-trad|
|[class-06-tpkJXDpSuGznfzGJ.htm](feats/class-06-tpkJXDpSuGznfzGJ.htm)|Mature Animal Companion (Ranger)|auto-trad|
|[class-06-Trj5azJlaOk5jgBi.htm](feats/class-06-Trj5azJlaOk5jgBi.htm)|Divine Weapon|auto-trad|
|[class-06-u8YnTCS2EGoJl90W.htm](feats/class-06-u8YnTCS2EGoJl90W.htm)|Strain Mind|auto-trad|
|[class-06-uH3ZRkXPsXi1ChO2.htm](feats/class-06-uH3ZRkXPsXi1ChO2.htm)|Corrupted Shield|auto-trad|
|[class-06-uiGsVmvRfujQQRlK.htm](feats/class-06-uiGsVmvRfujQQRlK.htm)|Steady Spellcasting|auto-trad|
|[class-06-uJpghjJtNbqKUxRd.htm](feats/class-06-uJpghjJtNbqKUxRd.htm)|Wolf Drag|auto-trad|
|[class-06-VAxtUenSWEBWYBRt.htm](feats/class-06-VAxtUenSWEBWYBRt.htm)|Enervating Wail|auto-trad|
|[class-06-VqVgcqmG6xmYuDbK.htm](feats/class-06-VqVgcqmG6xmYuDbK.htm)|Scouring Rage|auto-trad|
|[class-06-vWrGwqy4AhHMPz8V.htm](feats/class-06-vWrGwqy4AhHMPz8V.htm)|Dirge of Doom|auto-trad|
|[class-06-VYilg64xX9XpHeJr.htm](feats/class-06-VYilg64xX9XpHeJr.htm)|Far Throw|auto-trad|
|[class-06-w0nSRBNwexM5Dh0D.htm](feats/class-06-w0nSRBNwexM5Dh0D.htm)|Whirling Throw|auto-trad|
|[class-06-W21jKAcG0GtEtBiK.htm](feats/class-06-W21jKAcG0GtEtBiK.htm)|Giant's Stature|auto-trad|
|[class-06-WAx7RABHDvVVcRI8.htm](feats/class-06-WAx7RABHDvVVcRI8.htm)|Split Slot|auto-trad|
|[class-06-wios5UDRwKXoUYUD.htm](feats/class-06-wios5UDRwKXoUYUD.htm)|Master Summoner|auto-trad|
|[class-06-WjEwsu4kkexNvDcN.htm](feats/class-06-WjEwsu4kkexNvDcN.htm)|Thorough Research|auto-trad|
|[class-06-Wn3DWAXo2TRxrhI6.htm](feats/class-06-Wn3DWAXo2TRxrhI6.htm)|Sniper's Aim|auto-trad|
|[class-06-xgvKXeTxns0gIdAn.htm](feats/class-06-xgvKXeTxns0gIdAn.htm)|Smite Evil|auto-trad|
|[class-06-XRahcvEPEAEdGUn8.htm](feats/class-06-XRahcvEPEAEdGUn8.htm)|Gang Up|auto-trad|
|[class-06-XYaaj872JOO9CAws.htm](feats/class-06-XYaaj872JOO9CAws.htm)|Blazing Talon Surge|auto-trad|
|[class-06-y61mDkTqk2k77b4x.htm](feats/class-06-y61mDkTqk2k77b4x.htm)|Furious Focus|auto-trad|
|[class-06-Y8Rdt4gHBGAUf2zL.htm](feats/class-06-Y8Rdt4gHBGAUf2zL.htm)|Advantageous Assault|auto-trad|
|[class-06-yfzpmSF9IYsMeKxo.htm](feats/class-06-yfzpmSF9IYsMeKxo.htm)|One More Activation|auto-trad|
|[class-06-YhpR5bOquHx2JuIj.htm](feats/class-06-YhpR5bOquHx2JuIj.htm)|Litany Against Wrath|auto-trad|
|[class-06-yOyMlFGAgLfRas8m.htm](feats/class-06-yOyMlFGAgLfRas8m.htm)|Pistolero's Challenge|auto-trad|
|[class-06-yUpZcrQHrz4mflKQ.htm](feats/class-06-yUpZcrQHrz4mflKQ.htm)|Energetic Resonance|auto-trad|
|[class-06-yyt2I2lGbRndXjbc.htm](feats/class-06-yyt2I2lGbRndXjbc.htm)|Selective Energy|auto-trad|
|[class-06-z5fUX9jeqfAViOd8.htm](feats/class-06-z5fUX9jeqfAViOd8.htm)|Shove Down|auto-trad|
|[class-06-zbxqYhmn7KbqR2Sb.htm](feats/class-06-zbxqYhmn7KbqR2Sb.htm)|Shatter Defenses|auto-trad|
|[class-06-ZOr1FUlpJj1q6q3H.htm](feats/class-06-ZOr1FUlpJj1q6q3H.htm)|Knowledge is Power|auto-trad|
|[class-06-ZPclfDmiHzEqblry.htm](feats/class-06-ZPclfDmiHzEqblry.htm)|Animal Skin|auto-trad|
|[class-06-zwEaXGKqnlBTllfE.htm](feats/class-06-zwEaXGKqnlBTllfE.htm)|Spell Relay|auto-trad|
|[class-06-zZCyJhsaugHB6mZW.htm](feats/class-06-zZCyJhsaugHB6mZW.htm)|Watch Your Back|auto-trad|
|[class-08-0PcVi7eav6PMLOPl.htm](feats/class-08-0PcVi7eav6PMLOPl.htm)|Channeled Succor|auto-trad|
|[class-08-0XGLdVbEIISOOuuO.htm](feats/class-08-0XGLdVbEIISOOuuO.htm)|Bullseye|auto-trad|
|[class-08-16MOW7deoOoDwE9z.htm](feats/class-08-16MOW7deoOoDwE9z.htm)|Hazard Finder|auto-trad|
|[class-08-1qJCMbs3zcPMWDux.htm](feats/class-08-1qJCMbs3zcPMWDux.htm)|Know-It-All (Bard)|auto-trad|
|[class-08-1VLOhyq0IFMY2rqh.htm](feats/class-08-1VLOhyq0IFMY2rqh.htm)|Animal Rage|auto-trad|
|[class-08-264KzmKMK4zqi6AR.htm](feats/class-08-264KzmKMK4zqi6AR.htm)|Clue Them All In|auto-trad|
|[class-08-27R8yZcY2uXH6pZN.htm](feats/class-08-27R8yZcY2uXH6pZN.htm)|Powerful Snares|auto-trad|
|[class-08-2leK1eGVcIBTmx6J.htm](feats/class-08-2leK1eGVcIBTmx6J.htm)|Unseen Passage|auto-trad|
|[class-08-2MHzEh1KUQEbhjUf.htm](feats/class-08-2MHzEh1KUQEbhjUf.htm)|Vivacious Bravado|auto-trad|
|[class-08-2tUdsoPEnW9yS8so.htm](feats/class-08-2tUdsoPEnW9yS8so.htm)|Tangled Forest Stance|auto-trad|
|[class-08-3hhCDWPm021hvicR.htm](feats/class-08-3hhCDWPm021hvicR.htm)|Renewed Vigor|auto-trad|
|[class-08-3xD4LtEgpGZU0MNx.htm](feats/class-08-3xD4LtEgpGZU0MNx.htm)|Syu Tak-nwa's Hexed Locks|auto-trad|
|[class-08-4osf0dNeZ19KBoTb.htm](feats/class-08-4osf0dNeZ19KBoTb.htm)|Manifold Modifications|auto-trad|
|[class-08-58LYCoLrCzG2Ll8b.htm](feats/class-08-58LYCoLrCzG2Ll8b.htm)|Incredible Construct Companion|auto-trad|
|[class-08-5d4AyZ0Y6Ht1OwIa.htm](feats/class-08-5d4AyZ0Y6Ht1OwIa.htm)|Sidestep|auto-trad|
|[class-08-5hUj7glY8YnO5sBI.htm](feats/class-08-5hUj7glY8YnO5sBI.htm)|Advanced School Spell|auto-trad|
|[class-08-6biVVoaqqUdQmQ37.htm](feats/class-08-6biVVoaqqUdQmQ37.htm)|Fiery Retort|auto-trad|
|[class-08-6KiB0SLYB1p8Th5U.htm](feats/class-08-6KiB0SLYB1p8Th5U.htm)|Safeguarded Spell|auto-trad|
|[class-08-7IsHNime3WneCan6.htm](feats/class-08-7IsHNime3WneCan6.htm)|Wall Run|auto-trad|
|[class-08-7PlGRHizgieuYzDR.htm](feats/class-08-7PlGRHizgieuYzDR.htm)|Mental Static|auto-trad|
|[class-08-80DU0IvIzOIBGuUa.htm](feats/class-08-80DU0IvIzOIBGuUa.htm)|Loaner Spell|auto-trad|
|[class-08-8CLbJAtgSfwxk2rk.htm](feats/class-08-8CLbJAtgSfwxk2rk.htm)|Murksight|auto-trad|
|[class-08-8rE5zLEVe4putosB.htm](feats/class-08-8rE5zLEVe4putosB.htm)|Fey Caller|auto-trad|
|[class-08-8YLCu791osZNFKN2.htm](feats/class-08-8YLCu791osZNFKN2.htm)|Leap and Fire|auto-trad|
|[class-08-94PGauGdzrVARMLc.htm](feats/class-08-94PGauGdzrVARMLc.htm)|Grit and Tenacity|auto-trad|
|[class-08-9EmJElnNVmXQ7Rzn.htm](feats/class-08-9EmJElnNVmXQ7Rzn.htm)|Greenwatch Veteran|auto-trad|
|[class-08-9Eufa07qvXG41QmG.htm](feats/class-08-9Eufa07qvXG41QmG.htm)|Improved Poison Weapon|auto-trad|
|[class-08-9MiK0Lyro5dQgHij.htm](feats/class-08-9MiK0Lyro5dQgHij.htm)|Rippling Spin|auto-trad|
|[class-08-9pTQrhbeF348bYky.htm](feats/class-08-9pTQrhbeF348bYky.htm)|Inspired Stratagem|auto-trad|
|[class-08-9Q0tPGtOawPTU2TU.htm](feats/class-08-9Q0tPGtOawPTU2TU.htm)|Follow-up Assault|auto-trad|
|[class-08-9sl2t3jb5ZdQA3K4.htm](feats/class-08-9sl2t3jb5ZdQA3K4.htm)|Can't You See?|auto-trad|
|[class-08-AbgHCPWOKULeXrJ2.htm](feats/class-08-AbgHCPWOKULeXrJ2.htm)|Sly Striker|auto-trad|
|[class-08-Ad0XBuETAkMD6doj.htm](feats/class-08-Ad0XBuETAkMD6doj.htm)|Felling Strike|auto-trad|
|[class-08-AiV2xFhYB90KHt2x.htm](feats/class-08-AiV2xFhYB90KHt2x.htm)|Pinning Fire|auto-trad|
|[class-08-Ar6W97iun6yYI8Df.htm](feats/class-08-Ar6W97iun6yYI8Df.htm)|Delay Trap|auto-trad|
|[class-08-ASAXLrckAyTBYi8E.htm](feats/class-08-ASAXLrckAyTBYi8E.htm)|Forgotten Presence|auto-trad|
|[class-08-b1eGMNjBY3iqIt2S.htm](feats/class-08-b1eGMNjBY3iqIt2S.htm)|Magical Adept|auto-trad|
|[class-08-BaKEnNbzbGlenmRv.htm](feats/class-08-BaKEnNbzbGlenmRv.htm)|Eerie Traces|auto-trad|
|[class-08-bcExqs4CsG2Kc5Bs.htm](feats/class-08-bcExqs4CsG2Kc5Bs.htm)|Miniaturize|auto-trad|
|[class-08-BkKWTt3ufaCN2ZdI.htm](feats/class-08-BkKWTt3ufaCN2ZdI.htm)|Sudden Leap|auto-trad|
|[class-08-bPqRneuJPqeXc65G.htm](feats/class-08-bPqRneuJPqeXc65G.htm)|Sense Good|auto-trad|
|[class-08-bYijGvCvCmJnW6aA.htm](feats/class-08-bYijGvCvCmJnW6aA.htm)|Sacrifice Armor|auto-trad|
|[class-08-c1QTcMtI9957gQoB.htm](feats/class-08-c1QTcMtI9957gQoB.htm)|Remove Presence|auto-trad|
|[class-08-cA1IIy6UEsgETXiX.htm](feats/class-08-cA1IIy6UEsgETXiX.htm)|Furious Bully|auto-trad|
|[class-08-CL9pFxxMXqzIyg4j.htm](feats/class-08-CL9pFxxMXqzIyg4j.htm)|Wild Winds Initiate|auto-trad|
|[class-08-CN7tu5H6wTe9ENmO.htm](feats/class-08-CN7tu5H6wTe9ENmO.htm)|Crossblooded Evolution|auto-trad|
|[class-08-DGO6kyjw2bQG7dbY.htm](feats/class-08-DGO6kyjw2bQG7dbY.htm)|Incredible Aim|auto-trad|
|[class-08-dTO1ShJovbzrKUY4.htm](feats/class-08-dTO1ShJovbzrKUY4.htm)|Resounding Bravery|auto-trad|
|[class-08-dTPVRVzfVBlBUV2l.htm](feats/class-08-dTPVRVzfVBlBUV2l.htm)|Arrow Snatching|auto-trad|
|[class-08-dxujgA0NgiEvA0H8.htm](feats/class-08-dxujgA0NgiEvA0H8.htm)|Bleeding Finisher|auto-trad|
|[class-08-DYc108IqRBP9N9W6.htm](feats/class-08-DYc108IqRBP9N9W6.htm)|Soulsight (Bard)|auto-trad|
|[class-08-enPAJ1oSDutts7ic.htm](feats/class-08-enPAJ1oSDutts7ic.htm)|Deadly Aim|auto-trad|
|[class-08-EnpbhZr94ZyZI4hb.htm](feats/class-08-EnpbhZr94ZyZI4hb.htm)|Sense Evil|auto-trad|
|[class-08-enxzAkPICXT4sSFU.htm](feats/class-08-enxzAkPICXT4sSFU.htm)|Fused Staff|auto-trad|
|[class-08-F6ZAceuDpiM9bUiF.htm](feats/class-08-F6ZAceuDpiM9bUiF.htm)|Nimble Roll|auto-trad|
|[class-08-FEMXDBCf8gXBYdez.htm](feats/class-08-FEMXDBCf8gXBYdez.htm)|Dark Persona's Presence|auto-trad|
|[class-08-ffdXSxl4lVFrOvyQ.htm](feats/class-08-ffdXSxl4lVFrOvyQ.htm)|Ambushing Knockdown|auto-trad|
|[class-08-fFfRsvDavUsTBDF2.htm](feats/class-08-fFfRsvDavUsTBDF2.htm)|Martyr|auto-trad|
|[class-08-FjuuX0vUWlYchRNM.htm](feats/class-08-FjuuX0vUWlYchRNM.htm)|Inspire Heroics|auto-trad|
|[class-08-Fs88vjez9px2mmrC.htm](feats/class-08-Fs88vjez9px2mmrC.htm)|Spell Swipe|auto-trad|
|[class-08-gHc9mqHiMqayiOIx.htm](feats/class-08-gHc9mqHiMqayiOIx.htm)|Boost Summons|auto-trad|
|[class-08-gQ2EvesPqLbISLQV.htm](feats/class-08-gQ2EvesPqLbISLQV.htm)|Feral Mutagen|auto-trad|
|[class-08-gYcmow7HM8J3giwL.htm](feats/class-08-gYcmow7HM8J3giwL.htm)|Grand Dance|auto-trad|
|[class-08-H6qletvAJUCC1aIa.htm](feats/class-08-H6qletvAJUCC1aIa.htm)|Hulking Size|auto-trad|
|[class-08-HLC9g1pwluDl6vy7.htm](feats/class-08-HLC9g1pwluDl6vy7.htm)|Read Disaster|auto-trad|
|[class-08-j4zGMRiTi5t6guMF.htm](feats/class-08-j4zGMRiTi5t6guMF.htm)|Disorienting Opening|auto-trad|
|[class-08-jCIBYryi6Y3JwmqH.htm](feats/class-08-jCIBYryi6Y3JwmqH.htm)|Mixed Maneuver|auto-trad|
|[class-08-JHJBmiyILzWdFRJO.htm](feats/class-08-JHJBmiyILzWdFRJO.htm)|Advanced Deity's Domain|auto-trad|
|[class-08-Jj6sVfIX81tgvSY4.htm](feats/class-08-Jj6sVfIX81tgvSY4.htm)|Wind Caller|auto-trad|
|[class-08-jkBzlMB4TS1sS2Fm.htm](feats/class-08-jkBzlMB4TS1sS2Fm.htm)|Stunning Finisher|auto-trad|
|[class-08-JOq4Xe49A04YycRz.htm](feats/class-08-JOq4Xe49A04YycRz.htm)|Call And Response|auto-trad|
|[class-08-Joy1e6pdqx6fN9mH.htm](feats/class-08-Joy1e6pdqx6fN9mH.htm)|Incredible Familiar (Thaumaturge)|auto-trad|
|[class-08-jZy91ekcS9ZqmdEH.htm](feats/class-08-jZy91ekcS9ZqmdEH.htm)|Knight's Retaliation|auto-trad|
|[class-08-kPjBGlHMvBqFXNq2.htm](feats/class-08-kPjBGlHMvBqFXNq2.htm)|Cursed Effigy|auto-trad|
|[class-08-kZdcoaTD849QalR9.htm](feats/class-08-kZdcoaTD849QalR9.htm)|Greater Mercy|auto-trad|
|[class-08-l8KQgN8icNrzYIav.htm](feats/class-08-l8KQgN8icNrzYIav.htm)|Form Retention|auto-trad|
|[class-08-Le30algCdKIsxmeK.htm](feats/class-08-Le30algCdKIsxmeK.htm)|Ferocious Shape|auto-trad|
|[class-08-LnSMRHjMArCkE4w1.htm](feats/class-08-LnSMRHjMArCkE4w1.htm)|Heal Mount|auto-trad|
|[class-08-LVTquA3DpqCJDika.htm](feats/class-08-LVTquA3DpqCJDika.htm)|Instinctive Strike|auto-trad|
|[class-08-lVXk0fhZqjqKilhB.htm](feats/class-08-lVXk0fhZqjqKilhB.htm)|Energy Resistance|auto-trad|
|[class-08-mf2cdCRV8uowOMOm.htm](feats/class-08-mf2cdCRV8uowOMOm.htm)|Dueling Riposte|auto-trad|
|[class-08-MhoGCLKI5zxQ4SFD.htm](feats/class-08-MhoGCLKI5zxQ4SFD.htm)|Tactical Entry|auto-trad|
|[class-08-MRxQDZFNPpUKC0CL.htm](feats/class-08-MRxQDZFNPpUKC0CL.htm)|Surging Focus|auto-trad|
|[class-08-Mvay7CiSN8snJ7DK.htm](feats/class-08-Mvay7CiSN8snJ7DK.htm)|Perpetual Breadth|auto-trad|
|[class-08-NBvhmNqneeok7ZOr.htm](feats/class-08-NBvhmNqneeok7ZOr.htm)|Overdrive Ally|auto-trad|
|[class-08-nEmaHLsZEBru1Jjv.htm](feats/class-08-nEmaHLsZEBru1Jjv.htm)|Courageous Opportunity|auto-trad|
|[class-08-nL82Dzh0QwkNkJDA.htm](feats/class-08-nL82Dzh0QwkNkJDA.htm)|Disarming Assault|auto-trad|
|[class-08-nLTTph2tgwcQghVq.htm](feats/class-08-nLTTph2tgwcQghVq.htm)|Cremate Undead|auto-trad|
|[class-08-nlXyh7828TgZIewv.htm](feats/class-08-nlXyh7828TgZIewv.htm)|Capture Magic|auto-trad|
|[class-08-NLyldMwxUWaanlzH.htm](feats/class-08-NLyldMwxUWaanlzH.htm)|Thoughtsense|auto-trad|
|[class-08-nwcsv54Vs4YBGFs9.htm](feats/class-08-nwcsv54Vs4YBGFs9.htm)|Whispering Steps|auto-trad|
|[class-08-ny0nfGTDUE4p8TtO.htm](feats/class-08-ny0nfGTDUE4p8TtO.htm)|Ubiquitous Gadgets|auto-trad|
|[class-08-oEjRfI4ATIFxDCzL.htm](feats/class-08-oEjRfI4ATIFxDCzL.htm)|Witch's Bottle|auto-trad|
|[class-08-oTTddwzF9TPNkMyd.htm](feats/class-08-oTTddwzF9TPNkMyd.htm)|Accompany|auto-trad|
|[class-08-PmJHC91WEPwrMDNW.htm](feats/class-08-PmJHC91WEPwrMDNW.htm)|Know-It-All|auto-trad|
|[class-08-po0WNUVwtaEBK8LH.htm](feats/class-08-po0WNUVwtaEBK8LH.htm)|Eerie Environs|auto-trad|
|[class-08-PP1gfRCc1YwnQGxp.htm](feats/class-08-PP1gfRCc1YwnQGxp.htm)|Dual Finisher|auto-trad|
|[class-08-pRqcm5P2ZFihSpVI.htm](feats/class-08-pRqcm5P2ZFihSpVI.htm)|Quick Shield Block|auto-trad|
|[class-08-PuyvasWeofGMrhpu.htm](feats/class-08-PuyvasWeofGMrhpu.htm)|Opportune Backstab|auto-trad|
|[class-08-Q21KzuubUBBkoges.htm](feats/class-08-Q21KzuubUBBkoges.htm)|Gigaton Strike|auto-trad|
|[class-08-qgW8uHJXJGl3DKBS.htm](feats/class-08-qgW8uHJXJGl3DKBS.htm)|Incredible Companion (Druid)|auto-trad|
|[class-08-QkKMile0qqmuVY67.htm](feats/class-08-QkKMile0qqmuVY67.htm)|Clinging Shadows Initiate|auto-trad|
|[class-08-qnMdgukNGmLWtSbZ.htm](feats/class-08-qnMdgukNGmLWtSbZ.htm)|The Harder They Fall (Kingmaker)|auto-trad|
|[class-08-rKZE8BA9IQHSSWoW.htm](feats/class-08-rKZE8BA9IQHSSWoW.htm)|Emblazon Energy|auto-trad|
|[class-08-rMPL11JRcmlutvRi.htm](feats/class-08-rMPL11JRcmlutvRi.htm)|Thrash|auto-trad|
|[class-08-rpxFVUp8BuF31DYg.htm](feats/class-08-rpxFVUp8BuF31DYg.htm)|Universal Versatility|auto-trad|
|[class-08-s2GbcUIXG1ZBurSd.htm](feats/class-08-s2GbcUIXG1ZBurSd.htm)|Hamstringing Strike|auto-trad|
|[class-08-SAOtGk9k8veaX3Ww.htm](feats/class-08-SAOtGk9k8veaX3Ww.htm)|Flamboyant Cruelty|auto-trad|
|[class-08-si8FGX2ZRxetdVHp.htm](feats/class-08-si8FGX2ZRxetdVHp.htm)|Constricting Hold|auto-trad|
|[class-08-TEH73yqZBqByO624.htm](feats/class-08-TEH73yqZBqByO624.htm)|Positioning Assault|auto-trad|
|[class-08-TOyqtUUnOkOLl1Pm.htm](feats/class-08-TOyqtUUnOkOLl1Pm.htm)|Eclectic Skill|auto-trad|
|[class-08-tPb0FVkNDE89ACbC.htm](feats/class-08-tPb0FVkNDE89ACbC.htm)|Incredible Familiar (Witch)|auto-trad|
|[class-08-Tr2SnOE2WqFIIWIK.htm](feats/class-08-Tr2SnOE2WqFIIWIK.htm)|Enlarge Companion|auto-trad|
|[class-08-u2rvvAqZBugZgcYg.htm](feats/class-08-u2rvvAqZBugZgcYg.htm)|Greater Cruelty|auto-trad|
|[class-08-UFVw57jWNC4UCfyN.htm](feats/class-08-UFVw57jWNC4UCfyN.htm)|Deimatic Display|auto-trad|
|[class-08-UIMZe4QDJQ9k4npN.htm](feats/class-08-UIMZe4QDJQ9k4npN.htm)|Stab and Blast|auto-trad|
|[class-08-uR44wELN9OlU68cL.htm](feats/class-08-uR44wELN9OlU68cL.htm)|Advanced Domain|auto-trad|
|[class-08-V9kShXu84NlORfcg.htm](feats/class-08-V9kShXu84NlORfcg.htm)|Friendly Toss|auto-trad|
|[class-08-vayNZR1bTzU1oUa3.htm](feats/class-08-vayNZR1bTzU1oUa3.htm)|Share Rage|auto-trad|
|[class-08-vhHKUooXX3PYqGaU.htm](feats/class-08-vhHKUooXX3PYqGaU.htm)|Bond Conservation|auto-trad|
|[class-08-VKNxblSUxYXQYlLr.htm](feats/class-08-VKNxblSUxYXQYlLr.htm)|Terrain Master|auto-trad|
|[class-08-vNIimhmP636VOP01.htm](feats/class-08-vNIimhmP636VOP01.htm)|Bullet Split|auto-trad|
|[class-08-VO8HbMQ79NULE4FQ.htm](feats/class-08-VO8HbMQ79NULE4FQ.htm)|Elaborate Talisman Esoterica|auto-trad|
|[class-08-VZczZNj3ozCj1Lzk.htm](feats/class-08-VZczZNj3ozCj1Lzk.htm)|Second Ally|auto-trad|
|[class-08-WDid62NmmC6NiTE6.htm](feats/class-08-WDid62NmmC6NiTE6.htm)|Smoke Curtain|auto-trad|
|[class-08-wNHUryoRzlfDCFAd.htm](feats/class-08-wNHUryoRzlfDCFAd.htm)|Soaring Shape|auto-trad|
|[class-08-x9cYkB8DrUBBwqJd.htm](feats/class-08-x9cYkB8DrUBBwqJd.htm)|Ironblood Stance|auto-trad|
|[class-08-XtIPmZ3Ihq5NJHP2.htm](feats/class-08-XtIPmZ3Ihq5NJHP2.htm)|Pinpoint Poisoner|auto-trad|
|[class-08-xtXWw3cUnVB25XSV.htm](feats/class-08-xtXWw3cUnVB25XSV.htm)|Align Armament|auto-trad|
|[class-08-y2XeMe1F18lIyo59.htm](feats/class-08-y2XeMe1F18lIyo59.htm)|Blind-Fight|auto-trad|
|[class-08-y7SYHv0DWkkwjT95.htm](feats/class-08-y7SYHv0DWkkwjT95.htm)|Retaliatory Cleansing|auto-trad|
|[class-08-YGBPIpHaOgCsa2qO.htm](feats/class-08-YGBPIpHaOgCsa2qO.htm)|Soulsight (Sorcerer)|auto-trad|
|[class-08-yTUIiE9LXBZaA7aG.htm](feats/class-08-yTUIiE9LXBZaA7aG.htm)|Whodunnit?|auto-trad|
|[class-08-Yw0qVCDu94Y5TgxQ.htm](feats/class-08-Yw0qVCDu94Y5TgxQ.htm)|Predictive Purchase (Rogue)|auto-trad|
|[class-08-ZdL8pPPV0QCkBML1.htm](feats/class-08-ZdL8pPPV0QCkBML1.htm)|Subtle Shank|auto-trad|
|[class-08-zf6Poru1jNmrO3kk.htm](feats/class-08-zf6Poru1jNmrO3kk.htm)|Bloodline Resistance|auto-trad|
|[class-08-ZlL18xGsa76isqkX.htm](feats/class-08-ZlL18xGsa76isqkX.htm)|Runic Impression|auto-trad|
|[class-08-Zn2uUL5i3MAZ0Zwc.htm](feats/class-08-Zn2uUL5i3MAZ0Zwc.htm)|Standby Spell|auto-trad|
|[class-08-zQeglKcmBXvqfABR.htm](feats/class-08-zQeglKcmBXvqfABR.htm)|Brain Drain|auto-trad|
|[class-08-zXKfKKOxht0b0XNL.htm](feats/class-08-zXKfKKOxht0b0XNL.htm)|Sticky Bomb|auto-trad|
|[class-10-07jxXvRZ8nD3JLF4.htm](feats/class-10-07jxXvRZ8nD3JLF4.htm)|Imposing Destrier|auto-trad|
|[class-10-1k3H7cnARIzAVCsm.htm](feats/class-10-1k3H7cnARIzAVCsm.htm)|Fearsome Brute|auto-trad|
|[class-10-2Nu4ZdKQM8hx8x5D.htm](feats/class-10-2Nu4ZdKQM8hx8x5D.htm)|Sneak Savant|auto-trad|
|[class-10-2OYJOFaEkhc8dFbl.htm](feats/class-10-2OYJOFaEkhc8dFbl.htm)|Knockback Strike|auto-trad|
|[class-10-4IeAAmx2vZEHmRLX.htm](feats/class-10-4IeAAmx2vZEHmRLX.htm)|Witch's Communion|auto-trad|
|[class-10-4VbfFPuFpbGLMMKF.htm](feats/class-10-4VbfFPuFpbGLMMKF.htm)|Disarming Twist|auto-trad|
|[class-10-5csEOLLbYUWJDJoS.htm](feats/class-10-5csEOLLbYUWJDJoS.htm)|Master Monster Hunter|auto-trad|
|[class-10-5tlTRfyPXkGS9Coq.htm](feats/class-10-5tlTRfyPXkGS9Coq.htm)|Lunging Spellstrike|auto-trad|
|[class-10-647GZOyhWgcjw6Jg.htm](feats/class-10-647GZOyhWgcjw6Jg.htm)|Elucidating Mercy|auto-trad|
|[class-10-6xBu4BewIkOIt9M0.htm](feats/class-10-6xBu4BewIkOIt9M0.htm)|Cut From The Air|auto-trad|
|[class-10-85V3vdew0gykEtmu.htm](feats/class-10-85V3vdew0gykEtmu.htm)|Healing Transformation|auto-trad|
|[class-10-85xm82Z005CUNBMB.htm](feats/class-10-85xm82Z005CUNBMB.htm)|Twin Weakness|auto-trad|
|[class-10-9kY9B5WgtEleOicn.htm](feats/class-10-9kY9B5WgtEleOicn.htm)|Resounding Blow|auto-trad|
|[class-10-9uvymmdphxsD3yEd.htm](feats/class-10-9uvymmdphxsD3yEd.htm)|Dazzling Block|auto-trad|
|[class-10-9VGmE7X4aK2W8YWj.htm](feats/class-10-9VGmE7X4aK2W8YWj.htm)|Dueling Dance (Swashbuckler)|auto-trad|
|[class-10-9XXtDeRF2egCCzcx.htm](feats/class-10-9XXtDeRF2egCCzcx.htm)|Tectonic Stomp|auto-trad|
|[class-10-A0ZQpGSB7pvIDiou.htm](feats/class-10-A0ZQpGSB7pvIDiou.htm)|Improved Command Undead|auto-trad|
|[class-10-a2VAaXMzlQqALJeH.htm](feats/class-10-a2VAaXMzlQqALJeH.htm)|Share Weakness|auto-trad|
|[class-10-aajcuwKYihsinPCt.htm](feats/class-10-aajcuwKYihsinPCt.htm)|Wronged Monk's Wrath|auto-trad|
|[class-10-aMef2VM4mSxl0pmy.htm](feats/class-10-aMef2VM4mSxl0pmy.htm)|Replenishment of War|auto-trad|
|[class-10-Asb0UsQqeATsxqFJ.htm](feats/class-10-Asb0UsQqeATsxqFJ.htm)|Courageous Assault|auto-trad|
|[class-10-aWk0JaqVaUf2Cz9a.htm](feats/class-10-aWk0JaqVaUf2Cz9a.htm)|Twin Shot Knockdown|auto-trad|
|[class-10-b7KZ7Fg5u5z2gqvt.htm](feats/class-10-b7KZ7Fg5u5z2gqvt.htm)|Called Shot|auto-trad|
|[class-10-BJKTUGplI9nwhJxg.htm](feats/class-10-BJKTUGplI9nwhJxg.htm)|Agile Grace|auto-trad|
|[class-10-C0ozuEhrKh9A1wMO.htm](feats/class-10-C0ozuEhrKh9A1wMO.htm)|Overpowering Charge|auto-trad|
|[class-10-C6BIHPqYiWM0wlvv.htm](feats/class-10-C6BIHPqYiWM0wlvv.htm)|Signature Spell Expansion (Psychic)|auto-trad|
|[class-10-CAk1NNG4aO0VuHnZ.htm](feats/class-10-CAk1NNG4aO0VuHnZ.htm)|Ongoing Strategy|auto-trad|
|[class-10-CkK7WwaWnrLXK9sW.htm](feats/class-10-CkK7WwaWnrLXK9sW.htm)|Methodical Debilitations|auto-trad|
|[class-10-Cn4w9U7uk5m1bb2S.htm](feats/class-10-Cn4w9U7uk5m1bb2S.htm)|Greater Debilitating Bomb|auto-trad|
|[class-10-cvo3DIL0BIRrDkQ6.htm](feats/class-10-cvo3DIL0BIRrDkQ6.htm)|Ode To Ouroboros|auto-trad|
|[class-10-cznEQ1W61MSaXW0u.htm](feats/class-10-cznEQ1W61MSaXW0u.htm)|Great Cleave|auto-trad|
|[class-10-d8DI7wLxtUy99g9K.htm](feats/class-10-d8DI7wLxtUy99g9K.htm)|Emotional Surge|auto-trad|
|[class-10-DLkMoVb8qb4qxnx3.htm](feats/class-10-DLkMoVb8qb4qxnx3.htm)|Targeting Finisher|auto-trad|
|[class-10-dnm6c3nzGDHt3A76.htm](feats/class-10-dnm6c3nzGDHt3A76.htm)|Greenwatcher|auto-trad|
|[class-10-DpRMdytpPiCypmkJ.htm](feats/class-10-DpRMdytpPiCypmkJ.htm)|Greater Bloodline|auto-trad|
|[class-10-dutiFC41YFllm8fM.htm](feats/class-10-dutiFC41YFllm8fM.htm)|House of Imaginary Walls|auto-trad|
|[class-10-DZcy4sY07w0zPsDb.htm](feats/class-10-DZcy4sY07w0zPsDb.htm)|Rapid Recharge|auto-trad|
|[class-10-dzeiVab4Cogzl5XS.htm](feats/class-10-dzeiVab4Cogzl5XS.htm)|Gift of the Hoard|auto-trad|
|[class-10-E1WXnYE2QwhHQxQb.htm](feats/class-10-E1WXnYE2QwhHQxQb.htm)|Sleeper Hold|auto-trad|
|[class-10-F0MYBfiyOD8YHq5t.htm](feats/class-10-F0MYBfiyOD8YHq5t.htm)|Elemental Shape|auto-trad|
|[class-10-FIsMdgvGji5Nci8l.htm](feats/class-10-FIsMdgvGji5Nci8l.htm)|Devoted Focus|auto-trad|
|[class-10-FLQWJ2CIv9mCfSAx.htm](feats/class-10-FLQWJ2CIv9mCfSAx.htm)|Elastic Mutagen|auto-trad|
|[class-10-FWIQ3m5KZDWzDg47.htm](feats/class-10-FWIQ3m5KZDWzDg47.htm)|Wind Jump|auto-trad|
|[class-10-fzERYW7BJQoxlvoD.htm](feats/class-10-fzERYW7BJQoxlvoD.htm)|Corpse-Killer's Defiance|auto-trad|
|[class-10-Gcliatty0MGYbTVV.htm](feats/class-10-Gcliatty0MGYbTVV.htm)|Oracular Warning|auto-trad|
|[class-10-gdGusdEKt5zmh3rR.htm](feats/class-10-gdGusdEKt5zmh3rR.htm)|Pushing Attack|auto-trad|
|[class-10-GJIAecRq1bD2r8O0.htm](feats/class-10-GJIAecRq1bD2r8O0.htm)|Twin Riposte|auto-trad|
|[class-10-GLbl3qoWCvvjJr4S.htm](feats/class-10-GLbl3qoWCvvjJr4S.htm)|Tangle Of Battle|auto-trad|
|[class-10-gSc4ZUXkesN5vKrm.htm](feats/class-10-gSc4ZUXkesN5vKrm.htm)|Debilitating Shot|auto-trad|
|[class-10-gyVcJfZTmBytLsXq.htm](feats/class-10-gyVcJfZTmBytLsXq.htm)|Expanded Splash|auto-trad|
|[class-10-H0tffYs7ODMQ3JJj.htm](feats/class-10-H0tffYs7ODMQ3JJj.htm)|Terrain Transposition|auto-trad|
|[class-10-h5DzKmKDADGhvmF9.htm](feats/class-10-h5DzKmKDADGhvmF9.htm)|Certain Strike|auto-trad|
|[class-10-HjinlKihkadhkQ4Z.htm](feats/class-10-HjinlKihkadhkQ4Z.htm)|Terrifying Howl|auto-trad|
|[class-10-HKNOA5WGBWGEpdmH.htm](feats/class-10-HKNOA5WGBWGEpdmH.htm)|Redirecting Shot|auto-trad|
|[class-10-I9rSWQyueWHQyNxe.htm](feats/class-10-I9rSWQyueWHQyNxe.htm)|Plant Shape|auto-trad|
|[class-10-IeiTuTZExH5DQOqH.htm](feats/class-10-IeiTuTZExH5DQOqH.htm)|Burrowing Form|auto-trad|
|[class-10-j5Xjr8vZuBhCixIr.htm](feats/class-10-j5Xjr8vZuBhCixIr.htm)|Side by Side (Druid)|auto-trad|
|[class-10-JQs2O2TTgKWXgJgZ.htm](feats/class-10-JQs2O2TTgKWXgJgZ.htm)|Consecrate Spell|auto-trad|
|[class-10-JxZobgFhLBNcFCwE.htm](feats/class-10-JxZobgFhLBNcFCwE.htm)|Dream Guise|auto-trad|
|[class-10-K0gyvvX0S2FdJZ5T.htm](feats/class-10-K0gyvvX0S2FdJZ5T.htm)|Weighty Impact|auto-trad|
|[class-10-Km6YO7Ky2bEwhAFD.htm](feats/class-10-Km6YO7Ky2bEwhAFD.htm)|Greater Awakened Power|auto-trad|
|[class-10-KNhmeUpZCSW5eHqg.htm](feats/class-10-KNhmeUpZCSW5eHqg.htm)|Scour The Library|auto-trad|
|[class-10-knJlRpltciLNTZba.htm](feats/class-10-knJlRpltciLNTZba.htm)|Protective Bond|auto-trad|
|[class-10-kQEIPYoKTt69yXxV.htm](feats/class-10-kQEIPYoKTt69yXxV.htm)|Mirror Shield|auto-trad|
|[class-10-KR78kinMmAZQHeoa.htm](feats/class-10-KR78kinMmAZQHeoa.htm)|Heroic Recovery|auto-trad|
|[class-10-kUv9eiP8Zhck70WZ.htm](feats/class-10-kUv9eiP8Zhck70WZ.htm)|Eldritch Debilitations|auto-trad|
|[class-10-lG4dYrnkE42IgnGG.htm](feats/class-10-lG4dYrnkE42IgnGG.htm)|Energy Ward|auto-trad|
|[class-10-lIr2kC561L7oX290.htm](feats/class-10-lIr2kC561L7oX290.htm)|Rebounding Assault|auto-trad|
|[class-10-lTwi4lyVk1UXomhK.htm](feats/class-10-lTwi4lyVk1UXomhK.htm)|Sustaining Steel|auto-trad|
|[class-10-LvmYfUGX3uDCpIHY.htm](feats/class-10-LvmYfUGX3uDCpIHY.htm)|Electrify Armor|auto-trad|
|[class-10-McnLGEZnUbtYCNDW.htm](feats/class-10-McnLGEZnUbtYCNDW.htm)|Lock On|auto-trad|
|[class-10-MgqRwyL8PWyYvoZs.htm](feats/class-10-MgqRwyL8PWyYvoZs.htm)|Energy Fusion|auto-trad|
|[class-10-Mvw3ZFrdwMHedAxY.htm](feats/class-10-Mvw3ZFrdwMHedAxY.htm)|Merciful Elixir|auto-trad|
|[class-10-NPhH9XkHV0DG4WS9.htm](feats/class-10-NPhH9XkHV0DG4WS9.htm)|Transpose|auto-trad|
|[class-10-nx7UPAwAyno2rM9f.htm](feats/class-10-nx7UPAwAyno2rM9f.htm)|Thaumaturge's Investiture|auto-trad|
|[class-10-o8qiAUuDHfurmgpP.htm](feats/class-10-o8qiAUuDHfurmgpP.htm)|Dimensional Disappearance|auto-trad|
|[class-10-Ob4w36tsd1WKSxZo.htm](feats/class-10-Ob4w36tsd1WKSxZo.htm)|Eerie Proclamation|auto-trad|
|[class-10-oD4JyvTJj4kwe5vb.htm](feats/class-10-oD4JyvTJj4kwe5vb.htm)|Warden's Step|auto-trad|
|[class-10-oiauCibmdgJ91kNI.htm](feats/class-10-oiauCibmdgJ91kNI.htm)|Cascading Ray|auto-trad|
|[class-10-oltE3dIYZlM8s5BI.htm](feats/class-10-oltE3dIYZlM8s5BI.htm)|Blazing Streak|auto-trad|
|[class-10-otBBb0ndASgPdAXW.htm](feats/class-10-otBBb0ndASgPdAXW.htm)|Penetrating Shot|auto-trad|
|[class-10-p2I4o9Cc6UrXvjhO.htm](feats/class-10-p2I4o9Cc6UrXvjhO.htm)|Furious Sprint|auto-trad|
|[class-10-PIVC14saumGNKWbo.htm](feats/class-10-PIVC14saumGNKWbo.htm)|Annotate Composition|auto-trad|
|[class-10-PSqxbLzKKzbMUnU9.htm](feats/class-10-PSqxbLzKKzbMUnU9.htm)|Entwined Energy Ki|auto-trad|
|[class-10-pVLdMOqYwul745k3.htm](feats/class-10-pVLdMOqYwul745k3.htm)|Knockback|auto-trad|
|[class-10-qKpaZF0U9VV0YwTJ.htm](feats/class-10-qKpaZF0U9VV0YwTJ.htm)|Litany Of Self-Interest|auto-trad|
|[class-10-QO8l5Dao8HnaFQE4.htm](feats/class-10-QO8l5Dao8HnaFQE4.htm)|Impressive Landing|auto-trad|
|[class-10-qqe5VmcO1s8iTOfd.htm](feats/class-10-qqe5VmcO1s8iTOfd.htm)|Camouflage|auto-trad|
|[class-10-qUSWPWxYF8gfhfHM.htm](feats/class-10-qUSWPWxYF8gfhfHM.htm)|Helpful Tinkering|auto-trad|
|[class-10-R40U8hF0hWyRUze8.htm](feats/class-10-R40U8hF0hWyRUze8.htm)|Through Spell|auto-trad|
|[class-10-R7EaZPYtsy5H8lwn.htm](feats/class-10-R7EaZPYtsy5H8lwn.htm)|Merciless Rend|auto-trad|
|[class-10-REgTwIVgI3j1FQiJ.htm](feats/class-10-REgTwIVgI3j1FQiJ.htm)|Harden Flesh|auto-trad|
|[class-10-RfOnsZrmT6z2ajBN.htm](feats/class-10-RfOnsZrmT6z2ajBN.htm)|Siphon Life|auto-trad|
|[class-10-RWccAJJ2PaOI0Byp.htm](feats/class-10-RWccAJJ2PaOI0Byp.htm)|Litany Against Sloth|auto-trad|
|[class-10-rzaoi5Roef9zO22G.htm](feats/class-10-rzaoi5Roef9zO22G.htm)|Combat Reflexes|auto-trad|
|[class-10-Sc9clbAXe97vlzxM.htm](feats/class-10-Sc9clbAXe97vlzxM.htm)|Suspect of Opportunity|auto-trad|
|[class-10-SlMkuKMny7hWdNxL.htm](feats/class-10-SlMkuKMny7hWdNxL.htm)|Derring-do|auto-trad|
|[class-10-stwRJTOKUru9AmQC.htm](feats/class-10-stwRJTOKUru9AmQC.htm)|Meteoric Spellstrike|auto-trad|
|[class-10-SYVM6Z3sS50e5Vbd.htm](feats/class-10-SYVM6Z3sS50e5Vbd.htm)|Deny Support|auto-trad|
|[class-10-tCuMXQ0yMrCNwzqW.htm](feats/class-10-tCuMXQ0yMrCNwzqW.htm)|Resilient Touch|auto-trad|
|[class-10-tonJdHGheKZ16tMI.htm](feats/class-10-tonJdHGheKZ16tMI.htm)|Unusual Composition|auto-trad|
|[class-10-ToZw6ZjB0JhWwMeR.htm](feats/class-10-ToZw6ZjB0JhWwMeR.htm)|Deflecting Shot|auto-trad|
|[class-10-tSmd0cxq9wokSCh4.htm](feats/class-10-tSmd0cxq9wokSCh4.htm)|Castigating Weapon|auto-trad|
|[class-10-tXNfWDa6P7bCKrCt.htm](feats/class-10-tXNfWDa6P7bCKrCt.htm)|Overwhelming Energy|auto-trad|
|[class-10-tY3Zg0l14CdoKPpt.htm](feats/class-10-tY3Zg0l14CdoKPpt.htm)|Unstable Concoction|auto-trad|
|[class-10-u2fgdFIdQDplKOS3.htm](feats/class-10-u2fgdFIdQDplKOS3.htm)|Peafowl Strut|auto-trad|
|[class-10-U52NMdeSNbjSSRHE.htm](feats/class-10-U52NMdeSNbjSSRHE.htm)|Incredible Companion (Ranger)|auto-trad|
|[class-10-u5DBg0LrBUKP0JsJ.htm](feats/class-10-u5DBg0LrBUKP0JsJ.htm)|Scroll Savant|auto-trad|
|[class-10-UjYHf7rlWTFJ0v0A.htm](feats/class-10-UjYHf7rlWTFJ0v0A.htm)|Signature Spell Expansion|auto-trad|
|[class-10-uotQ9yqetPoAWrfW.htm](feats/class-10-uotQ9yqetPoAWrfW.htm)|Reflexive Riposte|auto-trad|
|[class-10-VC8qdcCxtzCmG98M.htm](feats/class-10-VC8qdcCxtzCmG98M.htm)|Impose Order (Psychic)|auto-trad|
|[class-10-VF5XFpzBlqUFd8Mw.htm](feats/class-10-VF5XFpzBlqUFd8Mw.htm)|Glass Skin|auto-trad|
|[class-10-vgsMKjAbRDNxT5TK.htm](feats/class-10-vgsMKjAbRDNxT5TK.htm)|Determined Dash|auto-trad|
|[class-10-VJl2xHDKr0HxTUrs.htm](feats/class-10-VJl2xHDKr0HxTUrs.htm)|Symphony Of The Unfettered Heart|auto-trad|
|[class-10-VsTmB32x9673ONJ0.htm](feats/class-10-VsTmB32x9673ONJ0.htm)|Shield of Reckoning|auto-trad|
|[class-10-vxDL78Yz0dPZdAJ5.htm](feats/class-10-vxDL78Yz0dPZdAJ5.htm)|Precious Munitions|auto-trad|
|[class-10-vXH0HWMHzevA1Wox.htm](feats/class-10-vXH0HWMHzevA1Wox.htm)|Winding Flow|auto-trad|
|[class-10-w3qyriA1YnzXaas3.htm](feats/class-10-w3qyriA1YnzXaas3.htm)|Shield Of Faith|auto-trad|
|[class-10-W7aT1UJOVFkYdQti.htm](feats/class-10-W7aT1UJOVFkYdQti.htm)|Hunter's Vision|auto-trad|
|[class-10-wIJC00ODLq9WYc1m.htm](feats/class-10-wIJC00ODLq9WYc1m.htm)|Shared Avoidance|auto-trad|
|[class-10-wvJBIzgS298ZRp6w.htm](feats/class-10-wvJBIzgS298ZRp6w.htm)|Precise Debilitations|auto-trad|
|[class-10-Xdmz09kzLjIxWz9C.htm](feats/class-10-Xdmz09kzLjIxWz9C.htm)|Tactical Debilitations|auto-trad|
|[class-10-xmf6oUYarFJGajtr.htm](feats/class-10-xmf6oUYarFJGajtr.htm)|Holy Light|auto-trad|
|[class-10-XPTSCUoA8c4o9RgQ.htm](feats/class-10-XPTSCUoA8c4o9RgQ.htm)|Distracting Explosion|auto-trad|
|[class-10-Xw7qG0SHepXx24vl.htm](feats/class-10-Xw7qG0SHepXx24vl.htm)|Prevailing Position|auto-trad|
|[class-10-YFPmD8BHv0XaF55G.htm](feats/class-10-YFPmD8BHv0XaF55G.htm)|Distant Wandering|auto-trad|
|[class-10-Yk3QGpalWDn5MhBV.htm](feats/class-10-Yk3QGpalWDn5MhBV.htm)|Silencing Strike|auto-trad|
|[class-10-YlaLePtgDXGB0Fsf.htm](feats/class-10-YlaLePtgDXGB0Fsf.htm)|Penetrating Fire|auto-trad|
|[class-10-YluQPhevo0LKdF1p.htm](feats/class-10-YluQPhevo0LKdF1p.htm)|Just One More Thing|auto-trad|
|[class-10-YNk0BekymS3bBvCT.htm](feats/class-10-YNk0BekymS3bBvCT.htm)|Ancestral Mage|auto-trad|
|[class-10-ySr510TkpUO3N6Ty.htm](feats/class-10-ySr510TkpUO3N6Ty.htm)|Trick Shot|auto-trad|
|[class-10-yTh9QwAf0hadP91j.htm](feats/class-10-yTh9QwAf0hadP91j.htm)|Improved Knockdown|auto-trad|
|[class-10-YV4X9u5Yuf0xvoCh.htm](feats/class-10-YV4X9u5Yuf0xvoCh.htm)|Surging Might|auto-trad|
|[class-10-z2ptq23nNBOeEI7H.htm](feats/class-10-z2ptq23nNBOeEI7H.htm)|Quickened Casting|auto-trad|
|[class-10-Z9gzWFk3qom2z904.htm](feats/class-10-Z9gzWFk3qom2z904.htm)|Come and Get Me|auto-trad|
|[class-10-ZFkCMl63ogK55Otq.htm](feats/class-10-ZFkCMl63ogK55Otq.htm)|Major Lesson|auto-trad|
|[class-10-zKL1lRcIbFblp2M2.htm](feats/class-10-zKL1lRcIbFblp2M2.htm)|Potent Poisoner|auto-trad|
|[class-10-zt5CWn3UrPViwaB3.htm](feats/class-10-zt5CWn3UrPViwaB3.htm)|Vicious Debilitations|auto-trad|
|[class-12-1JTEWgonWlmeCE3w.htm](feats/class-12-1JTEWgonWlmeCE3w.htm)|Liberating Stride|auto-trad|
|[class-12-2aFtxqRPnC4OXUGC.htm](feats/class-12-2aFtxqRPnC4OXUGC.htm)|Warden's Focus|auto-trad|
|[class-12-2UogDoEJ2M2tQFxn.htm](feats/class-12-2UogDoEJ2M2tQFxn.htm)|Expert Spellcasting|auto-trad|
|[class-12-3WUL8ExEkZDRYeBu.htm](feats/class-12-3WUL8ExEkZDRYeBu.htm)|Diviner Sense|auto-trad|
|[class-12-4l0ewDg4gMfkU2pi.htm](feats/class-12-4l0ewDg4gMfkU2pi.htm)|Link Focus|auto-trad|
|[class-12-4st3fSYSrJrxwHOP.htm](feats/class-12-4st3fSYSrJrxwHOP.htm)|Contingency Gadgets|auto-trad|
|[class-12-56BMXlQlZtg39SMV.htm](feats/class-12-56BMXlQlZtg39SMV.htm)|Lasting Doubt|auto-trad|
|[class-12-5YcnoTYKvEtkWiHh.htm](feats/class-12-5YcnoTYKvEtkWiHh.htm)|Side by Side (Ranger)|auto-trad|
|[class-12-7HPXQvPH3ovwtVae.htm](feats/class-12-7HPXQvPH3ovwtVae.htm)|Furious Grab|auto-trad|
|[class-12-7r79fZBr37qBN2EF.htm](feats/class-12-7r79fZBr37qBN2EF.htm)|Shattering Shot|auto-trad|
|[class-12-7sFhBYoz5GSBFNbY.htm](feats/class-12-7sFhBYoz5GSBFNbY.htm)|Aura of Faith|auto-trad|
|[class-12-7spk6rZPiNk2S0yA.htm](feats/class-12-7spk6rZPiNk2S0yA.htm)|Blood in the Air|auto-trad|
|[class-12-8cbSVw8RnVzy5USe.htm](feats/class-12-8cbSVw8RnVzy5USe.htm)|Enigma's Knowledge|auto-trad|
|[class-12-8FxKcuFtOrqsl1FH.htm](feats/class-12-8FxKcuFtOrqsl1FH.htm)|Roll With it (Kingmaker)|auto-trad|
|[class-12-8INrcMUv5vzWMG3X.htm](feats/class-12-8INrcMUv5vzWMG3X.htm)|Sunder Spell|auto-trad|
|[class-12-9Ht1eyBHsB1swpeE.htm](feats/class-12-9Ht1eyBHsB1swpeE.htm)|Uncanny Bombs|auto-trad|
|[class-12-9u0uW1vZThRayXk2.htm](feats/class-12-9u0uW1vZThRayXk2.htm)|Shaped Contaminant|auto-trad|
|[class-12-aFUxNGur3Hma8DKy.htm](feats/class-12-aFUxNGur3Hma8DKy.htm)|Predator's Pounce|auto-trad|
|[class-12-APfPNpUQlKlCAJkS.htm](feats/class-12-APfPNpUQlKlCAJkS.htm)|Miraculous Intervention|auto-trad|
|[class-12-asRbkgW59DZUpvAq.htm](feats/class-12-asRbkgW59DZUpvAq.htm)|Extend Elixir|auto-trad|
|[class-12-AXy4A7zTYk1JAiOV.htm](feats/class-12-AXy4A7zTYk1JAiOV.htm)|Magic Sense|auto-trad|
|[class-12-AYXherMu9gFTyXjp.htm](feats/class-12-AYXherMu9gFTyXjp.htm)|Deadly Poison Weapon|auto-trad|
|[class-12-bbpFExz1RXCkOhnb.htm](feats/class-12-bbpFExz1RXCkOhnb.htm)|Sense Ki|auto-trad|
|[class-12-BlfgmJHjDyTVGdPs.htm](feats/class-12-BlfgmJHjDyTVGdPs.htm)|Wandering Oasis|auto-trad|
|[class-12-BQrhDpLIp9zjbjEP.htm](feats/class-12-BQrhDpLIp9zjbjEP.htm)|Distracting Shot|auto-trad|
|[class-12-bqZkAFS6eq9TKXMO.htm](feats/class-12-bqZkAFS6eq9TKXMO.htm)|Dragon's Rage Wings|auto-trad|
|[class-12-c7mCj6wUdb47rrnw.htm](feats/class-12-c7mCj6wUdb47rrnw.htm)|Ricochet Shot|auto-trad|
|[class-12-COe0bYyVCyC78rzP.htm](feats/class-12-COe0bYyVCyC78rzP.htm)|Second Sting|auto-trad|
|[class-12-Cy5W8U4yN9P1EvBy.htm](feats/class-12-Cy5W8U4yN9P1EvBy.htm)|Amplifying Touch|auto-trad|
|[class-12-d1jQ0HyIOyUdCCaN.htm](feats/class-12-d1jQ0HyIOyUdCCaN.htm)|Spring Attack|auto-trad|
|[class-12-d1ktdX1Fk37dG5ms.htm](feats/class-12-d1ktdX1Fk37dG5ms.htm)|Defensive Recovery|auto-trad|
|[class-12-D2KSVHPRlBEibrV8.htm](feats/class-12-D2KSVHPRlBEibrV8.htm)|Cheat Death|auto-trad|
|[class-12-d6Vb8D9yOX93mdUI.htm](feats/class-12-d6Vb8D9yOX93mdUI.htm)|Flinging Shove|auto-trad|
|[class-12-d7DQhCJKYcLxpHen.htm](feats/class-12-d7DQhCJKYcLxpHen.htm)|Focused Shot|auto-trad|
|[class-12-dLof0i6LPcChJSBR.htm](feats/class-12-dLof0i6LPcChJSBR.htm)|Pale Horse|auto-trad|
|[class-12-Dwxi1q1OWB1ufFvy.htm](feats/class-12-Dwxi1q1OWB1ufFvy.htm)|Overwhelming Breath|auto-trad|
|[class-12-ecV3Nljvs4FOBS27.htm](feats/class-12-ecV3Nljvs4FOBS27.htm)|Reverberate|auto-trad|
|[class-12-eGZzifQWCf5gSq2y.htm](feats/class-12-eGZzifQWCf5gSq2y.htm)|Unshakable Grit|auto-trad|
|[class-12-emjWa77ltL5FytvA.htm](feats/class-12-emjWa77ltL5FytvA.htm)|Spring from the Shadows|auto-trad|
|[class-12-ENoRkTXtdfsbs98S.htm](feats/class-12-ENoRkTXtdfsbs98S.htm)|Domain Fluency|auto-trad|
|[class-12-eSoMkHGw38ld4gj2.htm](feats/class-12-eSoMkHGw38ld4gj2.htm)|Shared Warding|auto-trad|
|[class-12-fCDC53WOOYrsyVIR.htm](feats/class-12-fCDC53WOOYrsyVIR.htm)|Incredible Ricochet|auto-trad|
|[class-12-fcFrxvqbIX6k71os.htm](feats/class-12-fcFrxvqbIX6k71os.htm)|Meditative Focus|auto-trad|
|[class-12-fLlCodqKXyXbZR7C.htm](feats/class-12-fLlCodqKXyXbZR7C.htm)|Wave Spiral|auto-trad|
|[class-12-FYz5eQeTox9IDkSd.htm](feats/class-12-FYz5eQeTox9IDkSd.htm)|Dueling Dance (Fighter)|auto-trad|
|[class-12-GMrJdGwajADbL1y5.htm](feats/class-12-GMrJdGwajADbL1y5.htm)|Diamond Soul|auto-trad|
|[class-12-gO729iC9b5ypes2K.htm](feats/class-12-gO729iC9b5ypes2K.htm)|Spirit's Wrath|auto-trad|
|[class-12-gVLICIDQMvWN5D89.htm](feats/class-12-gVLICIDQMvWN5D89.htm)|Greater Spiritual Evolution|auto-trad|
|[class-12-hPanopG3TbXKr52O.htm](feats/class-12-hPanopG3TbXKr52O.htm)|Pesh Skin|auto-trad|
|[class-12-hPDerDCYmag3s0dP.htm](feats/class-12-hPDerDCYmag3s0dP.htm)|Paragon's Guard|auto-trad|
|[class-12-HPETR6zq8L6YJyi1.htm](feats/class-12-HPETR6zq8L6YJyi1.htm)|Improved Knockback|auto-trad|
|[class-12-HSW3N9pfHhM7upRB.htm](feats/class-12-HSW3N9pfHhM7upRB.htm)|Greater Revelation|auto-trad|
|[class-12-Ij6BBPzZvOFZ3prs.htm](feats/class-12-Ij6BBPzZvOFZ3prs.htm)|Felling Shot|auto-trad|
|[class-12-IMPP5pa8AmvCby4W.htm](feats/class-12-IMPP5pa8AmvCby4W.htm)|Clever Counterspell|auto-trad|
|[class-12-ixIKF2LMmbFthI8Z.htm](feats/class-12-ixIKF2LMmbFthI8Z.htm)|Towering Size|auto-trad|
|[class-12-j20djiiuVwUf8MqL.htm](feats/class-12-j20djiiuVwUf8MqL.htm)|Embrace The Pain|auto-trad|
|[class-12-JddrdXv4pfdePsGK.htm](feats/class-12-JddrdXv4pfdePsGK.htm)|Petrified Skin|auto-trad|
|[class-12-JfLIWyqgEVggWaBL.htm](feats/class-12-JfLIWyqgEVggWaBL.htm)|Magic Sense (Magus)|auto-trad|
|[class-12-jNeIaFUFSGUXoSON.htm](feats/class-12-jNeIaFUFSGUXoSON.htm)|Divine Wall|auto-trad|
|[class-12-jNzjecRGyyAqkkrm.htm](feats/class-12-jNzjecRGyyAqkkrm.htm)|Deep Freeze|auto-trad|
|[class-12-LI9VtCaL5ZRk0Wo8.htm](feats/class-12-LI9VtCaL5ZRk0Wo8.htm)|Stance Savant (Monk)|auto-trad|
|[class-12-lPTcPIshChHWz4J6.htm](feats/class-12-lPTcPIshChHWz4J6.htm)|Critical Debilitation|auto-trad|
|[class-12-lv8jRWK7bv7dR6SM.htm](feats/class-12-lv8jRWK7bv7dR6SM.htm)|Gigavolt|auto-trad|
|[class-12-mgs7vxq6d3hQoswa.htm](feats/class-12-mgs7vxq6d3hQoswa.htm)|Improved Dueling Riposte|auto-trad|
|[class-12-mSDKKRgK6sRMjqQo.htm](feats/class-12-mSDKKRgK6sRMjqQo.htm)|Summoner's Call|auto-trad|
|[class-12-mTkbgFOHJUBl0Qwg.htm](feats/class-12-mTkbgFOHJUBl0Qwg.htm)|Shared Replenishment|auto-trad|
|[class-12-mZcI1NKtQhteAQLn.htm](feats/class-12-mZcI1NKtQhteAQLn.htm)|Silver's Refrain|auto-trad|
|[class-12-nBWoZ311FXFJC8Zl.htm](feats/class-12-nBWoZ311FXFJC8Zl.htm)|Dodging Roll|auto-trad|
|[class-12-nI67dTzKYg5kKMsd.htm](feats/class-12-nI67dTzKYg5kKMsd.htm)|Titan's Stature|auto-trad|
|[class-12-NJyyxInJ743OotKf.htm](feats/class-12-NJyyxInJ743OotKf.htm)|Deadeye|auto-trad|
|[class-12-nsFnOLqYSkGWFhLD.htm](feats/class-12-nsFnOLqYSkGWFhLD.htm)|Affliction Mercy|auto-trad|
|[class-12-NtaOLg9meDYfg8aV.htm](feats/class-12-NtaOLg9meDYfg8aV.htm)|Blade of Justice|auto-trad|
|[class-12-nWd7m0yRcIEVUy7O.htm](feats/class-12-nWd7m0yRcIEVUy7O.htm)|Elaborate Scroll Esoterica|auto-trad|
|[class-12-O1qdoz5N3G4yvHcH.htm](feats/class-12-O1qdoz5N3G4yvHcH.htm)|Greater Physical Evolution|auto-trad|
|[class-12-OrNcWu1Y7c2O5zU6.htm](feats/class-12-OrNcWu1Y7c2O5zU6.htm)|Lightning Snares|auto-trad|
|[class-12-oUcB71V1jVaM8SFx.htm](feats/class-12-oUcB71V1jVaM8SFx.htm)|Shared Assault|auto-trad|
|[class-12-p0jZhb8PSswUsZaz.htm](feats/class-12-p0jZhb8PSswUsZaz.htm)|Dragon Shape|auto-trad|
|[class-12-P13ZhZcR67Ev0vrS.htm](feats/class-12-P13ZhZcR67Ev0vrS.htm)|Disrupt Ki|auto-trad|
|[class-12-p2tFR4yBauu8t3mC.htm](feats/class-12-p2tFR4yBauu8t3mC.htm)|Hex Focus|auto-trad|
|[class-12-pbD4lfAPkK1NNag0.htm](feats/class-12-pbD4lfAPkK1NNag0.htm)|Double Prey|auto-trad|
|[class-12-PEszRpnrcB7VPS9G.htm](feats/class-12-PEszRpnrcB7VPS9G.htm)|Gruesome Strike|auto-trad|
|[class-12-pM57xTqy5CMYZeqD.htm](feats/class-12-pM57xTqy5CMYZeqD.htm)|Obscured Emergence|auto-trad|
|[class-12-pm9PS32YNLJ2wp4o.htm](feats/class-12-pm9PS32YNLJ2wp4o.htm)|Reason Rapidly|auto-trad|
|[class-12-PmhprI0vr3zTOkok.htm](feats/class-12-PmhprI0vr3zTOkok.htm)|Foreseen Failure|auto-trad|
|[class-12-pmVaj53saxLE28Pl.htm](feats/class-12-pmVaj53saxLE28Pl.htm)|Overwhelming Spellstrike|auto-trad|
|[class-12-pmz1itHp13JtcrjW.htm](feats/class-12-pmz1itHp13JtcrjW.htm)|Unbalancing Sweep|auto-trad|
|[class-12-pVDgiaqu1RbCOhuv.htm](feats/class-12-pVDgiaqu1RbCOhuv.htm)|Foresee Danger|auto-trad|
|[class-12-Px1QZY0NdO9WAQQS.htm](feats/class-12-Px1QZY0NdO9WAQQS.htm)|Mobile Finisher|auto-trad|
|[class-12-PxBQ4JdaPS2KTAG7.htm](feats/class-12-PxBQ4JdaPS2KTAG7.htm)|Necromancer's Visage|auto-trad|
|[class-12-Q4puGx4kBMXy45fa.htm](feats/class-12-Q4puGx4kBMXy45fa.htm)|Familiar's Eyes|auto-trad|
|[class-12-qI5ZyuNVME95iXhJ.htm](feats/class-12-qI5ZyuNVME95iXhJ.htm)|Fantastic Leap|auto-trad|
|[class-12-quqG3jfWFdopF0G2.htm](feats/class-12-quqG3jfWFdopF0G2.htm)|No!!!|auto-trad|
|[class-12-qZsTI97BQUwoPgKF.htm](feats/class-12-qZsTI97BQUwoPgKF.htm)|Psi Catastrophe|auto-trad|
|[class-12-rHV8GUqqt8WLgTJp.htm](feats/class-12-rHV8GUqqt8WLgTJp.htm)|Thaumaturge's Demesne|auto-trad|
|[class-12-rqUJULinzDCUgimM.htm](feats/class-12-rqUJULinzDCUgimM.htm)|Shooter's Camouflage|auto-trad|
|[class-12-RSUmrIiFBEchdM8B.htm](feats/class-12-RSUmrIiFBEchdM8B.htm)|Primal Focus|auto-trad|
|[class-12-tGXJU6yx7bYuyLvd.htm](feats/class-12-tGXJU6yx7bYuyLvd.htm)|Brutal Finish|auto-trad|
|[class-12-U0XX0Mm2MbZcuNK2.htm](feats/class-12-U0XX0Mm2MbZcuNK2.htm)|Shared Overdrive|auto-trad|
|[class-12-U4AoJMBhJaFq5O1S.htm](feats/class-12-U4AoJMBhJaFq5O1S.htm)|Champion's Sacrifice|auto-trad|
|[class-12-U9uRb2TezYlQPNJd.htm](feats/class-12-U9uRb2TezYlQPNJd.htm)|Boost Modulation|auto-trad|
|[class-12-U9zIjPHPe2PyrtlU.htm](feats/class-12-U9zIjPHPe2PyrtlU.htm)|Flexible Transmogrification|auto-trad|
|[class-12-uAh31Hnp1EZSjd40.htm](feats/class-12-uAh31Hnp1EZSjd40.htm)|Invincible Mutagen|auto-trad|
|[class-12-UIRcjHxuSedoDOj4.htm](feats/class-12-UIRcjHxuSedoDOj4.htm)|Inspirational Focus|auto-trad|
|[class-12-UJafwv306v75Syy7.htm](feats/class-12-UJafwv306v75Syy7.htm)|Forcible Energy|auto-trad|
|[class-12-uJgATfMW3kumS6Y0.htm](feats/class-12-uJgATfMW3kumS6Y0.htm)|Intensify Investiture|auto-trad|
|[class-12-va7YMidXZW21oFwA.htm](feats/class-12-va7YMidXZW21oFwA.htm)|Blood Component Substitution|auto-trad|
|[class-12-vFa9crHKkNkPUjFl.htm](feats/class-12-vFa9crHKkNkPUjFl.htm)|Shared Sight|auto-trad|
|[class-12-VIjI8PtkTFjeAA6a.htm](feats/class-12-VIjI8PtkTFjeAA6a.htm)|Ricochet Feint|auto-trad|
|[class-12-vVhgYkOU9mPTGTxF.htm](feats/class-12-vVhgYkOU9mPTGTxF.htm)|Domain Focus|auto-trad|
|[class-12-vwBD55BRDOatp4ZV.htm](feats/class-12-vwBD55BRDOatp4ZV.htm)|Green Tongue|auto-trad|
|[class-12-VzQtyHSjq12E4Dzh.htm](feats/class-12-VzQtyHSjq12E4Dzh.htm)|Purifying Spell|auto-trad|
|[class-12-WsEVkMFe8ZEIRKLu.htm](feats/class-12-WsEVkMFe8ZEIRKLu.htm)|Grasping Limbs|auto-trad|
|[class-12-xAFdoKl7aOP9rVkl.htm](feats/class-12-xAFdoKl7aOP9rVkl.htm)|Emblazon Antimagic|auto-trad|
|[class-12-xBqDeQFzvuDfqhZC.htm](feats/class-12-xBqDeQFzvuDfqhZC.htm)|Reactive Interference|auto-trad|
|[class-12-Xdf00Kmv5C3qqrtK.htm](feats/class-12-Xdf00Kmv5C3qqrtK.htm)|Flesh Wound|auto-trad|
|[class-12-XyQpqznuO5LGFvhz.htm](feats/class-12-XyQpqznuO5LGFvhz.htm)|Coordinated Distraction|auto-trad|
|[class-12-yaxf1Tpk5iwPCSpW.htm](feats/class-12-yaxf1Tpk5iwPCSpW.htm)|Eclectic Polymath|auto-trad|
|[class-12-YTHsakqXumdMU0dn.htm](feats/class-12-YTHsakqXumdMU0dn.htm)|Conflux Focus|auto-trad|
|[class-12-YZ138OqflDhrkqmR.htm](feats/class-12-YZ138OqflDhrkqmR.htm)|Enforce Oath|auto-trad|
|[class-12-YZEG7LsiIIhwRB91.htm](feats/class-12-YZEG7LsiIIhwRB91.htm)|Necromantic Deflection|auto-trad|
|[class-12-zSTPB1FFWMfA1JPi.htm](feats/class-12-zSTPB1FFWMfA1JPi.htm)|Bloodline Focus|auto-trad|
|[class-12-zzMugLCUkQQPa2qT.htm](feats/class-12-zzMugLCUkQQPa2qT.htm)|Preparation|auto-trad|
|[class-14-0kkiE74cyHyxdPe6.htm](feats/class-14-0kkiE74cyHyxdPe6.htm)|Vengeful Strike|auto-trad|
|[class-14-1DaSVLJEdJWYOWek.htm](feats/class-14-1DaSVLJEdJWYOWek.htm)|Guiding Riposte|auto-trad|
|[class-14-1SvBUzVH5tp0lmn5.htm](feats/class-14-1SvBUzVH5tp0lmn5.htm)|Two-Weapon Flurry|auto-trad|
|[class-14-2h8a6pKhXTXwpJjP.htm](feats/class-14-2h8a6pKhXTXwpJjP.htm)|Premonition Of Clarity|auto-trad|
|[class-14-2HMvAB6mIVwvwtjT.htm](feats/class-14-2HMvAB6mIVwvwtjT.htm)|True Debilitating Bomb|auto-trad|
|[class-14-2KBKXkRthBXpw48X.htm](feats/class-14-2KBKXkRthBXpw48X.htm)|Stay Down!|auto-trad|
|[class-14-2wLfchKVDSKBOIpV.htm](feats/class-14-2wLfchKVDSKBOIpV.htm)|Soaring Armor|auto-trad|
|[class-14-3hnmQrhv4Bru5GKR.htm](feats/class-14-3hnmQrhv4Bru5GKR.htm)|Grand Talisman Esoterica|auto-trad|
|[class-14-3r5rg0BCqSh5RBNS.htm](feats/class-14-3r5rg0BCqSh5RBNS.htm)|Specialized Companion (Druid)|auto-trad|
|[class-14-3tm0TzyjO1I378fw.htm](feats/class-14-3tm0TzyjO1I378fw.htm)|Conscious Spell Specialization|auto-trad|
|[class-14-4n59y5tb9bxffKsi.htm](feats/class-14-4n59y5tb9bxffKsi.htm)|Esoteric Reflexes|auto-trad|
|[class-14-4Q9Q41KLPYJMdV4b.htm](feats/class-14-4Q9Q41KLPYJMdV4b.htm)|Reactive Transformation|auto-trad|
|[class-14-4SKech3n0F38HrR5.htm](feats/class-14-4SKech3n0F38HrR5.htm)|Vigorous Inspiration|auto-trad|
|[class-14-5cxkVY3mBsbYWd5K.htm](feats/class-14-5cxkVY3mBsbYWd5K.htm)|Timeless Body|auto-trad|
|[class-14-5dAFkOYPz8PPdFrw.htm](feats/class-14-5dAFkOYPz8PPdFrw.htm)|Leave an Opening|auto-trad|
|[class-14-5NyX1WnXEO40yEaT.htm](feats/class-14-5NyX1WnXEO40yEaT.htm)|Wyrmbane Aura|auto-trad|
|[class-14-5zeKULjKDkiIenhu.htm](feats/class-14-5zeKULjKDkiIenhu.htm)|Unstable Redundancies|auto-trad|
|[class-14-6gtm0YCaDFpyVy35.htm](feats/class-14-6gtm0YCaDFpyVy35.htm)|Warden's Guidance|auto-trad|
|[class-14-9t6Kfk8Yw8WJYY8Z.htm](feats/class-14-9t6Kfk8Yw8WJYY8Z.htm)|Guiding Finish|auto-trad|
|[class-14-9zH7IOsmhRBEqXAV.htm](feats/class-14-9zH7IOsmhRBEqXAV.htm)|Instant Opening|auto-trad|
|[class-14-A7uJCDT9odsuwlqW.htm](feats/class-14-A7uJCDT9odsuwlqW.htm)|Triggerbrand Blitz|auto-trad|
|[class-14-AfTMuAln2f0Pa3Lj.htm](feats/class-14-AfTMuAln2f0Pa3Lj.htm)|Shadow's Web|auto-trad|
|[class-14-AGydz5DKJ2KHSO4S.htm](feats/class-14-AGydz5DKJ2KHSO4S.htm)|Whirlwind Strike|auto-trad|
|[class-14-AOLf6QX068LR9L9e.htm](feats/class-14-AOLf6QX068LR9L9e.htm)|Earworm|auto-trad|
|[class-14-aPt0WfFoeLTzyQRA.htm](feats/class-14-aPt0WfFoeLTzyQRA.htm)|Defensive Roll|auto-trad|
|[class-14-Aqhsx5duEpBgaPB0.htm](feats/class-14-Aqhsx5duEpBgaPB0.htm)|Shared Prey|auto-trad|
|[class-14-AV6KZvUed7GdhHzc.htm](feats/class-14-AV6KZvUed7GdhHzc.htm)|Bizarre Transformation|auto-trad|
|[class-14-b5K067Pma4Il9IeD.htm](feats/class-14-b5K067Pma4Il9IeD.htm)|Anchoring Aura|auto-trad|
|[class-14-B912Vru18B6orzcs.htm](feats/class-14-B912Vru18B6orzcs.htm)|Trespass Teleportation|auto-trad|
|[class-14-bjNeSAldeTzRcEaQ.htm](feats/class-14-bjNeSAldeTzRcEaQ.htm)|Interweave Dispel|auto-trad|
|[class-14-bSC18SbdaNXfBHu9.htm](feats/class-14-bSC18SbdaNXfBHu9.htm)|Dance of Thunder|auto-trad|
|[class-14-BuaTJxALqxM5EZav.htm](feats/class-14-BuaTJxALqxM5EZav.htm)|Perfect Finisher|auto-trad|
|[class-14-cgtVYaZRVaTIW4sk.htm](feats/class-14-cgtVYaZRVaTIW4sk.htm)|Spell-Repelling Form|auto-trad|
|[class-14-ct4dJHBn1Dj4cx4B.htm](feats/class-14-ct4dJHBn1Dj4cx4B.htm)|Tongue of Sun and Moon|auto-trad|
|[class-14-d8yggbcJsKKyHip7.htm](feats/class-14-d8yggbcJsKKyHip7.htm)|Consume Spell|auto-trad|
|[class-14-DM9rjXZrmx2MFX7k.htm](feats/class-14-DM9rjXZrmx2MFX7k.htm)|Sense The Unseen|auto-trad|
|[class-14-DT1O80hPD7MX6oWp.htm](feats/class-14-DT1O80hPD7MX6oWp.htm)|Spell Shroud|auto-trad|
|[class-14-DUb1VWSbTjdsbAkQ.htm](feats/class-14-DUb1VWSbTjdsbAkQ.htm)|Deity's Protection|auto-trad|
|[class-14-EeSP1SNlgAuASvkP.htm](feats/class-14-EeSP1SNlgAuASvkP.htm)|Preternatural Parry|auto-trad|
|[class-14-Ek3nCIFRreqnSxAQ.htm](feats/class-14-Ek3nCIFRreqnSxAQ.htm)|Mysterious Repertoire|auto-trad|
|[class-14-erCOcFZJPT2O3gwC.htm](feats/class-14-erCOcFZJPT2O3gwC.htm)|Aura of Righteousness|auto-trad|
|[class-14-eUv2L0CLidxtA3sh.htm](feats/class-14-eUv2L0CLidxtA3sh.htm)|Dormant Eruption|auto-trad|
|[class-14-EvSfoYmuCDCRAvaF.htm](feats/class-14-EvSfoYmuCDCRAvaF.htm)|Divine Reflexes|auto-trad|
|[class-14-ewbt80Yin18k6oLq.htm](feats/class-14-ewbt80Yin18k6oLq.htm)|Tangled Forest Rake|auto-trad|
|[class-14-GsrwoQ7DIjERXuPf.htm](feats/class-14-GsrwoQ7DIjERXuPf.htm)|Superior Bond|auto-trad|
|[class-14-guSjEQS3WuXJqQxf.htm](feats/class-14-guSjEQS3WuXJqQxf.htm)|Impaling Thrust|auto-trad|
|[class-14-h8OB6zcdIqUPS6PC.htm](feats/class-14-h8OB6zcdIqUPS6PC.htm)|Airborne Form|auto-trad|
|[class-14-hO4sKslTrSQMLbGx.htm](feats/class-14-hO4sKslTrSQMLbGx.htm)|Mountain Quake|auto-trad|
|[class-14-HquaVwjOLSPzcJgB.htm](feats/class-14-HquaVwjOLSPzcJgB.htm)|Impossible Riposte|auto-trad|
|[class-14-HVwmYfSLhrnCksHV.htm](feats/class-14-HVwmYfSLhrnCksHV.htm)|Targeting Shot|auto-trad|
|[class-14-HxkAhxcGvua6SkfS.htm](feats/class-14-HxkAhxcGvua6SkfS.htm)|Glib Mutagen|auto-trad|
|[class-14-I1xq9TDYRHNtqzGz.htm](feats/class-14-I1xq9TDYRHNtqzGz.htm)|Sink and Swim|auto-trad|
|[class-14-IaiEZaA8erufMUCr.htm](feats/class-14-IaiEZaA8erufMUCr.htm)|Whirling Blade Stance|auto-trad|
|[class-14-Ice8oNOTbPFXyOww.htm](feats/class-14-Ice8oNOTbPFXyOww.htm)|Aura of Vengeance|auto-trad|
|[class-14-IqDbNiwHQH1xApo9.htm](feats/class-14-IqDbNiwHQH1xApo9.htm)|Ironblood Surge|auto-trad|
|[class-14-jdrdkxqautAtszCX.htm](feats/class-14-jdrdkxqautAtszCX.htm)|Share Eidolon Magic|auto-trad|
|[class-14-jhNkuXg8vJ29GSUJ.htm](feats/class-14-jhNkuXg8vJ29GSUJ.htm)|Deep Roots|auto-trad|
|[class-14-jwQERVkjtnlFp3Ec.htm](feats/class-14-jwQERVkjtnlFp3Ec.htm)|Fast Channel|auto-trad|
|[class-14-k42ntHdg70ZMEKrs.htm](feats/class-14-k42ntHdg70ZMEKrs.htm)|Purifying Breeze|auto-trad|
|[class-14-k72W0qMXsX5ekJTF.htm](feats/class-14-k72W0qMXsX5ekJTF.htm)|Reflect Spell|auto-trad|
|[class-14-l5nqNPmZPG6lYFnZ.htm](feats/class-14-l5nqNPmZPG6lYFnZ.htm)|Hasted Assault|auto-trad|
|[class-14-LmdOWCDffhBiyzM3.htm](feats/class-14-LmdOWCDffhBiyzM3.htm)|Soothing Ballad|auto-trad|
|[class-14-MFqFvuiYDAoADcft.htm](feats/class-14-MFqFvuiYDAoADcft.htm)|Giant's Lunge|auto-trad|
|[class-14-MfvkZ9efD013H34Z.htm](feats/class-14-MfvkZ9efD013H34Z.htm)|Come at Me!|auto-trad|
|[class-14-MwozkE6aj42WZ7Z1.htm](feats/class-14-MwozkE6aj42WZ7Z1.htm)|Stealthy Companion|auto-trad|
|[class-14-N02sHmzuF9XQj93m.htm](feats/class-14-N02sHmzuF9XQj93m.htm)|Resilient Shell|auto-trad|
|[class-14-N7CM5CmHuZ1cylV9.htm](feats/class-14-N7CM5CmHuZ1cylV9.htm)|Plot The Future|auto-trad|
|[class-14-NDbNNMouxnnwBqwm.htm](feats/class-14-NDbNNMouxnnwBqwm.htm)|Explosive Maneuver|auto-trad|
|[class-14-NgUB5toKxBd8RJmm.htm](feats/class-14-NgUB5toKxBd8RJmm.htm)|Strategic Bypass|auto-trad|
|[class-14-NNeRv9Gcua1kMp4s.htm](feats/class-14-NNeRv9Gcua1kMp4s.htm)|Forestall Curse|auto-trad|
|[class-14-nvPxCUOCMaYdhLp1.htm](feats/class-14-nvPxCUOCMaYdhLp1.htm)|Desperate Finisher|auto-trad|
|[class-14-OEwNLolzBarx8icm.htm](feats/class-14-OEwNLolzBarx8icm.htm)|Explosive Death Drop|auto-trad|
|[class-14-psr9lxsnj9aOiqlK.htm](feats/class-14-psr9lxsnj9aOiqlK.htm)|Sacral Monarch|auto-trad|
|[class-14-q2t5qfgQHFJbbV8d.htm](feats/class-14-q2t5qfgQHFJbbV8d.htm)|Shift Spell|auto-trad|
|[class-14-Q4NiHmThMtk8razS.htm](feats/class-14-Q4NiHmThMtk8razS.htm)|Headshot|auto-trad|
|[class-14-QC2ecMZ57MRJlxco.htm](feats/class-14-QC2ecMZ57MRJlxco.htm)|Bonded Focus|auto-trad|
|[class-14-QpnZwabXOVICJL5i.htm](feats/class-14-QpnZwabXOVICJL5i.htm)|Greater Merciful Elixir|auto-trad|
|[class-14-r4ZVpDjX2X0yrmhw.htm](feats/class-14-r4ZVpDjX2X0yrmhw.htm)|Blast Tackle|auto-trad|
|[class-14-R5hdEbfKdIVAQs24.htm](feats/class-14-R5hdEbfKdIVAQs24.htm)|Paragon Companion|auto-trad|
|[class-14-RbHacJSoe6XHT8Ks.htm](feats/class-14-RbHacJSoe6XHT8Ks.htm)|Litany of Righteousness|auto-trad|
|[class-14-rCnaBbk0M1gBVHjG.htm](feats/class-14-rCnaBbk0M1gBVHjG.htm)|Awesome Blow|auto-trad|
|[class-14-RHLfM9NlIlHTH85w.htm](feats/class-14-RHLfM9NlIlHTH85w.htm)|Triumphant Inspiration|auto-trad|
|[class-14-rOx7r8ygmPHPC6qF.htm](feats/class-14-rOx7r8ygmPHPC6qF.htm)|Allegro|auto-trad|
|[class-14-RpXWOgLWQLGdx74I.htm](feats/class-14-RpXWOgLWQLGdx74I.htm)|Sunder Enchantment|auto-trad|
|[class-14-t3unBu3PX6AO0uIW.htm](feats/class-14-t3unBu3PX6AO0uIW.htm)|Swift Banishment|auto-trad|
|[class-14-T3XFrLIBzir9IqD5.htm](feats/class-14-T3XFrLIBzir9IqD5.htm)|Extend Armament Alignment|auto-trad|
|[class-14-TdwC9rTGgtF4CQ25.htm](feats/class-14-TdwC9rTGgtF4CQ25.htm)|Flamboyant Leap|auto-trad|
|[class-14-TpWS2b9ISHnXVfZg.htm](feats/class-14-TpWS2b9ISHnXVfZg.htm)|Timeless Nature|auto-trad|
|[class-14-UA3iZTAZrugKClKE.htm](feats/class-14-UA3iZTAZrugKClKE.htm)|Shatter Space|auto-trad|
|[class-14-UZpBsHE57rmS0x6S.htm](feats/class-14-UZpBsHE57rmS0x6S.htm)|Showstopper|auto-trad|
|[class-14-UzuMXY8G88ULDRex.htm](feats/class-14-UzuMXY8G88ULDRex.htm)|Disruptive Blur|auto-trad|
|[class-14-VW0Tp5rJDRjaJuSn.htm](feats/class-14-VW0Tp5rJDRjaJuSn.htm)|Two-Weapon Fusillade|auto-trad|
|[class-14-w2v5LZmpJy0MBxo5.htm](feats/class-14-w2v5LZmpJy0MBxo5.htm)|Improved Twin Riposte (Fighter)|auto-trad|
|[class-14-wa9ZGBTlFuwOjPpH.htm](feats/class-14-wa9ZGBTlFuwOjPpH.htm)|Rites Of Transfiguration|auto-trad|
|[class-14-wdkbfWKEjAFXAxto.htm](feats/class-14-wdkbfWKEjAFXAxto.htm)|Determination|auto-trad|
|[class-14-WEUFs37Ids3ZRrqa.htm](feats/class-14-WEUFs37Ids3ZRrqa.htm)|Litany Of Depravity|auto-trad|
|[class-14-wKx7BWdQu5sEjL9j.htm](feats/class-14-wKx7BWdQu5sEjL9j.htm)|Wild Winds Gust|auto-trad|
|[class-14-wnrlBkwVr8BSVAZt.htm](feats/class-14-wnrlBkwVr8BSVAZt.htm)|Sepulchral Sublimation|auto-trad|
|[class-14-Xk9inG3pln4UKbs3.htm](feats/class-14-Xk9inG3pln4UKbs3.htm)|True Hypercognition|auto-trad|
|[class-14-xSJaOcdhqDF1CBs3.htm](feats/class-14-xSJaOcdhqDF1CBs3.htm)|Ebb And Flow|auto-trad|
|[class-14-XydqgTE4J119J5JV.htm](feats/class-14-XydqgTE4J119J5JV.htm)|Arcane Shroud|auto-trad|
|[class-14-YawVDUc9uzREIAnO.htm](feats/class-14-YawVDUc9uzREIAnO.htm)|Aura of Life|auto-trad|
|[class-14-yeSyGnYDkl2GUNmu.htm](feats/class-14-yeSyGnYDkl2GUNmu.htm)|Stance Savant (Fighter)|auto-trad|
|[class-14-ygPrwqiyDr1frUHw.htm](feats/class-14-ygPrwqiyDr1frUHw.htm)|Greater Interpose|auto-trad|
|[class-14-z7kwVNaCB4oJs3Fe.htm](feats/class-14-z7kwVNaCB4oJs3Fe.htm)|Aura Of Preservation|auto-trad|
|[class-16-05k4nkjazjjEUoGu.htm](feats/class-16-05k4nkjazjjEUoGu.htm)|Blank Slate|auto-trad|
|[class-16-1re3J4hWW7raXIRB.htm](feats/class-16-1re3J4hWW7raXIRB.htm)|Spell Tinker|auto-trad|
|[class-16-2HeRmbcHcsRMccir.htm](feats/class-16-2HeRmbcHcsRMccir.htm)|Diverse Mystery|auto-trad|
|[class-16-4caP26xpkQajkaDp.htm](feats/class-16-4caP26xpkQajkaDp.htm)|One-Millimeter Punch|auto-trad|
|[class-16-5ZoIJImgvpdhGcDR.htm](feats/class-16-5ZoIJImgvpdhGcDR.htm)|Just the Thing!|auto-trad|
|[class-16-6AXAJuMEMpxU27PJ.htm](feats/class-16-6AXAJuMEMpxU27PJ.htm)|Dispelling Slice|auto-trad|
|[class-16-6fojN9yBJByTZ1Q9.htm](feats/class-16-6fojN9yBJByTZ1Q9.htm)|Didactic Strike|auto-trad|
|[class-16-7GGxxoYNA4YrtML9.htm](feats/class-16-7GGxxoYNA4YrtML9.htm)|Trample|auto-trad|
|[class-16-7IDFHh2ZJLaB1y59.htm](feats/class-16-7IDFHh2ZJLaB1y59.htm)|Shield of Grace|auto-trad|
|[class-16-8C9qo5LL9J0UTNGc.htm](feats/class-16-8C9qo5LL9J0UTNGc.htm)|Courageous Onslaught|auto-trad|
|[class-16-8HYfYT4fHtxXP199.htm](feats/class-16-8HYfYT4fHtxXP199.htm)|Greater Mental Evolution|auto-trad|
|[class-16-9wd9hhtfjayeZ6zF.htm](feats/class-16-9wd9hhtfjayeZ6zF.htm)|Giant Slayer|auto-trad|
|[class-16-a6xUSibsRaclbSz3.htm](feats/class-16-a6xUSibsRaclbSz3.htm)|Ever-Vigilant Senses|auto-trad|
|[class-16-AObopXQkCIsaBAID.htm](feats/class-16-AObopXQkCIsaBAID.htm)|Implement's Flight|auto-trad|
|[class-16-aviDW7htxA77iarV.htm](feats/class-16-aviDW7htxA77iarV.htm)|Hair Trigger|auto-trad|
|[class-16-Bp02C07s4RTS4vsV.htm](feats/class-16-Bp02C07s4RTS4vsV.htm)|Reconstruct The Scene|auto-trad|
|[class-16-bX2WI5k0afqPpCfm.htm](feats/class-16-bX2WI5k0afqPpCfm.htm)|Ubiquitous Snares|auto-trad|
|[class-16-Cgk4By6gEomD2bJ0.htm](feats/class-16-Cgk4By6gEomD2bJ0.htm)|Improved Twin Riposte (Ranger)|auto-trad|
|[class-16-D71A2ZQfz9MVndqI.htm](feats/class-16-D71A2ZQfz9MVndqI.htm)|Felicitous Riposte|auto-trad|
|[class-16-DAFF7zJphhDPcAws.htm](feats/class-16-DAFF7zJphhDPcAws.htm)|Medusa's Wrath|auto-trad|
|[class-16-Dr6h8WRW6xnLRfxr.htm](feats/class-16-Dr6h8WRW6xnLRfxr.htm)|Resurrectionist|auto-trad|
|[class-16-DSmYJvCHMvZCP0aD.htm](feats/class-16-DSmYJvCHMvZCP0aD.htm)|Scintillating Spell|auto-trad|
|[class-16-E4xubBMtj81kX5Bk.htm](feats/class-16-E4xubBMtj81kX5Bk.htm)|Shattering Strike (Monk)|auto-trad|
|[class-16-e8ChYAaQ9Aa8aZES.htm](feats/class-16-e8ChYAaQ9Aa8aZES.htm)|Deadly Grace|auto-trad|
|[class-16-eOwjwVAbl99ZTy5D.htm](feats/class-16-eOwjwVAbl99ZTy5D.htm)|Overwhelming Blow|auto-trad|
|[class-16-EWeso1zDkCLGlnsW.htm](feats/class-16-EWeso1zDkCLGlnsW.htm)|Reckless Abandon (Barbarian)|auto-trad|
|[class-16-Fai5VMyrtOrYC5JL.htm](feats/class-16-Fai5VMyrtOrYC5JL.htm)|Specialized Companion (Ranger)|auto-trad|
|[class-16-fEfEabn53bubYVVT.htm](feats/class-16-fEfEabn53bubYVVT.htm)|Improved Reflexive Shield|auto-trad|
|[class-16-g1wBP9Z5HRqDe9FE.htm](feats/class-16-g1wBP9Z5HRqDe9FE.htm)|Quivering Palm|auto-trad|
|[class-16-giOEclnMp8txkRSU.htm](feats/class-16-giOEclnMp8txkRSU.htm)|Eternal Elixir|auto-trad|
|[class-16-GKP2dHkgQw1o0k8g.htm](feats/class-16-GKP2dHkgQw1o0k8g.htm)|Persistent Mutagen|auto-trad|
|[class-16-h08Vfel5iIAARWdy.htm](feats/class-16-h08Vfel5iIAARWdy.htm)|Instrument of Zeal|auto-trad|
|[class-16-H5uZqYVClk3s62ce.htm](feats/class-16-H5uZqYVClk3s62ce.htm)|Remediate|auto-trad|
|[class-16-H7Ocx80td7Sx7Cqn.htm](feats/class-16-H7Ocx80td7Sx7Cqn.htm)|Expand Aura|auto-trad|
|[class-16-hOZP7qOMsNYOe90o.htm](feats/class-16-hOZP7qOMsNYOe90o.htm)|Wandering Thoughts|auto-trad|
|[class-16-hr9maYUbtrNxpBPw.htm](feats/class-16-hr9maYUbtrNxpBPw.htm)|Exploitive Bomb|auto-trad|
|[class-16-i98NcWSAbWmNmBik.htm](feats/class-16-i98NcWSAbWmNmBik.htm)|Instrument Of Slaughter|auto-trad|
|[class-16-IA8p9oYmsCbipmhw.htm](feats/class-16-IA8p9oYmsCbipmhw.htm)|Chemical Contagion|auto-trad|
|[class-16-IevdA9OVFZE6FOVK.htm](feats/class-16-IevdA9OVFZE6FOVK.htm)|Sever Magic|auto-trad|
|[class-16-jSu9h6UvZtUJ7InD.htm](feats/class-16-jSu9h6UvZtUJ7InD.htm)|Fey Life|auto-trad|
|[class-16-kMLvQnx2vY7F3bjI.htm](feats/class-16-kMLvQnx2vY7F3bjI.htm)|Eternal Blessing|auto-trad|
|[class-16-L0f3c0DkT7FLQF9W.htm](feats/class-16-L0f3c0DkT7FLQF9W.htm)|Siphon Power|auto-trad|
|[class-16-LbMPCAyUrLRHykGA.htm](feats/class-16-LbMPCAyUrLRHykGA.htm)|Persistent Boost|auto-trad|
|[class-16-lgEihn7deZwHczGE.htm](feats/class-16-lgEihn7deZwHczGE.htm)|Dragon Transformation|auto-trad|
|[class-16-lhSqWHXK1JShUabF.htm](feats/class-16-lhSqWHXK1JShUabF.htm)|Resounding Finale|auto-trad|
|[class-16-mB7ENhF10qoOwQyG.htm](feats/class-16-mB7ENhF10qoOwQyG.htm)|Resounding Cascade|auto-trad|
|[class-16-mDczSj3GolIbkhGJ.htm](feats/class-16-mDczSj3GolIbkhGJ.htm)|Instant Return|auto-trad|
|[class-16-mj1pVVFtqGLKgCQM.htm](feats/class-16-mj1pVVFtqGLKgCQM.htm)|Genius Mutagen|auto-trad|
|[class-16-MjhCcwGKyI5dpNIY.htm](feats/class-16-MjhCcwGKyI5dpNIY.htm)|Perfect Distraction|auto-trad|
|[class-16-MUwIeDV0pBtqeU3p.htm](feats/class-16-MUwIeDV0pBtqeU3p.htm)|Walk on the Wind|auto-trad|
|[class-16-ND3nKsXCDBShUgYc.htm](feats/class-16-ND3nKsXCDBShUgYc.htm)|Greater Vital Evolution|auto-trad|
|[class-16-Ndxu9YVGgenYmZSb.htm](feats/class-16-Ndxu9YVGgenYmZSb.htm)|Dispelling Spellstrike|auto-trad|
|[class-16-Oyml3OGNy468z3XI.htm](feats/class-16-Oyml3OGNy468z3XI.htm)|Furious Vengeance|auto-trad|
|[class-16-PiKWzPloAR2nqIcC.htm](feats/class-16-PiKWzPloAR2nqIcC.htm)|Constant Levitation|auto-trad|
|[class-16-pzmob1HqVKZfL0BY.htm](feats/class-16-pzmob1HqVKZfL0BY.htm)|Eternal Bane|auto-trad|
|[class-16-QGpcyvIezLMgmTia.htm](feats/class-16-QGpcyvIezLMgmTia.htm)|Studious Capacity|auto-trad|
|[class-16-qkHVva51N6H8NNGR.htm](feats/class-16-qkHVva51N6H8NNGR.htm)|Ricochet Master|auto-trad|
|[class-16-qwnyuCsl5YvX0fdY.htm](feats/class-16-qwnyuCsl5YvX0fdY.htm)|Fatal Bullet|auto-trad|
|[class-16-qX9ZtfaAj6rxrVA7.htm](feats/class-16-qX9ZtfaAj6rxrVA7.htm)|Master of Many Styles|auto-trad|
|[class-16-r7FGPKl5e0xB4tuj.htm](feats/class-16-r7FGPKl5e0xB4tuj.htm)|Cloud Step|auto-trad|
|[class-16-rgs6OZJYCgi5At8J.htm](feats/class-16-rgs6OZJYCgi5At8J.htm)|Effortless Concentration|auto-trad|
|[class-16-RL7faGkymMFLAqTU.htm](feats/class-16-RL7faGkymMFLAqTU.htm)|Shattering Blows|auto-trad|
|[class-16-RMSVWMFoLUk0P1cC.htm](feats/class-16-RMSVWMFoLUk0P1cC.htm)|Terraforming Spell|auto-trad|
|[class-16-ROnjdPMvH0vkkWjQ.htm](feats/class-16-ROnjdPMvH0vkkWjQ.htm)|Enlightened Presence|auto-trad|
|[class-16-RqccUKf8DCPnsYXJ.htm](feats/class-16-RqccUKf8DCPnsYXJ.htm)|Implausible Purchase (Investigator)|auto-trad|
|[class-16-rRbMOxX1QTHIIwAi.htm](feats/class-16-rRbMOxX1QTHIIwAi.htm)|Portentous Spell|auto-trad|
|[class-16-RzhnxgiAopWILCvs.htm](feats/class-16-RzhnxgiAopWILCvs.htm)|Multishot Stance|auto-trad|
|[class-16-tBMMKWYfzD2OMD30.htm](feats/class-16-tBMMKWYfzD2OMD30.htm)|Penetrating Projectile|auto-trad|
|[class-16-TqODgJL1VS3eokhX.htm](feats/class-16-TqODgJL1VS3eokhX.htm)|Swift Elusion|auto-trad|
|[class-16-UgZDirfFb4CFhuWh.htm](feats/class-16-UgZDirfFb4CFhuWh.htm)|Seven-Part Link|auto-trad|
|[class-16-uixN9wbfe0veOHRn.htm](feats/class-16-uixN9wbfe0veOHRn.htm)|Auspicious Mount|auto-trad|
|[class-16-uL6q4wtwvuP8I4po.htm](feats/class-16-uL6q4wtwvuP8I4po.htm)|Flinging Blow|auto-trad|
|[class-16-vDJRIKS27md3LudA.htm](feats/class-16-vDJRIKS27md3LudA.htm)|Greater Distracting Shot|auto-trad|
|[class-16-Veyt4x2Kb5YcPGTv.htm](feats/class-16-Veyt4x2Kb5YcPGTv.htm)|Legendary Monster Hunter|auto-trad|
|[class-16-wPJFEUOXwf7y5jN3.htm](feats/class-16-wPJFEUOXwf7y5jN3.htm)|Cognitive Loophole|auto-trad|
|[class-16-wwvz79Hwp9qx96lL.htm](feats/class-16-wwvz79Hwp9qx96lL.htm)|Target of Psychic Ire|auto-trad|
|[class-16-WY7CjISdz6uwXwIb.htm](feats/class-16-WY7CjISdz6uwXwIb.htm)|Collateral Thrash|auto-trad|
|[class-16-xjwlP306nuda2z03.htm](feats/class-16-xjwlP306nuda2z03.htm)|Steal Spell|auto-trad|
|[class-16-zBS1qFyIpFuCGhWW.htm](feats/class-16-zBS1qFyIpFuCGhWW.htm)|You Failed to Account For... This!|auto-trad|
|[class-16-zfnZki2CxmZXdNBO.htm](feats/class-16-zfnZki2CxmZXdNBO.htm)|Electric Counter|auto-trad|
|[class-18-5fd95NhJJ55hm0Qt.htm](feats/class-18-5fd95NhJJ55hm0Qt.htm)|Link Wellspring|auto-trad|
|[class-18-6A1WTVY1PUlXy3rW.htm](feats/class-18-6A1WTVY1PUlXy3rW.htm)|Versatile Spellstrike|auto-trad|
|[class-18-6GUl9WG7OKvfVQo4.htm](feats/class-18-6GUl9WG7OKvfVQo4.htm)|Empty Body|auto-trad|
|[class-18-72qOqbwShnT2Apaw.htm](feats/class-18-72qOqbwShnT2Apaw.htm)|Implement's Assault|auto-trad|
|[class-18-7ATVpDUM6pRq6HOR.htm](feats/class-18-7ATVpDUM6pRq6HOR.htm)|Smash From The Air|auto-trad|
|[class-18-7mGMHb7irGKZ0eQo.htm](feats/class-18-7mGMHb7irGKZ0eQo.htm)|Miraculous Possibility|auto-trad|
|[class-18-8JMOgtB3XG7o6ffW.htm](feats/class-18-8JMOgtB3XG7o6ffW.htm)|Discordant Voice|auto-trad|
|[class-18-a1TSGGsA6b5gjP3H.htm](feats/class-18-a1TSGGsA6b5gjP3H.htm)|Reprepare Spell|auto-trad|
|[class-18-aoIEVpJrQEold2Mi.htm](feats/class-18-aoIEVpJrQEold2Mi.htm)|Magical Master|auto-trad|
|[class-18-AwxJcaIrutqMcUC8.htm](feats/class-18-AwxJcaIrutqMcUC8.htm)|Masterful Companion|auto-trad|
|[class-18-BtMemftiktI0Mn6X.htm](feats/class-18-BtMemftiktI0Mn6X.htm)|Manifold Edge (Precision)|auto-trad|
|[class-18-C9nb7H5u2ElBXvCR.htm](feats/class-18-C9nb7H5u2ElBXvCR.htm)|Mindblank Mutagen|auto-trad|
|[class-18-Cs0hRKBfWn2gOYzK.htm](feats/class-18-Cs0hRKBfWn2gOYzK.htm)|Devastating Weaponry|auto-trad|
|[class-18-Dcr63tofZUome1Ze.htm](feats/class-18-Dcr63tofZUome1Ze.htm)|Manifold Edge|auto-trad|
|[class-18-doD3jZylVXZ0oHWO.htm](feats/class-18-doD3jZylVXZ0oHWO.htm)|Greater Crossblooded Evolution|auto-trad|
|[class-18-EcQvuGYDJdqTfGuX.htm](feats/class-18-EcQvuGYDJdqTfGuX.htm)|Manifold Edge (Outwit)|auto-trad|
|[class-18-f1acyuIGYVp2BpKc.htm](feats/class-18-f1acyuIGYVp2BpKc.htm)|Shadow Hunter|auto-trad|
|[class-18-fLrwddS607eRFfHA.htm](feats/class-18-fLrwddS607eRFfHA.htm)|Implausible Infiltration|auto-trad|
|[class-18-G8iGrx1qBDwLk1HO.htm](feats/class-18-G8iGrx1qBDwLk1HO.htm)|Piercing Critical|auto-trad|
|[class-18-GLHh9MWlcINLC6Q0.htm](feats/class-18-GLHh9MWlcINLC6Q0.htm)|Engine of Destruction|auto-trad|
|[class-18-gtXy5gMNU0NDvkBL.htm](feats/class-18-gtXy5gMNU0NDvkBL.htm)|Fiendish Form|auto-trad|
|[class-18-GWnQMjSAIG6qcV8J.htm](feats/class-18-GWnQMjSAIG6qcV8J.htm)|Reach for the Stars|auto-trad|
|[class-18-HBhLR980Q0cb2rxp.htm](feats/class-18-HBhLR980Q0cb2rxp.htm)|Perfect Debilitation|auto-trad|
|[class-18-Hzzf7bi8xBMi6DCL.htm](feats/class-18-Hzzf7bi8xBMi6DCL.htm)|Powerful Sneak|auto-trad|
|[class-18-i3hALsjbjk9FdbGN.htm](feats/class-18-i3hALsjbjk9FdbGN.htm)|Vicious Evisceration|auto-trad|
|[class-18-iTtnN49D8ZJ2Ilur.htm](feats/class-18-iTtnN49D8ZJ2Ilur.htm)|Deep Lore|auto-trad|
|[class-18-IxggfXunfldeVOsQ.htm](feats/class-18-IxggfXunfldeVOsQ.htm)|Swift River|auto-trad|
|[class-18-J8HLcsOkAcXfTxYy.htm](feats/class-18-J8HLcsOkAcXfTxYy.htm)|Negate Damage|auto-trad|
|[class-18-JkQjKyzfhMWLr9Gs.htm](feats/class-18-JkQjKyzfhMWLr9Gs.htm)|Perfect Clarity|auto-trad|
|[class-18-JSkaMCO6pzKYZrZe.htm](feats/class-18-JSkaMCO6pzKYZrZe.htm)|Unerring Shot|auto-trad|
|[class-18-K9hM3AdWGbU3VE8L.htm](feats/class-18-K9hM3AdWGbU3VE8L.htm)|Final Shot|auto-trad|
|[class-18-KpF7RenGBXIMMGPX.htm](feats/class-18-KpF7RenGBXIMMGPX.htm)|Split Hex|auto-trad|
|[class-18-kwz8el0Z8kK0D5az.htm](feats/class-18-kwz8el0Z8kK0D5az.htm)|True Transmogrification|auto-trad|
|[class-18-kYLtH4PYfvqruLyo.htm](feats/class-18-kYLtH4PYfvqruLyo.htm)|All in Your Head|auto-trad|
|[class-18-L5n4PvQYhpl2WM9e.htm](feats/class-18-L5n4PvQYhpl2WM9e.htm)|Impossible Polymath|auto-trad|
|[class-18-LAHiW98iPJKplFyK.htm](feats/class-18-LAHiW98iPJKplFyK.htm)|Infinite Possibilities|auto-trad|
|[class-18-LHjPTV5vP3MOsPPJ.htm](feats/class-18-LHjPTV5vP3MOsPPJ.htm)|Grand Scroll Esoterica|auto-trad|
|[class-18-LKxAuH0mLyzNygIY.htm](feats/class-18-LKxAuH0mLyzNygIY.htm)|Hex Wellspring|auto-trad|
|[class-18-LLCf2xPXA7FVQT1D.htm](feats/class-18-LLCf2xPXA7FVQT1D.htm)|Effortless Reach|auto-trad|
|[class-18-m2pHkmyGvkwqfSSN.htm](feats/class-18-m2pHkmyGvkwqfSSN.htm)|Miracle Worker|auto-trad|
|[class-18-mCXoiMLAbGHGsZS3.htm](feats/class-18-mCXoiMLAbGHGsZS3.htm)|Improved Swift Banishment|auto-trad|
|[class-18-NAgaDfwUSjPAon4o.htm](feats/class-18-NAgaDfwUSjPAon4o.htm)|Master Spellcasting|auto-trad|
|[class-18-nLidn7L2z61Ktjzk.htm](feats/class-18-nLidn7L2z61Ktjzk.htm)|Blaze Of Revelation|auto-trad|
|[class-18-nnsoFOtuHnpz2QHc.htm](feats/class-18-nnsoFOtuHnpz2QHc.htm)|Improbable Elixirs|auto-trad|
|[class-18-NvEYf0jIETEu2LtP.htm](feats/class-18-NvEYf0jIETEu2LtP.htm)|Echoing Spell|auto-trad|
|[class-18-NY2AkQscVIHEC8hQ.htm](feats/class-18-NY2AkQscVIHEC8hQ.htm)|Impossible Volley (Fighter)|auto-trad|
|[class-18-o1Vo4PvU09UY8sj7.htm](feats/class-18-o1Vo4PvU09UY8sj7.htm)|Conflux Wellspring|auto-trad|
|[class-18-oNHmfpe8ezZ3eKDD.htm](feats/class-18-oNHmfpe8ezZ3eKDD.htm)|Brutal Critical|auto-trad|
|[class-18-OqHfUQQorVBkx34j.htm](feats/class-18-OqHfUQQorVBkx34j.htm)|Impossible Flurry|auto-trad|
|[class-18-ouKDey5RHQKN9YBT.htm](feats/class-18-ouKDey5RHQKN9YBT.htm)|Domain Wellspring|auto-trad|
|[class-18-oYFsw3LdgsN2QKVs.htm](feats/class-18-oYFsw3LdgsN2QKVs.htm)|Ki Form|auto-trad|
|[class-18-POrE3ZgBRdBL9MsW.htm](feats/class-18-POrE3ZgBRdBL9MsW.htm)|Trickster's Ace|auto-trad|
|[class-18-QWPFf6NOObxaZJwW.htm](feats/class-18-QWPFf6NOObxaZJwW.htm)|Manifold Edge (Flurry)|auto-trad|
|[class-18-Rb16bcCiovwRqVgN.htm](feats/class-18-Rb16bcCiovwRqVgN.htm)|Ultimate Mercy|auto-trad|
|[class-18-SzWeWBuzg3e0k98A.htm](feats/class-18-SzWeWBuzg3e0k98A.htm)|Primal Wellspring|auto-trad|
|[class-18-t1sM6Xj9T07fqpwN.htm](feats/class-18-t1sM6Xj9T07fqpwN.htm)|Divine Effusion|auto-trad|
|[class-18-T4Xm8vYtnGMOM0Cw.htm](feats/class-18-T4Xm8vYtnGMOM0Cw.htm)|Echoing Channel|auto-trad|
|[class-18-uAdnQZSkUuxcpEwz.htm](feats/class-18-uAdnQZSkUuxcpEwz.htm)|Incredible Luck (Swashbuckler)|auto-trad|
|[class-18-uIL1pbp7jhYMjYLS.htm](feats/class-18-uIL1pbp7jhYMjYLS.htm)|Parry And Riposte|auto-trad|
|[class-18-v88bFLoJEF3YfJKb.htm](feats/class-18-v88bFLoJEF3YfJKb.htm)|Savage Critical|auto-trad|
|[class-18-VAbfepohLNtubfi3.htm](feats/class-18-VAbfepohLNtubfi3.htm)|Celestial Form|auto-trad|
|[class-18-vCsprcXVTwgtUYAZ.htm](feats/class-18-vCsprcXVTwgtUYAZ.htm)|Cranial Detonation|auto-trad|
|[class-18-W1CthcTSbsmOo7lP.htm](feats/class-18-W1CthcTSbsmOo7lP.htm)|Impossible Volley (Ranger)|auto-trad|
|[class-18-wBCt1wcgrjduwHbi.htm](feats/class-18-wBCt1wcgrjduwHbi.htm)|Warden's Wellspring|auto-trad|
|[class-18-wIRZDXOacqWfI670.htm](feats/class-18-wIRZDXOacqWfI670.htm)|Ki Center|auto-trad|
|[class-18-wqAdzjRUOvTpKFKq.htm](feats/class-18-wqAdzjRUOvTpKFKq.htm)|All In My Head|auto-trad|
|[class-18-XafnXTx9Bn0kN1RG.htm](feats/class-18-XafnXTx9Bn0kN1RG.htm)|Second Chance Spell|auto-trad|
|[class-18-XfCPAoNdF2XMnH7K.htm](feats/class-18-XfCPAoNdF2XMnH7K.htm)|Lethal Finisher|auto-trad|
|[class-18-Xlz96xFxueCk47pb.htm](feats/class-18-Xlz96xFxueCk47pb.htm)|Deepest Wellspring|auto-trad|
|[class-18-xZrTjUub7V09sXZF.htm](feats/class-18-xZrTjUub7V09sXZF.htm)|Rejuvenating Touch|auto-trad|
|[class-18-Y5irKSCSBn8z1Qgx.htm](feats/class-18-Y5irKSCSBn8z1Qgx.htm)|Meditative Wellspring|auto-trad|
|[class-18-ybrx1nsg5J0L8d3j.htm](feats/class-18-ybrx1nsg5J0L8d3j.htm)|Shared Clarity|auto-trad|
|[class-18-yFoBVSOCnC2R2r8s.htm](feats/class-18-yFoBVSOCnC2R2r8s.htm)|Eternal Composition|auto-trad|
|[class-18-yRRM1dsY6jakEMaC.htm](feats/class-18-yRRM1dsY6jakEMaC.htm)|Intense Implement|auto-trad|
|[class-18-yUuvixlhM4mcjKMb.htm](feats/class-18-yUuvixlhM4mcjKMb.htm)|Bloodline Wellspring|auto-trad|
|[class-18-YWRdk9oGMY6WmgIC.htm](feats/class-18-YWRdk9oGMY6WmgIC.htm)|Lead Investigator|auto-trad|
|[class-18-zCASpQconMmtJKQN.htm](feats/class-18-zCASpQconMmtJKQN.htm)|Implausible Purchase (Rogue)|auto-trad|
|[class-18-zrIrpVOvbGS6a3ux.htm](feats/class-18-zrIrpVOvbGS6a3ux.htm)|Perfect Shot|auto-trad|
|[class-18-zTulA4sVXwLRm28Z.htm](feats/class-18-zTulA4sVXwLRm28Z.htm)|Diamond Fists|auto-trad|
|[class-20-0208T5UrkTY2ombM.htm](feats/class-20-0208T5UrkTY2ombM.htm)|Perfect Mutagen|auto-trad|
|[class-20-0VUx8g8GJzuxvLSa.htm](feats/class-20-0VUx8g8GJzuxvLSa.htm)|Steal Essence|auto-trad|
|[class-20-1BAJxKpeQc8xGaxZ.htm](feats/class-20-1BAJxKpeQc8xGaxZ.htm)|All the Time in the World|auto-trad|
|[class-20-1sD5Gu8jQL09Yz2j.htm](feats/class-20-1sD5Gu8jQL09Yz2j.htm)|Sever Space|auto-trad|
|[class-20-2FBZ0apnmZ7b61ct.htm](feats/class-20-2FBZ0apnmZ7b61ct.htm)|Efficient Alchemy (Paragon)|auto-trad|
|[class-20-2sCzFjq8sKvBR3Jh.htm](feats/class-20-2sCzFjq8sKvBR3Jh.htm)|Ultimate Skirmisher|auto-trad|
|[class-20-3w2SktSOZdG8f6Qr.htm](feats/class-20-3w2SktSOZdG8f6Qr.htm)|Fatal Aria|auto-trad|
|[class-20-4E4121lbfWgxui4y.htm](feats/class-20-4E4121lbfWgxui4y.htm)|Song of the Fallen|auto-trad|
|[class-20-5C0XMnfTuvgSKD7o.htm](feats/class-20-5C0XMnfTuvgSKD7o.htm)|Become Thought|auto-trad|
|[class-20-5H3Sk1yhalQQzUys.htm](feats/class-20-5H3Sk1yhalQQzUys.htm)|Hex Master|auto-trad|
|[class-20-62USpCx1ewK03wzm.htm](feats/class-20-62USpCx1ewK03wzm.htm)|Ricochet Legend|auto-trad|
|[class-20-6SEDoht4dXEJE5SW.htm](feats/class-20-6SEDoht4dXEJE5SW.htm)|Bloodline Perfection|auto-trad|
|[class-20-6yJxUx0W2hwHckNi.htm](feats/class-20-6yJxUx0W2hwHckNi.htm)|Full Automation|auto-trad|
|[class-20-79pGj9RC1bAt82UD.htm](feats/class-20-79pGj9RC1bAt82UD.htm)|Unlimited Demesne|auto-trad|
|[class-20-7QLLwcSKNGPWdOGG.htm](feats/class-20-7QLLwcSKNGPWdOGG.htm)|Annihilating Swing|auto-trad|
|[class-20-84ML8enTqOOdXA8O.htm](feats/class-20-84ML8enTqOOdXA8O.htm)|Accurate Flurry|auto-trad|
|[class-20-85boZA8KRMu4rihN.htm](feats/class-20-85boZA8KRMu4rihN.htm)|Ubiquitous Overdrive|auto-trad|
|[class-20-9Slu8lSOYnDtKsIb.htm](feats/class-20-9Slu8lSOYnDtKsIb.htm)|Ruby Resurrection|auto-trad|
|[class-20-AmfO4FHmfFr0oNi9.htm](feats/class-20-AmfO4FHmfFr0oNi9.htm)|Bloodline Conduit|auto-trad|
|[class-20-AXXlinOc2lq08NPH.htm](feats/class-20-AXXlinOc2lq08NPH.htm)|Celestial Mount|auto-trad|
|[class-20-B3Fz46wzUIzrxWsA.htm](feats/class-20-B3Fz46wzUIzrxWsA.htm)|Legendary Shot|auto-trad|
|[class-20-banx3uX4JjjcHEc8.htm](feats/class-20-banx3uX4JjjcHEc8.htm)|Superior Sight (Low-Light Vision)|auto-trad|
|[class-20-BQkk7qSSRTFc5jNG.htm](feats/class-20-BQkk7qSSRTFc5jNG.htm)|Leyline Conduit|auto-trad|
|[class-20-Chu6s3xVnpOB64GH.htm](feats/class-20-Chu6s3xVnpOB64GH.htm)|Hierophant's Power|auto-trad|
|[class-20-czdqBRpzpml23la9.htm](feats/class-20-czdqBRpzpml23la9.htm)|Eternal Boost|auto-trad|
|[class-20-D8XoWk1brpyW6oO2.htm](feats/class-20-D8XoWk1brpyW6oO2.htm)|Plum Deluge|auto-trad|
|[class-20-dJ1ZviNMpt4ID7lc.htm](feats/class-20-dJ1ZviNMpt4ID7lc.htm)|Spell Combination|auto-trad|
|[class-20-DMECB9RwLAhY0T9o.htm](feats/class-20-DMECB9RwLAhY0T9o.htm)|Emblazon Divinity|auto-trad|
|[class-20-dwloLQzWgwjJWzXt.htm](feats/class-20-dwloLQzWgwjJWzXt.htm)|Banishing Blow|auto-trad|
|[class-20-EHorYedQ8r05qAtk.htm](feats/class-20-EHorYedQ8r05qAtk.htm)|Triple Threat|auto-trad|
|[class-20-eIW5p8qqvsx2MFkY.htm](feats/class-20-eIW5p8qqvsx2MFkY.htm)|Mystery Conduit|auto-trad|
|[class-20-EP8kaXNmrMfxOFAf.htm](feats/class-20-EP8kaXNmrMfxOFAf.htm)|Everdistant Defense|auto-trad|
|[class-20-epzeES7xJxvIXDdj.htm](feats/class-20-epzeES7xJxvIXDdj.htm)|Dance of Intercession|auto-trad|
|[class-20-evMhKTjzdiuDKwMX.htm](feats/class-20-evMhKTjzdiuDKwMX.htm)|Aura of Unbreakable Virtue|auto-trad|
|[class-20-flRXjabqedf6GjuU.htm](feats/class-20-flRXjabqedf6GjuU.htm)|Extradimensional Stash|auto-trad|
|[class-20-fMAUsLdBd5SDoHBz.htm](feats/class-20-fMAUsLdBd5SDoHBz.htm)|Paradoxical Mystery|auto-trad|
|[class-20-FMjihpGLn9eQ14Gw.htm](feats/class-20-FMjihpGLn9eQ14Gw.htm)|Quaking Stomp|auto-trad|
|[class-20-GIKySPq1n7xUmICw.htm](feats/class-20-GIKySPq1n7xUmICw.htm)|Bloodline Metamorphosis|auto-trad|
|[class-20-HEZeZcBWQR1QeWDo.htm](feats/class-20-HEZeZcBWQR1QeWDo.htm)|Astonishing Explosion|auto-trad|
|[class-20-HLCeP87w7qEy8PUH.htm](feats/class-20-HLCeP87w7qEy8PUH.htm)|To the Ends of the Earth|auto-trad|
|[class-20-hYW6MsPk1UcFROFD.htm](feats/class-20-hYW6MsPk1UcFROFD.htm)|Twin Eidolon|auto-trad|
|[class-20-iHUuWrvkT2uR0PnK.htm](feats/class-20-iHUuWrvkT2uR0PnK.htm)|Whirlwind Spell|auto-trad|
|[class-20-IMArawT1Sc2PTcYM.htm](feats/class-20-IMArawT1Sc2PTcYM.htm)|Boundless Reprisals|auto-trad|
|[class-20-JaBDNtNYYDfTGYad.htm](feats/class-20-JaBDNtNYYDfTGYad.htm)|Sacred Defender|auto-trad|
|[class-20-JEFPufbvaCeiA0Zo.htm](feats/class-20-JEFPufbvaCeiA0Zo.htm)|Weapon Supremacy|auto-trad|
|[class-20-jG9YwAAvNbCShumf.htm](feats/class-20-jG9YwAAvNbCShumf.htm)|Inexhaustible Countermoves|auto-trad|
|[class-20-k2hxQ9SPOM7aFWQZ.htm](feats/class-20-k2hxQ9SPOM7aFWQZ.htm)|Vivacious Afterimage|auto-trad|
|[class-20-kceciNwoldkzAMbq.htm](feats/class-20-kceciNwoldkzAMbq.htm)|Fuse Stance|auto-trad|
|[class-20-kKZ4gnT7okaWS6tB.htm](feats/class-20-kKZ4gnT7okaWS6tB.htm)|Metamagic Mastery|auto-trad|
|[class-20-Kl1O0WK37KMTumv1.htm](feats/class-20-Kl1O0WK37KMTumv1.htm)|Hidden Paragon|auto-trad|
|[class-20-KTMzVCd6xAqCvxa5.htm](feats/class-20-KTMzVCd6xAqCvxa5.htm)|Fiendish Mount|auto-trad|
|[class-20-l0Qy74a7CILdE4Th.htm](feats/class-20-l0Qy74a7CILdE4Th.htm)|Metamagic Channel|auto-trad|
|[class-20-Lbpm0OrQb4u2LVtj.htm](feats/class-20-Lbpm0OrQb4u2LVtj.htm)|Verdant Presence|auto-trad|
|[class-20-LCrBGoMGat2ZXuOo.htm](feats/class-20-LCrBGoMGat2ZXuOo.htm)|Ubiquitous Weakness|auto-trad|
|[class-20-LDIZtE7saDLSBduG.htm](feats/class-20-LDIZtE7saDLSBduG.htm)|Reactive Distraction|auto-trad|
|[class-20-LLrGafdJij7qiWZi.htm](feats/class-20-LLrGafdJij7qiWZi.htm)|Reclaim Spell|auto-trad|
|[class-20-lQpY6E5Zvc1kRnyC.htm](feats/class-20-lQpY6E5Zvc1kRnyC.htm)|Unlimited Potential|auto-trad|
|[class-20-mjdLmXLCNaRgMLVw.htm](feats/class-20-mjdLmXLCNaRgMLVw.htm)|Mimic Protections|auto-trad|
|[class-20-mMMIHLVSr8fyvVQL.htm](feats/class-20-mMMIHLVSr8fyvVQL.htm)|Mega Bomb|auto-trad|
|[class-20-mSqzGGttJvj4LxK9.htm](feats/class-20-mSqzGGttJvj4LxK9.htm)|Impossible Striker|auto-trad|
|[class-20-N1ajKcWRo3O0oMQg.htm](feats/class-20-N1ajKcWRo3O0oMQg.htm)|Bloodline Mutation|auto-trad|
|[class-20-NGv7sphkVgF3CtXK.htm](feats/class-20-NGv7sphkVgF3CtXK.htm)|Wonder Worker|auto-trad|
|[class-20-nJe8eQUrIpKWLXh5.htm](feats/class-20-nJe8eQUrIpKWLXh5.htm)|Contagious Rage|auto-trad|
|[class-20-OliKxFIqzky2o6vk.htm](feats/class-20-OliKxFIqzky2o6vk.htm)|Ringmaster's Introduction|auto-trad|
|[class-20-Oo3yRbmrgqi8dmVs.htm](feats/class-20-Oo3yRbmrgqi8dmVs.htm)|Panache Paragon|auto-trad|
|[class-20-opeP0JF9WGmNG0pb.htm](feats/class-20-opeP0JF9WGmNG0pb.htm)|Avatar's Audience|auto-trad|
|[class-20-OYrcbyaV3v8ycksj.htm](feats/class-20-OYrcbyaV3v8ycksj.htm)|Head of the Night Parade|auto-trad|
|[class-20-p353WH847errsNvh.htm](feats/class-20-p353WH847errsNvh.htm)|Apex Companion|auto-trad|
|[class-20-P9swngiLXbhMegQ8.htm](feats/class-20-P9swngiLXbhMegQ8.htm)|Shield Paragon|auto-trad|
|[class-20-PDFbfCrV2z0wfMz0.htm](feats/class-20-PDFbfCrV2z0wfMz0.htm)|Demon's Hair|auto-trad|
|[class-20-PExiZZTSP4p7TZaW.htm](feats/class-20-PExiZZTSP4p7TZaW.htm)|Oracular Providence|auto-trad|
|[class-20-PTAdxHcTS7TjyBTg.htm](feats/class-20-PTAdxHcTS7TjyBTg.htm)|Twin Psyche|auto-trad|
|[class-20-QDjpZKOrWIV1G8XJ.htm](feats/class-20-QDjpZKOrWIV1G8XJ.htm)|Maker of Miracles|auto-trad|
|[class-20-QSBuAkJ5GMLcuZg9.htm](feats/class-20-QSBuAkJ5GMLcuZg9.htm)|Ultimate Polymath|auto-trad|
|[class-20-rMjlDss3Km1RQ8DE.htm](feats/class-20-rMjlDss3Km1RQ8DE.htm)|Slinger's Reflexes|auto-trad|
|[class-20-rwTbN2A2ZO7CdKoC.htm](feats/class-20-rwTbN2A2ZO7CdKoC.htm)|Impossible Snares|auto-trad|
|[class-20-RYUb5oxd46Yvdypz.htm](feats/class-20-RYUb5oxd46Yvdypz.htm)|Endurance of the Rooted Tree|auto-trad|
|[class-20-RzfWrOqHL2GcK0rr.htm](feats/class-20-RzfWrOqHL2GcK0rr.htm)|Enduring Debilitation|auto-trad|
|[class-20-sayRIHE2V6vuxr4r.htm](feats/class-20-sayRIHE2V6vuxr4r.htm)|Ultimate Flexibility|auto-trad|
|[class-20-SelPslNtTfzxp7fs.htm](feats/class-20-SelPslNtTfzxp7fs.htm)|Patron's Truth|auto-trad|
|[class-20-sfxLo9kz2WkCQiy4.htm](feats/class-20-sfxLo9kz2WkCQiy4.htm)|Symphony of the Muse|auto-trad|
|[class-20-siegOEdEpevAJNFw.htm](feats/class-20-siegOEdEpevAJNFw.htm)|Denier of Destruction|auto-trad|
|[class-20-Sq9muixjFptJO8Ae.htm](feats/class-20-Sq9muixjFptJO8Ae.htm)|Witch's Hut|auto-trad|
|[class-20-srWsvDDdz77yieY1.htm](feats/class-20-srWsvDDdz77yieY1.htm)|Impossible Technique|auto-trad|
|[class-20-tBWSxVxrojRcEzJt.htm](feats/class-20-tBWSxVxrojRcEzJt.htm)|Scapegoat Parallel Self|auto-trad|
|[class-20-Tj79ePSD212EZjRM.htm](feats/class-20-Tj79ePSD212EZjRM.htm)|Vitality-Manipulating Stance|auto-trad|
|[class-20-TLCeFMDRXFB46sa8.htm](feats/class-20-TLCeFMDRXFB46sa8.htm)|Deadly Strikes|auto-trad|
|[class-20-tP26mgaFPpr6df1i.htm](feats/class-20-tP26mgaFPpr6df1i.htm)|Archwizard's Might|auto-trad|
|[class-20-TsgW87kYudNr6Bfp.htm](feats/class-20-TsgW87kYudNr6Bfp.htm)|Wish Alchemy|auto-trad|
|[class-20-txLcSHu6kEfmrJj1.htm](feats/class-20-txLcSHu6kEfmrJj1.htm)|Enduring Quickness|auto-trad|
|[class-20-UlCbjWWVEmfvaf5a.htm](feats/class-20-UlCbjWWVEmfvaf5a.htm)|Tenacious Blood Magic|auto-trad|
|[class-20-Uof5QNeGklGnks1h.htm](feats/class-20-Uof5QNeGklGnks1h.htm)|Superior Sight|auto-trad|
|[class-20-UOxDJt8Y7SiCR4xq.htm](feats/class-20-UOxDJt8Y7SiCR4xq.htm)|Pied Piping|auto-trad|
|[class-20-uwp7Y4LNtPbhELjS.htm](feats/class-20-uwp7Y4LNtPbhELjS.htm)|Golden Body|auto-trad|
|[class-20-Ux9cBtDRP92EM6rl.htm](feats/class-20-Ux9cBtDRP92EM6rl.htm)|Mind Over Matter|auto-trad|
|[class-20-VSyuTWRuxdmgq2HS.htm](feats/class-20-VSyuTWRuxdmgq2HS.htm)|Invulnerable Juggernaut|auto-trad|
|[class-20-w4dijKncXx0ssBOQ.htm](feats/class-20-w4dijKncXx0ssBOQ.htm)|Everyone's A Suspect|auto-trad|
|[class-20-wPjNjyh60fYKrDXl.htm](feats/class-20-wPjNjyh60fYKrDXl.htm)|Legendary Summoner|auto-trad|
|[class-20-Wukrctjz2e8W4bbS.htm](feats/class-20-Wukrctjz2e8W4bbS.htm)|Perfect Readiness|auto-trad|
|[class-20-XenKYUBMWZQQ7niM.htm](feats/class-20-XenKYUBMWZQQ7niM.htm)|Whirlwind Toss|auto-trad|
|[class-20-xhiwito5kneP4sjV.htm](feats/class-20-xhiwito5kneP4sjV.htm)|Spell Mastery|auto-trad|
|[class-20-xhuXpOFOxDpJgngm.htm](feats/class-20-xhuXpOFOxDpJgngm.htm)|Perfect Encore|auto-trad|
|[class-20-xM0vwRFLZgmmI4YJ.htm](feats/class-20-xM0vwRFLZgmmI4YJ.htm)|Time Dilation Cascade|auto-trad|
|[class-20-xqjPVZezw1a73JAO.htm](feats/class-20-xqjPVZezw1a73JAO.htm)|Worldsphere Gravity|auto-trad|
|[class-20-YbneCzvDEr76mrsS.htm](feats/class-20-YbneCzvDEr76mrsS.htm)|Superior Sight (Darkvision)|auto-trad|
|[class-20-YCqHaqn0TxdiGxiW.htm](feats/class-20-YCqHaqn0TxdiGxiW.htm)|Unstoppable Juggernaut|auto-trad|
|[class-20-Yec6UwJf5FLvAbZ4.htm](feats/class-20-Yec6UwJf5FLvAbZ4.htm)|Reflecting Riposte|auto-trad|
|[class-20-ZfycfbXlPXZlSqw5.htm](feats/class-20-ZfycfbXlPXZlSqw5.htm)|Just The Facts|auto-trad|
|[class-20-zOK6IFSz3DIBRjEw.htm](feats/class-20-zOK6IFSz3DIBRjEw.htm)|Craft Philosopher's Stone|auto-trad|
|[class-20-zy7lx4SWkfDxqH6m.htm](feats/class-20-zy7lx4SWkfDxqH6m.htm)|Supreme Spellstrike|auto-trad|
|[general-01-2kwXTUjYYhoAGySr.htm](feats/general-01-2kwXTUjYYhoAGySr.htm)|Incredible Initiative|auto-trad|
|[general-01-aJUXbe9HJVvv0Mxa.htm](feats/general-01-aJUXbe9HJVvv0Mxa.htm)|Breath Control|auto-trad|
|[general-01-AmP0qu7c5dlBSath.htm](feats/general-01-AmP0qu7c5dlBSath.htm)|Toughness|auto-trad|
|[general-01-BStw1cANwx5baL6d.htm](feats/general-01-BStw1cANwx5baL6d.htm)|Armor Proficiency|auto-trad|
|[general-01-c9fHUSI5lRdXu1Ic.htm](feats/general-01-c9fHUSI5lRdXu1Ic.htm)|Feather Step|auto-trad|
|[general-01-I0BhPWqYf1bbzEYg.htm](feats/general-01-I0BhPWqYf1bbzEYg.htm)|Diehard|auto-trad|
|[general-01-i4yRvVwvXbGZDsD1.htm](feats/general-01-i4yRvVwvXbGZDsD1.htm)|Canny Acumen|auto-trad|
|[general-01-ihN8gkHSdPG9Trte.htm](feats/general-01-ihN8gkHSdPG9Trte.htm)|Adopted Ancestry|auto-trad|
|[general-01-jM72TjJ965jocBV8.htm](feats/general-01-jM72TjJ965jocBV8.htm)|Shield Block|auto-trad|
|[general-01-N8Xz5fuW6o7GW124.htm](feats/general-01-N8Xz5fuW6o7GW124.htm)|Fast Recovery|auto-trad|
|[general-01-Rq5wkA8DtsmbzoGV.htm](feats/general-01-Rq5wkA8DtsmbzoGV.htm)|Ride|auto-trad|
|[general-01-Ux73dmoF8KnavyUD.htm](feats/general-01-Ux73dmoF8KnavyUD.htm)|Fleet|auto-trad|
|[general-01-x9wxQ61HNkAVbDHr.htm](feats/general-01-x9wxQ61HNkAVbDHr.htm)|Weapon Proficiency|auto-trad|
|[general-01-z1Z22gTp7J1VRLSR.htm](feats/general-01-z1Z22gTp7J1VRLSR.htm)|Different Worlds|auto-trad|
|[general-03-38uOVS8fLZxraUrg.htm](feats/general-03-38uOVS8fLZxraUrg.htm)|Pick Up The Pace|auto-trad|
|[general-03-9jGaBxLUtevZYcZO.htm](feats/general-03-9jGaBxLUtevZYcZO.htm)|Untrained Improvisation|auto-trad|
|[general-03-9QQnv7nFpsNCGE89.htm](feats/general-03-9QQnv7nFpsNCGE89.htm)|Thorough Search|auto-trad|
|[general-03-bh2jHyyYrkLMsIdX.htm](feats/general-03-bh2jHyyYrkLMsIdX.htm)|Prescient Planner|auto-trad|
|[general-03-fD9xjrnPfJ8aQxYA.htm](feats/general-03-fD9xjrnPfJ8aQxYA.htm)|Keen Follower|auto-trad|
|[general-03-jFmdevE4nKevovzo.htm](feats/general-03-jFmdevE4nKevovzo.htm)|Steel Your Resolve|auto-trad|
|[general-03-m7KjpkAAh9PptJsY.htm](feats/general-03-m7KjpkAAh9PptJsY.htm)|Ancestral Paragon|auto-trad|
|[general-03-Wb3FHiDuY6Nuc0N0.htm](feats/general-03-Wb3FHiDuY6Nuc0N0.htm)|Hireling Manager|auto-trad|
|[general-03-wPHZhgKzNw4VcCFt.htm](feats/general-03-wPHZhgKzNw4VcCFt.htm)|Skitter|auto-trad|
|[general-03-xT593tHyPkumPuzz.htm](feats/general-03-xT593tHyPkumPuzz.htm)|Improvised Repair|auto-trad|
|[general-07-fOIUmDGa9gkeCHA0.htm](feats/general-07-fOIUmDGa9gkeCHA0.htm)|Supertaster|auto-trad|
|[general-07-GdZLxDtFXaQI3Fop.htm](feats/general-07-GdZLxDtFXaQI3Fop.htm)|Expeditious Search|auto-trad|
|[general-07-gfMP2aMs3YGONVeB.htm](feats/general-07-gfMP2aMs3YGONVeB.htm)|Numb to Death|auto-trad|
|[general-07-lHFz4MmebvPqTb0A.htm](feats/general-07-lHFz4MmebvPqTb0A.htm)|Prescient Consumable|auto-trad|
|[general-11-4vu6P3cYoOOeUbLK.htm](feats/general-11-4vu6P3cYoOOeUbLK.htm)|Incredible Investiture|auto-trad|
|[general-11-5ZsmRm7HvFAw2XDZ.htm](feats/general-11-5ZsmRm7HvFAw2XDZ.htm)|Caravan Leader|auto-trad|
|[general-11-aFoVHsuInMOkTZoQ.htm](feats/general-11-aFoVHsuInMOkTZoQ.htm)|Incredible Scout|auto-trad|
|[general-11-muMOxZyduEFv8UT6.htm](feats/general-11-muMOxZyduEFv8UT6.htm)|A Home In Every Port|auto-trad|
|[general-19-uudiUylT09WnHN7e.htm](feats/general-19-uudiUylT09WnHN7e.htm)|True Perception|auto-trad|
|[skill-01-09PurtIanNUPfNRq.htm](feats/skill-01-09PurtIanNUPfNRq.htm)|Combat Climber|auto-trad|
|[skill-01-0GF2j54roPFIDmXf.htm](feats/skill-01-0GF2j54roPFIDmXf.htm)|Bon Mot|auto-trad|
|[skill-01-0N8TtGSk5enoLBZ8.htm](feats/skill-01-0N8TtGSk5enoLBZ8.htm)|Eye For Numbers|auto-trad|
|[skill-01-1Bt7uCW2WI4sM84P.htm](feats/skill-01-1Bt7uCW2WI4sM84P.htm)|Dubious Knowledge|auto-trad|
|[skill-01-1Eceqc6zbMj2x0q9.htm](feats/skill-01-1Eceqc6zbMj2x0q9.htm)|Seasoned|auto-trad|
|[skill-01-22P7IFyhrF7Fbw8B.htm](feats/skill-01-22P7IFyhrF7Fbw8B.htm)|Root Magic|auto-trad|
|[skill-01-3G8xUlgCjRmRJNfP.htm](feats/skill-01-3G8xUlgCjRmRJNfP.htm)|Quick Squeeze|auto-trad|
|[skill-01-3HChkcD1IRqv4DbA.htm](feats/skill-01-3HChkcD1IRqv4DbA.htm)|Improvise Tool|auto-trad|
|[skill-01-4RjDxgvNXNl5GG9d.htm](feats/skill-01-4RjDxgvNXNl5GG9d.htm)|Hobnobber|auto-trad|
|[skill-01-4tTkRyOQ0VuRBac3.htm](feats/skill-01-4tTkRyOQ0VuRBac3.htm)|Terrain Expertise|auto-trad|
|[skill-01-5nc5ridFBfYpn2Om.htm](feats/skill-01-5nc5ridFBfYpn2Om.htm)|Bargain Hunter|auto-trad|
|[skill-01-5s8FqK4YZTVOvP0v.htm](feats/skill-01-5s8FqK4YZTVOvP0v.htm)|Reveal True Name|auto-trad|
|[skill-01-6GO3dtFJnsNnSwWz.htm](feats/skill-01-6GO3dtFJnsNnSwWz.htm)|Medical Researcher|auto-trad|
|[skill-01-6O8MoMheHs5hNHX1.htm](feats/skill-01-6O8MoMheHs5hNHX1.htm)|Crystal Healing|auto-trad|
|[skill-01-6ON8DjFXSMITZleX.htm](feats/skill-01-6ON8DjFXSMITZleX.htm)|No Cause For Alarm|auto-trad|
|[skill-01-7LB00jkh6JaJr3vS.htm](feats/skill-01-7LB00jkh6JaJr3vS.htm)|Fascinating Performance|auto-trad|
|[skill-01-8qebBeOJsyRIchcu.htm](feats/skill-01-8qebBeOJsyRIchcu.htm)|Forager|auto-trad|
|[skill-01-aAoFc10cOpxGypOY.htm](feats/skill-01-aAoFc10cOpxGypOY.htm)|Sign Language|auto-trad|
|[skill-01-ar2DUlvDK4LDcH9J.htm](feats/skill-01-ar2DUlvDK4LDcH9J.htm)|Quick Coercion|auto-trad|
|[skill-01-ASy9AKEIRxPYUi5o.htm](feats/skill-01-ASy9AKEIRxPYUi5o.htm)|Quick Repair|auto-trad|
|[skill-01-B6HbYsLBWb1RR6Fx.htm](feats/skill-01-B6HbYsLBWb1RR6Fx.htm)|Charming Liar|auto-trad|
|[skill-01-B9cQLRHtXoLlF0iR.htm](feats/skill-01-B9cQLRHtXoLlF0iR.htm)|Concealing Legerdemain|auto-trad|
|[skill-01-beyw5bdA5hkQbmaG.htm](feats/skill-01-beyw5bdA5hkQbmaG.htm)|Terrain Stalker|auto-trad|
|[skill-01-bkZgWFSFV4cAf5Ot.htm](feats/skill-01-bkZgWFSFV4cAf5Ot.htm)|Risky Surgery|auto-trad|
|[skill-01-blMeVamjGz4ODWxq.htm](feats/skill-01-blMeVamjGz4ODWxq.htm)|Arcane Sense|auto-trad|
|[skill-01-BocFD2KV0qgUC76x.htm](feats/skill-01-BocFD2KV0qgUC76x.htm)|Additional Lore|auto-trad|
|[skill-01-BqceQIKE0lwIS98s.htm](feats/skill-01-BqceQIKE0lwIS98s.htm)|Pilgrim's Token|auto-trad|
|[skill-01-C0Tcelg3BAPhML6J.htm](feats/skill-01-C0Tcelg3BAPhML6J.htm)|Hefty Hauler|auto-trad|
|[skill-01-CnqMJR8e9jqJR7MM.htm](feats/skill-01-CnqMJR8e9jqJR7MM.htm)|Steady Balance|auto-trad|
|[skill-01-d8AjCqU30z7IOpos.htm](feats/skill-01-d8AjCqU30z7IOpos.htm)|Ravening's Desperation|auto-trad|
|[skill-01-DMetdzt1VJL2Y62i.htm](feats/skill-01-DMetdzt1VJL2Y62i.htm)|Snare Crafting|auto-trad|
|[skill-01-Dvz54d6aPhjsmUux.htm](feats/skill-01-Dvz54d6aPhjsmUux.htm)|Lie To Me|auto-trad|
|[skill-01-dZDmWXzZfIoBJ53Q.htm](feats/skill-01-dZDmWXzZfIoBJ53Q.htm)|Tame Animal|auto-trad|
|[skill-01-f0faBEUPtspdutKx.htm](feats/skill-01-f0faBEUPtspdutKx.htm)|Acrobatic Performer|auto-trad|
|[skill-01-FbGPETHJR9VKxf9i.htm](feats/skill-01-FbGPETHJR9VKxf9i.htm)|Folk Dowsing|auto-trad|
|[skill-01-gArdEleFCvUHtdGk.htm](feats/skill-01-gArdEleFCvUHtdGk.htm)|Express Rider|auto-trad|
|[skill-01-gnH9SpdNQegDqIar.htm](feats/skill-01-gnH9SpdNQegDqIar.htm)|All of the Animal|auto-trad|
|[skill-01-gUqvezs2zzoTXFAI.htm](feats/skill-01-gUqvezs2zzoTXFAI.htm)|Group Coercion|auto-trad|
|[skill-01-hDGosy2ZTwnyctEP.htm](feats/skill-01-hDGosy2ZTwnyctEP.htm)|Oddity Identification|auto-trad|
|[skill-01-HEBXaS656MZTiWFu.htm](feats/skill-01-HEBXaS656MZTiWFu.htm)|Lengthy Diversion|auto-trad|
|[skill-01-hVZbnsDuXihggylt.htm](feats/skill-01-hVZbnsDuXihggylt.htm)|Subtle Theft|auto-trad|
|[skill-01-IlOQuCQIhjJpig3S.htm](feats/skill-01-IlOQuCQIhjJpig3S.htm)|Quick Identification|auto-trad|
|[skill-01-iOY6YfGBaOvMNAor.htm](feats/skill-01-iOY6YfGBaOvMNAor.htm)|Underwater Marauder|auto-trad|
|[skill-01-is3Oz9wt11lNq62K.htm](feats/skill-01-is3Oz9wt11lNq62K.htm)|Alchemical Crafting|auto-trad|
|[skill-01-jDdOqFmZLwE4dblQ.htm](feats/skill-01-jDdOqFmZLwE4dblQ.htm)|Pickpocket|auto-trad|
|[skill-01-JtjnFsOToBLnSRO9.htm](feats/skill-01-JtjnFsOToBLnSRO9.htm)|Courtly Graces|auto-trad|
|[skill-01-KpFetnUqTiweypZk.htm](feats/skill-01-KpFetnUqTiweypZk.htm)|Group Impression|auto-trad|
|[skill-01-KxaYlC50zzHysJj8.htm](feats/skill-01-KxaYlC50zzHysJj8.htm)|Titan Wrestler|auto-trad|
|[skill-01-Lc4dJZivRwU3QEmT.htm](feats/skill-01-Lc4dJZivRwU3QEmT.htm)|Armor Assist|auto-trad|
|[skill-01-lEgYzFHransLkSvI.htm](feats/skill-01-lEgYzFHransLkSvI.htm)|Schooled In Secrets|auto-trad|
|[skill-01-lFwfUvH2708rl6i3.htm](feats/skill-01-lFwfUvH2708rl6i3.htm)|Virtuosic Performer|auto-trad|
|[skill-01-lQs2i9L09MQiZSPC.htm](feats/skill-01-lQs2i9L09MQiZSPC.htm)|Charlatan|auto-trad|
|[skill-01-LQw0yIMDUJJkq1nD.htm](feats/skill-01-LQw0yIMDUJJkq1nD.htm)|Cat Fall|auto-trad|
|[skill-01-MjQyTcV8Jiv1Jtln.htm](feats/skill-01-MjQyTcV8Jiv1Jtln.htm)|Recognize Spell|auto-trad|
|[skill-01-nowEaHgIyij7im8F.htm](feats/skill-01-nowEaHgIyij7im8F.htm)|Train Animal|auto-trad|
|[skill-01-p5Bmj3d0uAGnrzIn.htm](feats/skill-01-p5Bmj3d0uAGnrzIn.htm)|Read Psychometric Resonance|auto-trad|
|[skill-01-P6icK2DbRoZ3H6kc.htm](feats/skill-01-P6icK2DbRoZ3H6kc.htm)|Skill Training|auto-trad|
|[skill-01-P9HCz0uR6xPHuw72.htm](feats/skill-01-P9HCz0uR6xPHuw72.htm)|Multilingual|auto-trad|
|[skill-01-QLeMH5mQgh28sa5o.htm](feats/skill-01-QLeMH5mQgh28sa5o.htm)|Specialty Crafting|auto-trad|
|[skill-01-QShgLWlfKYJO750P.htm](feats/skill-01-QShgLWlfKYJO750P.htm)|Stitch Flesh|auto-trad|
|[skill-01-r7cgrrHh75R8UEqN.htm](feats/skill-01-r7cgrrHh75R8UEqN.htm)|Deceptive Worship|auto-trad|
|[skill-01-sMCpihnBEpx18GBD.htm](feats/skill-01-sMCpihnBEpx18GBD.htm)|Inoculation|auto-trad|
|[skill-01-sMm0UfYxEPpq2Yzd.htm](feats/skill-01-sMm0UfYxEPpq2Yzd.htm)|Experienced Professional|auto-trad|
|[skill-01-t3btih0O5RUwWynt.htm](feats/skill-01-t3btih0O5RUwWynt.htm)|Experienced Smuggler|auto-trad|
|[skill-01-tGIXuk0XeWmG04CX.htm](feats/skill-01-tGIXuk0XeWmG04CX.htm)|Survey Wildlife|auto-trad|
|[skill-01-uR62fVC9FyQAMCO1.htm](feats/skill-01-uR62fVC9FyQAMCO1.htm)|Trick Magic Item|auto-trad|
|[skill-01-urQZwmzg2kS53vd5.htm](feats/skill-01-urQZwmzg2kS53vd5.htm)|Experienced Tracker|auto-trad|
|[skill-01-voYr7ygVcWmlg1f4.htm](feats/skill-01-voYr7ygVcWmlg1f4.htm)|Crafter's Appraisal|auto-trad|
|[skill-01-W6Gl9ePmItfDHji0.htm](feats/skill-01-W6Gl9ePmItfDHji0.htm)|Assurance|auto-trad|
|[skill-01-wbjTkaKRygpaZS0r.htm](feats/skill-01-wbjTkaKRygpaZS0r.htm)|Secret Speech|auto-trad|
|[skill-01-WC4xLBGmBsdOdHWu.htm](feats/skill-01-WC4xLBGmBsdOdHWu.htm)|Natural Medicine|auto-trad|
|[skill-01-WeQGWvlWdeLeOlCN.htm](feats/skill-01-WeQGWvlWdeLeOlCN.htm)|Glean Contents|auto-trad|
|[skill-01-Ws9JlysHcFoz6WAQ.htm](feats/skill-01-Ws9JlysHcFoz6WAQ.htm)|Forensic Acumen|auto-trad|
|[skill-01-wYerMk6F1RZb0Fwt.htm](feats/skill-01-wYerMk6F1RZb0Fwt.htm)|Battle Medicine|auto-trad|
|[skill-01-X2jGFfLU5qI5XVot.htm](feats/skill-01-X2jGFfLU5qI5XVot.htm)|Streetwise|auto-trad|
|[skill-01-x7EMZNMavris2aHY.htm](feats/skill-01-x7EMZNMavris2aHY.htm)|Student of the Canon|auto-trad|
|[skill-01-xqAdXRd2gSQcqp5E.htm](feats/skill-01-xqAdXRd2gSQcqp5E.htm)|Impressive Performance|auto-trad|
|[skill-01-xQMz6eDgX75WX2ce.htm](feats/skill-01-xQMz6eDgX75WX2ce.htm)|Intimidating Glare|auto-trad|
|[skill-01-yUuU9xyotrpwpTyC.htm](feats/skill-01-yUuU9xyotrpwpTyC.htm)|Read Lips|auto-trad|
|[skill-01-ZBhvJ9O8MvBFAlhq.htm](feats/skill-01-ZBhvJ9O8MvBFAlhq.htm)|Quick Jump|auto-trad|
|[skill-02-3ZerjLH8ls3JT6cD.htm](feats/skill-02-3ZerjLH8ls3JT6cD.htm)|Robust Recovery|auto-trad|
|[skill-02-4UXyMtXLaOxuH6Js.htm](feats/skill-02-4UXyMtXLaOxuH6Js.htm)|Distracting Performance|auto-trad|
|[skill-02-5f8gQxVdioUcgsTD.htm](feats/skill-02-5f8gQxVdioUcgsTD.htm)|Familiar Oddities|auto-trad|
|[skill-02-5I97q0FfAeXcUQhs.htm](feats/skill-02-5I97q0FfAeXcUQhs.htm)|Nimble Crawl|auto-trad|
|[skill-02-6vnbC90UQ3I57RrQ.htm](feats/skill-02-6vnbC90UQ3I57RrQ.htm)|Lasting Coercion|auto-trad|
|[skill-02-6Z4e3B9vePYpibcy.htm](feats/skill-02-6Z4e3B9vePYpibcy.htm)|Confabulator|auto-trad|
|[skill-02-7F3sTNRoNsQgD8tX.htm](feats/skill-02-7F3sTNRoNsQgD8tX.htm)|Backup Disguise|auto-trad|
|[skill-02-7t2VGjVhYNU3MsEm.htm](feats/skill-02-7t2VGjVhYNU3MsEm.htm)|Spirit Speaker|auto-trad|
|[skill-02-8c61nOIr5AM3KxZi.htm](feats/skill-02-8c61nOIr5AM3KxZi.htm)|Ward Medic|auto-trad|
|[skill-02-aMI39DZhWgNgJTAn.htm](feats/skill-02-aMI39DZhWgNgJTAn.htm)|Malleable Movement|auto-trad|
|[skill-02-AYb8PmGJ37HwIMwj.htm](feats/skill-02-AYb8PmGJ37HwIMwj.htm)|Juggle|auto-trad|
|[skill-02-BV5jpSifVJsTwoO7.htm](feats/skill-02-BV5jpSifVJsTwoO7.htm)|Wilderness Spotter|auto-trad|
|[skill-02-c85a69mB1urW2Se2.htm](feats/skill-02-c85a69mB1urW2Se2.htm)|Continual Recovery|auto-trad|
|[skill-02-cc8O47KFsODReoBe.htm](feats/skill-02-cc8O47KFsODReoBe.htm)|Glad-Hand|auto-trad|
|[skill-02-cmuvvPJvt2R16vGe.htm](feats/skill-02-cmuvvPJvt2R16vGe.htm)|Fresh Ingredients|auto-trad|
|[skill-02-dUnT3HWMFD3d2eBJ.htm](feats/skill-02-dUnT3HWMFD3d2eBJ.htm)|Encouraging Words|auto-trad|
|[skill-02-e4KB4pSkx2lDBNw3.htm](feats/skill-02-e4KB4pSkx2lDBNw3.htm)|Quick Disguise|auto-trad|
|[skill-02-EZrxp0XxYh9rjghB.htm](feats/skill-02-EZrxp0XxYh9rjghB.htm)|Embed Aeon Stone|auto-trad|
|[skill-02-FJK7JTLSgugRIlvS.htm](feats/skill-02-FJK7JTLSgugRIlvS.htm)|Rapid Mantel|auto-trad|
|[skill-02-gydOsP9VsdRw3Wg1.htm](feats/skill-02-gydOsP9VsdRw3Wg1.htm)|Underground Network|auto-trad|
|[skill-02-H3I2X0f7v4EzwxuN.htm](feats/skill-02-H3I2X0f7v4EzwxuN.htm)|Automatic Knowledge|auto-trad|
|[skill-02-HJYQlmGTdtyGWr6a.htm](feats/skill-02-HJYQlmGTdtyGWr6a.htm)|Powerful Leap|auto-trad|
|[skill-02-iCv8KTZ5PcF4GqeV.htm](feats/skill-02-iCv8KTZ5PcF4GqeV.htm)|Connections|auto-trad|
|[skill-02-iDimfGmQHacwxeh2.htm](feats/skill-02-iDimfGmQHacwxeh2.htm)|Terrifying Resistance|auto-trad|
|[skill-02-K2R1xGTgBnSCDMUl.htm](feats/skill-02-K2R1xGTgBnSCDMUl.htm)|Express Driver|auto-trad|
|[skill-02-lB9MVGCJr7aJQuIH.htm](feats/skill-02-lB9MVGCJr7aJQuIH.htm)|Armored Stealth|auto-trad|
|[skill-02-LDUWw2TeEEH0KA6M.htm](feats/skill-02-LDUWw2TeEEH0KA6M.htm)|Criminal Connections|auto-trad|
|[skill-02-mEk2POFNU1Q0TQg2.htm](feats/skill-02-mEk2POFNU1Q0TQg2.htm)|Mortal Healing|auto-trad|
|[skill-02-N7IsnLDFt73r7x56.htm](feats/skill-02-N7IsnLDFt73r7x56.htm)|Shadow Mark|auto-trad|
|[skill-02-nJ3EBRat9yUgeWwv.htm](feats/skill-02-nJ3EBRat9yUgeWwv.htm)|Automatic Writing|auto-trad|
|[skill-02-NUHosufuAhQCnF7N.htm](feats/skill-02-NUHosufuAhQCnF7N.htm)|Triumphant Boast|auto-trad|
|[skill-02-OtV7esAwza1U6Kwr.htm](feats/skill-02-OtV7esAwza1U6Kwr.htm)|Eye of the Arclords|auto-trad|
|[skill-02-pekKtubQTkG9m1xK.htm](feats/skill-02-pekKtubQTkG9m1xK.htm)|Quiet Allies|auto-trad|
|[skill-02-Peyf3L7c9esRTsgR.htm](feats/skill-02-Peyf3L7c9esRTsgR.htm)|Predict Weather|auto-trad|
|[skill-02-pLjvgeqwHrYdg411.htm](feats/skill-02-pLjvgeqwHrYdg411.htm)|Bonded Animal|auto-trad|
|[skill-02-QvBIYW6aAqoiyim3.htm](feats/skill-02-QvBIYW6aAqoiyim3.htm)|Exhort The Faithful|auto-trad|
|[skill-02-qvLcZGsV0HP2O0CG.htm](feats/skill-02-qvLcZGsV0HP2O0CG.htm)|Battle Planner|auto-trad|
|[skill-02-RLBYJiGMVkaGL5w9.htm](feats/skill-02-RLBYJiGMVkaGL5w9.htm)|Wary Disarmament|auto-trad|
|[skill-02-RlFZ648UR0Q0YECL.htm](feats/skill-02-RlFZ648UR0Q0YECL.htm)|Chromotherapy|auto-trad|
|[skill-02-sLEawQueTV1wGn0B.htm](feats/skill-02-sLEawQueTV1wGn0B.htm)|Sow Rumor|auto-trad|
|[skill-02-ThoOsKjn5xCuZUqM.htm](feats/skill-02-ThoOsKjn5xCuZUqM.htm)|Quick Contacts|auto-trad|
|[skill-02-uF0ATN2Zw1Q67ew2.htm](feats/skill-02-uF0ATN2Zw1Q67ew2.htm)|Discreet Inquiry|auto-trad|
|[skill-02-v7Bt6hjmzYnLFLeG.htm](feats/skill-02-v7Bt6hjmzYnLFLeG.htm)|Magical Shorthand|auto-trad|
|[skill-02-vjQ3VUpYlTAAIx3b.htm](feats/skill-02-vjQ3VUpYlTAAIx3b.htm)|Lead Climber|auto-trad|
|[skill-02-vWtPxwND60EpxBAU.htm](feats/skill-02-vWtPxwND60EpxBAU.htm)|Tweak Appearances|auto-trad|
|[skill-02-XFJiGllNZp8Xebda.htm](feats/skill-02-XFJiGllNZp8Xebda.htm)|Intimidating Prowess|auto-trad|
|[skill-02-XmF4q4rzKWg55vG4.htm](feats/skill-02-XmF4q4rzKWg55vG4.htm)|Assured Identification|auto-trad|
|[skill-02-XvX1EyxWbbBF32NV.htm](feats/skill-02-XvX1EyxWbbBF32NV.htm)|Unmistakable Lore|auto-trad|
|[skill-02-xWY5omyIcILNR7y1.htm](feats/skill-02-xWY5omyIcILNR7y1.htm)|Magical Crafting|auto-trad|
|[skill-02-yTLGclKtWVFZLKIz.htm](feats/skill-02-yTLGclKtWVFZLKIz.htm)|Godless Healing|auto-trad|
|[skill-02-zdjPTg6vRwg8r2Lm.htm](feats/skill-02-zdjPTg6vRwg8r2Lm.htm)|Aura Sight|auto-trad|
|[skill-03-9lyFxaoZjF1ZjVN9.htm](feats/skill-03-9lyFxaoZjF1ZjVN9.htm)|Know the Beat|auto-trad|
|[skill-03-FbGPETHJR9VKxf9i.htm](feats/skill-03-FbGPETHJR9VKxf9i.htm)|Folk Dowsing|auto-trad|
|[skill-04-0Dy8RlFqrzCVOTl4.htm](feats/skill-04-0Dy8RlFqrzCVOTl4.htm)|Thorough Reports|auto-trad|
|[skill-04-0UzxiSrTfVs0jvBa.htm](feats/skill-04-0UzxiSrTfVs0jvBa.htm)|Familiar Foe|auto-trad|
|[skill-04-5qXw5Gl9TxbPMZLB.htm](feats/skill-04-5qXw5Gl9TxbPMZLB.htm)|Orthographic Mastery|auto-trad|
|[skill-04-68Kc4UyhnP4l8mxq.htm](feats/skill-04-68Kc4UyhnP4l8mxq.htm)|Dead Reckoning|auto-trad|
|[skill-04-7WBIXGqZbAKbqEU6.htm](feats/skill-04-7WBIXGqZbAKbqEU6.htm)|Named Artillery|auto-trad|
|[skill-04-9SdFlVQW4vM8ggh8.htm](feats/skill-04-9SdFlVQW4vM8ggh8.htm)|Fane's Escape|auto-trad|
|[skill-04-B4nuabUvA1rk7Hej.htm](feats/skill-04-B4nuabUvA1rk7Hej.htm)|Multilingual Cipher|auto-trad|
|[skill-04-cQptGH6RUYZmS41Q.htm](feats/skill-04-cQptGH6RUYZmS41Q.htm)|Hideous Ululation|auto-trad|
|[skill-04-f0s3WwaJN5f2UTYY.htm](feats/skill-04-f0s3WwaJN5f2UTYY.htm)|Reverse Engineering|auto-trad|
|[skill-04-G9l2g7sDpPVbZJza.htm](feats/skill-04-G9l2g7sDpPVbZJza.htm)|Quick Mount|auto-trad|
|[skill-04-HRVCODLOrhjRDtGb.htm](feats/skill-04-HRVCODLOrhjRDtGb.htm)|Sure Foot|auto-trad|
|[skill-04-Jivt1iaqKfT6EcwV.htm](feats/skill-04-Jivt1iaqKfT6EcwV.htm)|That's Not Natural!|auto-trad|
|[skill-04-jzflcD1XnBp2bSZI.htm](feats/skill-04-jzflcD1XnBp2bSZI.htm)|Distracting Flattery|auto-trad|
|[skill-04-K3Au5071pfvNwGob.htm](feats/skill-04-K3Au5071pfvNwGob.htm)|Hidden Magic|auto-trad|
|[skill-04-kIXFNPFBStOKunq4.htm](feats/skill-04-kIXFNPFBStOKunq4.htm)|Kreighton's Cognitive Crossover|auto-trad|
|[skill-04-knrawm4bMbDL9XS3.htm](feats/skill-04-knrawm4bMbDL9XS3.htm)|Ambush Tactics|auto-trad|
|[skill-04-MWjbG8L5JWg888kJ.htm](feats/skill-04-MWjbG8L5JWg888kJ.htm)|Efficient Explorer|auto-trad|
|[skill-04-mwZzcwYVcTvxbXDl.htm](feats/skill-04-mwZzcwYVcTvxbXDl.htm)|Fancy Moves|auto-trad|
|[skill-04-NADqgn78Rvl7TUG8.htm](feats/skill-04-NADqgn78Rvl7TUG8.htm)|Sociable Vow|auto-trad|
|[skill-04-p17VlHs0I4Yc4m34.htm](feats/skill-04-p17VlHs0I4Yc4m34.htm)|Engine Bay|auto-trad|
|[skill-04-R7BO8br4BjCmpjit.htm](feats/skill-04-R7BO8br4BjCmpjit.htm)|Settlement Scholastics|auto-trad|
|[skill-04-rAbfuZ1mc3lUYH41.htm](feats/skill-04-rAbfuZ1mc3lUYH41.htm)|Minion Guise|auto-trad|
|[skill-04-rfnEcjxIFqwlJwJT.htm](feats/skill-04-rfnEcjxIFqwlJwJT.htm)|Treat Condition|auto-trad|
|[skill-04-SmePERlF5BZl6cTo.htm](feats/skill-04-SmePERlF5BZl6cTo.htm)|Blessed Medicine|auto-trad|
|[skill-04-smYXWVI9WXmqpiCs.htm](feats/skill-04-smYXWVI9WXmqpiCs.htm)|Eclipsed Vitality|auto-trad|
|[skill-04-SXXyAwe9FGBwhJIW.htm](feats/skill-04-SXXyAwe9FGBwhJIW.htm)|Power Slide|auto-trad|
|[skill-04-SZ8J3NbtiizFaNzz.htm](feats/skill-04-SZ8J3NbtiizFaNzz.htm)|Change of Face|auto-trad|
|[skill-04-uxwHHjWs3ehqtG4b.htm](feats/skill-04-uxwHHjWs3ehqtG4b.htm)|Steel Skin|auto-trad|
|[skill-04-wpqKltAoJjRQgWow.htm](feats/skill-04-wpqKltAoJjRQgWow.htm)|Rope Runner|auto-trad|
|[skill-04-Wsrw68pklQyaScMX.htm](feats/skill-04-Wsrw68pklQyaScMX.htm)|Phonetic Training|auto-trad|
|[skill-04-WxL8NMW9JQ5igu0C.htm](feats/skill-04-WxL8NMW9JQ5igu0C.htm)|Diabolic Certitude|auto-trad|
|[skill-04-ZC9C6rxPJKrw6Ktx.htm](feats/skill-04-ZC9C6rxPJKrw6Ktx.htm)|In Plain Sight|auto-trad|
|[skill-04-ZiSmhTsnQMLqsmyw.htm](feats/skill-04-ZiSmhTsnQMLqsmyw.htm)|Final Rest|auto-trad|
|[skill-06-1WfvvjjObPKeZyid.htm](feats/skill-06-1WfvvjjObPKeZyid.htm)|Holistic Care|auto-trad|
|[skill-06-AnTBWhLiIA1c7jkg.htm](feats/skill-06-AnTBWhLiIA1c7jkg.htm)|Megafauna Veterinarian|auto-trad|
|[skill-06-dAckQkpg1qyTz8od.htm](feats/skill-06-dAckQkpg1qyTz8od.htm)|Resourceful Ritualist|auto-trad|
|[skill-06-EZ24QwnFteLCrgLg.htm](feats/skill-06-EZ24QwnFteLCrgLg.htm)|Emerald Boughs Accustomation|auto-trad|
|[skill-06-hAsaNx1dd3xvvAsE.htm](feats/skill-06-hAsaNx1dd3xvvAsE.htm)|Say That Again!|auto-trad|
|[skill-06-my4uFR8cnDC4mJE2.htm](feats/skill-06-my4uFR8cnDC4mJE2.htm)|Uzunjati Storytelling|auto-trad|
|[skill-06-TZASOwBqVveGjw77.htm](feats/skill-06-TZASOwBqVveGjw77.htm)|Analyze Idiolect|auto-trad|
|[skill-06-uwJQUFLymAWtJu1a.htm](feats/skill-06-uwJQUFLymAWtJu1a.htm)|Forced Entry|auto-trad|
|[skill-06-xLD9EmkEmT8hgwv7.htm](feats/skill-06-xLD9EmkEmT8hgwv7.htm)|Craft Facsimile|auto-trad|
|[skill-07-17FAYfreumeKbSGr.htm](feats/skill-07-17FAYfreumeKbSGr.htm)|Advanced First Aid|auto-trad|
|[skill-07-2rSyTfPgAmNAo01r.htm](feats/skill-07-2rSyTfPgAmNAo01r.htm)|Quick Recognition|auto-trad|
|[skill-07-63sSYk5yqiAyZGb9.htm](feats/skill-07-63sSYk5yqiAyZGb9.htm)|Graceful Leaper|auto-trad|
|[skill-07-6vwLzzrFfeiR9pm0.htm](feats/skill-07-6vwLzzrFfeiR9pm0.htm)|Entourage|auto-trad|
|[skill-07-7GmXKrkzmInkFyEr.htm](feats/skill-07-7GmXKrkzmInkFyEr.htm)|Quick Unlock|auto-trad|
|[skill-07-9o2VSlMQVPB4LN09.htm](feats/skill-07-9o2VSlMQVPB4LN09.htm)|Bizarre Magic|auto-trad|
|[skill-07-bcEI0iIFhZXqjoaR.htm](feats/skill-07-bcEI0iIFhZXqjoaR.htm)|Quick Setup|auto-trad|
|[skill-07-bFoh3267kNLk68cU.htm](feats/skill-07-bFoh3267kNLk68cU.htm)|Quick Swim|auto-trad|
|[skill-07-C5CweUNPP7HlRfBM.htm](feats/skill-07-C5CweUNPP7HlRfBM.htm)|Muscle Mimicry|auto-trad|
|[skill-07-Crd3qMecF9FYHjuH.htm](feats/skill-07-Crd3qMecF9FYHjuH.htm)|Read Shibboleths|auto-trad|
|[skill-07-dc8X2Mbtwq6kGp7F.htm](feats/skill-07-dc8X2Mbtwq6kGp7F.htm)|Terrified Retreat|auto-trad|
|[skill-07-dz1rHYk3n9pUFfgm.htm](feats/skill-07-dz1rHYk3n9pUFfgm.htm)|Bless Toxin|auto-trad|
|[skill-07-e6s2nIvlTycuzlR9.htm](feats/skill-07-e6s2nIvlTycuzlR9.htm)|Fabricated Connections|auto-trad|
|[skill-07-ePObIpaJDgDb9CQj.htm](feats/skill-07-ePObIpaJDgDb9CQj.htm)|Battle Cry|auto-trad|
|[skill-07-f5JOSyW1tKMpz6hU.htm](feats/skill-07-f5JOSyW1tKMpz6hU.htm)|Influence Nature|auto-trad|
|[skill-07-fvYwsHM9O1twQa5N.htm](feats/skill-07-fvYwsHM9O1twQa5N.htm)|Consult The Spirits|auto-trad|
|[skill-07-g3zmkEVJJIjE32fY.htm](feats/skill-07-g3zmkEVJJIjE32fY.htm)|Scholastic Identification|auto-trad|
|[skill-07-gBSPbQRXdagZTUwY.htm](feats/skill-07-gBSPbQRXdagZTUwY.htm)|Kip Up|auto-trad|
|[skill-07-gEj8aJmCThMzAjKY.htm](feats/skill-07-gEj8aJmCThMzAjKY.htm)|Ashen Veil|auto-trad|
|[skill-07-gHBdjbEnIK8clK8u.htm](feats/skill-07-gHBdjbEnIK8clK8u.htm)|Sacred Defense|auto-trad|
|[skill-07-hkSuxXOc9qBleJbd.htm](feats/skill-07-hkSuxXOc9qBleJbd.htm)|Disturbing Knowledge|auto-trad|
|[skill-07-IJQJBnD5CjKvFYEx.htm](feats/skill-07-IJQJBnD5CjKvFYEx.htm)|Foil Senses|auto-trad|
|[skill-07-Imvu2RV2ggjJ2HZt.htm](feats/skill-07-Imvu2RV2ggjJ2HZt.htm)|Swift Sneak|auto-trad|
|[skill-07-JEzjyNbpsh05iymG.htm](feats/skill-07-JEzjyNbpsh05iymG.htm)|Prepare Papers|auto-trad|
|[skill-07-Jk9XEMfMXoPT0ua2.htm](feats/skill-07-Jk9XEMfMXoPT0ua2.htm)|Skeptic's Defense|auto-trad|
|[skill-07-KIK2Eza9TK47MEb2.htm](feats/skill-07-KIK2Eza9TK47MEb2.htm)|Shameless Request|auto-trad|
|[skill-07-KP5VugClDb7I8enS.htm](feats/skill-07-KP5VugClDb7I8enS.htm)|That Was a Close One, Huh?|auto-trad|
|[skill-07-MTO0spetPKyIa4sT.htm](feats/skill-07-MTO0spetPKyIa4sT.htm)|Water Sprint|auto-trad|
|[skill-07-nBlzWZnmYuFHrMyV.htm](feats/skill-07-nBlzWZnmYuFHrMyV.htm)|Battle Prayer|auto-trad|
|[skill-07-oXoQ9wwOmDe0hwbU.htm](feats/skill-07-oXoQ9wwOmDe0hwbU.htm)|Subjective Truth|auto-trad|
|[skill-07-P04Hw8E6WAWARKHP.htm](feats/skill-07-P04Hw8E6WAWARKHP.htm)|Quick Climb|auto-trad|
|[skill-07-P9dVBWB8nYZt4AFA.htm](feats/skill-07-P9dVBWB8nYZt4AFA.htm)|Sanctify Water|auto-trad|
|[skill-07-PiUe3tpv7UVtnfvS.htm](feats/skill-07-PiUe3tpv7UVtnfvS.htm)|Impeccable Crafting|auto-trad|
|[skill-07-pKnX7MGZayqZui0Z.htm](feats/skill-07-pKnX7MGZayqZui0Z.htm)|Aerobatics Mastery|auto-trad|
|[skill-07-qibU5SZPFHMNnpAP.htm](feats/skill-07-qibU5SZPFHMNnpAP.htm)|Doublespeak|auto-trad|
|[skill-07-RiuZT3H4QZIIEQXJ.htm](feats/skill-07-RiuZT3H4QZIIEQXJ.htm)|Slippery Secrets|auto-trad|
|[skill-07-TkCy7jZUjhD8IypE.htm](feats/skill-07-TkCy7jZUjhD8IypE.htm)|Expert Disassembler|auto-trad|
|[skill-07-TXXrkkFfNSWgHrn5.htm](feats/skill-07-TXXrkkFfNSWgHrn5.htm)|Chronocognizance|auto-trad|
|[skill-07-u6tLp3zTBweq7CxO.htm](feats/skill-07-u6tLp3zTBweq7CxO.htm)|Environmental Grace|auto-trad|
|[skill-07-UaCHd5SpYsZwf2hM.htm](feats/skill-07-UaCHd5SpYsZwf2hM.htm)|Push It|auto-trad|
|[skill-07-ucmJ2Z5VXUtCiE3q.htm](feats/skill-07-ucmJ2Z5VXUtCiE3q.htm)|Efficient Controls|auto-trad|
|[skill-07-UHyoXbp8O6idQ6ee.htm](feats/skill-07-UHyoXbp8O6idQ6ee.htm)|Bless Tonic|auto-trad|
|[skill-07-UkMG3wMvrw8X0I98.htm](feats/skill-07-UkMG3wMvrw8X0I98.htm)|Propeller Attachment|auto-trad|
|[skill-07-wlbfINUTHDPqbV7v.htm](feats/skill-07-wlbfINUTHDPqbV7v.htm)|Morphic Manipulation|auto-trad|
|[skill-07-wqhxZwB1TR8fvpHP.htm](feats/skill-07-wqhxZwB1TR8fvpHP.htm)|Party Crasher|auto-trad|
|[skill-07-WqTqHPDbargixuej.htm](feats/skill-07-WqTqHPDbargixuej.htm)|Biographical Eye|auto-trad|
|[skill-07-WQtt44keeBP8t25P.htm](feats/skill-07-WQtt44keeBP8t25P.htm)|Voice Cold as Death|auto-trad|
|[skill-07-X8iSUF1m0eezmrjs.htm](feats/skill-07-X8iSUF1m0eezmrjs.htm)|Wall Jump|auto-trad|
|[skill-07-x9xA8P2Vlz98He7C.htm](feats/skill-07-x9xA8P2Vlz98He7C.htm)|Rapid Affixture|auto-trad|
|[skill-07-xOMwuKCf02aFzyp3.htm](feats/skill-07-xOMwuKCf02aFzyp3.htm)|Paragon Battle Medicine|auto-trad|
|[skill-07-XR95taODq1sq82Du.htm](feats/skill-07-XR95taODq1sq82Du.htm)|Inventor|auto-trad|
|[skill-07-YBge8sTgeY5jncX2.htm](feats/skill-07-YBge8sTgeY5jncX2.htm)|Speech of the Mammoth Lords|auto-trad|
|[skill-07-YgbcLfAEdi4xxvX5.htm](feats/skill-07-YgbcLfAEdi4xxvX5.htm)|Evangelize|auto-trad|
|[skill-07-Yj4mpROEjdCjQzMd.htm](feats/skill-07-Yj4mpROEjdCjQzMd.htm)|Planar Survival|auto-trad|
|[skill-08-2Tla5D1vpGioh42x.htm](feats/skill-08-2Tla5D1vpGioh42x.htm)|Unravel Mysteries|auto-trad|
|[skill-08-5vRXZcGAbqKRoaqL.htm](feats/skill-08-5vRXZcGAbqKRoaqL.htm)|Recognize Threat|auto-trad|
|[skill-08-n3vpCWPjXAInRTyR.htm](feats/skill-08-n3vpCWPjXAInRTyR.htm)|Snap Out Of It! (Pathfinder Agent)|auto-trad|
|[skill-08-RcQv16RK80R6c4id.htm](feats/skill-08-RcQv16RK80R6c4id.htm)|Improvised Crafting|auto-trad|
|[skill-08-rhVL28qFl760qJQe.htm](feats/skill-08-rhVL28qFl760qJQe.htm)|Insistent Command|auto-trad|
|[skill-08-yoeMOIgH8Snw1JCQ.htm](feats/skill-08-yoeMOIgH8Snw1JCQ.htm)|Diverse Recognition|auto-trad|
|[skill-10-TNV1cs1VFqdj4D2M.htm](feats/skill-10-TNV1cs1VFqdj4D2M.htm)|Masterful Obfuscation|auto-trad|
|[skill-10-UCen3Tq6BJlNI7rx.htm](feats/skill-10-UCen3Tq6BJlNI7rx.htm)|Uzunjati Recollection|auto-trad|
|[skill-12-9AZjpeeS824VsYv8.htm](feats/skill-12-9AZjpeeS824VsYv8.htm)|Emergency Medical Assistance|auto-trad|
|[skill-12-m7AOg13xEJRHyoTt.htm](feats/skill-12-m7AOg13xEJRHyoTt.htm)|Rugged Survivalist|auto-trad|
|[skill-12-oFcn7SDOH6W2QhJl.htm](feats/skill-12-oFcn7SDOH6W2QhJl.htm)|Too Angry to Die|auto-trad|
|[skill-12-QoPooHpBjPh1sjRD.htm](feats/skill-12-QoPooHpBjPh1sjRD.htm)|Recollect Studies|auto-trad|
|[skill-15-467qQoiy6bjWU1G8.htm](feats/skill-15-467qQoiy6bjWU1G8.htm)|Legendary Performer|auto-trad|
|[skill-15-A0TNeMNvyY8QpmLz.htm](feats/skill-15-A0TNeMNvyY8QpmLz.htm)|Legendary Negotiation|auto-trad|
|[skill-15-dYMxP8SsHrwOze8v.htm](feats/skill-15-dYMxP8SsHrwOze8v.htm)|Divine Guidance|auto-trad|
|[skill-15-GZba7ped7ZxYHchf.htm](feats/skill-15-GZba7ped7ZxYHchf.htm)|Legendary Professional|auto-trad|
|[skill-15-hrq3NOpS6148aVY1.htm](feats/skill-15-hrq3NOpS6148aVY1.htm)|Unified Theory|auto-trad|
|[skill-15-IZbjUaZI5zHTd1Vp.htm](feats/skill-15-IZbjUaZI5zHTd1Vp.htm)|Legendary Thief|auto-trad|
|[skill-15-Kk4AMZtpQnLEgN0b.htm](feats/skill-15-Kk4AMZtpQnLEgN0b.htm)|Legendary Medic|auto-trad|
|[skill-15-mZttsiWl1ql5NvrH.htm](feats/skill-15-mZttsiWl1ql5NvrH.htm)|Scare to Death|auto-trad|
|[skill-15-n0S0tJiOJPQk0Rne.htm](feats/skill-15-n0S0tJiOJPQk0Rne.htm)|Legendary Linguist|auto-trad|
|[skill-15-n0urrOL8YlnVBVRQ.htm](feats/skill-15-n0urrOL8YlnVBVRQ.htm)|Legendary Sneak|auto-trad|
|[skill-15-Ta61ObC8Lk7BxTFO.htm](feats/skill-15-Ta61ObC8Lk7BxTFO.htm)|Legendary Survivalist|auto-trad|
|[skill-15-TVFfTP9fHRidwBlW.htm](feats/skill-15-TVFfTP9fHRidwBlW.htm)|Cloud Jump|auto-trad|
|[skill-15-v62QzTwHOT3t86cL.htm](feats/skill-15-v62QzTwHOT3t86cL.htm)|Craft Anything|auto-trad|
|[skill-15-Vk7BzAb3D9r226sI.htm](feats/skill-15-Vk7BzAb3D9r226sI.htm)|Legendary Guide|auto-trad|
|[skill-15-VRCBTEyrcBf7auGz.htm](feats/skill-15-VRCBTEyrcBf7auGz.htm)|Legendary Tattoo Artist|auto-trad|
|[skill-15-XZcd1wFHy111klu2.htm](feats/skill-15-XZcd1wFHy111klu2.htm)|Reveal Machinations|auto-trad|
|[skill-15-Zf4yiLDdxHPovEQI.htm](feats/skill-15-Zf4yiLDdxHPovEQI.htm)|Legendary Codebreaker|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[ancestry-01-5qJBjjitpHjkX5Wh.htm](feats/ancestry-01-5qJBjjitpHjkX5Wh.htm)|Embodied Legionary Subjectivity|La subjetividad legionaria encarnada|modificada|
|[ancestry-01-AmFv3ClkAVRowHLI.htm](feats/ancestry-01-AmFv3ClkAVRowHLI.htm)|Tengu Weapon Familiarity|Tengu Weapon Familiarity|modificada|
|[ancestry-01-nyNsIePvpovlDAws.htm](feats/ancestry-01-nyNsIePvpovlDAws.htm)|Alchemical Scholar|Erudito alquímico|modificada|
|[ancestry-01-PPUNMjRLQYnmwQvF.htm](feats/ancestry-01-PPUNMjRLQYnmwQvF.htm)|Kobold Breath|Aliento de Kobold|modificada|
|[ancestry-05-b8BZl7wbm83ObEtO.htm](feats/ancestry-05-b8BZl7wbm83ObEtO.htm)|Integrated Armament|Armamento integrado|modificada|
|[ancestry-05-B9IytVeJ1SMSJawB.htm](feats/ancestry-05-B9IytVeJ1SMSJawB.htm)|Defensive Needles|Agujas defensivas|modificada|
|[ancestry-05-JvTSfyCkG70bmY7f.htm](feats/ancestry-05-JvTSfyCkG70bmY7f.htm)|Bloody Blows|Golpes Sangrientos|modificada|
|[ancestry-05-ya9YlhUA4WspUlHB.htm](feats/ancestry-05-ya9YlhUA4WspUlHB.htm)|Clan Protector|Protector del Clan|modificada|
|[ancestry-09-8aRa9VHoDKl9B1Z1.htm](feats/ancestry-09-8aRa9VHoDKl9B1Z1.htm)|Brightness Seeker|Buscador de la iluminación|modificada|
|[ancestry-09-BaVO8UU5ZkL8OZZj.htm](feats/ancestry-09-BaVO8UU5ZkL8OZZj.htm)|Dragon's Breath|Aliento de dragón|modificada|
|[ancestry-09-ByqxxJkJiNFtjghh.htm](feats/ancestry-09-ByqxxJkJiNFtjghh.htm)|Mask Of Pain|Máscara de dolor|modificada|
|[ancestry-09-FrzskqwNWexKY5BA.htm](feats/ancestry-09-FrzskqwNWexKY5BA.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[ancestry-09-G6rCbMrHacYWNu1K.htm](feats/ancestry-09-G6rCbMrHacYWNu1K.htm)|Aggravating Scratch|Rascarse Agravante|modificada|
|[ancestry-09-KsFrWhIPVLOqxV07.htm](feats/ancestry-09-KsFrWhIPVLOqxV07.htm)|Accursed Claws|Garras malditas|modificada|
|[ancestry-09-ux6kbsqRMsu9VHtn.htm](feats/ancestry-09-ux6kbsqRMsu9VHtn.htm)|Quill Spray|Rociada de púas|modificada|
|[ancestry-09-Xwk41o4fERfM07NR.htm](feats/ancestry-09-Xwk41o4fERfM07NR.htm)|Rokoan Arts|Rokoan Arts|modificada|
|[ancestry-09-YDHr12qVA3XRjkLP.htm](feats/ancestry-09-YDHr12qVA3XRjkLP.htm)|Bloodletting Fangs|Colmillos Sanguinarios|modificada|
|[ancestry-09-YfjuRHzqLFhLLgCc.htm](feats/ancestry-09-YfjuRHzqLFhLLgCc.htm)|Piercing Quills|Púas perforantes|modificada|
|[ancestry-13-oaqkhZ6c0Dbk78wi.htm](feats/ancestry-13-oaqkhZ6c0Dbk78wi.htm)|Envenomed Edge|Filo envenenado|modificada|
|[ancestry-17-ABx8keV4c43gEmeN.htm](feats/ancestry-17-ABx8keV4c43gEmeN.htm)|Pierce The Light|Pierce The Light|modificada|
|[ancestry-17-E4MS2wnRQJfyldrT.htm](feats/ancestry-17-E4MS2wnRQJfyldrT.htm)|Bone Swarm|Bone Swarm|modificada|
|[ancestry-17-xXaKiJbKhClF5eJx.htm](feats/ancestry-17-xXaKiJbKhClF5eJx.htm)|Final Form|Forma final|modificada|
|[archetype-02-eBdajOzs8kiJDic2.htm](feats/archetype-02-eBdajOzs8kiJDic2.htm)|Blessed One Dedication|Dedicación Bendecida|modificada|
|[archetype-02-Ziky4XVV7syXVbUg.htm](feats/archetype-02-Ziky4XVV7syXVbUg.htm)|Powder Punch Stance|Posición de Puñetazo de Polvo|modificada|
|[archetype-02-ZR5Buon23cDQ1ryB.htm](feats/archetype-02-ZR5Buon23cDQ1ryB.htm)|Zombie Dedication|Dedicación Zombie|modificada|
|[archetype-04-9KvsO72JJ3pfkG4U.htm](feats/archetype-04-9KvsO72JJ3pfkG4U.htm)|Hurling Charge|Hurling Charge|modificada|
|[archetype-04-xUaEpnfd1FMGNG1z.htm](feats/archetype-04-xUaEpnfd1FMGNG1z.htm)|Frightful Moan|Frightful Moan|modificada|
|[archetype-06-55wEtw47Zl11uqlr.htm](feats/archetype-06-55wEtw47Zl11uqlr.htm)|Numb|Numb|modificada|
|[archetype-06-O0POcPD2aELYTcIK.htm](feats/archetype-06-O0POcPD2aELYTcIK.htm)|Rough Terrain Stance|Posición de terreno abrupto.|modificada|
|[archetype-06-uBOToHKQJr5JBEsg.htm](feats/archetype-06-uBOToHKQJr5JBEsg.htm)|Mummy's Despair|Mummy's Despair|modificada|
|[archetype-08-8z9XkfZalQ5tUjfy.htm](feats/archetype-08-8z9XkfZalQ5tUjfy.htm)|Paralyzing Slash|Tajo paralizante|modificada|
|[archetype-08-F6VlPyZZpqV6d2CS.htm](feats/archetype-08-F6VlPyZZpqV6d2CS.htm)|Flensing Slice|Flensing Slice|modificada|
|[archetype-08-IZupJre7o5We2VrK.htm](feats/archetype-08-IZupJre7o5We2VrK.htm)|Vicious Fangs|Colmillos viciosos|modificada|
|[archetype-08-sIN6t7nCWdI5u1HK.htm](feats/archetype-08-sIN6t7nCWdI5u1HK.htm)|Spirit's Anguish|Angustia del espíritu|modificada|
|[archetype-08-y4Cws9vZj3Bf9uqH.htm](feats/archetype-08-y4Cws9vZj3Bf9uqH.htm)|Mysterious Breadth|Misteriosa amplitud|modificada|
|[archetype-10-3poGYUYCBTmbeCUs.htm](feats/archetype-10-3poGYUYCBTmbeCUs.htm)|Angel Of Death|Ángel de la Muerte|modificada|
|[archetype-10-tha0L3Z6608JrUwN.htm](feats/archetype-10-tha0L3Z6608JrUwN.htm)|Shadow Magic|Magia de las Sombras|modificada|
|[archetype-10-UEqntGzFrFA7ncUO.htm](feats/archetype-10-UEqntGzFrFA7ncUO.htm)|Aerial Piledriver|Aerial Pilerdriver|modificada|
|[archetype-10-wZZyasfIqwiJBQAQ.htm](feats/archetype-10-wZZyasfIqwiJBQAQ.htm)|Whirlwind Stance|Posición Torbellino|modificada|
|[archetype-12-MrBHGo9nmzcVii3k.htm](feats/archetype-12-MrBHGo9nmzcVii3k.htm)|Desiccating Inhalation|Inhalación Desecante|modificada|
|[archetype-12-U5hjS5qFrtGHWlVG.htm](feats/archetype-12-U5hjS5qFrtGHWlVG.htm)|Corpse Stench|Hedor de Cadáver|modificada|
|[archetype-14-2VZxwS5LTi9YxikG.htm](feats/archetype-14-2VZxwS5LTi9YxikG.htm)|Reset the Past|Restablecer el pasado|modificada|
|[archetype-14-AtpMrGXaMPJtDIDR.htm](feats/archetype-14-AtpMrGXaMPJtDIDR.htm)|Prayer Attack|Ataque de Oración|modificada|
|[archetype-18-5JG0kjxukERBeayd.htm](feats/archetype-18-5JG0kjxukERBeayd.htm)|Frightful Aura|Aura espantosa|modificada|
|[archetype-18-AJRWIcBGCIxXG0RS.htm](feats/archetype-18-AJRWIcBGCIxXG0RS.htm)|Master Magus Spellcasting|Mago Maestro Lanzador de Conjuros|modificada|
|[class-01-SheifYobjKqyK3Fv.htm](feats/class-01-SheifYobjKqyK3Fv.htm)|Tamper|Tamper|modificada|
|[class-01-VCjAlOvjNvFJOsG5.htm](feats/class-01-VCjAlOvjNvFJOsG5.htm)|Tiger Stance|Posición del tigre|modificada|
|[class-02-80CEAB05TP5ki9iW.htm](feats/class-02-80CEAB05TP5ki9iW.htm)|Fane's Fourberie|Fourberie de Fane|modificada|
|[class-02-obGGzuPcgO5Xiz6P.htm](feats/class-02-obGGzuPcgO5Xiz6P.htm)|Searing Restoration|Restablecimiento abrasador|modificada|
|[class-02-TtAvM02UvfNaXeXd.htm](feats/class-02-TtAvM02UvfNaXeXd.htm)|Blasting Beams|Blasting Beams|modificada|
|[class-04-hYu6XxARNJYdf8Qe.htm](feats/class-04-hYu6XxARNJYdf8Qe.htm)|Scalpel's Point|Punta de Bisturí|modificada|
|[class-04-lpG7ZXFDZygmkbH4.htm](feats/class-04-lpG7ZXFDZygmkbH4.htm)|Bloodletting Claws|Garras Sanguinarias|modificada|
|[class-04-meQJfsKVar9tm6c9.htm](feats/class-04-meQJfsKVar9tm6c9.htm)|Running Reload|Recarga a la carrera|modificada|
|[class-04-rPbh7sOhhL7i3j1z.htm](feats/class-04-rPbh7sOhhL7i3j1z.htm)|Greenwatch Initiate|Greenwatch Iniciar|modificada|
|[class-04-tmGsnUkPv8SIhBgn.htm](feats/class-04-tmGsnUkPv8SIhBgn.htm)|Flamboyant Athlete|Atleta extravagante|modificada|
|[class-04-xSYkYRKlSyLzPrH1.htm](feats/class-04-xSYkYRKlSyLzPrH1.htm)|Undying Conviction|Convicción imperecedera|modificada|
|[class-06-tRHjUCl0xqG97nok.htm](feats/class-06-tRHjUCl0xqG97nok.htm)|Ricochet Stance (Fighter)|Posición Ricochet (Luchador)|modificada|
|[class-06-xDTjr415ZZM8x2WW.htm](feats/class-06-xDTjr415ZZM8x2WW.htm)|High-Speed Regeneration|Regeneración de alta Velocidad.|modificada|
|[class-08-iy9XKih5jIAdv67c.htm](feats/class-08-iy9XKih5jIAdv67c.htm)|Debilitating Dichotomy|Debilitación Dicotomﾃｭa|modificada|
|[class-08-Jwq5o13uZF3ooln1.htm](feats/class-08-Jwq5o13uZF3ooln1.htm)|Jellyfish Stance|Posición Medusa|modificada|
|[class-08-oHra9QanDFpAZ4hh.htm](feats/class-08-oHra9QanDFpAZ4hh.htm)|Warden's Boon|Merced del guardián|modificada|
|[class-08-rByA8NDI6ZtNgBeT.htm](feats/class-08-rByA8NDI6ZtNgBeT.htm)|Mobile Shot Stance|Posición de disparo múltiple.|modificada|
|[class-08-RsNvCSrCN7czHC0G.htm](feats/class-08-RsNvCSrCN7czHC0G.htm)|Ricochet Stance (Rogue)|Posición Ricochet (Pícaro)|modificada|
|[class-08-YeyOqNFKaeuOTiJr.htm](feats/class-08-YeyOqNFKaeuOTiJr.htm)|Impassable Wall Stance|Posición de muro infranqueable|modificada|
|[class-10-h5ksUZlrVGBjq6p4.htm](feats/class-10-h5ksUZlrVGBjq6p4.htm)|Radiant Blade Spirit|Espíritu del filo radiante|modificada|
|[class-10-nWxNV9pFeBHV671W.htm](feats/class-10-nWxNV9pFeBHV671W.htm)|Pristine Weapon|Arma prístina|modificada|
|[class-10-OEGhbRgW6wRbccns.htm](feats/class-10-OEGhbRgW6wRbccns.htm)|Disruptive Stance|Posición perturbadora|modificada|
|[class-10-SY6bU7DOyfs22cJX.htm](feats/class-10-SY6bU7DOyfs22cJX.htm)|Temporary Potions|Pociones Temporales|modificada|
|[class-10-tDWc2LQNl0Op1Auq.htm](feats/class-10-tDWc2LQNl0Op1Auq.htm)|Buckler Dance|Danza de la hebilla|modificada|
|[class-10-xQuNswWB3eg1UM28.htm](feats/class-10-xQuNswWB3eg1UM28.htm)|Cobra Envenom|Cobra Envenom|modificada|
|[class-12-I00uuseTfPypVgLQ.htm](feats/class-12-I00uuseTfPypVgLQ.htm)|Primal Summons|Convocación primigenia|modificada|
|[class-12-QicYF43HqgpOBLzo.htm](feats/class-12-QicYF43HqgpOBLzo.htm)|Bloody Debilitation|Debilitación sangrienta|modificada|
|[class-12-ZghzLmYgeE19GqjP.htm](feats/class-12-ZghzLmYgeE19GqjP.htm)|Lunging Stance|Posición de acometida|modificada|
|[class-14-kTRGAST9J9ZxJZ4A.htm](feats/class-14-kTRGAST9J9ZxJZ4A.htm)|Twinned Defense (Swashbuckler)|Defensa gemela (Swashbuckler)|modificada|
|[class-14-orjVLLoziFTmf1mz.htm](feats/class-14-orjVLLoziFTmf1mz.htm)|Verdant Metamorphosis|Metamorfosis verde|modificada|
|[class-14-TyWFsX9DliAdAVs8.htm](feats/class-14-TyWFsX9DliAdAVs8.htm)|Sow Spell|Hechizo Siembra|modificada|
|[class-16-54JzsYCx3uoj7Wlz.htm](feats/class-16-54JzsYCx3uoj7Wlz.htm)|Monstrosity Shape|Forma de monstruosidad|modificada|
|[class-16-rFaUJtB46scuAidY.htm](feats/class-16-rFaUJtB46scuAidY.htm)|Graceful Poise|Elegancia grácil|modificada|
|[class-16-urYHB6VHhqvPMSy7.htm](feats/class-16-urYHB6VHhqvPMSy7.htm)|Impaling Briars|Los Zarzas empalador|modificada|
|[class-16-xjLbabfyQzBNT4y1.htm](feats/class-16-xjLbabfyQzBNT4y1.htm)|Twinned Defense (Fighter)|Defensa gemela (Luchador)|modificada|
|[class-18-2RaDe6Fi4t7S2IDF.htm](feats/class-18-2RaDe6Fi4t7S2IDF.htm)|Invoke Disaster|Invocar desastre|modificada|
|[class-18-62glnJI2o0KnHULB.htm](feats/class-18-62glnJI2o0KnHULB.htm)|Triangle Shot|Tiro Triangular|modificada|
|[class-18-AbjVJIBdNjbQbVnV.htm](feats/class-18-AbjVJIBdNjbQbVnV.htm)|Primal Aegis|Primal Aegis|modificada|
|[class-18-xAIUuSw5A85XEStY.htm](feats/class-18-xAIUuSw5A85XEStY.htm)|Perfect Form Control|Control de forma perfecto.|modificada|
|[class-20-1ul2dasQBdlaMEC5.htm](feats/class-20-1ul2dasQBdlaMEC5.htm)|Heart of the Kaiju|Corazón del Kaiju|modificada|
|[class-20-jYEMVfrXJLpXS6aC.htm](feats/class-20-jYEMVfrXJLpXS6aC.htm)|Radiant Blade Master|Maestro del filo radiante|modificada|
|[class-20-mGzPR7M9H733j2wN.htm](feats/class-20-mGzPR7M9H733j2wN.htm)|True Shapeshifter|Cambiaformas verdadero|modificada|
|[skill-02-z0HX5L7bOOrKi0dD.htm](feats/skill-02-z0HX5L7bOOrKi0dD.htm)|Tattoo Artist|Artista del tatuaje|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
