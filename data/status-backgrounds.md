# Estado de la traducción (backgrounds)

 * **auto-trad**: 338
 * **modificada**: 3


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0c9Np7Yq5JSxZ6Tb.htm](backgrounds/0c9Np7Yq5JSxZ6Tb.htm)|Alkenstar Tinker|auto-trad|
|[0FgYkkKv9u8zxWiO.htm](backgrounds/0FgYkkKv9u8zxWiO.htm)|Child of the Twin Village|auto-trad|
|[0otxwSLobUCf0l1I.htm](backgrounds/0otxwSLobUCf0l1I.htm)|Surge Investigator|auto-trad|
|[0wzTmGUb8yvzMrO0.htm](backgrounds/0wzTmGUb8yvzMrO0.htm)|Back-Alley Doctor|auto-trad|
|[0z0PSizHviOehdJF.htm](backgrounds/0z0PSizHviOehdJF.htm)|Haunting Vision|auto-trad|
|[0ZfBP7Tp2P3WN7Dp.htm](backgrounds/0ZfBP7Tp2P3WN7Dp.htm)|Amnesiac|auto-trad|
|[18Jzk3gAsXspFOYC.htm](backgrounds/18Jzk3gAsXspFOYC.htm)|Haunted Citizen|auto-trad|
|[1bjReaLqCD9pvBJ3.htm](backgrounds/1bjReaLqCD9pvBJ3.htm)|Academy Dropout|auto-trad|
|[1Fpf40RBd0EgwP7R.htm](backgrounds/1Fpf40RBd0EgwP7R.htm)|Sentinel Reflectance|auto-trad|
|[1M91pTjatEejBjEl.htm](backgrounds/1M91pTjatEejBjEl.htm)|Hired Killer|auto-trad|
|[1VdLr4Qm8fv1m4tM.htm](backgrounds/1VdLr4Qm8fv1m4tM.htm)|Godless Graycloak|auto-trad|
|[2bzqI0D4J3LUi8nq.htm](backgrounds/2bzqI0D4J3LUi8nq.htm)|Laborer|auto-trad|
|[2FUdcPsXwoWT1bms.htm](backgrounds/2FUdcPsXwoWT1bms.htm)|Tech Reliant|auto-trad|
|[2jeonnY3GZTdEMsm.htm](backgrounds/2jeonnY3GZTdEMsm.htm)|Harrow-Led|auto-trad|
|[2lk5NOcu1aUglUdK.htm](backgrounds/2lk5NOcu1aUglUdK.htm)|Fireworks Performer|auto-trad|
|[2Mm0BHMJZhqIqoQG.htm](backgrounds/2Mm0BHMJZhqIqoQG.htm)|Undertaker|auto-trad|
|[2qH61dLeaqgNOdOp.htm](backgrounds/2qH61dLeaqgNOdOp.htm)|Desert Tracker|auto-trad|
|[2TImvKwcFLDVhzUC.htm](backgrounds/2TImvKwcFLDVhzUC.htm)|Circuit Judge|auto-trad|
|[2wGmG7ntcnkvtw8l.htm](backgrounds/2wGmG7ntcnkvtw8l.htm)|Lost Loved One|auto-trad|
|[3frMfODIYFeqTl2k.htm](backgrounds/3frMfODIYFeqTl2k.htm)|Fortune Teller|auto-trad|
|[3gN09dOT2hMwGcK2.htm](backgrounds/3gN09dOT2hMwGcK2.htm)|Cook|auto-trad|
|[3kXTGUvodNMnJTxb.htm](backgrounds/3kXTGUvodNMnJTxb.htm)|Aiudara Seeker|auto-trad|
|[3M2FRDlunjFshzbq.htm](backgrounds/3M2FRDlunjFshzbq.htm)|Fogfen Tale-Teller|auto-trad|
|[3wLnNwWnZ2dHIbV4.htm](backgrounds/3wLnNwWnZ2dHIbV4.htm)|Diobel Pearl Diver|auto-trad|
|[3WPo7m6rJQh9L7MN.htm](backgrounds/3WPo7m6rJQh9L7MN.htm)|Emissary|auto-trad|
|[3YQ1Wcjzk8ftoyo7.htm](backgrounds/3YQ1Wcjzk8ftoyo7.htm)|Astrologer|auto-trad|
|[4a2sVO0o2mMTydN8.htm](backgrounds/4a2sVO0o2mMTydN8.htm)|Local Scion|auto-trad|
|[4aVFnYyRajog0mNl.htm](backgrounds/4aVFnYyRajog0mNl.htm)|Mantis Scion|auto-trad|
|[4fBIXtSVSRYn2ZGi.htm](backgrounds/4fBIXtSVSRYn2ZGi.htm)|Goblinblood Orphan|auto-trad|
|[4II2KZpizlBBcfCy.htm](backgrounds/4II2KZpizlBBcfCy.htm)|Borderlands Pioneer|auto-trad|
|[4KCa67gyxUn60zdf.htm](backgrounds/4KCa67gyxUn60zdf.htm)|Saloon Entertainer|auto-trad|
|[4lbfPXwZEf3eE0ip.htm](backgrounds/4lbfPXwZEf3eE0ip.htm)|Snubbed Out Stoolie|auto-trad|
|[4naQmCXBl0007c2W.htm](backgrounds/4naQmCXBl0007c2W.htm)|Touched by Dahak|auto-trad|
|[4Vc8uXVHonrasFnU.htm](backgrounds/4Vc8uXVHonrasFnU.htm)|Saboteur|auto-trad|
|[4XpWgGejwb2L6WpK.htm](backgrounds/4XpWgGejwb2L6WpK.htm)|Codebreaker|auto-trad|
|[4yN5miHoMvKwZIsa.htm](backgrounds/4yN5miHoMvKwZIsa.htm)|Press-Ganged (LOWG)|auto-trad|
|[5Bfzt3sZyiaKrBO0.htm](backgrounds/5Bfzt3sZyiaKrBO0.htm)|Sign Bound|auto-trad|
|[5dezuNcZJ6wfecnG.htm](backgrounds/5dezuNcZJ6wfecnG.htm)|Gunsmith|auto-trad|
|[5lcDlcXKD8eji6n3.htm](backgrounds/5lcDlcXKD8eji6n3.htm)|Astrological Augur|auto-trad|
|[5Oh9SdD4rhwlpHzg.htm](backgrounds/5Oh9SdD4rhwlpHzg.htm)|Tide Watcher|auto-trad|
|[5qUQOpdlNsJjpFVX.htm](backgrounds/5qUQOpdlNsJjpFVX.htm)|Ruin Delver|auto-trad|
|[5RGLAPi5sLykRcmm.htm](backgrounds/5RGLAPi5sLykRcmm.htm)|Animal Whisperer|auto-trad|
|[5RhHdnzWWKfh5faz.htm](backgrounds/5RhHdnzWWKfh5faz.htm)|Guest of Sedeq Lodge|auto-trad|
|[5Z3cLEpsx9nHVwhM.htm](backgrounds/5Z3cLEpsx9nHVwhM.htm)|Hunter|auto-trad|
|[6abqATPjYoF946LD.htm](backgrounds/6abqATPjYoF946LD.htm)|Bandit|auto-trad|
|[6c0rsuiiAaVqGTu7.htm](backgrounds/6c0rsuiiAaVqGTu7.htm)|Rivethun Adherent|auto-trad|
|[6irgRkKZ8tRZzLvs.htm](backgrounds/6irgRkKZ8tRZzLvs.htm)|Artisan|auto-trad|
|[6K6jJkjZ2MJYqQ6h.htm](backgrounds/6K6jJkjZ2MJYqQ6h.htm)|Bellflower Agent|auto-trad|
|[6UmhTxOQeqFnppxx.htm](backgrounds/6UmhTxOQeqFnppxx.htm)|Guard|auto-trad|
|[6vsoyCZKqxG0lVe8.htm](backgrounds/6vsoyCZKqxG0lVe8.htm)|Inlander|auto-trad|
|[76j9ds5URXv1dqnm.htm](backgrounds/76j9ds5URXv1dqnm.htm)|Plant Whisperer|auto-trad|
|[76RK9WizWYdyhMy5.htm](backgrounds/76RK9WizWYdyhMy5.htm)|Mammoth Speaker|auto-trad|
|[7AfixHrjbXgDPPkp.htm](backgrounds/7AfixHrjbXgDPPkp.htm)|Translator|auto-trad|
|[7fCZTzmv5I2dI4sr.htm](backgrounds/7fCZTzmv5I2dI4sr.htm)|Discarded Duplicate|auto-trad|
|[7IrOApgShgnmp1A5.htm](backgrounds/7IrOApgShgnmp1A5.htm)|Rigger|auto-trad|
|[7K6ZSWOoihZKSdyd.htm](backgrounds/7K6ZSWOoihZKSdyd.htm)|Ruby Phoenix Enthusiast|auto-trad|
|[7OAX5QMd15svZJzX.htm](backgrounds/7OAX5QMd15svZJzX.htm)|Relentless Dedication|auto-trad|
|[7QkQQsHjv2iNFIsF.htm](backgrounds/7QkQQsHjv2iNFIsF.htm)|Grave Robber|auto-trad|
|[7z33GaNsmSxel1xJ.htm](backgrounds/7z33GaNsmSxel1xJ.htm)|Doomcaller|auto-trad|
|[84uVpQFCqn0Atfpo.htm](backgrounds/84uVpQFCqn0Atfpo.htm)|Legendary Parents|auto-trad|
|[86TbxxwfpWjScwSQ.htm](backgrounds/86TbxxwfpWjScwSQ.htm)|Undercover Lotus Guard|auto-trad|
|[88WyCqU5x1eJ0MK2.htm](backgrounds/88WyCqU5x1eJ0MK2.htm)|Gladiator|auto-trad|
|[89LEOv97ZwsjnhNx.htm](backgrounds/89LEOv97ZwsjnhNx.htm)|Gambler|auto-trad|
|[8CLTXqyjdzEMKnZC.htm](backgrounds/8CLTXqyjdzEMKnZC.htm)|Cannoneer|auto-trad|
|[8hsMIh3lVGfZwjG5.htm](backgrounds/8hsMIh3lVGfZwjG5.htm)|Pillar|auto-trad|
|[8q4PhvpmIxZD7rsV.htm](backgrounds/8q4PhvpmIxZD7rsV.htm)|Rostland Partisan|auto-trad|
|[8UEKgUkagUDixkL2.htm](backgrounds/8UEKgUkagUDixkL2.htm)|Issian Partisan|auto-trad|
|[8usMHYAmFdqmmkTS.htm](backgrounds/8usMHYAmFdqmmkTS.htm)|Sponsored by a Village|auto-trad|
|[8UXahQfkP9GZ1TNW.htm](backgrounds/8UXahQfkP9GZ1TNW.htm)|Nomad|auto-trad|
|[90vPOBrSXY27k0bL.htm](backgrounds/90vPOBrSXY27k0bL.htm)|Tyrant Witness|auto-trad|
|[93icIDHD4IrqI2oV.htm](backgrounds/93icIDHD4IrqI2oV.htm)|Sodden Scavenger|auto-trad|
|[9l6q90sk8UM292CY.htm](backgrounds/9l6q90sk8UM292CY.htm)|Waste Walker|auto-trad|
|[9LnXsMRwYcxi7nDO.htm](backgrounds/9LnXsMRwYcxi7nDO.htm)|Bekyar Restorer|auto-trad|
|[9lVw1JGl5ser6626.htm](backgrounds/9lVw1JGl5ser6626.htm)|Criminal|auto-trad|
|[9pK15dQJVypSCjzO.htm](backgrounds/9pK15dQJVypSCjzO.htm)|Blessed|auto-trad|
|[9uqDWl8V2AgGMRXi.htm](backgrounds/9uqDWl8V2AgGMRXi.htm)|Propaganda Promoter|auto-trad|
|[9uTdwJaj27F18ZDX.htm](backgrounds/9uTdwJaj27F18ZDX.htm)|Razmiran Faithful|auto-trad|
|[a45LqkSRX07ljKdW.htm](backgrounds/a45LqkSRX07ljKdW.htm)|Merabite Prodigy|auto-trad|
|[a5dCSuAwGE2hqQjj.htm](backgrounds/a5dCSuAwGE2hqQjj.htm)|Freed Slave of Absalom|auto-trad|
|[a8BmnIIUR7AYog5B.htm](backgrounds/a8BmnIIUR7AYog5B.htm)|Barkeep|auto-trad|
|[a9Q4iIiAZryVWN27.htm](backgrounds/a9Q4iIiAZryVWN27.htm)|Bounty Hunter|auto-trad|
|[a9YjyQRNEOyespFf.htm](backgrounds/a9YjyQRNEOyespFf.htm)|Pyre Tender|auto-trad|
|[AfBCrHsw1xbRFejN.htm](backgrounds/AfBCrHsw1xbRFejN.htm)|Sleepless Suns Star|auto-trad|
|[aisuJF1A98bHfkLH.htm](backgrounds/aisuJF1A98bHfkLH.htm)|Whispering Way Scion|auto-trad|
|[AJ41zFEYwlOUghXp.htm](backgrounds/AJ41zFEYwlOUghXp.htm)|Osirionologist|auto-trad|
|[ajcpRVb5EG00l7Y4.htm](backgrounds/ajcpRVb5EG00l7Y4.htm)|Cursed Family|auto-trad|
|[Am8kwC9c2GQ5bJAW.htm](backgrounds/Am8kwC9c2GQ5bJAW.htm)|Undercover Contender|auto-trad|
|[ap25MWBuFGwwhYIG.htm](backgrounds/ap25MWBuFGwwhYIG.htm)|Aspiring Free-Captain|auto-trad|
|[apXTV7jJx6yJpj8D.htm](backgrounds/apXTV7jJx6yJpj8D.htm)|Prisoner|auto-trad|
|[AqhHif8mzYjlGMxJ.htm](backgrounds/AqhHif8mzYjlGMxJ.htm)|Sheriff|auto-trad|
|[aWAfj7bhTZM2oK81.htm](backgrounds/aWAfj7bhTZM2oK81.htm)|Hookclaw Digger|auto-trad|
|[b1588nhgRoYMef44.htm](backgrounds/b1588nhgRoYMef44.htm)|Wanderlust|auto-trad|
|[b3UC18ueX8m9Ov0W.htm](backgrounds/b3UC18ueX8m9Ov0W.htm)|Hermit|auto-trad|
|[B3Z5gbhMDRUATTrE.htm](backgrounds/B3Z5gbhMDRUATTrE.htm)|Gloriana's Fixer|auto-trad|
|[B6kNrX1y8XNbQYea.htm](backgrounds/B6kNrX1y8XNbQYea.htm)|Goldhand Arms Dealer|auto-trad|
|[B8kEwzPUMIjhofUm.htm](backgrounds/B8kEwzPUMIjhofUm.htm)|Sarkorian Survivor|auto-trad|
|[b9EPEY09dYOVzdue.htm](backgrounds/b9EPEY09dYOVzdue.htm)|Shadow War Survivor|auto-trad|
|[BBeJA7n0xpSsBCGq.htm](backgrounds/BBeJA7n0xpSsBCGq.htm)|Lesser Scion|auto-trad|
|[bCJ9p3P5uJDAtaUI.htm](backgrounds/bCJ9p3P5uJDAtaUI.htm)|Miner|auto-trad|
|[bDyb0k0rTfDTyhd8.htm](backgrounds/bDyb0k0rTfDTyhd8.htm)|Geb Crusader|auto-trad|
|[bh6O2Ad5mkYwRngM.htm](backgrounds/bh6O2Ad5mkYwRngM.htm)|Hermean Expatriate|auto-trad|
|[bNsYkUGMKmtc28MX.htm](backgrounds/bNsYkUGMKmtc28MX.htm)|Issian Patriot|auto-trad|
|[BRXXlw03K6ZeB2Li.htm](backgrounds/BRXXlw03K6ZeB2Li.htm)|Eidolon Contact|auto-trad|
|[bxWID85noazU72O3.htm](backgrounds/bxWID85noazU72O3.htm)|Ratted-Out Gun Runner|auto-trad|
|[byyceQSVT0H5GfNB.htm](backgrounds/byyceQSVT0H5GfNB.htm)|Anti-Magical|auto-trad|
|[BZhPPw9VD9U2ur6B.htm](backgrounds/BZhPPw9VD9U2ur6B.htm)|Witchlight Follower|auto-trad|
|[CAjQrHZZbALE7Qjy.htm](backgrounds/CAjQrHZZbALE7Qjy.htm)|Acolyte|auto-trad|
|[cCi1Lsld9Umz1uHs.htm](backgrounds/cCi1Lsld9Umz1uHs.htm)|Sense of Belonging|auto-trad|
|[ChAoT1V3Nc4sKXz4.htm](backgrounds/ChAoT1V3Nc4sKXz4.htm)|Corpse Stitcher|auto-trad|
|[CjNA8ippwoAvpxeI.htm](backgrounds/CjNA8ippwoAvpxeI.htm)|Ozem Experience|auto-trad|
|[CKU1sbFofcwZUJMx.htm](backgrounds/CKU1sbFofcwZUJMx.htm)|Ex-Con Token Guard|auto-trad|
|[cODrdTvko4Om26ik.htm](backgrounds/cODrdTvko4Om26ik.htm)|Mechanic|auto-trad|
|[D00NdG0WtUNMO546.htm](backgrounds/D00NdG0WtUNMO546.htm)|Mammoth Herder|auto-trad|
|[d5fKB0ZMJQkwDF5p.htm](backgrounds/d5fKB0ZMJQkwDF5p.htm)|Otherworldly Mission|auto-trad|
|[dAvFZ5QmbAHgXcNp.htm](backgrounds/dAvFZ5QmbAHgXcNp.htm)|Outrider|auto-trad|
|[DBxOUwM7qhGH8MrF.htm](backgrounds/DBxOUwM7qhGH8MrF.htm)|Courier|auto-trad|
|[DHrzVqB8f1ed3zTk.htm](backgrounds/DHrzVqB8f1ed3zTk.htm)|Clown|auto-trad|
|[DpmWrtkrmmgnVtAc.htm](backgrounds/DpmWrtkrmmgnVtAc.htm)|Construction Occultist|auto-trad|
|[dSYel8ABTevOc5YA.htm](backgrounds/dSYel8ABTevOc5YA.htm)|Clockfighter|auto-trad|
|[dVRDDjT4FOu6uLDR.htm](backgrounds/dVRDDjT4FOu6uLDR.htm)|Detective|auto-trad|
|[DVtZab19D1vD3a0n.htm](backgrounds/DVtZab19D1vD3a0n.htm)|Post Guard of All Trades|auto-trad|
|[E2ij2Cg8oMC0W0NS.htm](backgrounds/E2ij2Cg8oMC0W0NS.htm)|Nirmathi Guerrilla|auto-trad|
|[ECKic2p6yIyoGYod.htm](backgrounds/ECKic2p6yIyoGYod.htm)|Songsinger in Training|auto-trad|
|[eGPlKAoYBTUqjyrr.htm](backgrounds/eGPlKAoYBTUqjyrr.htm)|Tomb Born|auto-trad|
|[eHfsMNiVeqwnrpG3.htm](backgrounds/eHfsMNiVeqwnrpG3.htm)|Framed in Ferrous Quarter|auto-trad|
|[EJRWGsPWzAhixuvQ.htm](backgrounds/EJRWGsPWzAhixuvQ.htm)|Belkzen Slayer|auto-trad|
|[EYPorNgoYcp37akm.htm](backgrounds/EYPorNgoYcp37akm.htm)|Magical Misfit|auto-trad|
|[eYY3bX7xSH7aicqT.htm](backgrounds/eYY3bX7xSH7aicqT.htm)|Atteran Rancher|auto-trad|
|[ffcNsTUBsxFwbNgJ.htm](backgrounds/ffcNsTUBsxFwbNgJ.htm)|Lastwall Survivor|auto-trad|
|[FKHut73XDUGTnKkP.htm](backgrounds/FKHut73XDUGTnKkP.htm)|Field Medic|auto-trad|
|[FlXu29r5C4CborZv.htm](backgrounds/FlXu29r5C4CborZv.htm)|Friend of Greensteeples|auto-trad|
|[fML6YrXYDqQy0g7L.htm](backgrounds/fML6YrXYDqQy0g7L.htm)|Iolite Trainee Hobgoblin|auto-trad|
|[fo2nGLI1t1b7nAHK.htm](backgrounds/fo2nGLI1t1b7nAHK.htm)|Dreams of Vengeance|auto-trad|
|[FovAoohhvG7vIbX9.htm](backgrounds/FovAoohhvG7vIbX9.htm)|Gold Falls Regular|auto-trad|
|[fuTLDmihr9Z9e5wa.htm](backgrounds/fuTLDmihr9Z9e5wa.htm)|Cultist|auto-trad|
|[FVE2mg6EEyPVLz3M.htm](backgrounds/FVE2mg6EEyPVLz3M.htm)|Student of Magic|auto-trad|
|[fyKPDYFeIrgzADJB.htm](backgrounds/fyKPDYFeIrgzADJB.htm)|Alkenstar Outlaw|auto-trad|
|[FznrY4fZASquT6hY.htm](backgrounds/FznrY4fZASquT6hY.htm)|Sponsored by a Stranger|auto-trad|
|[G3GIfGLY1xSNqj17.htm](backgrounds/G3GIfGLY1xSNqj17.htm)|Almas Clerk|auto-trad|
|[g8xUX7DAu2ShZ90Q.htm](backgrounds/g8xUX7DAu2ShZ90Q.htm)|Empty Whispers|auto-trad|
|[gfklP8ub45R4wXKe.htm](backgrounds/gfklP8ub45R4wXKe.htm)|Aspiring River Monarch|auto-trad|
|[GNidqGnSABx1rQUQ.htm](backgrounds/GNidqGnSABx1rQUQ.htm)|Missionary|auto-trad|
|[GPI5kNu0xfom9kKa.htm](backgrounds/GPI5kNu0xfom9kKa.htm)|Dragon Scholar|auto-trad|
|[H3E69w8Xg0T7rAqD.htm](backgrounds/H3E69w8Xg0T7rAqD.htm)|Shoanti Name-Bearer|auto-trad|
|[h98cEl4DY75IL6KJ.htm](backgrounds/h98cEl4DY75IL6KJ.htm)|Teamster|auto-trad|
|[HdnmIaLadhRfZq8X.htm](backgrounds/HdnmIaLadhRfZq8X.htm)|Insurgent|auto-trad|
|[HDquvQywAZimmcFF.htm](backgrounds/HDquvQywAZimmcFF.htm)|Refugee (Fall of Plaguestone)|auto-trad|
|[HEd2Lxgvl080nRxx.htm](backgrounds/HEd2Lxgvl080nRxx.htm)|Shadow Lodge Defector|auto-trad|
|[HiOvPmXEXBjUy0VZ.htm](backgrounds/HiOvPmXEXBjUy0VZ.htm)|Brevic Outcast|auto-trad|
|[HKRcQO8Xj7xzBxAw.htm](backgrounds/HKRcQO8Xj7xzBxAw.htm)|Faction Opportunist|auto-trad|
|[HMIfr9N4rco1dzBO.htm](backgrounds/HMIfr9N4rco1dzBO.htm)|Reclaimed|auto-trad|
|[HNdGlDHkcczqNw2U.htm](backgrounds/HNdGlDHkcczqNw2U.htm)|Musical Prodigy|auto-trad|
|[hPx0xiv00GQqPWUH.htm](backgrounds/hPx0xiv00GQqPWUH.htm)|Kalistrade Follower|auto-trad|
|[HrWEyXTgIj16Z1RR.htm](backgrounds/HrWEyXTgIj16Z1RR.htm)|Reborn Soul|auto-trad|
|[HsAZDAPpqynArFAp.htm](backgrounds/HsAZDAPpqynArFAp.htm)|Magaambya Academic|auto-trad|
|[HSNUDvPgO1NESW7S.htm](backgrounds/HSNUDvPgO1NESW7S.htm)|Junk Collector|auto-trad|
|[HZ3oBBdEnsH3fWrm.htm](backgrounds/HZ3oBBdEnsH3fWrm.htm)|Shory Seeker|auto-trad|
|[I0vuIFypx8ADSJQC.htm](backgrounds/I0vuIFypx8ADSJQC.htm)|Black Market Smuggler|auto-trad|
|[i28Z9JXhEvoc7BX5.htm](backgrounds/i28Z9JXhEvoc7BX5.htm)|Refugee (APG)|auto-trad|
|[i4hN6OYv8qmi3GLW.htm](backgrounds/i4hN6OYv8qmi3GLW.htm)|Entertainer|auto-trad|
|[I5cRrqrPCHsQqFI9.htm](backgrounds/I5cRrqrPCHsQqFI9.htm)|Magical Merchant|auto-trad|
|[i5G6E5dkGWiq838C.htm](backgrounds/i5G6E5dkGWiq838C.htm)|Scholar of the Ancients|auto-trad|
|[i79pgNIAtJfkkOiw.htm](backgrounds/i79pgNIAtJfkkOiw.htm)|Tinker|auto-trad|
|[iaM6TjvijLCgiHeD.htm](backgrounds/iaM6TjvijLCgiHeD.htm)|Returning Descendant|auto-trad|
|[IFHYbU6Nu8BiTsRa.htm](backgrounds/IFHYbU6Nu8BiTsRa.htm)|Acrobat|auto-trad|
|[IfpYRxN8qyV4ym0o.htm](backgrounds/IfpYRxN8qyV4ym0o.htm)|Purveyor of the Bizarre|auto-trad|
|[IoBhge83aYpq0pPV.htm](backgrounds/IoBhge83aYpq0pPV.htm)|Archaeologist|auto-trad|
|[IObZEUz8wneEMgR3.htm](backgrounds/IObZEUz8wneEMgR3.htm)|Deckhand|auto-trad|
|[IOcjPmcemrQFFb2b.htm](backgrounds/IOcjPmcemrQFFb2b.htm)|Time Traveler|auto-trad|
|[irDibuV3Wi7T43sL.htm](backgrounds/irDibuV3Wi7T43sL.htm)|Child of the Puddles|auto-trad|
|[IuwNiQSSeRMQyDE7.htm](backgrounds/IuwNiQSSeRMQyDE7.htm)|Sword Scion|auto-trad|
|[iWWg16f3re1YChiD.htm](backgrounds/iWWg16f3re1YChiD.htm)|Oenopion-Ooze Tender|auto-trad|
|[ixluAGUDZciLEHtb.htm](backgrounds/ixluAGUDZciLEHtb.htm)|Tax Collector|auto-trad|
|[IXxdCzBS0xP20ckw.htm](backgrounds/IXxdCzBS0xP20ckw.htm)|Ward|auto-trad|
|[j2G71vQahw1DiWpO.htm](backgrounds/j2G71vQahw1DiWpO.htm)|Cursed|auto-trad|
|[j9v38iHA0sVy59SR.htm](backgrounds/j9v38iHA0sVy59SR.htm)|Pathfinder Hopeful|auto-trad|
|[JauSkDtMV6dhDZS8.htm](backgrounds/JauSkDtMV6dhDZS8.htm)|Political Scion|auto-trad|
|[JBRBb2818DZ4hjXw.htm](backgrounds/JBRBb2818DZ4hjXw.htm)|Total Power|auto-trad|
|[JfGVkZkaoz2lnmov.htm](backgrounds/JfGVkZkaoz2lnmov.htm)|Bright Lion|auto-trad|
|[jjkY6r7NNWhxDqja.htm](backgrounds/jjkY6r7NNWhxDqja.htm)|Once Bitten|auto-trad|
|[jpgO5zofecGeyXd5.htm](backgrounds/jpgO5zofecGeyXd5.htm)|Occult Librarian|auto-trad|
|[K35I1WCbzT5xnJ6N.htm](backgrounds/K35I1WCbzT5xnJ6N.htm)|Animal Wrangler|auto-trad|
|[KC5oI1bs6Wx8h91u.htm](backgrounds/KC5oI1bs6Wx8h91u.htm)|Deputy|auto-trad|
|[KEG5KFNrXKhTsj6J.htm](backgrounds/KEG5KFNrXKhTsj6J.htm)|Money Counter|auto-trad|
|[khGFmnQMBYmz2ONR.htm](backgrounds/khGFmnQMBYmz2ONR.htm)|Sarkorian Reclaimer|auto-trad|
|[KHMEcCCG132ILkuU.htm](backgrounds/KHMEcCCG132ILkuU.htm)|Starless One|auto-trad|
|[kLjqmylGOXeQ5o5Y.htm](backgrounds/kLjqmylGOXeQ5o5Y.htm)|Bonuwat Wavetouched|auto-trad|
|[kmhZZgR7KlBzRBX0.htm](backgrounds/kmhZZgR7KlBzRBX0.htm)|Driver|auto-trad|
|[KMv7ollLVaZ81XDV.htm](backgrounds/KMv7ollLVaZ81XDV.htm)|Merchant|auto-trad|
|[l5Kj0owzxfPcTvIb.htm](backgrounds/l5Kj0owzxfPcTvIb.htm)|Unsponsored|auto-trad|
|[lav3yRNPc7lQ7e9k.htm](backgrounds/lav3yRNPc7lQ7e9k.htm)|Senghor Sailor|auto-trad|
|[LbuWzGpCIP79UwVB.htm](backgrounds/LbuWzGpCIP79UwVB.htm)|Spell Seeker|auto-trad|
|[lCR8gyEZbwqh3RWi.htm](backgrounds/lCR8gyEZbwqh3RWi.htm)|Harbor Guard Moonlighter|auto-trad|
|[LHk50lz5Kk5ZYTeo.htm](backgrounds/LHk50lz5Kk5ZYTeo.htm)|Abadar's Avenger|auto-trad|
|[locc0cjOmOQHe3j7.htm](backgrounds/locc0cjOmOQHe3j7.htm)|Savior of Air|auto-trad|
|[LoeTd2SS6jfEgo1H.htm](backgrounds/LoeTd2SS6jfEgo1H.htm)|Genie-Blessed|auto-trad|
|[lqjmBmGHYRaSiglZ.htm](backgrounds/lqjmBmGHYRaSiglZ.htm)|Molthuni Mercenary|auto-trad|
|[LwAu4r3uocYfpKA8.htm](backgrounds/LwAu4r3uocYfpKA8.htm)|Trailblazer|auto-trad|
|[LWoPiYHAyLp8pvYx.htm](backgrounds/LWoPiYHAyLp8pvYx.htm)|Broken Tusk Recruiter|auto-trad|
|[lX5KDS2hU5LihZRs.htm](backgrounds/lX5KDS2hU5LihZRs.htm)|Martial Disciple|auto-trad|
|[m1vRLRHTpCrgk89G.htm](backgrounds/m1vRLRHTpCrgk89G.htm)|Thrune Loyalist|auto-trad|
|[M8EfhY6Knb2iQm6S.htm](backgrounds/M8EfhY6Knb2iQm6S.htm)|Press-Ganged (G&G)|auto-trad|
|[mcL4WLO2yxBGlvuG.htm](backgrounds/mcL4WLO2yxBGlvuG.htm)|Farmsteader|auto-trad|
|[MgiZ25JdT6O3fLbO.htm](backgrounds/MgiZ25JdT6O3fLbO.htm)|Anti-Tech Activist|auto-trad|
|[MiRWGXZnEdurMvVf.htm](backgrounds/MiRWGXZnEdurMvVf.htm)|Eldritch Anatomist|auto-trad|
|[moVRsnpjB5THCwxE.htm](backgrounds/moVRsnpjB5THCwxE.htm)|Street Urchin|auto-trad|
|[mrkgVjiEdlPjLUsN.htm](backgrounds/mrkgVjiEdlPjLUsN.htm)|Vidrian Reformer|auto-trad|
|[MrZvq1ebEgEN9cIv.htm](backgrounds/MrZvq1ebEgEN9cIv.htm)|Sandswept Survivor|auto-trad|
|[MslumKt6iwJ85GKZ.htm](backgrounds/MslumKt6iwJ85GKZ.htm)|Thuvian Unifier|auto-trad|
|[mWpNrTiREluJ6fLB.htm](backgrounds/mWpNrTiREluJ6fLB.htm)|Able Carter|auto-trad|
|[mxJRdRSMsyZfBf5c.htm](backgrounds/mxJRdRSMsyZfBf5c.htm)|Hermean Heritor|auto-trad|
|[n2JN5Kiu7tOCAHPr.htm](backgrounds/n2JN5Kiu7tOCAHPr.htm)|Market Runner|auto-trad|
|[n7tPT1SfVTcHjrj3.htm](backgrounds/n7tPT1SfVTcHjrj3.htm)|Tapestry Refugee|auto-trad|
|[nAe65lvsOJAIHGGT.htm](backgrounds/nAe65lvsOJAIHGGT.htm)|False Medium|auto-trad|
|[nhQKn1tVV6PKCurq.htm](backgrounds/nhQKn1tVV6PKCurq.htm)|Butcher|auto-trad|
|[ns9BOvCyjdapYhI0.htm](backgrounds/ns9BOvCyjdapYhI0.htm)|Root Worker|auto-trad|
|[nXnaV9JwUG1N2dsg.htm](backgrounds/nXnaV9JwUG1N2dsg.htm)|Attention Addict|auto-trad|
|[NXYce9NAHls2fcIf.htm](backgrounds/NXYce9NAHls2fcIf.htm)|Pathfinder Recruiter|auto-trad|
|[NywLl1XMQmzA6rP7.htm](backgrounds/NywLl1XMQmzA6rP7.htm)|Demon Slayer|auto-trad|
|[NZY0r4Csjul6eVPp.htm](backgrounds/NZY0r4Csjul6eVPp.htm)|Finadar Leshy|auto-trad|
|[o1lhSKOpKPamTITI.htm](backgrounds/o1lhSKOpKPamTITI.htm)|Bookkeeper|auto-trad|
|[o3ArTg5c8pLe7iGm.htm](backgrounds/o3ArTg5c8pLe7iGm.htm)|Song of the Deep|auto-trad|
|[O4xKRtHb21DCTvQ0.htm](backgrounds/O4xKRtHb21DCTvQ0.htm)|Wished Alive|auto-trad|
|[o7RbsQbv5iLRvd8j.htm](backgrounds/o7RbsQbv5iLRvd8j.htm)|Scout|auto-trad|
|[OD8RTS7cTeMJJFcR.htm](backgrounds/OD8RTS7cTeMJJFcR.htm)|Learned Guard Prodigy|auto-trad|
|[oEm937kNrP5sXxFD.htm](backgrounds/oEm937kNrP5sXxFD.htm)|Emancipated|auto-trad|
|[OhP7cqvNouFgHIdJ.htm](backgrounds/OhP7cqvNouFgHIdJ.htm)|Sewer Dragon|auto-trad|
|[oj4B6KLAOlUbY0wr.htm](backgrounds/oj4B6KLAOlUbY0wr.htm)|Eclipseborn|auto-trad|
|[p27PSjFtHAWikKaw.htm](backgrounds/p27PSjFtHAWikKaw.htm)|Early Explorer|auto-trad|
|[P65AGDPkhD2B4JtG.htm](backgrounds/P65AGDPkhD2B4JtG.htm)|Perfection Seeker|auto-trad|
|[p6Asr6f2cI2BWorr.htm](backgrounds/p6Asr6f2cI2BWorr.htm)|Sponsored by Teacher Ot|auto-trad|
|[pBX18FI1grWwkWjk.htm](backgrounds/pBX18FI1grWwkWjk.htm)|Kyonin Emissary|auto-trad|
|[pChM2ApTVEQ8SohP.htm](backgrounds/pChM2ApTVEQ8SohP.htm)|Reclaimer Investigator|auto-trad|
|[pGOlKz4Krnh7MyUM.htm](backgrounds/pGOlKz4Krnh7MyUM.htm)|Haunted|auto-trad|
|[PhqUBXLLkVXb6oUE.htm](backgrounds/PhqUBXLLkVXb6oUE.htm)|Taldan Schemer|auto-trad|
|[Phvnfdmz4bB7jrI3.htm](backgrounds/Phvnfdmz4bB7jrI3.htm)|Barrister|auto-trad|
|[ppBGlWl0UkBKkJgE.htm](backgrounds/ppBGlWl0UkBKkJgE.htm)|Feybound|auto-trad|
|[q1OCPBAgYgvwy1Of.htm](backgrounds/q1OCPBAgYgvwy1Of.htm)|Megafauna Hunter|auto-trad|
|[qB7g1OiZ8v8zgvkL.htm](backgrounds/qB7g1OiZ8v8zgvkL.htm)|Wonder Taster|auto-trad|
|[qbvzNG8hMjb8f66D.htm](backgrounds/qbvzNG8hMjb8f66D.htm)|Squire|auto-trad|
|[qJNmSgzq0ae29qCC.htm](backgrounds/qJNmSgzq0ae29qCC.htm)|Printer|auto-trad|
|[QnL7hqUi9HPenrbC.htm](backgrounds/QnL7hqUi9HPenrbC.htm)|Newcomer In Need|auto-trad|
|[qNpLqx6LhBo1jY4A.htm](backgrounds/qNpLqx6LhBo1jY4A.htm)|Blow-In|auto-trad|
|[qY4IUwVWIKPSFskP.htm](backgrounds/qY4IUwVWIKPSFskP.htm)|Aerialist|auto-trad|
|[qzKT7L1qldz14q8M.htm](backgrounds/qzKT7L1qldz14q8M.htm)|Legacy of the Hammer|auto-trad|
|[r0kYIbN06Cv8eNG3.htm](backgrounds/r0kYIbN06Cv8eNG3.htm)|Warrior|auto-trad|
|[R1v4gUu8oRMoOASM.htm](backgrounds/R1v4gUu8oRMoOASM.htm)|Wildwood Local|auto-trad|
|[r9fzNQEz33HyKTxm.htm](backgrounds/r9fzNQEz33HyKTxm.htm)|Pilgrim|auto-trad|
|[RC4l6WsxPn89a1f8.htm](backgrounds/RC4l6WsxPn89a1f8.htm)|Raised by Belief|auto-trad|
|[RPU1aNfVz3ZbymvV.htm](backgrounds/RPU1aNfVz3ZbymvV.htm)|Scion of Slayers|auto-trad|
|[RxhdWJUoRTBEeHYZ.htm](backgrounds/RxhdWJUoRTBEeHYZ.htm)|Night Watch|auto-trad|
|[rzyRtasSTfHS3e0y.htm](backgrounds/rzyRtasSTfHS3e0y.htm)|Returned|auto-trad|
|[SJ3nNOI5A8A4hK0Q.htm](backgrounds/SJ3nNOI5A8A4hK0Q.htm)|Winter's Child|auto-trad|
|[Sj91CUyEUeWoPh2R.htm](backgrounds/Sj91CUyEUeWoPh2R.htm)|Toymaker|auto-trad|
|[SOmJyAtPOokesZoe.htm](backgrounds/SOmJyAtPOokesZoe.htm)|Farmhand|auto-trad|
|[sR3S7Xn15drU6rOF.htm](backgrounds/sR3S7Xn15drU6rOF.htm)|Starwatcher|auto-trad|
|[StWWI7Wi0WRgRmxS.htm](backgrounds/StWWI7Wi0WRgRmxS.htm)|Nocturnal Navigator|auto-trad|
|[su8y75pGMVTUsNHK.htm](backgrounds/su8y75pGMVTUsNHK.htm)|Thassilonian Traveler|auto-trad|
|[t0t1ck8iKpaI4o5W.htm](backgrounds/t0t1ck8iKpaI4o5W.htm)|Charlatan|auto-trad|
|[T537wo3aem8GvnmR.htm](backgrounds/T537wo3aem8GvnmR.htm)|Hounded Thief|auto-trad|
|[T8SlgHjj0hVjKW2Q.htm](backgrounds/T8SlgHjj0hVjKW2Q.htm)|Sponsored by Family|auto-trad|
|[tA0nggrWfBEhvsKA.htm](backgrounds/tA0nggrWfBEhvsKA.htm)|Chelish Rebel|auto-trad|
|[TC7jpN5EA4UBIYep.htm](backgrounds/TC7jpN5EA4UBIYep.htm)|Truth Seeker|auto-trad|
|[tcsSxwkl4wCsfO3k.htm](backgrounds/tcsSxwkl4wCsfO3k.htm)|Trade Consortium Underling|auto-trad|
|[TPoP1mKpqUOpRQ5Y.htm](backgrounds/TPoP1mKpqUOpRQ5Y.htm)|Reputation Seeker|auto-trad|
|[tQ9t7uIssRCR2y3W.htm](backgrounds/tQ9t7uIssRCR2y3W.htm)|Final Blade Survivor|auto-trad|
|[TQBYPTRTVGLMv7cx.htm](backgrounds/TQBYPTRTVGLMv7cx.htm)|Local Brigand|auto-trad|
|[tqnrnXVTbdehohPL.htm](backgrounds/tqnrnXVTbdehohPL.htm)|Street Preacher|auto-trad|
|[Tujic4RHrQJmEYX4.htm](backgrounds/Tujic4RHrQJmEYX4.htm)|Mystic Tutor|auto-trad|
|[Ty8FRM0k262xuHfF.htm](backgrounds/Ty8FRM0k262xuHfF.htm)|Undersea Enthusiast|auto-trad|
|[U1gbRkmZqJ7SmpeF.htm](backgrounds/U1gbRkmZqJ7SmpeF.htm)|Banished Brighite|auto-trad|
|[uC6D2nmDTATxXrV6.htm](backgrounds/uC6D2nmDTATxXrV6.htm)|Royalty|auto-trad|
|[UdOUj7i8XGTI72Zc.htm](backgrounds/UdOUj7i8XGTI72Zc.htm)|Servant|auto-trad|
|[uF9nw15tK6b1bgre.htm](backgrounds/uF9nw15tK6b1bgre.htm)|Ruby Phoenix Fanatic|auto-trad|
|[UFHezf1LXUwcQIAQ.htm](backgrounds/UFHezf1LXUwcQIAQ.htm)|Wandering Preacher|auto-trad|
|[UgityMZaujmYUpil.htm](backgrounds/UgityMZaujmYUpil.htm)|Out-Of-Towner|auto-trad|
|[uJcFanGjVranEarv.htm](backgrounds/uJcFanGjVranEarv.htm)|Lumber Consortium Laborer|auto-trad|
|[UK40FVVVx6IKxipW.htm](backgrounds/UK40FVVVx6IKxipW.htm)|Inexplicably Expelled|auto-trad|
|[Uk4W7mKQbLtDDHwo.htm](backgrounds/Uk4W7mKQbLtDDHwo.htm)|Curandero|auto-trad|
|[UNbV4UwZEXxJy703.htm](backgrounds/UNbV4UwZEXxJy703.htm)|Sun Dancer|auto-trad|
|[uNhdcyhiog7YvXPT.htm](backgrounds/uNhdcyhiog7YvXPT.htm)|Varisian Wanderer|auto-trad|
|[uNvD4XK1kdvGjQVo.htm](backgrounds/uNvD4XK1kdvGjQVo.htm)|Child of Westcrown|auto-trad|
|[UU4MOe7Lozt9V8tg.htm](backgrounds/UU4MOe7Lozt9V8tg.htm)|Seer of the Dead|auto-trad|
|[UURvnfwXypRYYXBI.htm](backgrounds/UURvnfwXypRYYXBI.htm)|Storm Survivor|auto-trad|
|[uwZ0emSBFNMGA74j.htm](backgrounds/uwZ0emSBFNMGA74j.htm)|Tall Tale|auto-trad|
|[UyddtAwqDGjQ1SZK.htm](backgrounds/UyddtAwqDGjQ1SZK.htm)|Scholar of the Sky Key|auto-trad|
|[v0WPfxN6G8XFfFZT.htm](backgrounds/v0WPfxN6G8XFfFZT.htm)|Scholar|auto-trad|
|[V1RAIckpUJd2OzXi.htm](backgrounds/V1RAIckpUJd2OzXi.htm)|Mana Wastes Refugee|auto-trad|
|[V31KRG7aA7xS0m8L.htm](backgrounds/V31KRG7aA7xS0m8L.htm)|Shadow Haunted|auto-trad|
|[V3nYEAhyA54RtYky.htm](backgrounds/V3nYEAhyA54RtYky.htm)|Ulfen Raider|auto-trad|
|[V9VsomfjbPQNfaSL.htm](backgrounds/V9VsomfjbPQNfaSL.htm)|Disciple of the Gear|auto-trad|
|[vBPu7RwNXGDQ1ThL.htm](backgrounds/vBPu7RwNXGDQ1ThL.htm)|Dreamer of the Verdant Moon|auto-trad|
|[vE6nb2OSIXqprDXk.htm](backgrounds/vE6nb2OSIXqprDXk.htm)|Sally Guard Neophyte|auto-trad|
|[vEl3OBUsSmPh8b4N.htm](backgrounds/vEl3OBUsSmPh8b4N.htm)|Friendly Darkmoon Kobold|auto-trad|
|[vgin9ff2sUBMpuaI.htm](backgrounds/vgin9ff2sUBMpuaI.htm)|Former Aspis Agent|auto-trad|
|[vHeP960qjhfob4Je.htm](backgrounds/vHeP960qjhfob4Je.htm)|Scavenger|auto-trad|
|[vjhB0ZTV9OZgSuSz.htm](backgrounds/vjhB0ZTV9OZgSuSz.htm)|Thassilonian Delver|auto-trad|
|[vmdhw37F22FCMm50.htm](backgrounds/vmdhw37F22FCMm50.htm)|Food Trader|auto-trad|
|[vMyMUZpg8MYZT2AZ.htm](backgrounds/vMyMUZpg8MYZT2AZ.htm)|Wish for Riches|auto-trad|
|[vNWSzv36L1GBPPoc.htm](backgrounds/vNWSzv36L1GBPPoc.htm)|Hellknight Historian|auto-trad|
|[vTyG1bQkEENZHroY.htm](backgrounds/vTyG1bQkEENZHroY.htm)|Junker|auto-trad|
|[w2GSRC5uMcSUfPGJ.htm](backgrounds/w2GSRC5uMcSUfPGJ.htm)|Chosen One|auto-trad|
|[W6OIPZhjcV45gfoX.htm](backgrounds/W6OIPZhjcV45gfoX.htm)|Revenant|auto-trad|
|[wGB222d5UIBGYWsK.htm](backgrounds/wGB222d5UIBGYWsK.htm)|Mechanical Symbiosis|auto-trad|
|[WIWR8jURAdSAzxIh.htm](backgrounds/WIWR8jURAdSAzxIh.htm)|Magical Experiment|auto-trad|
|[wKiFedYlHCsM5wN4.htm](backgrounds/wKiFedYlHCsM5wN4.htm)|Rostlander|auto-trad|
|[WrKVeUQ6KeRCm9uH.htm](backgrounds/WrKVeUQ6KeRCm9uH.htm)|Teacher|auto-trad|
|[wU1qd8tZNcYn43y2.htm](backgrounds/wU1qd8tZNcYn43y2.htm)|Lost and Alone|auto-trad|
|[wudDO9OEsRjJsqhU.htm](backgrounds/wudDO9OEsRjJsqhU.htm)|Barber|auto-trad|
|[WueM94C9JXk10jPd.htm](backgrounds/WueM94C9JXk10jPd.htm)|Onyx Trader|auto-trad|
|[wz4fDeZtCvxC4vyO.htm](backgrounds/wz4fDeZtCvxC4vyO.htm)|Second Chance Champion|auto-trad|
|[wZikbWmCWYpjFxQF.htm](backgrounds/wZikbWmCWYpjFxQF.htm)|Wanted Witness|auto-trad|
|[x2y25cE98Eq4qxbu.htm](backgrounds/x2y25cE98Eq4qxbu.htm)|Ustalavic Academic|auto-trad|
|[x3fFxtLogoy5hZfe.htm](backgrounds/x3fFxtLogoy5hZfe.htm)|Medicinal Clocksmith|auto-trad|
|[xbyQ1RAF6x4ceXLf.htm](backgrounds/xbyQ1RAF6x4ceXLf.htm)|Energy Scarred|auto-trad|
|[XcacLiaGDgjzHS3T.htm](backgrounds/XcacLiaGDgjzHS3T.htm)|Spotter|auto-trad|
|[xCCvT9tprRQVFVDq.htm](backgrounds/xCCvT9tprRQVFVDq.htm)|Grizzled Muckrucker|auto-trad|
|[XHY0xrSSbX0cTJKK.htm](backgrounds/XHY0xrSSbX0cTJKK.htm)|Droskari Disciple|auto-trad|
|[xKEQvxDMHWRUkL6i.htm](backgrounds/xKEQvxDMHWRUkL6i.htm)|Clockwork Researcher|auto-trad|
|[xvz5F7iYBWEIjz0r.htm](backgrounds/xvz5F7iYBWEIjz0r.htm)|Bibliophile|auto-trad|
|[xyHndp512sLU4UOL.htm](backgrounds/xyHndp512sLU4UOL.htm)|Northland Forager|auto-trad|
|[y0WZS51fSi6dILHq.htm](backgrounds/y0WZS51fSi6dILHq.htm)|Secular Medic|auto-trad|
|[Y35nOXZRryiyHjlk.htm](backgrounds/Y35nOXZRryiyHjlk.htm)|Nexian Mystic|auto-trad|
|[Y50ssWBBKSRVBpSa.htm](backgrounds/Y50ssWBBKSRVBpSa.htm)|Mystic Seer|auto-trad|
|[y9OyNsxGfmjqdcP0.htm](backgrounds/y9OyNsxGfmjqdcP0.htm)|Artist|auto-trad|
|[Yai061jIDzojHzsn.htm](backgrounds/Yai061jIDzojHzsn.htm)|Necromancer's Apprentice|auto-trad|
|[yAtyaKbcHZWCJlf5.htm](backgrounds/yAtyaKbcHZWCJlf5.htm)|Witch Wary|auto-trad|
|[YJpEdmSOjlA2QZeu.htm](backgrounds/YJpEdmSOjlA2QZeu.htm)|Circus Born|auto-trad|
|[yK40c3082U30BUX5.htm](backgrounds/yK40c3082U30BUX5.htm)|Grand Council Bureaucrat|auto-trad|
|[ykYaQwIX7sxvbhwc.htm](backgrounds/ykYaQwIX7sxvbhwc.htm)|Willing Host|auto-trad|
|[ynObDI0VbZ4sqeMI.htm](backgrounds/ynObDI0VbZ4sqeMI.htm)|Ex-Mendevian Crusader|auto-trad|
|[Yu7Cl0Lk94LdPRi6.htm](backgrounds/Yu7Cl0Lk94LdPRi6.htm)|Noble|auto-trad|
|[Yuwr2pT3z3WYTX9T.htm](backgrounds/Yuwr2pT3z3WYTX9T.htm)|Alkenstar Sojourner|auto-trad|
|[YyzIzLxn2UCFubj4.htm](backgrounds/YyzIzLxn2UCFubj4.htm)|Menagerie Dung Sweeper|auto-trad|
|[yZUzLCeuaIkNk4up.htm](backgrounds/yZUzLCeuaIkNk4up.htm)|Saved by Clockwork|auto-trad|
|[z4cCsOT36MB7xldR.htm](backgrounds/z4cCsOT36MB7xldR.htm)|Barker|auto-trad|
|[ZdhPKEY9FfaOS8Wy.htm](backgrounds/ZdhPKEY9FfaOS8Wy.htm)|Herbalist|auto-trad|
|[Zmwyhsxe4i6rZN75.htm](backgrounds/Zmwyhsxe4i6rZN75.htm)|Sailor|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[i6y4DiKvqitdE0PW.htm](backgrounds/i6y4DiKvqitdE0PW.htm)|Quick|Quick|modificada|
|[Q2brdDtEoI3cmpuD.htm](backgrounds/Q2brdDtEoI3cmpuD.htm)|Feral Child|Feral Child|modificada|
|[U8347JbRAjhowP1q.htm](backgrounds/U8347JbRAjhowP1q.htm)|Brevic Noble|Brevic Noble|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
