# Estado de la traducción (age-of-ashes-bestiary)

 * **auto-trad**: 128
 * **modificada**: 1


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[080v247YtmFRxT3l.htm](age-of-ashes-bestiary/080v247YtmFRxT3l.htm)|Bshez "Sand Claws" Shak|auto-trad|
|[1lkay2gwgEquq0NF.htm](age-of-ashes-bestiary/1lkay2gwgEquq0NF.htm)|Scarlet Triad Sneak|auto-trad|
|[1tDEWL9mAIXvTYik.htm](age-of-ashes-bestiary/1tDEWL9mAIXvTYik.htm)|Warbal Bumblebrasher|auto-trad|
|[2FbG5nLE91b6BNiB.htm](age-of-ashes-bestiary/2FbG5nLE91b6BNiB.htm)|Duergar Slave Lord|auto-trad|
|[30FX1WFr0RkGeueX.htm](age-of-ashes-bestiary/30FX1WFr0RkGeueX.htm)|Inizra Arumelo|auto-trad|
|[4vEQ8zRXC51Qo4Mv.htm](age-of-ashes-bestiary/4vEQ8zRXC51Qo4Mv.htm)|Bloody Blade Mercenary|auto-trad|
|[53lk7ek73j65A09B.htm](age-of-ashes-bestiary/53lk7ek73j65A09B.htm)|Candlaron's Echo|auto-trad|
|[595MAHwB4JWtzLAA.htm](age-of-ashes-bestiary/595MAHwB4JWtzLAA.htm)|Soulbound Ruin|auto-trad|
|[5dSVk2y88SLsPPON.htm](age-of-ashes-bestiary/5dSVk2y88SLsPPON.htm)|Hermean Mutant|auto-trad|
|[5DwiLZJf6KxpIjDq.htm](age-of-ashes-bestiary/5DwiLZJf6KxpIjDq.htm)|Grippli Archer|auto-trad|
|[5jFR05QRixpOChRy.htm](age-of-ashes-bestiary/5jFR05QRixpOChRy.htm)|Deculi|auto-trad|
|[5r2S6FeE6D1Oh66T.htm](age-of-ashes-bestiary/5r2S6FeE6D1Oh66T.htm)|Tarrasque, The Armageddon Engine|auto-trad|
|[60aeSJvu09ZM3SPx.htm](age-of-ashes-bestiary/60aeSJvu09ZM3SPx.htm)|Racharak|auto-trad|
|[6AN7eagk2WrWc4im.htm](age-of-ashes-bestiary/6AN7eagk2WrWc4im.htm)|Mengkare|auto-trad|
|[7WNFNE3SVMRGbBDf.htm](age-of-ashes-bestiary/7WNFNE3SVMRGbBDf.htm)|Scarlet Triad Sniper|auto-trad|
|[91BSCAJA1Oto2ctf.htm](age-of-ashes-bestiary/91BSCAJA1Oto2ctf.htm)|Blood Boar|auto-trad|
|[9oNwQuwKGUuG9G9g.htm](age-of-ashes-bestiary/9oNwQuwKGUuG9G9g.htm)|Laslunn|auto-trad|
|[aaDiR0EIWRQx8wdy.htm](age-of-ashes-bestiary/aaDiR0EIWRQx8wdy.htm)|Calmont|auto-trad|
|[Afq4Osh3W1k9Bcsh.htm](age-of-ashes-bestiary/Afq4Osh3W1k9Bcsh.htm)|Promise Guard|auto-trad|
|[Aii18rhNPMLW4Pxh.htm](age-of-ashes-bestiary/Aii18rhNPMLW4Pxh.htm)|Skeletal Hellknight|auto-trad|
|[aiNE06bxKD6jfoLd.htm](age-of-ashes-bestiary/aiNE06bxKD6jfoLd.htm)|Uri Zandivar|auto-trad|
|[ajt6K2LyAxhJ8GuP.htm](age-of-ashes-bestiary/ajt6K2LyAxhJ8GuP.htm)|Black Powder Bomb|auto-trad|
|[AtiN3EsRpHn5qbuv.htm](age-of-ashes-bestiary/AtiN3EsRpHn5qbuv.htm)|Immortal Ichor|auto-trad|
|[axCgEgk7n1yXpgdp.htm](age-of-ashes-bestiary/axCgEgk7n1yXpgdp.htm)|Caustic Vapor|auto-trad|
|[BudeoAoJ4dOxjj0K.htm](age-of-ashes-bestiary/BudeoAoJ4dOxjj0K.htm)|Dahak's Shell|auto-trad|
|[C4PD4p4I9byZ8yp6.htm](age-of-ashes-bestiary/C4PD4p4I9byZ8yp6.htm)|King Harral|auto-trad|
|[CJVRPFTKuJroiD5C.htm](age-of-ashes-bestiary/CJVRPFTKuJroiD5C.htm)|Xevalorg|auto-trad|
|[dG5DBgrxlaimsWOS.htm](age-of-ashes-bestiary/dG5DBgrxlaimsWOS.htm)|Falrok|auto-trad|
|[dlW3UaXVpnzjd6xe.htm](age-of-ashes-bestiary/dlW3UaXVpnzjd6xe.htm)|Heuberk Thropp|auto-trad|
|[dTikLHqGfiSYemuZ.htm](age-of-ashes-bestiary/dTikLHqGfiSYemuZ.htm)|Veshumirix|auto-trad|
|[DxlauoGqSVDZWBOM.htm](age-of-ashes-bestiary/DxlauoGqSVDZWBOM.htm)|Acidic Needle Launcher|auto-trad|
|[egTpOr4Wc0L5e0iY.htm](age-of-ashes-bestiary/egTpOr4Wc0L5e0iY.htm)|Scarlet Triad Enforcer|auto-trad|
|[Ejxngh2tHFseZQHW.htm](age-of-ashes-bestiary/Ejxngh2tHFseZQHW.htm)|Vaklish|auto-trad|
|[El7eSPV9aRi98Ufy.htm](age-of-ashes-bestiary/El7eSPV9aRi98Ufy.htm)|Mercenary Sailor|auto-trad|
|[EUYisYdY3fcGL8zp.htm](age-of-ashes-bestiary/EUYisYdY3fcGL8zp.htm)|Lesser Dragonstorm|auto-trad|
|[Ev930dfPpwCR8Zju.htm](age-of-ashes-bestiary/Ev930dfPpwCR8Zju.htm)|Ilgreth|auto-trad|
|[F9BHPV16N4eEL0TP.htm](age-of-ashes-bestiary/F9BHPV16N4eEL0TP.htm)|Dragonstorm Fire Giant|auto-trad|
|[Fe1lYhCUY4UO4Plw.htm](age-of-ashes-bestiary/Fe1lYhCUY4UO4Plw.htm)|Talamira|auto-trad|
|[gF8Qy1k8gPBEUBAd.htm](age-of-ashes-bestiary/gF8Qy1k8gPBEUBAd.htm)|Scarlet Triad Mage|auto-trad|
|[H0pP1GqpMfX1WEiQ.htm](age-of-ashes-bestiary/H0pP1GqpMfX1WEiQ.htm)|Grauladon|auto-trad|
|[h7apR3QSxVZmy2nt.htm](age-of-ashes-bestiary/h7apR3QSxVZmy2nt.htm)|Dahak's Skull|auto-trad|
|[H7NO4Q7ctHnfGKeJ.htm](age-of-ashes-bestiary/H7NO4Q7ctHnfGKeJ.htm)|Weathered Wail|auto-trad|
|[HbgvZUYFJd2VfwmQ.htm](age-of-ashes-bestiary/HbgvZUYFJd2VfwmQ.htm)|Tree of Dreadful Dreams|auto-trad|
|[HI2xA7LCpNPkpV03.htm](age-of-ashes-bestiary/HI2xA7LCpNPkpV03.htm)|Wailing Crystals|auto-trad|
|[hPPdfkiRZ1LUpN2h.htm](age-of-ashes-bestiary/hPPdfkiRZ1LUpN2h.htm)|Damurdiel's Vengeance|auto-trad|
|[IdpkEJycZgrkvdSn.htm](age-of-ashes-bestiary/IdpkEJycZgrkvdSn.htm)|Phantom Bells|auto-trad|
|[Ig1joUmSHSNL6QVU.htm](age-of-ashes-bestiary/Ig1joUmSHSNL6QVU.htm)|Ingnovim's Assistant|auto-trad|
|[j4BCcTvIMtoUPRzw.htm](age-of-ashes-bestiary/j4BCcTvIMtoUPRzw.htm)|Aluum Enforcer|auto-trad|
|[ja9KowUPZjhwhJ1x.htm](age-of-ashes-bestiary/ja9KowUPZjhwhJ1x.htm)|Echoes of Betrayal|auto-trad|
|[JD6kdfZveObBe1mR.htm](age-of-ashes-bestiary/JD6kdfZveObBe1mR.htm)|Xotanispawn|auto-trad|
|[jO2fGZayk4R1AzYK.htm](age-of-ashes-bestiary/jO2fGZayk4R1AzYK.htm)|Bida|auto-trad|
|[jZc4PrsX3HCJnkXx.htm](age-of-ashes-bestiary/jZc4PrsX3HCJnkXx.htm)|Emaliza Zandivar|auto-trad|
|[kciXaXw31gHA3gZl.htm](age-of-ashes-bestiary/kciXaXw31gHA3gZl.htm)|Jahsi|auto-trad|
|[KEbmwWwmpAmIoBcm.htm](age-of-ashes-bestiary/KEbmwWwmpAmIoBcm.htm)|Mental Scream Trap|auto-trad|
|[kLX36WXp6rjTt71z.htm](age-of-ashes-bestiary/kLX36WXp6rjTt71z.htm)|Rinnarv Bontimar|auto-trad|
|[KROWh0CteDFFGsih.htm](age-of-ashes-bestiary/KROWh0CteDFFGsih.htm)|Hellcrown|auto-trad|
|[KzKQYgiUZ3hJo473.htm](age-of-ashes-bestiary/KzKQYgiUZ3hJo473.htm)|Kalavakus|auto-trad|
|[L4K4V09tQVXj7ZiI.htm](age-of-ashes-bestiary/L4K4V09tQVXj7ZiI.htm)|Dmiri Yoltosha|auto-trad|
|[L7IJO5z82nEN9IjM.htm](age-of-ashes-bestiary/L7IJO5z82nEN9IjM.htm)|Lazurite-Infused Stone Golem|auto-trad|
|[l7WhZMoYlQC8lELj.htm](age-of-ashes-bestiary/l7WhZMoYlQC8lELj.htm)|Dragonstorm|auto-trad|
|[lcYcBIFmfKFt8Hcs.htm](age-of-ashes-bestiary/lcYcBIFmfKFt8Hcs.htm)|Ingnovim Tluss|auto-trad|
|[lDgabn0WtDKbLtfc.htm](age-of-ashes-bestiary/lDgabn0WtDKbLtfc.htm)|Luminous Ward|auto-trad|
|[lRqptquQcM6ZcQ4O.htm](age-of-ashes-bestiary/lRqptquQcM6ZcQ4O.htm)|Ishti|auto-trad|
|[Lx5JiVOGWnzzjCrW.htm](age-of-ashes-bestiary/Lx5JiVOGWnzzjCrW.htm)|Scarlet Triad Bruiser|auto-trad|
|[MFTMMXiwvW1aqOzs.htm](age-of-ashes-bestiary/MFTMMXiwvW1aqOzs.htm)|Kralgurn|auto-trad|
|[MOyYEls8wNGHoe8F.htm](age-of-ashes-bestiary/MOyYEls8wNGHoe8F.htm)|Doorwarden|auto-trad|
|[MWQmOXxGcbaMsXDD.htm](age-of-ashes-bestiary/MWQmOXxGcbaMsXDD.htm)|Hezle|auto-trad|
|[mY494hn9sKVD9q8C.htm](age-of-ashes-bestiary/mY494hn9sKVD9q8C.htm)|Jaggaki|auto-trad|
|[mYJ6NNthl702Pz2s.htm](age-of-ashes-bestiary/mYJ6NNthl702Pz2s.htm)|Scarlet Triad Agent|auto-trad|
|[n6FQeNsDgKaDIF7b.htm](age-of-ashes-bestiary/n6FQeNsDgKaDIF7b.htm)|Spiritbound Aluum|auto-trad|
|[N8vOjTD2SqS2b9Sy.htm](age-of-ashes-bestiary/N8vOjTD2SqS2b9Sy.htm)|Lesser Manifestation Of Dahak|auto-trad|
|[NK49bh04355Lgz5r.htm](age-of-ashes-bestiary/NK49bh04355Lgz5r.htm)|Nketiah|auto-trad|
|[nQ2eBOpK71I8D2JC.htm](age-of-ashes-bestiary/nQ2eBOpK71I8D2JC.htm)|Scarlet Triad Poisoner|auto-trad|
|[NrMgYvnIHFxeFEhX.htm](age-of-ashes-bestiary/NrMgYvnIHFxeFEhX.htm)|Dalos|auto-trad|
|[NwLrwNqwedh95iry.htm](age-of-ashes-bestiary/NwLrwNqwedh95iry.htm)|Scarlet Triad Thug|auto-trad|
|[NwNu2yvMnbvPvpY8.htm](age-of-ashes-bestiary/NwNu2yvMnbvPvpY8.htm)|Ekujae Guardian|auto-trad|
|[oHUIqVPBpHAbN3qO.htm](age-of-ashes-bestiary/oHUIqVPBpHAbN3qO.htm)|Gloomglow Mushrooms|auto-trad|
|[OJH8y8LqmgbkN8ce.htm](age-of-ashes-bestiary/OJH8y8LqmgbkN8ce.htm)|Ghastly Bear|auto-trad|
|[oPXRJxxWrY2kz2qf.htm](age-of-ashes-bestiary/oPXRJxxWrY2kz2qf.htm)|Belmazog|auto-trad|
|[Oqj8XIWBb29NZ8QX.htm](age-of-ashes-bestiary/Oqj8XIWBb29NZ8QX.htm)|Tixitog|auto-trad|
|[Pmi71NUPAIE3YMws.htm](age-of-ashes-bestiary/Pmi71NUPAIE3YMws.htm)|Vision of Dahak|auto-trad|
|[qemmmfu7exswGMGZ.htm](age-of-ashes-bestiary/qemmmfu7exswGMGZ.htm)|Kelda Halrig|auto-trad|
|[QKkvnlqrhgLHuP1t.htm](age-of-ashes-bestiary/QKkvnlqrhgLHuP1t.htm)|Mialari Docur|auto-trad|
|[QoS5lEJqji2h490F.htm](age-of-ashes-bestiary/QoS5lEJqji2h490F.htm)|Lifeleech Crystal Patches|auto-trad|
|[qouYGPM8mE4KUCTe.htm](age-of-ashes-bestiary/qouYGPM8mE4KUCTe.htm)|Rusty Mae|auto-trad|
|[r02b4f1XNq7OihhD.htm](age-of-ashes-bestiary/r02b4f1XNq7OihhD.htm)|Renali|auto-trad|
|[rd8Scu1YBSyKFwnk.htm](age-of-ashes-bestiary/rd8Scu1YBSyKFwnk.htm)|Emperor Bird|auto-trad|
|[roRmUbaC9kJC7met.htm](age-of-ashes-bestiary/roRmUbaC9kJC7met.htm)|Endless Elven Aging|auto-trad|
|[scPhFbBqlmD2fQHa.htm](age-of-ashes-bestiary/scPhFbBqlmD2fQHa.htm)|Dragonscarred Dead|auto-trad|
|[sqXPqmuUYOgxspV3.htm](age-of-ashes-bestiary/sqXPqmuUYOgxspV3.htm)|Barzillai's Hounds|auto-trad|
|[sQZDwS08l6Agsryq.htm](age-of-ashes-bestiary/sQZDwS08l6Agsryq.htm)|Barushak Il-Varashma|auto-trad|
|[SUQJPKt8N7rxx7Q1.htm](age-of-ashes-bestiary/SUQJPKt8N7rxx7Q1.htm)|Spiked Doorframe|auto-trad|
|[SVh7cPwyGgmZORVz.htm](age-of-ashes-bestiary/SVh7cPwyGgmZORVz.htm)|Scarlet Triad Boss|auto-trad|
|[SVLUUPOXDINbKyFL.htm](age-of-ashes-bestiary/SVLUUPOXDINbKyFL.htm)|Malarunk|auto-trad|
|[sY47PB9b7nCJjgRq.htm](age-of-ashes-bestiary/sY47PB9b7nCJjgRq.htm)|Manifestation Of Dahak|auto-trad|
|[T3UVfAfcvAMe9rih.htm](age-of-ashes-bestiary/T3UVfAfcvAMe9rih.htm)|Charau-ka Dragon Priest|auto-trad|
|[teabm1YiRAKdUaEQ.htm](age-of-ashes-bestiary/teabm1YiRAKdUaEQ.htm)|Seismic Spears Trap|auto-trad|
|[tGyPrTGSpndXKU88.htm](age-of-ashes-bestiary/tGyPrTGSpndXKU88.htm)|Nolly Peltry|auto-trad|
|[tr3qlFJVHqloE9zI.htm](age-of-ashes-bestiary/tr3qlFJVHqloE9zI.htm)|Forge-Spurned|auto-trad|
|[U2QGjhcg5QFFkHwv.htm](age-of-ashes-bestiary/U2QGjhcg5QFFkHwv.htm)|Saggorak Poltergeist|auto-trad|
|[u9QIJIpkSgW5VRIG.htm](age-of-ashes-bestiary/u9QIJIpkSgW5VRIG.htm)|Town Hall Fire|auto-trad|
|[UD7EwTQG2Sbl4d8R.htm](age-of-ashes-bestiary/UD7EwTQG2Sbl4d8R.htm)|Voz Lirayne|auto-trad|
|[UQDxkvqXKg03ESTQ.htm](age-of-ashes-bestiary/UQDxkvqXKg03ESTQ.htm)|Gerhard Pendergrast|auto-trad|
|[VQiBBu6D2srK6Fmj.htm](age-of-ashes-bestiary/VQiBBu6D2srK6Fmj.htm)|Wrath of the Destroyer|auto-trad|
|[w1Pqv0YmG4MevpQc.htm](age-of-ashes-bestiary/w1Pqv0YmG4MevpQc.htm)|Corrupt Guard|auto-trad|
|[We1Nq7CrqDQHEZxY.htm](age-of-ashes-bestiary/We1Nq7CrqDQHEZxY.htm)|Quarry Sluiceway|auto-trad|
|[WouSA1UsIzvBTOix.htm](age-of-ashes-bestiary/WouSA1UsIzvBTOix.htm)|Carnivorous Crystal|auto-trad|
|[X2edjYEqAHsdQc6L.htm](age-of-ashes-bestiary/X2edjYEqAHsdQc6L.htm)|Grippli Greenspeaker|auto-trad|
|[X4oeJnv1KcpHFO6i.htm](age-of-ashes-bestiary/X4oeJnv1KcpHFO6i.htm)|Xotani, The Firebleeder|auto-trad|
|[X6TTBlHIfJZ43OqR.htm](age-of-ashes-bestiary/X6TTBlHIfJZ43OqR.htm)|Dragonshard Guardian|auto-trad|
|[X7zSx8LFh2ZDnOYy.htm](age-of-ashes-bestiary/X7zSx8LFh2ZDnOYy.htm)|Teyam Ishtori|auto-trad|
|[XHJC4G6bfPiVXGKE.htm](age-of-ashes-bestiary/XHJC4G6bfPiVXGKE.htm)|Ilssrah Embermead|auto-trad|
|[XNso2IMnhnfHcMCn.htm](age-of-ashes-bestiary/XNso2IMnhnfHcMCn.htm)|Zuferian|auto-trad|
|[xPV49ZBCwDdCq5eI.htm](age-of-ashes-bestiary/xPV49ZBCwDdCq5eI.htm)|Vazgorlu|auto-trad|
|[xrxjjjRMKzwsYbGm.htm](age-of-ashes-bestiary/xrxjjjRMKzwsYbGm.htm)|Accursed Forge-Spurned|auto-trad|
|[xXtGS9uBa9z43X7y.htm](age-of-ashes-bestiary/xXtGS9uBa9z43X7y.htm)|Graveshell|auto-trad|
|[YdLf7y2ZHudpjWx9.htm](age-of-ashes-bestiary/YdLf7y2ZHudpjWx9.htm)|Living Sap|auto-trad|
|[YIXAcvSyI1C94r9l.htm](age-of-ashes-bestiary/YIXAcvSyI1C94r9l.htm)|Zephyr Guard|auto-trad|
|[yLhO5vUrPQF42Lh8.htm](age-of-ashes-bestiary/yLhO5vUrPQF42Lh8.htm)|Charau-ka|auto-trad|
|[yrU0xi4eKBmcGudo.htm](age-of-ashes-bestiary/yrU0xi4eKBmcGudo.htm)|Animated Dragonstorm|auto-trad|
|[YV3wA2Kgjp74l7YJ.htm](age-of-ashes-bestiary/YV3wA2Kgjp74l7YJ.htm)|Alak Stagram|auto-trad|
|[z3zWW4jLADAei1uo.htm](age-of-ashes-bestiary/z3zWW4jLADAei1uo.htm)|Ash Web|auto-trad|
|[z4rlpEsE2KgzpOCc.htm](age-of-ashes-bestiary/z4rlpEsE2KgzpOCc.htm)|Remnant of Barzillai|auto-trad|
|[zdJHl3xMY7n2Lwlf.htm](age-of-ashes-bestiary/zdJHl3xMY7n2Lwlf.htm)|Trapped Lathe|auto-trad|
|[zNIjGSxkG8xyDLgR.htm](age-of-ashes-bestiary/zNIjGSxkG8xyDLgR.htm)|Dragon Pillar|auto-trad|
|[ZqL6MP4XxDys9dmW.htm](age-of-ashes-bestiary/ZqL6MP4XxDys9dmW.htm)|Aiudara Wraith|auto-trad|
|[ZRGRAqkDEZbRn8x3.htm](age-of-ashes-bestiary/ZRGRAqkDEZbRn8x3.htm)|Crucidaemon|auto-trad|
|[ZtSb3mHZ5sD2uqHd.htm](age-of-ashes-bestiary/ZtSb3mHZ5sD2uqHd.htm)|Mud Spider|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[CmbcgWZgOSDmo2Pm.htm](age-of-ashes-bestiary/CmbcgWZgOSDmo2Pm.htm)|Precentor|Precentor|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
