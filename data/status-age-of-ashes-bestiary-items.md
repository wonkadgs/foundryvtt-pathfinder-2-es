# Estado de la traducción (age-of-ashes-bestiary-items)

 * **auto-trad**: 972
 * **ninguna**: 1
 * **modificada**: 5
 * **vacía**: 2


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[8vIB5BOiDuY5uD2a.htm](age-of-ashes-bestiary-items/8vIB5BOiDuY5uD2a.htm)|Regeneration 25 (deactivated by Good or Silver)|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[055E78h70itsjy7c.htm](age-of-ashes-bestiary-items/055E78h70itsjy7c.htm)|Grab|auto-trad|
|[06QX0MyzRFRMZtye.htm](age-of-ashes-bestiary-items/06QX0MyzRFRMZtye.htm)|Scimitar|auto-trad|
|[09IvgWSDnJI3o0MQ.htm](age-of-ashes-bestiary-items/09IvgWSDnJI3o0MQ.htm)|Darkvision|auto-trad|
|[0AiUaTUE96AdfKo2.htm](age-of-ashes-bestiary-items/0AiUaTUE96AdfKo2.htm)|Hunt Prey|auto-trad|
|[0At2kB40YzOYoP4s.htm](age-of-ashes-bestiary-items/0At2kB40YzOYoP4s.htm)|Manly Left Hook|auto-trad|
|[0dYMOi8x5GT5a1Yq.htm](age-of-ashes-bestiary-items/0dYMOi8x5GT5a1Yq.htm)|1-2 Acid Rain|auto-trad|
|[0Eqa5Be8keCH6y7q.htm](age-of-ashes-bestiary-items/0Eqa5Be8keCH6y7q.htm)|Light Mace|auto-trad|
|[0ew9olScumzeKG3i.htm](age-of-ashes-bestiary-items/0ew9olScumzeKG3i.htm)|Dart|auto-trad|
|[0fMOfuFCNYpqOsRI.htm](age-of-ashes-bestiary-items/0fMOfuFCNYpqOsRI.htm)|Maul|auto-trad|
|[0J60fBJDJuBW0TbY.htm](age-of-ashes-bestiary-items/0J60fBJDJuBW0TbY.htm)|Dagger|auto-trad|
|[0JCPDdK7GNe3JLNR.htm](age-of-ashes-bestiary-items/0JCPDdK7GNe3JLNR.htm)|Tail|auto-trad|
|[0mC5JVblweUZ0WxU.htm](age-of-ashes-bestiary-items/0mC5JVblweUZ0WxU.htm)|Jaws|auto-trad|
|[0N1KAl4hWrBUvv2S.htm](age-of-ashes-bestiary-items/0N1KAl4hWrBUvv2S.htm)|Darkvision|auto-trad|
|[0n2OnJwSpxVhh6zy.htm](age-of-ashes-bestiary-items/0n2OnJwSpxVhh6zy.htm)|+3 Status to All Saves vs. Divine Magic|auto-trad|
|[0pbel7KeTVy39RkF.htm](age-of-ashes-bestiary-items/0pbel7KeTVy39RkF.htm)|At-Will Spells|auto-trad|
|[0rm1ESKWoiQPJPo1.htm](age-of-ashes-bestiary-items/0rm1ESKWoiQPJPo1.htm)|Shrieking Frenzy|auto-trad|
|[0RsLlXY365ytSohG.htm](age-of-ashes-bestiary-items/0RsLlXY365ytSohG.htm)|Countermeasures|auto-trad|
|[0rU9AWHWD11ECFwB.htm](age-of-ashes-bestiary-items/0rU9AWHWD11ECFwB.htm)|Boundless Reprisals|auto-trad|
|[0tHs3BRTN0UIM4j7.htm](age-of-ashes-bestiary-items/0tHs3BRTN0UIM4j7.htm)|Ash Web Spores|auto-trad|
|[0tyhJYUBZLs3j7c9.htm](age-of-ashes-bestiary-items/0tyhJYUBZLs3j7c9.htm)|Low-Light Vision|auto-trad|
|[0vtEhVnugRtcZgdh.htm](age-of-ashes-bestiary-items/0vtEhVnugRtcZgdh.htm)|Attack of Opportunity (Special)|auto-trad|
|[0YBsTxxadKi2UTcv.htm](age-of-ashes-bestiary-items/0YBsTxxadKi2UTcv.htm)|Scoundrel|auto-trad|
|[0z7Fxpm5UuCECxe5.htm](age-of-ashes-bestiary-items/0z7Fxpm5UuCECxe5.htm)|Efficient Capture|auto-trad|
|[117QFjXSLlaZ1zZl.htm](age-of-ashes-bestiary-items/117QFjXSLlaZ1zZl.htm)|Improved Knockdown|auto-trad|
|[15vIK60zfhRa5WGH.htm](age-of-ashes-bestiary-items/15vIK60zfhRa5WGH.htm)|Uncanny Bombs|auto-trad|
|[15Yz15Mslx43Yuro.htm](age-of-ashes-bestiary-items/15Yz15Mslx43Yuro.htm)|Body Slam|auto-trad|
|[197Q2S7Pdx8jKLm7.htm](age-of-ashes-bestiary-items/197Q2S7Pdx8jKLm7.htm)|Darkvision|auto-trad|
|[19cNQjbQEVxmiger.htm](age-of-ashes-bestiary-items/19cNQjbQEVxmiger.htm)|Telekinetic Whirlwind|auto-trad|
|[1AvPepGSMr92x8Jz.htm](age-of-ashes-bestiary-items/1AvPepGSMr92x8Jz.htm)|Fist|auto-trad|
|[1BZlxGhTFq59fpA6.htm](age-of-ashes-bestiary-items/1BZlxGhTFq59fpA6.htm)|Infused Items|auto-trad|
|[1h7ULVYpJvCMccHA.htm](age-of-ashes-bestiary-items/1h7ULVYpJvCMccHA.htm)|Jaws|auto-trad|
|[1mhGkpmF4fQNlduu.htm](age-of-ashes-bestiary-items/1mhGkpmF4fQNlduu.htm)|Rust|auto-trad|
|[1o1raLwrZRGxcwrA.htm](age-of-ashes-bestiary-items/1o1raLwrZRGxcwrA.htm)|Destructive Frenzy|auto-trad|
|[1pNOXTSqIPHQk8Lw.htm](age-of-ashes-bestiary-items/1pNOXTSqIPHQk8Lw.htm)|Scent (Imprecise) 120 feet|auto-trad|
|[1pOgJ4dFgPo6yfdm.htm](age-of-ashes-bestiary-items/1pOgJ4dFgPo6yfdm.htm)|Dagger|auto-trad|
|[1QF8m8mUgLiyrAeW.htm](age-of-ashes-bestiary-items/1QF8m8mUgLiyrAeW.htm)|Swift Capture|auto-trad|
|[1qFhgFYCKIDaF8mk.htm](age-of-ashes-bestiary-items/1qFhgFYCKIDaF8mk.htm)|Devour Soul|auto-trad|
|[1rRyEALEgAvhDMWc.htm](age-of-ashes-bestiary-items/1rRyEALEgAvhDMWc.htm)|Imitate Door|auto-trad|
|[1sYunHFicjStIbRN.htm](age-of-ashes-bestiary-items/1sYunHFicjStIbRN.htm)|Begin the Hunt|auto-trad|
|[1WAPJkfVFJzVJi9o.htm](age-of-ashes-bestiary-items/1WAPJkfVFJzVJi9o.htm)|Claw|auto-trad|
|[1wkVUb6UVwlesFiF.htm](age-of-ashes-bestiary-items/1wkVUb6UVwlesFiF.htm)|Darkvision|auto-trad|
|[1WQFCxrBbvgqJCnn.htm](age-of-ashes-bestiary-items/1WQFCxrBbvgqJCnn.htm)|Claw|auto-trad|
|[1wtSED5UzPWNuVfD.htm](age-of-ashes-bestiary-items/1wtSED5UzPWNuVfD.htm)|Negative Healing|auto-trad|
|[1Zo0SRU37QPsakyi.htm](age-of-ashes-bestiary-items/1Zo0SRU37QPsakyi.htm)|Thrown Rock|auto-trad|
|[21SVgDPf6zOVZqq8.htm](age-of-ashes-bestiary-items/21SVgDPf6zOVZqq8.htm)|Silver Dagger|auto-trad|
|[21tiRXk96SAqD3GL.htm](age-of-ashes-bestiary-items/21tiRXk96SAqD3GL.htm)|Divine Prepared Spells|auto-trad|
|[23MQbc0sTY6HeULo.htm](age-of-ashes-bestiary-items/23MQbc0sTY6HeULo.htm)|Rapier|auto-trad|
|[28PBBJfMs67GTIf3.htm](age-of-ashes-bestiary-items/28PBBJfMs67GTIf3.htm)|Leg|auto-trad|
|[29nZUXJwv5knWe9B.htm](age-of-ashes-bestiary-items/29nZUXJwv5knWe9B.htm)|Shoddy Blunderbuss|auto-trad|
|[2AlP13lCWmjbH1ea.htm](age-of-ashes-bestiary-items/2AlP13lCWmjbH1ea.htm)|Staff|auto-trad|
|[2j69CqOoQwIanSEG.htm](age-of-ashes-bestiary-items/2j69CqOoQwIanSEG.htm)|Rejuvenation|auto-trad|
|[2md78psqZkdESFj7.htm](age-of-ashes-bestiary-items/2md78psqZkdESFj7.htm)|Dagger|auto-trad|
|[2mthLx7yReK93i3n.htm](age-of-ashes-bestiary-items/2mthLx7yReK93i3n.htm)|Grab|auto-trad|
|[2MUmPkWIDTgvArGY.htm](age-of-ashes-bestiary-items/2MUmPkWIDTgvArGY.htm)|Greater Alchemist's Fire|auto-trad|
|[2MzdlMtLJGxFzqB3.htm](age-of-ashes-bestiary-items/2MzdlMtLJGxFzqB3.htm)|Jaws|auto-trad|
|[2nAk1Aknjdxt219t.htm](age-of-ashes-bestiary-items/2nAk1Aknjdxt219t.htm)|Darkvision|auto-trad|
|[2OGTeZ56MmSCLl9P.htm](age-of-ashes-bestiary-items/2OGTeZ56MmSCLl9P.htm)|Terrifying Croak|auto-trad|
|[2shpOEob027noRHp.htm](age-of-ashes-bestiary-items/2shpOEob027noRHp.htm)|Darkvision|auto-trad|
|[2tkYDAIULPFGgxZg.htm](age-of-ashes-bestiary-items/2tkYDAIULPFGgxZg.htm)|3-4 Freezing Wind|auto-trad|
|[2Xd0tA3QaFBV8Nwk.htm](age-of-ashes-bestiary-items/2Xd0tA3QaFBV8Nwk.htm)|Silver Dagger|auto-trad|
|[3Evl5c2LrCI0TdM8.htm](age-of-ashes-bestiary-items/3Evl5c2LrCI0TdM8.htm)|Darkvision|auto-trad|
|[3f4ao6e0shuKxhhj.htm](age-of-ashes-bestiary-items/3f4ao6e0shuKxhhj.htm)|Low-Light Vision|auto-trad|
|[3fAiqgS5r4NDvcsP.htm](age-of-ashes-bestiary-items/3fAiqgS5r4NDvcsP.htm)|Low-Light Vision|auto-trad|
|[3h72FdcRfSluIGqq.htm](age-of-ashes-bestiary-items/3h72FdcRfSluIGqq.htm)|Jaws|auto-trad|
|[3LczZ5phdTxD5h93.htm](age-of-ashes-bestiary-items/3LczZ5phdTxD5h93.htm)|Regeneration 20 (Deactivated by Cold)|auto-trad|
|[3nm3Uxd0Ev8aBu6h.htm](age-of-ashes-bestiary-items/3nm3Uxd0Ev8aBu6h.htm)|Fist|auto-trad|
|[3oioSefge5pvcqam.htm](age-of-ashes-bestiary-items/3oioSefge5pvcqam.htm)|Sneak Attack|auto-trad|
|[3s5bvgFh0mAe2sgw.htm](age-of-ashes-bestiary-items/3s5bvgFh0mAe2sgw.htm)|Attack of Opportunity (Jaws only)|auto-trad|
|[3T41i9CjiNKKumzp.htm](age-of-ashes-bestiary-items/3T41i9CjiNKKumzp.htm)|Drain Life|auto-trad|
|[3tkJJs6AuFOwEB2K.htm](age-of-ashes-bestiary-items/3tkJJs6AuFOwEB2K.htm)|Resanguinate|auto-trad|
|[3Tll1xyzUd3udZZZ.htm](age-of-ashes-bestiary-items/3Tll1xyzUd3udZZZ.htm)|Dragon Heat|auto-trad|
|[3U0dwrOeAIosNI9e.htm](age-of-ashes-bestiary-items/3U0dwrOeAIosNI9e.htm)|Swallow Whole|auto-trad|
|[3ueUOMbC3RvbqrJs.htm](age-of-ashes-bestiary-items/3ueUOMbC3RvbqrJs.htm)|Slice Legs|auto-trad|
|[3xCCKuQS0ngUiwBo.htm](age-of-ashes-bestiary-items/3xCCKuQS0ngUiwBo.htm)|Draconic Momentum|auto-trad|
|[40tzhYIHgsaEat6r.htm](age-of-ashes-bestiary-items/40tzhYIHgsaEat6r.htm)|Attack of Opportunity (Special)|auto-trad|
|[42HRKo2P9cQi4d4N.htm](age-of-ashes-bestiary-items/42HRKo2P9cQi4d4N.htm)|Heavy Crossbow|auto-trad|
|[43THokQpj0pCe1oU.htm](age-of-ashes-bestiary-items/43THokQpj0pCe1oU.htm)|Contingency|auto-trad|
|[454nSdnfQdhFZ4cJ.htm](age-of-ashes-bestiary-items/454nSdnfQdhFZ4cJ.htm)|Alchemical Formulas|auto-trad|
|[49agY84bFYE0480h.htm](age-of-ashes-bestiary-items/49agY84bFYE0480h.htm)|Cold Iron Dagger|auto-trad|
|[4IWeNCrddBnixyyR.htm](age-of-ashes-bestiary-items/4IWeNCrddBnixyyR.htm)|Dragonstorm Blade|auto-trad|
|[4K4yW5oQfO6cRjez.htm](age-of-ashes-bestiary-items/4K4yW5oQfO6cRjez.htm)|Breath-Seared Sword|auto-trad|
|[4LjhH5zCnNYIijXd.htm](age-of-ashes-bestiary-items/4LjhH5zCnNYIijXd.htm)|Fist|auto-trad|
|[4RTJGyTlfqna9dXb.htm](age-of-ashes-bestiary-items/4RTJGyTlfqna9dXb.htm)|Sneak Attack|auto-trad|
|[4So35zJiQ6sRy14O.htm](age-of-ashes-bestiary-items/4So35zJiQ6sRy14O.htm)|At-Will Spells|auto-trad|
|[4U9ib14fmpR2wh3D.htm](age-of-ashes-bestiary-items/4U9ib14fmpR2wh3D.htm)|Darkvision|auto-trad|
|[4VBtm8sTSFf6jv0w.htm](age-of-ashes-bestiary-items/4VBtm8sTSFf6jv0w.htm)|Claw|auto-trad|
|[4Wv4ON3tD5f4zi8r.htm](age-of-ashes-bestiary-items/4Wv4ON3tD5f4zi8r.htm)|Shortbow|auto-trad|
|[4x2IFkW9CrMXOEC5.htm](age-of-ashes-bestiary-items/4x2IFkW9CrMXOEC5.htm)|Dual Abuse|auto-trad|
|[53jaWddHn7SXaQse.htm](age-of-ashes-bestiary-items/53jaWddHn7SXaQse.htm)|Claw|auto-trad|
|[5Fp56J3PmkRq827t.htm](age-of-ashes-bestiary-items/5Fp56J3PmkRq827t.htm)|Fast Swallow|auto-trad|
|[5G4MLloILn0eWN2G.htm](age-of-ashes-bestiary-items/5G4MLloILn0eWN2G.htm)|Divine Providence|auto-trad|
|[5MhFpMwgTDRkEcvj.htm](age-of-ashes-bestiary-items/5MhFpMwgTDRkEcvj.htm)|Heat Surge|auto-trad|
|[5PFC36aGHaF9Jyoe.htm](age-of-ashes-bestiary-items/5PFC36aGHaF9Jyoe.htm)|Hand Crossbow|auto-trad|
|[5s7kaeGdKYRnxQlF.htm](age-of-ashes-bestiary-items/5s7kaeGdKYRnxQlF.htm)|Jaws|auto-trad|
|[5SF4QUg5rtkR7Sy2.htm](age-of-ashes-bestiary-items/5SF4QUg5rtkR7Sy2.htm)|Mirror Shield|auto-trad|
|[5UfaoE8p4Wo5eUod.htm](age-of-ashes-bestiary-items/5UfaoE8p4Wo5eUod.htm)|Dagger|auto-trad|
|[5uXkMRUCPP1LhCN7.htm](age-of-ashes-bestiary-items/5uXkMRUCPP1LhCN7.htm)|Darkvision|auto-trad|
|[61z76N54xAG9rCLF.htm](age-of-ashes-bestiary-items/61z76N54xAG9rCLF.htm)|Constant Spells|auto-trad|
|[6CJRM2LT3a7O3Hhp.htm](age-of-ashes-bestiary-items/6CJRM2LT3a7O3Hhp.htm)|Constrict|auto-trad|
|[6CWSsZ6ciRweK8gJ.htm](age-of-ashes-bestiary-items/6CWSsZ6ciRweK8gJ.htm)|Longsword|auto-trad|
|[6hxF1odU5xuRbrDX.htm](age-of-ashes-bestiary-items/6hxF1odU5xuRbrDX.htm)|Paralysis|auto-trad|
|[6js1QaHEpOIDiNH1.htm](age-of-ashes-bestiary-items/6js1QaHEpOIDiNH1.htm)|Fist|auto-trad|
|[6K2DndSSiWoNraQj.htm](age-of-ashes-bestiary-items/6K2DndSSiWoNraQj.htm)|Opportune Backstab|auto-trad|
|[6kF6A19eKeAk7fOO.htm](age-of-ashes-bestiary-items/6kF6A19eKeAk7fOO.htm)|Jaws|auto-trad|
|[6kjg8IiIh8GmUJa7.htm](age-of-ashes-bestiary-items/6kjg8IiIh8GmUJa7.htm)|Mobility|auto-trad|
|[6Lt3GJTTuXRvxjPp.htm](age-of-ashes-bestiary-items/6Lt3GJTTuXRvxjPp.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[6m1q9eJjaPr21j44.htm](age-of-ashes-bestiary-items/6m1q9eJjaPr21j44.htm)|Redirect Energy|auto-trad|
|[6mPULTjHntSqyOmv.htm](age-of-ashes-bestiary-items/6mPULTjHntSqyOmv.htm)|Javelin|auto-trad|
|[6RB1tHnIwHnJMvAR.htm](age-of-ashes-bestiary-items/6RB1tHnIwHnJMvAR.htm)|Composite Longbow|auto-trad|
|[6SHvoR38wFsY94R8.htm](age-of-ashes-bestiary-items/6SHvoR38wFsY94R8.htm)|Negative Healing|auto-trad|
|[6sPaytqHJB4OlRwk.htm](age-of-ashes-bestiary-items/6sPaytqHJB4OlRwk.htm)|Dagger|auto-trad|
|[6xNQdZfi7r9teJPg.htm](age-of-ashes-bestiary-items/6xNQdZfi7r9teJPg.htm)|Fist|auto-trad|
|[6XZ1oUx9X7nxnnLP.htm](age-of-ashes-bestiary-items/6XZ1oUx9X7nxnnLP.htm)|Breath Weapon|auto-trad|
|[7067hk1ZShJt1wS4.htm](age-of-ashes-bestiary-items/7067hk1ZShJt1wS4.htm)|Thrown Weapon Mastery|auto-trad|
|[70hKpMI6lW9A6okT.htm](age-of-ashes-bestiary-items/70hKpMI6lW9A6okT.htm)|Unnerving Gaze|auto-trad|
|[7FqmJPlXVaPfi1Q8.htm](age-of-ashes-bestiary-items/7FqmJPlXVaPfi1Q8.htm)|Bomb Barrage|auto-trad|
|[7im3eC8rElLqJkOE.htm](age-of-ashes-bestiary-items/7im3eC8rElLqJkOE.htm)|Nimble Dodge|auto-trad|
|[7imiZn40Mr9yePd1.htm](age-of-ashes-bestiary-items/7imiZn40Mr9yePd1.htm)|Hunted Shot|auto-trad|
|[7LPUMs664rIwb4pr.htm](age-of-ashes-bestiary-items/7LPUMs664rIwb4pr.htm)|Terrifying Squeal|auto-trad|
|[7nBGTwZnc14evBxB.htm](age-of-ashes-bestiary-items/7nBGTwZnc14evBxB.htm)|Enternal Damnation|auto-trad|
|[7nMzSCqgZtUZbgud.htm](age-of-ashes-bestiary-items/7nMzSCqgZtUZbgud.htm)|Frightful Presence|auto-trad|
|[7OH0rFP8UnDTxfdD.htm](age-of-ashes-bestiary-items/7OH0rFP8UnDTxfdD.htm)|Throw Rock|auto-trad|
|[7P75nDnE4avWwiZu.htm](age-of-ashes-bestiary-items/7P75nDnE4avWwiZu.htm)|Breath Weapon|auto-trad|
|[7sAyxHFuVZ6ydyvs.htm](age-of-ashes-bestiary-items/7sAyxHFuVZ6ydyvs.htm)|Regeneration 30 (Deactivated by Cold Iron)|auto-trad|
|[7SEc5hjoyVGbbNng.htm](age-of-ashes-bestiary-items/7SEc5hjoyVGbbNng.htm)|Intimidating Strike|auto-trad|
|[7sEHksBXglrNJ2nG.htm](age-of-ashes-bestiary-items/7sEHksBXglrNJ2nG.htm)|Jaws|auto-trad|
|[7wgXd0InH2pMtroD.htm](age-of-ashes-bestiary-items/7wgXd0InH2pMtroD.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[7wokY8VtRk0KvDhV.htm](age-of-ashes-bestiary-items/7wokY8VtRk0KvDhV.htm)|Manifest|auto-trad|
|[7WZNigBilIpl5GhG.htm](age-of-ashes-bestiary-items/7WZNigBilIpl5GhG.htm)|Flurry of Daggers|auto-trad|
|[7YRi5wuplNhSsBsZ.htm](age-of-ashes-bestiary-items/7YRi5wuplNhSsBsZ.htm)|Darkvision|auto-trad|
|[85LHfl8B6bj27rPs.htm](age-of-ashes-bestiary-items/85LHfl8B6bj27rPs.htm)|Attack of Opportunity|auto-trad|
|[85uLuXBsMIG8xwb8.htm](age-of-ashes-bestiary-items/85uLuXBsMIG8xwb8.htm)|Fangs|auto-trad|
|[8BX0w5bbGwyCsIQB.htm](age-of-ashes-bestiary-items/8BX0w5bbGwyCsIQB.htm)|Jaws|auto-trad|
|[8BZpNNw0UfSHhtxH.htm](age-of-ashes-bestiary-items/8BZpNNw0UfSHhtxH.htm)|Mask Settlement|auto-trad|
|[8dMHcMp0PP7ZO27V.htm](age-of-ashes-bestiary-items/8dMHcMp0PP7ZO27V.htm)|Deep Breath|auto-trad|
|[8Hel9Nl2oYP8kVQc.htm](age-of-ashes-bestiary-items/8Hel9Nl2oYP8kVQc.htm)|Sickle|auto-trad|
|[8hzvjvbfBWJpAFep.htm](age-of-ashes-bestiary-items/8hzvjvbfBWJpAFep.htm)|Jaws|auto-trad|
|[8KfKjg5fTmB7ZCal.htm](age-of-ashes-bestiary-items/8KfKjg5fTmB7ZCal.htm)|Darkvision|auto-trad|
|[8l7ngHz7yzWzu3vM.htm](age-of-ashes-bestiary-items/8l7ngHz7yzWzu3vM.htm)|Elven Curve Blade|auto-trad|
|[8nd5VVeytqa77pD8.htm](age-of-ashes-bestiary-items/8nd5VVeytqa77pD8.htm)|Rapier|auto-trad|
|[8NJr1zJWkswMN2qH.htm](age-of-ashes-bestiary-items/8NJr1zJWkswMN2qH.htm)|Reactive|auto-trad|
|[8OAdLpknHQWsmvmE.htm](age-of-ashes-bestiary-items/8OAdLpknHQWsmvmE.htm)|Orange Eye Beam|auto-trad|
|[8sgTj0l0kmAK5fpR.htm](age-of-ashes-bestiary-items/8sgTj0l0kmAK5fpR.htm)|Collapse Ceiling|auto-trad|
|[8V6geW11QKk6SncS.htm](age-of-ashes-bestiary-items/8V6geW11QKk6SncS.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[8XB58su9CoZLqKH7.htm](age-of-ashes-bestiary-items/8XB58su9CoZLqKH7.htm)|Fist|auto-trad|
|[8XwJYwcx1a2Md13M.htm](age-of-ashes-bestiary-items/8XwJYwcx1a2Md13M.htm)|Tail|auto-trad|
|[91EDD4ZPxtMkhm0e.htm](age-of-ashes-bestiary-items/91EDD4ZPxtMkhm0e.htm)|Negative Healing|auto-trad|
|[95BHm1GQDEedURfw.htm](age-of-ashes-bestiary-items/95BHm1GQDEedURfw.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[97RfnojA9doZCy1G.htm](age-of-ashes-bestiary-items/97RfnojA9doZCy1G.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[9A3DxTCqkhPOFzSd.htm](age-of-ashes-bestiary-items/9A3DxTCqkhPOFzSd.htm)|Weapon Supremacy|auto-trad|
|[9APnpN5PuTxG8v98.htm](age-of-ashes-bestiary-items/9APnpN5PuTxG8v98.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[9b70qoAMc6GR4eCV.htm](age-of-ashes-bestiary-items/9b70qoAMc6GR4eCV.htm)|Negative Healing|auto-trad|
|[9dzKJk6ywUvYJxGr.htm](age-of-ashes-bestiary-items/9dzKJk6ywUvYJxGr.htm)|Efficient Capture|auto-trad|
|[9EtKLNxC3l6ph7LD.htm](age-of-ashes-bestiary-items/9EtKLNxC3l6ph7LD.htm)|Eight Coils|auto-trad|
|[9jVBK8J30sirxvYJ.htm](age-of-ashes-bestiary-items/9jVBK8J30sirxvYJ.htm)|Javelin|auto-trad|
|[9kCNk71urnz8zzJ8.htm](age-of-ashes-bestiary-items/9kCNk71urnz8zzJ8.htm)|Frighten|auto-trad|
|[9m7Isxk9yPpKADHT.htm](age-of-ashes-bestiary-items/9m7Isxk9yPpKADHT.htm)|Rituals|auto-trad|
|[9Ogunu3AaCrgsxpU.htm](age-of-ashes-bestiary-items/9Ogunu3AaCrgsxpU.htm)|Telekinetic Crystalline Assault|auto-trad|
|[9RaYMbm66QxVktif.htm](age-of-ashes-bestiary-items/9RaYMbm66QxVktif.htm)|Shipboard Savvy|auto-trad|
|[9SKMKDVUzRsDh3cL.htm](age-of-ashes-bestiary-items/9SKMKDVUzRsDh3cL.htm)|Darkvision|auto-trad|
|[9uKEXs9NLIvxXisk.htm](age-of-ashes-bestiary-items/9uKEXs9NLIvxXisk.htm)|Green Eye Beam|auto-trad|
|[9V1OCxFjilGcKV8s.htm](age-of-ashes-bestiary-items/9V1OCxFjilGcKV8s.htm)|Major Frost Vial|auto-trad|
|[9x8nl6F6ULV0kslr.htm](age-of-ashes-bestiary-items/9x8nl6F6ULV0kslr.htm)|Tongue|auto-trad|
|[9yNmVUtApEo0Ky5V.htm](age-of-ashes-bestiary-items/9yNmVUtApEo0Ky5V.htm)|Dead Spells|auto-trad|
|[A4k38xHP7GlSzTCt.htm](age-of-ashes-bestiary-items/A4k38xHP7GlSzTCt.htm)|Whip|auto-trad|
|[A4w2Db2zI5g3QPOa.htm](age-of-ashes-bestiary-items/A4w2Db2zI5g3QPOa.htm)|Negative Healing|auto-trad|
|[abiEy2lj68FImtR8.htm](age-of-ashes-bestiary-items/abiEy2lj68FImtR8.htm)|Enslave Soul|auto-trad|
|[AdsHpmWCgQto5myp.htm](age-of-ashes-bestiary-items/AdsHpmWCgQto5myp.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[AGJUwsQFIjEtEsuB.htm](age-of-ashes-bestiary-items/AGJUwsQFIjEtEsuB.htm)|Natural Invisibility|auto-trad|
|[aGM9Fk35YNBZ55lZ.htm](age-of-ashes-bestiary-items/aGM9Fk35YNBZ55lZ.htm)|Eternal Damnation|auto-trad|
|[agUYP6p6kB6OegCM.htm](age-of-ashes-bestiary-items/agUYP6p6kB6OegCM.htm)|Verdant Staff|auto-trad|
|[agvYAJn23dKfPmcI.htm](age-of-ashes-bestiary-items/agvYAJn23dKfPmcI.htm)|Branch|auto-trad|
|[aGyOlzElbjBAx2ru.htm](age-of-ashes-bestiary-items/aGyOlzElbjBAx2ru.htm)|Occult Spontaneous Spells|auto-trad|
|[AHGxpzkTgjwABgBa.htm](age-of-ashes-bestiary-items/AHGxpzkTgjwABgBa.htm)|Pseudopod|auto-trad|
|[AhUdp5npAh56MGtg.htm](age-of-ashes-bestiary-items/AhUdp5npAh56MGtg.htm)|Shell Spikes|auto-trad|
|[AjWArPc1kXvPmWvW.htm](age-of-ashes-bestiary-items/AjWArPc1kXvPmWvW.htm)|Primal Innate Spells|auto-trad|
|[AkuMvwXObEyEbsN4.htm](age-of-ashes-bestiary-items/AkuMvwXObEyEbsN4.htm)|Corrosive Whip|auto-trad|
|[Am5pbCNVDkDBNz5Y.htm](age-of-ashes-bestiary-items/Am5pbCNVDkDBNz5Y.htm)|Stench|auto-trad|
|[aM8ZIzzGKo3LwAaz.htm](age-of-ashes-bestiary-items/aM8ZIzzGKo3LwAaz.htm)|Cleric Domain Spells|auto-trad|
|[aMtBg34djNUQyLrP.htm](age-of-ashes-bestiary-items/aMtBg34djNUQyLrP.htm)|Telepathy 1 Mile|auto-trad|
|[anCls0FI0TRAgdl5.htm](age-of-ashes-bestiary-items/anCls0FI0TRAgdl5.htm)|Ritual|auto-trad|
|[anEeBSbM8nCZzVZ4.htm](age-of-ashes-bestiary-items/anEeBSbM8nCZzVZ4.htm)|Scent (Imprecise) 120 feet|auto-trad|
|[aocJaGIdE31itQEp.htm](age-of-ashes-bestiary-items/aocJaGIdE31itQEp.htm)|Swift Slash|auto-trad|
|[aod69CMkW6wAO7gp.htm](age-of-ashes-bestiary-items/aod69CMkW6wAO7gp.htm)|Spear|auto-trad|
|[AP6aPJLtb3i0qgVG.htm](age-of-ashes-bestiary-items/AP6aPJLtb3i0qgVG.htm)|Dagger|auto-trad|
|[Ari4IpvWQTIYV64K.htm](age-of-ashes-bestiary-items/Ari4IpvWQTIYV64K.htm)|Yellow Eye Beam|auto-trad|
|[aTXAOXeP9oRRcBfj.htm](age-of-ashes-bestiary-items/aTXAOXeP9oRRcBfj.htm)|Grasping Hands|auto-trad|
|[axG8Gqi3ZFCSb6l3.htm](age-of-ashes-bestiary-items/axG8Gqi3ZFCSb6l3.htm)|Staff|auto-trad|
|[AxvNayOm3jws9vMf.htm](age-of-ashes-bestiary-items/AxvNayOm3jws9vMf.htm)|Constrict|auto-trad|
|[ayGFqy96KRzgh1Jx.htm](age-of-ashes-bestiary-items/ayGFqy96KRzgh1Jx.htm)|Magma Splash|auto-trad|
|[ayPIYrcz520FXkl7.htm](age-of-ashes-bestiary-items/ayPIYrcz520FXkl7.htm)|Sap|auto-trad|
|[aYXWYRJZzjfCY6n3.htm](age-of-ashes-bestiary-items/aYXWYRJZzjfCY6n3.htm)|Arcane Rituals|auto-trad|
|[AzkWSZ1llpwDtMLX.htm](age-of-ashes-bestiary-items/AzkWSZ1llpwDtMLX.htm)|Spin Silk|auto-trad|
|[Azt0vDQY7wORmDZM.htm](age-of-ashes-bestiary-items/Azt0vDQY7wORmDZM.htm)|Ritual|auto-trad|
|[B2pDIbpVOV0J4SPQ.htm](age-of-ashes-bestiary-items/B2pDIbpVOV0J4SPQ.htm)|Primal Innate Spells|auto-trad|
|[B2pWjBsWDmisLrz0.htm](age-of-ashes-bestiary-items/B2pWjBsWDmisLrz0.htm)|Running Poison Strike|auto-trad|
|[b4gT3E9tko3GkAav.htm](age-of-ashes-bestiary-items/b4gT3E9tko3GkAav.htm)|Dagger|auto-trad|
|[B6yK16tyh4kEH9SU.htm](age-of-ashes-bestiary-items/B6yK16tyh4kEH9SU.htm)|Negative Healing|auto-trad|
|[b8bCRfgf4RGqbIsZ.htm](age-of-ashes-bestiary-items/b8bCRfgf4RGqbIsZ.htm)|Attack of Opportunity|auto-trad|
|[b9ku7PIEzSu9iFAq.htm](age-of-ashes-bestiary-items/b9ku7PIEzSu9iFAq.htm)|Breath Weapon|auto-trad|
|[bbD52Yw4aCU9xjzy.htm](age-of-ashes-bestiary-items/bbD52Yw4aCU9xjzy.htm)|Composite Longbow|auto-trad|
|[BbG4VRHLFcOdy1kI.htm](age-of-ashes-bestiary-items/BbG4VRHLFcOdy1kI.htm)|Darkvision|auto-trad|
|[bbL3QbriTw1sy7so.htm](age-of-ashes-bestiary-items/bbL3QbriTw1sy7so.htm)|Belch Smoke|auto-trad|
|[bc7DxJeQl2oX946w.htm](age-of-ashes-bestiary-items/bc7DxJeQl2oX946w.htm)|Woodland Stride|auto-trad|
|[BCcVa7LcDbfMataD.htm](age-of-ashes-bestiary-items/BCcVa7LcDbfMataD.htm)|Rejuvenation|auto-trad|
|[bD1gctOA2Ni82NSM.htm](age-of-ashes-bestiary-items/bD1gctOA2Ni82NSM.htm)|Breath-Seared Greatsword|auto-trad|
|[BDUlQffqQeFK7d1K.htm](age-of-ashes-bestiary-items/BDUlQffqQeFK7d1K.htm)|Focused Breath Weapon|auto-trad|
|[BEdK4O8CNtdvGUvX.htm](age-of-ashes-bestiary-items/BEdK4O8CNtdvGUvX.htm)|Shadow Strike|auto-trad|
|[bFgmfCe3rBGAWZK1.htm](age-of-ashes-bestiary-items/bFgmfCe3rBGAWZK1.htm)|Darkvision|auto-trad|
|[bfmkqrbi3vcWnEQ8.htm](age-of-ashes-bestiary-items/bfmkqrbi3vcWnEQ8.htm)|Hunt Prey|auto-trad|
|[bFQ8s2UKCuHaBspm.htm](age-of-ashes-bestiary-items/bFQ8s2UKCuHaBspm.htm)|Noxious Breath|auto-trad|
|[BiJxTD0UCp5DwYqu.htm](age-of-ashes-bestiary-items/BiJxTD0UCp5DwYqu.htm)|Nimble Dodge|auto-trad|
|[BIvhzgu4Bi6t171L.htm](age-of-ashes-bestiary-items/BIvhzgu4Bi6t171L.htm)|Deny Advantage|auto-trad|
|[Bizy0PzkQAYT8X7H.htm](age-of-ashes-bestiary-items/Bizy0PzkQAYT8X7H.htm)|Heat Rock|auto-trad|
|[BJZe244H2HjWtcpq.htm](age-of-ashes-bestiary-items/BJZe244H2HjWtcpq.htm)|Lava Bomb|auto-trad|
|[bK1xdYaxhyB8kJli.htm](age-of-ashes-bestiary-items/bK1xdYaxhyB8kJli.htm)|Sudden Charge|auto-trad|
|[BKBcAo4en90X1m11.htm](age-of-ashes-bestiary-items/BKBcAo4en90X1m11.htm)|Arcane Prepared Spells|auto-trad|
|[BKJk7pGIcKhoCKA6.htm](age-of-ashes-bestiary-items/BKJk7pGIcKhoCKA6.htm)|Forge Breath|auto-trad|
|[bLoaxjzxhJesDzcL.htm](age-of-ashes-bestiary-items/bLoaxjzxhJesDzcL.htm)|Golem Antimagic|auto-trad|
|[bnOBGha4PmmSNkZJ.htm](age-of-ashes-bestiary-items/bnOBGha4PmmSNkZJ.htm)|Stench|auto-trad|
|[Brr7VPPaAfdPuEoy.htm](age-of-ashes-bestiary-items/Brr7VPPaAfdPuEoy.htm)|Occult Innate Spells|auto-trad|
|[BSFOYE4jxrwLzw75.htm](age-of-ashes-bestiary-items/BSFOYE4jxrwLzw75.htm)|Regeneration 25|auto-trad|
|[bTTWJNcprDJ2oony.htm](age-of-ashes-bestiary-items/bTTWJNcprDJ2oony.htm)|Attack of Opportunity|auto-trad|
|[BtXKfb6sra2e5xGW.htm](age-of-ashes-bestiary-items/BtXKfb6sra2e5xGW.htm)|Tail|auto-trad|
|[BuGYWvXzoxguBhaX.htm](age-of-ashes-bestiary-items/BuGYWvXzoxguBhaX.htm)|Quick Dervish Strike|auto-trad|
|[bUNqaCaQSkkB6se8.htm](age-of-ashes-bestiary-items/bUNqaCaQSkkB6se8.htm)|+1 Status to All Saves vs. Mental|auto-trad|
|[BvK09Yczybu8kLG6.htm](age-of-ashes-bestiary-items/BvK09Yczybu8kLG6.htm)|Darkvision|auto-trad|
|[bvXcRvh1dBNmRcSg.htm](age-of-ashes-bestiary-items/bvXcRvh1dBNmRcSg.htm)|Live a Thousand Lives|auto-trad|
|[BWXMuY5UwGtZJiaG.htm](age-of-ashes-bestiary-items/BWXMuY5UwGtZJiaG.htm)|Knockdown|auto-trad|
|[bXT2RQRD2q0ZmU2m.htm](age-of-ashes-bestiary-items/bXT2RQRD2q0ZmU2m.htm)|Soul Chain|auto-trad|
|[C18MqFsSOmOKFyGk.htm](age-of-ashes-bestiary-items/C18MqFsSOmOKFyGk.htm)|Divine Prepared Spells|auto-trad|
|[c2BbfXmMmIl5wmhi.htm](age-of-ashes-bestiary-items/c2BbfXmMmIl5wmhi.htm)|Infrared Vision|auto-trad|
|[C5HAmEH4wICnDXXA.htm](age-of-ashes-bestiary-items/C5HAmEH4wICnDXXA.htm)|Occult Spontaneous Spells|auto-trad|
|[C9IfyXUudzvj3eop.htm](age-of-ashes-bestiary-items/C9IfyXUudzvj3eop.htm)|Deep Breath|auto-trad|
|[CadzQEjQFp41MtkP.htm](age-of-ashes-bestiary-items/CadzQEjQFp41MtkP.htm)|Blowgun|auto-trad|
|[cbNmcndriv0G3Pqu.htm](age-of-ashes-bestiary-items/cbNmcndriv0G3Pqu.htm)|Dagger|auto-trad|
|[cBPi7wQqwCZJ4GR7.htm](age-of-ashes-bestiary-items/cBPi7wQqwCZJ4GR7.htm)|Efficient Capture|auto-trad|
|[CCddbUlZDpZVHPi9.htm](age-of-ashes-bestiary-items/CCddbUlZDpZVHPi9.htm)|Arcane Spells Known|auto-trad|
|[ceaQ24wBGNxFLxDc.htm](age-of-ashes-bestiary-items/ceaQ24wBGNxFLxDc.htm)|Dagger|auto-trad|
|[CFIFmdujP4DOl3KL.htm](age-of-ashes-bestiary-items/CFIFmdujP4DOl3KL.htm)|Thrown Weapon Mastery|auto-trad|
|[CgQzFuFeDdZQSFQL.htm](age-of-ashes-bestiary-items/CgQzFuFeDdZQSFQL.htm)|Jaws|auto-trad|
|[CJe3a5GahNX20FL8.htm](age-of-ashes-bestiary-items/CJe3a5GahNX20FL8.htm)|Divine Innate Spells|auto-trad|
|[CKxM9QsObYnbO5fx.htm](age-of-ashes-bestiary-items/CKxM9QsObYnbO5fx.htm)|Dagger|auto-trad|
|[CLQtaRBZzEVhkEpc.htm](age-of-ashes-bestiary-items/CLQtaRBZzEVhkEpc.htm)|Staff Gems|auto-trad|
|[CP15DfrCLkhY6tX0.htm](age-of-ashes-bestiary-items/CP15DfrCLkhY6tX0.htm)|Darkvision|auto-trad|
|[cP2We7SRbI7YuRzt.htm](age-of-ashes-bestiary-items/cP2We7SRbI7YuRzt.htm)|Constrict|auto-trad|
|[cpgcQN4JOvn67e7q.htm](age-of-ashes-bestiary-items/cpgcQN4JOvn67e7q.htm)|Jaws|auto-trad|
|[cstX7ITEtTu7WN4L.htm](age-of-ashes-bestiary-items/cstX7ITEtTu7WN4L.htm)|Vine Lash|auto-trad|
|[cSY3pNZ6AkV4mhbe.htm](age-of-ashes-bestiary-items/cSY3pNZ6AkV4mhbe.htm)|Goblin Scuttle|auto-trad|
|[ct0XvSPn4vZOIhf3.htm](age-of-ashes-bestiary-items/ct0XvSPn4vZOIhf3.htm)|Punish Imperfection|auto-trad|
|[CU86HVtetBBtNPBx.htm](age-of-ashes-bestiary-items/CU86HVtetBBtNPBx.htm)|Dragonstorm Strike|auto-trad|
|[cUPegejySeJGT7RK.htm](age-of-ashes-bestiary-items/cUPegejySeJGT7RK.htm)|Corpse Disguise|auto-trad|
|[CVnZaJ028qTQ4fIE.htm](age-of-ashes-bestiary-items/CVnZaJ028qTQ4fIE.htm)|Darkvision|auto-trad|
|[D1apG8hCM6fFYauw.htm](age-of-ashes-bestiary-items/D1apG8hCM6fFYauw.htm)|Breath Weapon|auto-trad|
|[d5HPFb8SkWD4lJZS.htm](age-of-ashes-bestiary-items/d5HPFb8SkWD4lJZS.htm)|Dagger|auto-trad|
|[D72zZj9nlTG1c06S.htm](age-of-ashes-bestiary-items/D72zZj9nlTG1c06S.htm)|Horns|auto-trad|
|[d8AM5zYWUUZUAdDc.htm](age-of-ashes-bestiary-items/d8AM5zYWUUZUAdDc.htm)|Batter the Fallen|auto-trad|
|[D8iQ4Gr9GPDsrYgl.htm](age-of-ashes-bestiary-items/D8iQ4Gr9GPDsrYgl.htm)|Searing Heat|auto-trad|
|[D9pjDoEUMn7yYSQT.htm](age-of-ashes-bestiary-items/D9pjDoEUMn7yYSQT.htm)|Mudwalking|auto-trad|
|[Da7jV5DGeFRpV5qb.htm](age-of-ashes-bestiary-items/Da7jV5DGeFRpV5qb.htm)|Unleash Fragments|auto-trad|
|[DCLGBKaqRvqYPFZZ.htm](age-of-ashes-bestiary-items/DCLGBKaqRvqYPFZZ.htm)|Divine Innate Spells|auto-trad|
|[DCw8NyXCQyGIT7dh.htm](age-of-ashes-bestiary-items/DCw8NyXCQyGIT7dh.htm)|Spear (Thrown)|auto-trad|
|[dDFsE7xMeGqFnlPS.htm](age-of-ashes-bestiary-items/dDFsE7xMeGqFnlPS.htm)|Firebleed|auto-trad|
|[DdM0HuOCPDC9g1gC.htm](age-of-ashes-bestiary-items/DdM0HuOCPDC9g1gC.htm)|Rend|auto-trad|
|[DDzd3ocX0kkjYD9e.htm](age-of-ashes-bestiary-items/DDzd3ocX0kkjYD9e.htm)|Trap Dodger|auto-trad|
|[delWZwlGtrbwQofE.htm](age-of-ashes-bestiary-items/delWZwlGtrbwQofE.htm)|Shell Game|auto-trad|
|[DEoIu6kGyGbwbSn3.htm](age-of-ashes-bestiary-items/DEoIu6kGyGbwbSn3.htm)|Consume Flesh|auto-trad|
|[DF8C1D50NHzFNPco.htm](age-of-ashes-bestiary-items/DF8C1D50NHzFNPco.htm)|Attack of Opportunity|auto-trad|
|[DFdEgb4naWOYCPXX.htm](age-of-ashes-bestiary-items/DFdEgb4naWOYCPXX.htm)|Site Bound|auto-trad|
|[dfNLOS9TpYGudTfO.htm](age-of-ashes-bestiary-items/dfNLOS9TpYGudTfO.htm)|Improved Grab|auto-trad|
|[DH9wyO9XNqPjz1gc.htm](age-of-ashes-bestiary-items/DH9wyO9XNqPjz1gc.htm)|Bonds of Iron|auto-trad|
|[dI9RG9Y1v3nG3JkI.htm](age-of-ashes-bestiary-items/dI9RG9Y1v3nG3JkI.htm)|Longbow|auto-trad|
|[djgu0hguLWheZqK0.htm](age-of-ashes-bestiary-items/djgu0hguLWheZqK0.htm)|Divine Rituals|auto-trad|
|[DjmGUYM8SGPzUgRQ.htm](age-of-ashes-bestiary-items/DjmGUYM8SGPzUgRQ.htm)|Telekinetic Assault|auto-trad|
|[DJoAQhbwF6EFhlAL.htm](age-of-ashes-bestiary-items/DJoAQhbwF6EFhlAL.htm)|Swallow Whole|auto-trad|
|[dKp6yeEMkDRQTEq9.htm](age-of-ashes-bestiary-items/dKp6yeEMkDRQTEq9.htm)|Greatclub|auto-trad|
|[DlwhGb66IrdDlQFU.htm](age-of-ashes-bestiary-items/DlwhGb66IrdDlQFU.htm)|Hand|auto-trad|
|[Dmc8CB0p5bleq65p.htm](age-of-ashes-bestiary-items/Dmc8CB0p5bleq65p.htm)|Soul Shriek|auto-trad|
|[DMn2jw41YgSewTab.htm](age-of-ashes-bestiary-items/DMn2jw41YgSewTab.htm)|Leg|auto-trad|
|[Do1UK3MaHGPk6Raa.htm](age-of-ashes-bestiary-items/Do1UK3MaHGPk6Raa.htm)|Shrieking Frenzy|auto-trad|
|[doKYunVCGEcZDNXb.htm](age-of-ashes-bestiary-items/doKYunVCGEcZDNXb.htm)|Light Blindness|auto-trad|
|[DOuyoqgTXzVADMKJ.htm](age-of-ashes-bestiary-items/DOuyoqgTXzVADMKJ.htm)|Attack of Opportunity (Special)|auto-trad|
|[DPwtg3OqirDYbsrI.htm](age-of-ashes-bestiary-items/DPwtg3OqirDYbsrI.htm)|Focus Gaze|auto-trad|
|[dQFY1Er30mYSlRnj.htm](age-of-ashes-bestiary-items/dQFY1Er30mYSlRnj.htm)|Arcane Prepared Spells|auto-trad|
|[DqIf07lCKF6aemjq.htm](age-of-ashes-bestiary-items/DqIf07lCKF6aemjq.htm)|Jungle Stride|auto-trad|
|[dqiO3lCiS7jAYB3f.htm](age-of-ashes-bestiary-items/dqiO3lCiS7jAYB3f.htm)|Improved Evasion|auto-trad|
|[dU7uiLZral2AnGbC.htm](age-of-ashes-bestiary-items/dU7uiLZral2AnGbC.htm)|Confuse|auto-trad|
|[DuTAy3tlbbhX6YSj.htm](age-of-ashes-bestiary-items/DuTAy3tlbbhX6YSj.htm)|Darkvision|auto-trad|
|[DxojGnsJOG0ItLxo.htm](age-of-ashes-bestiary-items/DxojGnsJOG0ItLxo.htm)|Darkvision|auto-trad|
|[DyilSr4hYYAgoPFF.htm](age-of-ashes-bestiary-items/DyilSr4hYYAgoPFF.htm)|Reverberating Revenge|auto-trad|
|[Dz4yG96ucUj1zePA.htm](age-of-ashes-bestiary-items/Dz4yG96ucUj1zePA.htm)|Draconic Frenzy|auto-trad|
|[e0qpil2ll45f3L8R.htm](age-of-ashes-bestiary-items/e0qpil2ll45f3L8R.htm)|Greater Thunderstone|auto-trad|
|[e2jWaDeB99NST1wo.htm](age-of-ashes-bestiary-items/e2jWaDeB99NST1wo.htm)|Negative Healing|auto-trad|
|[e2Kf0nqsKQ0S7M4n.htm](age-of-ashes-bestiary-items/e2Kf0nqsKQ0S7M4n.htm)|Smoky Retreat|auto-trad|
|[E6DkiwmjLTAKqArb.htm](age-of-ashes-bestiary-items/E6DkiwmjLTAKqArb.htm)|Primal Innate Spells|auto-trad|
|[e73hhyixLdkOAGhD.htm](age-of-ashes-bestiary-items/e73hhyixLdkOAGhD.htm)|Javelin|auto-trad|
|[e7C7CLUio6dT7UwM.htm](age-of-ashes-bestiary-items/e7C7CLUio6dT7UwM.htm)|Web|auto-trad|
|[ebQ7QkhGrSSsXuy0.htm](age-of-ashes-bestiary-items/ebQ7QkhGrSSsXuy0.htm)|Drain Bonded Item|auto-trad|
|[EcbzD7TsltUXe4Ge.htm](age-of-ashes-bestiary-items/EcbzD7TsltUXe4Ge.htm)|Wing|auto-trad|
|[eCRAMtpF3k73LRFd.htm](age-of-ashes-bestiary-items/eCRAMtpF3k73LRFd.htm)|Paralysis|auto-trad|
|[EDnXO2OsX5YQGXQy.htm](age-of-ashes-bestiary-items/EDnXO2OsX5YQGXQy.htm)|Trample|auto-trad|
|[EDSriTe4fC1hsq33.htm](age-of-ashes-bestiary-items/EDSriTe4fC1hsq33.htm)|Attack of Opportunity (Special)|auto-trad|
|[EferdcZmtLTCxvjj.htm](age-of-ashes-bestiary-items/EferdcZmtLTCxvjj.htm)|Staff Mastery|auto-trad|
|[efWcncs8XDaP6xQA.htm](age-of-ashes-bestiary-items/efWcncs8XDaP6xQA.htm)|Corrosive Dagger|auto-trad|
|[Egs11D8l9LMkS7uI.htm](age-of-ashes-bestiary-items/Egs11D8l9LMkS7uI.htm)|Needle Rain|auto-trad|
|[ekJjxL6yjWuexk2l.htm](age-of-ashes-bestiary-items/ekJjxL6yjWuexk2l.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[Elbm7mEpf5b1tHsf.htm](age-of-ashes-bestiary-items/Elbm7mEpf5b1tHsf.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[EM1Qpnk3JXilo1Qb.htm](age-of-ashes-bestiary-items/EM1Qpnk3JXilo1Qb.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[EMNCDSnJYrVEceeF.htm](age-of-ashes-bestiary-items/EMNCDSnJYrVEceeF.htm)|Fangs|auto-trad|
|[eMRmzaCOQXRtZ69R.htm](age-of-ashes-bestiary-items/eMRmzaCOQXRtZ69R.htm)|Power Attack|auto-trad|
|[EorcRyybCCQIkYR8.htm](age-of-ashes-bestiary-items/EorcRyybCCQIkYR8.htm)|Woodland Elf|auto-trad|
|[ePFuv9mcwpdpUPHc.htm](age-of-ashes-bestiary-items/ePFuv9mcwpdpUPHc.htm)|Claw|auto-trad|
|[EPvcNjklx5lHjwSK.htm](age-of-ashes-bestiary-items/EPvcNjklx5lHjwSK.htm)|Counterspell|auto-trad|
|[eQxLyu1oOuefKVbK.htm](age-of-ashes-bestiary-items/eQxLyu1oOuefKVbK.htm)|+2 Circumstance to All Saves to Disbelieve Illusions|auto-trad|
|[eTaUxizC6y1b7l3T.htm](age-of-ashes-bestiary-items/eTaUxizC6y1b7l3T.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[ETzIyo5ZvX3NpC3J.htm](age-of-ashes-bestiary-items/ETzIyo5ZvX3NpC3J.htm)|Obliteration Beam|auto-trad|
|[EuBw3avFZNz7BacT.htm](age-of-ashes-bestiary-items/EuBw3avFZNz7BacT.htm)|Dagger|auto-trad|
|[eV1zBAr9CgzQ2C5X.htm](age-of-ashes-bestiary-items/eV1zBAr9CgzQ2C5X.htm)|Spore Explosion|auto-trad|
|[eVfKn6MTgzYtBFUg.htm](age-of-ashes-bestiary-items/eVfKn6MTgzYtBFUg.htm)|Darkvision|auto-trad|
|[eWRYW3sMrzor03KL.htm](age-of-ashes-bestiary-items/eWRYW3sMrzor03KL.htm)|Frightful Presence|auto-trad|
|[eX2VLJ64ecfgdr3K.htm](age-of-ashes-bestiary-items/eX2VLJ64ecfgdr3K.htm)|Scimitar|auto-trad|
|[EYE5NG1a4JuddG5h.htm](age-of-ashes-bestiary-items/EYE5NG1a4JuddG5h.htm)|Perception and Vision|auto-trad|
|[F2trczQsM5ejlXEm.htm](age-of-ashes-bestiary-items/F2trczQsM5ejlXEm.htm)|Jaws|auto-trad|
|[fBvRbrhTqjBsWGVa.htm](age-of-ashes-bestiary-items/fBvRbrhTqjBsWGVa.htm)|Draconic Frenzy|auto-trad|
|[fDfoVMGyQ5Rl97y8.htm](age-of-ashes-bestiary-items/fDfoVMGyQ5Rl97y8.htm)|Warhammer|auto-trad|
|[FDGrNkxmckSR32ou.htm](age-of-ashes-bestiary-items/FDGrNkxmckSR32ou.htm)|Forge Breath|auto-trad|
|[fINveZMkSiZN9kMX.htm](age-of-ashes-bestiary-items/fINveZMkSiZN9kMX.htm)|Expunge|auto-trad|
|[Fj81D52uCvQB3Zp7.htm](age-of-ashes-bestiary-items/Fj81D52uCvQB3Zp7.htm)|Divine Innate Spells|auto-trad|
|[fjgBPuKgD22yTRuQ.htm](age-of-ashes-bestiary-items/fjgBPuKgD22yTRuQ.htm)|Inject Unstable Mutagen|auto-trad|
|[fjxiygmwqMkFDeyM.htm](age-of-ashes-bestiary-items/fjxiygmwqMkFDeyM.htm)|Constant Spells|auto-trad|
|[FP0lfB9biTbzRAwD.htm](age-of-ashes-bestiary-items/FP0lfB9biTbzRAwD.htm)|Tail|auto-trad|
|[FQumD5lxgm3RrE1g.htm](age-of-ashes-bestiary-items/FQumD5lxgm3RrE1g.htm)|Blindsight (Precise) 120 feet|auto-trad|
|[FrfAFUxOL2jRQG6y.htm](age-of-ashes-bestiary-items/FrfAFUxOL2jRQG6y.htm)|Darkvision|auto-trad|
|[fRTC42KFOaC3bfxs.htm](age-of-ashes-bestiary-items/fRTC42KFOaC3bfxs.htm)|Disrupting Cold Iron Dagger|auto-trad|
|[FwcS5N0auHwePEe8.htm](age-of-ashes-bestiary-items/FwcS5N0auHwePEe8.htm)|Adroit Disarm|auto-trad|
|[fWPGUdYIRBw22fnz.htm](age-of-ashes-bestiary-items/fWPGUdYIRBw22fnz.htm)|Sneak Attack|auto-trad|
|[fXz6WRXGipzaUAhE.htm](age-of-ashes-bestiary-items/fXz6WRXGipzaUAhE.htm)|Radiant Explosion|auto-trad|
|[fyWgrVIE0U0GZARU.htm](age-of-ashes-bestiary-items/fyWgrVIE0U0GZARU.htm)|Lifelike Scintillation|auto-trad|
|[Fzwm2tFZ9TfeKZxu.htm](age-of-ashes-bestiary-items/Fzwm2tFZ9TfeKZxu.htm)|Contingency|auto-trad|
|[G04XQpPTsP0scmx5.htm](age-of-ashes-bestiary-items/G04XQpPTsP0scmx5.htm)|Shipboard Grace|auto-trad|
|[G0N1HOgAQ6sfApBK.htm](age-of-ashes-bestiary-items/G0N1HOgAQ6sfApBK.htm)|Darkvision|auto-trad|
|[G2rX0J2oGGfLdB0F.htm](age-of-ashes-bestiary-items/G2rX0J2oGGfLdB0F.htm)|Attack of Opportunity|auto-trad|
|[g6yamqCVKN9pYELp.htm](age-of-ashes-bestiary-items/g6yamqCVKN9pYELp.htm)|Inspiring Presence|auto-trad|
|[GAMbl4nEeYZTf7VD.htm](age-of-ashes-bestiary-items/GAMbl4nEeYZTf7VD.htm)|Constant Spells|auto-trad|
|[gca4Y5kOq7GHUaJp.htm](age-of-ashes-bestiary-items/gca4Y5kOq7GHUaJp.htm)|Bones of Stone|auto-trad|
|[gDANDUYKwxqtpEpB.htm](age-of-ashes-bestiary-items/gDANDUYKwxqtpEpB.htm)|Graceful Double Slice|auto-trad|
|[GDrd8HWCNlKRmARz.htm](age-of-ashes-bestiary-items/GDrd8HWCNlKRmARz.htm)|Telekinetic Defense|auto-trad|
|[GE9EMUIIHP8yDVMg.htm](age-of-ashes-bestiary-items/GE9EMUIIHP8yDVMg.htm)|Attack of Opportunity|auto-trad|
|[gEAiXMZU4XPY9yTM.htm](age-of-ashes-bestiary-items/gEAiXMZU4XPY9yTM.htm)|Engulf|auto-trad|
|[GFSu7vcUlllwKJzY.htm](age-of-ashes-bestiary-items/GFSu7vcUlllwKJzY.htm)|Darkvision|auto-trad|
|[gg05Cq52L76J5iG8.htm](age-of-ashes-bestiary-items/gg05Cq52L76J5iG8.htm)|Draconic Momentum|auto-trad|
|[ggGBjbtHJJq44MAq.htm](age-of-ashes-bestiary-items/ggGBjbtHJJq44MAq.htm)|Claw|auto-trad|
|[gGMQ3mV8xiQNHsV3.htm](age-of-ashes-bestiary-items/gGMQ3mV8xiQNHsV3.htm)|Attack of Opportunity|auto-trad|
|[gJ5Si4cUxad6gAip.htm](age-of-ashes-bestiary-items/gJ5Si4cUxad6gAip.htm)|Eschew Materials|auto-trad|
|[gKRDi4EHuz4qIfX0.htm](age-of-ashes-bestiary-items/gKRDi4EHuz4qIfX0.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[GLAGuOAJAsQFW841.htm](age-of-ashes-bestiary-items/GLAGuOAJAsQFW841.htm)|Major Alchemist's Fire|auto-trad|
|[GLOrUOMtmv1uHTph.htm](age-of-ashes-bestiary-items/GLOrUOMtmv1uHTph.htm)|Change Shape|auto-trad|
|[gncjMzeBqmNqD4vz.htm](age-of-ashes-bestiary-items/gncjMzeBqmNqD4vz.htm)|Mauler|auto-trad|
|[go1LGZS91Af3mtP5.htm](age-of-ashes-bestiary-items/go1LGZS91Af3mtP5.htm)|Reactive|auto-trad|
|[gOhhyt5jY5m4BCII.htm](age-of-ashes-bestiary-items/gOhhyt5jY5m4BCII.htm)|Motion Sense 60 feet|auto-trad|
|[GpY52PWVwnKAAp0M.htm](age-of-ashes-bestiary-items/GpY52PWVwnKAAp0M.htm)|Quick Bomber|auto-trad|
|[gqBKUBOWGOGtavDi.htm](age-of-ashes-bestiary-items/gqBKUBOWGOGtavDi.htm)|Darkvision|auto-trad|
|[GQHHw4yF36Vczq0u.htm](age-of-ashes-bestiary-items/GQHHw4yF36Vczq0u.htm)|Negative Healing|auto-trad|
|[gQJ5aamwehy4Pgwd.htm](age-of-ashes-bestiary-items/gQJ5aamwehy4Pgwd.htm)|Master Smith|auto-trad|
|[grsWv0LAFcO3iohL.htm](age-of-ashes-bestiary-items/grsWv0LAFcO3iohL.htm)|Claw|auto-trad|
|[gTfs4nOLCXGSkmgU.htm](age-of-ashes-bestiary-items/gTfs4nOLCXGSkmgU.htm)|Silver Dagger|auto-trad|
|[guGMBVBsqTK7okFR.htm](age-of-ashes-bestiary-items/guGMBVBsqTK7okFR.htm)|Slam Doors|auto-trad|
|[gW2dzfKwJdsR6lTt.htm](age-of-ashes-bestiary-items/gW2dzfKwJdsR6lTt.htm)|Channel Smite|auto-trad|
|[gw4Ka0K9vOSo5rbh.htm](age-of-ashes-bestiary-items/gw4Ka0K9vOSo5rbh.htm)|Soul Shriek|auto-trad|
|[gw66iZEKmcFi0JjW.htm](age-of-ashes-bestiary-items/gw66iZEKmcFi0JjW.htm)|Howl|auto-trad|
|[GynHyVx5cEZ0vb9p.htm](age-of-ashes-bestiary-items/GynHyVx5cEZ0vb9p.htm)|Chase Down|auto-trad|
|[gzeeloXOYQJ3ZCvu.htm](age-of-ashes-bestiary-items/gzeeloXOYQJ3ZCvu.htm)|Mental Erosion|auto-trad|
|[GZrlbqO60jo1adXW.htm](age-of-ashes-bestiary-items/GZrlbqO60jo1adXW.htm)|Darkvision|auto-trad|
|[H1vAXye0WIg0VDxS.htm](age-of-ashes-bestiary-items/H1vAXye0WIg0VDxS.htm)|Pick|auto-trad|
|[H2vI4lzuGs1S1ljJ.htm](age-of-ashes-bestiary-items/H2vI4lzuGs1S1ljJ.htm)|True Debilitating Bomb|auto-trad|
|[H2zLCRS1UX8XpweP.htm](age-of-ashes-bestiary-items/H2zLCRS1UX8XpweP.htm)|At-Will Spells|auto-trad|
|[H4swqBH4RI4TQYpt.htm](age-of-ashes-bestiary-items/H4swqBH4RI4TQYpt.htm)|Cold Iron Dagger|auto-trad|
|[h4y4kf4q85cEAF9x.htm](age-of-ashes-bestiary-items/h4y4kf4q85cEAF9x.htm)|At-Will Spells|auto-trad|
|[H6cbKbRUjKF71Gko.htm](age-of-ashes-bestiary-items/H6cbKbRUjKF71Gko.htm)|Dagger|auto-trad|
|[h7AGKtRekFbuZp1Z.htm](age-of-ashes-bestiary-items/h7AGKtRekFbuZp1Z.htm)|Heavy Book|auto-trad|
|[h7oremneSeWvOijy.htm](age-of-ashes-bestiary-items/h7oremneSeWvOijy.htm)|Change Shape|auto-trad|
|[hCNYJ9tfRIOCl989.htm](age-of-ashes-bestiary-items/hCNYJ9tfRIOCl989.htm)|Greater Acid Flask|auto-trad|
|[hdN9cUP88jTa2Ykb.htm](age-of-ashes-bestiary-items/hdN9cUP88jTa2Ykb.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[hdRK1K0vud2hRJEt.htm](age-of-ashes-bestiary-items/hdRK1K0vud2hRJEt.htm)|Promise Guard Stance|auto-trad|
|[HEnPvo9LJho9qb75.htm](age-of-ashes-bestiary-items/HEnPvo9LJho9qb75.htm)|Manipulate Energy|auto-trad|
|[HezD1ffNIEmWNt12.htm](age-of-ashes-bestiary-items/HezD1ffNIEmWNt12.htm)|Tail|auto-trad|
|[Hfn2kPN8v4JuYF1l.htm](age-of-ashes-bestiary-items/Hfn2kPN8v4JuYF1l.htm)|Quick Bomber|auto-trad|
|[hgONc5BRFBZc4j73.htm](age-of-ashes-bestiary-items/hgONc5BRFBZc4j73.htm)|Darkvision|auto-trad|
|[HH6SL5ftuMn3NSHu.htm](age-of-ashes-bestiary-items/HH6SL5ftuMn3NSHu.htm)|Echoing Cry|auto-trad|
|[Hh8TSJGN8yL5SyKZ.htm](age-of-ashes-bestiary-items/Hh8TSJGN8yL5SyKZ.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[hhR7r91njgZuHH4x.htm](age-of-ashes-bestiary-items/hhR7r91njgZuHH4x.htm)|Brutal Cleave|auto-trad|
|[hI3zcHdUBaFLeoZX.htm](age-of-ashes-bestiary-items/hI3zcHdUBaFLeoZX.htm)|Jaws|auto-trad|
|[hJc7HGNvwjdmOdfq.htm](age-of-ashes-bestiary-items/hJc7HGNvwjdmOdfq.htm)|Edifice|auto-trad|
|[HlgR9oLdO3ZGqCry.htm](age-of-ashes-bestiary-items/HlgR9oLdO3ZGqCry.htm)|Reactive Breath|auto-trad|
|[HoXBI3O9X2kkeeC1.htm](age-of-ashes-bestiary-items/HoXBI3O9X2kkeeC1.htm)|Darkvision|auto-trad|
|[hPc0H3LlS5oAeiST.htm](age-of-ashes-bestiary-items/hPc0H3LlS5oAeiST.htm)|Frightful Presence|auto-trad|
|[HPLwJZH8RwLW9c13.htm](age-of-ashes-bestiary-items/HPLwJZH8RwLW9c13.htm)|Black Eye Beam|auto-trad|
|[hRrW3wtTTKj4Kv6s.htm](age-of-ashes-bestiary-items/hRrW3wtTTKj4Kv6s.htm)|Longbow|auto-trad|
|[hrt10usZTV3s3uKQ.htm](age-of-ashes-bestiary-items/hrt10usZTV3s3uKQ.htm)|Haunted Form|auto-trad|
|[HSs8knjTJuMzsAQr.htm](age-of-ashes-bestiary-items/HSs8knjTJuMzsAQr.htm)|Recognize Ally|auto-trad|
|[HtPykjM3gCE3oQiR.htm](age-of-ashes-bestiary-items/HtPykjM3gCE3oQiR.htm)|Torpor|auto-trad|
|[hVAcTBzqhXEsus66.htm](age-of-ashes-bestiary-items/hVAcTBzqhXEsus66.htm)|Wendigo Torment|auto-trad|
|[HVINdiV8c3uYz4or.htm](age-of-ashes-bestiary-items/HVINdiV8c3uYz4or.htm)|Darkvision|auto-trad|
|[hWFsgyTptyHOBJtD.htm](age-of-ashes-bestiary-items/hWFsgyTptyHOBJtD.htm)|Arcane Prepared Spells|auto-trad|
|[HXiycpYZMsyFqs9l.htm](age-of-ashes-bestiary-items/HXiycpYZMsyFqs9l.htm)|Shortbow|auto-trad|
|[hZNyCnxdujLVMKqb.htm](age-of-ashes-bestiary-items/hZNyCnxdujLVMKqb.htm)|Identify an Opening|auto-trad|
|[HzVWIyzzAWGlhGDC.htm](age-of-ashes-bestiary-items/HzVWIyzzAWGlhGDC.htm)|Claw|auto-trad|
|[i2cs4dR5xnohX9E6.htm](age-of-ashes-bestiary-items/i2cs4dR5xnohX9E6.htm)|Primal Innate Spells|auto-trad|
|[i7B8yhwnUu48ZFdz.htm](age-of-ashes-bestiary-items/i7B8yhwnUu48ZFdz.htm)|Dragon Pillar Glance|auto-trad|
|[i7bZ6f6zTCdQwhiN.htm](age-of-ashes-bestiary-items/i7bZ6f6zTCdQwhiN.htm)|Low-Light Vision|auto-trad|
|[IBkN8sEe9mFN0Uf4.htm](age-of-ashes-bestiary-items/IBkN8sEe9mFN0Uf4.htm)|Pseudopod|auto-trad|
|[ICaSdqQPFGE4xmmc.htm](age-of-ashes-bestiary-items/ICaSdqQPFGE4xmmc.htm)|Dagger|auto-trad|
|[IcCigf17UR8tbG7v.htm](age-of-ashes-bestiary-items/IcCigf17UR8tbG7v.htm)|Reflect|auto-trad|
|[ICOl05YuWjz7C6GN.htm](age-of-ashes-bestiary-items/ICOl05YuWjz7C6GN.htm)|Paragon's Guard Stance|auto-trad|
|[IcprrivigaRsacAR.htm](age-of-ashes-bestiary-items/IcprrivigaRsacAR.htm)|Swallow Whole|auto-trad|
|[IDbHyzOZv9b2OyKQ.htm](age-of-ashes-bestiary-items/IDbHyzOZv9b2OyKQ.htm)|Impede|auto-trad|
|[IDc6afCxHUwfqrkX.htm](age-of-ashes-bestiary-items/IDc6afCxHUwfqrkX.htm)|At-Will Spells|auto-trad|
|[Idy0MtsKPxYUxKqC.htm](age-of-ashes-bestiary-items/Idy0MtsKPxYUxKqC.htm)|Ghostly Hand|auto-trad|
|[ieB3sq8pLvjePur7.htm](age-of-ashes-bestiary-items/ieB3sq8pLvjePur7.htm)|Ghast Fever|auto-trad|
|[IFl5ilnFtCXtwx8I.htm](age-of-ashes-bestiary-items/IFl5ilnFtCXtwx8I.htm)|Pack Attack|auto-trad|
|[IHKQdP1f65ZJ8Oqu.htm](age-of-ashes-bestiary-items/IHKQdP1f65ZJ8Oqu.htm)|Poison Weapon|auto-trad|
|[IHmGGY9udr3P8p7m.htm](age-of-ashes-bestiary-items/IHmGGY9udr3P8p7m.htm)|Light Blindness|auto-trad|
|[IJhw9ETWROv5Y9rt.htm](age-of-ashes-bestiary-items/IJhw9ETWROv5Y9rt.htm)|Spikes|auto-trad|
|[iLD58rCFSuSX1HF6.htm](age-of-ashes-bestiary-items/iLD58rCFSuSX1HF6.htm)|Thundering Kukri|auto-trad|
|[IrffLfsr4F6AQ70K.htm](age-of-ashes-bestiary-items/IrffLfsr4F6AQ70K.htm)|Subduing Strikes|auto-trad|
|[iTmWT27s6I0W1gVm.htm](age-of-ashes-bestiary-items/iTmWT27s6I0W1gVm.htm)|Manifest Lesser Dragonstorm|auto-trad|
|[iVkBxitUPibUUdxV.htm](age-of-ashes-bestiary-items/iVkBxitUPibUUdxV.htm)|Shield Breaker|auto-trad|
|[iVzCadSFlB0zTY05.htm](age-of-ashes-bestiary-items/iVzCadSFlB0zTY05.htm)|Shortsword|auto-trad|
|[iVZygV9OkEH28o2Q.htm](age-of-ashes-bestiary-items/iVZygV9OkEH28o2Q.htm)|Black Eye Beam|auto-trad|
|[IwqiOEcQaE9YK1Sa.htm](age-of-ashes-bestiary-items/IwqiOEcQaE9YK1Sa.htm)|Spell Strike|auto-trad|
|[j0tisNS7G4pqIgVz.htm](age-of-ashes-bestiary-items/j0tisNS7G4pqIgVz.htm)|Shield Block|auto-trad|
|[j3NCaHUgoOUiHpye.htm](age-of-ashes-bestiary-items/j3NCaHUgoOUiHpye.htm)|Ghast Fever|auto-trad|
|[j4MfbH7vW63xSuRI.htm](age-of-ashes-bestiary-items/j4MfbH7vW63xSuRI.htm)|Shield Boss|auto-trad|
|[j5LDyAGkf9FeoBAt.htm](age-of-ashes-bestiary-items/j5LDyAGkf9FeoBAt.htm)|Rapier|auto-trad|
|[J7B6WzdiD0YTP0qQ.htm](age-of-ashes-bestiary-items/J7B6WzdiD0YTP0qQ.htm)|Light Blindness|auto-trad|
|[j7hDiUXLpry6mkqN.htm](age-of-ashes-bestiary-items/j7hDiUXLpry6mkqN.htm)|Darkvision|auto-trad|
|[j95Y0cxi70Mb6UMQ.htm](age-of-ashes-bestiary-items/j95Y0cxi70Mb6UMQ.htm)|Rugged Travel|auto-trad|
|[Janx9PZwNEn44igV.htm](age-of-ashes-bestiary-items/Janx9PZwNEn44igV.htm)|Fleshroaster|auto-trad|
|[jBkyceYP8fzH2x9P.htm](age-of-ashes-bestiary-items/jBkyceYP8fzH2x9P.htm)|Rock|auto-trad|
|[jfBUSjcjhlewvG1J.htm](age-of-ashes-bestiary-items/jfBUSjcjhlewvG1J.htm)|Longsword|auto-trad|
|[jFY95i89pS7nXrQl.htm](age-of-ashes-bestiary-items/jFY95i89pS7nXrQl.htm)|Darkvision|auto-trad|
|[jHCCY0ZIpf7axn62.htm](age-of-ashes-bestiary-items/jHCCY0ZIpf7axn62.htm)|Channel Dragonstorm|auto-trad|
|[jI9GCbJaB0b3F4Kw.htm](age-of-ashes-bestiary-items/jI9GCbJaB0b3F4Kw.htm)|Indigo Eye Beam|auto-trad|
|[jL6Ovx8CLeSJ0K2S.htm](age-of-ashes-bestiary-items/jL6Ovx8CLeSJ0K2S.htm)|Divine Innate Spells|auto-trad|
|[jNcUglLORusRcA4f.htm](age-of-ashes-bestiary-items/jNcUglLORusRcA4f.htm)|Red Eye Beam|auto-trad|
|[jQUqBzYMtuQRpcmS.htm](age-of-ashes-bestiary-items/jQUqBzYMtuQRpcmS.htm)|Darkvision|auto-trad|
|[JRd0vnYlb4G88YJ6.htm](age-of-ashes-bestiary-items/JRd0vnYlb4G88YJ6.htm)|Crossbow|auto-trad|
|[jVPlrYrdfUoKsCFZ.htm](age-of-ashes-bestiary-items/jVPlrYrdfUoKsCFZ.htm)|Tremorsense|auto-trad|
|[jwRUzgElIiQUtmhR.htm](age-of-ashes-bestiary-items/jwRUzgElIiQUtmhR.htm)|Dimensional Tether|auto-trad|
|[JZbnTWFBrM3l78qS.htm](age-of-ashes-bestiary-items/JZbnTWFBrM3l78qS.htm)|Volcanic Eruption|auto-trad|
|[jZFyZg8r1I1EWoBY.htm](age-of-ashes-bestiary-items/jZFyZg8r1I1EWoBY.htm)|Timely Distraction|auto-trad|
|[jZsVGuNQlvV5TkQ9.htm](age-of-ashes-bestiary-items/jZsVGuNQlvV5TkQ9.htm)|Perception and Vision|auto-trad|
|[K28NAtboAXcpvklg.htm](age-of-ashes-bestiary-items/K28NAtboAXcpvklg.htm)|Dahak's Glance|auto-trad|
|[K5dIa5gCC7aePzQf.htm](age-of-ashes-bestiary-items/K5dIa5gCC7aePzQf.htm)|Soul Siphon|auto-trad|
|[k7a5PsosLu8FXsTk.htm](age-of-ashes-bestiary-items/k7a5PsosLu8FXsTk.htm)|Speed Scimitar|auto-trad|
|[k8bgECKZjROUF5gA.htm](age-of-ashes-bestiary-items/k8bgECKZjROUF5gA.htm)|Staff of Fire|auto-trad|
|[kedOBfOiNpFOHZa0.htm](age-of-ashes-bestiary-items/kedOBfOiNpFOHZa0.htm)|Jaws|auto-trad|
|[kg205YMOcrEz6XQw.htm](age-of-ashes-bestiary-items/kg205YMOcrEz6XQw.htm)|Ranged Legerdemain|auto-trad|
|[KhFyp3oQ1nmWpdA9.htm](age-of-ashes-bestiary-items/KhFyp3oQ1nmWpdA9.htm)|Darkvision|auto-trad|
|[KHiVxpvVx4edKn9M.htm](age-of-ashes-bestiary-items/KHiVxpvVx4edKn9M.htm)|Horns|auto-trad|
|[KiltiTEGGXxRaLvr.htm](age-of-ashes-bestiary-items/KiltiTEGGXxRaLvr.htm)|Attack of Opportunity|auto-trad|
|[kJlV6Vzu6KZumb6u.htm](age-of-ashes-bestiary-items/kJlV6Vzu6KZumb6u.htm)|Volcanic Heat|auto-trad|
|[KJxjnogCSiWsIs3n.htm](age-of-ashes-bestiary-items/KJxjnogCSiWsIs3n.htm)|Magma Swim|auto-trad|
|[kJz2LXDyAG4vASIf.htm](age-of-ashes-bestiary-items/kJz2LXDyAG4vASIf.htm)|Draconic Frenzy|auto-trad|
|[KKaF35HWVDgg10gX.htm](age-of-ashes-bestiary-items/KKaF35HWVDgg10gX.htm)|Scimitar|auto-trad|
|[KOEGii7EkPZsq9Iz.htm](age-of-ashes-bestiary-items/KOEGii7EkPZsq9Iz.htm)|Glare of Rage|auto-trad|
|[KoIGP6ravPoUypu2.htm](age-of-ashes-bestiary-items/KoIGP6ravPoUypu2.htm)|Constant Spells|auto-trad|
|[koqHCbsGHHME7p6J.htm](age-of-ashes-bestiary-items/koqHCbsGHHME7p6J.htm)|Claw|auto-trad|
|[Kp4oqpyn0RuzJfDQ.htm](age-of-ashes-bestiary-items/Kp4oqpyn0RuzJfDQ.htm)|Staff|auto-trad|
|[kPGNXeFYyzZHqKiv.htm](age-of-ashes-bestiary-items/kPGNXeFYyzZHqKiv.htm)|Return Fire|auto-trad|
|[kqgeNf8SyOkbJ0qi.htm](age-of-ashes-bestiary-items/kqgeNf8SyOkbJ0qi.htm)|Greatsword|auto-trad|
|[KqSNLofqd5sNL9bp.htm](age-of-ashes-bestiary-items/KqSNLofqd5sNL9bp.htm)|Arcane Prepared Spells|auto-trad|
|[KqV4dBWVzfi3DXyl.htm](age-of-ashes-bestiary-items/KqV4dBWVzfi3DXyl.htm)|Efficient Capture|auto-trad|
|[KrTgWTUW9uB20sKT.htm](age-of-ashes-bestiary-items/KrTgWTUW9uB20sKT.htm)|Frightful Presence|auto-trad|
|[kT5280SVobphMNJn.htm](age-of-ashes-bestiary-items/kT5280SVobphMNJn.htm)|Sneak Attack|auto-trad|
|[Kvh0b9Gxe3FzqXsg.htm](age-of-ashes-bestiary-items/Kvh0b9Gxe3FzqXsg.htm)|At-Will Spells|auto-trad|
|[kVU7cb7Ts2sMfPyu.htm](age-of-ashes-bestiary-items/kVU7cb7Ts2sMfPyu.htm)|Whip|auto-trad|
|[kwP6P4BpQ8TfjZTO.htm](age-of-ashes-bestiary-items/kwP6P4BpQ8TfjZTO.htm)|Heavy Book|auto-trad|
|[kx9XRnPu3GxZYqST.htm](age-of-ashes-bestiary-items/kx9XRnPu3GxZYqST.htm)|Darkvision|auto-trad|
|[kyJU0dp0AuLccYHV.htm](age-of-ashes-bestiary-items/kyJU0dp0AuLccYHV.htm)|Breath Weapon|auto-trad|
|[KYpQ8TgZcmUrKnjC.htm](age-of-ashes-bestiary-items/KYpQ8TgZcmUrKnjC.htm)|Independent Limbs|auto-trad|
|[Kz3r31N7mvhlPQc8.htm](age-of-ashes-bestiary-items/Kz3r31N7mvhlPQc8.htm)|Occult Innate Spellss|auto-trad|
|[kZ6DWvbDyy1GyJ6D.htm](age-of-ashes-bestiary-items/kZ6DWvbDyy1GyJ6D.htm)|Personal Quake|auto-trad|
|[kZcHhT2dlbVHF7AS.htm](age-of-ashes-bestiary-items/kZcHhT2dlbVHF7AS.htm)|Kobold Explosives|auto-trad|
|[L0ZHkQFYtz2JK3hH.htm](age-of-ashes-bestiary-items/L0ZHkQFYtz2JK3hH.htm)|Composite Longbow|auto-trad|
|[L10IjFT5OxF7Vdtw.htm](age-of-ashes-bestiary-items/L10IjFT5OxF7Vdtw.htm)|Steady Spellcasting|auto-trad|
|[L1rIkFNagYO7XkgM.htm](age-of-ashes-bestiary-items/L1rIkFNagYO7XkgM.htm)|Wing Deflection|auto-trad|
|[l1U1HsRzfUA7agTu.htm](age-of-ashes-bestiary-items/l1U1HsRzfUA7agTu.htm)|Walk in Shadow|auto-trad|
|[l69fOTyMIBdJNWTY.htm](age-of-ashes-bestiary-items/l69fOTyMIBdJNWTY.htm)|Guardian Sense|auto-trad|
|[L6oDlNintmXHhjAv.htm](age-of-ashes-bestiary-items/L6oDlNintmXHhjAv.htm)|Precision|auto-trad|
|[l7LvpPXTQL4jhPb5.htm](age-of-ashes-bestiary-items/l7LvpPXTQL4jhPb5.htm)|Low-Light Vision|auto-trad|
|[LB86IJopeELMnAze.htm](age-of-ashes-bestiary-items/LB86IJopeELMnAze.htm)|Frightful Moan|auto-trad|
|[lBp2RqOAK3CORaeJ.htm](age-of-ashes-bestiary-items/lBp2RqOAK3CORaeJ.htm)|Spine|auto-trad|
|[lbSqT3JsKYmywHiq.htm](age-of-ashes-bestiary-items/lbSqT3JsKYmywHiq.htm)|Jaws|auto-trad|
|[LbVTFpHTRiZGGVE9.htm](age-of-ashes-bestiary-items/LbVTFpHTRiZGGVE9.htm)|Longsword|auto-trad|
|[lcdFqkdanjf6Jdom.htm](age-of-ashes-bestiary-items/lcdFqkdanjf6Jdom.htm)|Caustic Nightmare Vapor|auto-trad|
|[ld8xSt4F2q5m6vRe.htm](age-of-ashes-bestiary-items/ld8xSt4F2q5m6vRe.htm)|Paralyzing Force|auto-trad|
|[LePEQxJOrm3uEIPP.htm](age-of-ashes-bestiary-items/LePEQxJOrm3uEIPP.htm)|Arcane Innate Spells|auto-trad|
|[lfmWRoj9CXxKLkCs.htm](age-of-ashes-bestiary-items/lfmWRoj9CXxKLkCs.htm)|Red Eye Beam|auto-trad|
|[lJ6fQwHU2UpZ6h6F.htm](age-of-ashes-bestiary-items/lJ6fQwHU2UpZ6h6F.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[LLzxyBASOUnyUmZk.htm](age-of-ashes-bestiary-items/LLzxyBASOUnyUmZk.htm)|Green Empathy|auto-trad|
|[LM8A2eiEIyuB9hk2.htm](age-of-ashes-bestiary-items/LM8A2eiEIyuB9hk2.htm)|Shortbow|auto-trad|
|[LNJkhJSFpg8gvkwp.htm](age-of-ashes-bestiary-items/LNJkhJSFpg8gvkwp.htm)|Heatsight|auto-trad|
|[LnwQpnNyyd6h5Xx0.htm](age-of-ashes-bestiary-items/LnwQpnNyyd6h5Xx0.htm)|Face of the Fatal Divine|auto-trad|
|[lpDLQW5YfXIYXiJS.htm](age-of-ashes-bestiary-items/lpDLQW5YfXIYXiJS.htm)|Claw|auto-trad|
|[lpORbE7ZJjtwviYL.htm](age-of-ashes-bestiary-items/lpORbE7ZJjtwviYL.htm)|Darkvision|auto-trad|
|[lPxb0HqwCjMCigsv.htm](age-of-ashes-bestiary-items/lPxb0HqwCjMCigsv.htm)|Returning Light Hammer|auto-trad|
|[LQ20lTeaoccMicH8.htm](age-of-ashes-bestiary-items/LQ20lTeaoccMicH8.htm)|Psychic Screech|auto-trad|
|[lRGKk5oP862ZMOI3.htm](age-of-ashes-bestiary-items/lRGKk5oP862ZMOI3.htm)|Change Shape|auto-trad|
|[LsLTSWuEmgszvIJo.htm](age-of-ashes-bestiary-items/LsLTSWuEmgszvIJo.htm)|Shortbow|auto-trad|
|[lSxI33XT0yIaAARS.htm](age-of-ashes-bestiary-items/lSxI33XT0yIaAARS.htm)|Darkvision|auto-trad|
|[LT73ykCcsBulHrcy.htm](age-of-ashes-bestiary-items/LT73ykCcsBulHrcy.htm)|Jaws|auto-trad|
|[lup461lEK9zazSYF.htm](age-of-ashes-bestiary-items/lup461lEK9zazSYF.htm)|Arcane Innate Spells|auto-trad|
|[lw6P5HJ72vY1Ef6K.htm](age-of-ashes-bestiary-items/lw6P5HJ72vY1Ef6K.htm)|Composite Shortbow|auto-trad|
|[lWDO2icn4khYwlgs.htm](age-of-ashes-bestiary-items/lWDO2icn4khYwlgs.htm)|Jaws|auto-trad|
|[LwLNUmtRRcLH4yWo.htm](age-of-ashes-bestiary-items/LwLNUmtRRcLH4yWo.htm)|Ghostly Hand|auto-trad|
|[lxqSAaKp7jMekQke.htm](age-of-ashes-bestiary-items/lxqSAaKp7jMekQke.htm)|Inflate Bellows|auto-trad|
|[M0hdta5U4NIGRZAD.htm](age-of-ashes-bestiary-items/M0hdta5U4NIGRZAD.htm)|Terrifying Visions|auto-trad|
|[M4OptJYHoUkyQh2f.htm](age-of-ashes-bestiary-items/M4OptJYHoUkyQh2f.htm)|Trample|auto-trad|
|[M4S8LvGZQeUkoq88.htm](age-of-ashes-bestiary-items/M4S8LvGZQeUkoq88.htm)|Horns|auto-trad|
|[m8n5v2B0saXm9zp1.htm](age-of-ashes-bestiary-items/m8n5v2B0saXm9zp1.htm)|Major Bottled Lightning|auto-trad|
|[m8X8IWzzFl0HzmIE.htm](age-of-ashes-bestiary-items/m8X8IWzzFl0HzmIE.htm)|Golden Luck|auto-trad|
|[MaPDXpGVOauOwVc8.htm](age-of-ashes-bestiary-items/MaPDXpGVOauOwVc8.htm)|Rock|auto-trad|
|[mEFkqvuYvc9WzXcr.htm](age-of-ashes-bestiary-items/mEFkqvuYvc9WzXcr.htm)|Reactive Shield|auto-trad|
|[mgUFs2wbhtnPQXLr.htm](age-of-ashes-bestiary-items/mgUFs2wbhtnPQXLr.htm)|Fist|auto-trad|
|[MhYvDcxQITUxb0tw.htm](age-of-ashes-bestiary-items/MhYvDcxQITUxb0tw.htm)|Sneak Attack|auto-trad|
|[MJg2MnY8wKpQte4q.htm](age-of-ashes-bestiary-items/MJg2MnY8wKpQte4q.htm)|Attack of Opportunity (Special)|auto-trad|
|[mjhchEu4tZZFuxhA.htm](age-of-ashes-bestiary-items/mjhchEu4tZZFuxhA.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[mJhd5iLsmLx74Qjo.htm](age-of-ashes-bestiary-items/mJhd5iLsmLx74Qjo.htm)|Spontaneous Divine Spells|auto-trad|
|[mkKQLOSUvNml05qi.htm](age-of-ashes-bestiary-items/mkKQLOSUvNml05qi.htm)|Composite Shortbow|auto-trad|
|[mKqF9CDVTOOPFVm6.htm](age-of-ashes-bestiary-items/mKqF9CDVTOOPFVm6.htm)|Surprise Attack|auto-trad|
|[MLHgQjjeyBRDloTd.htm](age-of-ashes-bestiary-items/MLHgQjjeyBRDloTd.htm)|Entangling Residue|auto-trad|
|[MMtAAek31GnnT3PK.htm](age-of-ashes-bestiary-items/MMtAAek31GnnT3PK.htm)|Warhammer|auto-trad|
|[mMwtoDsUUn0ZCGvL.htm](age-of-ashes-bestiary-items/mMwtoDsUUn0ZCGvL.htm)|Attack of Opportunity|auto-trad|
|[MNIeEFHVbRZ8b5Ln.htm](age-of-ashes-bestiary-items/MNIeEFHVbRZ8b5Ln.htm)|Hostile Juxtaposition|auto-trad|
|[mnMWLmMnVuCl04IC.htm](age-of-ashes-bestiary-items/mnMWLmMnVuCl04IC.htm)|Fling Sand in the Eyes|auto-trad|
|[Mo8pDcRqUWKoQzPF.htm](age-of-ashes-bestiary-items/Mo8pDcRqUWKoQzPF.htm)|Jaws|auto-trad|
|[moQZVLDgyoG4xYuR.htm](age-of-ashes-bestiary-items/moQZVLDgyoG4xYuR.htm)|Volcanic Purge|auto-trad|
|[MpCCxEfa5YHRcVVR.htm](age-of-ashes-bestiary-items/MpCCxEfa5YHRcVVR.htm)|Mace|auto-trad|
|[MQjyJEBhzAMRt6f5.htm](age-of-ashes-bestiary-items/MQjyJEBhzAMRt6f5.htm)|Ritual|auto-trad|
|[MQvALn3TuAb3iI1B.htm](age-of-ashes-bestiary-items/MQvALn3TuAb3iI1B.htm)|Innate Arcane Spells|auto-trad|
|[Mrf2WIIKXTByxA81.htm](age-of-ashes-bestiary-items/Mrf2WIIKXTByxA81.htm)|Intimidating Attack of Opportunity|auto-trad|
|[mrHQQjwSTfXCnZch.htm](age-of-ashes-bestiary-items/mrHQQjwSTfXCnZch.htm)|Chained Dagger|auto-trad|
|[mSAR7TLZJBzuU46P.htm](age-of-ashes-bestiary-items/mSAR7TLZJBzuU46P.htm)|Thrown Rock|auto-trad|
|[msk25zy4vClQBl9Q.htm](age-of-ashes-bestiary-items/msk25zy4vClQBl9Q.htm)|Inflate Bellows|auto-trad|
|[MTe5oaN8qxo4KYFm.htm](age-of-ashes-bestiary-items/MTe5oaN8qxo4KYFm.htm)|Demiplane Lair|auto-trad|
|[mTgeFQ9F1VC2Jckr.htm](age-of-ashes-bestiary-items/mTgeFQ9F1VC2Jckr.htm)|Wizard School Spells|auto-trad|
|[MuK4kIpfXEUbojeB.htm](age-of-ashes-bestiary-items/MuK4kIpfXEUbojeB.htm)|HP is 72 for each 5 foot patch|auto-trad|
|[MW0LxdUJZd9ozWtO.htm](age-of-ashes-bestiary-items/MW0LxdUJZd9ozWtO.htm)|Fast Healing 20|auto-trad|
|[mwEc6YhRZUrr9Qkb.htm](age-of-ashes-bestiary-items/mwEc6YhRZUrr9Qkb.htm)|Kick|auto-trad|
|[MxiNEcQrKuXmCuWD.htm](age-of-ashes-bestiary-items/MxiNEcQrKuXmCuWD.htm)|Fangs|auto-trad|
|[mXRRlIf5fS8NO5I1.htm](age-of-ashes-bestiary-items/mXRRlIf5fS8NO5I1.htm)|Woodland Stride|auto-trad|
|[MYgT9dmiyGDI48Hx.htm](age-of-ashes-bestiary-items/MYgT9dmiyGDI48Hx.htm)|Superlative Summoner|auto-trad|
|[MyJ5pEba9K3unoCm.htm](age-of-ashes-bestiary-items/MyJ5pEba9K3unoCm.htm)|Archery Experience|auto-trad|
|[MZs5uxec71OZQSzk.htm](age-of-ashes-bestiary-items/MZs5uxec71OZQSzk.htm)|Disrupting Cold Iron Dagger|auto-trad|
|[N01NwkU6GvutbIp7.htm](age-of-ashes-bestiary-items/N01NwkU6GvutbIp7.htm)|Shell Block|auto-trad|
|[N096jcv77bKpi9xA.htm](age-of-ashes-bestiary-items/N096jcv77bKpi9xA.htm)|Backshot|auto-trad|
|[n0JJ4L7jPOWqDejM.htm](age-of-ashes-bestiary-items/n0JJ4L7jPOWqDejM.htm)|Matriarch's Caress|auto-trad|
|[n2I1TMJLTQ65exGf.htm](age-of-ashes-bestiary-items/n2I1TMJLTQ65exGf.htm)|Arcane Innate Spells|auto-trad|
|[n327lTMbR7EaIPxV.htm](age-of-ashes-bestiary-items/n327lTMbR7EaIPxV.htm)|Darting Shot|auto-trad|
|[n32H8SDG3hQfZVNB.htm](age-of-ashes-bestiary-items/n32H8SDG3hQfZVNB.htm)|Instantaneous Movement|auto-trad|
|[n4acrtFiDu5TwYGe.htm](age-of-ashes-bestiary-items/n4acrtFiDu5TwYGe.htm)|Hammer the Chained|auto-trad|
|[N62QvxPwdPCrIlP1.htm](age-of-ashes-bestiary-items/N62QvxPwdPCrIlP1.htm)|Occult Spontaneous Spells|auto-trad|
|[n6SPErVyS2S3b3lv.htm](age-of-ashes-bestiary-items/n6SPErVyS2S3b3lv.htm)|Horn Snare|auto-trad|
|[N9ItpLBOPJW5l9DI.htm](age-of-ashes-bestiary-items/N9ItpLBOPJW5l9DI.htm)|Extra Reaction|auto-trad|
|[nA6MKkodX1XzMSli.htm](age-of-ashes-bestiary-items/nA6MKkodX1XzMSli.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[naVs815EnCcnoaW1.htm](age-of-ashes-bestiary-items/naVs815EnCcnoaW1.htm)|Drowning Drone|auto-trad|
|[nf1iMpVleTsR2jEV.htm](age-of-ashes-bestiary-items/nf1iMpVleTsR2jEV.htm)|Internal Furnace|auto-trad|
|[NfPbxqhBdeyeKfyS.htm](age-of-ashes-bestiary-items/NfPbxqhBdeyeKfyS.htm)|Weapon Master|auto-trad|
|[NgedLFfO7OJlxvqT.htm](age-of-ashes-bestiary-items/NgedLFfO7OJlxvqT.htm)|Frightful Presence|auto-trad|
|[NgHUvhpolU7B4yNd.htm](age-of-ashes-bestiary-items/NgHUvhpolU7B4yNd.htm)|Dazzling Display|auto-trad|
|[NI8nqs0x537N5LvI.htm](age-of-ashes-bestiary-items/NI8nqs0x537N5LvI.htm)|Tail|auto-trad|
|[NioStPlx80WxmDvR.htm](age-of-ashes-bestiary-items/NioStPlx80WxmDvR.htm)|Searing Heat|auto-trad|
|[nkAN9Dt2tljmS96M.htm](age-of-ashes-bestiary-items/nkAN9Dt2tljmS96M.htm)|Site Bound (Jewelgate Way Station)|auto-trad|
|[nKfHeaA4JlYimGVv.htm](age-of-ashes-bestiary-items/nKfHeaA4JlYimGVv.htm)|Occult Innate Spells|auto-trad|
|[NKkENtFd0bUL6yx7.htm](age-of-ashes-bestiary-items/NKkENtFd0bUL6yx7.htm)|Woodland Stride|auto-trad|
|[nMOYUQ60Uw7g4k0q.htm](age-of-ashes-bestiary-items/nMOYUQ60Uw7g4k0q.htm)|Flurry|auto-trad|
|[nnJ4kOGHM4Qct2PR.htm](age-of-ashes-bestiary-items/nnJ4kOGHM4Qct2PR.htm)|Steady Spellcasting|auto-trad|
|[NNXU0f8NQLPkYMva.htm](age-of-ashes-bestiary-items/NNXU0f8NQLPkYMva.htm)|Painsight|auto-trad|
|[nPeM50gZBykCKDxe.htm](age-of-ashes-bestiary-items/nPeM50gZBykCKDxe.htm)|Distant Ringing|auto-trad|
|[nPeylmjWQ7slkCXr.htm](age-of-ashes-bestiary-items/nPeylmjWQ7slkCXr.htm)|Teleportation Attachment|auto-trad|
|[nTtoBqYHxSoaPWuC.htm](age-of-ashes-bestiary-items/nTtoBqYHxSoaPWuC.htm)|Site Bound|auto-trad|
|[NttUPgbz0pGvaOnd.htm](age-of-ashes-bestiary-items/NttUPgbz0pGvaOnd.htm)|Low-Light Vision|auto-trad|
|[Nu8j6VpdlorVM75S.htm](age-of-ashes-bestiary-items/Nu8j6VpdlorVM75S.htm)|Designate Bellflower Crop|auto-trad|
|[nurvAKW6FmusUHRh.htm](age-of-ashes-bestiary-items/nurvAKW6FmusUHRh.htm)|Dagger|auto-trad|
|[nw4MbhXR5zCwPJBV.htm](age-of-ashes-bestiary-items/nw4MbhXR5zCwPJBV.htm)|Dagger|auto-trad|
|[nxnCWI2x0Zy4iWjR.htm](age-of-ashes-bestiary-items/nxnCWI2x0Zy4iWjR.htm)|Primal Innate Spells|auto-trad|
|[nycjIkuU99HnHnf9.htm](age-of-ashes-bestiary-items/nycjIkuU99HnHnf9.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[NYttSUeq7rE5Lwlt.htm](age-of-ashes-bestiary-items/NYttSUeq7rE5Lwlt.htm)|Rapier|auto-trad|
|[Nyz0P3ikAyq0ud3H.htm](age-of-ashes-bestiary-items/Nyz0P3ikAyq0ud3H.htm)|Thrown Weapon Mastery|auto-trad|
|[NZ4xdM6bKvntdTEa.htm](age-of-ashes-bestiary-items/NZ4xdM6bKvntdTEa.htm)|Fearsome Brute|auto-trad|
|[o0njxNAMbjPeC9AS.htm](age-of-ashes-bestiary-items/o0njxNAMbjPeC9AS.htm)|Cataclysmic Rain|auto-trad|
|[o10Yz3qVKjeI4Fxz.htm](age-of-ashes-bestiary-items/o10Yz3qVKjeI4Fxz.htm)|Breath Weapon|auto-trad|
|[o1edk89NwTsVdvC1.htm](age-of-ashes-bestiary-items/o1edk89NwTsVdvC1.htm)|Rejuvenation|auto-trad|
|[o4fl6Wqm7Pupv1R8.htm](age-of-ashes-bestiary-items/o4fl6Wqm7Pupv1R8.htm)|Tail|auto-trad|
|[O4Vu0lAef9YMzn23.htm](age-of-ashes-bestiary-items/O4Vu0lAef9YMzn23.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[O5uoqD3Lzqcdx708.htm](age-of-ashes-bestiary-items/O5uoqD3Lzqcdx708.htm)|Wizard School Spells|auto-trad|
|[O6DYo04qm4tluUeG.htm](age-of-ashes-bestiary-items/O6DYo04qm4tluUeG.htm)|Spine Volley|auto-trad|
|[o8XSgCFe5Tgc8VwW.htm](age-of-ashes-bestiary-items/o8XSgCFe5Tgc8VwW.htm)|Vulnerable to Stone to Flesh|auto-trad|
|[o9eWNluFGesUiB9v.htm](age-of-ashes-bestiary-items/o9eWNluFGesUiB9v.htm)|Draconic Frenzy|auto-trad|
|[O9V5OSgRwAX3gEBH.htm](age-of-ashes-bestiary-items/O9V5OSgRwAX3gEBH.htm)|Grab|auto-trad|
|[OBpAWIEWPW9DSQMx.htm](age-of-ashes-bestiary-items/OBpAWIEWPW9DSQMx.htm)|Longbow|auto-trad|
|[oBtQcDrceotbFbrk.htm](age-of-ashes-bestiary-items/oBtQcDrceotbFbrk.htm)|Constant Spells|auto-trad|
|[oBv9CmgE9lFftp0E.htm](age-of-ashes-bestiary-items/oBv9CmgE9lFftp0E.htm)|Swiftness|auto-trad|
|[obvyw8sjJyDFGLDy.htm](age-of-ashes-bestiary-items/obvyw8sjJyDFGLDy.htm)|Infused Items|auto-trad|
|[OC3Cenu8w3vfySOp.htm](age-of-ashes-bestiary-items/OC3Cenu8w3vfySOp.htm)|Twisting Tail|auto-trad|
|[Od7nphkGvQM8Iv5L.htm](age-of-ashes-bestiary-items/Od7nphkGvQM8Iv5L.htm)|Blue Eye Beam|auto-trad|
|[oDodI8lhFK3QYtHx.htm](age-of-ashes-bestiary-items/oDodI8lhFK3QYtHx.htm)|Indigo Eye Beam|auto-trad|
|[ODRNG6vG9zjC3Etf.htm](age-of-ashes-bestiary-items/ODRNG6vG9zjC3Etf.htm)|Rend|auto-trad|
|[OfIb5QtIPTTftNlq.htm](age-of-ashes-bestiary-items/OfIb5QtIPTTftNlq.htm)|Low-Light Vision|auto-trad|
|[OHnvdksMNZmChe4c.htm](age-of-ashes-bestiary-items/OHnvdksMNZmChe4c.htm)|Light Hammer|auto-trad|
|[oIJ6ppXsMXZMBfSi.htm](age-of-ashes-bestiary-items/oIJ6ppXsMXZMBfSi.htm)|Liberation Vulnerability|auto-trad|
|[Okk4imbIUOwkYs3E.htm](age-of-ashes-bestiary-items/Okk4imbIUOwkYs3E.htm)|Hunter's Aim|auto-trad|
|[OLb44HyPmPQ1A0Sm.htm](age-of-ashes-bestiary-items/OLb44HyPmPQ1A0Sm.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[OlZeNmmphsIzWVz5.htm](age-of-ashes-bestiary-items/OlZeNmmphsIzWVz5.htm)|Composite Shortbow|auto-trad|
|[OM9iuNqyLKsh2ZjP.htm](age-of-ashes-bestiary-items/OM9iuNqyLKsh2ZjP.htm)|Darkvision|auto-trad|
|[OmjmOPy2aa488z2d.htm](age-of-ashes-bestiary-items/OmjmOPy2aa488z2d.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Omu620MVyXotE5Nc.htm](age-of-ashes-bestiary-items/Omu620MVyXotE5Nc.htm)|Breath Weapon|auto-trad|
|[OOmkcTVG03cJ0cT3.htm](age-of-ashes-bestiary-items/OOmkcTVG03cJ0cT3.htm)|Shield Block|auto-trad|
|[oPh7RYiHA3OguL9Z.htm](age-of-ashes-bestiary-items/oPh7RYiHA3OguL9Z.htm)|Attack of Opportunity|auto-trad|
|[OqoxUu62vH7mcrPs.htm](age-of-ashes-bestiary-items/OqoxUu62vH7mcrPs.htm)|Terrifying Stare|auto-trad|
|[Oqy1IXCABemD4A9g.htm](age-of-ashes-bestiary-items/Oqy1IXCABemD4A9g.htm)|Sneak Attack|auto-trad|
|[OSgjqd6Z5NewzIw4.htm](age-of-ashes-bestiary-items/OSgjqd6Z5NewzIw4.htm)|Focus Spells|auto-trad|
|[oUbNiZbQMyGjNTIj.htm](age-of-ashes-bestiary-items/oUbNiZbQMyGjNTIj.htm)|Reactionary|auto-trad|
|[OvKpoweI2x9CioBO.htm](age-of-ashes-bestiary-items/OvKpoweI2x9CioBO.htm)|Overwhelming Energy|auto-trad|
|[OVyCsNi5aandXp9w.htm](age-of-ashes-bestiary-items/OVyCsNi5aandXp9w.htm)|Hunt Prey|auto-trad|
|[owejjJWuMlBx46tR.htm](age-of-ashes-bestiary-items/owejjJWuMlBx46tR.htm)|Whiplash|auto-trad|
|[OWhmwuZtv80QG0wy.htm](age-of-ashes-bestiary-items/OWhmwuZtv80QG0wy.htm)|Surprise Attack|auto-trad|
|[OwoH6FLZ94ihNvEv.htm](age-of-ashes-bestiary-items/OwoH6FLZ94ihNvEv.htm)|Returning Light Hammer|auto-trad|
|[OzVIHvIPV38rnIVv.htm](age-of-ashes-bestiary-items/OzVIHvIPV38rnIVv.htm)|Darkvision|auto-trad|
|[ozZqtI8i4QAUvyoA.htm](age-of-ashes-bestiary-items/ozZqtI8i4QAUvyoA.htm)|Primal Prepared Spells|auto-trad|
|[p1bvrj2qMZFdHpsv.htm](age-of-ashes-bestiary-items/p1bvrj2qMZFdHpsv.htm)|Self-Repair|auto-trad|
|[P54JJ58qynLo2URI.htm](age-of-ashes-bestiary-items/P54JJ58qynLo2URI.htm)|Breath Weapon|auto-trad|
|[p5rguZk07i9usCOe.htm](age-of-ashes-bestiary-items/p5rguZk07i9usCOe.htm)|Dagger|auto-trad|
|[p9DHIoccyeBea4Yd.htm](age-of-ashes-bestiary-items/p9DHIoccyeBea4Yd.htm)|Twin Shot|auto-trad|
|[pb3yDpk6Rl0BuY7j.htm](age-of-ashes-bestiary-items/pb3yDpk6Rl0BuY7j.htm)|Arcane Innate Spells|auto-trad|
|[PcDH3sH6RU74yewe.htm](age-of-ashes-bestiary-items/PcDH3sH6RU74yewe.htm)|Liberating Trick|auto-trad|
|[PcLLTu6aeVnY2Ogg.htm](age-of-ashes-bestiary-items/PcLLTu6aeVnY2Ogg.htm)|Claw|auto-trad|
|[pDkq3pcVeqspt9o5.htm](age-of-ashes-bestiary-items/pDkq3pcVeqspt9o5.htm)|Darkvision|auto-trad|
|[PdMypFZGmGTQBUO7.htm](age-of-ashes-bestiary-items/PdMypFZGmGTQBUO7.htm)|Door|auto-trad|
|[pdp1XYZ1mtcsTvM3.htm](age-of-ashes-bestiary-items/pdp1XYZ1mtcsTvM3.htm)|Prepared Arcane Spells|auto-trad|
|[peAxpqZ3KlwaPcjL.htm](age-of-ashes-bestiary-items/peAxpqZ3KlwaPcjL.htm)|Paralyzing Force|auto-trad|
|[PemrcXjJFtKQ8192.htm](age-of-ashes-bestiary-items/PemrcXjJFtKQ8192.htm)|Shortsword|auto-trad|
|[peSYmPiuqBy9OU2r.htm](age-of-ashes-bestiary-items/peSYmPiuqBy9OU2r.htm)|Change Shape|auto-trad|
|[pF8fcF4d3JB5eI7V.htm](age-of-ashes-bestiary-items/pF8fcF4d3JB5eI7V.htm)|Darkvision|auto-trad|
|[pfkbWfIp6yg9hS9F.htm](age-of-ashes-bestiary-items/pfkbWfIp6yg9hS9F.htm)|Shortsword|auto-trad|
|[pFweIlOkcH38fuxf.htm](age-of-ashes-bestiary-items/pFweIlOkcH38fuxf.htm)|Spikes|auto-trad|
|[pHsMHzt7Z1sM5ScM.htm](age-of-ashes-bestiary-items/pHsMHzt7Z1sM5ScM.htm)|Tongue|auto-trad|
|[PhUe9NLaMWI1oIUJ.htm](age-of-ashes-bestiary-items/PhUe9NLaMWI1oIUJ.htm)|Halberd|auto-trad|
|[pIKbKs8nTXF2nUAm.htm](age-of-ashes-bestiary-items/pIKbKs8nTXF2nUAm.htm)|Fiend Summoner|auto-trad|
|[pjPtT8ePki7wgMaO.htm](age-of-ashes-bestiary-items/pjPtT8ePki7wgMaO.htm)|Swift Leap|auto-trad|
|[plcsZ25Y6jlruAMn.htm](age-of-ashes-bestiary-items/plcsZ25Y6jlruAMn.htm)|Twin Parry|auto-trad|
|[pLURCvEu8rYktiZE.htm](age-of-ashes-bestiary-items/pLURCvEu8rYktiZE.htm)|Paralyzing Touch|auto-trad|
|[pmGTuXbF38zbcznu.htm](age-of-ashes-bestiary-items/pmGTuXbF38zbcznu.htm)|Claw|auto-trad|
|[PmP7kVHHTCVo9E2y.htm](age-of-ashes-bestiary-items/PmP7kVHHTCVo9E2y.htm)|Longsword|auto-trad|
|[PMvxB0zhxiZNDxxW.htm](age-of-ashes-bestiary-items/PMvxB0zhxiZNDxxW.htm)|Necrotic Field|auto-trad|
|[ppHf9s7R9QhMXyxE.htm](age-of-ashes-bestiary-items/ppHf9s7R9QhMXyxE.htm)|Draconic Momentum|auto-trad|
|[ppTxZD2ni4N12liM.htm](age-of-ashes-bestiary-items/ppTxZD2ni4N12liM.htm)|Fangs|auto-trad|
|[pqOaIKfZFro1QcD2.htm](age-of-ashes-bestiary-items/pqOaIKfZFro1QcD2.htm)|Shrieking Frenzy|auto-trad|
|[PrMrxrT6XDC0RiYK.htm](age-of-ashes-bestiary-items/PrMrxrT6XDC0RiYK.htm)|Thrown Weapon Mastery|auto-trad|
|[Ps0fTafFUadvtLAi.htm](age-of-ashes-bestiary-items/Ps0fTafFUadvtLAi.htm)|Efficient Capture|auto-trad|
|[PT2MUTRJtcohFtP9.htm](age-of-ashes-bestiary-items/PT2MUTRJtcohFtP9.htm)|Lava Bomb|auto-trad|
|[pTHZcKbaFZmIwmOo.htm](age-of-ashes-bestiary-items/pTHZcKbaFZmIwmOo.htm)|Occult Prepared Spells|auto-trad|
|[PTLfrf6aBtRzqBS1.htm](age-of-ashes-bestiary-items/PTLfrf6aBtRzqBS1.htm)|Nolly's Hoe|auto-trad|
|[pvaF5cf0XkH3cCbK.htm](age-of-ashes-bestiary-items/pvaF5cf0XkH3cCbK.htm)|Claw|auto-trad|
|[pWNu3N2Ovu0m978Z.htm](age-of-ashes-bestiary-items/pWNu3N2Ovu0m978Z.htm)|Blood Quarry|auto-trad|
|[PWOiCJ1IBh9T12ba.htm](age-of-ashes-bestiary-items/PWOiCJ1IBh9T12ba.htm)|Darkvision|auto-trad|
|[Py6UVHQQNzpI6Lww.htm](age-of-ashes-bestiary-items/Py6UVHQQNzpI6Lww.htm)|Soul Chain|auto-trad|
|[py7MzheoXU7EEjNA.htm](age-of-ashes-bestiary-items/py7MzheoXU7EEjNA.htm)|Negative Healing|auto-trad|
|[pYNPdghXNuSzUUUS.htm](age-of-ashes-bestiary-items/pYNPdghXNuSzUUUS.htm)|Light Blindness|auto-trad|
|[pzO0JwhFqVcRKzAN.htm](age-of-ashes-bestiary-items/pzO0JwhFqVcRKzAN.htm)|Giant Tarantula Venom|auto-trad|
|[q1GTtW7ypFaAaLpl.htm](age-of-ashes-bestiary-items/q1GTtW7ypFaAaLpl.htm)|Shield Block|auto-trad|
|[Q2kYqdtBur9KmjY3.htm](age-of-ashes-bestiary-items/Q2kYqdtBur9KmjY3.htm)|Jaws|auto-trad|
|[Q2ShfAoB91gQzS6V.htm](age-of-ashes-bestiary-items/Q2ShfAoB91gQzS6V.htm)|Divine Rituals|auto-trad|
|[Q6x7Lrplu7fSsiYf.htm](age-of-ashes-bestiary-items/Q6x7Lrplu7fSsiYf.htm)|Darkvision|auto-trad|
|[q90PCZCTBqoxOX62.htm](age-of-ashes-bestiary-items/q90PCZCTBqoxOX62.htm)|Jaws|auto-trad|
|[Qbd7q1jTcWs61OsF.htm](age-of-ashes-bestiary-items/Qbd7q1jTcWs61OsF.htm)|Hail of Arrows|auto-trad|
|[QD3U7kcL3GCOpa3y.htm](age-of-ashes-bestiary-items/QD3U7kcL3GCOpa3y.htm)|Smoke Vision|auto-trad|
|[qdRHgb9PmpqQ2jTG.htm](age-of-ashes-bestiary-items/qdRHgb9PmpqQ2jTG.htm)|Quick Draw|auto-trad|
|[qfAZ12H9DKd8pEad.htm](age-of-ashes-bestiary-items/qfAZ12H9DKd8pEad.htm)|Shell Defense|auto-trad|
|[QFVHYWU00lctwBwf.htm](age-of-ashes-bestiary-items/QFVHYWU00lctwBwf.htm)|Change Shape|auto-trad|
|[QhwSqyQ7YKPRITDw.htm](age-of-ashes-bestiary-items/QhwSqyQ7YKPRITDw.htm)|Attack of Opportunity|auto-trad|
|[qi5FtOzBWTiiCStB.htm](age-of-ashes-bestiary-items/qi5FtOzBWTiiCStB.htm)|Explosion|auto-trad|
|[QKmae867GS9N8IKK.htm](age-of-ashes-bestiary-items/QKmae867GS9N8IKK.htm)|Darkvision|auto-trad|
|[qLKyo9iiDXWWqXvs.htm](age-of-ashes-bestiary-items/qLKyo9iiDXWWqXvs.htm)|Grab|auto-trad|
|[QmrJQN5wGkBpsqMo.htm](age-of-ashes-bestiary-items/QmrJQN5wGkBpsqMo.htm)|Bard Composition Spells|auto-trad|
|[qnM0VhMof6zWC5w1.htm](age-of-ashes-bestiary-items/qnM0VhMof6zWC5w1.htm)|Claw|auto-trad|
|[QPPzshzEbERB3TmQ.htm](age-of-ashes-bestiary-items/QPPzshzEbERB3TmQ.htm)|Divine Prepared Spells|auto-trad|
|[Qqwiu5T6ag0OjmLs.htm](age-of-ashes-bestiary-items/Qqwiu5T6ag0OjmLs.htm)|Darkvision|auto-trad|
|[QStNE8d3elauT8lt.htm](age-of-ashes-bestiary-items/QStNE8d3elauT8lt.htm)|Silver Dagger|auto-trad|
|[QsuzBxkK6bf7oS8S.htm](age-of-ashes-bestiary-items/QsuzBxkK6bf7oS8S.htm)|Captivate|auto-trad|
|[QtbetvkizVrPlsdb.htm](age-of-ashes-bestiary-items/QtbetvkizVrPlsdb.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[qtiwr8Rp5Y4kIa7O.htm](age-of-ashes-bestiary-items/qtiwr8Rp5Y4kIa7O.htm)|Quick Alchemy|auto-trad|
|[QVKGD3jLsVMeTZzu.htm](age-of-ashes-bestiary-items/QVKGD3jLsVMeTZzu.htm)|Surprise Strike|auto-trad|
|[Qw4pImBH1KPofsS1.htm](age-of-ashes-bestiary-items/Qw4pImBH1KPofsS1.htm)|Assemble Choir|auto-trad|
|[QWZ2jVcqi9CN4U6I.htm](age-of-ashes-bestiary-items/QWZ2jVcqi9CN4U6I.htm)|Sneak Attack|auto-trad|
|[r0yOc789JUXP2zpN.htm](age-of-ashes-bestiary-items/r0yOc789JUXP2zpN.htm)|Aluum Antimagic|auto-trad|
|[R9ndNOt0KSVZBrCG.htm](age-of-ashes-bestiary-items/R9ndNOt0KSVZBrCG.htm)|Nimble Dodge|auto-trad|
|[RBix0BOrbHGTntW9.htm](age-of-ashes-bestiary-items/RBix0BOrbHGTntW9.htm)|Dragonstorm Aura|auto-trad|
|[RF1LobyvELbvZKdf.htm](age-of-ashes-bestiary-items/RF1LobyvELbvZKdf.htm)|Wings|auto-trad|
|[Rg3ygnHjRloP7FoD.htm](age-of-ashes-bestiary-items/Rg3ygnHjRloP7FoD.htm)|Arcane Innate Spells|auto-trad|
|[rGBruApiTZDc1HuB.htm](age-of-ashes-bestiary-items/rGBruApiTZDc1HuB.htm)|Redirect Energy|auto-trad|
|[RgYma4kYCK90TSJD.htm](age-of-ashes-bestiary-items/RgYma4kYCK90TSJD.htm)|Imprisoned|auto-trad|
|[RhOO7QphVzTN8Y2j.htm](age-of-ashes-bestiary-items/RhOO7QphVzTN8Y2j.htm)|Furious Claws|auto-trad|
|[rI6uxFg6LJxzLDta.htm](age-of-ashes-bestiary-items/rI6uxFg6LJxzLDta.htm)|Sneak Attack|auto-trad|
|[riLQEjTb79Im6cTV.htm](age-of-ashes-bestiary-items/riLQEjTb79Im6cTV.htm)|Tongue Pull|auto-trad|
|[RiXHSOTCKfKIONvP.htm](age-of-ashes-bestiary-items/RiXHSOTCKfKIONvP.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[RjIX3v1EU84c920i.htm](age-of-ashes-bestiary-items/RjIX3v1EU84c920i.htm)|Breath Snatcher|auto-trad|
|[rJLMQhvHJTZBujHg.htm](age-of-ashes-bestiary-items/rJLMQhvHJTZBujHg.htm)|Frightful Presence|auto-trad|
|[RJtBHxR2U04jtD7I.htm](age-of-ashes-bestiary-items/RJtBHxR2U04jtD7I.htm)|Destructive Explosion|auto-trad|
|[RKUjuuHr7izDXZqd.htm](age-of-ashes-bestiary-items/RKUjuuHr7izDXZqd.htm)|Reach Spell|auto-trad|
|[RmNG2smIjmn2isvO.htm](age-of-ashes-bestiary-items/RmNG2smIjmn2isvO.htm)|Deny Advantage|auto-trad|
|[rOBeT7cNsBIJSzT7.htm](age-of-ashes-bestiary-items/rOBeT7cNsBIJSzT7.htm)|Cleric Domain Spells|auto-trad|
|[rqq8NjCeDPoInPTq.htm](age-of-ashes-bestiary-items/rqq8NjCeDPoInPTq.htm)|Arcane Innate Spells|auto-trad|
|[rRrTo5REtGDduBlv.htm](age-of-ashes-bestiary-items/rRrTo5REtGDduBlv.htm)|Fangs|auto-trad|
|[rU9gIiMETSDDnipL.htm](age-of-ashes-bestiary-items/rU9gIiMETSDDnipL.htm)|Tail|auto-trad|
|[rUMDjgQAaue8rA2w.htm](age-of-ashes-bestiary-items/rUMDjgQAaue8rA2w.htm)|Regeneration 15 (Deactivated by Good)|auto-trad|
|[RwCa647AJFm8BWJR.htm](age-of-ashes-bestiary-items/RwCa647AJFm8BWJR.htm)|Darkvision|auto-trad|
|[rWg63VpAt6JzaV7z.htm](age-of-ashes-bestiary-items/rWg63VpAt6JzaV7z.htm)|Wing|auto-trad|
|[RwOAEo9R3fFzTJY7.htm](age-of-ashes-bestiary-items/RwOAEo9R3fFzTJY7.htm)|Surprise Attack|auto-trad|
|[ryLB6NeDkc2PFBmb.htm](age-of-ashes-bestiary-items/ryLB6NeDkc2PFBmb.htm)|Carapace|auto-trad|
|[rZ3NKdEarhgXFXmc.htm](age-of-ashes-bestiary-items/rZ3NKdEarhgXFXmc.htm)|Catch Rock|auto-trad|
|[RzjCNg6HpRoCBHoz.htm](age-of-ashes-bestiary-items/RzjCNg6HpRoCBHoz.htm)|Jungle Stride|auto-trad|
|[s2CoTcNYzppH89ej.htm](age-of-ashes-bestiary-items/s2CoTcNYzppH89ej.htm)|Cinderclaw Gauntlet|auto-trad|
|[S5Inn9EP3p8RwITB.htm](age-of-ashes-bestiary-items/S5Inn9EP3p8RwITB.htm)|Warded Casting|auto-trad|
|[s5L6PEZRqvHlBdWg.htm](age-of-ashes-bestiary-items/s5L6PEZRqvHlBdWg.htm)|Rejuvenation|auto-trad|
|[saqRnmZhtXKwqFed.htm](age-of-ashes-bestiary-items/saqRnmZhtXKwqFed.htm)|Demilich Eye Gems|auto-trad|
|[sBHiaipaEIdoNG9q.htm](age-of-ashes-bestiary-items/sBHiaipaEIdoNG9q.htm)|Telekinetic Storm|auto-trad|
|[SBQifbHhjnwomydG.htm](age-of-ashes-bestiary-items/SBQifbHhjnwomydG.htm)|Darkvision|auto-trad|
|[sc3daf0IgUPTb2rM.htm](age-of-ashes-bestiary-items/sc3daf0IgUPTb2rM.htm)|Greater Frost Vial|auto-trad|
|[SdtmvPS7LAUlE41O.htm](age-of-ashes-bestiary-items/SdtmvPS7LAUlE41O.htm)|Swamp Stride|auto-trad|
|[seHBkvEFgGTlfAtt.htm](age-of-ashes-bestiary-items/seHBkvEFgGTlfAtt.htm)|Dagger|auto-trad|
|[sEn2RRTxyiFjpqfu.htm](age-of-ashes-bestiary-items/sEn2RRTxyiFjpqfu.htm)|Resonant Wail|auto-trad|
|[SH0nk4BfgI1ur3oJ.htm](age-of-ashes-bestiary-items/SH0nk4BfgI1ur3oJ.htm)|Unbalancing Blow|auto-trad|
|[SH5975wJdafMupdy.htm](age-of-ashes-bestiary-items/SH5975wJdafMupdy.htm)|Rejuvenation|auto-trad|
|[shtsWYCyenHOUURJ.htm](age-of-ashes-bestiary-items/shtsWYCyenHOUURJ.htm)|Primal Rituals|auto-trad|
|[SHZi101TqFOYWlIL.htm](age-of-ashes-bestiary-items/SHZi101TqFOYWlIL.htm)|Green Eye Beam|auto-trad|
|[SJg4gN7fByvyyHi4.htm](age-of-ashes-bestiary-items/SJg4gN7fByvyyHi4.htm)|Sneak Attack|auto-trad|
|[sjs5I4Em4t7kFOwK.htm](age-of-ashes-bestiary-items/sjs5I4Em4t7kFOwK.htm)|Violet Eye Beam|auto-trad|
|[SJvjp5tv9lMwcTyw.htm](age-of-ashes-bestiary-items/SJvjp5tv9lMwcTyw.htm)|Weapon Trick|auto-trad|
|[sKH86qpUBKLIW8ls.htm](age-of-ashes-bestiary-items/sKH86qpUBKLIW8ls.htm)|Efficient Capture|auto-trad|
|[skQ2ydydslD2F8Br.htm](age-of-ashes-bestiary-items/skQ2ydydslD2F8Br.htm)|Fist|auto-trad|
|[SkzgfQVojElPeAh6.htm](age-of-ashes-bestiary-items/SkzgfQVojElPeAh6.htm)|Breath Weapon|auto-trad|
|[sLKipq0eV2QIdP0P.htm](age-of-ashes-bestiary-items/sLKipq0eV2QIdP0P.htm)|Consume Flesh|auto-trad|
|[sLQqwa6hoaw69U93.htm](age-of-ashes-bestiary-items/sLQqwa6hoaw69U93.htm)|Drain Bonded Item|auto-trad|
|[SlVqql8huNGs1yPM.htm](age-of-ashes-bestiary-items/SlVqql8huNGs1yPM.htm)|Horns|auto-trad|
|[SmEfn9FnR2kxlXyr.htm](age-of-ashes-bestiary-items/SmEfn9FnR2kxlXyr.htm)|Sneak Attack|auto-trad|
|[SNejKUlOFhtJWGTZ.htm](age-of-ashes-bestiary-items/SNejKUlOFhtJWGTZ.htm)|Attack of Opportunity|auto-trad|
|[Sohj5I5HF2aiJ17e.htm](age-of-ashes-bestiary-items/Sohj5I5HF2aiJ17e.htm)|Yellow Eye Beam|auto-trad|
|[sQsoukNeadSMEKWS.htm](age-of-ashes-bestiary-items/sQsoukNeadSMEKWS.htm)|Beak|auto-trad|
|[sRUKk7rcYO0y4CrH.htm](age-of-ashes-bestiary-items/sRUKk7rcYO0y4CrH.htm)|Razor Sharp|auto-trad|
|[Supu8qO5sZ5gfG4U.htm](age-of-ashes-bestiary-items/Supu8qO5sZ5gfG4U.htm)|Claw|auto-trad|
|[SV6WrRtaq5OJtKUq.htm](age-of-ashes-bestiary-items/SV6WrRtaq5OJtKUq.htm)|Hampering Shot|auto-trad|
|[SW6CqLFVtIpxlZcy.htm](age-of-ashes-bestiary-items/SW6CqLFVtIpxlZcy.htm)|Major Acid Flask|auto-trad|
|[swIJOeM7FXkgQV1H.htm](age-of-ashes-bestiary-items/swIJOeM7FXkgQV1H.htm)|Jaws|auto-trad|
|[SxjIbhY3Md3bS3dx.htm](age-of-ashes-bestiary-items/SxjIbhY3Md3bS3dx.htm)|Morningstar|auto-trad|
|[sXWZxvJlU7LZrpHC.htm](age-of-ashes-bestiary-items/sXWZxvJlU7LZrpHC.htm)|Orange Eye Beam|auto-trad|
|[t00h5GlGA9LELf56.htm](age-of-ashes-bestiary-items/t00h5GlGA9LELf56.htm)|Soul Chain|auto-trad|
|[T1sHFc7JoaRRC0iK.htm](age-of-ashes-bestiary-items/T1sHFc7JoaRRC0iK.htm)|Claw|auto-trad|
|[T2qx3jfSrArHamOu.htm](age-of-ashes-bestiary-items/T2qx3jfSrArHamOu.htm)|Rituals|auto-trad|
|[T3t1bWcAoYCVuiiE.htm](age-of-ashes-bestiary-items/T3t1bWcAoYCVuiiE.htm)|Drain Soul Cage|auto-trad|
|[t5AqMVMcvrvoUCcN.htm](age-of-ashes-bestiary-items/t5AqMVMcvrvoUCcN.htm)|Necrotic Affliction|auto-trad|
|[T82SzULUe8LH0NIZ.htm](age-of-ashes-bestiary-items/T82SzULUe8LH0NIZ.htm)|Split|auto-trad|
|[TBCoGqPy0fhFmpMq.htm](age-of-ashes-bestiary-items/TBCoGqPy0fhFmpMq.htm)|Twin Takedown|auto-trad|
|[tBIBeHhWxRinsTO5.htm](age-of-ashes-bestiary-items/tBIBeHhWxRinsTO5.htm)|Soul Binder|auto-trad|
|[TbMZhJ1evBpaauR7.htm](age-of-ashes-bestiary-items/TbMZhJ1evBpaauR7.htm)|Create Shadow Sanctuary|auto-trad|
|[tCcBQS2TNgo7EIvB.htm](age-of-ashes-bestiary-items/tCcBQS2TNgo7EIvB.htm)|Efficient Capture|auto-trad|
|[tdbB9sBzDpFxgzUz.htm](age-of-ashes-bestiary-items/tdbB9sBzDpFxgzUz.htm)|Darkvision|auto-trad|
|[TF94bqW7ak7QdxKX.htm](age-of-ashes-bestiary-items/TF94bqW7ak7QdxKX.htm)|Efficient Capture|auto-trad|
|[tfAQ1l4sV2c1psLD.htm](age-of-ashes-bestiary-items/tfAQ1l4sV2c1psLD.htm)|Precise Debilitations|auto-trad|
|[Thxxg6IpvoOEOFZu.htm](age-of-ashes-bestiary-items/Thxxg6IpvoOEOFZu.htm)|Scoff at the Divine|auto-trad|
|[tHzRquAwQcwKEsgJ.htm](age-of-ashes-bestiary-items/tHzRquAwQcwKEsgJ.htm)|Superior Pack Attack|auto-trad|
|[TIhbTdeTcedCd2mp.htm](age-of-ashes-bestiary-items/TIhbTdeTcedCd2mp.htm)|Low-Light Vision|auto-trad|
|[TJWwRY8dDZB5iZvQ.htm](age-of-ashes-bestiary-items/TJWwRY8dDZB5iZvQ.htm)|Eerie Flexibility|auto-trad|
|[tKTardJEDrHVNygR.htm](age-of-ashes-bestiary-items/tKTardJEDrHVNygR.htm)|Darkvision|auto-trad|
|[tl2R3rnbspKfWaNX.htm](age-of-ashes-bestiary-items/tl2R3rnbspKfWaNX.htm)|Darkvision|auto-trad|
|[tmioSSIeBsfcu9eY.htm](age-of-ashes-bestiary-items/tmioSSIeBsfcu9eY.htm)|5-6 Bolts of Lightning|auto-trad|
|[To3ttdceko61OtLl.htm](age-of-ashes-bestiary-items/To3ttdceko61OtLl.htm)|Breath Weapon|auto-trad|
|[TOo0Vq8jXzgZHSGq.htm](age-of-ashes-bestiary-items/TOo0Vq8jXzgZHSGq.htm)|At-Will Spells|auto-trad|
|[TpYZfRAOyWTWC5du.htm](age-of-ashes-bestiary-items/TpYZfRAOyWTWC5du.htm)|Quick Movements|auto-trad|
|[TsPUS6z43vjlNNkp.htm](age-of-ashes-bestiary-items/TsPUS6z43vjlNNkp.htm)|Primal Innate Spells|auto-trad|
|[TSSV8a0oKTioJrDT.htm](age-of-ashes-bestiary-items/TSSV8a0oKTioJrDT.htm)|Spectral Fist|auto-trad|
|[TTHC9vPRacUfSJco.htm](age-of-ashes-bestiary-items/TTHC9vPRacUfSJco.htm)|Crystallize|auto-trad|
|[TU1DbG3ubOh1bDcw.htm](age-of-ashes-bestiary-items/TU1DbG3ubOh1bDcw.htm)|Composite Shortbow|auto-trad|
|[TupueOehWDuKS2G5.htm](age-of-ashes-bestiary-items/TupueOehWDuKS2G5.htm)|Shortbow|auto-trad|
|[tvo7UaALlpq5xWzP.htm](age-of-ashes-bestiary-items/tvo7UaALlpq5xWzP.htm)|Halfling Luck|auto-trad|
|[TW4r3ip5ObjvYZMs.htm](age-of-ashes-bestiary-items/TW4r3ip5ObjvYZMs.htm)|Shield Spikes|auto-trad|
|[txRGabDLLfoByLm2.htm](age-of-ashes-bestiary-items/txRGabDLLfoByLm2.htm)|Thundering Maul|auto-trad|
|[u3F1k0BPoP05Xfzn.htm](age-of-ashes-bestiary-items/u3F1k0BPoP05Xfzn.htm)|Scent (Imprecise) 120 feet|auto-trad|
|[U62hbZDWfFSaAj64.htm](age-of-ashes-bestiary-items/U62hbZDWfFSaAj64.htm)|Smoke Vision|auto-trad|
|[U6Wwg69lSYJAW46L.htm](age-of-ashes-bestiary-items/U6Wwg69lSYJAW46L.htm)|Constant Spells|auto-trad|
|[uapfpdQSjUz13nBn.htm](age-of-ashes-bestiary-items/uapfpdQSjUz13nBn.htm)|Eye Beam|auto-trad|
|[uAtJgY4Z2GeiZYOi.htm](age-of-ashes-bestiary-items/uAtJgY4Z2GeiZYOi.htm)|No MAP|auto-trad|
|[ubsRZqqQfGfMpXdi.htm](age-of-ashes-bestiary-items/ubsRZqqQfGfMpXdi.htm)|Occult Innate Spells|auto-trad|
|[UcUzuyj7o64QhvCg.htm](age-of-ashes-bestiary-items/UcUzuyj7o64QhvCg.htm)|Dagger|auto-trad|
|[UDRLkUU6I38waiLi.htm](age-of-ashes-bestiary-items/UDRLkUU6I38waiLi.htm)|Tail Swipe|auto-trad|
|[UDx53YecyYB7KIrJ.htm](age-of-ashes-bestiary-items/UDx53YecyYB7KIrJ.htm)|Anadi Venom|auto-trad|
|[uf0REXRCA5Os0He4.htm](age-of-ashes-bestiary-items/uf0REXRCA5Os0He4.htm)|Claw|auto-trad|
|[Uf6zmBkqRIkAQTiY.htm](age-of-ashes-bestiary-items/Uf6zmBkqRIkAQTiY.htm)|Claw|auto-trad|
|[UfanUBQ8Zs0ulv4e.htm](age-of-ashes-bestiary-items/UfanUBQ8Zs0ulv4e.htm)|Frightful Presence|auto-trad|
|[UhafuYzXLYPCHxPD.htm](age-of-ashes-bestiary-items/UhafuYzXLYPCHxPD.htm)|Low-Light Vision|auto-trad|
|[uIFB2aU5gYZC7XXL.htm](age-of-ashes-bestiary-items/uIFB2aU5gYZC7XXL.htm)|Innate Arcane Spells|auto-trad|
|[uiKPE6MQz8FAQvOx.htm](age-of-ashes-bestiary-items/uiKPE6MQz8FAQvOx.htm)|Shield Warden|auto-trad|
|[UIvPyDXGpNjhmqBZ.htm](age-of-ashes-bestiary-items/UIvPyDXGpNjhmqBZ.htm)|Hatchet|auto-trad|
|[ulyeBuv2PkM39FKm.htm](age-of-ashes-bestiary-items/ulyeBuv2PkM39FKm.htm)|Arcane Innate Spells|auto-trad|
|[umJV0GGKKxt5cyGw.htm](age-of-ashes-bestiary-items/umJV0GGKKxt5cyGw.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[UOfQIWygJYcqT1kQ.htm](age-of-ashes-bestiary-items/UOfQIWygJYcqT1kQ.htm)|Mobility|auto-trad|
|[UQaty56jSFbPq0pG.htm](age-of-ashes-bestiary-items/UQaty56jSFbPq0pG.htm)|Bite|auto-trad|
|[UQInVEZyWsihDZJI.htm](age-of-ashes-bestiary-items/UQInVEZyWsihDZJI.htm)|Drain Bonded Item|auto-trad|
|[UQnCKGq6QIQn8xKq.htm](age-of-ashes-bestiary-items/UQnCKGq6QIQn8xKq.htm)|Greatsword|auto-trad|
|[uR0sxFBmOFXxYqZi.htm](age-of-ashes-bestiary-items/uR0sxFBmOFXxYqZi.htm)|Claw|auto-trad|
|[ur5ordRthB6Bqyei.htm](age-of-ashes-bestiary-items/ur5ordRthB6Bqyei.htm)|Grab|auto-trad|
|[Ut7DWly2y0Fghbxh.htm](age-of-ashes-bestiary-items/Ut7DWly2y0Fghbxh.htm)|Corrupt Ally|auto-trad|
|[uufsa9Y9z4IZhCZb.htm](age-of-ashes-bestiary-items/uufsa9Y9z4IZhCZb.htm)|Keen Eyes|auto-trad|
|[UwdvzOviNTex0Xqb.htm](age-of-ashes-bestiary-items/UwdvzOviNTex0Xqb.htm)|Primal Innate Spells|auto-trad|
|[UxtBtayqxXB5mZip.htm](age-of-ashes-bestiary-items/UxtBtayqxXB5mZip.htm)|Bully's Push|auto-trad|
|[V02rKZLk4zOrx65r.htm](age-of-ashes-bestiary-items/V02rKZLk4zOrx65r.htm)|Thrown Debris|auto-trad|
|[v0Bn1s1wRHKj8MGA.htm](age-of-ashes-bestiary-items/v0Bn1s1wRHKj8MGA.htm)|Lazurite-Infused Flesh|auto-trad|
|[v1DFg4W547LdWp1p.htm](age-of-ashes-bestiary-items/v1DFg4W547LdWp1p.htm)|Pummeling Flurry|auto-trad|
|[V2OOVzLbdPKRulIf.htm](age-of-ashes-bestiary-items/V2OOVzLbdPKRulIf.htm)|Adhesive Body|auto-trad|
|[v2P0c4rmwI5BoEBT.htm](age-of-ashes-bestiary-items/v2P0c4rmwI5BoEBT.htm)|Attack of Opportunity|auto-trad|
|[v5JDDS5HftErPvPh.htm](age-of-ashes-bestiary-items/v5JDDS5HftErPvPh.htm)|Opened Sluices|auto-trad|
|[V6HMMbCm0RtvxhNC.htm](age-of-ashes-bestiary-items/V6HMMbCm0RtvxhNC.htm)|Claw|auto-trad|
|[V7VvhfarrGzmaDnf.htm](age-of-ashes-bestiary-items/V7VvhfarrGzmaDnf.htm)|Bite|auto-trad|
|[v8QaZXPq7Yj9K2v1.htm](age-of-ashes-bestiary-items/v8QaZXPq7Yj9K2v1.htm)|Constant Spells|auto-trad|
|[vCxz30plLPffDrlE.htm](age-of-ashes-bestiary-items/vCxz30plLPffDrlE.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[VDCmacyFJeeLLjxL.htm](age-of-ashes-bestiary-items/VDCmacyFJeeLLjxL.htm)|Incredible Initiative|auto-trad|
|[veIehkJyX5KsiJzn.htm](age-of-ashes-bestiary-items/veIehkJyX5KsiJzn.htm)|Hunter's Flurry|auto-trad|
|[veigzkgmikNe9iEL.htm](age-of-ashes-bestiary-items/veigzkgmikNe9iEL.htm)|Occult Innate Spells|auto-trad|
|[vEOKWwRMGGUetHuk.htm](age-of-ashes-bestiary-items/vEOKWwRMGGUetHuk.htm)|Telepathy 100 feet|auto-trad|
|[vevNfOyFN6QRJtR2.htm](age-of-ashes-bestiary-items/vevNfOyFN6QRJtR2.htm)|At-Will Spells|auto-trad|
|[vfDhFBTrynIBoAmF.htm](age-of-ashes-bestiary-items/vfDhFBTrynIBoAmF.htm)|Prepared Divine Spells|auto-trad|
|[VGLGaYgY8xICa8nV.htm](age-of-ashes-bestiary-items/VGLGaYgY8xICa8nV.htm)|Improved Grab|auto-trad|
|[VHTjtjDkxGqmFbdd.htm](age-of-ashes-bestiary-items/VHTjtjDkxGqmFbdd.htm)|Dagger|auto-trad|
|[vJ1d2ivm8JgAi3ET.htm](age-of-ashes-bestiary-items/vJ1d2ivm8JgAi3ET.htm)|Hatchet|auto-trad|
|[VJbm5TGMwUCws6K1.htm](age-of-ashes-bestiary-items/VJbm5TGMwUCws6K1.htm)|Longsword|auto-trad|
|[VkjtpvreAQ1v0h8Z.htm](age-of-ashes-bestiary-items/VkjtpvreAQ1v0h8Z.htm)|Hatchet|auto-trad|
|[VlCgM24yW7fJGoEX.htm](age-of-ashes-bestiary-items/VlCgM24yW7fJGoEX.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[vlFbh7Px7CtFWTTe.htm](age-of-ashes-bestiary-items/vlFbh7Px7CtFWTTe.htm)|Divine Prepared Spells|auto-trad|
|[vM66vkQqjowc47rC.htm](age-of-ashes-bestiary-items/vM66vkQqjowc47rC.htm)|Divine Innate Spells|auto-trad|
|[vmOhdTHRiDnoF4LK.htm](age-of-ashes-bestiary-items/vmOhdTHRiDnoF4LK.htm)|Eye Beam|auto-trad|
|[VNd49RdCdrZvopZ3.htm](age-of-ashes-bestiary-items/VNd49RdCdrZvopZ3.htm)|Motion Sense|auto-trad|
|[vPx7Kv8CDnL5rd3e.htm](age-of-ashes-bestiary-items/vPx7Kv8CDnL5rd3e.htm)|Warhammer|auto-trad|
|[VqMcIUne6jfbwvel.htm](age-of-ashes-bestiary-items/VqMcIUne6jfbwvel.htm)|Blood Magic|auto-trad|
|[vreeTrUJqAchFZUM.htm](age-of-ashes-bestiary-items/vreeTrUJqAchFZUM.htm)|+2 Circumstance to All Saves vs. Shove, Trip, or Being Knocked Prone|auto-trad|
|[vt4zVYNHBbCrRw9A.htm](age-of-ashes-bestiary-items/vt4zVYNHBbCrRw9A.htm)|At-Will Spells|auto-trad|
|[VTvR3zWnwVcxzIwl.htm](age-of-ashes-bestiary-items/VTvR3zWnwVcxzIwl.htm)|Improved Grab|auto-trad|
|[vxAKcJqC8kzmxb4t.htm](age-of-ashes-bestiary-items/vxAKcJqC8kzmxb4t.htm)|Darkvision|auto-trad|
|[vYOEAucH84uYzG3z.htm](age-of-ashes-bestiary-items/vYOEAucH84uYzG3z.htm)|Darkvision|auto-trad|
|[VzUnnud6z8Zj4DTC.htm](age-of-ashes-bestiary-items/VzUnnud6z8Zj4DTC.htm)|Scimitar|auto-trad|
|[W0905p3wQrOz7E2x.htm](age-of-ashes-bestiary-items/W0905p3wQrOz7E2x.htm)|Shield Block|auto-trad|
|[w40QDydlY3kEtlQE.htm](age-of-ashes-bestiary-items/w40QDydlY3kEtlQE.htm)|Rain of Arrows|auto-trad|
|[w481TRk2bURCXIxx.htm](age-of-ashes-bestiary-items/w481TRk2bURCXIxx.htm)|Coven|auto-trad|
|[w5Yk889pfRGklcLm.htm](age-of-ashes-bestiary-items/w5Yk889pfRGklcLm.htm)|Violet Eye Beam|auto-trad|
|[W6f4zhENM79wqemW.htm](age-of-ashes-bestiary-items/W6f4zhENM79wqemW.htm)|Tail|auto-trad|
|[wb5jOSkiZA5lYAX9.htm](age-of-ashes-bestiary-items/wb5jOSkiZA5lYAX9.htm)|Destructive Strikes|auto-trad|
|[wBZt1NPsZgzDeD2m.htm](age-of-ashes-bestiary-items/wBZt1NPsZgzDeD2m.htm)|Expanded Splash|auto-trad|
|[wDnuOjox5cjI817l.htm](age-of-ashes-bestiary-items/wDnuOjox5cjI817l.htm)|Immortality|auto-trad|
|[wHfHFuiq0qNw7Q0J.htm](age-of-ashes-bestiary-items/wHfHFuiq0qNw7Q0J.htm)|Vulnerability to Exorcism|auto-trad|
|[whQM5kvewGUXnnLe.htm](age-of-ashes-bestiary-items/whQM5kvewGUXnnLe.htm)|Anguished Shriek|auto-trad|
|[WIq9iFyHE2YJoFwA.htm](age-of-ashes-bestiary-items/WIq9iFyHE2YJoFwA.htm)|Claw|auto-trad|
|[wkS0gm33Vs1iiRST.htm](age-of-ashes-bestiary-items/wkS0gm33Vs1iiRST.htm)|Dream Haunting|auto-trad|
|[wmpfvEE1W9Sses2l.htm](age-of-ashes-bestiary-items/wmpfvEE1W9Sses2l.htm)|Bark Orders|auto-trad|
|[wnE8aQGkQtI87toJ.htm](age-of-ashes-bestiary-items/wnE8aQGkQtI87toJ.htm)|Darkvision|auto-trad|
|[Wo5Z8Jix5ohz1Urx.htm](age-of-ashes-bestiary-items/Wo5Z8Jix5ohz1Urx.htm)|Rock Dwarf|auto-trad|
|[wOCajMqUzhODb2NB.htm](age-of-ashes-bestiary-items/wOCajMqUzhODb2NB.htm)|Steady Spellcasting|auto-trad|
|[WonIEfwUndYFXxyq.htm](age-of-ashes-bestiary-items/WonIEfwUndYFXxyq.htm)|Thrown Debris|auto-trad|
|[WpLmYPgmK0qG58K8.htm](age-of-ashes-bestiary-items/WpLmYPgmK0qG58K8.htm)|Pseudopod|auto-trad|
|[wqr4H9kTRu8PWkfc.htm](age-of-ashes-bestiary-items/wqr4H9kTRu8PWkfc.htm)|Tongue Grab|auto-trad|
|[wSkREkhq1FagvrHD.htm](age-of-ashes-bestiary-items/wSkREkhq1FagvrHD.htm)|Arcane Prepared Spells|auto-trad|
|[WsUJjMncNN9DWybk.htm](age-of-ashes-bestiary-items/WsUJjMncNN9DWybk.htm)|Frightful Presence|auto-trad|
|[wt7Ds9r5d3F2v34T.htm](age-of-ashes-bestiary-items/wt7Ds9r5d3F2v34T.htm)|Draconic Momentum|auto-trad|
|[wTrhi9PyRoPxh06i.htm](age-of-ashes-bestiary-items/wTrhi9PyRoPxh06i.htm)|Burning Mace|auto-trad|
|[WvSXMmKbLjsbglaD.htm](age-of-ashes-bestiary-items/WvSXMmKbLjsbglaD.htm)|Glimpse of Redemption|auto-trad|
|[wwajiyf5MX0aRcgm.htm](age-of-ashes-bestiary-items/wwajiyf5MX0aRcgm.htm)|Tail|auto-trad|
|[wwCXCm8afQthQH5k.htm](age-of-ashes-bestiary-items/wwCXCm8afQthQH5k.htm)|Occult Rituals|auto-trad|
|[wXHpR0xMnyNfcooq.htm](age-of-ashes-bestiary-items/wXHpR0xMnyNfcooq.htm)|Darkvision|auto-trad|
|[wXqVDCpWmbgHRsvv.htm](age-of-ashes-bestiary-items/wXqVDCpWmbgHRsvv.htm)|Prismatic Beam|auto-trad|
|[x4yLU5eVHfGlI7wG.htm](age-of-ashes-bestiary-items/x4yLU5eVHfGlI7wG.htm)|Telekinetic Object|auto-trad|
|[x6TyhZ97JVNfZqqb.htm](age-of-ashes-bestiary-items/x6TyhZ97JVNfZqqb.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[x7eL2M3jSBwA2oka.htm](age-of-ashes-bestiary-items/x7eL2M3jSBwA2oka.htm)|Frightful Presence|auto-trad|
|[x94Pe7R5UrLKdRbG.htm](age-of-ashes-bestiary-items/x94Pe7R5UrLKdRbG.htm)|Invoke Reckoning|auto-trad|
|[x95HHOmRVl9H3tZ3.htm](age-of-ashes-bestiary-items/x95HHOmRVl9H3tZ3.htm)|Catch Rock|auto-trad|
|[x9FRA1b0TaNaQQ1p.htm](age-of-ashes-bestiary-items/x9FRA1b0TaNaQQ1p.htm)|Pervasive Attacks|auto-trad|
|[xCWXBbQXE3SoIRlC.htm](age-of-ashes-bestiary-items/xCWXBbQXE3SoIRlC.htm)|Spectral Hand|auto-trad|
|[XD9BdYMcx2Thd1zB.htm](age-of-ashes-bestiary-items/XD9BdYMcx2Thd1zB.htm)|Shield Shove|auto-trad|
|[XDuhN8g49cd7hcxi.htm](age-of-ashes-bestiary-items/XDuhN8g49cd7hcxi.htm)|Lifesense 60 feet|auto-trad|
|[XfTtTfEahCIBTcwm.htm](age-of-ashes-bestiary-items/XfTtTfEahCIBTcwm.htm)|Flail|auto-trad|
|[xfV7KvKayKVA2xs0.htm](age-of-ashes-bestiary-items/xfV7KvKayKVA2xs0.htm)|Explosive Breath|auto-trad|
|[xGPdcgIZR0tAI9IY.htm](age-of-ashes-bestiary-items/xGPdcgIZR0tAI9IY.htm)|Hair Barrage|auto-trad|
|[xHg8hrfrTg35erxf.htm](age-of-ashes-bestiary-items/xHg8hrfrTg35erxf.htm)|Contingency|auto-trad|
|[XIHvMvJwIJxCpNA7.htm](age-of-ashes-bestiary-items/XIHvMvJwIJxCpNA7.htm)|Arcane Innate Spells|auto-trad|
|[XiZUzjxIJiMTuUq9.htm](age-of-ashes-bestiary-items/XiZUzjxIJiMTuUq9.htm)|Attack of Opportunity|auto-trad|
|[XJmutb6gsFNsubD3.htm](age-of-ashes-bestiary-items/XJmutb6gsFNsubD3.htm)|Jaws|auto-trad|
|[XLA0uMn7KxLm7iN7.htm](age-of-ashes-bestiary-items/XLA0uMn7KxLm7iN7.htm)|Tail Lash|auto-trad|
|[XLCaqjJ5OC0PqgJp.htm](age-of-ashes-bestiary-items/XLCaqjJ5OC0PqgJp.htm)|Dagger|auto-trad|
|[xMoISwQsrQGkhK7q.htm](age-of-ashes-bestiary-items/xMoISwQsrQGkhK7q.htm)|Dimensional Shunt|auto-trad|
|[xN7xuZd6V7GrLGii.htm](age-of-ashes-bestiary-items/xN7xuZd6V7GrLGii.htm)|7-8 Flaming Vortex|auto-trad|
|[Xnas07kWC3ZCJmY4.htm](age-of-ashes-bestiary-items/Xnas07kWC3ZCJmY4.htm)|Dragonstorm Breath|auto-trad|
|[xnnq7Jjs1DHgYSUC.htm](age-of-ashes-bestiary-items/xnnq7Jjs1DHgYSUC.htm)|Web Sense (Imprecise) 60 feet|auto-trad|
|[XO1T3uBgfNZ2aJks.htm](age-of-ashes-bestiary-items/XO1T3uBgfNZ2aJks.htm)|Throw Rock|auto-trad|
|[XqfuKJG5gZuzeCDr.htm](age-of-ashes-bestiary-items/XqfuKJG5gZuzeCDr.htm)|Frightful Presence|auto-trad|
|[XrDITDvengsQOcjL.htm](age-of-ashes-bestiary-items/XrDITDvengsQOcjL.htm)|Occult Innate Spells|auto-trad|
|[xSd36hFNsd1RqBw7.htm](age-of-ashes-bestiary-items/xSd36hFNsd1RqBw7.htm)|Ferocity|auto-trad|
|[XsSwjx894G8tLV3p.htm](age-of-ashes-bestiary-items/XsSwjx894G8tLV3p.htm)|+2 Status to All Saves vs. Arcane Magic|auto-trad|
|[XteT6RS3CQMyiNDq.htm](age-of-ashes-bestiary-items/XteT6RS3CQMyiNDq.htm)|At-Will Spells|auto-trad|
|[XTtyz95mOu6Xrre4.htm](age-of-ashes-bestiary-items/XTtyz95mOu6Xrre4.htm)|Sneak Attack|auto-trad|
|[XUqvBZPxNzGJ4RIm.htm](age-of-ashes-bestiary-items/XUqvBZPxNzGJ4RIm.htm)|Vulnerable to Dispelling|auto-trad|
|[xvs14t3klUSEYCct.htm](age-of-ashes-bestiary-items/xvs14t3klUSEYCct.htm)|Prepared Arcane Spells|auto-trad|
|[XXHOTNV0xQRjxtwV.htm](age-of-ashes-bestiary-items/XXHOTNV0xQRjxtwV.htm)|Stunning Retort|auto-trad|
|[XxjjAD0GTCEaspdx.htm](age-of-ashes-bestiary-items/XxjjAD0GTCEaspdx.htm)|Perfect Will|auto-trad|
|[Xy8CYVbA7GcoN8SC.htm](age-of-ashes-bestiary-items/Xy8CYVbA7GcoN8SC.htm)|Whip Swing|auto-trad|
|[xyPbW6KkYnYYyzU1.htm](age-of-ashes-bestiary-items/xyPbW6KkYnYYyzU1.htm)|At-Will Spells|auto-trad|
|[XzC7Tpe9v9HjnSor.htm](age-of-ashes-bestiary-items/XzC7Tpe9v9HjnSor.htm)|Localized Dragonstorm|auto-trad|
|[XZIP4xIGRqQ4M88R.htm](age-of-ashes-bestiary-items/XZIP4xIGRqQ4M88R.htm)|Inexorable|auto-trad|
|[Y1AIIIjE3wSGI9AA.htm](age-of-ashes-bestiary-items/Y1AIIIjE3wSGI9AA.htm)|Engulf|auto-trad|
|[y1Ce9dDq4YBhhTY5.htm](age-of-ashes-bestiary-items/y1Ce9dDq4YBhhTY5.htm)|Arcane Innate Spells|auto-trad|
|[y5Na5QhVKW187D0K.htm](age-of-ashes-bestiary-items/y5Na5QhVKW187D0K.htm)|Mental Magic|auto-trad|
|[y6J0cFsDLtTzzYwe.htm](age-of-ashes-bestiary-items/y6J0cFsDLtTzzYwe.htm)|Negative Healing|auto-trad|
|[Y6pLT1gObD3hTlFj.htm](age-of-ashes-bestiary-items/Y6pLT1gObD3hTlFj.htm)|Champion Devotion Spells|auto-trad|
|[Y8FacHkJWvG6NwDu.htm](age-of-ashes-bestiary-items/Y8FacHkJWvG6NwDu.htm)|Rend|auto-trad|
|[YC7WTdAOh21NEeNx.htm](age-of-ashes-bestiary-items/YC7WTdAOh21NEeNx.htm)|Occult Innate Spells|auto-trad|
|[yCCEm7HEqedgAAhD.htm](age-of-ashes-bestiary-items/yCCEm7HEqedgAAhD.htm)|+1 Status to All Saves vs. Poison|auto-trad|
|[yCOLPsjbm44QUBqX.htm](age-of-ashes-bestiary-items/yCOLPsjbm44QUBqX.htm)|At-Will Spells|auto-trad|
|[YeFEaw3Ja6zgrf9o.htm](age-of-ashes-bestiary-items/YeFEaw3Ja6zgrf9o.htm)|Double Slice|auto-trad|
|[yEzDmYi6aYTiIA8Y.htm](age-of-ashes-bestiary-items/yEzDmYi6aYTiIA8Y.htm)|Ride the Wind|auto-trad|
|[yfiUAGMbYu5GpkVc.htm](age-of-ashes-bestiary-items/yfiUAGMbYu5GpkVc.htm)|Constant Spells|auto-trad|
|[yggAbFpFmUKjj7Bz.htm](age-of-ashes-bestiary-items/yggAbFpFmUKjj7Bz.htm)|Drain Bonded Item|auto-trad|
|[yh0jnKztqB0nQlYV.htm](age-of-ashes-bestiary-items/yh0jnKztqB0nQlYV.htm)|Nail|auto-trad|
|[YhBi7QJI7xdnQU10.htm](age-of-ashes-bestiary-items/YhBi7QJI7xdnQU10.htm)|Acidic Needle|auto-trad|
|[YhdTXGv6J7fkl4wd.htm](age-of-ashes-bestiary-items/YhdTXGv6J7fkl4wd.htm)|Inexorable March|auto-trad|
|[Yimb91yYaAqRGR3x.htm](age-of-ashes-bestiary-items/Yimb91yYaAqRGR3x.htm)|Seismic Spear|auto-trad|
|[yJtHwSdZ0Gj2LGAH.htm](age-of-ashes-bestiary-items/yJtHwSdZ0Gj2LGAH.htm)|Shrieking Frenzy|auto-trad|
|[ymEZhbkIeDn7RKGI.htm](age-of-ashes-bestiary-items/ymEZhbkIeDn7RKGI.htm)|Trap Soul|auto-trad|
|[YMJhZThbEkA9mBqf.htm](age-of-ashes-bestiary-items/YMJhZThbEkA9mBqf.htm)|Darkvision|auto-trad|
|[yo9I3mhk7rIUPwWa.htm](age-of-ashes-bestiary-items/yo9I3mhk7rIUPwWa.htm)|Daemonic Trap Making|auto-trad|
|[YoSJ0Xce0DnEuH7Z.htm](age-of-ashes-bestiary-items/YoSJ0Xce0DnEuH7Z.htm)|Composite Longbow|auto-trad|
|[yqO9SHE3AFcvwy0R.htm](age-of-ashes-bestiary-items/yqO9SHE3AFcvwy0R.htm)|Disturbing Vision|auto-trad|
|[YQZTNPJv0qGnH7F6.htm](age-of-ashes-bestiary-items/YQZTNPJv0qGnH7F6.htm)|Focus Spells|auto-trad|
|[yrnHfAodzInJxTRU.htm](age-of-ashes-bestiary-items/yrnHfAodzInJxTRU.htm)|Constant Spells|auto-trad|
|[yRzCy4eBcJG8ydmJ.htm](age-of-ashes-bestiary-items/yRzCy4eBcJG8ydmJ.htm)|9-10 Poison Miasma|auto-trad|
|[YsvmoSYwOoP0KhQx.htm](age-of-ashes-bestiary-items/YsvmoSYwOoP0KhQx.htm)|Blue Eye Beam|auto-trad|
|[yt0OzxOSDhYO0Nle.htm](age-of-ashes-bestiary-items/yt0OzxOSDhYO0Nle.htm)|Composite Shortbow|auto-trad|
|[YT5Tkw6exXuHF2ph.htm](age-of-ashes-bestiary-items/YT5Tkw6exXuHF2ph.htm)|Soul Chain|auto-trad|
|[yTHVYVd7pjTLwWpu.htm](age-of-ashes-bestiary-items/yTHVYVd7pjTLwWpu.htm)|Syringe|auto-trad|
|[Yuh3IpCAUHQMRRoE.htm](age-of-ashes-bestiary-items/Yuh3IpCAUHQMRRoE.htm)|Deny Advantage|auto-trad|
|[YW4QH2QsfjdJXFo8.htm](age-of-ashes-bestiary-items/YW4QH2QsfjdJXFo8.htm)|Aluum Antimagic|auto-trad|
|[yXL2GLDidWh51HPb.htm](age-of-ashes-bestiary-items/yXL2GLDidWh51HPb.htm)|Dagger|auto-trad|
|[yyARzCq3i2SQxTFI.htm](age-of-ashes-bestiary-items/yyARzCq3i2SQxTFI.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Z28y4lP04fvgUiy2.htm](age-of-ashes-bestiary-items/Z28y4lP04fvgUiy2.htm)|Mutagenic Adaptation|auto-trad|
|[z4fETZWZ1URqOEw5.htm](age-of-ashes-bestiary-items/z4fETZWZ1URqOEw5.htm)|Subsonic Hum|auto-trad|
|[Z6mjzInaKKCOyvbM.htm](age-of-ashes-bestiary-items/Z6mjzInaKKCOyvbM.htm)|Glowing Spores|auto-trad|
|[Z7DJdh6dNliHnh4s.htm](age-of-ashes-bestiary-items/Z7DJdh6dNliHnh4s.htm)|Crossbow|auto-trad|
|[Z875mlZme0MfKkqS.htm](age-of-ashes-bestiary-items/Z875mlZme0MfKkqS.htm)|Redirect Portal|auto-trad|
|[z89EX9zy3v9pZxJc.htm](age-of-ashes-bestiary-items/z89EX9zy3v9pZxJc.htm)|Command Skeletons|auto-trad|
|[ZB91njeJdhCxI5DS.htm](age-of-ashes-bestiary-items/ZB91njeJdhCxI5DS.htm)|Rock|auto-trad|
|[zFmXqrFJBmQGs6z5.htm](age-of-ashes-bestiary-items/zFmXqrFJBmQGs6z5.htm)|Create Haunt|auto-trad|
|[zgALCNYNRMkV6Bhv.htm](age-of-ashes-bestiary-items/zgALCNYNRMkV6Bhv.htm)|Dagger|auto-trad|
|[ZgtDywSfiOFQD0r7.htm](age-of-ashes-bestiary-items/ZgtDywSfiOFQD0r7.htm)|Regeneration 50|auto-trad|
|[zhwIQhGc33778Khq.htm](age-of-ashes-bestiary-items/zhwIQhGc33778Khq.htm)|Improved Push|auto-trad|
|[Zj2BDpm3BaAvGpbd.htm](age-of-ashes-bestiary-items/Zj2BDpm3BaAvGpbd.htm)|Arborean Arm|auto-trad|
|[ZkM3S4OabItFRUIq.htm](age-of-ashes-bestiary-items/ZkM3S4OabItFRUIq.htm)|Negative Healing|auto-trad|
|[znA23DnIxfbHuPHh.htm](age-of-ashes-bestiary-items/znA23DnIxfbHuPHh.htm)|Draconic Frenzy|auto-trad|
|[zRPB5eZcHteseFIt.htm](age-of-ashes-bestiary-items/zRPB5eZcHteseFIt.htm)|Nul-Acrumi Vazghul Ritual|auto-trad|
|[ztAvaU3ULkuadpMc.htm](age-of-ashes-bestiary-items/ztAvaU3ULkuadpMc.htm)|Slash and Slam|auto-trad|
|[zuDdECTRYKz8JuqE.htm](age-of-ashes-bestiary-items/zuDdECTRYKz8JuqE.htm)|Darkvision|auto-trad|
|[zVCalXk0BtUnOMfM.htm](age-of-ashes-bestiary-items/zVCalXk0BtUnOMfM.htm)|Sharpened Fragment|auto-trad|
|[zWMWo1FSBvdX4mhf.htm](age-of-ashes-bestiary-items/zWMWo1FSBvdX4mhf.htm)|Attack of Opportunity|auto-trad|
|[ZXC6yvdBWQpHBJIb.htm](age-of-ashes-bestiary-items/ZXC6yvdBWQpHBJIb.htm)|Constrict|auto-trad|
|[ZXkVB1alDTTOm35s.htm](age-of-ashes-bestiary-items/ZXkVB1alDTTOm35s.htm)|Freeze|auto-trad|
|[ZyKpCc2am8rFqQ93.htm](age-of-ashes-bestiary-items/ZyKpCc2am8rFqQ93.htm)|Manifest Dagger|auto-trad|
|[ZYT8B7LGfrmxllL4.htm](age-of-ashes-bestiary-items/ZYT8B7LGfrmxllL4.htm)|Bloodsense|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[AKFVNPYKCZ4jZLKw.htm](age-of-ashes-bestiary-items/AKFVNPYKCZ4jZLKw.htm)|Necrotic Pulse|Pulso Necrótico|modificada|
|[AXpLna1FjT83aiYJ.htm](age-of-ashes-bestiary-items/AXpLna1FjT83aiYJ.htm)|Tormenting Touch|Toque Atormentador|modificada|
|[F3y7pLxdrd17fqdw.htm](age-of-ashes-bestiary-items/F3y7pLxdrd17fqdw.htm)|Death Throes|Estertores de muerte|modificada|
|[USQ16Z86qZYqJDkV.htm](age-of-ashes-bestiary-items/USQ16Z86qZYqJDkV.htm)|Persistent Bleed Critical|Persistente Sangrado Crítico|modificada|
|[ZWaPe5FQtP4LDiEM.htm](age-of-ashes-bestiary-items/ZWaPe5FQtP4LDiEM.htm)|Bleeding Nail|Uña sangrante|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[BdxoNSpPKQa1FYSZ.htm](age-of-ashes-bestiary-items/BdxoNSpPKQa1FYSZ.htm)|Soul Chain|vacía|
|[x15lr82vvdm9m35m.htm](age-of-ashes-bestiary-items/x15lr82vvdm9m35m.htm)|Warhammer|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
