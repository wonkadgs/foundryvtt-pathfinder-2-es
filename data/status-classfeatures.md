# Estado de la traducción (classfeatures)

 * **auto-trad**: 508
 * **modificada**: 17
 * **ninguna**: 17


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[inventor-01-fOulT6iKVqIK4jJX.htm](classfeatures/inventor-01-fOulT6iKVqIK4jJX.htm)|Razor Prongs|
|[inventor-01-Nbg4ZllDI9uCowZL.htm](classfeatures/inventor-01-Nbg4ZllDI9uCowZL.htm)|Hefty Composition|
|[inventor-01-O3r84Uv6HytaSIbX.htm](classfeatures/inventor-01-O3r84Uv6HytaSIbX.htm)|Blunt Shot|
|[inventor-01-O9wpXEKtKYJOMIlK.htm](classfeatures/inventor-01-O9wpXEKtKYJOMIlK.htm)|Dynamic Weighting|
|[inventor-01-pSXlZggdCCbkQqNr.htm](classfeatures/inventor-01-pSXlZggdCCbkQqNr.htm)|Pacification Tools|
|[inventor-01-qIOKqT93h6CX6V4k.htm](classfeatures/inventor-01-qIOKqT93h6CX6V4k.htm)|Complex Simplicity|
|[inventor-01-qwhfPgE2tTW0hvPe.htm](classfeatures/inventor-01-qwhfPgE2tTW0hvPe.htm)|Modular Head|
|[inventor-01-R8cfRNPdaCkd2bud.htm](classfeatures/inventor-01-R8cfRNPdaCkd2bud.htm)|Hampering Spikes|
|[inventor-01-Z1au5zxYcjZvdQpd.htm](classfeatures/inventor-01-Z1au5zxYcjZvdQpd.htm)|Entangling Form|
|[inventor-01-ZCfPjOn6JJ8Zrgvg.htm](classfeatures/inventor-01-ZCfPjOn6JJ8Zrgvg.htm)|Segmented Frame|
|[inventor-07-7Zw83ysONrNhJMr8.htm](classfeatures/inventor-07-7Zw83ysONrNhJMr8.htm)|Aerodynamic Construction|
|[inventor-07-h1hNr4jABip2ERDd.htm](classfeatures/inventor-07-h1hNr4jABip2ERDd.htm)|Inconspicuous Appearance|
|[inventor-07-P1GbGEePC8zDi8K4.htm](classfeatures/inventor-07-P1GbGEePC8zDi8K4.htm)|Advanced Rangefinder|
|[inventor-07-ptBLVvRqn1fA3A4l.htm](classfeatures/inventor-07-ptBLVvRqn1fA3A4l.htm)|Rope Shot|
|[inventor-07-tPL4lrGLCB5XbgeV.htm](classfeatures/inventor-07-tPL4lrGLCB5XbgeV.htm)|Integrated Gauntlet|
|[inventor-07-WxkZMlETXo165XnC.htm](classfeatures/inventor-07-WxkZMlETXo165XnC.htm)|Tangle Line|
|[inventor-07-y62b9R6RUBbkZECA.htm](classfeatures/inventor-07-y62b9R6RUBbkZECA.htm)|Manifold Alloy|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[alchemist-01-7JbiaZ8bxODM5mzS.htm](classfeatures/alchemist-01-7JbiaZ8bxODM5mzS.htm)|Bomber|auto-trad|
|[alchemist-01-cU2ofQLj7pg6wTSi.htm](classfeatures/alchemist-01-cU2ofQLj7pg6wTSi.htm)|Research Field|auto-trad|
|[alchemist-01-eNZnx4LISDNftbx2.htm](classfeatures/alchemist-01-eNZnx4LISDNftbx2.htm)|Chirurgeon|auto-trad|
|[alchemist-01-Pe0zmIqyTBc2Td0I.htm](classfeatures/alchemist-01-Pe0zmIqyTBc2Td0I.htm)|Advanced Alchemy|auto-trad|
|[alchemist-01-sPtl05wwTpqFI0lL.htm](classfeatures/alchemist-01-sPtl05wwTpqFI0lL.htm)|Quick Alchemy|auto-trad|
|[alchemist-01-tvdb1jkjl2bRZjSp.htm](classfeatures/alchemist-01-tvdb1jkjl2bRZjSp.htm)|Mutagenist|auto-trad|
|[alchemist-01-w3aS3tsvH2Ub6XMn.htm](classfeatures/alchemist-01-w3aS3tsvH2Ub6XMn.htm)|Alchemy|auto-trad|
|[alchemist-01-wySB9VHOW1v3TX1L.htm](classfeatures/alchemist-01-wySB9VHOW1v3TX1L.htm)|Infused Reagents|auto-trad|
|[alchemist-01-XPPG7nN9pxt0sjMg.htm](classfeatures/alchemist-01-XPPG7nN9pxt0sjMg.htm)|Formula Book|auto-trad|
|[alchemist-05-6zo2PJGYoig7nFpR.htm](classfeatures/alchemist-05-6zo2PJGYoig7nFpR.htm)|Field Discovery (Toxicologist)|auto-trad|
|[alchemist-05-7JK2a1D3VeWDcObo.htm](classfeatures/alchemist-05-7JK2a1D3VeWDcObo.htm)|Powerful Alchemy|auto-trad|
|[alchemist-05-8QAFgy9U8PxEa7Dw.htm](classfeatures/alchemist-05-8QAFgy9U8PxEa7Dw.htm)|Field Discovery (Bomber)|auto-trad|
|[alchemist-05-IxxPEahbqXwIXum7.htm](classfeatures/alchemist-05-IxxPEahbqXwIXum7.htm)|Field Discovery|auto-trad|
|[alchemist-05-qC0Iz6SlG2i9gv6g.htm](classfeatures/alchemist-05-qC0Iz6SlG2i9gv6g.htm)|Field Discovery (Chirurgeon)|auto-trad|
|[alchemist-05-V4Jt7eDnJBLv5bDj.htm](classfeatures/alchemist-05-V4Jt7eDnJBLv5bDj.htm)|Field Discovery (Mutagenist)|auto-trad|
|[alchemist-07-4ocPy4O0OCLY0XCM.htm](classfeatures/alchemist-07-4ocPy4O0OCLY0XCM.htm)|Alchemical Weapon Expertise|auto-trad|
|[alchemist-07-DFQDtT1Van4fFEHi.htm](classfeatures/alchemist-07-DFQDtT1Van4fFEHi.htm)|Perpetual Infusions (Bomber)|auto-trad|
|[alchemist-07-Dug1oaVYejLmYEFt.htm](classfeatures/alchemist-07-Dug1oaVYejLmYEFt.htm)|Perpetual Infusions (Mutagenist)|auto-trad|
|[alchemist-07-fzvIe6FwwCuIdnjX.htm](classfeatures/alchemist-07-fzvIe6FwwCuIdnjX.htm)|Perpetual Infusions (Chirurgeon)|auto-trad|
|[alchemist-07-LlZ5R50z9j8jysZL.htm](classfeatures/alchemist-07-LlZ5R50z9j8jysZL.htm)|Perpetual Infusions (Toxicologist)|auto-trad|
|[alchemist-07-ZqwHAoIZrI1dGoqK.htm](classfeatures/alchemist-07-ZqwHAoIZrI1dGoqK.htm)|Perpetual Infusions|auto-trad|
|[alchemist-09-3e1PlMXmlSwKoc6d.htm](classfeatures/alchemist-09-3e1PlMXmlSwKoc6d.htm)|Alchemical Expertise|auto-trad|
|[alchemist-09-76cwNLJEm4Yetnee.htm](classfeatures/alchemist-09-76cwNLJEm4Yetnee.htm)|Double Brew|auto-trad|
|[alchemist-11-8rEVg03QJ71ic3PP.htm](classfeatures/alchemist-11-8rEVg03QJ71ic3PP.htm)|Perpetual Potency (Bomber)|auto-trad|
|[alchemist-11-JOdbVu14phvdjhaY.htm](classfeatures/alchemist-11-JOdbVu14phvdjhaY.htm)|Perpetual Potency (Toxicologist)|auto-trad|
|[alchemist-11-MGn2wezOr3VAdO3U.htm](classfeatures/alchemist-11-MGn2wezOr3VAdO3U.htm)|Perpetual Potency|auto-trad|
|[alchemist-11-mZFqRLYOQEqKA8ri.htm](classfeatures/alchemist-11-mZFqRLYOQEqKA8ri.htm)|Perpetual Potency (Mutagenist)|auto-trad|
|[alchemist-11-VS5vkqUQu4n7E28Y.htm](classfeatures/alchemist-11-VS5vkqUQu4n7E28Y.htm)|Perpetual Potency (Chirurgeon)|auto-trad|
|[alchemist-13-1BKdOJ0HNL6Eg3xw.htm](classfeatures/alchemist-13-1BKdOJ0HNL6Eg3xw.htm)|Greater Field Discovery (Mutagenist)|auto-trad|
|[alchemist-13-bv3Qel8v9tpoFbw4.htm](classfeatures/alchemist-13-bv3Qel8v9tpoFbw4.htm)|Alchemist Armor Expertise (Level 13)|auto-trad|
|[alchemist-13-JJcaVijwRt9dsnac.htm](classfeatures/alchemist-13-JJcaVijwRt9dsnac.htm)|Greater Field Discovery (Chirurgeon)|auto-trad|
|[alchemist-13-MEwvBnT2VsO5lQ6I.htm](classfeatures/alchemist-13-MEwvBnT2VsO5lQ6I.htm)|Greater Field Discovery|auto-trad|
|[alchemist-13-RGs4uR3CAvgbtBAA.htm](classfeatures/alchemist-13-RGs4uR3CAvgbtBAA.htm)|Greater Field Discovery (Bomber)|auto-trad|
|[alchemist-13-tnqyQrhrZeDtDvcO.htm](classfeatures/alchemist-13-tnqyQrhrZeDtDvcO.htm)|Greater Field Discovery (Toxicologist)|auto-trad|
|[alchemist-15-Eood6pNPaJxuSgD1.htm](classfeatures/alchemist-15-Eood6pNPaJxuSgD1.htm)|Alchemical Alacrity|auto-trad|
|[alchemist-17-11nGqrSJOoGRlDjO.htm](classfeatures/alchemist-17-11nGqrSJOoGRlDjO.htm)|Perpetual Perfection|auto-trad|
|[alchemist-17-3R19zS7gERhEX87F.htm](classfeatures/alchemist-17-3R19zS7gERhEX87F.htm)|Perpetual Perfection (Toxicologist)|auto-trad|
|[alchemist-17-CGetAmSbv06fW7GT.htm](classfeatures/alchemist-17-CGetAmSbv06fW7GT.htm)|Perpetual Perfection (Mutagenist)|auto-trad|
|[alchemist-17-eG7FBDjCdEFzW9V9.htm](classfeatures/alchemist-17-eG7FBDjCdEFzW9V9.htm)|Alchemical Mastery|auto-trad|
|[alchemist-17-xO90iBD8XNGyaCkz.htm](classfeatures/alchemist-17-xO90iBD8XNGyaCkz.htm)|Perpetual Perfection (Bomber)|auto-trad|
|[alchemist-17-YByJ9O7oe8wxfbqs.htm](classfeatures/alchemist-17-YByJ9O7oe8wxfbqs.htm)|Perpetual Perfection (Chirurgeon)|auto-trad|
|[alchemist-19-FiVYuIPTBzPzNP4E.htm](classfeatures/alchemist-19-FiVYuIPTBzPzNP4E.htm)|Alchemist Armor Mastery (Level 19)|auto-trad|
|[barbarian-01-1ZugTzJHsa94AZRW.htm](classfeatures/barbarian-01-1ZugTzJHsa94AZRW.htm)|Copper Dragon Instinct|auto-trad|
|[barbarian-01-2esqOHCn4GcZ4zYD.htm](classfeatures/barbarian-01-2esqOHCn4GcZ4zYD.htm)|White Dragon Instinct|auto-trad|
|[barbarian-01-31sPXwmEbbcvgsM9.htm](classfeatures/barbarian-01-31sPXwmEbbcvgsM9.htm)|Bull Animal Instinct|auto-trad|
|[barbarian-01-3lxIGMbsPZLNEXQ7.htm](classfeatures/barbarian-01-3lxIGMbsPZLNEXQ7.htm)|Gold Dragon Instinct|auto-trad|
|[barbarian-01-b5rvKZQCfpgBenKJ.htm](classfeatures/barbarian-01-b5rvKZQCfpgBenKJ.htm)|Brass Dragon Instinct|auto-trad|
|[barbarian-01-CXZwt1e6ManeBaFV.htm](classfeatures/barbarian-01-CXZwt1e6ManeBaFV.htm)|Frog Animal Instinct|auto-trad|
|[barbarian-01-dU7xRpg4kFd01hwZ.htm](classfeatures/barbarian-01-dU7xRpg4kFd01hwZ.htm)|Instinct|auto-trad|
|[barbarian-01-hyHgLQCDMSrR4RfE.htm](classfeatures/barbarian-01-hyHgLQCDMSrR4RfE.htm)|Red Dragon Instinct|auto-trad|
|[barbarian-01-IezPDYlweTtwCqkT.htm](classfeatures/barbarian-01-IezPDYlweTtwCqkT.htm)|Green Dragon Instinct|auto-trad|
|[barbarian-01-JuKD6k7nDwfO0Ckv.htm](classfeatures/barbarian-01-JuKD6k7nDwfO0Ckv.htm)|Giant Instinct|auto-trad|
|[barbarian-01-k7M9jedvt31AJ5ZR.htm](classfeatures/barbarian-01-k7M9jedvt31AJ5ZR.htm)|Fury Instinct|auto-trad|
|[barbarian-01-kdzIxHpzeRbdRqQA.htm](classfeatures/barbarian-01-kdzIxHpzeRbdRqQA.htm)|Bronze Dragon Instinct|auto-trad|
|[barbarian-01-OJmI1L4dhQfz8vze.htm](classfeatures/barbarian-01-OJmI1L4dhQfz8vze.htm)|Shark Animal Instinct|auto-trad|
|[barbarian-01-pIYWMCNnYDQfSRQh.htm](classfeatures/barbarian-01-pIYWMCNnYDQfSRQh.htm)|Snake Animal Instinct|auto-trad|
|[barbarian-01-RiOww9KMu06D7wtW.htm](classfeatures/barbarian-01-RiOww9KMu06D7wtW.htm)|Blue Dragon Instinct|auto-trad|
|[barbarian-01-RQUJgDjJODO775qb.htm](classfeatures/barbarian-01-RQUJgDjJODO775qb.htm)|Deer Animal Instinct|auto-trad|
|[barbarian-01-SCYSjUbMmw8JD9P9.htm](classfeatures/barbarian-01-SCYSjUbMmw8JD9P9.htm)|Superstition Instinct|auto-trad|
|[barbarian-01-TQqv9Q5mB4PW6LH9.htm](classfeatures/barbarian-01-TQqv9Q5mB4PW6LH9.htm)|Spirit Instinct|auto-trad|
|[barbarian-01-uGY2yddm8mZx8Yo2.htm](classfeatures/barbarian-01-uGY2yddm8mZx8Yo2.htm)|Bear Animal Instinct|auto-trad|
|[barbarian-01-vCNtX2LwlemhA3tu.htm](classfeatures/barbarian-01-vCNtX2LwlemhA3tu.htm)|Cat Animal Instinct|auto-trad|
|[barbarian-01-vlRvOQS1HZZqSyh7.htm](classfeatures/barbarian-01-vlRvOQS1HZZqSyh7.htm)|Ape Animal Instinct|auto-trad|
|[barbarian-01-VNbDNiWjARtGQQAs.htm](classfeatures/barbarian-01-VNbDNiWjARtGQQAs.htm)|Black Dragon Instinct|auto-trad|
|[barbarian-01-xX6KnYYgHlPGoTG6.htm](classfeatures/barbarian-01-xX6KnYYgHlPGoTG6.htm)|Wolf Animal Instinct|auto-trad|
|[barbarian-01-Z2eWkfXblU0QxFx1.htm](classfeatures/barbarian-01-Z2eWkfXblU0QxFx1.htm)|Silver Dragon Instinct|auto-trad|
|[barbarian-05-EEUTd0jAyfwTLzjk.htm](classfeatures/barbarian-05-EEUTd0jAyfwTLzjk.htm)|Brutality|auto-trad|
|[barbarian-09-ie6xDX9GMEcA2Iuq.htm](classfeatures/barbarian-09-ie6xDX9GMEcA2Iuq.htm)|Raging Resistance|auto-trad|
|[barbarian-11-88Q33X2a0iYPkbzd.htm](classfeatures/barbarian-11-88Q33X2a0iYPkbzd.htm)|Mighty Rage|auto-trad|
|[barbarian-13-ejP4jVQkS48uKRFz.htm](classfeatures/barbarian-13-ejP4jVQkS48uKRFz.htm)|Weapon Fury|auto-trad|
|[barbarian-13-TuL0UfqH14MtqYVh.htm](classfeatures/barbarian-13-TuL0UfqH14MtqYVh.htm)|Greater Juggernaut|auto-trad|
|[barbarian-15-7JjhxMFo8DMwpGx0.htm](classfeatures/barbarian-15-7JjhxMFo8DMwpGx0.htm)|Greater Weapon Specialization (Barbarian)|auto-trad|
|[barbarian-15-BZnqKnqKVImjSIFE.htm](classfeatures/barbarian-15-BZnqKnqKVImjSIFE.htm)|Indomitable Will|auto-trad|
|[barbarian-17-7MhzrbOyue5GQsck.htm](classfeatures/barbarian-17-7MhzrbOyue5GQsck.htm)|Heightened Senses|auto-trad|
|[barbarian-17-qMtyQGUllPdgpzUo.htm](classfeatures/barbarian-17-qMtyQGUllPdgpzUo.htm)|Quick Rage|auto-trad|
|[barbarian-19-QTCIahokREpnAYDi.htm](classfeatures/barbarian-19-QTCIahokREpnAYDi.htm)|Armor of Fury|auto-trad|
|[barbarian-19-VLiT503OLOM3vaDx.htm](classfeatures/barbarian-19-VLiT503OLOM3vaDx.htm)|Devastator|auto-trad|
|[bard-01-4ripp6EfdVpS0d60.htm](classfeatures/bard-01-4ripp6EfdVpS0d60.htm)|Enigma|auto-trad|
|[bard-01-6FsusoMYxxjyIkVh.htm](classfeatures/bard-01-6FsusoMYxxjyIkVh.htm)|Spell Repertoire (Bard)|auto-trad|
|[bard-01-AIOBWGOS4nkfH3kW.htm](classfeatures/bard-01-AIOBWGOS4nkfH3kW.htm)|Muses|auto-trad|
|[bard-01-fEOj0eOBe34qYdAa.htm](classfeatures/bard-01-fEOj0eOBe34qYdAa.htm)|Occult Spellcasting|auto-trad|
|[bard-01-N03BtRvjX9TeHRa4.htm](classfeatures/bard-01-N03BtRvjX9TeHRa4.htm)|Warrior|auto-trad|
|[bard-01-s0VbbQJNlSgPocui.htm](classfeatures/bard-01-s0VbbQJNlSgPocui.htm)|Composition Spells|auto-trad|
|[bard-01-y0jGimYdMGDJWrEq.htm](classfeatures/bard-01-y0jGimYdMGDJWrEq.htm)|Polymath|auto-trad|
|[bard-01-YMBsi4bndRAk5CX4.htm](classfeatures/bard-01-YMBsi4bndRAk5CX4.htm)|Maestro|auto-trad|
|[bard-11-4lp8oG9A3zuqhPBS.htm](classfeatures/bard-11-4lp8oG9A3zuqhPBS.htm)|Bard Weapon Expertise|auto-trad|
|[bard-19-NjsOpWbbzUY2Hpk3.htm](classfeatures/bard-19-NjsOpWbbzUY2Hpk3.htm)|Magnum Opus|auto-trad|
|[champion-00-QQP0mu0cyWIwNUh9.htm](classfeatures/champion-00-QQP0mu0cyWIwNUh9.htm)|Shield Ally|auto-trad|
|[champion-00-Z6E1O8X7CFcyczB1.htm](classfeatures/champion-00-Z6E1O8X7CFcyczB1.htm)|Steed Ally|auto-trad|
|[champion-01-8YIA0jh64Ecz0TG6.htm](classfeatures/champion-01-8YIA0jh64Ecz0TG6.htm)|Desecrator|auto-trad|
|[champion-01-ehL7mnkqxN5wIkgu.htm](classfeatures/champion-01-ehL7mnkqxN5wIkgu.htm)|Deity and Cause|auto-trad|
|[champion-01-EQ6DVIQHAUXUhY6Y.htm](classfeatures/champion-01-EQ6DVIQHAUXUhY6Y.htm)|Antipaladin|auto-trad|
|[champion-01-FCoMFUsth4xB4veC.htm](classfeatures/champion-01-FCoMFUsth4xB4veC.htm)|Liberator|auto-trad|
|[champion-01-FeBsYn2mHfMVDZvw.htm](classfeatures/champion-01-FeBsYn2mHfMVDZvw.htm)|Deific Weapon|auto-trad|
|[champion-01-fykh5pE99O3I2sOI.htm](classfeatures/champion-01-fykh5pE99O3I2sOI.htm)|Champion's Code|auto-trad|
|[champion-01-JiY2ZB4FkK8RJm4T.htm](classfeatures/champion-01-JiY2ZB4FkK8RJm4T.htm)|The Tenets of Evil|auto-trad|
|[champion-01-nxZYP3KGfTSkaW6J.htm](classfeatures/champion-01-nxZYP3KGfTSkaW6J.htm)|The Tenets of Good|auto-trad|
|[champion-01-peEXunfbSD8WcMFk.htm](classfeatures/champion-01-peEXunfbSD8WcMFk.htm)|Paladin|auto-trad|
|[champion-01-Q1VfQZp49hkhY0HY.htm](classfeatures/champion-01-Q1VfQZp49hkhY0HY.htm)|Devotion Spells|auto-trad|
|[champion-01-sXVX4ARUuo8Egrz5.htm](classfeatures/champion-01-sXVX4ARUuo8Egrz5.htm)|Champion's Reaction|auto-trad|
|[champion-01-UyuwFp0jQqYL2AdF.htm](classfeatures/champion-01-UyuwFp0jQqYL2AdF.htm)|Redeemer|auto-trad|
|[champion-09-3XK573A7GH1rrLgO.htm](classfeatures/champion-09-3XK573A7GH1rrLgO.htm)|Divine Smite|auto-trad|
|[champion-09-VgmfNKtQLgBaNi5r.htm](classfeatures/champion-09-VgmfNKtQLgBaNi5r.htm)|Champion Expertise|auto-trad|
|[champion-11-uptzvOLrZ3fctrl2.htm](classfeatures/champion-11-uptzvOLrZ3fctrl2.htm)|Exalt|auto-trad|
|[champion-11-xygfZopqXBJ6dKBA.htm](classfeatures/champion-11-xygfZopqXBJ6dKBA.htm)|Divine Will|auto-trad|
|[champion-17-voiSCh7ZXA2ogwiC.htm](classfeatures/champion-17-voiSCh7ZXA2ogwiC.htm)|Legendary Armor|auto-trad|
|[champion-17-z5G0o04uV65zyxDB.htm](classfeatures/champion-17-z5G0o04uV65zyxDB.htm)|Champion Mastery|auto-trad|
|[champion-19-LzB6X9vOaq3wq1FZ.htm](classfeatures/champion-19-LzB6X9vOaq3wq1FZ.htm)|Hero's Defiance|auto-trad|
|[cleric-01-0Aocw3igLwna9cjp.htm](classfeatures/cleric-01-0Aocw3igLwna9cjp.htm)|Warpriest|auto-trad|
|[cleric-01-aiwxBj5MjnafCMyn.htm](classfeatures/cleric-01-aiwxBj5MjnafCMyn.htm)|First Doctrine (Cloistered Cleric)|auto-trad|
|[cleric-01-AvNbdGSOTWNRgcxs.htm](classfeatures/cleric-01-AvNbdGSOTWNRgcxs.htm)|Divine Spellcasting (Cleric)|auto-trad|
|[cleric-01-DutW12WMFPHBoLTH.htm](classfeatures/cleric-01-DutW12WMFPHBoLTH.htm)|Deity|auto-trad|
|[cleric-01-gblTFUOgolqFS9v4.htm](classfeatures/cleric-01-gblTFUOgolqFS9v4.htm)|Divine Font|auto-trad|
|[cleric-01-Qejo7FUWQtPTpgWH.htm](classfeatures/cleric-01-Qejo7FUWQtPTpgWH.htm)|First Doctrine|auto-trad|
|[cleric-01-tyrBwBTzo5t9Zho7.htm](classfeatures/cleric-01-tyrBwBTzo5t9Zho7.htm)|Doctrine|auto-trad|
|[cleric-01-UV1HlClbWCNcaKBZ.htm](classfeatures/cleric-01-UV1HlClbWCNcaKBZ.htm)|Anathema (Cleric)|auto-trad|
|[cleric-01-xxkszluN9icAiTO4.htm](classfeatures/cleric-01-xxkszluN9icAiTO4.htm)|First Doctrine (Warpriest)|auto-trad|
|[cleric-01-ZZzLMOUAtBVgV1DF.htm](classfeatures/cleric-01-ZZzLMOUAtBVgV1DF.htm)|Cloistered Cleric|auto-trad|
|[cleric-03-D34mPo29r1J3DPaX.htm](classfeatures/cleric-03-D34mPo29r1J3DPaX.htm)|Second Doctrine (Warpriest)|auto-trad|
|[cleric-03-OnfrrwCfDFCFw0tc.htm](classfeatures/cleric-03-OnfrrwCfDFCFw0tc.htm)|Second Doctrine|auto-trad|
|[cleric-03-sa7BWfnyCswAvBVa.htm](classfeatures/cleric-03-sa7BWfnyCswAvBVa.htm)|Second Doctrine (Cloistered Cleric)|auto-trad|
|[cleric-07-gxNxfN9OBlQ1icus.htm](classfeatures/cleric-07-gxNxfN9OBlQ1icus.htm)|Third Doctrine|auto-trad|
|[cleric-07-s8WEmc4GGZSHSC7q.htm](classfeatures/cleric-07-s8WEmc4GGZSHSC7q.htm)|Third Doctrine (Cloistered Cleric)|auto-trad|
|[cleric-07-Zp81uTBItG1xlH4O.htm](classfeatures/cleric-07-Zp81uTBItG1xlH4O.htm)|Third Doctrine (Warpriest)|auto-trad|
|[cleric-11-o8nHreMyiLi64rZz.htm](classfeatures/cleric-11-o8nHreMyiLi64rZz.htm)|Fourth Doctrine|auto-trad|
|[cleric-11-px3gVYp7zlEQIpcl.htm](classfeatures/cleric-11-px3gVYp7zlEQIpcl.htm)|Fourth Doctrine (Warpriest)|auto-trad|
|[cleric-11-vxOf4LXZcqUG3P7a.htm](classfeatures/cleric-11-vxOf4LXZcqUG3P7a.htm)|Fourth Doctrine (Cloistered Cleric)|auto-trad|
|[cleric-13-0mJTp4LdEHBLInoe.htm](classfeatures/cleric-13-0mJTp4LdEHBLInoe.htm)|Divine Defense|auto-trad|
|[cleric-15-kmimy4VOaoEOgOiQ.htm](classfeatures/cleric-15-kmimy4VOaoEOgOiQ.htm)|Fifth Doctrine (Warpriest)|auto-trad|
|[cleric-15-n9W8MjjRgPpUTvWf.htm](classfeatures/cleric-15-n9W8MjjRgPpUTvWf.htm)|Fifth Doctrine (Cloistered Cleric)|auto-trad|
|[cleric-15-Zb7DuGbFoLEp0H1K.htm](classfeatures/cleric-15-Zb7DuGbFoLEp0H1K.htm)|Fifth Doctrine|auto-trad|
|[cleric-19-3uf31A91h3ywmlqm.htm](classfeatures/cleric-19-3uf31A91h3ywmlqm.htm)|Miraculous Spell|auto-trad|
|[cleric-19-DgGefatQ4v6xT6f9.htm](classfeatures/cleric-19-DgGefatQ4v6xT6f9.htm)|Final Doctrine (Cloistered Cleric)|auto-trad|
|[cleric-19-N1ugDqZlslxbp3Uy.htm](classfeatures/cleric-19-N1ugDqZlslxbp3Uy.htm)|Final Doctrine (Warpriest)|auto-trad|
|[cleric-19-urBGOPrUwBmkixAo.htm](classfeatures/cleric-19-urBGOPrUwBmkixAo.htm)|Final Doctrine|auto-trad|
|[druid-01-8STJEFVJISujgpMR.htm](classfeatures/druid-01-8STJEFVJISujgpMR.htm)|Druidic Order|auto-trad|
|[druid-01-acqqlYmti8D9QJi0.htm](classfeatures/druid-01-acqqlYmti8D9QJi0.htm)|Storm Order|auto-trad|
|[druid-01-b8pnRxGuNzG0buuh.htm](classfeatures/druid-01-b8pnRxGuNzG0buuh.htm)|Primal Spellcasting|auto-trad|
|[druid-01-d5BFFHXFJYKs5LXr.htm](classfeatures/druid-01-d5BFFHXFJYKs5LXr.htm)|Wild Empathy|auto-trad|
|[druid-01-fKTewWlYgFuhl4KA.htm](classfeatures/druid-01-fKTewWlYgFuhl4KA.htm)|Stone Order|auto-trad|
|[druid-01-FuUXyv2yBs7zRgqT.htm](classfeatures/druid-01-FuUXyv2yBs7zRgqT.htm)|Wave Order|auto-trad|
|[druid-01-NdeFvIXdHwKYLiUj.htm](classfeatures/druid-01-NdeFvIXdHwKYLiUj.htm)|Flame Order|auto-trad|
|[druid-01-nfBn8QB6HVdzpTFV.htm](classfeatures/druid-01-nfBn8QB6HVdzpTFV.htm)|Anathema (Druid)|auto-trad|
|[druid-01-POBvoXifa9HaejAg.htm](classfeatures/druid-01-POBvoXifa9HaejAg.htm)|Animal Order|auto-trad|
|[druid-01-RiAGlnnp4S21BAG3.htm](classfeatures/druid-01-RiAGlnnp4S21BAG3.htm)|Druidic Language|auto-trad|
|[druid-01-u4nlOzPj2WHkIj9l.htm](classfeatures/druid-01-u4nlOzPj2WHkIj9l.htm)|Leaf Order|auto-trad|
|[druid-01-v0EjtiwdeMj8ykI0.htm](classfeatures/druid-01-v0EjtiwdeMj8ykI0.htm)|Wild Order|auto-trad|
|[druid-11-Ra32tlqBxHzT6fzN.htm](classfeatures/druid-11-Ra32tlqBxHzT6fzN.htm)|Druid Weapon Expertise|auto-trad|
|[druid-19-nzgb43mQmLgaqDoQ.htm](classfeatures/druid-19-nzgb43mQmLgaqDoQ.htm)|Primal Hierophant|auto-trad|
|[fighter-01-eZNCckLzbH3GyncH.htm](classfeatures/fighter-01-eZNCckLzbH3GyncH.htm)|Shield Block|auto-trad|
|[fighter-01-hmShTfPOcTaKgbf4.htm](classfeatures/fighter-01-hmShTfPOcTaKgbf4.htm)|Attack of Opportunity|auto-trad|
|[fighter-03-GJKJafDGuX4BeAeN.htm](classfeatures/fighter-03-GJKJafDGuX4BeAeN.htm)|Bravery|auto-trad|
|[fighter-05-gApJtAdNb9ST4Ms9.htm](classfeatures/fighter-05-gApJtAdNb9ST4Ms9.htm)|Fighter Weapon Mastery|auto-trad|
|[fighter-07-TIvzBALymvb56L79.htm](classfeatures/fighter-07-TIvzBALymvb56L79.htm)|Battlefield Surveyor|auto-trad|
|[fighter-09-8g6HzARbhfcgilP8.htm](classfeatures/fighter-09-8g6HzARbhfcgilP8.htm)|Combat Flexibility|auto-trad|
|[fighter-11-bAaI7h937Nr3g93U.htm](classfeatures/fighter-11-bAaI7h937Nr3g93U.htm)|Fighter Expertise|auto-trad|
|[fighter-13-F5VenhIQMDkeGvmV.htm](classfeatures/fighter-13-F5VenhIQMDkeGvmV.htm)|Weapon Legend|auto-trad|
|[fighter-15-W2rwudMNcAxs8VoX.htm](classfeatures/fighter-15-W2rwudMNcAxs8VoX.htm)|Improved Flexibility|auto-trad|
|[fighter-19-0H2LxtiZTJ275pSD.htm](classfeatures/fighter-19-0H2LxtiZTJ275pSD.htm)|Versatile Legend|auto-trad|
|[gunslinger-01-a3pSIKkDVTvvNSRO.htm](classfeatures/gunslinger-01-a3pSIKkDVTvvNSRO.htm)|Way of the Drifter|auto-trad|
|[gunslinger-01-LDqVxLKrwEqSegiu.htm](classfeatures/gunslinger-01-LDqVxLKrwEqSegiu.htm)|Gunslinger's Way|auto-trad|
|[gunslinger-01-OmgtSDV1FubDUqWR.htm](classfeatures/gunslinger-01-OmgtSDV1FubDUqWR.htm)|Way of the Spellshot|auto-trad|
|[gunslinger-01-qRLRrHf0kzaJ7xt0.htm](classfeatures/gunslinger-01-qRLRrHf0kzaJ7xt0.htm)|Way of the Pistolero|auto-trad|
|[gunslinger-01-QWXvksGJhOjXbBqi.htm](classfeatures/gunslinger-01-QWXvksGJhOjXbBqi.htm)|Way of the Sniper|auto-trad|
|[gunslinger-01-vB0yVFxJVZwalt2g.htm](classfeatures/gunslinger-01-vB0yVFxJVZwalt2g.htm)|Way of the Vanguard|auto-trad|
|[gunslinger-01-vXbk7Nm1TOTTUNvF.htm](classfeatures/gunslinger-01-vXbk7Nm1TOTTUNvF.htm)|Singular Expertise|auto-trad|
|[gunslinger-03-Wpdeh6EkcAKA60oH.htm](classfeatures/gunslinger-03-Wpdeh6EkcAKA60oH.htm)|Stubborn|auto-trad|
|[gunslinger-05-9nRT8aq05Fy2D3i3.htm](classfeatures/gunslinger-05-9nRT8aq05Fy2D3i3.htm)|Gunslinger Weapon Mastery|auto-trad|
|[gunslinger-09-3JLW5vPshsJf3nCY.htm](classfeatures/gunslinger-09-3JLW5vPshsJf3nCY.htm)|Advanced Deed|auto-trad|
|[gunslinger-09-aKr6OE8vI2BsJzf1.htm](classfeatures/gunslinger-09-aKr6OE8vI2BsJzf1.htm)|Gunslinger Expertise|auto-trad|
|[gunslinger-13-ULOAZWZEokbJC6Rq.htm](classfeatures/gunslinger-13-ULOAZWZEokbJC6Rq.htm)|Gunslinging Legend|auto-trad|
|[gunslinger-15-yc9RuXXxmZ9YidH6.htm](classfeatures/gunslinger-15-yc9RuXXxmZ9YidH6.htm)|Greater Deed|auto-trad|
|[gunslinger-17-RkofVX55ciXZyfAA.htm](classfeatures/gunslinger-17-RkofVX55ciXZyfAA.htm)|Shootist's Edge|auto-trad|
|[inventor-01-5Xj38QeMKcFdrzqH.htm](classfeatures/inventor-01-5Xj38QeMKcFdrzqH.htm)|Phlogistonic Regulator|auto-trad|
|[inventor-01-bok3P78CMchFibxC.htm](classfeatures/inventor-01-bok3P78CMchFibxC.htm)|Weapon Innovation|auto-trad|
|[inventor-01-dsy2w4LfjMIWgy5D.htm](classfeatures/inventor-01-dsy2w4LfjMIWgy5D.htm)|Harmonic Oscillator|auto-trad|
|[inventor-01-fpwtpm8pdwO1I6MO.htm](classfeatures/inventor-01-fpwtpm8pdwO1I6MO.htm)|Armor Innovation|auto-trad|
|[inventor-01-JH6um0St37UrjLNG.htm](classfeatures/inventor-01-JH6um0St37UrjLNG.htm)|Peerless Inventor|auto-trad|
|[inventor-01-jHE4fPwU0sSIAjMo.htm](classfeatures/inventor-01-jHE4fPwU0sSIAjMo.htm)|Otherworldly Protection|auto-trad|
|[inventor-01-jIAgXe2FetAKBwt7.htm](classfeatures/inventor-01-jIAgXe2FetAKBwt7.htm)|Innovation|auto-trad|
|[inventor-01-o70O2FysDd7BS9e0.htm](classfeatures/inventor-01-o70O2FysDd7BS9e0.htm)|Construct Innovation|auto-trad|
|[inventor-01-OkxoJWrOXhM25mhi.htm](classfeatures/inventor-01-OkxoJWrOXhM25mhi.htm)|Muscular Exoskeleton|auto-trad|
|[inventor-01-oP5zM5Yu41xcx3iu.htm](classfeatures/inventor-01-oP5zM5Yu41xcx3iu.htm)|Overdrive|auto-trad|
|[inventor-01-pEm1RTNuzzQVKkR0.htm](classfeatures/inventor-01-pEm1RTNuzzQVKkR0.htm)|Explode|auto-trad|
|[inventor-01-sTWY6PLqr1X7icgZ.htm](classfeatures/inventor-01-sTWY6PLqr1X7icgZ.htm)|Speed Boosters|auto-trad|
|[inventor-01-X3TtVdNhrydeQ3SX.htm](classfeatures/inventor-01-X3TtVdNhrydeQ3SX.htm)|Subtle Dampeners|auto-trad|
|[inventor-01-xndlv9T3JgYYUtf8.htm](classfeatures/inventor-01-xndlv9T3JgYYUtf8.htm)|Metallic Reactance|auto-trad|
|[inventor-03-J46wcNqKXvtokBD1.htm](classfeatures/inventor-03-J46wcNqKXvtokBD1.htm)|Reconfigure|auto-trad|
|[inventor-03-YMKxN56w617BYwu4.htm](classfeatures/inventor-03-YMKxN56w617BYwu4.htm)|Expert Overdrive|auto-trad|
|[inventor-05-0NyPgi6UACMTmAGE.htm](classfeatures/inventor-05-0NyPgi6UACMTmAGE.htm)|Inventor Weapon Expertise|auto-trad|
|[inventor-07-78HIjRbGoONMpF31.htm](classfeatures/inventor-07-78HIjRbGoONMpF31.htm)|Breakthrough Innovation|auto-trad|
|[inventor-07-SXv9bJFbntDOMRIL.htm](classfeatures/inventor-07-SXv9bJFbntDOMRIL.htm)|Master Overdrive|auto-trad|
|[inventor-09-F8oXHnu9iNTcpXbJ.htm](classfeatures/inventor-09-F8oXHnu9iNTcpXbJ.htm)|Offensive Boost|auto-trad|
|[inventor-09-mQVC1iDyNi2tfsF8.htm](classfeatures/inventor-09-mQVC1iDyNi2tfsF8.htm)|Inventive Expertise|auto-trad|
|[inventor-13-j0klWHkH3AxUAgok.htm](classfeatures/inventor-13-j0klWHkH3AxUAgok.htm)|Complete Reconfiguration|auto-trad|
|[inventor-13-mJpPaoVlNmTK47x1.htm](classfeatures/inventor-13-mJpPaoVlNmTK47x1.htm)|Inventor Weapon Mastery|auto-trad|
|[inventor-15-o1omL2LdHvjEwh3P.htm](classfeatures/inventor-15-o1omL2LdHvjEwh3P.htm)|Legendary Overdrive|auto-trad|
|[inventor-15-tXbadIT3LzwuSR19.htm](classfeatures/inventor-15-tXbadIT3LzwuSR19.htm)|Revolutionary Innovation|auto-trad|
|[inventor-15-vUt71oX9QNLAUmZn.htm](classfeatures/inventor-15-vUt71oX9QNLAUmZn.htm)|Energy Barrier|auto-trad|
|[inventor-17-Uu8VnpAo3XZZEKPd.htm](classfeatures/inventor-17-Uu8VnpAo3XZZEKPd.htm)|Inventive Mastery|auto-trad|
|[inventor-19-rOaLbipkComjc6qh.htm](classfeatures/inventor-19-rOaLbipkComjc6qh.htm)|Infinite Invention|auto-trad|
|[investigator-01-2Fe4YZCvAr9Yf6w7.htm](classfeatures/investigator-01-2Fe4YZCvAr9Yf6w7.htm)|Strategic Strike|auto-trad|
|[investigator-01-6FasgIXUJ1X8ekRn.htm](classfeatures/investigator-01-6FasgIXUJ1X8ekRn.htm)|On the Case|auto-trad|
|[investigator-01-g3mNzNphtVxyR9Xr.htm](classfeatures/investigator-01-g3mNzNphtVxyR9Xr.htm)|Empiricism Methodology|auto-trad|
|[investigator-01-lgo65ldX7WkXC8Ir.htm](classfeatures/investigator-01-lgo65ldX7WkXC8Ir.htm)|Devise a Stratagem|auto-trad|
|[investigator-01-O3IX7rTxXWWvDVM3.htm](classfeatures/investigator-01-O3IX7rTxXWWvDVM3.htm)|Forensic Medicine Methodology|auto-trad|
|[investigator-01-uhHg9BXBiHpL5ndS.htm](classfeatures/investigator-01-uhHg9BXBiHpL5ndS.htm)|Methodology|auto-trad|
|[investigator-01-UIHUNNYZyQ3p4Vmo.htm](classfeatures/investigator-01-UIHUNNYZyQ3p4Vmo.htm)|Interrogation Methodology|auto-trad|
|[investigator-03-dmK1wya8GBi9MmCB.htm](classfeatures/investigator-03-dmK1wya8GBi9MmCB.htm)|Skillful Lessons|auto-trad|
|[investigator-03-DZWQspPi4IkfXV2E.htm](classfeatures/investigator-03-DZWQspPi4IkfXV2E.htm)|Keen Recollection|auto-trad|
|[investigator-09-PFvB79O2VFdiAeSj.htm](classfeatures/investigator-09-PFvB79O2VFdiAeSj.htm)|Investigator Expertise|auto-trad|
|[investigator-11-malYpr0CYL4fDGhr.htm](classfeatures/investigator-11-malYpr0CYL4fDGhr.htm)|Deductive Improvisation|auto-trad|
|[investigator-19-flEx8eY0NinF9XZU.htm](classfeatures/investigator-19-flEx8eY0NinF9XZU.htm)|Master Detective|auto-trad|
|[magus-01-09iL38CZZEa0q0Mt.htm](classfeatures/magus-01-09iL38CZZEa0q0Mt.htm)|Arcane Cascade|auto-trad|
|[magus-01-3gVDqDPSz4fB5T9G.htm](classfeatures/magus-01-3gVDqDPSz4fB5T9G.htm)|Laughing Shadow|auto-trad|
|[magus-01-6YJ8KFl7THkVy6Gm.htm](classfeatures/magus-01-6YJ8KFl7THkVy6Gm.htm)|Twisting Tree|auto-trad|
|[magus-01-FkbFgmoVz5lHhSMo.htm](classfeatures/magus-01-FkbFgmoVz5lHhSMo.htm)|Conflux Spells|auto-trad|
|[magus-01-FTeIs1Z1Qeli4BIF.htm](classfeatures/magus-01-FTeIs1Z1Qeli4BIF.htm)|Hybrid Study|auto-trad|
|[magus-01-KVj5ofUwu3VJSrVw.htm](classfeatures/magus-01-KVj5ofUwu3VJSrVw.htm)|Spellstrike|auto-trad|
|[magus-01-maGzhKLmgubAdUlN.htm](classfeatures/magus-01-maGzhKLmgubAdUlN.htm)|Sparkling Targe|auto-trad|
|[magus-01-Pew7duAozEeAemif.htm](classfeatures/magus-01-Pew7duAozEeAemif.htm)|Starlit Span|auto-trad|
|[magus-01-wXaz41gwqNtTn6tf.htm](classfeatures/magus-01-wXaz41gwqNtTn6tf.htm)|Arcane Spellcasting (Magus)|auto-trad|
|[magus-01-ZslXrvYRxHBXc1Ds.htm](classfeatures/magus-01-ZslXrvYRxHBXc1Ds.htm)|Inexorable Iron|auto-trad|
|[magus-07-6HCI2iHyBZAr7a4P.htm](classfeatures/magus-07-6HCI2iHyBZAr7a4P.htm)|Studious Spells|auto-trad|
|[magus-19-VmPIJomEdmgGrCMS.htm](classfeatures/magus-19-VmPIJomEdmgGrCMS.htm)|Double Spellstrike|auto-trad|
|[mental-01-WZUCvxqbigXos1L9.htm](classfeatures/mental-01-WZUCvxqbigXos1L9.htm)|Rage|auto-trad|
|[monk-01-NLHHHiAcdnZ5ohc2.htm](classfeatures/monk-01-NLHHHiAcdnZ5ohc2.htm)|Flurry of Blows|auto-trad|
|[monk-01-SB8UJ8rZmvbcBweJ.htm](classfeatures/monk-01-SB8UJ8rZmvbcBweJ.htm)|Powerful Fist|auto-trad|
|[monk-03-Cq6NjvcKZOMySBVj.htm](classfeatures/monk-03-Cq6NjvcKZOMySBVj.htm)|Incredible Movement|auto-trad|
|[monk-03-D2AE8RfMlZ3D1FuV.htm](classfeatures/monk-03-D2AE8RfMlZ3D1FuV.htm)|Mystic Strikes|auto-trad|
|[monk-05-VgZIutWjFl8oZQFi.htm](classfeatures/monk-05-VgZIutWjFl8oZQFi.htm)|Expert Strikes|auto-trad|
|[monk-07-1K6m6AVmn3r8XZ9d.htm](classfeatures/monk-07-1K6m6AVmn3r8XZ9d.htm)|Path to Perfection|auto-trad|
|[monk-07-7lanxgmoOHNdtDe2.htm](classfeatures/monk-07-7lanxgmoOHNdtDe2.htm)|Path to Perfection (Will)|auto-trad|
|[monk-07-Bwr9G9IR4ynm5wzz.htm](classfeatures/monk-07-Bwr9G9IR4ynm5wzz.htm)|Path to Perfection (Reflex)|auto-trad|
|[monk-07-KIqptJsjq9pS9CP7.htm](classfeatures/monk-07-KIqptJsjq9pS9CP7.htm)|Path to Perfection (Fortitude)|auto-trad|
|[monk-09-CoRfFkisEsHE1e43.htm](classfeatures/monk-09-CoRfFkisEsHE1e43.htm)|Metal Strikes|auto-trad|
|[monk-09-lxImO5D0qWp0gXFB.htm](classfeatures/monk-09-lxImO5D0qWp0gXFB.htm)|Monk Expertise|auto-trad|
|[monk-11-RVPhB0RqmoJg7xI6.htm](classfeatures/monk-11-RVPhB0RqmoJg7xI6.htm)|Second Path to Perfection (Will)|auto-trad|
|[monk-11-XZnPwZ0ohlDXlFea.htm](classfeatures/monk-11-XZnPwZ0ohlDXlFea.htm)|Second Path to Perfection (Fortitude)|auto-trad|
|[monk-11-y6qnbUc8y0815QNE.htm](classfeatures/monk-11-y6qnbUc8y0815QNE.htm)|Second Path to Perfection|auto-trad|
|[monk-11-yDL9l9Klki6gE2ZD.htm](classfeatures/monk-11-yDL9l9Klki6gE2ZD.htm)|Second Path to Perfection (Reflex)|auto-trad|
|[monk-13-0iidKkzC2yy13lIf.htm](classfeatures/monk-13-0iidKkzC2yy13lIf.htm)|Master Strikes|auto-trad|
|[monk-13-95LI24ZSx0d4qfKX.htm](classfeatures/monk-13-95LI24ZSx0d4qfKX.htm)|Graceful Mastery|auto-trad|
|[monk-15-8kukH9c4h82e3qjl.htm](classfeatures/monk-15-8kukH9c4h82e3qjl.htm)|Third Path to Perfection (Reflex)|auto-trad|
|[monk-15-dUMsM0yDTCdV31p6.htm](classfeatures/monk-15-dUMsM0yDTCdV31p6.htm)|Third Path to Perfection (Fortitude)|auto-trad|
|[monk-15-haoTkr2U5k7kaAKN.htm](classfeatures/monk-15-haoTkr2U5k7kaAKN.htm)|Third Path to Perfection|auto-trad|
|[monk-15-oVNRYF0FHbH8NsJD.htm](classfeatures/monk-15-oVNRYF0FHbH8NsJD.htm)|Third Path to Perfection (Will)|auto-trad|
|[monk-17-5cthRUkRqRtduVvN.htm](classfeatures/monk-17-5cthRUkRqRtduVvN.htm)|Adamantine Strikes|auto-trad|
|[monk-17-JWDfzYub3JfuEtth.htm](classfeatures/monk-17-JWDfzYub3JfuEtth.htm)|Graceful Legend|auto-trad|
|[monk-19-KmTfg7Sg5va4yU00.htm](classfeatures/monk-19-KmTfg7Sg5va4yU00.htm)|Perfected Form|auto-trad|
|[None-00-6RAHnACKReBW68Sa.htm](classfeatures/None-00-6RAHnACKReBW68Sa.htm)|Encroaching Presence|auto-trad|
|[None-00-7PutMrgKIaAeKBuU.htm](classfeatures/None-00-7PutMrgKIaAeKBuU.htm)|Catharsis Emotion (Fear)|auto-trad|
|[None-00-8Jjy7VsQAJJqCeyE.htm](classfeatures/None-00-8Jjy7VsQAJJqCeyE.htm)|Wraith Deviant Classification|auto-trad|
|[None-00-8PjTI21Mif26XWY7.htm](classfeatures/None-00-8PjTI21Mif26XWY7.htm)|Energetic Meltdown|auto-trad|
|[None-00-ccrTv2j7eplr2JU8.htm](classfeatures/None-00-ccrTv2j7eplr2JU8.htm)|Catharsis Emotion (Pride)|auto-trad|
|[None-00-eNFYMQMS2BszErCX.htm](classfeatures/None-00-eNFYMQMS2BszErCX.htm)|Catharsis Emotion (Dedication)|auto-trad|
|[None-00-HPYo5j8GgQIVoOOr.htm](classfeatures/None-00-HPYo5j8GgQIVoOOr.htm)|Troll Deviant Classification|auto-trad|
|[None-00-huDGjojm3hnuA1E8.htm](classfeatures/None-00-huDGjojm3hnuA1E8.htm)|Strained Metabolism|auto-trad|
|[None-00-igMHwREgpM9GsvLs.htm](classfeatures/None-00-igMHwREgpM9GsvLs.htm)|Order of the Gate|auto-trad|
|[None-00-jklanV9AbZDnZHaD.htm](classfeatures/None-00-jklanV9AbZDnZHaD.htm)|Catharsis Emotion (Misery)|auto-trad|
|[None-00-lDjKVZ48zbqQf3CU.htm](classfeatures/None-00-lDjKVZ48zbqQf3CU.htm)|Catharsis Emotion (Love)|auto-trad|
|[None-00-lVdfcITy5bkywW5f.htm](classfeatures/None-00-lVdfcITy5bkywW5f.htm)|Order of the Rack|auto-trad|
|[None-00-MizJPiKnopfpGmvw.htm](classfeatures/None-00-MizJPiKnopfpGmvw.htm)|Catharsis Emotion (Anger)|auto-trad|
|[None-00-R41sy7weOd0JhOiW.htm](classfeatures/None-00-R41sy7weOd0JhOiW.htm)|Dragon Deviant Classification|auto-trad|
|[None-00-rgLaxC3I2L7HSPNn.htm](classfeatures/None-00-rgLaxC3I2L7HSPNn.htm)|Catharsis Emotion (Awe)|auto-trad|
|[None-00-S4uY5ap2yhDRqhd0.htm](classfeatures/None-00-S4uY5ap2yhDRqhd0.htm)|Catharsis Emotion (Hatred)|auto-trad|
|[None-00-t01K3DB2qHnbt1q3.htm](classfeatures/None-00-t01K3DB2qHnbt1q3.htm)|Order of the Scourge|auto-trad|
|[None-00-T9gUmwghw16itUAO.htm](classfeatures/None-00-T9gUmwghw16itUAO.htm)|Catharsis Emotion (Remorse)|auto-trad|
|[None-00-ub9gwFXnMuKvhnPL.htm](classfeatures/None-00-ub9gwFXnMuKvhnPL.htm)|Order of the Nail|auto-trad|
|[None-00-UTRDN1TAieBMjwP1.htm](classfeatures/None-00-UTRDN1TAieBMjwP1.htm)|Order of the Godclaw|auto-trad|
|[None-00-YrJj8UI0XpkHv0Ho.htm](classfeatures/None-00-YrJj8UI0XpkHv0Ho.htm)|Order of the Chain|auto-trad|
|[None-00-zFY2PlJTObXpcQW4.htm](classfeatures/None-00-zFY2PlJTObXpcQW4.htm)|Catharsis Emotion (Joy)|auto-trad|
|[None-00-zGxO2cETUsXuvqRu.htm](classfeatures/None-00-zGxO2cETUsXuvqRu.htm)|Order of the Pyre|auto-trad|
|[None-01-1FPVkksuE2ncw9rF.htm](classfeatures/None-01-1FPVkksuE2ncw9rF.htm)|Ki Spells|auto-trad|
|[None-01-1tyNn9sduyexXLfL.htm](classfeatures/None-01-1tyNn9sduyexXLfL.htm)|Psi Cantrips and Amps|auto-trad|
|[None-01-HwUps0waR29bwlTI.htm](classfeatures/None-01-HwUps0waR29bwlTI.htm)|Unleash Psyche|auto-trad|
|[None-01-HYTaibaCGE85rhbZ.htm](classfeatures/None-01-HYTaibaCGE85rhbZ.htm)|Runelord Specialization|auto-trad|
|[None-01-mRvyq7G0rqRP1EAr.htm](classfeatures/None-01-mRvyq7G0rqRP1EAr.htm)|Wellspring Magic|auto-trad|
|[None-01-pUkUC8HHom2DmYzz.htm](classfeatures/None-01-pUkUC8HHom2DmYzz.htm)|Elemental Magic|auto-trad|
|[None-01-T25ZLQWn6O4KchLo.htm](classfeatures/None-01-T25ZLQWn6O4KchLo.htm)|Focus Spells|auto-trad|
|[None-01-Upf1LXtWNJ6eB5sm.htm](classfeatures/None-01-Upf1LXtWNJ6eB5sm.htm)|Flexible Spell Preparation|auto-trad|
|[None-03-D8CSi8c9XiRpVc5M.htm](classfeatures/None-03-D8CSi8c9XiRpVc5M.htm)|Alertness|auto-trad|
|[None-03-F57Na5VxfBp56kke.htm](classfeatures/None-03-F57Na5VxfBp56kke.htm)|Great Fortitude|auto-trad|
|[None-03-TUOeATt52P43r5W0.htm](classfeatures/None-03-TUOeATt52P43r5W0.htm)|Lightning Reflexes|auto-trad|
|[None-03-wMyDcVNmA7xGK83S.htm](classfeatures/None-03-wMyDcVNmA7xGK83S.htm)|Iron Will|auto-trad|
|[None-05-70jqXP2eS4tRZ0Ok.htm](classfeatures/None-05-70jqXP2eS4tRZ0Ok.htm)|Magical Fortitude|auto-trad|
|[None-05-9XLUh9iMepZesdmc.htm](classfeatures/None-05-9XLUh9iMepZesdmc.htm)|Weapon Expertise|auto-trad|
|[None-07-0npO4rPscGm0dX13.htm](classfeatures/None-07-0npO4rPscGm0dX13.htm)|Vigilant Senses|auto-trad|
|[None-07-9EqIasqfI8YIM3Pt.htm](classfeatures/None-07-9EqIasqfI8YIM3Pt.htm)|Weapon Specialization|auto-trad|
|[None-07-cD3nSupdCvONuHiE.htm](classfeatures/None-07-cD3nSupdCvONuHiE.htm)|Expert Spellcaster|auto-trad|
|[None-07-JQAujUXjczVnYDEI.htm](classfeatures/None-07-JQAujUXjczVnYDEI.htm)|Resolve|auto-trad|
|[None-07-MV6XIuAgN9uSA0Da.htm](classfeatures/None-07-MV6XIuAgN9uSA0Da.htm)|Evasion|auto-trad|
|[None-07-OMZs5y16jZRW9KQK.htm](classfeatures/None-07-OMZs5y16jZRW9KQK.htm)|Juggernaut|auto-trad|
|[None-07-x5jaCJxsmD5sx3KB.htm](classfeatures/None-07-x5jaCJxsmD5sx3KB.htm)|Armor Expertise|auto-trad|
|[None-11-esyKPSDbFQPB4lhq.htm](classfeatures/None-11-esyKPSDbFQPB4lhq.htm)|Medium Armor Expertise (Inventor)|auto-trad|
|[None-11-FCEp9jjxxgRJDJV3.htm](classfeatures/None-11-FCEp9jjxxgRJDJV3.htm)|Medium Armor Expertise|auto-trad|
|[None-13-a58MGVX2L589sC9g.htm](classfeatures/None-13-a58MGVX2L589sC9g.htm)|Psychic Weapon Specialization|auto-trad|
|[None-13-CGB1TczFhQhdQxml.htm](classfeatures/None-13-CGB1TczFhQhdQxml.htm)|Armor Mastery|auto-trad|
|[None-13-L5D0NwFXdLiVSnk5.htm](classfeatures/None-13-L5D0NwFXdLiVSnk5.htm)|Improved Evasion|auto-trad|
|[None-13-nLwPMPLRne1HnL00.htm](classfeatures/None-13-nLwPMPLRne1HnL00.htm)|Incredible Senses|auto-trad|
|[None-15-l1InYvhnQSz6Ucxc.htm](classfeatures/None-15-l1InYvhnQSz6Ucxc.htm)|Master Spellcaster|auto-trad|
|[None-17-cGMSYAErbUG5E8X2.htm](classfeatures/None-17-cGMSYAErbUG5E8X2.htm)|Medium Armor Mastery|auto-trad|
|[oracle-01-7AVspOB6ITNzGFZi.htm](classfeatures/oracle-01-7AVspOB6ITNzGFZi.htm)|Divine Spellcasting (Oracle)|auto-trad|
|[oracle-01-cFe6vFb3gSDyNeS9.htm](classfeatures/oracle-01-cFe6vFb3gSDyNeS9.htm)|Spell Repertoire (Oracle)|auto-trad|
|[oracle-01-EslxR2sbDK9XJaAl.htm](classfeatures/oracle-01-EslxR2sbDK9XJaAl.htm)|Time|auto-trad|
|[oracle-01-gjOGOR30Czpnx3tM.htm](classfeatures/oracle-01-gjOGOR30Czpnx3tM.htm)|Battle|auto-trad|
|[oracle-01-GTSvbFb36InvuH0w.htm](classfeatures/oracle-01-GTSvbFb36InvuH0w.htm)|Flames|auto-trad|
|[oracle-01-IaxmCkdsPlA52spu.htm](classfeatures/oracle-01-IaxmCkdsPlA52spu.htm)|Bones|auto-trad|
|[oracle-01-ibX2EhKkyUtbOHLj.htm](classfeatures/oracle-01-ibX2EhKkyUtbOHLj.htm)|Oracular Curse|auto-trad|
|[oracle-01-NXUOtO9NytHQurlg.htm](classfeatures/oracle-01-NXUOtO9NytHQurlg.htm)|Revelation Spells|auto-trad|
|[oracle-01-o1gGG36wpn9mxeop.htm](classfeatures/oracle-01-o1gGG36wpn9mxeop.htm)|Life|auto-trad|
|[oracle-01-PRJYLksQEwT39bTl.htm](classfeatures/oracle-01-PRJYLksQEwT39bTl.htm)|Mystery|auto-trad|
|[oracle-01-qvRlih3u7vK3FYUR.htm](classfeatures/oracle-01-qvRlih3u7vK3FYUR.htm)|Ancestors|auto-trad|
|[oracle-01-RI2EMRBBPNSoTJXu.htm](classfeatures/oracle-01-RI2EMRBBPNSoTJXu.htm)|Cosmos|auto-trad|
|[oracle-01-tZBb3Kh4nJcNoUFI.htm](classfeatures/oracle-01-tZBb3Kh4nJcNoUFI.htm)|Lore|auto-trad|
|[oracle-01-W9cF7wZztLDb1WGY.htm](classfeatures/oracle-01-W9cF7wZztLDb1WGY.htm)|Tempest|auto-trad|
|[oracle-11-rrzItB68Er0DzKx7.htm](classfeatures/oracle-11-rrzItB68Er0DzKx7.htm)|Major Curse|auto-trad|
|[oracle-17-5LOARurr4qWkfS9K.htm](classfeatures/oracle-17-5LOARurr4qWkfS9K.htm)|Greater Resolve|auto-trad|
|[oracle-17-F4brPlp1tHGUqyuI.htm](classfeatures/oracle-17-F4brPlp1tHGUqyuI.htm)|Extreme Curse|auto-trad|
|[oracle-19-571c1aGnvNVwfF6b.htm](classfeatures/oracle-19-571c1aGnvNVwfF6b.htm)|Oracular Clarity|auto-trad|
|[psychic-01-0fv6NVMZZ0peGL9e.htm](classfeatures/psychic-01-0fv6NVMZZ0peGL9e.htm)|Conscious Mind|auto-trad|
|[psychic-01-0UKDEtZ7ffTEsqCK.htm](classfeatures/psychic-01-0UKDEtZ7ffTEsqCK.htm)|Gathered Lore|auto-trad|
|[psychic-01-1mMdsSIVsyyqNr2t.htm](classfeatures/psychic-01-1mMdsSIVsyyqNr2t.htm)|Spell Repertoire (Psychic)|auto-trad|
|[psychic-01-1rlBo3LcwIuPDCvz.htm](classfeatures/psychic-01-1rlBo3LcwIuPDCvz.htm)|The Distant Grasp|auto-trad|
|[psychic-01-79ZetrRF6S01P4Vf.htm](classfeatures/psychic-01-79ZetrRF6S01P4Vf.htm)|Subconscious Mind|auto-trad|
|[psychic-01-EMErBnhrgUHEKAsZ.htm](classfeatures/psychic-01-EMErBnhrgUHEKAsZ.htm)|Wandering Reverie|auto-trad|
|[psychic-01-iXwqJyBsjJNrKJae.htm](classfeatures/psychic-01-iXwqJyBsjJNrKJae.htm)|Psychic Spellcasting|auto-trad|
|[psychic-01-PNihL10QAB1sYSRn.htm](classfeatures/psychic-01-PNihL10QAB1sYSRn.htm)|Emotional Acceptance|auto-trad|
|[psychic-01-Qrhw4SILfT8YNQgB.htm](classfeatures/psychic-01-Qrhw4SILfT8YNQgB.htm)|Precise Discipline|auto-trad|
|[psychic-05-Ftz5jVa9X6aXybkC.htm](classfeatures/psychic-05-Ftz5jVa9X6aXybkC.htm)|Precognitive Reflexes|auto-trad|
|[psychic-05-k9MeSdp2DbGd1hFz.htm](classfeatures/psychic-05-k9MeSdp2DbGd1hFz.htm)|Clarity of Focus|auto-trad|
|[psychic-09-zAe95Uk5IPIT23K1.htm](classfeatures/psychic-09-zAe95Uk5IPIT23K1.htm)|Great Fortitude (Psychic)|auto-trad|
|[psychic-11-Kf9lSN6pVS2Hy4KI.htm](classfeatures/psychic-11-Kf9lSN6pVS2Hy4KI.htm)|Walls of Will|auto-trad|
|[psychic-11-kLschzVZFoe3U63C.htm](classfeatures/psychic-11-kLschzVZFoe3U63C.htm)|Psychic Weapon Expertise|auto-trad|
|[psychic-11-wOl7EeF7S6i753Ef.htm](classfeatures/psychic-11-wOl7EeF7S6i753Ef.htm)|Extrasensory Perception|auto-trad|
|[psychic-13-MtHLCQGD6OW98WC2.htm](classfeatures/psychic-13-MtHLCQGD6OW98WC2.htm)|Personal Barrier|auto-trad|
|[psychic-17-Hw6Ji7Fgx0XkVkac.htm](classfeatures/psychic-17-Hw6Ji7Fgx0XkVkac.htm)|Fortress of Will|auto-trad|
|[psychic-19-mZwD2brwXlyR9RAR.htm](classfeatures/psychic-19-mZwD2brwXlyR9RAR.htm)|Infinite Mind|auto-trad|
|[ranger-01-0nIOGpHQNHsKSFKT.htm](classfeatures/ranger-01-0nIOGpHQNHsKSFKT.htm)|Hunt Prey|auto-trad|
|[ranger-01-6v4Rj7wWfOH1882r.htm](classfeatures/ranger-01-6v4Rj7wWfOH1882r.htm)|Flurry|auto-trad|
|[ranger-01-mzkkj9LEWjJPBhaq.htm](classfeatures/ranger-01-mzkkj9LEWjJPBhaq.htm)|Hunter's Edge|auto-trad|
|[ranger-01-NBHyoTrI8q62uDsU.htm](classfeatures/ranger-01-NBHyoTrI8q62uDsU.htm)|Outwit|auto-trad|
|[ranger-01-u6cBjqz2fiRBadBt.htm](classfeatures/ranger-01-u6cBjqz2fiRBadBt.htm)|Precision|auto-trad|
|[ranger-01-w3HysrCgDs5uFXKX.htm](classfeatures/ranger-01-w3HysrCgDs5uFXKX.htm)|Warden Spells|auto-trad|
|[ranger-05-PeZi7E9lI4vz8EGY.htm](classfeatures/ranger-05-PeZi7E9lI4vz8EGY.htm)|Trackless Step|auto-trad|
|[ranger-05-QhoW8ivPvYmWzyEZ.htm](classfeatures/ranger-05-QhoW8ivPvYmWzyEZ.htm)|Ranger Weapon Expertise|auto-trad|
|[ranger-09-5likl5SAxQPrQ3KF.htm](classfeatures/ranger-09-5likl5SAxQPrQ3KF.htm)|Ranger Expertise|auto-trad|
|[ranger-09-j2R64kwUgEJ1TudD.htm](classfeatures/ranger-09-j2R64kwUgEJ1TudD.htm)|Nature's Edge|auto-trad|
|[ranger-11-RlwE99yKnhq8FUuy.htm](classfeatures/ranger-11-RlwE99yKnhq8FUuy.htm)|Wild Stride|auto-trad|
|[ranger-17-BJYSUbFUGcTLaPDn.htm](classfeatures/ranger-17-BJYSUbFUGcTLaPDn.htm)|Masterful Hunter (Precision)|auto-trad|
|[ranger-17-JhLncIB10GSQowWL.htm](classfeatures/ranger-17-JhLncIB10GSQowWL.htm)|Masterful Hunter (Flurry)|auto-trad|
|[ranger-17-RVZC4wVy5B5W2OeS.htm](classfeatures/ranger-17-RVZC4wVy5B5W2OeS.htm)|Masterful Hunter|auto-trad|
|[ranger-17-vWZaLE2fEKMBw3D5.htm](classfeatures/ranger-17-vWZaLE2fEKMBw3D5.htm)|Masterful Hunter (Outwit)|auto-trad|
|[ranger-19-bBGb1LcffXEqar0p.htm](classfeatures/ranger-19-bBGb1LcffXEqar0p.htm)|Swift Prey|auto-trad|
|[ranger-19-phwQ2MrDZ13D2HxC.htm](classfeatures/ranger-19-phwQ2MrDZ13D2HxC.htm)|Second Skin|auto-trad|
|[rogue-01-3KPZ7svIO6kmmEKH.htm](classfeatures/rogue-01-3KPZ7svIO6kmmEKH.htm)|Ruffian|auto-trad|
|[rogue-01-D8qtAo2w4jsqjBrM.htm](classfeatures/rogue-01-D8qtAo2w4jsqjBrM.htm)|Eldritch Trickster|auto-trad|
|[rogue-01-j1JE61quDxdge4mg.htm](classfeatures/rogue-01-j1JE61quDxdge4mg.htm)|Sneak Attack|auto-trad|
|[rogue-01-RyOkmu0W9svavuAB.htm](classfeatures/rogue-01-RyOkmu0W9svavuAB.htm)|Mastermind|auto-trad|
|[rogue-01-uGuCGQvUmioFV2Bd.htm](classfeatures/rogue-01-uGuCGQvUmioFV2Bd.htm)|Rogue's Racket|auto-trad|
|[rogue-01-w6rMqmGzhUahdnA7.htm](classfeatures/rogue-01-w6rMqmGzhUahdnA7.htm)|Surprise Attack|auto-trad|
|[rogue-01-wAh2riuFRzz0edPl.htm](classfeatures/rogue-01-wAh2riuFRzz0edPl.htm)|Thief|auto-trad|
|[rogue-01-ZvfxtUMtfIOLYHyg.htm](classfeatures/rogue-01-ZvfxtUMtfIOLYHyg.htm)|Scoundrel|auto-trad|
|[rogue-03-PNpmVmD21zViDtGC.htm](classfeatures/rogue-03-PNpmVmD21zViDtGC.htm)|Deny Advantage|auto-trad|
|[rogue-05-v8UNEJR5IDKi8yqa.htm](classfeatures/rogue-05-v8UNEJR5IDKi8yqa.htm)|Weapon Tricks|auto-trad|
|[rogue-09-9SruVg2lZpNaYLOB.htm](classfeatures/rogue-09-9SruVg2lZpNaYLOB.htm)|Debilitating Strikes|auto-trad|
|[rogue-11-f3Dh32EU4VsHu01b.htm](classfeatures/rogue-11-f3Dh32EU4VsHu01b.htm)|Rogue Expertise|auto-trad|
|[rogue-13-myvcir1LEkaVxOlE.htm](classfeatures/rogue-13-myvcir1LEkaVxOlE.htm)|Master Tricks|auto-trad|
|[rogue-15-W1FkMHYVDg3yTU5r.htm](classfeatures/rogue-15-W1FkMHYVDg3yTU5r.htm)|Double Debilitation|auto-trad|
|[rogue-17-xmZ7oeTDcQVXegUP.htm](classfeatures/rogue-17-xmZ7oeTDcQVXegUP.htm)|Slippery Mind|auto-trad|
|[rogue-19-SUUdWG0t33VKa5q4.htm](classfeatures/rogue-19-SUUdWG0t33VKa5q4.htm)|Master Strike|auto-trad|
|[sorcerer-01-2goYo6VNbwC6aKF1.htm](classfeatures/sorcerer-01-2goYo6VNbwC6aKF1.htm)|Bloodline|auto-trad|
|[sorcerer-01-3qqvnC2U8W26yae7.htm](classfeatures/sorcerer-01-3qqvnC2U8W26yae7.htm)|Bloodline: Aberrant|auto-trad|
|[sorcerer-01-5Wxjghw7lHuCxjZz.htm](classfeatures/sorcerer-01-5Wxjghw7lHuCxjZz.htm)|Bloodline: Nymph|auto-trad|
|[sorcerer-01-7WBZ2kkhZ7JorWu2.htm](classfeatures/sorcerer-01-7WBZ2kkhZ7JorWu2.htm)|Bloodline: Undead|auto-trad|
|[sorcerer-01-dKTb959aCQIzSIXj.htm](classfeatures/sorcerer-01-dKTb959aCQIzSIXj.htm)|Bloodline: Wyrmblessed|auto-trad|
|[sorcerer-01-eW3cfCH7Wpx2vik2.htm](classfeatures/sorcerer-01-eW3cfCH7Wpx2vik2.htm)|Bloodline: Fey|auto-trad|
|[sorcerer-01-gmnx7e1g08bppbqt.htm](classfeatures/sorcerer-01-gmnx7e1g08bppbqt.htm)|Sorcerer Spellcasting|auto-trad|
|[sorcerer-01-H6ziAPvCipTPG8SH.htm](classfeatures/sorcerer-01-H6ziAPvCipTPG8SH.htm)|Bloodline Spells|auto-trad|
|[sorcerer-01-lURKSJZAGKVD6cH9.htm](classfeatures/sorcerer-01-lURKSJZAGKVD6cH9.htm)|Spell Repertoire (Sorcerer)|auto-trad|
|[sorcerer-01-O0uXZRWMNliDbkxU.htm](classfeatures/sorcerer-01-O0uXZRWMNliDbkxU.htm)|Bloodline: Hag|auto-trad|
|[sorcerer-01-o39zQMIdERWtmBSB.htm](classfeatures/sorcerer-01-o39zQMIdERWtmBSB.htm)|Bloodline: Diabolic|auto-trad|
|[sorcerer-01-PpzH9tJULk5ksX9w.htm](classfeatures/sorcerer-01-PpzH9tJULk5ksX9w.htm)|Bloodline: Psychopomp|auto-trad|
|[sorcerer-01-RXRnJcG4XSabZ35a.htm](classfeatures/sorcerer-01-RXRnJcG4XSabZ35a.htm)|Bloodline: Elemental|auto-trad|
|[sorcerer-01-TWR1wbPJuCLnGdFZ.htm](classfeatures/sorcerer-01-TWR1wbPJuCLnGdFZ.htm)|Bloodline: Phoenix|auto-trad|
|[sorcerer-01-tYOMBiH3HbViNWwn.htm](classfeatures/sorcerer-01-tYOMBiH3HbViNWwn.htm)|Bloodline: Genie|auto-trad|
|[sorcerer-01-uoQOm41BVdSo6pAS.htm](classfeatures/sorcerer-01-uoQOm41BVdSo6pAS.htm)|Bloodline: Shadow|auto-trad|
|[sorcerer-01-vhW3glAaEfq2DKrw.htm](classfeatures/sorcerer-01-vhW3glAaEfq2DKrw.htm)|Bloodline: Angelic|auto-trad|
|[sorcerer-01-w5koctOVrEcpxTIq.htm](classfeatures/sorcerer-01-w5koctOVrEcpxTIq.htm)|Bloodline: Demonic|auto-trad|
|[sorcerer-01-ZEtJJ5UOlV5oTWWp.htm](classfeatures/sorcerer-01-ZEtJJ5UOlV5oTWWp.htm)|Bloodline: Imperial|auto-trad|
|[sorcerer-01-ZHabYxSgYK0XbjhM.htm](classfeatures/sorcerer-01-ZHabYxSgYK0XbjhM.htm)|Bloodline: Draconic|auto-trad|
|[sorcerer-03-VKRjmXxBFLrJK01c.htm](classfeatures/sorcerer-03-VKRjmXxBFLrJK01c.htm)|Signature Spells|auto-trad|
|[sorcerer-19-feCnVrPPlKhl701x.htm](classfeatures/sorcerer-19-feCnVrPPlKhl701x.htm)|Bloodline Paragon|auto-trad|
|[summoner-01-gWcN75VNpSZ4FqNb.htm](classfeatures/summoner-01-gWcN75VNpSZ4FqNb.htm)|Summoner Spellcasting|auto-trad|
|[summoner-01-IPcdQAwJk0aZe5mg.htm](classfeatures/summoner-01-IPcdQAwJk0aZe5mg.htm)|Evolution Feat|auto-trad|
|[summoner-01-Ju2Tp5s5iBB76tQO.htm](classfeatures/summoner-01-Ju2Tp5s5iBB76tQO.htm)|Spell Repertoire (Summoner)|auto-trad|
|[summoner-01-qOEpe596B0UjhcG0.htm](classfeatures/summoner-01-qOEpe596B0UjhcG0.htm)|Eidolon|auto-trad|
|[summoner-01-wguqw300DB5XdD8W.htm](classfeatures/summoner-01-wguqw300DB5XdD8W.htm)|Link Spells|auto-trad|
|[summoner-03-P34Jx6i4GJGoqTtG.htm](classfeatures/summoner-03-P34Jx6i4GJGoqTtG.htm)|Unlimited Signature Spells|auto-trad|
|[summoner-03-QiMlJ33kNEoyh1M0.htm](classfeatures/summoner-03-QiMlJ33kNEoyh1M0.htm)|Shared Vigilance|auto-trad|
|[summoner-05-GI5IAl4dkly4At8e.htm](classfeatures/summoner-05-GI5IAl4dkly4At8e.htm)|Ability Boosts|auto-trad|
|[summoner-05-pda6iUaU9waXId5Q.htm](classfeatures/summoner-05-pda6iUaU9waXId5Q.htm)|Eidolon Unarmed Expertise|auto-trad|
|[summoner-07-oCnyGRvkfjTsZXcX.htm](classfeatures/summoner-07-oCnyGRvkfjTsZXcX.htm)|Eidolon Weapon Specialization|auto-trad|
|[summoner-07-skQBrwRwJW2K6ACj.htm](classfeatures/summoner-07-skQBrwRwJW2K6ACj.htm)|Eidolon Symbiosis|auto-trad|
|[summoner-09-dZNAXTQovlWVvAyX.htm](classfeatures/summoner-09-dZNAXTQovlWVvAyX.htm)|Shared Reflexes|auto-trad|
|[summoner-11-2CZPYoyWih6zYTcb.htm](classfeatures/summoner-11-2CZPYoyWih6zYTcb.htm)|Eidolon Defensive Expertise|auto-trad|
|[summoner-11-q1Y12Pg2gQg2FJPR.htm](classfeatures/summoner-11-q1Y12Pg2gQg2FJPR.htm)|Twin Juggernauts|auto-trad|
|[summoner-13-NIzHfVcVMhDmvA49.htm](classfeatures/summoner-13-NIzHfVcVMhDmvA49.htm)|Eidolon Unarmed Mastery|auto-trad|
|[summoner-15-B5SyM7qHrU0gTGR0.htm](classfeatures/summoner-15-B5SyM7qHrU0gTGR0.htm)|Greater Eidolon Specialization|auto-trad|
|[summoner-15-eZPfHVz14j42jCnS.htm](classfeatures/summoner-15-eZPfHVz14j42jCnS.htm)|Shared Resolve|auto-trad|
|[summoner-17-nCE9DzkugRefREqT.htm](classfeatures/summoner-17-nCE9DzkugRefREqT.htm)|Eidolon Transcendence|auto-trad|
|[summoner-19-0WvI8KM5m0SaZ3MH.htm](classfeatures/summoner-19-0WvI8KM5m0SaZ3MH.htm)|Eidolon Defensive Mastery|auto-trad|
|[summoner-19-H0iWhiyP0QqgmAKs.htm](classfeatures/summoner-19-H0iWhiyP0QqgmAKs.htm)|Instant Manifestation|auto-trad|
|[swashbuckler-01-4lGhbEjlEoGP4scl.htm](classfeatures/swashbuckler-01-4lGhbEjlEoGP4scl.htm)|Wit|auto-trad|
|[swashbuckler-01-B7RMnrHwQHlezlJT.htm](classfeatures/swashbuckler-01-B7RMnrHwQHlezlJT.htm)|Gymnast|auto-trad|
|[swashbuckler-01-beW1OqibVQ3fBvRw.htm](classfeatures/swashbuckler-01-beW1OqibVQ3fBvRw.htm)|Swashbuckler's Style|auto-trad|
|[swashbuckler-01-Jgid6Ja6Y879COlN.htm](classfeatures/swashbuckler-01-Jgid6Ja6Y879COlN.htm)|Fencer|auto-trad|
|[swashbuckler-01-KBhwFjdptrKyN5EM.htm](classfeatures/swashbuckler-01-KBhwFjdptrKyN5EM.htm)|Braggart|auto-trad|
|[swashbuckler-01-LzYi0OuOoypNb6jd.htm](classfeatures/swashbuckler-01-LzYi0OuOoypNb6jd.htm)|Panache|auto-trad|
|[swashbuckler-01-pyo0vmxUFIFX2GNl.htm](classfeatures/swashbuckler-01-pyo0vmxUFIFX2GNl.htm)|Confident Finisher|auto-trad|
|[swashbuckler-01-RQH6vigvhmiYKKjg.htm](classfeatures/swashbuckler-01-RQH6vigvhmiYKKjg.htm)|Precise Strike|auto-trad|
|[swashbuckler-03-8BOFeRE7ZfJ02N0O.htm](classfeatures/swashbuckler-03-8BOFeRE7ZfJ02N0O.htm)|Vivacious Speed|auto-trad|
|[swashbuckler-03-Jtn7IugykXDlIoZq.htm](classfeatures/swashbuckler-03-Jtn7IugykXDlIoZq.htm)|Opportune Riposte|auto-trad|
|[swashbuckler-03-pthjQIK9pDxnbER6.htm](classfeatures/swashbuckler-03-pthjQIK9pDxnbER6.htm)|Stylish Tricks|auto-trad|
|[swashbuckler-05-F5BHEav90oOJ2LwN.htm](classfeatures/swashbuckler-05-F5BHEav90oOJ2LwN.htm)|Weapon Expertise (Swashbuckler)|auto-trad|
|[swashbuckler-09-KxpaxUSuBC7hr4F7.htm](classfeatures/swashbuckler-09-KxpaxUSuBC7hr4F7.htm)|Exemplary Finisher|auto-trad|
|[swashbuckler-09-U74JoAcLHTOsZG6q.htm](classfeatures/swashbuckler-09-U74JoAcLHTOsZG6q.htm)|Swashbuckler Expertise|auto-trad|
|[swashbuckler-11-13QpCrR8a8XULbJa.htm](classfeatures/swashbuckler-11-13QpCrR8a8XULbJa.htm)|Continuous Flair|auto-trad|
|[swashbuckler-13-i6563IU7x4L9oRgC.htm](classfeatures/swashbuckler-13-i6563IU7x4L9oRgC.htm)|Weapon Mastery|auto-trad|
|[swashbuckler-13-pZYkb12t5DSwtts7.htm](classfeatures/swashbuckler-13-pZYkb12t5DSwtts7.htm)|Light Armor Expertise|auto-trad|
|[swashbuckler-15-Pk3Ht0KZyFxSeL07.htm](classfeatures/swashbuckler-15-Pk3Ht0KZyFxSeL07.htm)|Keen Flair|auto-trad|
|[swashbuckler-15-Z7HX6TeFsaup7Dx9.htm](classfeatures/swashbuckler-15-Z7HX6TeFsaup7Dx9.htm)|Greater Weapon Specialization|auto-trad|
|[swashbuckler-19-SHpjmM4A3Sw4GgDz.htm](classfeatures/swashbuckler-19-SHpjmM4A3Sw4GgDz.htm)|Light Armor Mastery|auto-trad|
|[swashbuckler-19-ypfT3iybew6ZSIUl.htm](classfeatures/swashbuckler-19-ypfT3iybew6ZSIUl.htm)|Eternal Confidence|auto-trad|
|[thaumaturge-01-1vgFGSnn0DIBmK7j.htm](classfeatures/thaumaturge-01-1vgFGSnn0DIBmK7j.htm)|Chalice|auto-trad|
|[thaumaturge-01-AltwHU7hCqTwpn48.htm](classfeatures/thaumaturge-01-AltwHU7hCqTwpn48.htm)|Lantern|auto-trad|
|[thaumaturge-01-cvQmPkJtybMcHinK.htm](classfeatures/thaumaturge-01-cvQmPkJtybMcHinK.htm)|Esoteric Lore|auto-trad|
|[thaumaturge-01-DhdLzrcMvB93Rjmt.htm](classfeatures/thaumaturge-01-DhdLzrcMvB93Rjmt.htm)|Regalia|auto-trad|
|[thaumaturge-01-DK1LCE5pd0YCY11c.htm](classfeatures/thaumaturge-01-DK1LCE5pd0YCY11c.htm)|Bell|auto-trad|
|[thaumaturge-01-MyN1cQgE0HsLF20e.htm](classfeatures/thaumaturge-01-MyN1cQgE0HsLF20e.htm)|Tome|auto-trad|
|[thaumaturge-01-N6KvTbaRsphc0Ymb.htm](classfeatures/thaumaturge-01-N6KvTbaRsphc0Ymb.htm)|Mirror|auto-trad|
|[thaumaturge-01-PbNS8d3w3pYQYcVN.htm](classfeatures/thaumaturge-01-PbNS8d3w3pYQYcVN.htm)|Implement's Empowerment|auto-trad|
|[thaumaturge-01-PoclGJ7BCEyIuqJe.htm](classfeatures/thaumaturge-01-PoclGJ7BCEyIuqJe.htm)|Amulet|auto-trad|
|[thaumaturge-01-uwLNfBprqZw2osTb.htm](classfeatures/thaumaturge-01-uwLNfBprqZw2osTb.htm)|Exploit Vulnerability|auto-trad|
|[thaumaturge-01-VSQJtzQE6ikKdsnP.htm](classfeatures/thaumaturge-01-VSQJtzQE6ikKdsnP.htm)|First Implement and Esoterica|auto-trad|
|[thaumaturge-01-YiDkrwaxiF7Gao7y.htm](classfeatures/thaumaturge-01-YiDkrwaxiF7Gao7y.htm)|Weapon|auto-trad|
|[thaumaturge-05-ABYmUcLdxDFXEtzu.htm](classfeatures/thaumaturge-05-ABYmUcLdxDFXEtzu.htm)|Thaumaturge Weapon Expertise|auto-trad|
|[thaumaturge-05-Z8WpDAdAXyefLB7Q.htm](classfeatures/thaumaturge-05-Z8WpDAdAXyefLB7Q.htm)|Second Implement|auto-trad|
|[thaumaturge-07-Obm4ItMIIr0whYeO.htm](classfeatures/thaumaturge-07-Obm4ItMIIr0whYeO.htm)|Implement Adept|auto-trad|
|[thaumaturge-09-VdwNvQwq9sHflEwe.htm](classfeatures/thaumaturge-09-VdwNvQwq9sHflEwe.htm)|Intensify Vulnerability|auto-trad|
|[thaumaturge-09-yvdSUIRU5uLr5eF2.htm](classfeatures/thaumaturge-09-yvdSUIRU5uLr5eF2.htm)|Thaumaturgic Expertise|auto-trad|
|[thaumaturge-11-ZEUxZ4Ta1kDPHiq5.htm](classfeatures/thaumaturge-11-ZEUxZ4Ta1kDPHiq5.htm)|Second Adept|auto-trad|
|[thaumaturge-15-zxZzjN2T53wnH4vU.htm](classfeatures/thaumaturge-15-zxZzjN2T53wnH4vU.htm)|Third Implement|auto-trad|
|[thaumaturge-17-QEtgbY8N2V4wTbsI.htm](classfeatures/thaumaturge-17-QEtgbY8N2V4wTbsI.htm)|Implement Paragon|auto-trad|
|[thaumaturge-17-VywXtJCa0Y9fdGVH.htm](classfeatures/thaumaturge-17-VywXtJCa0Y9fdGVH.htm)|Thaumaturgic Mastery|auto-trad|
|[thaumaturge-19-9ItMYxEkvxqBHrV1.htm](classfeatures/thaumaturge-19-9ItMYxEkvxqBHrV1.htm)|Unlimited Esoterica|auto-trad|
|[witch-01-4IfYHrQMosJNM8hv.htm](classfeatures/witch-01-4IfYHrQMosJNM8hv.htm)|Fervor Patron|auto-trad|
|[witch-01-9uLh5z2uPo6LDFRY.htm](classfeatures/witch-01-9uLh5z2uPo6LDFRY.htm)|Hexes|auto-trad|
|[witch-01-ejmSQOJR5lJv1pzh.htm](classfeatures/witch-01-ejmSQOJR5lJv1pzh.htm)|Rune Patron|auto-trad|
|[witch-01-IuQ7wMznDwvBnmXX.htm](classfeatures/witch-01-IuQ7wMznDwvBnmXX.htm)|Pacts Patron|auto-trad|
|[witch-01-KPtF29AaeX2sJW0K.htm](classfeatures/witch-01-KPtF29AaeX2sJW0K.htm)|Patron|auto-trad|
|[witch-01-NAXRmMjj0gcyD7ie.htm](classfeatures/witch-01-NAXRmMjj0gcyD7ie.htm)|Curse Patron|auto-trad|
|[witch-01-nocYmxbi4rqCC2qS.htm](classfeatures/witch-01-nocYmxbi4rqCC2qS.htm)|Patron Theme|auto-trad|
|[witch-01-qf12ubZ07Q0z0NcN.htm](classfeatures/witch-01-qf12ubZ07Q0z0NcN.htm)|Winter Patron|auto-trad|
|[witch-01-qMZiTugiLCEmkg8h.htm](classfeatures/witch-01-qMZiTugiLCEmkg8h.htm)|Fate Patron|auto-trad|
|[witch-01-VVMMJdIWL7fAsQf3.htm](classfeatures/witch-01-VVMMJdIWL7fAsQf3.htm)|Baba Yaga Patron|auto-trad|
|[witch-01-x2gzQMPvLwHWDdAC.htm](classfeatures/witch-01-x2gzQMPvLwHWDdAC.htm)|Wild Patron|auto-trad|
|[witch-01-XFTWJO6txmLNRLae.htm](classfeatures/witch-01-XFTWJO6txmLNRLae.htm)|Night Patron|auto-trad|
|[witch-01-yksPhweBZYVCsE1A.htm](classfeatures/witch-01-yksPhweBZYVCsE1A.htm)|Familiar (Witch)|auto-trad|
|[witch-01-zT6QiTMxxj8JYoN9.htm](classfeatures/witch-01-zT6QiTMxxj8JYoN9.htm)|Witch Spellcasting|auto-trad|
|[witch-01-zy0toWeGIeQstbT4.htm](classfeatures/witch-01-zy0toWeGIeQstbT4.htm)|Mosquito Witch Patron|auto-trad|
|[witch-02-DtmKrCvsmutVLAhH.htm](classfeatures/witch-02-DtmKrCvsmutVLAhH.htm)|Lesson of Vengeance|auto-trad|
|[witch-02-evKUM58ymuuypRn9.htm](classfeatures/witch-02-evKUM58ymuuypRn9.htm)|Lesson of Dreams|auto-trad|
|[witch-02-FuOHRoEU8nHOXZnk.htm](classfeatures/witch-02-FuOHRoEU8nHOXZnk.htm)|Lesson of Elements|auto-trad|
|[witch-02-HbREpzudMXPscgCj.htm](classfeatures/witch-02-HbREpzudMXPscgCj.htm)|Lesson of Life|auto-trad|
|[witch-02-KHKe3PmctOFUeh85.htm](classfeatures/witch-02-KHKe3PmctOFUeh85.htm)|Lesson of Protection|auto-trad|
|[witch-02-PLMmDXJCDdMS0V5C.htm](classfeatures/witch-02-PLMmDXJCDdMS0V5C.htm)|Lesson of Calamity|auto-trad|
|[witch-06-2IhZbkx889pATIjq.htm](classfeatures/witch-06-2IhZbkx889pATIjq.htm)|Lesson of Favors|auto-trad|
|[witch-06-rECRfbkVwHuG06vO.htm](classfeatures/witch-06-rECRfbkVwHuG06vO.htm)|Lesson of Snow|auto-trad|
|[witch-06-XBgBh3xZhgGQP7lF.htm](classfeatures/witch-06-XBgBh3xZhgGQP7lF.htm)|Lesson of Mischief|auto-trad|
|[witch-06-YSxymsAe2Dvq47f2.htm](classfeatures/witch-06-YSxymsAe2Dvq47f2.htm)|Lesson of Shadow|auto-trad|
|[witch-10-29ynmOCdtIFDzrWx.htm](classfeatures/witch-10-29ynmOCdtIFDzrWx.htm)|Lesson of the Frozen Queen|auto-trad|
|[witch-10-7Y2XDqr4gRisjiAG.htm](classfeatures/witch-10-7Y2XDqr4gRisjiAG.htm)|Lesson of Renewal|auto-trad|
|[witch-10-RcmqV0cuOLcnKQr0.htm](classfeatures/witch-10-RcmqV0cuOLcnKQr0.htm)|Lesson of Bargains|auto-trad|
|[witch-10-wRHeY7tCN6HFFF3a.htm](classfeatures/witch-10-wRHeY7tCN6HFFF3a.htm)|Lesson of Death|auto-trad|
|[witch-19-cDnFXfl3i5Z2l7JP.htm](classfeatures/witch-19-cDnFXfl3i5Z2l7JP.htm)|Patron's Gift|auto-trad|
|[wizard-01-7nbKDBGvwSx9T27G.htm](classfeatures/wizard-01-7nbKDBGvwSx9T27G.htm)|Arcane School|auto-trad|
|[wizard-01-89zWKD2CN7nRu2xp.htm](classfeatures/wizard-01-89zWKD2CN7nRu2xp.htm)|Metamagical Experimentation|auto-trad|
|[wizard-01-au0lwQ1nAcNQwcGh.htm](classfeatures/wizard-01-au0lwQ1nAcNQwcGh.htm)|Arcane Bond|auto-trad|
|[wizard-01-gCwcys8CnS102tji.htm](classfeatures/wizard-01-gCwcys8CnS102tji.htm)|Abjuration|auto-trad|
|[wizard-01-ibhml5y20g5M3Vgd.htm](classfeatures/wizard-01-ibhml5y20g5M3Vgd.htm)|Evocation|auto-trad|
|[wizard-01-K6hG7nH8yjmbA0Q9.htm](classfeatures/wizard-01-K6hG7nH8yjmbA0Q9.htm)|Illusion|auto-trad|
|[wizard-01-Klb35AwlkNrq1gpB.htm](classfeatures/wizard-01-Klb35AwlkNrq1gpB.htm)|Staff Nexus|auto-trad|
|[wizard-01-M89l9FOnjHe63wD7.htm](classfeatures/wizard-01-M89l9FOnjHe63wD7.htm)|Arcane Thesis|auto-trad|
|[wizard-01-OAcxS625AXSGrQIC.htm](classfeatures/wizard-01-OAcxS625AXSGrQIC.htm)|Spell Blending|auto-trad|
|[wizard-01-qczCKdg47eAmCOUD.htm](classfeatures/wizard-01-qczCKdg47eAmCOUD.htm)|Universalist|auto-trad|
|[wizard-01-QzWXMCSGNfvvpYgF.htm](classfeatures/wizard-01-QzWXMCSGNfvvpYgF.htm)|Spell Substitution|auto-trad|
|[wizard-01-rHxkPijLnQ9O9AGV.htm](classfeatures/wizard-01-rHxkPijLnQ9O9AGV.htm)|Transmutation|auto-trad|
|[wizard-01-S6WW4Yyg4XonXGHD.htm](classfeatures/wizard-01-S6WW4Yyg4XonXGHD.htm)|Arcane Spellcasting (Wizard)|auto-trad|
|[wizard-01-SNeVaUBTHwvoO6kr.htm](classfeatures/wizard-01-SNeVaUBTHwvoO6kr.htm)|Conjuration|auto-trad|
|[wizard-01-SNZ46g3u7U6x0XJj.htm](classfeatures/wizard-01-SNZ46g3u7U6x0XJj.htm)|Improved Familiar Attunement|auto-trad|
|[wizard-01-uNM7qZQokRKAEd7k.htm](classfeatures/wizard-01-uNM7qZQokRKAEd7k.htm)|Necromancy|auto-trad|
|[wizard-01-yobGgrHdgjs5QW5o.htm](classfeatures/wizard-01-yobGgrHdgjs5QW5o.htm)|Divination|auto-trad|
|[wizard-01-ZHwGACWQEy6kTzcP.htm](classfeatures/wizard-01-ZHwGACWQEy6kTzcP.htm)|Enchantment|auto-trad|
|[wizard-11-GBsC2cARoFiqMi9V.htm](classfeatures/wizard-11-GBsC2cARoFiqMi9V.htm)|Wizard Weapon Expertise|auto-trad|
|[wizard-13-gU7epgcPSm0TD1UK.htm](classfeatures/wizard-13-gU7epgcPSm0TD1UK.htm)|Defensive Robes|auto-trad|
|[wizard-19-Hfaa7TuLn3nE8lr3.htm](classfeatures/wizard-19-Hfaa7TuLn3nE8lr3.htm)|Legendary Spellcaster|auto-trad|
|[wizard-19-ZjwJHmjPrSs6VDez.htm](classfeatures/wizard-19-ZjwJHmjPrSs6VDez.htm)|Archwizard's Spellcraft|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[alchemist-01-P9quO9XZi3OWFe1k.htm](classfeatures/alchemist-01-P9quO9XZi3OWFe1k.htm)|Toxicologist|Toxicólogo|modificada|
|[barbarian-01-0FtzFbUrN56KA67z.htm](classfeatures/barbarian-01-0FtzFbUrN56KA67z.htm)|Animal Instinct|Instinto animal|modificada|
|[barbarian-01-VDot7CDcXElxmkkz.htm](classfeatures/barbarian-01-VDot7CDcXElxmkkz.htm)|Dragon Instinct|Instinto de dragón|modificada|
|[champion-00-EtltLdiy9kNfHU0c.htm](classfeatures/champion-00-EtltLdiy9kNfHU0c.htm)|Blade Ally|Blade Ally|modificada|
|[champion-01-HiIvez0TqESbleB5.htm](classfeatures/champion-01-HiIvez0TqESbleB5.htm)|Tyrant|Tirano|modificada|
|[champion-03-ERwuazupczhUSZ73.htm](classfeatures/champion-03-ERwuazupczhUSZ73.htm)|Divine Ally|Aliado divino|modificada|
|[gunslinger-01-YryaWAGcHeaRnXzS.htm](classfeatures/gunslinger-01-YryaWAGcHeaRnXzS.htm)|Way of the Triggerbrand|Camino de la Triggerbrand|modificada|
|[investigator-01-ln2Y1a4SxlU9sizX.htm](classfeatures/investigator-01-ln2Y1a4SxlU9sizX.htm)|Alchemical Sciences Methodology|Alchemical Sciences Methodology|modificada|
|[oracle-01-tzDW9l4lBwXCVYtz.htm](classfeatures/oracle-01-tzDW9l4lBwXCVYtz.htm)|Ashes|Cenizas|modificada|
|[psychic-01-5tSR0WzMPFn5s3Xs.htm](classfeatures/psychic-01-5tSR0WzMPFn5s3Xs.htm)|The Oscillating Wave|La onda oscilante|modificada|
|[psychic-01-8dAm3ULUqaK4N5a7.htm](classfeatures/psychic-01-8dAm3ULUqaK4N5a7.htm)|The Tangible Dream|El Sue tangible|modificada|
|[psychic-01-FaSY47siV1x6CAQp.htm](classfeatures/psychic-01-FaSY47siV1x6CAQp.htm)|The Unbound Step|El Paso Sin Ataduras|modificada|
|[psychic-01-rdWWlqvxXgfWDaSO.htm](classfeatures/psychic-01-rdWWlqvxXgfWDaSO.htm)|The Silent Whisper|El susurro silencioso|modificada|
|[psychic-01-rvpTsj9epRuNH3uB.htm](classfeatures/psychic-01-rvpTsj9epRuNH3uB.htm)|The Infinite Eye|El Ojo Infinito|modificada|
|[swashbuckler-01-5HoEwzLDJGTCZtFa.htm](classfeatures/swashbuckler-01-5HoEwzLDJGTCZtFa.htm)|Battledancer|Battledancer|modificada|
|[thaumaturge-01-pDxdE8S8QJV2PGiB.htm](classfeatures/thaumaturge-01-pDxdE8S8QJV2PGiB.htm)|Wand|Varita|modificada|
|[witch-01-SOan0fqyFTrkqJLV.htm](classfeatures/witch-01-SOan0fqyFTrkqJLV.htm)|Witch Lessons|Witch Lessons|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
