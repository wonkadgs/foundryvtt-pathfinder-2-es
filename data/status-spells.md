# Estado de la traducción (spells)

 * **auto-trad**: 1312
 * **modificada**: 36


DÚltima actualización: 2023-03-20 01:04 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[abjuration-01-0cF9HvHzzWSbCFBP.htm](spells/abjuration-01-0cF9HvHzzWSbCFBP.htm)|Empty Inside|auto-trad|
|[abjuration-01-0fKHBh5goe2eiFYL.htm](spells/abjuration-01-0fKHBh5goe2eiFYL.htm)|Negate Aroma|auto-trad|
|[abjuration-01-0uRpypf1Hi7ahvTl.htm](spells/abjuration-01-0uRpypf1Hi7ahvTl.htm)|Shattering Gem|auto-trad|
|[abjuration-01-3wmX7htzOXiHLdAn.htm](spells/abjuration-01-3wmX7htzOXiHLdAn.htm)|Delay Consequence|auto-trad|
|[abjuration-01-4WAib3GichxLjp5p.htm](spells/abjuration-01-4WAib3GichxLjp5p.htm)|Alarm|auto-trad|
|[abjuration-01-53NqGTJOf4LcjVyD.htm](spells/abjuration-01-53NqGTJOf4LcjVyD.htm)|Shielding Strike|auto-trad|
|[abjuration-01-8xRzLhwGL7Dgy3EZ.htm](spells/abjuration-01-8xRzLhwGL7Dgy3EZ.htm)|Sanctuary|auto-trad|
|[abjuration-01-aAbfKn8maGjJjk2W.htm](spells/abjuration-01-aAbfKn8maGjJjk2W.htm)|Mage Armor|auto-trad|
|[abjuration-01-AfOpnnwdZwHi2Tnc.htm](spells/abjuration-01-AfOpnnwdZwHi2Tnc.htm)|Protect Companion|auto-trad|
|[abjuration-01-Azoh0BSoCASrA1lr.htm](spells/abjuration-01-Azoh0BSoCASrA1lr.htm)|Lock|auto-trad|
|[abjuration-01-cDFAQN7Z3es07WSA.htm](spells/abjuration-01-cDFAQN7Z3es07WSA.htm)|Perfected Mind|auto-trad|
|[abjuration-01-cJq5NarY0eOZN74A.htm](spells/abjuration-01-cJq5NarY0eOZN74A.htm)|Share Burden|auto-trad|
|[abjuration-01-dFejDNEmVj3CwYLL.htm](spells/abjuration-01-dFejDNEmVj3CwYLL.htm)|Blood Ward|auto-trad|
|[abjuration-01-gMODOGamz88rgHuf.htm](spells/abjuration-01-gMODOGamz88rgHuf.htm)|Protection|auto-trad|
|[abjuration-01-GzlVI3qYdCtUigLz.htm](spells/abjuration-01-GzlVI3qYdCtUigLz.htm)|Fey Abeyance|auto-trad|
|[abjuration-01-hW9pgce6vTme61g1.htm](spells/abjuration-01-hW9pgce6vTme61g1.htm)|Unfetter Eidolon|auto-trad|
|[abjuration-01-k43PIYwuQqjeJ3S3.htm](spells/abjuration-01-k43PIYwuQqjeJ3S3.htm)|Forced Quiet|auto-trad|
|[abjuration-01-lmHmKGs1N3yNAvln.htm](spells/abjuration-01-lmHmKGs1N3yNAvln.htm)|Wash Your Luck|auto-trad|
|[abjuration-01-MVrxZarUTnJxAUN8.htm](spells/abjuration-01-MVrxZarUTnJxAUN8.htm)|Tether|auto-trad|
|[abjuration-01-OyiKIbWllLZC6sGz.htm](spells/abjuration-01-OyiKIbWllLZC6sGz.htm)|Genie's Veil|auto-trad|
|[abjuration-01-P9bqJsF3WkxGAJKJ.htm](spells/abjuration-01-P9bqJsF3WkxGAJKJ.htm)|Sudden Shift|auto-trad|
|[abjuration-01-PGVhjDbzC4lf6aXF.htm](spells/abjuration-01-PGVhjDbzC4lf6aXF.htm)|Breadcrumbs|auto-trad|
|[abjuration-01-qjUcAMgcSLIamjEq.htm](spells/abjuration-01-qjUcAMgcSLIamjEq.htm)|String of Fate|auto-trad|
|[abjuration-01-R569jdqpNry8m0TJ.htm](spells/abjuration-01-R569jdqpNry8m0TJ.htm)|Guided Introspection|auto-trad|
|[abjuration-01-RA7VKcen3p56rVyZ.htm](spells/abjuration-01-RA7VKcen3p56rVyZ.htm)|Forbidding Ward|auto-trad|
|[abjuration-01-rMOI8JFJ0nT2mrCF.htm](spells/abjuration-01-rMOI8JFJ0nT2mrCF.htm)|Phase Familiar|auto-trad|
|[abjuration-01-rQYob0QMJ0I1U2sU.htm](spells/abjuration-01-rQYob0QMJ0I1U2sU.htm)|Protector's Sacrifice|auto-trad|
|[abjuration-01-TTwOKGqmZeKSyNMH.htm](spells/abjuration-01-TTwOKGqmZeKSyNMH.htm)|Feather Fall|auto-trad|
|[abjuration-01-TVKNbcgTee19PXZR.htm](spells/abjuration-01-TVKNbcgTee19PXZR.htm)|Shield|auto-trad|
|[abjuration-01-TYbCj4dgXDOZou9k.htm](spells/abjuration-01-TYbCj4dgXDOZou9k.htm)|Reinforce Eidolon|auto-trad|
|[abjuration-01-u2uSeH6YSbK1ajTy.htm](spells/abjuration-01-u2uSeH6YSbK1ajTy.htm)|Magic Hide|auto-trad|
|[abjuration-01-w3uGXDVEdbLFZVO0.htm](spells/abjuration-01-w3uGXDVEdbLFZVO0.htm)|Angelic Halo|auto-trad|
|[abjuration-01-xM35hJacTM1BSXUl.htm](spells/abjuration-01-xM35hJacTM1BSXUl.htm)|Silver's Refrain|auto-trad|
|[abjuration-01-zlnXpME1T2uvn8Lr.htm](spells/abjuration-01-zlnXpME1T2uvn8Lr.htm)|Pass Without Trace|auto-trad|
|[abjuration-02-02J0rDTk37KN2sjt.htm](spells/abjuration-02-02J0rDTk37KN2sjt.htm)|Quench|auto-trad|
|[abjuration-02-24U1b9K4Lj94cgaj.htm](spells/abjuration-02-24U1b9K4Lj94cgaj.htm)|Magnetic Repulsion|auto-trad|
|[abjuration-02-5esP2GVzvxWsMgaX.htm](spells/abjuration-02-5esP2GVzvxWsMgaX.htm)|Endure Elements|auto-trad|
|[abjuration-02-66xBcxqzYcpbItBU.htm](spells/abjuration-02-66xBcxqzYcpbItBU.htm)|Purify Soul Path|auto-trad|
|[abjuration-02-9HpwDN4MYQJnW0LG.htm](spells/abjuration-02-9HpwDN4MYQJnW0LG.htm)|Dispel Magic|auto-trad|
|[abjuration-02-CLThxp8Qf43IQ3Sb.htm](spells/abjuration-02-CLThxp8Qf43IQ3Sb.htm)|Extract Poison|auto-trad|
|[abjuration-02-dZV8nZUKRhGIr6g9.htm](spells/abjuration-02-dZV8nZUKRhGIr6g9.htm)|Heartbond|auto-trad|
|[abjuration-02-Fr58LDSrbndgld9n.htm](spells/abjuration-02-Fr58LDSrbndgld9n.htm)|Resist Energy|auto-trad|
|[abjuration-02-fWU7Qjp1JiX9g6eg.htm](spells/abjuration-02-fWU7Qjp1JiX9g6eg.htm)|Animus Mine|auto-trad|
|[abjuration-02-FzAtX8yXBjTaisJK.htm](spells/abjuration-02-FzAtX8yXBjTaisJK.htm)|Undetectable Alignment|auto-trad|
|[abjuration-02-qFO9HgplrShoaAPY.htm](spells/abjuration-02-qFO9HgplrShoaAPY.htm)|Lock Item|auto-trad|
|[abjuration-02-Tw9e2rPaNdxcM1Rp.htm](spells/abjuration-02-Tw9e2rPaNdxcM1Rp.htm)|Erase Trail|auto-trad|
|[abjuration-02-Y8cSjhU33oUqccxJ.htm](spells/abjuration-02-Y8cSjhU33oUqccxJ.htm)|Brand the Impenitent|auto-trad|
|[abjuration-02-YWrfKetOqDwVFut7.htm](spells/abjuration-02-YWrfKetOqDwVFut7.htm)|Barkskin|auto-trad|
|[abjuration-03-3ipOKanMLnJrPwbr.htm](spells/abjuration-03-3ipOKanMLnJrPwbr.htm)|Guardian's Aegis|auto-trad|
|[abjuration-03-9TauMFkIsmvKJNzZ.htm](spells/abjuration-03-9TauMFkIsmvKJNzZ.htm)|Elemental Absorption|auto-trad|
|[abjuration-03-a0AMgATvQGwDR5dR.htm](spells/abjuration-03-a0AMgATvQGwDR5dR.htm)|Vector Screen|auto-trad|
|[abjuration-03-CIjj9CU5ekeq1oLT.htm](spells/abjuration-03-CIjj9CU5ekeq1oLT.htm)|Warding Aggression|auto-trad|
|[abjuration-03-EoKBlgf6Smt8opaU.htm](spells/abjuration-03-EoKBlgf6Smt8opaU.htm)|Nondetection|auto-trad|
|[abjuration-03-FPnkOYyWIaOzkmqn.htm](spells/abjuration-03-FPnkOYyWIaOzkmqn.htm)|Untwisting Iron Buffer|auto-trad|
|[abjuration-03-IFuEzfmmWyNwVbhY.htm](spells/abjuration-03-IFuEzfmmWyNwVbhY.htm)|Safe Passage|auto-trad|
|[abjuration-03-J5KrjQKCg2PrF1vz.htm](spells/abjuration-03-J5KrjQKCg2PrF1vz.htm)|Ancestral Defense|auto-trad|
|[abjuration-03-k9x6bXXpIgAXMDsx.htm](spells/abjuration-03-k9x6bXXpIgAXMDsx.htm)|Whirling Scarves|auto-trad|
|[abjuration-03-mpGCMTldMVa0pWYs.htm](spells/abjuration-03-mpGCMTldMVa0pWYs.htm)|Circle of Protection|auto-trad|
|[abjuration-03-mriIWoSDtJTJIBjX.htm](spells/abjuration-03-mriIWoSDtJTJIBjX.htm)|Caster's Imposition|auto-trad|
|[abjuration-03-o0l57UfBm9ScEUMW.htm](spells/abjuration-03-o0l57UfBm9ScEUMW.htm)|Glyph of Warding|auto-trad|
|[abjuration-03-rtisvvvhkpZPdgXc.htm](spells/abjuration-03-rtisvvvhkpZPdgXc.htm)|Cascade Countermeasure|auto-trad|
|[abjuration-03-VBNevVovTCpn04vL.htm](spells/abjuration-03-VBNevVovTCpn04vL.htm)|Unblinking Flame Revelation|auto-trad|
|[abjuration-03-y3jlOfgsQH1JjkJE.htm](spells/abjuration-03-y3jlOfgsQH1JjkJE.htm)|Sanctified Ground|auto-trad|
|[abjuration-04-2BV2yYPfVJ5zirZt.htm](spells/abjuration-04-2BV2yYPfVJ5zirZt.htm)|Stoneskin|auto-trad|
|[abjuration-04-5p8naLYjFcG13OkU.htm](spells/abjuration-04-5p8naLYjFcG13OkU.htm)|Rebounding Barrier|auto-trad|
|[abjuration-04-7Fd4lxozd11MQ55N.htm](spells/abjuration-04-7Fd4lxozd11MQ55N.htm)|Atone|auto-trad|
|[abjuration-04-8ifpNZkaxrbs3dBJ.htm](spells/abjuration-04-8ifpNZkaxrbs3dBJ.htm)|Perfected Form|auto-trad|
|[abjuration-04-aqRYNoSvxsVfqglH.htm](spells/abjuration-04-aqRYNoSvxsVfqglH.htm)|Freedom of Movement|auto-trad|
|[abjuration-04-caehfpQz7yp9yNzz.htm](spells/abjuration-04-caehfpQz7yp9yNzz.htm)|Dutiful Challenge|auto-trad|
|[abjuration-04-DH9Y3RQGWO0GzXGU.htm](spells/abjuration-04-DH9Y3RQGWO0GzXGU.htm)|Protector's Sphere|auto-trad|
|[abjuration-04-eG1fBodYwolaXK98.htm](spells/abjuration-04-eG1fBodYwolaXK98.htm)|Enduring Might|auto-trad|
|[abjuration-04-ER1LOEgCLtmEKd05.htm](spells/abjuration-04-ER1LOEgCLtmEKd05.htm)|Ravenous Portal|auto-trad|
|[abjuration-04-GjmMFEJcDd6MDG2P.htm](spells/abjuration-04-GjmMFEJcDd6MDG2P.htm)|Spiritual Attunement|auto-trad|
|[abjuration-04-GoKkejPj5yWJPIPK.htm](spells/abjuration-04-GoKkejPj5yWJPIPK.htm)|Adaptive Ablation|auto-trad|
|[abjuration-04-JOdOpbPDl7nqvJUm.htm](spells/abjuration-04-JOdOpbPDl7nqvJUm.htm)|Globe of Invulnerability|auto-trad|
|[abjuration-04-ksLCg62cLOojw3gN.htm](spells/abjuration-04-ksLCg62cLOojw3gN.htm)|Dimensional Anchor|auto-trad|
|[abjuration-04-LoBjvguamA12iyW0.htm](spells/abjuration-04-LoBjvguamA12iyW0.htm)|Energy Absorption|auto-trad|
|[abjuration-04-Mv5L4201uk8hnAtD.htm](spells/abjuration-04-Mv5L4201uk8hnAtD.htm)|Reflective Scales|auto-trad|
|[abjuration-04-NBSBFHxBm88qxQUy.htm](spells/abjuration-04-NBSBFHxBm88qxQUy.htm)|Chromatic Armor|auto-trad|
|[abjuration-04-onjZCEHs3JJJRTD0.htm](spells/abjuration-04-onjZCEHs3JJJRTD0.htm)|Private Sanctum|auto-trad|
|[abjuration-04-OsrtOeG0TvDNnEFH.htm](spells/abjuration-04-OsrtOeG0TvDNnEFH.htm)|Safeguard Secret|auto-trad|
|[abjuration-04-qJQADktwD0x8kLAy.htm](spells/abjuration-04-qJQADktwD0x8kLAy.htm)|Resilient Sphere|auto-trad|
|[abjuration-04-SSsUC7rZo0CwayPn.htm](spells/abjuration-04-SSsUC7rZo0CwayPn.htm)|Retributive Pain|auto-trad|
|[abjuration-04-tgIhRUFtgDSELpJE.htm](spells/abjuration-04-tgIhRUFtgDSELpJE.htm)|Spell Immunity|auto-trad|
|[abjuration-04-TLmkJ8ga8s057HBp.htm](spells/abjuration-04-TLmkJ8ga8s057HBp.htm)|Soft Landing|auto-trad|
|[abjuration-04-tYLBjOTvBVn9JtRb.htm](spells/abjuration-04-tYLBjOTvBVn9JtRb.htm)|Unity|auto-trad|
|[abjuration-04-y7Tusv3CieZktkkV.htm](spells/abjuration-04-y7Tusv3CieZktkkV.htm)|Flame Barrier|auto-trad|
|[abjuration-04-yY1H5zhO5dHmD8lz.htm](spells/abjuration-04-yY1H5zhO5dHmD8lz.htm)|Font of Serenity|auto-trad|
|[abjuration-05-1b55SgYTV65JvmQd.htm](spells/abjuration-05-1b55SgYTV65JvmQd.htm)|Blessing of Defiance|auto-trad|
|[abjuration-05-8rAcQOvm0nFrqAUt.htm](spells/abjuration-05-8rAcQOvm0nFrqAUt.htm)|Lashunta's Life Bubble|auto-trad|
|[abjuration-05-aVqsE5OedOwdHQvF.htm](spells/abjuration-05-aVqsE5OedOwdHQvF.htm)|Summerland Spell|auto-trad|
|[abjuration-05-bay4AfSu2iIozNNW.htm](spells/abjuration-05-bay4AfSu2iIozNNW.htm)|Banishment|auto-trad|
|[abjuration-05-forsqeofEszBNtLq.htm](spells/abjuration-05-forsqeofEszBNtLq.htm)|Chromatic Wall|auto-trad|
|[abjuration-05-h47yv6j6x1pUtzlr.htm](spells/abjuration-05-h47yv6j6x1pUtzlr.htm)|Arcane Countermeasure|auto-trad|
|[abjuration-05-h7h0wMxu4WOpveQ3.htm](spells/abjuration-05-h7h0wMxu4WOpveQ3.htm)|Ritual Obstruction|auto-trad|
|[abjuration-05-jQdm301h6e8hIY4U.htm](spells/abjuration-05-jQdm301h6e8hIY4U.htm)|Spiritual Guardian|auto-trad|
|[abjuration-05-N9KjnGyVMKgqKcCw.htm](spells/abjuration-05-N9KjnGyVMKgqKcCw.htm)|Bandit's Doom|auto-trad|
|[abjuration-05-VrhCpCIQB8PFax77.htm](spells/abjuration-05-VrhCpCIQB8PFax77.htm)|Spellmaster's Ward|auto-trad|
|[abjuration-05-wrm1zIiLyFD1Is6h.htm](spells/abjuration-05-wrm1zIiLyFD1Is6h.htm)|Aspirational State|auto-trad|
|[abjuration-05-XlQBVvlDWGrGlApl.htm](spells/abjuration-05-XlQBVvlDWGrGlApl.htm)|Establish Ward|auto-trad|
|[abjuration-05-YvXKGlHOt7mdW2jZ.htm](spells/abjuration-05-YvXKGlHOt7mdW2jZ.htm)|Death Ward|auto-trad|
|[abjuration-05-YWuyjhWbdoTiA1pw.htm](spells/abjuration-05-YWuyjhWbdoTiA1pw.htm)|Temporary Glyph|auto-trad|
|[abjuration-05-ZQk2cGBCkATO25w0.htm](spells/abjuration-05-ZQk2cGBCkATO25w0.htm)|Unbreaking Wave Vapor|auto-trad|
|[abjuration-06-2ykmAVKrsAWcazcC.htm](spells/abjuration-06-2ykmAVKrsAWcazcC.htm)|Planar Binding|auto-trad|
|[abjuration-06-8Umt1AzYfFbC4fui.htm](spells/abjuration-06-8Umt1AzYfFbC4fui.htm)|Spellwrack|auto-trad|
|[abjuration-06-9W2Qv0wXLg6tQg3y.htm](spells/abjuration-06-9W2Qv0wXLg6tQg3y.htm)|Scintillating Safeguard|auto-trad|
|[abjuration-06-Er9XNUlL0wB0PM36.htm](spells/abjuration-06-Er9XNUlL0wB0PM36.htm)|Ward Domain|auto-trad|
|[abjuration-06-kLxJx7BECVgHh2vb.htm](spells/abjuration-06-kLxJx7BECVgHh2vb.htm)|Temporal Ward|auto-trad|
|[abjuration-06-pZc8ZwtsyWnxUUW0.htm](spells/abjuration-06-pZc8ZwtsyWnxUUW0.htm)|Primal Call|auto-trad|
|[abjuration-06-USM530HlzZ1RMd99.htm](spells/abjuration-06-USM530HlzZ1RMd99.htm)|Champion's Sacrifice|auto-trad|
|[abjuration-06-yrZA4k2VAqEP8xx7.htm](spells/abjuration-06-yrZA4k2VAqEP8xx7.htm)|Repulsion|auto-trad|
|[abjuration-07-4nEY2BcV85wavKLO.htm](spells/abjuration-07-4nEY2BcV85wavKLO.htm)|Prismatic Armor|auto-trad|
|[abjuration-07-bVtkBJvGLP69qVGI.htm](spells/abjuration-07-bVtkBJvGLP69qVGI.htm)|Unfettered Pack|auto-trad|
|[abjuration-07-m2xFMNyQiUKQDRaj.htm](spells/abjuration-07-m2xFMNyQiUKQDRaj.htm)|Energy Aegis|auto-trad|
|[abjuration-07-OOELvfkTedBMlWtq.htm](spells/abjuration-07-OOELvfkTedBMlWtq.htm)|Spell Turning|auto-trad|
|[abjuration-07-v8VnSMzaSYwkq6c7.htm](spells/abjuration-07-v8VnSMzaSYwkq6c7.htm)|Return To Essence|auto-trad|
|[abjuration-07-WG91Z5TiR6oO5FOw.htm](spells/abjuration-07-WG91Z5TiR6oO5FOw.htm)|Contingency|auto-trad|
|[abjuration-07-XZE4BawIlTf88Yl9.htm](spells/abjuration-07-XZE4BawIlTf88Yl9.htm)|Dimensional Lock|auto-trad|
|[abjuration-07-YtJXpiu4ijkB6nP2.htm](spells/abjuration-07-YtJXpiu4ijkB6nP2.htm)|Unbreaking Wave Barrier|auto-trad|
|[abjuration-08-4ddJSjC9Zz5DX0oG.htm](spells/abjuration-08-4ddJSjC9Zz5DX0oG.htm)|Freedom|auto-trad|
|[abjuration-08-C2w3YfBKjIRS07DP.htm](spells/abjuration-08-C2w3YfBKjIRS07DP.htm)|Mind Blank|auto-trad|
|[abjuration-08-iL6TujgTCtRRa0Y0.htm](spells/abjuration-08-iL6TujgTCtRRa0Y0.htm)|Prismatic Wall|auto-trad|
|[abjuration-08-My7FvAoLYgGDDBzy.htm](spells/abjuration-08-My7FvAoLYgGDDBzy.htm)|Antimagic Field|auto-trad|
|[abjuration-08-nsQvjNyg4Whw2mek.htm](spells/abjuration-08-nsQvjNyg4Whw2mek.htm)|Divine Aura|auto-trad|
|[abjuration-08-TYydobaLN4U7Ol3M.htm](spells/abjuration-08-TYydobaLN4U7Ol3M.htm)|Unfettered Mark|auto-trad|
|[abjuration-09-9DtuMEyHhtHFRp5a.htm](spells/abjuration-09-9DtuMEyHhtHFRp5a.htm)|Untwisting Iron Pillar|auto-trad|
|[abjuration-09-AlCFTjSBaCHuRHBv.htm](spells/abjuration-09-AlCFTjSBaCHuRHBv.htm)|Prismatic Shield|auto-trad|
|[abjuration-09-BI4iwu3nApyIG0zY.htm](spells/abjuration-09-BI4iwu3nApyIG0zY.htm)|Astral Labyrinth|auto-trad|
|[abjuration-09-ihbRf964JDXztcy3.htm](spells/abjuration-09-ihbRf964JDXztcy3.htm)|Disjunction|auto-trad|
|[abjuration-09-KkimKrhDxEJVm6TH.htm](spells/abjuration-09-KkimKrhDxEJVm6TH.htm)|Vital Singularity|auto-trad|
|[abjuration-09-PngDCmU0MXZkbu0v.htm](spells/abjuration-09-PngDCmU0MXZkbu0v.htm)|Prismatic Sphere|auto-trad|
|[abjuration-10-KRcccPeNZOZ5Nweh.htm](spells/abjuration-10-KRcccPeNZOZ5Nweh.htm)|Nullify|auto-trad|
|[abjuration-10-UG0SmRYSdbrx2rTA.htm](spells/abjuration-10-UG0SmRYSdbrx2rTA.htm)|Indestructibility|auto-trad|
|[abjuration-10-X0t0gr7S7CeCt2Q5.htm](spells/abjuration-10-X0t0gr7S7CeCt2Q5.htm)|Anima Invocation (Modified)|auto-trad|
|[conjuration-01-30BBep9U4BDV0EgQ.htm](spells/conjuration-01-30BBep9U4BDV0EgQ.htm)|Infernal Pact|auto-trad|
|[conjuration-01-4YnON9JHYqtLzccu.htm](spells/conjuration-01-4YnON9JHYqtLzccu.htm)|Summon Animal|auto-trad|
|[conjuration-01-5p9Y4ACrtRM4gTpN.htm](spells/conjuration-01-5p9Y4ACrtRM4gTpN.htm)|Dimensional Assault|auto-trad|
|[conjuration-01-Aap7nGpC7neTWm78.htm](spells/conjuration-01-Aap7nGpC7neTWm78.htm)|Animal Allies|auto-trad|
|[conjuration-01-B0FZLkoHsiRgw7gv.htm](spells/conjuration-01-B0FZLkoHsiRgw7gv.htm)|Summon Lesser Servitor|auto-trad|
|[conjuration-01-cokeXkDHUAo4zHsw.htm](spells/conjuration-01-cokeXkDHUAo4zHsw.htm)|Oathkeeper's Insignia|auto-trad|
|[conjuration-01-D8cWhrzcsd43OlIX.htm](spells/conjuration-01-D8cWhrzcsd43OlIX.htm)|Div Pact|auto-trad|
|[conjuration-01-dDt8VFuLuhznT19v.htm](spells/conjuration-01-dDt8VFuLuhznT19v.htm)|Snare Hopping|auto-trad|
|[conjuration-01-EDABphKEPUBiMmQC.htm](spells/conjuration-01-EDABphKEPUBiMmQC.htm)|Verdant Sprout|auto-trad|
|[conjuration-01-Einy9RNTGVq1kY3j.htm](spells/conjuration-01-Einy9RNTGVq1kY3j.htm)|Inside Ropes|auto-trad|
|[conjuration-01-hs7h8f4Z1ZNdUt3s.htm](spells/conjuration-01-hs7h8f4Z1ZNdUt3s.htm)|Summon Fey|auto-trad|
|[conjuration-01-jks2h5pMsm8pCi8e.htm](spells/conjuration-01-jks2h5pMsm8pCi8e.htm)|Wildfire|auto-trad|
|[conjuration-01-joEruBVz31Uxczzq.htm](spells/conjuration-01-joEruBVz31Uxczzq.htm)|Angelic Messenger|auto-trad|
|[conjuration-01-jSRAyd57kd4WZ4yE.htm](spells/conjuration-01-jSRAyd57kd4WZ4yE.htm)|Summon Plant or Fungus|auto-trad|
|[conjuration-01-K9gI08enGtmih5X1.htm](spells/conjuration-01-K9gI08enGtmih5X1.htm)|Protector Tree|auto-trad|
|[conjuration-01-lKcsmeOrgHtK4xQa.htm](spells/conjuration-01-lKcsmeOrgHtK4xQa.htm)|Summon Construct|auto-trad|
|[conjuration-01-lsR3RLEdBG4rcSzd.htm](spells/conjuration-01-lsR3RLEdBG4rcSzd.htm)|Efficient Apport|auto-trad|
|[conjuration-01-LvezN4a3kYf1OHMg.htm](spells/conjuration-01-LvezN4a3kYf1OHMg.htm)|Floating Disk|auto-trad|
|[conjuration-01-MiTFNcqCI9f34A2V.htm](spells/conjuration-01-MiTFNcqCI9f34A2V.htm)|Inkshot|auto-trad|
|[conjuration-01-MraZBLJ4Be3ogmWL.htm](spells/conjuration-01-MraZBLJ4Be3ogmWL.htm)|Clinging Ice|auto-trad|
|[conjuration-01-myC2EIrsjmB8xosi.htm](spells/conjuration-01-myC2EIrsjmB8xosi.htm)|Pushing Gust|auto-trad|
|[conjuration-01-qTr2oCgIXl703Whb.htm](spells/conjuration-01-qTr2oCgIXl703Whb.htm)|Thoughtful Gift|auto-trad|
|[conjuration-01-rOwOLlfVgLMg1FND.htm](spells/conjuration-01-rOwOLlfVgLMg1FND.htm)|Elemental Sentinel|auto-trad|
|[conjuration-01-sX2g6WFSQPNW9jzx.htm](spells/conjuration-01-sX2g6WFSQPNW9jzx.htm)|Warp Step|auto-trad|
|[conjuration-01-TPI9fRCAYsDqpAe4.htm](spells/conjuration-01-TPI9fRCAYsDqpAe4.htm)|Temporary Tool|auto-trad|
|[conjuration-01-tsKnoBuBbKMXkiz5.htm](spells/conjuration-01-tsKnoBuBbKMXkiz5.htm)|Abyssal Pact|auto-trad|
|[conjuration-01-tWzxuJdbXqvskdIo.htm](spells/conjuration-01-tWzxuJdbXqvskdIo.htm)|Augment Summoning|auto-trad|
|[conjuration-01-uZK2BYzPnxUBnDjr.htm](spells/conjuration-01-uZK2BYzPnxUBnDjr.htm)|Tanglefoot|auto-trad|
|[conjuration-01-Vpohy4XH1DaH95hT.htm](spells/conjuration-01-Vpohy4XH1DaH95hT.htm)|Daemonic Pact|auto-trad|
|[conjuration-01-W69zswpj0Trdy5rj.htm](spells/conjuration-01-W69zswpj0Trdy5rj.htm)|Air Bubble|auto-trad|
|[conjuration-01-WFSazGLCUuPsBv1p.htm](spells/conjuration-01-WFSazGLCUuPsBv1p.htm)|Frost's Touch|auto-trad|
|[conjuration-01-WOM9alxiTdp0HEVD.htm](spells/conjuration-01-WOM9alxiTdp0HEVD.htm)|Distortion Lens|auto-trad|
|[conjuration-01-Wu0xFpewMKRK3HG8.htm](spells/conjuration-01-Wu0xFpewMKRK3HG8.htm)|Grease|auto-trad|
|[conjuration-01-WzLKjSw6hsBhuklC.htm](spells/conjuration-01-WzLKjSw6hsBhuklC.htm)|Create Water|auto-trad|
|[conjuration-01-xqmHD8JIjak15lRk.htm](spells/conjuration-01-xqmHD8JIjak15lRk.htm)|Unseen Servant|auto-trad|
|[conjuration-01-yAiSNmz39qXOIlco.htm](spells/conjuration-01-yAiSNmz39qXOIlco.htm)|Mud Pit|auto-trad|
|[conjuration-01-yvs1zN5Pai5U4CJX.htm](spells/conjuration-01-yvs1zN5Pai5U4CJX.htm)|Summon Instrument|auto-trad|
|[conjuration-01-zTN6zuruDUKOea6h.htm](spells/conjuration-01-zTN6zuruDUKOea6h.htm)|Rising Surf|auto-trad|
|[conjuration-02-23cv1p49NFd0Onng.htm](spells/conjuration-02-23cv1p49NFd0Onng.htm)|Ash Cloud|auto-trad|
|[conjuration-02-9s5tqqXNzcoKamWx.htm](spells/conjuration-02-9s5tqqXNzcoKamWx.htm)|Web|auto-trad|
|[conjuration-02-9XHmC2JgTUIQ1CCm.htm](spells/conjuration-02-9XHmC2JgTUIQ1CCm.htm)|Obscuring Mist|auto-trad|
|[conjuration-02-crf7EL1JtBYwvAEg.htm](spells/conjuration-02-crf7EL1JtBYwvAEg.htm)|Blood Duplicate|auto-trad|
|[conjuration-02-lpT6LotUaQPfinjj.htm](spells/conjuration-02-lpT6LotUaQPfinjj.htm)|Summon Elemental|auto-trad|
|[conjuration-02-Mt6ZzkVX8Q4xigFq.htm](spells/conjuration-02-Mt6ZzkVX8Q4xigFq.htm)|Create Food|auto-trad|
|[conjuration-02-NacrNSvfODxpZena.htm](spells/conjuration-02-NacrNSvfODxpZena.htm)|Blazing Blade|auto-trad|
|[conjuration-02-oOFilBgsXTVIbJpN.htm](spells/conjuration-02-oOFilBgsXTVIbJpN.htm)|Phantom Ship|auto-trad|
|[conjuration-02-qCmihBL0G0G5ExPF.htm](spells/conjuration-02-qCmihBL0G0G5ExPF.htm)|Persistent Servant|auto-trad|
|[conjuration-02-uiv8FKuxE8HOWTxW.htm](spells/conjuration-02-uiv8FKuxE8HOWTxW.htm)|Instant Armor|auto-trad|
|[conjuration-02-WPKJOhEihhcIm2uQ.htm](spells/conjuration-02-WPKJOhEihhcIm2uQ.htm)|Phantom Steed|auto-trad|
|[conjuration-02-Yv3AFIO55FONQYMH.htm](spells/conjuration-02-Yv3AFIO55FONQYMH.htm)|Sonata Span|auto-trad|
|[conjuration-03-1bXLp8cyIKFjZtVx.htm](spells/conjuration-03-1bXLp8cyIKFjZtVx.htm)|Temporal Twin|auto-trad|
|[conjuration-03-2ZPqcM9wNoVnpwkK.htm](spells/conjuration-03-2ZPqcM9wNoVnpwkK.htm)|Magical Fetters|auto-trad|
|[conjuration-03-5XUn9NADr05IyiVw.htm](spells/conjuration-03-5XUn9NADr05IyiVw.htm)|Soothing Blossoms|auto-trad|
|[conjuration-03-98gJvb8Xtn8OLIY7.htm](spells/conjuration-03-98gJvb8Xtn8OLIY7.htm)|Rally Point|auto-trad|
|[conjuration-03-AkF4yFcSCdhoULyZ.htm](spells/conjuration-03-AkF4yFcSCdhoULyZ.htm)|Awaken Portal|auto-trad|
|[conjuration-03-F9mA2Bg27QKniIdv.htm](spells/conjuration-03-F9mA2Bg27QKniIdv.htm)|Bottomless Stomach|auto-trad|
|[conjuration-03-g4MAIQodRDVfNp1B.htm](spells/conjuration-03-g4MAIQodRDVfNp1B.htm)|Personal Blizzard|auto-trad|
|[conjuration-03-GKpEcy9WId6NJvtx.htm](spells/conjuration-03-GKpEcy9WId6NJvtx.htm)|Stinking Cloud|auto-trad|
|[conjuration-03-iQD8OhhkwhvD8Blw.htm](spells/conjuration-03-iQD8OhhkwhvD8Blw.htm)|Swamp of Sloth|auto-trad|
|[conjuration-03-jOWQ5wPd4xvSkjI5.htm](spells/conjuration-03-jOWQ5wPd4xvSkjI5.htm)|Mystic Carriage|auto-trad|
|[conjuration-03-JqHAxsMUZ4Mr5bTr.htm](spells/conjuration-03-JqHAxsMUZ4Mr5bTr.htm)|Ghostly Shift|auto-trad|
|[conjuration-03-KsWhliKfUs3IpW3c.htm](spells/conjuration-03-KsWhliKfUs3IpW3c.htm)|Wall of Thorns|auto-trad|
|[conjuration-03-LNOwaJAkCfsgTKgV.htm](spells/conjuration-03-LNOwaJAkCfsgTKgV.htm)|Wall of Water|auto-trad|
|[conjuration-03-mwPfoYfVGSMAaUec.htm](spells/conjuration-03-mwPfoYfVGSMAaUec.htm)|Cozy Cabin|auto-trad|
|[conjuration-03-NtzNCW32UlPdY2xS.htm](spells/conjuration-03-NtzNCW32UlPdY2xS.htm)|Ashen Wind|auto-trad|
|[conjuration-03-oUDNCArkQTdhllxD.htm](spells/conjuration-03-oUDNCArkQTdhllxD.htm)|Aqueous Orb|auto-trad|
|[conjuration-03-SxRVCc1Q2MtVuPMo.htm](spells/conjuration-03-SxRVCc1Q2MtVuPMo.htm)|Sign of Conviction|auto-trad|
|[conjuration-03-tFKJCPvOQZxKq6ON.htm](spells/conjuration-03-tFKJCPvOQZxKq6ON.htm)|Mad Monkeys|auto-trad|
|[conjuration-03-tSosbMsftXcRaQgT.htm](spells/conjuration-03-tSosbMsftXcRaQgT.htm)|Sea of Thought|auto-trad|
|[conjuration-03-u3G7KX1qpFJlSeWm.htm](spells/conjuration-03-u3G7KX1qpFJlSeWm.htm)|Owb Pact|auto-trad|
|[conjuration-03-uuoPmbjEtqzWZs0v.htm](spells/conjuration-03-uuoPmbjEtqzWZs0v.htm)|Unseen Custodians|auto-trad|
|[conjuration-04-3xD8DYrr8YDVYGg7.htm](spells/conjuration-04-3xD8DYrr8YDVYGg7.htm)|Spike Stones|auto-trad|
|[conjuration-04-Az3PmWnlWSb5ELX9.htm](spells/conjuration-04-Az3PmWnlWSb5ELX9.htm)|Abundant Step|auto-trad|
|[conjuration-04-D31YX7zvRBvenTAz.htm](spells/conjuration-04-D31YX7zvRBvenTAz.htm)|Petal Storm|auto-trad|
|[conjuration-04-f9duI5d8xNYqxI8d.htm](spells/conjuration-04-f9duI5d8xNYqxI8d.htm)|Magic Mailbox|auto-trad|
|[conjuration-04-fMnjP4hpNRV9EfVM.htm](spells/conjuration-04-fMnjP4hpNRV9EfVM.htm)|Inevitable Destination|auto-trad|
|[conjuration-04-GzdgM0m7wXKuFSho.htm](spells/conjuration-04-GzdgM0m7wXKuFSho.htm)|Rope Trick|auto-trad|
|[conjuration-04-iMmexY6ZosLS4I5R.htm](spells/conjuration-04-iMmexY6ZosLS4I5R.htm)|Door to Beyond|auto-trad|
|[conjuration-04-K1wmI4qPmRhFczmy.htm](spells/conjuration-04-K1wmI4qPmRhFczmy.htm)|Dust Storm|auto-trad|
|[conjuration-04-kCgkreCT6g0dipMd.htm](spells/conjuration-04-kCgkreCT6g0dipMd.htm)|Murderous Vine|auto-trad|
|[conjuration-04-piMJO6aYeDJbrhEo.htm](spells/conjuration-04-piMJO6aYeDJbrhEo.htm)|Solid Fog|auto-trad|
|[conjuration-04-TUj8eugNqAvB1vVR.htm](spells/conjuration-04-TUj8eugNqAvB1vVR.htm)|Creation|auto-trad|
|[conjuration-04-VlNcjmYyu95vOUe8.htm](spells/conjuration-04-VlNcjmYyu95vOUe8.htm)|Dimension Door|auto-trad|
|[conjuration-04-vSSKyUdrHu86E5Gk.htm](spells/conjuration-04-vSSKyUdrHu86E5Gk.htm)|Nature's Bounty|auto-trad|
|[conjuration-04-yfCykJ6cs0uUL79b.htm](spells/conjuration-04-yfCykJ6cs0uUL79b.htm)|Radiant Heart of Devotion|auto-trad|
|[conjuration-04-zjG6NncHyAKqSF7m.htm](spells/conjuration-04-zjG6NncHyAKqSF7m.htm)|Dimensional Steps|auto-trad|
|[conjuration-04-zR67Rt3UMHKC5evy.htm](spells/conjuration-04-zR67Rt3UMHKC5evy.htm)|Blink|auto-trad|
|[conjuration-05-115Xp9E38CJENhNS.htm](spells/conjuration-05-115Xp9E38CJENhNS.htm)|Passwall|auto-trad|
|[conjuration-05-29ytKctjg7qSW2ff.htm](spells/conjuration-05-29ytKctjg7qSW2ff.htm)|Summon Fiend|auto-trad|
|[conjuration-05-2w4OpAGihn1JSHFD.htm](spells/conjuration-05-2w4OpAGihn1JSHFD.htm)|Black Tentacles|auto-trad|
|[conjuration-05-5ttAVJbWg2GVKmrN.htm](spells/conjuration-05-5ttAVJbWg2GVKmrN.htm)|Shadow Jump|auto-trad|
|[conjuration-05-6AqH5SGchbdhOJxA.htm](spells/conjuration-05-6AqH5SGchbdhOJxA.htm)|Flammable Fumes|auto-trad|
|[conjuration-05-ba12fO37w7O37gim.htm](spells/conjuration-05-ba12fO37w7O37gim.htm)|Summon Axiom|auto-trad|
|[conjuration-05-BoA00y45uDlmou07.htm](spells/conjuration-05-BoA00y45uDlmou07.htm)|Secret Chest|auto-trad|
|[conjuration-05-dNpIc5k1aCI3bHIg.htm](spells/conjuration-05-dNpIc5k1aCI3bHIg.htm)|Mosquito Blight|auto-trad|
|[conjuration-05-e9UJoVYUd5kJWUpi.htm](spells/conjuration-05-e9UJoVYUd5kJWUpi.htm)|Summon Giant|auto-trad|
|[conjuration-05-F1qxaqsEItmBura2.htm](spells/conjuration-05-F1qxaqsEItmBura2.htm)|Tree Stride|auto-trad|
|[conjuration-05-i1TvBID5QLyXrUCa.htm](spells/conjuration-05-i1TvBID5QLyXrUCa.htm)|Summon Entity|auto-trad|
|[conjuration-05-I2fwPslQth0DTPQD.htm](spells/conjuration-05-I2fwPslQth0DTPQD.htm)|Incendiary Fog|auto-trad|
|[conjuration-05-kghwmH3tQjMIhdH1.htm](spells/conjuration-05-kghwmH3tQjMIhdH1.htm)|Summon Dragon|auto-trad|
|[conjuration-05-kOa055FIrO9Smnya.htm](spells/conjuration-05-kOa055FIrO9Smnya.htm)|Wall of Stone|auto-trad|
|[conjuration-05-lTDixrrNKaCvLKwX.htm](spells/conjuration-05-lTDixrrNKaCvLKwX.htm)|Summon Celestial|auto-trad|
|[conjuration-05-n8ckecJpatSBEp7M.htm](spells/conjuration-05-n8ckecJpatSBEp7M.htm)|Summon Anarch|auto-trad|
|[conjuration-05-oXeEbcUdgJGWHGEJ.htm](spells/conjuration-05-oXeEbcUdgJGWHGEJ.htm)|Impaling Spike|auto-trad|
|[conjuration-05-qr0HOiiuqj5LKlDt.htm](spells/conjuration-05-qr0HOiiuqj5LKlDt.htm)|Pillars of Sand|auto-trad|
|[conjuration-05-rtA3HRGoy7PQTOhq.htm](spells/conjuration-05-rtA3HRGoy7PQTOhq.htm)|Terrain Transposition|auto-trad|
|[conjuration-05-ru3YdXajUREbKQDV.htm](spells/conjuration-05-ru3YdXajUREbKQDV.htm)|Return Beacon|auto-trad|
|[conjuration-05-rxvS7EMJ7qmexAyA.htm](spells/conjuration-05-rxvS7EMJ7qmexAyA.htm)|Shadow Walk|auto-trad|
|[conjuration-05-TDaMnCtZ72uyYrz8.htm](spells/conjuration-05-TDaMnCtZ72uyYrz8.htm)|Blink Charge|auto-trad|
|[conjuration-05-vgy00hnqxN9VoeoF.htm](spells/conjuration-05-vgy00hnqxN9VoeoF.htm)|Planar Ally|auto-trad|
|[conjuration-05-yRf59eFtZ50cGlem.htm](spells/conjuration-05-yRf59eFtZ50cGlem.htm)|Heroes' Feast|auto-trad|
|[conjuration-05-Z1MoMTcgFQiCI90t.htm](spells/conjuration-05-Z1MoMTcgFQiCI90t.htm)|Tesseract Tunnel|auto-trad|
|[conjuration-06-69L70wKfGDY66Mk9.htm](spells/conjuration-06-69L70wKfGDY66Mk9.htm)|Teleport|auto-trad|
|[conjuration-06-c3XygMbzrZMgV1y3.htm](spells/conjuration-06-c3XygMbzrZMgV1y3.htm)|Collective Transposition|auto-trad|
|[conjuration-06-Cpj05laa7ogqGwS3.htm](spells/conjuration-06-Cpj05laa7ogqGwS3.htm)|Elemental Confluence|auto-trad|
|[conjuration-06-fRUxp4G9kG816XAt.htm](spells/conjuration-06-fRUxp4G9kG816XAt.htm)|Lure Dream|auto-trad|
|[conjuration-06-In2A7GCyxxaqZdPI.htm](spells/conjuration-06-In2A7GCyxxaqZdPI.htm)|Moonlight Bridge|auto-trad|
|[conjuration-06-JbAcSLu62TU1OgNF.htm](spells/conjuration-06-JbAcSLu62TU1OgNF.htm)|Tangling Creepers|auto-trad|
|[conjuration-06-kwlKUxEuT8T15YW6.htm](spells/conjuration-06-kwlKUxEuT8T15YW6.htm)|Primal Summons|auto-trad|
|[conjuration-06-OOMQ8Z6EIrGt5NJ6.htm](spells/conjuration-06-OOMQ8Z6EIrGt5NJ6.htm)|Unexpected Transposition|auto-trad|
|[conjuration-07-1VOjUKd1pacI67RZ.htm](spells/conjuration-07-1VOjUKd1pacI67RZ.htm)|Curse of the Spirit Orchestra|auto-trad|
|[conjuration-07-5bTt2CvYHPvaR7QQ.htm](spells/conjuration-07-5bTt2CvYHPvaR7QQ.htm)|Plane Shift|auto-trad|
|[conjuration-07-5ZW1w9f4gWlSIuWA.htm](spells/conjuration-07-5ZW1w9f4gWlSIuWA.htm)|Teleportation Circle|auto-trad|
|[conjuration-07-73rToy0v5Ra9NvL6.htm](spells/conjuration-07-73rToy0v5Ra9NvL6.htm)|Duplicate Foe|auto-trad|
|[conjuration-07-D2nPKbIS67m9199U.htm](spells/conjuration-07-D2nPKbIS67m9199U.htm)|Ethereal Jaunt|auto-trad|
|[conjuration-07-ggtZTxeyEnDYbt6f.htm](spells/conjuration-07-ggtZTxeyEnDYbt6f.htm)|Momentary Recovery|auto-trad|
|[conjuration-07-nzbnTqHgNKiGZkrZ.htm](spells/conjuration-07-nzbnTqHgNKiGZkrZ.htm)|Word of Recall|auto-trad|
|[conjuration-07-qOeBQyC1z7OScHvP.htm](spells/conjuration-07-qOeBQyC1z7OScHvP.htm)|Maze of Locked Doors|auto-trad|
|[conjuration-07-vPWMEyVTreMOoFnm.htm](spells/conjuration-07-vPWMEyVTreMOoFnm.htm)|Magnificent Mansion|auto-trad|
|[conjuration-08-4ONjK2hoMBmuAAyk.htm](spells/conjuration-08-4ONjK2hoMBmuAAyk.htm)|Summon Irii|auto-trad|
|[conjuration-08-A0sMo1L0271yLeDA.htm](spells/conjuration-08-A0sMo1L0271yLeDA.htm)|Clone Companion|auto-trad|
|[conjuration-08-fLiowSDQXo3vCQDh.htm](spells/conjuration-08-fLiowSDQXo3vCQDh.htm)|Rite of the Red Star|auto-trad|
|[conjuration-08-JAaETUBg0xlttpCH.htm](spells/conjuration-08-JAaETUBg0xlttpCH.htm)|Summon Archmage|auto-trad|
|[conjuration-08-kIRWUBxocERjIBni.htm](spells/conjuration-08-kIRWUBxocERjIBni.htm)|Summon Deific Herald|auto-trad|
|[conjuration-08-oGV6YdpZLIG4G4gH.htm](spells/conjuration-08-oGV6YdpZLIG4G4gH.htm)|Impaling Briars|auto-trad|
|[conjuration-08-Oj1PJBMQD9vuwCv7.htm](spells/conjuration-08-Oj1PJBMQD9vuwCv7.htm)|Maze|auto-trad|
|[conjuration-08-Y9w4TYnb8cXU4UKY.htm](spells/conjuration-08-Y9w4TYnb8cXU4UKY.htm)|Split Shadow|auto-trad|
|[conjuration-08-ZwwIUavMbEwcZz35.htm](spells/conjuration-08-ZwwIUavMbEwcZz35.htm)|Create Demiplane|auto-trad|
|[conjuration-09-2EIUqc8TCTQimggQ.htm](spells/conjuration-09-2EIUqc8TCTQimggQ.htm)|Summon Draconic Legion|auto-trad|
|[conjuration-09-HHCgEEkeeShVQf8d.htm](spells/conjuration-09-HHCgEEkeeShVQf8d.htm)|Bilocation|auto-trad|
|[conjuration-09-KPDHmmjJiw7PhTYF.htm](spells/conjuration-09-KPDHmmjJiw7PhTYF.htm)|Resplendent Mansion|auto-trad|
|[conjuration-09-mau1Olq58ECF0ZPi.htm](spells/conjuration-09-mau1Olq58ECF0ZPi.htm)|Empty Body|auto-trad|
|[conjuration-09-tgJTm276cikEL8vU.htm](spells/conjuration-09-tgJTm276cikEL8vU.htm)|Summon Ancient Fleshforged|auto-trad|
|[conjuration-09-xxhS66k68u5iIOHC.htm](spells/conjuration-09-xxhS66k68u5iIOHC.htm)|Upheaval|auto-trad|
|[conjuration-10-dMKP4fkWx8V2cqAy.htm](spells/conjuration-10-dMKP4fkWx8V2cqAy.htm)|Remake|auto-trad|
|[conjuration-10-FA55Fxf8MBXhje95.htm](spells/conjuration-10-FA55Fxf8MBXhje95.htm)|Dinosaur Fort|auto-trad|
|[conjuration-10-U13bC0tNgrlHoeTK.htm](spells/conjuration-10-U13bC0tNgrlHoeTK.htm)|Gate|auto-trad|
|[conjuration-10-WRP8TDf36hqHyGv1.htm](spells/conjuration-10-WRP8TDf36hqHyGv1.htm)|Summon Kaiju|auto-trad|
|[divination-01-3mINzPzup2m9qzFU.htm](spells/divination-01-3mINzPzup2m9qzFU.htm)|Sepulchral Mask|auto-trad|
|[divination-01-5LYi9Efs6cko4GGL.htm](spells/divination-01-5LYi9Efs6cko4GGL.htm)|Object Reading|auto-trad|
|[divination-01-5Pc55FGGqVpIAJ62.htm](spells/divination-01-5Pc55FGGqVpIAJ62.htm)|Loremaster's Etude|auto-trad|
|[divination-01-5wjl0ZwEvvUh7sor.htm](spells/divination-01-5wjl0ZwEvvUh7sor.htm)|Swarmsense|auto-trad|
|[divination-01-6ZIKB0151LUR19Rw.htm](spells/divination-01-6ZIKB0151LUR19Rw.htm)|Ill Omen|auto-trad|
|[divination-01-8bdt1TvNKzsCu9Ct.htm](spells/divination-01-8bdt1TvNKzsCu9Ct.htm)|Join Pasts|auto-trad|
|[divination-01-aF7RiG7c8GzSQLYt.htm](spells/divination-01-aF7RiG7c8GzSQLYt.htm)|Word of Truth|auto-trad|
|[divination-01-BvNbDwFYaidKJG9j.htm](spells/divination-01-BvNbDwFYaidKJG9j.htm)|Time Sense|auto-trad|
|[divination-01-cNnHV97gPqxJ3Rrr.htm](spells/divination-01-cNnHV97gPqxJ3Rrr.htm)|Glimpse Weakness|auto-trad|
|[divination-01-CR8OKDbeFJoZbOCu.htm](spells/divination-01-CR8OKDbeFJoZbOCu.htm)|Extend Boost|auto-trad|
|[divination-01-D442XMADp01qJ7Cs.htm](spells/divination-01-D442XMADp01qJ7Cs.htm)|Mindlink|auto-trad|
|[divination-01-DgcSiOCR1uDXGaEA.htm](spells/divination-01-DgcSiOCR1uDXGaEA.htm)|Synchronize|auto-trad|
|[divination-01-dtOUkMC57izf93z5.htm](spells/divination-01-dtOUkMC57izf93z5.htm)|Ancestral Memories|auto-trad|
|[divination-01-dXIRotMLsABDQQSB.htm](spells/divination-01-dXIRotMLsABDQQSB.htm)|Scholarly Recollection|auto-trad|
|[divination-01-EqdSKqr8Qj5TUhMR.htm](spells/divination-01-EqdSKqr8Qj5TUhMR.htm)|Approximate|auto-trad|
|[divination-01-Fr2CGvWcgSyLcUi7.htm](spells/divination-01-Fr2CGvWcgSyLcUi7.htm)|Bit of Luck|auto-trad|
|[divination-01-G0T1xv1FoZ23Jxvt.htm](spells/divination-01-G0T1xv1FoZ23Jxvt.htm)|Nudge Fate|auto-trad|
|[divination-01-Gb7SeieEvd0pL2Eh.htm](spells/divination-01-Gb7SeieEvd0pL2Eh.htm)|True Strike|auto-trad|
|[divination-01-gpzpAAAJ1Lza2JVl.htm](spells/divination-01-gpzpAAAJ1Lza2JVl.htm)|Detect Magic|auto-trad|
|[divination-01-HOj2YsTpkoMpYJH9.htm](spells/divination-01-HOj2YsTpkoMpYJH9.htm)|Practice Makes Perfect|auto-trad|
|[divination-01-izcxFQFwf3woCnFs.htm](spells/divination-01-izcxFQFwf3woCnFs.htm)|Guidance|auto-trad|
|[divination-01-lg73SvJZno1ypPAj.htm](spells/divination-01-lg73SvJZno1ypPAj.htm)|Read the Air|auto-trad|
|[divination-01-mOUwbIN1SUp8FyPR.htm](spells/divination-01-mOUwbIN1SUp8FyPR.htm)|Cinder Gaze|auto-trad|
|[divination-01-nVfP43Xbs6I1PO8v.htm](spells/divination-01-nVfP43Xbs6I1PO8v.htm)|Shooting Star|auto-trad|
|[divination-01-nXmC2Xx9WmS5NsAo.htm](spells/divination-01-nXmC2Xx9WmS5NsAo.htm)|Share Lore|auto-trad|
|[divination-01-oFwmdb6LlRrh9AUT.htm](spells/divination-01-oFwmdb6LlRrh9AUT.htm)|Diviner's Sight|auto-trad|
|[divination-01-OhD2Z6rIGGD5ocZA.htm](spells/divination-01-OhD2Z6rIGGD5ocZA.htm)|Read Aura|auto-trad|
|[divination-01-q5vbYgLztQ04FQZg.htm](spells/divination-01-q5vbYgLztQ04FQZg.htm)|Seashell of Stolen Sound|auto-trad|
|[divination-01-QjdvYC1QkpMaemoX.htm](spells/divination-01-QjdvYC1QkpMaemoX.htm)|Nudge the Odds|auto-trad|
|[divination-01-QnTtGCAvdWRU4spv.htm](spells/divination-01-QnTtGCAvdWRU4spv.htm)|Detect Alignment|auto-trad|
|[divination-01-QqxwHeYEVylkYjsO.htm](spells/divination-01-QqxwHeYEVylkYjsO.htm)|Detect Poison|auto-trad|
|[divination-01-RztmhJrLLQWoGVdB.htm](spells/divination-01-RztmhJrLLQWoGVdB.htm)|Object Memory|auto-trad|
|[divination-01-TjWHgXqPv5jywMti.htm](spells/divination-01-TjWHgXqPv5jywMti.htm)|Pocket Library|auto-trad|
|[divination-01-tXa5vOu5giBNCjdR.htm](spells/divination-01-tXa5vOu5giBNCjdR.htm)|Know Direction|auto-trad|
|[divination-01-UmXhuKrYZR3W16mQ.htm](spells/divination-01-UmXhuKrYZR3W16mQ.htm)|Discern Secrets|auto-trad|
|[divination-01-Vvxgn7saUPW2bJhb.htm](spells/divination-01-Vvxgn7saUPW2bJhb.htm)|Read Fate|auto-trad|
|[divination-01-WJOQryAODgYmrL6g.htm](spells/divination-01-WJOQryAODgYmrL6g.htm)|Imprint Message|auto-trad|
|[divination-01-XhgMx9WC6NfXd9RP.htm](spells/divination-01-XhgMx9WC6NfXd9RP.htm)|Hyperfocus|auto-trad|
|[divination-01-Yrek2Yd4k3DPC2zV.htm](spells/divination-01-Yrek2Yd4k3DPC2zV.htm)|Zenith Star|auto-trad|
|[divination-01-Z7N5IxJCwrAdIgSg.htm](spells/divination-01-Z7N5IxJCwrAdIgSg.htm)|Message Rune|auto-trad|
|[divination-02-3ehSrqTAm7IPqbIZ.htm](spells/divination-02-3ehSrqTAm7IPqbIZ.htm)|Spirit Sense|auto-trad|
|[divination-02-41TZEjhO6D1nWw2X.htm](spells/divination-02-41TZEjhO6D1nWw2X.htm)|Augury|auto-trad|
|[divination-02-8VzbumNyMEdSzZSz.htm](spells/divination-02-8VzbumNyMEdSzZSz.htm)|Impeccable Flow|auto-trad|
|[divination-02-BBvV7qoXGdw09q1C.htm](spells/divination-02-BBvV7qoXGdw09q1C.htm)|Speak with Animals|auto-trad|
|[divination-02-eW7DqGEvU50CDHqc.htm](spells/divination-02-eW7DqGEvU50CDHqc.htm)|Pack Attack|auto-trad|
|[divination-02-GfrKNJ9pNeATiKCc.htm](spells/divination-02-GfrKNJ9pNeATiKCc.htm)|Hunter's Luck|auto-trad|
|[divination-02-GQopUYTuhmtb7WMG.htm](spells/divination-02-GQopUYTuhmtb7WMG.htm)|Perfect Strike|auto-trad|
|[divination-02-HTou8cG05yuSkesj.htm](spells/divination-02-HTou8cG05yuSkesj.htm)|Status|auto-trad|
|[divination-02-jwK43yKsHTkJQvQ9.htm](spells/divination-02-jwK43yKsHTkJQvQ9.htm)|See Invisibility|auto-trad|
|[divination-02-L10q1ng6U3sega9S.htm](spells/divination-02-L10q1ng6U3sega9S.htm)|Guiding Star|auto-trad|
|[divination-02-MkHshwYy2UOeEHRR.htm](spells/divination-02-MkHshwYy2UOeEHRR.htm)|Lucky Month|auto-trad|
|[divination-02-NhNKzq1DvFxkvTEc.htm](spells/divination-02-NhNKzq1DvFxkvTEc.htm)|Vision of Weakness|auto-trad|
|[divination-02-obVA6duK5fGbfFUY.htm](spells/divination-02-obVA6duK5fGbfFUY.htm)|Empathic Link|auto-trad|
|[divination-02-OhgfPzB5gymZ0IZM.htm](spells/divination-02-OhgfPzB5gymZ0IZM.htm)|Timely Tutor|auto-trad|
|[divination-02-ou56ShiFH7GWF8hX.htm](spells/divination-02-ou56ShiFH7GWF8hX.htm)|Light of Revelation|auto-trad|
|[divination-02-pZTqGY1MLRjgKasV.htm](spells/divination-02-pZTqGY1MLRjgKasV.htm)|Darkvision|auto-trad|
|[divination-02-rDDYGY2ekZ1yVv7q.htm](spells/divination-02-rDDYGY2ekZ1yVv7q.htm)|Lucky Number|auto-trad|
|[divination-02-vTQvfYu2llKQedmY.htm](spells/divination-02-vTQvfYu2llKQedmY.htm)|Comprehend Language|auto-trad|
|[divination-03-1HfusQ8NDWutGvMx.htm](spells/divination-03-1HfusQ8NDWutGvMx.htm)|Animal Vision|auto-trad|
|[divination-03-28kgh0JzBO6pt38C.htm](spells/divination-03-28kgh0JzBO6pt38C.htm)|Focusing Hum|auto-trad|
|[divination-03-32NpJbPblay77K6v.htm](spells/divination-03-32NpJbPblay77K6v.htm)|Tattoo Whispers|auto-trad|
|[divination-03-3mQkAjexj6hhAnkR.htm](spells/divination-03-3mQkAjexj6hhAnkR.htm)|Behold the Weave|auto-trad|
|[divination-03-5WM3WjshXgrkVCg6.htm](spells/divination-03-5WM3WjshXgrkVCg6.htm)|Beastmaster Trance|auto-trad|
|[divination-03-9GkOWDFDEMuV3hJr.htm](spells/divination-03-9GkOWDFDEMuV3hJr.htm)|Familiar's Face|auto-trad|
|[divination-03-9vDmTCHyAWdWWPIs.htm](spells/divination-03-9vDmTCHyAWdWWPIs.htm)|Contact Friends|auto-trad|
|[divination-03-bH9cH9aDByY91l1d.htm](spells/divination-03-bH9cH9aDByY91l1d.htm)|Organsight|auto-trad|
|[divination-03-dL3A7H4LlRH26Imo.htm](spells/divination-03-dL3A7H4LlRH26Imo.htm)|Arcane Weaving|auto-trad|
|[divination-03-DLFupJuCfzyLCQcO.htm](spells/divination-03-DLFupJuCfzyLCQcO.htm)|Eyes of the Dead|auto-trad|
|[divination-03-ErCsWsqXe8ccRjgO.htm](spells/divination-03-ErCsWsqXe8ccRjgO.htm)|Far Sight|auto-trad|
|[divination-03-GurWFDj4IjKv73kL.htm](spells/divination-03-GurWFDj4IjKv73kL.htm)|Excise Lexicon|auto-trad|
|[divination-03-GxxnhRIaoGKtu1iO.htm](spells/divination-03-GxxnhRIaoGKtu1iO.htm)|Extend Spell|auto-trad|
|[divination-03-HXhWYJviWalN5tQ2.htm](spells/divination-03-HXhWYJviWalN5tQ2.htm)|Clairaudience|auto-trad|
|[divination-03-KHnhPHL4x1AQHfbC.htm](spells/divination-03-KHnhPHL4x1AQHfbC.htm)|Mind Reading|auto-trad|
|[divination-03-l4bHFR9UW2XiY3kH.htm](spells/divination-03-l4bHFR9UW2XiY3kH.htm)|Omnidirectional Scan|auto-trad|
|[divination-03-LbPLNWlLCxKCo5gF.htm](spells/divination-03-LbPLNWlLCxKCo5gF.htm)|Access Lore|auto-trad|
|[divination-03-LQzlKbYjZSMFQawP.htm](spells/divination-03-LQzlKbYjZSMFQawP.htm)|Locate|auto-trad|
|[divination-03-oLylenH2jlP5UbRT.htm](spells/divination-03-oLylenH2jlP5UbRT.htm)|Painted Scout|auto-trad|
|[divination-03-ovx7O2FHvkjXhMcA.htm](spells/divination-03-ovx7O2FHvkjXhMcA.htm)|Perseis's Precautions|auto-trad|
|[divination-03-ppA1StEigPLKEQqR.htm](spells/divination-03-ppA1StEigPLKEQqR.htm)|Wanderer's Guide|auto-trad|
|[divination-03-QpjHqxwTGdILLvjD.htm](spells/divination-03-QpjHqxwTGdILLvjD.htm)|Ephemeral Tracking|auto-trad|
|[divination-03-rfFZKfeCSweFv7P3.htm](spells/divination-03-rfFZKfeCSweFv7P3.htm)|Impending Doom|auto-trad|
|[divination-03-ThE5zPYKF4weiljj.htm](spells/divination-03-ThE5zPYKF4weiljj.htm)|Show the Way|auto-trad|
|[divination-03-XQSPJHOf3TyfqvgS.htm](spells/divination-03-XQSPJHOf3TyfqvgS.htm)|Web of Eyes|auto-trad|
|[divination-03-YZnLggKDHkMY4cnw.htm](spells/divination-03-YZnLggKDHkMY4cnw.htm)|Transcribe Conflict|auto-trad|
|[divination-03-ZYoC630tNGutgbE0.htm](spells/divination-03-ZYoC630tNGutgbE0.htm)|Hypercognition|auto-trad|
|[divination-04-11P3KgDTMIeQIJD7.htm](spells/divination-04-11P3KgDTMIeQIJD7.htm)|Dawnflower's Light|auto-trad|
|[divination-04-4LSf04FFvDgMyDk6.htm](spells/divination-04-4LSf04FFvDgMyDk6.htm)|Vigilant Eye|auto-trad|
|[divination-04-7OwZHalOdRCRnFmZ.htm](spells/divination-04-7OwZHalOdRCRnFmZ.htm)|Lucky Break|auto-trad|
|[divination-04-a5uwCgOe7ayHPtHe.htm](spells/divination-04-a5uwCgOe7ayHPtHe.htm)|Enhance Senses|auto-trad|
|[divination-04-AnWCohzPgK4L9GVl.htm](spells/divination-04-AnWCohzPgK4L9GVl.htm)|Detect Scrying|auto-trad|
|[divination-04-EGTTGI1pkmsVPqHp.htm](spells/divination-04-EGTTGI1pkmsVPqHp.htm)|Winning Streak|auto-trad|
|[divination-04-ehd9PtifLMwjk4ep.htm](spells/divination-04-ehd9PtifLMwjk4ep.htm)|Concealment's Curtain|auto-trad|
|[divination-04-FhOaQDTSnsY7tiam.htm](spells/divination-04-FhOaQDTSnsY7tiam.htm)|Modify Memory|auto-trad|
|[divination-04-GqvKSxzN7A7kuFk4.htm](spells/divination-04-GqvKSxzN7A7kuFk4.htm)|Tempt Fate|auto-trad|
|[divination-04-HqTI6wRrck1YXp3F.htm](spells/divination-04-HqTI6wRrck1YXp3F.htm)|Telepathy|auto-trad|
|[divination-04-HqZ1VWIcXXZXWm3K.htm](spells/divination-04-HqZ1VWIcXXZXWm3K.htm)|Web of Influence|auto-trad|
|[divination-04-ivKnEtI1z4UqEKIA.htm](spells/divination-04-ivKnEtI1z4UqEKIA.htm)|Pulse of the City|auto-trad|
|[divination-04-kREpsv78anJpIiq2.htm](spells/divination-04-kREpsv78anJpIiq2.htm)|Detect Creator|auto-trad|
|[divination-04-l6zjNysNedpJcmDT.htm](spells/divination-04-l6zjNysNedpJcmDT.htm)|Know the Enemy|auto-trad|
|[divination-04-nrW6lGV4xDMqLS3P.htm](spells/divination-04-nrW6lGV4xDMqLS3P.htm)|Remember the Lost|auto-trad|
|[divination-04-pkcOby5prOausy1k.htm](spells/divination-04-pkcOby5prOausy1k.htm)|Read Omens|auto-trad|
|[divination-04-qvwIwJ9QBihy8R0t.htm](spells/divination-04-qvwIwJ9QBihy8R0t.htm)|Speak with Plants|auto-trad|
|[divination-04-s3LISC5Z55urpBgU.htm](spells/divination-04-s3LISC5Z55urpBgU.htm)|Path of Least Resistance|auto-trad|
|[divination-04-TRpUjBNPQz3Eshaq.htm](spells/divination-04-TRpUjBNPQz3Eshaq.htm)|Weaponize Secret|auto-trad|
|[divination-04-u0AtDZs6BhBPtjEs.htm](spells/divination-04-u0AtDZs6BhBPtjEs.htm)|Forgotten Lines|auto-trad|
|[divination-04-uNsliWpl8Q1JdFcM.htm](spells/divination-04-uNsliWpl8Q1JdFcM.htm)|Discern Lies|auto-trad|
|[divination-04-WVc30DGbM7TRLLul.htm](spells/divination-04-WVc30DGbM7TRLLul.htm)|Ghostly Tragedy|auto-trad|
|[divination-04-ykyKclKTCMp2SFXa.htm](spells/divination-04-ykyKclKTCMp2SFXa.htm)|Countless Eyes|auto-trad|
|[divination-04-ZjbVgIIqMstmdkqP.htm](spells/divination-04-ZjbVgIIqMstmdkqP.htm)|Glimpse the Truth|auto-trad|
|[divination-04-zvKWclOZ7A53DObE.htm](spells/divination-04-zvKWclOZ7A53DObE.htm)|Clairvoyance|auto-trad|
|[divination-05-3DB3F2hIx2dMbX8n.htm](spells/divination-05-3DB3F2hIx2dMbX8n.htm)|The World's a Stage|auto-trad|
|[divination-05-4Cntq9odgW6xMpAs.htm](spells/divination-05-4Cntq9odgW6xMpAs.htm)|Astral Projection|auto-trad|
|[divination-05-8THDHP0UC7SgOYYF.htm](spells/divination-05-8THDHP0UC7SgOYYF.htm)|Inevitable Disaster|auto-trad|
|[divination-05-9BnhadUO8FMLmeZ3.htm](spells/divination-05-9BnhadUO8FMLmeZ3.htm)|Mind Probe|auto-trad|
|[divination-05-Ek5XI0aEdZhBgm21.htm](spells/divination-05-Ek5XI0aEdZhBgm21.htm)|Prying Eye|auto-trad|
|[divination-05-floDCUOSzT0F6r77.htm](spells/divination-05-floDCUOSzT0F6r77.htm)|Unblinking Flame Aura|auto-trad|
|[divination-05-gfVXAW95YWRz0pJC.htm](spells/divination-05-gfVXAW95YWRz0pJC.htm)|Telepathic Bond|auto-trad|
|[divination-05-NIop4eI2i7cKFGad.htm](spells/divination-05-NIop4eI2i7cKFGad.htm)|Mirecloak|auto-trad|
|[divination-05-OAt2ZEns1gIOCgrn.htm](spells/divination-05-OAt2ZEns1gIOCgrn.htm)|Synesthesia|auto-trad|
|[divination-05-R9xqCBblkS5KE4y7.htm](spells/divination-05-R9xqCBblkS5KE4y7.htm)|Sending|auto-trad|
|[divination-05-s4HTepjhQWV1yfBs.htm](spells/divination-05-s4HTepjhQWV1yfBs.htm)|Foresee the Path|auto-trad|
|[divination-05-SwUiVavHKMWG7t5K.htm](spells/divination-05-SwUiVavHKMWG7t5K.htm)|Tongues|auto-trad|
|[divination-05-tj86Rnq3QuQnDtG3.htm](spells/divination-05-tj86Rnq3QuQnDtG3.htm)|Hunter's Vision|auto-trad|
|[divination-05-zaZieDHguhahmM2z.htm](spells/divination-05-zaZieDHguhahmM2z.htm)|Waters of Prediction|auto-trad|
|[divination-06-4j0FQ1mkidBAXuQV.htm](spells/divination-06-4j0FQ1mkidBAXuQV.htm)|Heroic Feat|auto-trad|
|[divination-06-7DN13ILADW2N9Z1t.htm](spells/divination-06-7DN13ILADW2N9Z1t.htm)|Commune|auto-trad|
|[divination-06-9lz67JCNCLDfIEy3.htm](spells/divination-06-9lz67JCNCLDfIEy3.htm)|Awaken Object|auto-trad|
|[divination-06-B4dDkYsHFo1H0CIF.htm](spells/divination-06-B4dDkYsHFo1H0CIF.htm)|Awaken Animal|auto-trad|
|[divination-06-BazbvgNmK46XjrVc.htm](spells/divination-06-BazbvgNmK46XjrVc.htm)|Word of Revision|auto-trad|
|[divination-06-FyOgUkq71LNC143w.htm](spells/divination-06-FyOgUkq71LNC143w.htm)|Catch Your Name|auto-trad|
|[divination-06-k0qJfSZH5xUEggwU.htm](spells/divination-06-k0qJfSZH5xUEggwU.htm)|Suspended Retribution|auto-trad|
|[divination-06-l4LFwY7iuzX6sDXr.htm](spells/divination-06-l4LFwY7iuzX6sDXr.htm)|Commune with Nature|auto-trad|
|[divination-06-MT8usUfwudDVUm5H.htm](spells/divination-06-MT8usUfwudDVUm5H.htm)|Manifold Lives|auto-trad|
|[divination-06-NeyyAPwa639WjYGG.htm](spells/divination-06-NeyyAPwa639WjYGG.htm)|Asmodean Wager|auto-trad|
|[divination-06-OdqM06M0wDUqZWiR.htm](spells/divination-06-OdqM06M0wDUqZWiR.htm)|Cast into Time|auto-trad|
|[divination-06-r784cIz17eWujtQj.htm](spells/divination-06-r784cIz17eWujtQj.htm)|Scrying|auto-trad|
|[divination-06-UJmKPm1FC6pf6txP.htm](spells/divination-06-UJmKPm1FC6pf6txP.htm)|Halcyon Infusion|auto-trad|
|[divination-06-uqlxMQQeSGWEVjki.htm](spells/divination-06-uqlxMQQeSGWEVjki.htm)|True Seeing|auto-trad|
|[divination-06-XULNb8ItUsfupxqH.htm](spells/divination-06-XULNb8ItUsfupxqH.htm)|Dread Secret|auto-trad|
|[divination-06-Zvg0FWzClGbzucFd.htm](spells/divination-06-Zvg0FWzClGbzucFd.htm)|Speaking Sky|auto-trad|
|[divination-07-6TKGaQm4PfEMkeRd.htm](spells/divination-07-6TKGaQm4PfEMkeRd.htm)|Supreme Connection|auto-trad|
|[divination-07-AlbpWWN87yGegoAF.htm](spells/divination-07-AlbpWWN87yGegoAF.htm)|True Target|auto-trad|
|[divination-07-G56DJkxlUjFv0C4Z.htm](spells/divination-07-G56DJkxlUjFv0C4Z.htm)|Time Beacon|auto-trad|
|[divination-07-hp6Q64dl7xbdn4gQ.htm](spells/divination-07-hp6Q64dl7xbdn4gQ.htm)|Legend Lore|auto-trad|
|[divination-07-KADeCawMG6WAzYHa.htm](spells/divination-07-KADeCawMG6WAzYHa.htm)|Unblinking Flame Emblem|auto-trad|
|[divination-07-rsZ5c0AUyywe5yoK.htm](spells/divination-07-rsZ5c0AUyywe5yoK.htm)|Retrocognition|auto-trad|
|[divination-08-CeSh8QcVnqP5OlLj.htm](spells/divination-08-CeSh8QcVnqP5OlLj.htm)|Discern Location|auto-trad|
|[divination-08-pt3gEnzA159uHcJC.htm](spells/divination-08-pt3gEnzA159uHcJC.htm)|Prying Survey|auto-trad|
|[divination-08-y2cQYLr5mljDSu1G.htm](spells/divination-08-y2cQYLr5mljDSu1G.htm)|Unrelenting Observation|auto-trad|
|[divination-09-aFd00WT266VYPqOG.htm](spells/divination-09-aFd00WT266VYPqOG.htm)|Unblinking Flame Ignition|auto-trad|
|[divination-09-coKiMMBLESkaLNVa.htm](spells/divination-09-coKiMMBLESkaLNVa.htm)|Proliferating Eyes|auto-trad|
|[divination-09-qsNeG9KZpODSACMq.htm](spells/divination-09-qsNeG9KZpODSACMq.htm)|Foresight|auto-trad|
|[divination-10-6dDtGIUerazSHIOu.htm](spells/divination-10-6dDtGIUerazSHIOu.htm)|Wish|auto-trad|
|[divination-10-Di9gL6KIZ9eFgbs1.htm](spells/divination-10-Di9gL6KIZ9eFgbs1.htm)|Fated Confrontation|auto-trad|
|[divination-10-h8zxY9hTeHtWsBVW.htm](spells/divination-10-h8zxY9hTeHtWsBVW.htm)|Alter Reality|auto-trad|
|[divination-10-MLNeD5sAunV0E23j.htm](spells/divination-10-MLNeD5sAunV0E23j.htm)|Primal Phenomenon|auto-trad|
|[divination-10-YfJTXyVGzLhM6V8U.htm](spells/divination-10-YfJTXyVGzLhM6V8U.htm)|Miracle|auto-trad|
|[enchantment-01-09C2wvWZLCxwjINk.htm](spells/enchantment-01-09C2wvWZLCxwjINk.htm)|Invoke True Name|auto-trad|
|[enchantment-01-0Dcd4iEXqCrkm4Jn.htm](spells/enchantment-01-0Dcd4iEXqCrkm4Jn.htm)|Liberating Command|auto-trad|
|[enchantment-01-4c1c6eNzU1PFGkAy.htm](spells/enchantment-01-4c1c6eNzU1PFGkAy.htm)|Endure|auto-trad|
|[enchantment-01-4gBIw4IDrSfFHik4.htm](spells/enchantment-01-4gBIw4IDrSfFHik4.htm)|Daze|auto-trad|
|[enchantment-01-4koZzrnMXhhosn0D.htm](spells/enchantment-01-4koZzrnMXhhosn0D.htm)|Fear|auto-trad|
|[enchantment-01-7ZinJNzxq0XF0oMx.htm](spells/enchantment-01-7ZinJNzxq0XF0oMx.htm)|Bane|auto-trad|
|[enchantment-01-8E97SA9KAWCNdXfO.htm](spells/enchantment-01-8E97SA9KAWCNdXfO.htm)|Schadenfreude|auto-trad|
|[enchantment-01-aEitTTb9PnOyidRf.htm](spells/enchantment-01-aEitTTb9PnOyidRf.htm)|Needle of Vengeance|auto-trad|
|[enchantment-01-aIHY2DArKFweIrpf.htm](spells/enchantment-01-aIHY2DArKFweIrpf.htm)|Command|auto-trad|
|[enchantment-01-BQMEaeJiEFC1nepg.htm](spells/enchantment-01-BQMEaeJiEFC1nepg.htm)|Forced Mercy|auto-trad|
|[enchantment-01-BRtKFk0PKfWIlCAB.htm](spells/enchantment-01-BRtKFk0PKfWIlCAB.htm)|Sweet Dream|auto-trad|
|[enchantment-01-d2pi7laQkzlr3wrS.htm](spells/enchantment-01-d2pi7laQkzlr3wrS.htm)|Ancestral Touch|auto-trad|
|[enchantment-01-Dj44lViYKvOJ8a53.htm](spells/enchantment-01-Dj44lViYKvOJ8a53.htm)|Blind Ambition|auto-trad|
|[enchantment-01-dqaCLzINHBiKjh4J.htm](spells/enchantment-01-dqaCLzINHBiKjh4J.htm)|Call to Arms|auto-trad|
|[enchantment-01-DU5daB09xwfE1y38.htm](spells/enchantment-01-DU5daB09xwfE1y38.htm)|Waking Nightmare|auto-trad|
|[enchantment-01-e9tVGZhYW37CtbHA.htm](spells/enchantment-01-e9tVGZhYW37CtbHA.htm)|Movanic Glimmer|auto-trad|
|[enchantment-01-EeEgzWYcAY7ZQkS6.htm](spells/enchantment-01-EeEgzWYcAY7ZQkS6.htm)|Synchronize Steps|auto-trad|
|[enchantment-01-f0Z5mqGA6Yu79B8x.htm](spells/enchantment-01-f0Z5mqGA6Yu79B8x.htm)|Inspire Competence|auto-trad|
|[enchantment-01-f45JpY7Ph2cAJGW2.htm](spells/enchantment-01-f45JpY7Ph2cAJGW2.htm)|Evil Eye|auto-trad|
|[enchantment-01-fxRaWoeOGyi6THYH.htm](spells/enchantment-01-fxRaWoeOGyi6THYH.htm)|Frenzied Revelry|auto-trad|
|[enchantment-01-GdN5YQE47gd79k7X.htm](spells/enchantment-01-GdN5YQE47gd79k7X.htm)|Wilding Word|auto-trad|
|[enchantment-01-GeUbPvwdZ4B4l0up.htm](spells/enchantment-01-GeUbPvwdZ4B4l0up.htm)|Stoke the Heart|auto-trad|
|[enchantment-01-IAjvwqgiDr3qGYxY.htm](spells/enchantment-01-IAjvwqgiDr3qGYxY.htm)|Inspire Courage|auto-trad|
|[enchantment-01-IkS3lDGUpIOMug7v.htm](spells/enchantment-01-IkS3lDGUpIOMug7v.htm)|Faerie Dust|auto-trad|
|[enchantment-01-irTdhxTixU9u9YUm.htm](spells/enchantment-01-irTdhxTixU9u9YUm.htm)|Lingering Composition|auto-trad|
|[enchantment-01-jsWthW1Qy6tg3SwD.htm](spells/enchantment-01-jsWthW1Qy6tg3SwD.htm)|Draw Ire|auto-trad|
|[enchantment-01-k2QrUk7jWMAWozMh.htm](spells/enchantment-01-k2QrUk7jWMAWozMh.htm)|Brain Drain|auto-trad|
|[enchantment-01-k6f5nvSv0XIhbiHj.htm](spells/enchantment-01-k6f5nvSv0XIhbiHj.htm)|Hollow Heart|auto-trad|
|[enchantment-01-KMFRKzNCq7hVNH7H.htm](spells/enchantment-01-KMFRKzNCq7hVNH7H.htm)|Charming Words|auto-trad|
|[enchantment-01-lpoWfblSMLcJfxsZ.htm](spells/enchantment-01-lpoWfblSMLcJfxsZ.htm)|Forbidden Thought|auto-trad|
|[enchantment-01-MmQiEc7aM9PDLO2J.htm](spells/enchantment-01-MmQiEc7aM9PDLO2J.htm)|Touch of Obedience|auto-trad|
|[enchantment-01-NNoKWiWKqJkdD2ln.htm](spells/enchantment-01-NNoKWiWKqJkdD2ln.htm)|Veil of Dreams|auto-trad|
|[enchantment-01-o4lRVTwSxnOOn5vl.htm](spells/enchantment-01-o4lRVTwSxnOOn5vl.htm)|Sleep|auto-trad|
|[enchantment-01-oPVyu2a0K3aTVIR8.htm](spells/enchantment-01-oPVyu2a0K3aTVIR8.htm)|Elysian Whimsy|auto-trad|
|[enchantment-01-pBevG6bSQOiyflev.htm](spells/enchantment-01-pBevG6bSQOiyflev.htm)|Befuddle|auto-trad|
|[enchantment-01-pHrVvoTKygXeczVG.htm](spells/enchantment-01-pHrVvoTKygXeczVG.htm)|Nymph's Token|auto-trad|
|[enchantment-01-Q25JQAgnJSGgFDKZ.htm](spells/enchantment-01-Q25JQAgnJSGgFDKZ.htm)|Veil of Confidence|auto-trad|
|[enchantment-01-r8g7oSumKOHDqJsd.htm](spells/enchantment-01-r8g7oSumKOHDqJsd.htm)|Agitate|auto-trad|
|[enchantment-01-rerNA6YZsdxuJYt3.htm](spells/enchantment-01-rerNA6YZsdxuJYt3.htm)|Déjà Vu|auto-trad|
|[enchantment-01-s7ILzY2xh1tc9U1v.htm](spells/enchantment-01-s7ILzY2xh1tc9U1v.htm)|Tame|auto-trad|
|[enchantment-01-szIyEsvihc5e1w8n.htm](spells/enchantment-01-szIyEsvihc5e1w8n.htm)|Soothe|auto-trad|
|[enchantment-01-T90ij2uu6ZaBaSXV.htm](spells/enchantment-01-T90ij2uu6ZaBaSXV.htm)|Lament|auto-trad|
|[enchantment-01-u4FGIUQgruLjml7J.htm](spells/enchantment-01-u4FGIUQgruLjml7J.htm)|Magic's Vessel|auto-trad|
|[enchantment-01-ut9IhJ9jSZSHDUop.htm](spells/enchantment-01-ut9IhJ9jSZSHDUop.htm)|Charming Touch|auto-trad|
|[enchantment-01-VACvA5VRbddjt5s9.htm](spells/enchantment-01-VACvA5VRbddjt5s9.htm)|Infectious Enthusiasm|auto-trad|
|[enchantment-01-Vctwx1ewa8HUOA94.htm](spells/enchantment-01-Vctwx1ewa8HUOA94.htm)|Diabolic Edict|auto-trad|
|[enchantment-01-vLA0q0WOK2YPuJs6.htm](spells/enchantment-01-vLA0q0WOK2YPuJs6.htm)|Charm|auto-trad|
|[enchantment-01-vZSWzkw0uF4iFWSM.htm](spells/enchantment-01-vZSWzkw0uF4iFWSM.htm)|Celestial Accord|auto-trad|
|[enchantment-01-WILXkjU5Yq3yw10r.htm](spells/enchantment-01-WILXkjU5Yq3yw10r.htm)|Counter Performance|auto-trad|
|[enchantment-01-XSujb7EsSwKl19Uu.htm](spells/enchantment-01-XSujb7EsSwKl19Uu.htm)|Bless|auto-trad|
|[enchantment-01-Xxdwkt0EEDgP1LGc.htm](spells/enchantment-01-Xxdwkt0EEDgP1LGc.htm)|Song of Strength|auto-trad|
|[enchantment-01-YGRpHU5yxw73mls8.htm](spells/enchantment-01-YGRpHU5yxw73mls8.htm)|Soothing Words|auto-trad|
|[enchantment-01-YNAthsgsJjQIXbc8.htm](spells/enchantment-01-YNAthsgsJjQIXbc8.htm)|Pact Broker|auto-trad|
|[enchantment-01-YVK3JUkPVzHIeGXQ.htm](spells/enchantment-01-YVK3JUkPVzHIeGXQ.htm)|Cackle|auto-trad|
|[enchantment-02-29JyWqqZ0thCFr1C.htm](spells/enchantment-02-29JyWqqZ0thCFr1C.htm)|Mind Games|auto-trad|
|[enchantment-02-5pwK2FZX6QwgtfqX.htm](spells/enchantment-02-5pwK2FZX6QwgtfqX.htm)|Inveigle|auto-trad|
|[enchantment-02-b515AZlB0sridKSq.htm](spells/enchantment-02-b515AZlB0sridKSq.htm)|Calm Emotions|auto-trad|
|[enchantment-02-bH0kPuf7UKxRvi2P.htm](spells/enchantment-02-bH0kPuf7UKxRvi2P.htm)|Inspire Defense|auto-trad|
|[enchantment-02-c8R2fpk88fBwJ1ie.htm](spells/enchantment-02-c8R2fpk88fBwJ1ie.htm)|Triple Time|auto-trad|
|[enchantment-02-CQb8HtQ1BPeZmu9h.htm](spells/enchantment-02-CQb8HtQ1BPeZmu9h.htm)|Touch of Idiocy|auto-trad|
|[enchantment-02-EfFMLVbmkBWmzoLF.htm](spells/enchantment-02-EfFMLVbmkBWmzoLF.htm)|Remove Fear|auto-trad|
|[enchantment-02-h6VoHgPC0JCTVzgP.htm](spells/enchantment-02-h6VoHgPC0JCTVzgP.htm)|Vicious Jealousy|auto-trad|
|[enchantment-02-hoR6w8BqX2F35Tdx.htm](spells/enchantment-02-hoR6w8BqX2F35Tdx.htm)|Blistering Invective|auto-trad|
|[enchantment-02-kIwA7kwp5E0AC3yM.htm](spells/enchantment-02-kIwA7kwp5E0AC3yM.htm)|Warrior's Regret|auto-trad|
|[enchantment-02-LrRyNA2bo5UwBxud.htm](spells/enchantment-02-LrRyNA2bo5UwBxud.htm)|Horrifying Blood Loss|auto-trad|
|[enchantment-02-mMgLHTLHjjGa206q.htm](spells/enchantment-02-mMgLHTLHjjGa206q.htm)|Thundering Dominance|auto-trad|
|[enchantment-02-tlSE7Ly8vi1Dgddv.htm](spells/enchantment-02-tlSE7Ly8vi1Dgddv.htm)|Hideous Laughter|auto-trad|
|[enchantment-02-UirEIHILQgip87qv.htm](spells/enchantment-02-UirEIHILQgip87qv.htm)|Charitable Urge|auto-trad|
|[enchantment-02-VJW3IzDpAFio7uls.htm](spells/enchantment-02-VJW3IzDpAFio7uls.htm)|Impart Empathy|auto-trad|
|[enchantment-02-yhz9fF69uwRhnHix.htm](spells/enchantment-02-yhz9fF69uwRhnHix.htm)|Animal Messenger|auto-trad|
|[enchantment-03-1xLVcA8Y1onw7toT.htm](spells/enchantment-03-1xLVcA8Y1onw7toT.htm)|Dirge of Doom|auto-trad|
|[enchantment-03-3x6eUCm17n6ROzUa.htm](spells/enchantment-03-3x6eUCm17n6ROzUa.htm)|Crisis of Faith|auto-trad|
|[enchantment-03-aewxsale5xWEPKLk.htm](spells/enchantment-03-aewxsale5xWEPKLk.htm)|Zone of Truth|auto-trad|
|[enchantment-03-czO0wbT1i320gcu9.htm](spells/enchantment-03-czO0wbT1i320gcu9.htm)|Roaring Applause|auto-trad|
|[enchantment-03-DCQHaLrYXMI37dvW.htm](spells/enchantment-03-DCQHaLrYXMI37dvW.htm)|Paralyze|auto-trad|
|[enchantment-03-fPlFRu4dp09qJs3K.htm](spells/enchantment-03-fPlFRu4dp09qJs3K.htm)|Mind of Menace|auto-trad|
|[enchantment-03-h5cMTygLpjY3IEF0.htm](spells/enchantment-03-h5cMTygLpjY3IEF0.htm)|Sudden Recollection|auto-trad|
|[enchantment-03-IihxWhRfpsBgQ5jS.htm](spells/enchantment-03-IihxWhRfpsBgQ5jS.htm)|Enthrall|auto-trad|
|[enchantment-03-KqvqNAfGIE5a9wSv.htm](spells/enchantment-03-KqvqNAfGIE5a9wSv.htm)|Heroism|auto-trad|
|[enchantment-03-lONs4r14LEBZRLLw.htm](spells/enchantment-03-lONs4r14LEBZRLLw.htm)|Shift Blame|auto-trad|
|[enchantment-03-LTUaK3smfm5eDiFK.htm](spells/enchantment-03-LTUaK3smfm5eDiFK.htm)|Song of Marching|auto-trad|
|[enchantment-03-mBojKJatf9PTYC38.htm](spells/enchantment-03-mBojKJatf9PTYC38.htm)|Fey Disappearance|auto-trad|
|[enchantment-03-nplNt08TvokZUxtR.htm](spells/enchantment-03-nplNt08TvokZUxtR.htm)|Agonizing Despair|auto-trad|
|[enchantment-03-oYBIs8MICYkFwtXD.htm](spells/enchantment-03-oYBIs8MICYkFwtXD.htm)|Infectious Ennui|auto-trad|
|[enchantment-03-Q56HLIHVKY6bC5W3.htm](spells/enchantment-03-Q56HLIHVKY6bC5W3.htm)|Blinding Beauty|auto-trad|
|[enchantment-03-Q690d3mw3TUrKX7E.htm](spells/enchantment-03-Q690d3mw3TUrKX7E.htm)|Geas|auto-trad|
|[enchantment-03-qhJfRnkCRrMI4G1O.htm](spells/enchantment-03-qhJfRnkCRrMI4G1O.htm)|Aberrant Whispers|auto-trad|
|[enchantment-03-yM3KTTSAIHhyuP14.htm](spells/enchantment-03-yM3KTTSAIHhyuP14.htm)|Dream Message|auto-trad|
|[enchantment-04-2jB9eyX7YekwoCvA.htm](spells/enchantment-04-2jB9eyX7YekwoCvA.htm)|Word of Freedom|auto-trad|
|[enchantment-04-4cEDhvchskRvxSw6.htm](spells/enchantment-04-4cEDhvchskRvxSw6.htm)|Procyal Philosophy|auto-trad|
|[enchantment-04-4DaHIgtMBTyxebY3.htm](spells/enchantment-04-4DaHIgtMBTyxebY3.htm)|Commanding Lash|auto-trad|
|[enchantment-04-53Kw9WQtvrEtABEu.htm](spells/enchantment-04-53Kw9WQtvrEtABEu.htm)|Compel True Name|auto-trad|
|[enchantment-04-7tR29sQt35NfIWqN.htm](spells/enchantment-04-7tR29sQt35NfIWqN.htm)|Anathematic Reprisal|auto-trad|
|[enchantment-04-9j35xXKVVtHCh1Pe.htm](spells/enchantment-04-9j35xXKVVtHCh1Pe.htm)|Shaken Confidence|auto-trad|
|[enchantment-04-APDrC83QljsyHenB.htm](spells/enchantment-04-APDrC83QljsyHenB.htm)|Savant's Curse|auto-trad|
|[enchantment-04-CPNlhDQP3aDmLzB3.htm](spells/enchantment-04-CPNlhDQP3aDmLzB3.htm)|Ordained Purpose|auto-trad|
|[enchantment-04-dARE6VfJ3Uoq5M53.htm](spells/enchantment-04-dARE6VfJ3Uoq5M53.htm)|Daydreamer's Curse|auto-trad|
|[enchantment-04-e9tVGZhYW37CtbHA.htm](spells/enchantment-04-e9tVGZhYW37CtbHA.htm)|Movanic Glimmer|auto-trad|
|[enchantment-04-eCniO6INHNfc9Svr.htm](spells/enchantment-04-eCniO6INHNfc9Svr.htm)|Overflowing Sorrow|auto-trad|
|[enchantment-04-FSu6ZKxr3xdS75wq.htm](spells/enchantment-04-FSu6ZKxr3xdS75wq.htm)|Roar of the Wyrm|auto-trad|
|[enchantment-04-i06YFOtfGfpUvcD7.htm](spells/enchantment-04-i06YFOtfGfpUvcD7.htm)|Wind Whispers|auto-trad|
|[enchantment-04-i7u6gAdNcyIyyo3h.htm](spells/enchantment-04-i7u6gAdNcyIyyo3h.htm)|Favorable Review|auto-trad|
|[enchantment-04-I8CPe9Pp7GABqOyB.htm](spells/enchantment-04-I8CPe9Pp7GABqOyB.htm)|Zeal for Battle|auto-trad|
|[enchantment-04-J8pL8yTshga8QOk8.htm](spells/enchantment-04-J8pL8yTshga8QOk8.htm)|Delusional Pride|auto-trad|
|[enchantment-04-jJphHQlENHFlSElH.htm](spells/enchantment-04-jJphHQlENHFlSElH.htm)|Captivating Adoration|auto-trad|
|[enchantment-04-JyxTmqjYYn63V5LY.htm](spells/enchantment-04-JyxTmqjYYn63V5LY.htm)|Glibness|auto-trad|
|[enchantment-04-kF0rs9mCPvJGfAZE.htm](spells/enchantment-04-kF0rs9mCPvJGfAZE.htm)|Inspire Heroics|auto-trad|
|[enchantment-04-KPGGkyBFbKse7KpK.htm](spells/enchantment-04-KPGGkyBFbKse7KpK.htm)|Dread Aura|auto-trad|
|[enchantment-04-KSAEhNfZyXMO7Z7V.htm](spells/enchantment-04-KSAEhNfZyXMO7Z7V.htm)|Outcast's Curse|auto-trad|
|[enchantment-04-LiGbewa9pO0yjbsY.htm](spells/enchantment-04-LiGbewa9pO0yjbsY.htm)|Confusion|auto-trad|
|[enchantment-04-LX4pCagYLpc9hEji.htm](spells/enchantment-04-LX4pCagYLpc9hEji.htm)|Aromatic Lure|auto-trad|
|[enchantment-04-MLdMUOdwSKegOGlo.htm](spells/enchantment-04-MLdMUOdwSKegOGlo.htm)|Dull Ambition|auto-trad|
|[enchantment-04-mUz3wqbMQ5JQ06M5.htm](spells/enchantment-04-mUz3wqbMQ5JQ06M5.htm)|Infectious Melody|auto-trad|
|[enchantment-04-NOB92Wpn7jXvtyVW.htm](spells/enchantment-04-NOB92Wpn7jXvtyVW.htm)|Competitive Edge|auto-trad|
|[enchantment-04-OmhnoYOzFhrp6rXv.htm](spells/enchantment-04-OmhnoYOzFhrp6rXv.htm)|Waking Dream|auto-trad|
|[enchantment-04-oXCwHBeDja4e0Mx0.htm](spells/enchantment-04-oXCwHBeDja4e0Mx0.htm)|Touch of the Moon|auto-trad|
|[enchantment-04-pLEhF3f7KRxFo0vp.htm](spells/enchantment-04-pLEhF3f7KRxFo0vp.htm)|Girzanje's March|auto-trad|
|[enchantment-04-PztLrElcZfLwRnEq.htm](spells/enchantment-04-PztLrElcZfLwRnEq.htm)|Dreamer's Call|auto-trad|
|[enchantment-04-qwlh6aDgi86U3Q7H.htm](spells/enchantment-04-qwlh6aDgi86U3Q7H.htm)|Suggestion|auto-trad|
|[enchantment-04-S7ylpCJyq0CYkux9.htm](spells/enchantment-04-S7ylpCJyq0CYkux9.htm)|Clownish Curse|auto-trad|
|[enchantment-04-skvgOWNTitLehL0b.htm](spells/enchantment-04-skvgOWNTitLehL0b.htm)|Shared Nightmare|auto-trad|
|[enchantment-04-sUvTGELaggrBSgGm.htm](spells/enchantment-04-sUvTGELaggrBSgGm.htm)|Implement of Destruction|auto-trad|
|[enchantment-05-1QMjWGq1DPOQr4VL.htm](spells/enchantment-05-1QMjWGq1DPOQr4VL.htm)|Dread Ambience|auto-trad|
|[enchantment-05-BilnTGuXrof9Dt9D.htm](spells/enchantment-05-BilnTGuXrof9Dt9D.htm)|Synaptic Pulse|auto-trad|
|[enchantment-05-Dby1eQ7y5dXBWlyc.htm](spells/enchantment-05-Dby1eQ7y5dXBWlyc.htm)|Contagious Idea|auto-trad|
|[enchantment-05-ftZ750SD2iIJKIy3.htm](spells/enchantment-05-ftZ750SD2iIJKIy3.htm)|Glimmer of Charm|auto-trad|
|[enchantment-05-GaRQlC9Yw1BGKHfN.htm](spells/enchantment-05-GaRQlC9Yw1BGKHfN.htm)|Crushing Despair|auto-trad|
|[enchantment-05-GP3wewkQXEPrLxYj.htm](spells/enchantment-05-GP3wewkQXEPrLxYj.htm)|Subconscious Suggestion|auto-trad|
|[enchantment-05-mm3hZ6jgaJaKK16n.htm](spells/enchantment-05-mm3hZ6jgaJaKK16n.htm)|Litany of Self-Interest|auto-trad|
|[enchantment-05-QIjc7Zyej0P3b9v5.htm](spells/enchantment-05-QIjc7Zyej0P3b9v5.htm)|Oblivious Expulsion|auto-trad|
|[enchantment-05-SE3MddYAUyPKABuF.htm](spells/enchantment-05-SE3MddYAUyPKABuF.htm)|Infectious Comedy|auto-trad|
|[enchantment-05-x2LALaHXO7644GQA.htm](spells/enchantment-05-x2LALaHXO7644GQA.htm)|You're Mine|auto-trad|
|[enchantment-05-y0Vy7iNL3ET8K00C.htm](spells/enchantment-05-y0Vy7iNL3ET8K00C.htm)|Dreaming Potential|auto-trad|
|[enchantment-06-5BbU1V6wGSGbrmRD.htm](spells/enchantment-06-5BbU1V6wGSGbrmRD.htm)|Feeblemind|auto-trad|
|[enchantment-06-GYD0XZ4t3tQq6shc.htm](spells/enchantment-06-GYD0XZ4t3tQq6shc.htm)|Zealous Conviction|auto-trad|
|[enchantment-06-M6HsK7W6JMkJXeog.htm](spells/enchantment-06-M6HsK7W6JMkJXeog.htm)|Ideal Mimicry|auto-trad|
|[enchantment-06-NhGXgmI3AjkkwnPk.htm](spells/enchantment-06-NhGXgmI3AjkkwnPk.htm)|Blinding Fury|auto-trad|
|[enchantment-06-OsOhx3TGIZ7AhD0P.htm](spells/enchantment-06-OsOhx3TGIZ7AhD0P.htm)|Dominate|auto-trad|
|[enchantment-06-s0SerOrkkUd7SAH9.htm](spells/enchantment-06-s0SerOrkkUd7SAH9.htm)|Bacchanalia|auto-trad|
|[enchantment-07-0JigNJDRwevZOyjI.htm](spells/enchantment-07-0JigNJDRwevZOyjI.htm)|Soothing Ballad|auto-trad|
|[enchantment-07-8kJbiBEjMWG4VUjs.htm](spells/enchantment-07-8kJbiBEjMWG4VUjs.htm)|Warp Mind|auto-trad|
|[enchantment-07-gMbrNFKz9NP3TR4s.htm](spells/enchantment-07-gMbrNFKz9NP3TR4s.htm)|Inexhaustible Cynicism|auto-trad|
|[enchantment-07-IQchIYUwbsVTa9Mc.htm](spells/enchantment-07-IQchIYUwbsVTa9Mc.htm)|Allegro|auto-trad|
|[enchantment-07-Vw2CNwlRRKABsuZi.htm](spells/enchantment-07-Vw2CNwlRRKABsuZi.htm)|Entrancing Eyes|auto-trad|
|[enchantment-07-Yk3t4ekEiFIoEz9c.htm](spells/enchantment-07-Yk3t4ekEiFIoEz9c.htm)|Power Word Blind|auto-trad|
|[enchantment-08-7PJSqUeKxTqOVrPk.htm](spells/enchantment-08-7PJSqUeKxTqOVrPk.htm)|Power Word Stun|auto-trad|
|[enchantment-08-Jvyy6oVIQsD34MHB.htm](spells/enchantment-08-Jvyy6oVIQsD34MHB.htm)|Uncontrollable Dance|auto-trad|
|[enchantment-08-KtTGLbLG9nqMbUYL.htm](spells/enchantment-08-KtTGLbLG9nqMbUYL.htm)|Divine Inspiration|auto-trad|
|[enchantment-08-kWDt0JcKPgX6MvdD.htm](spells/enchantment-08-kWDt0JcKPgX6MvdD.htm)|Burning Blossoms|auto-trad|
|[enchantment-08-qlxM7Ik3uUeUIOcv.htm](spells/enchantment-08-qlxM7Ik3uUeUIOcv.htm)|Canticle of Everlasting Grief|auto-trad|
|[enchantment-09-fkDeKktdmbeplYRY.htm](spells/enchantment-09-fkDeKktdmbeplYRY.htm)|Overwhelming Presence|auto-trad|
|[enchantment-09-FmNDwqMEjeTEGPrY.htm](spells/enchantment-09-FmNDwqMEjeTEGPrY.htm)|Unfathomable Song|auto-trad|
|[enchantment-09-ItZgGznUBhgWBwFG.htm](spells/enchantment-09-ItZgGznUBhgWBwFG.htm)|Telepathic Demand|auto-trad|
|[enchantment-09-m3lcOFm400lQCUps.htm](spells/enchantment-09-m3lcOFm400lQCUps.htm)|Power Word Kill|auto-trad|
|[enchantment-09-T5Mt4jXFuh14uREv.htm](spells/enchantment-09-T5Mt4jXFuh14uREv.htm)|Divinity Leech|auto-trad|
|[enchantment-09-Tc5NLaMu71vrGTJQ.htm](spells/enchantment-09-Tc5NLaMu71vrGTJQ.htm)|Nature's Enmity|auto-trad|
|[enchantment-09-xFY9RtDE4DQKlWNR.htm](spells/enchantment-09-xFY9RtDE4DQKlWNR.htm)|Crusade|auto-trad|
|[enchantment-10-6s0UW4bujggma9TC.htm](spells/enchantment-10-6s0UW4bujggma9TC.htm)|Fabricated Truth|auto-trad|
|[enchantment-10-lyJDBD9OFW11vLyT.htm](spells/enchantment-10-lyJDBD9OFW11vLyT.htm)|Fatal Aria|auto-trad|
|[enchantment-10-Um0aaJotqMKGmAlR.htm](spells/enchantment-10-Um0aaJotqMKGmAlR.htm)|Pied Piping|auto-trad|
|[evocation-01-0H1ozccQGGFLUwFI.htm](spells/evocation-01-0H1ozccQGGFLUwFI.htm)|Cry of Destruction|auto-trad|
|[evocation-01-0JUOgbbFCapp3HlW.htm](spells/evocation-01-0JUOgbbFCapp3HlW.htm)|Elemental Toss|auto-trad|
|[evocation-01-1meVElIu1CEVYWkv.htm](spells/evocation-01-1meVElIu1CEVYWkv.htm)|Noxious Vapors|auto-trad|
|[evocation-01-5gophZ4AOKW4VW27.htm](spells/evocation-01-5gophZ4AOKW4VW27.htm)|Phase Bolt|auto-trad|
|[evocation-01-5QD75UbyB5EiG3yz.htm](spells/evocation-01-5QD75UbyB5EiG3yz.htm)|Scorching Blast|auto-trad|
|[evocation-01-60sgbuMWN0268dB7.htm](spells/evocation-01-60sgbuMWN0268dB7.htm)|Telekinetic Projectile|auto-trad|
|[evocation-01-6UafOE1ZUbHamsZJ.htm](spells/evocation-01-6UafOE1ZUbHamsZJ.htm)|Dim the Light|auto-trad|
|[evocation-01-6wLY5LnehCo3tHlr.htm](spells/evocation-01-6wLY5LnehCo3tHlr.htm)|Aqueous Blast|auto-trad|
|[evocation-01-7CUgqHunmHfW2lC5.htm](spells/evocation-01-7CUgqHunmHfW2lC5.htm)|Parch|auto-trad|
|[evocation-01-8lZhUreL1bRk1v4Z.htm](spells/evocation-01-8lZhUreL1bRk1v4Z.htm)|Briny Bolt|auto-trad|
|[evocation-01-8TQiFzGf4feoHeH0.htm](spells/evocation-01-8TQiFzGf4feoHeH0.htm)|Chilling Spray|auto-trad|
|[evocation-01-9Ga1AOQdHKYXUY4O.htm](spells/evocation-01-9Ga1AOQdHKYXUY4O.htm)|Purifying Icicle|auto-trad|
|[evocation-01-9mPEoOPN0AMuixIv.htm](spells/evocation-01-9mPEoOPN0AMuixIv.htm)|Force Fang|auto-trad|
|[evocation-01-AspA30tzKCHFWRf0.htm](spells/evocation-01-AspA30tzKCHFWRf0.htm)|Incendiary Aura|auto-trad|
|[evocation-01-b5BQbwmuBhgPXTyi.htm](spells/evocation-01-b5BQbwmuBhgPXTyi.htm)|Haunting Hymn|auto-trad|
|[evocation-01-BItahht2hEHvR9Bt.htm](spells/evocation-01-BItahht2hEHvR9Bt.htm)|Buzzing Bites|auto-trad|
|[evocation-01-bSDTWUIvgXkBaEv8.htm](spells/evocation-01-bSDTWUIvgXkBaEv8.htm)|Hand of the Apprentice|auto-trad|
|[evocation-01-cE7PRAX8Up7fmYef.htm](spells/evocation-01-cE7PRAX8Up7fmYef.htm)|Shroud of Night|auto-trad|
|[evocation-01-D7ZEhTNIDWDLC2J4.htm](spells/evocation-01-D7ZEhTNIDWDLC2J4.htm)|Puff of Poison|auto-trad|
|[evocation-01-dDiOnjcsBFbAvP6t.htm](spells/evocation-01-dDiOnjcsBFbAvP6t.htm)|Gale Blast|auto-trad|
|[evocation-01-DeNz6eAUlE0IE9U3.htm](spells/evocation-01-DeNz6eAUlE0IE9U3.htm)|Winter Bolt|auto-trad|
|[evocation-01-dgCH2E0gMLMUgyFl.htm](spells/evocation-01-dgCH2E0gMLMUgyFl.htm)|Shockwave|auto-trad|
|[evocation-01-eSL5hVT9gXrnRLtd.htm](spells/evocation-01-eSL5hVT9gXrnRLtd.htm)|Spout|auto-trad|
|[evocation-01-EzB9i7R6aBRAtJCh.htm](spells/evocation-01-EzB9i7R6aBRAtJCh.htm)|Tempest Touch|auto-trad|
|[evocation-01-F1fQC9SarAkjNxpP.htm](spells/evocation-01-F1fQC9SarAkjNxpP.htm)|Gravitational Pull|auto-trad|
|[evocation-01-f9uqHnNBMU0774SF.htm](spells/evocation-01-f9uqHnNBMU0774SF.htm)|Elemental Betrayal|auto-trad|
|[evocation-01-fprqWKUc0jnMIyGU.htm](spells/evocation-01-fprqWKUc0jnMIyGU.htm)|Airburst|auto-trad|
|[evocation-01-g1eY1vN44mgluE33.htm](spells/evocation-01-g1eY1vN44mgluE33.htm)|Charged Javelin|auto-trad|
|[evocation-01-g8QqHpv2CWDwmIm1.htm](spells/evocation-01-g8QqHpv2CWDwmIm1.htm)|Gust of Wind|auto-trad|
|[evocation-01-gISYsBFby1TiXfBt.htm](spells/evocation-01-gISYsBFby1TiXfBt.htm)|Acid Splash|auto-trad|
|[evocation-01-gKKqvLohtrSJj3BM.htm](spells/evocation-01-gKKqvLohtrSJj3BM.htm)|Magic Missile|auto-trad|
|[evocation-01-gYjPm7YwGtEa1oxh.htm](spells/evocation-01-gYjPm7YwGtEa1oxh.htm)|Ray of Frost|auto-trad|
|[evocation-01-HGmBY8KjgLV97nUp.htm](spells/evocation-01-HGmBY8KjgLV97nUp.htm)|Scouring Sand|auto-trad|
|[evocation-01-ho1jSoYKrHUNnM90.htm](spells/evocation-01-ho1jSoYKrHUNnM90.htm)|Tempest Surge|auto-trad|
|[evocation-01-HStu2Yhw3iQER9tY.htm](spells/evocation-01-HStu2Yhw3iQER9tY.htm)|Boost Eidolon|auto-trad|
|[evocation-01-Hu38hoAUSYeFpkVa.htm](spells/evocation-01-Hu38hoAUSYeFpkVa.htm)|Force Bolt|auto-trad|
|[evocation-01-iAnpxrLaBU4V6Sej.htm](spells/evocation-01-iAnpxrLaBU4V6Sej.htm)|Tidal Surge|auto-trad|
|[evocation-01-IWUe32Y5k2QFd7YQ.htm](spells/evocation-01-IWUe32Y5k2QFd7YQ.htm)|Gravity Weapon|auto-trad|
|[evocation-01-jfVCuOpzC6mUrf6f.htm](spells/evocation-01-jfVCuOpzC6mUrf6f.htm)|Hydraulic Push|auto-trad|
|[evocation-01-KAapTzGKJMbMQCL1.htm](spells/evocation-01-KAapTzGKJMbMQCL1.htm)|Spinning Staff|auto-trad|
|[evocation-01-kBhaPuzLUSwS6vVf.htm](spells/evocation-01-kBhaPuzLUSwS6vVf.htm)|Electric Arc|auto-trad|
|[evocation-01-kJKSLfCgqxmN2FY8.htm](spells/evocation-01-kJKSLfCgqxmN2FY8.htm)|Personal Rain Cloud|auto-trad|
|[evocation-01-kl2q6JvBZwed4B6v.htm](spells/evocation-01-kl2q6JvBZwed4B6v.htm)|Dancing Lights|auto-trad|
|[evocation-01-mlNYROcFrUF8nFgk.htm](spells/evocation-01-mlNYROcFrUF8nFgk.htm)|Spray of Stars|auto-trad|
|[evocation-01-oJKZi8OQgmVXHOc0.htm](spells/evocation-01-oJKZi8OQgmVXHOc0.htm)|Fire Ray|auto-trad|
|[evocation-01-pRKaEXnjGJXbPHPC.htm](spells/evocation-01-pRKaEXnjGJXbPHPC.htm)|Hurtling Stone|auto-trad|
|[evocation-01-pwzdSlJgYqN7bs2w.htm](spells/evocation-01-pwzdSlJgYqN7bs2w.htm)|Mage Hand|auto-trad|
|[evocation-01-QozxgBbcmktLKdBs.htm](spells/evocation-01-QozxgBbcmktLKdBs.htm)|Updraft|auto-trad|
|[evocation-01-Qw3fnUlaUbnn7ipC.htm](spells/evocation-01-Qw3fnUlaUbnn7ipC.htm)|Prestidigitation|auto-trad|
|[evocation-01-qwZBXN6zBoB9BHXE.htm](spells/evocation-01-qwZBXN6zBoB9BHXE.htm)|Divine Lance|auto-trad|
|[evocation-01-r3NeUnsgt9mS03Sn.htm](spells/evocation-01-r3NeUnsgt9mS03Sn.htm)|Shocking Grasp|auto-trad|
|[evocation-01-r82rqcm0MmGaBFkM.htm](spells/evocation-01-r82rqcm0MmGaBFkM.htm)|Thunderous Strike|auto-trad|
|[evocation-01-Rn2LkoSq1XhLsODV.htm](spells/evocation-01-Rn2LkoSq1XhLsODV.htm)|Pummeling Rubble|auto-trad|
|[evocation-01-rnNGALRtsjspFTws.htm](spells/evocation-01-rnNGALRtsjspFTws.htm)|Acidic Burst|auto-trad|
|[evocation-01-S6Kkk15MWGqzC00a.htm](spells/evocation-01-S6Kkk15MWGqzC00a.htm)|Draconic Barrage|auto-trad|
|[evocation-01-s8gTmnNMg4H4bHEF.htm](spells/evocation-01-s8gTmnNMg4H4bHEF.htm)|Gritty Wheeze|auto-trad|
|[evocation-01-SE0fbgBj7atuukdv.htm](spells/evocation-01-SE0fbgBj7atuukdv.htm)|Cloak of Shadow|auto-trad|
|[evocation-01-sPHcuLIKj9SDaDAD.htm](spells/evocation-01-sPHcuLIKj9SDaDAD.htm)|Kinetic Ram|auto-trad|
|[evocation-01-SuBtUJiU6DbSJYIw.htm](spells/evocation-01-SuBtUJiU6DbSJYIw.htm)|Moonbeam|auto-trad|
|[evocation-01-sUr5KCpeE6AXfvPp.htm](spells/evocation-01-sUr5KCpeE6AXfvPp.htm)|Imaginary Weapon|auto-trad|
|[evocation-01-t4hGPdh6vAEgBFgZ.htm](spells/evocation-01-t4hGPdh6vAEgBFgZ.htm)|Victory Cry|auto-trad|
|[evocation-01-TjrZGl8z2INgf3vi.htm](spells/evocation-01-TjrZGl8z2INgf3vi.htm)|Friendfetch|auto-trad|
|[evocation-01-TSyFZNAbqfkRrcq0.htm](spells/evocation-01-TSyFZNAbqfkRrcq0.htm)|Concordant Choir|auto-trad|
|[evocation-01-vtCCVe869shMCJMj.htm](spells/evocation-01-vtCCVe869shMCJMj.htm)|Echoing Weapon|auto-trad|
|[evocation-01-W37iBXLsY2trJ1rS.htm](spells/evocation-01-W37iBXLsY2trJ1rS.htm)|Weapon Surge|auto-trad|
|[evocation-01-W6QlRwQLPoBSw6PZ.htm](spells/evocation-01-W6QlRwQLPoBSw6PZ.htm)|Snowball|auto-trad|
|[evocation-01-WBmvzNDfpwka3qT4.htm](spells/evocation-01-WBmvzNDfpwka3qT4.htm)|Light|auto-trad|
|[evocation-01-xGFAGlonNySXrunq.htm](spells/evocation-01-xGFAGlonNySXrunq.htm)|Buffeting Winds|auto-trad|
|[evocation-01-y6rAdMK6EFlV6U0t.htm](spells/evocation-01-y6rAdMK6EFlV6U0t.htm)|Burning Hands|auto-trad|
|[evocation-01-yafsV0ni7rFgqJBj.htm](spells/evocation-01-yafsV0ni7rFgqJBj.htm)|Biting Words|auto-trad|
|[evocation-01-yyz029C9eqfY38PT.htm](spells/evocation-01-yyz029C9eqfY38PT.htm)|Telekinetic Rend|auto-trad|
|[evocation-01-zA0jNIBRgLsyTpbm.htm](spells/evocation-01-zA0jNIBRgLsyTpbm.htm)|Scatter Scree|auto-trad|
|[evocation-01-zdb8cjOIDVKYMWdr.htm](spells/evocation-01-zdb8cjOIDVKYMWdr.htm)|Penumbral Shroud|auto-trad|
|[evocation-01-zul5cBTfr7NXHBZf.htm](spells/evocation-01-zul5cBTfr7NXHBZf.htm)|Dazzling Flash|auto-trad|
|[evocation-02-0qaqksrGGDj74HXE.htm](spells/evocation-02-0qaqksrGGDj74HXE.htm)|Glitterdust|auto-trad|
|[evocation-02-1xbFBQDRs0hT5xZ9.htm](spells/evocation-02-1xbFBQDRs0hT5xZ9.htm)|Shatter|auto-trad|
|[evocation-02-2ZdHjnpEQJuqOYSG.htm](spells/evocation-02-2ZdHjnpEQJuqOYSG.htm)|Flaming Sphere|auto-trad|
|[evocation-02-3GXU3ugrwLv0P7AH.htm](spells/evocation-02-3GXU3ugrwLv0P7AH.htm)|Ignite Fireworks|auto-trad|
|[evocation-02-3q9tBMWsWQKlXPPJ.htm](spells/evocation-02-3q9tBMWsWQKlXPPJ.htm)|Flame Wisp|auto-trad|
|[evocation-02-4GE2ZdODgIQtg51c.htm](spells/evocation-02-4GE2ZdODgIQtg51c.htm)|Darkness|auto-trad|
|[evocation-02-4Sg6ZngswhphxiBD.htm](spells/evocation-02-4Sg6ZngswhphxiBD.htm)|Swallow Light|auto-trad|
|[evocation-02-7dKtMehJ6YrXwJLR.htm](spells/evocation-02-7dKtMehJ6YrXwJLR.htm)|Magnetic Attraction|auto-trad|
|[evocation-02-8Hw3P6eurX1MYm7L.htm](spells/evocation-02-8Hw3P6eurX1MYm7L.htm)|Dancing Shield|auto-trad|
|[evocation-02-AsKLseOo8hwv5Jha.htm](spells/evocation-02-AsKLseOo8hwv5Jha.htm)|Invoke the Crimson Oath|auto-trad|
|[evocation-02-cB17yFc9456Pyfec.htm](spells/evocation-02-cB17yFc9456Pyfec.htm)|Vomit Swarm|auto-trad|
|[evocation-02-cf7Jkm39uEjUFtHt.htm](spells/evocation-02-cf7Jkm39uEjUFtHt.htm)|Sea Surge|auto-trad|
|[evocation-02-Dbd5W6G8U2vzWolN.htm](spells/evocation-02-Dbd5W6G8U2vzWolN.htm)|Consecrate|auto-trad|
|[evocation-02-Fq9yCbqI2RDt6Orw.htm](spells/evocation-02-Fq9yCbqI2RDt6Orw.htm)|Spiritual Weapon|auto-trad|
|[evocation-02-HRb2doyaLtaoCfi3.htm](spells/evocation-02-HRb2doyaLtaoCfi3.htm)|Faerie Fire|auto-trad|
|[evocation-02-kMffb6SvhCBMMI4k.htm](spells/evocation-02-kMffb6SvhCBMMI4k.htm)|Elemental Zone|auto-trad|
|[evocation-02-mrDi3v933gsmnw25.htm](spells/evocation-02-mrDi3v933gsmnw25.htm)|Telekinetic Maneuver|auto-trad|
|[evocation-02-pMTltbI3S3UIuFaR.htm](spells/evocation-02-pMTltbI3S3UIuFaR.htm)|Sun Blade|auto-trad|
|[evocation-02-Popa5umI3H33levx.htm](spells/evocation-02-Popa5umI3H33levx.htm)|Rime Slick|auto-trad|
|[evocation-02-SspI4ijjR7N7r4Cc.htm](spells/evocation-02-SspI4ijjR7N7r4Cc.htm)|Bralani Referendum|auto-trad|
|[evocation-02-v3vFzGazNSFEDdRB.htm](spells/evocation-02-v3vFzGazNSFEDdRB.htm)|Radiant Field|auto-trad|
|[evocation-02-v4KzRPol5XQOOmk0.htm](spells/evocation-02-v4KzRPol5XQOOmk0.htm)|Heat Metal|auto-trad|
|[evocation-02-WqPhJNzLa8vSjrH6.htm](spells/evocation-02-WqPhJNzLa8vSjrH6.htm)|Animated Assault|auto-trad|
|[evocation-02-Wt94cw03L77sbud7.htm](spells/evocation-02-Wt94cw03L77sbud7.htm)|Breath of Drought|auto-trad|
|[evocation-02-wzLkNU3AAqOSKFPR.htm](spells/evocation-02-wzLkNU3AAqOSKFPR.htm)|Sound Burst|auto-trad|
|[evocation-02-x0rWq0wS06dns4G2.htm](spells/evocation-02-x0rWq0wS06dns4G2.htm)|Final Sacrifice|auto-trad|
|[evocation-02-ynm8JIU3sc3qUMpa.htm](spells/evocation-02-ynm8JIU3sc3qUMpa.htm)|Continual Flame|auto-trad|
|[evocation-02-zK0e9d9DSnxC4eAD.htm](spells/evocation-02-zK0e9d9DSnxC4eAD.htm)|Sudden Bolt|auto-trad|
|[evocation-02-ZxHC7V7HtjUsB8zH.htm](spells/evocation-02-ZxHC7V7HtjUsB8zH.htm)|Scorching Ray|auto-trad|
|[evocation-03-06pzGkKTyPE3tHR8.htm](spells/evocation-03-06pzGkKTyPE3tHR8.htm)|Gravity Well|auto-trad|
|[evocation-03-0jWBnIDFpJjJShdQ.htm](spells/evocation-03-0jWBnIDFpJjJShdQ.htm)|Dragon Breath (Blue or Bronze)|auto-trad|
|[evocation-03-3P3M9SysSesALyoT.htm](spells/evocation-03-3P3M9SysSesALyoT.htm)|Dragon Breath (Sky)|auto-trad|
|[evocation-03-57ulIxg3Of2wCbEh.htm](spells/evocation-03-57ulIxg3Of2wCbEh.htm)|Electrified Crystal Ward|auto-trad|
|[evocation-03-6mmEz1UoTHGB7Sy9.htm](spells/evocation-03-6mmEz1UoTHGB7Sy9.htm)|Dragon Breath (Black, Brine or Copper)|auto-trad|
|[evocation-03-9AAkVUCwF6WVNNY2.htm](spells/evocation-03-9AAkVUCwF6WVNNY2.htm)|Lightning Bolt|auto-trad|
|[evocation-03-bmDgIbLa5NfkP97J.htm](spells/evocation-03-bmDgIbLa5NfkP97J.htm)|Dragon Breath (Silver or White)|auto-trad|
|[evocation-03-BqJAOPimCq5uCcEJ.htm](spells/evocation-03-BqJAOPimCq5uCcEJ.htm)|Shatter Mind|auto-trad|
|[evocation-03-DeF63UTmr7rchF60.htm](spells/evocation-03-DeF63UTmr7rchF60.htm)|Wall of Shadow|auto-trad|
|[evocation-03-E80SrXuBdZViPGiH.htm](spells/evocation-03-E80SrXuBdZViPGiH.htm)|Pulverizing Cascade|auto-trad|
|[evocation-03-ECApRjIIxD0JogOa.htm](spells/evocation-03-ECApRjIIxD0JogOa.htm)|Stone Lance|auto-trad|
|[evocation-03-eIPIZp2FUbFcLNdj.htm](spells/evocation-03-eIPIZp2FUbFcLNdj.htm)|Pillar of Water|auto-trad|
|[evocation-03-fI20AVwOzJMHXRdo.htm](spells/evocation-03-fI20AVwOzJMHXRdo.htm)|Levitate|auto-trad|
|[evocation-03-fnDBgdEeuzHRupDu.htm](spells/evocation-03-fnDBgdEeuzHRupDu.htm)|Dragon Breath (Umbral)|auto-trad|
|[evocation-03-HWrNMQENi9WSGbnF.htm](spells/evocation-03-HWrNMQENi9WSGbnF.htm)|Wall of Radiance|auto-trad|
|[evocation-03-it4ZsAi6XgvGcodc.htm](spells/evocation-03-it4ZsAi6XgvGcodc.htm)|Wall of Wind|auto-trad|
|[evocation-03-JcobNl4iE9HmMYtE.htm](spells/evocation-03-JcobNl4iE9HmMYtE.htm)|Dragon Breath|auto-trad|
|[evocation-03-jupppBeFqhe6LMIb.htm](spells/evocation-03-jupppBeFqhe6LMIb.htm)|Dragon Breath (Green)|auto-trad|
|[evocation-03-k9M0kUhhHHGYquiA.htm](spells/evocation-03-k9M0kUhhHHGYquiA.htm)|Dragon Breath (Cloud)|auto-trad|
|[evocation-03-KEkPxx7Sm6Xf4W3s.htm](spells/evocation-03-KEkPxx7Sm6Xf4W3s.htm)|Blazing Dive|auto-trad|
|[evocation-03-kfsv7zSHb3pr7s9v.htm](spells/evocation-03-kfsv7zSHb3pr7s9v.htm)|Dragon Breath (Gold, Magma or Red)|auto-trad|
|[evocation-03-kRsmUlSWhi6PJvZ7.htm](spells/evocation-03-kRsmUlSWhi6PJvZ7.htm)|Angelic Wings|auto-trad|
|[evocation-03-kuZnUNrhXHRYQ2eM.htm](spells/evocation-03-kuZnUNrhXHRYQ2eM.htm)|Eidolon's Wrath|auto-trad|
|[evocation-03-L37RTc7K79OUpZ7X.htm](spells/evocation-03-L37RTc7K79OUpZ7X.htm)|Interstellar Void|auto-trad|
|[evocation-03-LFSwMtQVP05EzlZe.htm](spells/evocation-03-LFSwMtQVP05EzlZe.htm)|Thunderburst|auto-trad|
|[evocation-03-LrFUj76CHDBV0vHW.htm](spells/evocation-03-LrFUj76CHDBV0vHW.htm)|Sun's Fury|auto-trad|
|[evocation-03-mtlyAyf30JnIvxVn.htm](spells/evocation-03-mtlyAyf30JnIvxVn.htm)|Moonlight Ray|auto-trad|
|[evocation-03-N0h3UodJFKdNQw1Y.htm](spells/evocation-03-N0h3UodJFKdNQw1Y.htm)|Dragon Breath (Brass)|auto-trad|
|[evocation-03-NeoRkU7C4BIz8fUM.htm](spells/evocation-03-NeoRkU7C4BIz8fUM.htm)|Bracing Tendrils|auto-trad|
|[evocation-03-nUSi2B7RhIKjaiXQ.htm](spells/evocation-03-nUSi2B7RhIKjaiXQ.htm)|Astral Rain|auto-trad|
|[evocation-03-NXm8z4W4YZazE2gn.htm](spells/evocation-03-NXm8z4W4YZazE2gn.htm)|Dragon Breath (Sea)|auto-trad|
|[evocation-03-NyxQAazFBdBAqZGX.htm](spells/evocation-03-NyxQAazFBdBAqZGX.htm)|Elemental Annihilation Wave|auto-trad|
|[evocation-03-oo7YcRC2gcez81PV.htm](spells/evocation-03-oo7YcRC2gcez81PV.htm)|Ki Blast|auto-trad|
|[evocation-03-Q1OWufw6dUiY8yEI.htm](spells/evocation-03-Q1OWufw6dUiY8yEI.htm)|Shroud of Flame|auto-trad|
|[evocation-03-sRfSBHWHdbIa0aGc.htm](spells/evocation-03-sRfSBHWHdbIa0aGc.htm)|Chilling Darkness|auto-trad|
|[evocation-03-sxQZ6yqTn0czJxVd.htm](spells/evocation-03-sxQZ6yqTn0czJxVd.htm)|Fireball|auto-trad|
|[evocation-03-T4QKmtYPeCgYxVGe.htm](spells/evocation-03-T4QKmtYPeCgYxVGe.htm)|Crashing Wave|auto-trad|
|[evocation-03-t5HAnKSHfSfHAJwH.htm](spells/evocation-03-t5HAnKSHfSfHAJwH.htm)|Dragon Breath (Crystal or Forest)|auto-trad|
|[evocation-03-tC9HJ3sfQJFTacrE.htm](spells/evocation-03-tC9HJ3sfQJFTacrE.htm)|Dragon Breath (Sovereign)|auto-trad|
|[evocation-03-TT9owkeMBXJxcERB.htm](spells/evocation-03-TT9owkeMBXJxcERB.htm)|Unseasonable Squall|auto-trad|
|[evocation-03-tvBhGfGcg2fsMOMe.htm](spells/evocation-03-tvBhGfGcg2fsMOMe.htm)|Dragon Breath (Underworld)|auto-trad|
|[evocation-03-tvO6Kmc2pQve9DC5.htm](spells/evocation-03-tvO6Kmc2pQve9DC5.htm)|Unfolding Wind Rush|auto-trad|
|[evocation-03-Tx8OqkBFA2QlaldW.htm](spells/evocation-03-Tx8OqkBFA2QlaldW.htm)|Magnetic Acceleration|auto-trad|
|[evocation-03-v4QHuVOhFD1JMAqu.htm](spells/evocation-03-v4QHuVOhFD1JMAqu.htm)|Powerful Inhalation|auto-trad|
|[evocation-03-vhMCd15ZwNJn0zen.htm](spells/evocation-03-vhMCd15ZwNJn0zen.htm)|Malicious Shadow|auto-trad|
|[evocation-03-Wi2HcreCfujKiCvW.htm](spells/evocation-03-Wi2HcreCfujKiCvW.htm)|Whirling Flames|auto-trad|
|[evocation-03-X1b9ollVMSLXDN9o.htm](spells/evocation-03-X1b9ollVMSLXDN9o.htm)|Litany against Wrath|auto-trad|
|[evocation-03-X4T5RlQBrdpmA35n.htm](spells/evocation-03-X4T5RlQBrdpmA35n.htm)|Entropic Wheel|auto-trad|
|[evocation-03-ytboJsyZEbE1MLeV.htm](spells/evocation-03-ytboJsyZEbE1MLeV.htm)|Unbreaking Wave Advance|auto-trad|
|[evocation-03-Z3kJty995FkrsZRb.htm](spells/evocation-03-Z3kJty995FkrsZRb.htm)|Combustion|auto-trad|
|[evocation-03-ZJOQBqkNErZu1QAa.htm](spells/evocation-03-ZJOQBqkNErZu1QAa.htm)|Wall of Virtue|auto-trad|
|[evocation-04-2SYq0ZTsOtJEigFx.htm](spells/evocation-04-2SYq0ZTsOtJEigFx.htm)|Mystic Beacon|auto-trad|
|[evocation-04-3ySPK8qwNcuESwa0.htm](spells/evocation-04-3ySPK8qwNcuESwa0.htm)|Purifying Veil|auto-trad|
|[evocation-04-5LGDygjRxURUOKGR.htm](spells/evocation-04-5LGDygjRxURUOKGR.htm)|Radiant Beam|auto-trad|
|[evocation-04-7d4DUTDIlzDa8OvX.htm](spells/evocation-04-7d4DUTDIlzDa8OvX.htm)|Destructive Aura|auto-trad|
|[evocation-04-8M03UxGXjYyDFAoy.htm](spells/evocation-04-8M03UxGXjYyDFAoy.htm)|Weapon Storm|auto-trad|
|[evocation-04-DZ9bzXYqMjAK9TzC.htm](spells/evocation-04-DZ9bzXYqMjAK9TzC.htm)|Holy Cascade|auto-trad|
|[evocation-04-Hb8GdAhP0zBCv3zU.htm](spells/evocation-04-Hb8GdAhP0zBCv3zU.htm)|Chromatic Ray|auto-trad|
|[evocation-04-hVU9msO9yGkxKZ3J.htm](spells/evocation-04-hVU9msO9yGkxKZ3J.htm)|Divine Wrath|auto-trad|
|[evocation-04-IarZrgCeaiUqOuRu.htm](spells/evocation-04-IarZrgCeaiUqOuRu.htm)|Wall of Fire|auto-trad|
|[evocation-04-K4LXpaBWrGy6jIER.htm](spells/evocation-04-K4LXpaBWrGy6jIER.htm)|Downpour|auto-trad|
|[evocation-04-KG7amdeXWc7MjGXe.htm](spells/evocation-04-KG7amdeXWc7MjGXe.htm)|Asterism|auto-trad|
|[evocation-04-kHyjQbibRGPNCixx.htm](spells/evocation-04-kHyjQbibRGPNCixx.htm)|Ice Storm|auto-trad|
|[evocation-04-LrhTFHUtSS9ahogL.htm](spells/evocation-04-LrhTFHUtSS9ahogL.htm)|Traveler's Transit|auto-trad|
|[evocation-04-n7OgbKme4hNwxVwQ.htm](spells/evocation-04-n7OgbKme4hNwxVwQ.htm)|Draw the Lightning|auto-trad|
|[evocation-04-NxOYiKCqcuAHVRCj.htm](spells/evocation-04-NxOYiKCqcuAHVRCj.htm)|Transcribe Moment|auto-trad|
|[evocation-04-oOaEfsdVpgTXRhrY.htm](spells/evocation-04-oOaEfsdVpgTXRhrY.htm)|Runic Impression|auto-trad|
|[evocation-04-qTzvVnAMWL05VitC.htm](spells/evocation-04-qTzvVnAMWL05VitC.htm)|Painful Vibrations|auto-trad|
|[evocation-04-SkarN4VlNxSJSJNw.htm](spells/evocation-04-SkarN4VlNxSJSJNw.htm)|Wild Winds Stance|auto-trad|
|[evocation-04-uJXs4M6IeixfPBLc.htm](spells/evocation-04-uJXs4M6IeixfPBLc.htm)|Clinging Shadows Stance|auto-trad|
|[evocation-04-vfHr1N8Rf2bBpdgn.htm](spells/evocation-04-vfHr1N8Rf2bBpdgn.htm)|Elemental Tempest|auto-trad|
|[evocation-04-VmqdVWCb8zAUCW8S.htm](spells/evocation-04-VmqdVWCb8zAUCW8S.htm)|Debilitating Dichotomy|auto-trad|
|[evocation-04-VUwpDY4Z91s9QCg0.htm](spells/evocation-04-VUwpDY4Z91s9QCg0.htm)|Bottle the Storm|auto-trad|
|[evocation-04-wjJW9hWY5CkkMvY5.htm](spells/evocation-04-wjJW9hWY5CkkMvY5.htm)|Diamond Dust|auto-trad|
|[evocation-04-Y3G6Y6EDgCY0s3fq.htm](spells/evocation-04-Y3G6Y6EDgCY0s3fq.htm)|Hydraulic Torrent|auto-trad|
|[evocation-04-YrzBLPLd3r9m6t1p.htm](spells/evocation-04-YrzBLPLd3r9m6t1p.htm)|Fire Shield|auto-trad|
|[evocation-05-2mVW1KT3AjW2pvDO.htm](spells/evocation-05-2mVW1KT3AjW2pvDO.htm)|Litany against Sloth|auto-trad|
|[evocation-05-3puDanGfpEt6jK5k.htm](spells/evocation-05-3puDanGfpEt6jK5k.htm)|Cone of Cold|auto-trad|
|[evocation-05-8TBgEzjZxPaOJOm1.htm](spells/evocation-05-8TBgEzjZxPaOJOm1.htm)|Wronged Monk's Wrath|auto-trad|
|[evocation-05-9BGEf9Sv5rgNBCk0.htm](spells/evocation-05-9BGEf9Sv5rgNBCk0.htm)|Dance of Darkness|auto-trad|
|[evocation-05-9bOqFkewRz7Z3HZ5.htm](spells/evocation-05-9bOqFkewRz7Z3HZ5.htm)|Scouring Pulse|auto-trad|
|[evocation-05-9LHr9SuDLTicdbXs.htm](spells/evocation-05-9LHr9SuDLTicdbXs.htm)|Hellfire Plume|auto-trad|
|[evocation-05-ALuvl9GYawnsZZCx.htm](spells/evocation-05-ALuvl9GYawnsZZCx.htm)|Unfolding Wind Buffet|auto-trad|
|[evocation-05-bZMwSGc9t5K7uxZV.htm](spells/evocation-05-bZMwSGc9t5K7uxZV.htm)|Redistribute Potential|auto-trad|
|[evocation-05-crF4g9jRN1y84MSD.htm](spells/evocation-05-crF4g9jRN1y84MSD.htm)|Abyssal Wrath|auto-trad|
|[evocation-05-E3X2RbzWHCdz7gsk.htm](spells/evocation-05-E3X2RbzWHCdz7gsk.htm)|Flame Strike|auto-trad|
|[evocation-05-gasOL6a0WhOhUsgL.htm](spells/evocation-05-gasOL6a0WhOhUsgL.htm)|Etheric Shards|auto-trad|
|[evocation-05-HMTloW1hvRFJ5Z2D.htm](spells/evocation-05-HMTloW1hvRFJ5Z2D.htm)|Consuming Darkness|auto-trad|
|[evocation-05-i88Vi47PDDoP6gQv.htm](spells/evocation-05-i88Vi47PDDoP6gQv.htm)|Geyser|auto-trad|
|[evocation-05-Ifc2b6bNVdjKV7Si.htm](spells/evocation-05-Ifc2b6bNVdjKV7Si.htm)|Stormburst|auto-trad|
|[evocation-05-IqJ9URobmJ9L9UBG.htm](spells/evocation-05-IqJ9URobmJ9L9UBG.htm)|Shadow Blast|auto-trad|
|[evocation-05-JyT346VmGtRLsDnV.htm](spells/evocation-05-JyT346VmGtRLsDnV.htm)|Lightning Storm|auto-trad|
|[evocation-05-OYOCWWefZPEfp8Nl.htm](spells/evocation-05-OYOCWWefZPEfp8Nl.htm)|Blazing Fissure|auto-trad|
|[evocation-05-Qlp8G3knwLGhAxQ0.htm](spells/evocation-05-Qlp8G3knwLGhAxQ0.htm)|Elemental Blast|auto-trad|
|[evocation-05-R5FHRv7VqyRnxg2t.htm](spells/evocation-05-R5FHRv7VqyRnxg2t.htm)|Wall of Ice|auto-trad|
|[evocation-05-rMGkk23qWvxTmgA8.htm](spells/evocation-05-rMGkk23qWvxTmgA8.htm)|Forceful Hand|auto-trad|
|[evocation-05-TLqMFgCewxuricJw.htm](spells/evocation-05-TLqMFgCewxuricJw.htm)|Repelling Pulse|auto-trad|
|[evocation-05-tpLTLbJUrYcMWGld.htm](spells/evocation-05-tpLTLbJUrYcMWGld.htm)|Telekinetic Haul|auto-trad|
|[evocation-05-ViqzVEprQVzCXZ9f.htm](spells/evocation-05-ViqzVEprQVzCXZ9f.htm)|Dancing Blade|auto-trad|
|[evocation-05-zCLyETFoPqCQXLVy.htm](spells/evocation-05-zCLyETFoPqCQXLVy.htm)|Flowing Strike|auto-trad|
|[evocation-05-zfn5RqAdF63neqpP.htm](spells/evocation-05-zfn5RqAdF63neqpP.htm)|Control Water|auto-trad|
|[evocation-05-zoY0fQYTF1NzezTg.htm](spells/evocation-05-zoY0fQYTF1NzezTg.htm)|Steal the Sky|auto-trad|
|[evocation-05-ZW8ovbu1etdfMre3.htm](spells/evocation-05-ZW8ovbu1etdfMre3.htm)|Acid Storm|auto-trad|
|[evocation-05-ZyREiMaul0VhDYh3.htm](spells/evocation-05-ZyREiMaul0VhDYh3.htm)|Glacial Heart|auto-trad|
|[evocation-06-7Iela4GgVeO3LfAo.htm](spells/evocation-06-7Iela4GgVeO3LfAo.htm)|Wall of Force|auto-trad|
|[evocation-06-bynT1UKaDqr8dLNM.htm](spells/evocation-06-bynT1UKaDqr8dLNM.htm)|Flaming Fusillade|auto-trad|
|[evocation-06-Gn5deIoobHpd3SiR.htm](spells/evocation-06-Gn5deIoobHpd3SiR.htm)|Flame Vortex|auto-trad|
|[evocation-06-kuoYff1csM5eAcAP.htm](spells/evocation-06-kuoYff1csM5eAcAP.htm)|Fire Seeds|auto-trad|
|[evocation-06-mtxMpGWpwwWSbySj.htm](spells/evocation-06-mtxMpGWpwwWSbySj.htm)|For Love, For Lightning|auto-trad|
|[evocation-06-P1iqzEIidRagUs7W.htm](spells/evocation-06-P1iqzEIidRagUs7W.htm)|Zero Gravity|auto-trad|
|[evocation-06-peCF6VArm8urfwxZ.htm](spells/evocation-06-peCF6VArm8urfwxZ.htm)|Blade Barrier|auto-trad|
|[evocation-06-qGWORxQ0aSsH2taf.htm](spells/evocation-06-qGWORxQ0aSsH2taf.htm)|Suffocate|auto-trad|
|[evocation-06-r7ihOgKv19eJQnik.htm](spells/evocation-06-r7ihOgKv19eJQnik.htm)|Disintegrate|auto-trad|
|[evocation-06-TDNlDWbYb58Y55Da.htm](spells/evocation-06-TDNlDWbYb58Y55Da.htm)|Chain Lightning|auto-trad|
|[evocation-06-uhEjKSFdEhXzszh6.htm](spells/evocation-06-uhEjKSFdEhXzszh6.htm)|Poltergeist's Fury|auto-trad|
|[evocation-06-YIMampGpij4Y30yE.htm](spells/evocation-06-YIMampGpij4Y30yE.htm)|Stone Tell|auto-trad|
|[evocation-07-37ESlJzUvVbOudOT.htm](spells/evocation-07-37ESlJzUvVbOudOT.htm)|Reverse Gravity|auto-trad|
|[evocation-07-a3aQxCpoj1q1NQxC.htm](spells/evocation-07-a3aQxCpoj1q1NQxC.htm)|Sunburst|auto-trad|
|[evocation-07-d6o52BnjViNz7Gub.htm](spells/evocation-07-d6o52BnjViNz7Gub.htm)|Prismatic Spray|auto-trad|
|[evocation-07-EgkypvUZIZkx1UlQ.htm](spells/evocation-07-EgkypvUZIZkx1UlQ.htm)|Blightburn Blast|auto-trad|
|[evocation-07-HES5jvGiNZZnJycK.htm](spells/evocation-07-HES5jvGiNZZnJycK.htm)|Force Cage|auto-trad|
|[evocation-07-n3b3pDmA6L5YRTyq.htm](spells/evocation-07-n3b3pDmA6L5YRTyq.htm)|Litany of Righteousness|auto-trad|
|[evocation-07-n8eEXXAtguoErW0y.htm](spells/evocation-07-n8eEXXAtguoErW0y.htm)|Shadow's Web|auto-trad|
|[evocation-07-O7ZEqWjwdKyo2CUv.htm](spells/evocation-07-O7ZEqWjwdKyo2CUv.htm)|Volcanic Eruption|auto-trad|
|[evocation-07-oahqARSgOGDRybBQ.htm](spells/evocation-07-oahqARSgOGDRybBQ.htm)|Control Sand|auto-trad|
|[evocation-07-sFwoKj0TsacsmoWj.htm](spells/evocation-07-sFwoKj0TsacsmoWj.htm)|Darklight|auto-trad|
|[evocation-07-Shiuhdb2nO6Qgk3k.htm](spells/evocation-07-Shiuhdb2nO6Qgk3k.htm)|Moonburst|auto-trad|
|[evocation-07-sX2o0HH4RjJDAZ8C.htm](spells/evocation-07-sX2o0HH4RjJDAZ8C.htm)|Divine Decree|auto-trad|
|[evocation-07-tYP8unoR0a5Dq9EA.htm](spells/evocation-07-tYP8unoR0a5Dq9EA.htm)|Litany of Depravity|auto-trad|
|[evocation-07-uc4I1diSSX6XYzb3.htm](spells/evocation-07-uc4I1diSSX6XYzb3.htm)|Telekinetic Bombardment|auto-trad|
|[evocation-07-vFkz0gVBUT2gGnm1.htm](spells/evocation-07-vFkz0gVBUT2gGnm1.htm)|Frigid Flurry|auto-trad|
|[evocation-07-x9RIFhquazom4p02.htm](spells/evocation-07-x9RIFhquazom4p02.htm)|Deity's Strike|auto-trad|
|[evocation-07-XnpRRUVAVoY6z88H.htm](spells/evocation-07-XnpRRUVAVoY6z88H.htm)|Empower Ley Line|auto-trad|
|[evocation-08-2CNqkt2s2IYkVnv6.htm](spells/evocation-08-2CNqkt2s2IYkVnv6.htm)|Imprisonment|auto-trad|
|[evocation-08-8fEfjvC01gNclDKJ.htm](spells/evocation-08-8fEfjvC01gNclDKJ.htm)|Deluge|auto-trad|
|[evocation-08-8hKW4mWQyLnkHVta.htm](spells/evocation-08-8hKW4mWQyLnkHVta.htm)|Whirlwind|auto-trad|
|[evocation-08-BKIet436snMNcnez.htm](spells/evocation-08-BKIet436snMNcnez.htm)|Polar Ray|auto-trad|
|[evocation-08-fBoiPXmcIO50OFFR.htm](spells/evocation-08-fBoiPXmcIO50OFFR.htm)|Boil Blood|auto-trad|
|[evocation-08-vm9O7ne48NM72yrJ.htm](spells/evocation-08-vm9O7ne48NM72yrJ.htm)|Falling Sky|auto-trad|
|[evocation-08-wi405lBjPcbF1DeR.htm](spells/evocation-08-wi405lBjPcbF1DeR.htm)|Punishing Winds|auto-trad|
|[evocation-08-x7SPrsRxGb2Vy2nu.htm](spells/evocation-08-x7SPrsRxGb2Vy2nu.htm)|Earthquake|auto-trad|
|[evocation-08-XkDCzMIyc0YOjw05.htm](spells/evocation-08-XkDCzMIyc0YOjw05.htm)|Control Weather|auto-trad|
|[evocation-09-4WS7HrFjwNvTn8T2.htm](spells/evocation-09-4WS7HrFjwNvTn8T2.htm)|Implosion|auto-trad|
|[evocation-09-axYqY70kYu2lN20R.htm](spells/evocation-09-axYqY70kYu2lN20R.htm)|Unbreaking Wave Containment|auto-trad|
|[evocation-09-r4HLQcYwB62bTayl.htm](spells/evocation-09-r4HLQcYwB62bTayl.htm)|Storm of Vengeance|auto-trad|
|[evocation-09-XjNROFtWnwovhCsq.htm](spells/evocation-09-XjNROFtWnwovhCsq.htm)|Unfolding Wind Crash|auto-trad|
|[evocation-09-XYhU3Wi94n1RKxTa.htm](spells/evocation-09-XYhU3Wi94n1RKxTa.htm)|Storm Lord|auto-trad|
|[evocation-09-ZqmP9gijBmK7y8Xy.htm](spells/evocation-09-ZqmP9gijBmK7y8Xy.htm)|Weapon of Judgment|auto-trad|
|[evocation-10-wLIvH0AT1u7oa64N.htm](spells/evocation-10-wLIvH0AT1u7oa64N.htm)|Cataclysm|auto-trad|
|[illusion-01-2oH5IufzdESuYxat.htm](spells/illusion-01-2oH5IufzdESuYxat.htm)|Illusory Object|auto-trad|
|[illusion-01-4ZGte0i9YbLh4dRi.htm](spells/illusion-01-4ZGte0i9YbLh4dRi.htm)|Item Facade|auto-trad|
|[illusion-01-6GjJtLJnwC18Y0aZ.htm](spells/illusion-01-6GjJtLJnwC18Y0aZ.htm)|Lift Nature's Caul|auto-trad|
|[illusion-01-atlgGNI1E1Ox3O3a.htm](spells/illusion-01-atlgGNI1E1Ox3O3a.htm)|Ghost Sound|auto-trad|
|[illusion-01-fXdADBwxmBsU9xPk.htm](spells/illusion-01-fXdADBwxmBsU9xPk.htm)|Warped Terrain|auto-trad|
|[illusion-01-i35dpZFI7jZcRoBo.htm](spells/illusion-01-i35dpZFI7jZcRoBo.htm)|Illusory Disguise|auto-trad|
|[illusion-01-iohRnd4gUCmJlh54.htm](spells/illusion-01-iohRnd4gUCmJlh54.htm)|Lose the Path|auto-trad|
|[illusion-01-nX85Brzax9f650aK.htm](spells/illusion-01-nX85Brzax9f650aK.htm)|Invisible Item|auto-trad|
|[illusion-01-PRrZ7anETWPm90YY.htm](spells/illusion-01-PRrZ7anETWPm90YY.htm)|Magic Aura|auto-trad|
|[illusion-01-UKsIOWmMx4hSpafl.htm](spells/illusion-01-UKsIOWmMx4hSpafl.htm)|Color Spray|auto-trad|
|[illusion-01-vLzFcIaSXs7YTIqJ.htm](spells/illusion-01-vLzFcIaSXs7YTIqJ.htm)|Message|auto-trad|
|[illusion-01-W02bHXylIpoXbO4e.htm](spells/illusion-01-W02bHXylIpoXbO4e.htm)|Bullhorn|auto-trad|
|[illusion-01-X56VdA98VedAfGTU.htm](spells/illusion-01-X56VdA98VedAfGTU.htm)|Thicket of Knives|auto-trad|
|[illusion-01-xTpp8dHZsNMDm75B.htm](spells/illusion-01-xTpp8dHZsNMDm75B.htm)|Splash of Art|auto-trad|
|[illusion-01-yH13KXUK2x093NUv.htm](spells/illusion-01-yH13KXUK2x093NUv.htm)|Face in the Crowd|auto-trad|
|[illusion-01-yV7Ouzaoe7DHLESI.htm](spells/illusion-01-yV7Ouzaoe7DHLESI.htm)|Ventriloquism|auto-trad|
|[illusion-01-ZeftDoh0nFAXBAWY.htm](spells/illusion-01-ZeftDoh0nFAXBAWY.htm)|Appearance of Wealth|auto-trad|
|[illusion-01-znMvKqcRDilIVMwA.htm](spells/illusion-01-znMvKqcRDilIVMwA.htm)|Exchange Image|auto-trad|
|[illusion-02-3JG1t3T4mWn6vTke.htm](spells/illusion-02-3JG1t3T4mWn6vTke.htm)|Blur|auto-trad|
|[illusion-02-c2bTWBNO1BYX4Zfg.htm](spells/illusion-02-c2bTWBNO1BYX4Zfg.htm)|Misdirection|auto-trad|
|[illusion-02-CQWTyDxiVSW4uRn7.htm](spells/illusion-02-CQWTyDxiVSW4uRn7.htm)|Empty Pack|auto-trad|
|[illusion-02-EDwakYQTS1t6XHD4.htm](spells/illusion-02-EDwakYQTS1t6XHD4.htm)|Umbral Extraction|auto-trad|
|[illusion-02-eIQ86FOXK34HiNLs.htm](spells/illusion-02-eIQ86FOXK34HiNLs.htm)|Magic Mouth|auto-trad|
|[illusion-02-f8SBoXiXQjlCKqly.htm](spells/illusion-02-f8SBoXiXQjlCKqly.htm)|Illusory Creature|auto-trad|
|[illusion-02-gIdDLrbswTV3OBJy.htm](spells/illusion-02-gIdDLrbswTV3OBJy.htm)|Silence|auto-trad|
|[illusion-02-j8vIoIEWElvpwkcI.htm](spells/illusion-02-j8vIoIEWElvpwkcI.htm)|Mirror Image|auto-trad|
|[illusion-02-L0GoJpHxSD0wRY5k.htm](spells/illusion-02-L0GoJpHxSD0wRY5k.htm)|Phantasmal Treasure|auto-trad|
|[illusion-02-Mkbq9xlAUxHUHyR2.htm](spells/illusion-02-Mkbq9xlAUxHUHyR2.htm)|Paranoia|auto-trad|
|[illusion-02-q5qmNn144ZJGxnvJ.htm](spells/illusion-02-q5qmNn144ZJGxnvJ.htm)|Befitting Attire|auto-trad|
|[illusion-02-QZ7OHptO1xnwaruq.htm](spells/illusion-02-QZ7OHptO1xnwaruq.htm)|Penumbral Disguise|auto-trad|
|[illusion-02-uopaLE01meX11Mbw.htm](spells/illusion-02-uopaLE01meX11Mbw.htm)|Umbral Mindtheft|auto-trad|
|[illusion-02-WfINystwLv9ohYpS.htm](spells/illusion-02-WfINystwLv9ohYpS.htm)|Phantom Crowd|auto-trad|
|[illusion-02-XXqE1eY3w3z6xJCB.htm](spells/illusion-02-XXqE1eY3w3z6xJCB.htm)|Invisibility|auto-trad|
|[illusion-03-87zLUuTMmZ9zc7gH.htm](spells/illusion-03-87zLUuTMmZ9zc7gH.htm)|Oneiric Mire|auto-trad|
|[illusion-03-8cuqRFB3mWBOgy61.htm](spells/illusion-03-8cuqRFB3mWBOgy61.htm)|Shadow Projectile|auto-trad|
|[illusion-03-8MAHUK6jphbME4BR.htm](spells/illusion-03-8MAHUK6jphbME4BR.htm)|Mindscape Door|auto-trad|
|[illusion-03-aZg3amDcrXz3cLCz.htm](spells/illusion-03-aZg3amDcrXz3cLCz.htm)|Horrific Visage|auto-trad|
|[illusion-03-FrKPwgFxWIGMGgs4.htm](spells/illusion-03-FrKPwgFxWIGMGgs4.htm)|Deceiver's Cloak|auto-trad|
|[illusion-03-HHGUBGle4OjoxvNR.htm](spells/illusion-03-HHGUBGle4OjoxvNR.htm)|Sculpt Sound|auto-trad|
|[illusion-03-I0j56TNRmGcTyoqJ.htm](spells/illusion-03-I0j56TNRmGcTyoqJ.htm)|Invisibility Sphere|auto-trad|
|[illusion-03-JHntYF0SbaWKq7wR.htm](spells/illusion-03-JHntYF0SbaWKq7wR.htm)|Distracting Chatter|auto-trad|
|[illusion-03-K2WpC3FFoHrqX9Q5.htm](spells/illusion-03-K2WpC3FFoHrqX9Q5.htm)|Hypnotic Pattern|auto-trad|
|[illusion-03-pQ3NIzZXeIIcU81C.htm](spells/illusion-03-pQ3NIzZXeIIcU81C.htm)|Spirit Veil|auto-trad|
|[illusion-03-PVXqMko4yGgw90uo.htm](spells/illusion-03-PVXqMko4yGgw90uo.htm)|Heart's Desire|auto-trad|
|[illusion-03-Pwq6T7xpfAJXV5aj.htm](spells/illusion-03-Pwq6T7xpfAJXV5aj.htm)|Phantom Prison|auto-trad|
|[illusion-03-VVAZPCvd4d90qVA1.htm](spells/illusion-03-VVAZPCvd4d90qVA1.htm)|Secret Page|auto-trad|
|[illusion-03-YQWq1DZuLRk32M3h.htm](spells/illusion-03-YQWq1DZuLRk32M3h.htm)|Inscrutable Mask|auto-trad|
|[illusion-04-5z1700cfG8Ybm0e6.htm](spells/illusion-04-5z1700cfG8Ybm0e6.htm)|False Nature|auto-trad|
|[illusion-04-A7w3YQBrFNH8KYsB.htm](spells/illusion-04-A7w3YQBrFNH8KYsB.htm)|Mirror's Misfortune|auto-trad|
|[illusion-04-a8xXWCQhIR7o6IvP.htm](spells/illusion-04-a8xXWCQhIR7o6IvP.htm)|Invisibility Curtain|auto-trad|
|[illusion-04-h5UqEdeqK8iTcU0J.htm](spells/illusion-04-h5UqEdeqK8iTcU0J.htm)|Simulacrum|auto-trad|
|[illusion-04-HBJPsonQnWcC3qdX.htm](spells/illusion-04-HBJPsonQnWcC3qdX.htm)|Hallucinatory Terrain|auto-trad|
|[illusion-04-HisaZTk67YAxLGBq.htm](spells/illusion-04-HisaZTk67YAxLGBq.htm)|Ephemeral Hazards|auto-trad|
|[illusion-04-hkfH9Z53hPzcOwNB.htm](spells/illusion-04-hkfH9Z53hPzcOwNB.htm)|Veil|auto-trad|
|[illusion-04-HvBpSeUDg0KcK2Hg.htm](spells/illusion-04-HvBpSeUDg0KcK2Hg.htm)|Ocular Overload|auto-trad|
|[illusion-04-IKb9EOfsrhScO3GO.htm](spells/illusion-04-IKb9EOfsrhScO3GO.htm)|Phantasmal Protagonist|auto-trad|
|[illusion-04-jqb52wy7A0Eqwuzx.htm](spells/illusion-04-jqb52wy7A0Eqwuzx.htm)|Vision of Beauty|auto-trad|
|[illusion-04-juzb53CW6Uzz5pFY.htm](spells/illusion-04-juzb53CW6Uzz5pFY.htm)|Umbral Graft|auto-trad|
|[illusion-04-mer4V7vWdTs1oLbG.htm](spells/illusion-04-mer4V7vWdTs1oLbG.htm)|Isolation|auto-trad|
|[illusion-04-Nun72GTmb31YqSKh.htm](spells/illusion-04-Nun72GTmb31YqSKh.htm)|Invisibility Cloak|auto-trad|
|[illusion-04-srEKUvym9kDVbEhD.htm](spells/illusion-04-srEKUvym9kDVbEhD.htm)|Replicate|auto-trad|
|[illusion-04-tlcrVRqW1MSKJ5IC.htm](spells/illusion-04-tlcrVRqW1MSKJ5IC.htm)|Phantasmal Killer|auto-trad|
|[illusion-04-Uqj344bezBq3ESdq.htm](spells/illusion-04-Uqj344bezBq3ESdq.htm)|Nightmare|auto-trad|
|[illusion-04-VGK9s6LCMMS027zP.htm](spells/illusion-04-VGK9s6LCMMS027zP.htm)|Confront Selves|auto-trad|
|[illusion-04-vhe9DduqaivMs8FV.htm](spells/illusion-04-vhe9DduqaivMs8FV.htm)|Ghostly Transcription|auto-trad|
|[illusion-04-ZhLYJlOZzUB1OKoe.htm](spells/illusion-04-ZhLYJlOZzUB1OKoe.htm)|Trickster's Twin|auto-trad|
|[illusion-05-CmZCq4htcZ6W0TKk.htm](spells/illusion-05-CmZCq4htcZ6W0TKk.htm)|Mirror Malefactors|auto-trad|
|[illusion-05-CuRat8IXBUu9C3Yw.htm](spells/illusion-05-CuRat8IXBUu9C3Yw.htm)|Portrait of the Artist|auto-trad|
|[illusion-05-DdXKfIjDtORUtUvY.htm](spells/illusion-05-DdXKfIjDtORUtUvY.htm)|Fey Glamour|auto-trad|
|[illusion-05-m4Mc5XbdML92BKOE.htm](spells/illusion-05-m4Mc5XbdML92BKOE.htm)|Strange Geometry|auto-trad|
|[illusion-05-PEfSofHm73IT3Khc.htm](spells/illusion-05-PEfSofHm73IT3Khc.htm)|House of Imaginary Walls|auto-trad|
|[illusion-05-pfFxj4NI5KwO119I.htm](spells/illusion-05-pfFxj4NI5KwO119I.htm)|Blind Eye|auto-trad|
|[illusion-05-Pp88ueeq4AlkaL6g.htm](spells/illusion-05-Pp88ueeq4AlkaL6g.htm)|Construct Mindscape|auto-trad|
|[illusion-05-RCbLd7dfquHnuvrZ.htm](spells/illusion-05-RCbLd7dfquHnuvrZ.htm)|False Vision|auto-trad|
|[illusion-05-scTRIrTfXquVYHGw.htm](spells/illusion-05-scTRIrTfXquVYHGw.htm)|Drop Dead|auto-trad|
|[illusion-05-TCk2MDwf5L5OYjFC.htm](spells/illusion-05-TCk2MDwf5L5OYjFC.htm)|Cloak of Colors|auto-trad|
|[illusion-05-tcwT97RWKxsJiefG.htm](spells/illusion-05-tcwT97RWKxsJiefG.htm)|Shadow Siphon|auto-trad|
|[illusion-05-U58aQWJ47VrI36yP.htm](spells/illusion-05-U58aQWJ47VrI36yP.htm)|Hallucination|auto-trad|
|[illusion-05-Ucf8eynbZMfUucjE.htm](spells/illusion-05-Ucf8eynbZMfUucjE.htm)|Illusory Scene|auto-trad|
|[illusion-05-wykbu2KW9tMBRySr.htm](spells/illusion-05-wykbu2KW9tMBRySr.htm)|Hologram Cage|auto-trad|
|[illusion-05-xPrbxyOEwy9QaPVn.htm](spells/illusion-05-xPrbxyOEwy9QaPVn.htm)|Chameleon Coat|auto-trad|
|[illusion-06-0XP2XOxT9VSiXFDr.htm](spells/illusion-06-0XP2XOxT9VSiXFDr.htm)|Phantasmal Calamity|auto-trad|
|[illusion-06-EOWh6VVcSjB3WPjX.htm](spells/illusion-06-EOWh6VVcSjB3WPjX.htm)|Shadow Illusion|auto-trad|
|[illusion-06-FgRpddPORsRFwNoX.htm](spells/illusion-06-FgRpddPORsRFwNoX.htm)|Chromatic Image|auto-trad|
|[illusion-06-m34WOIGZCEg1h76G.htm](spells/illusion-06-m34WOIGZCEg1h76G.htm)|Blanket Of Stars|auto-trad|
|[illusion-06-RQjSQVZRG497cJhX.htm](spells/illusion-06-RQjSQVZRG497cJhX.htm)|Vibrant Pattern|auto-trad|
|[illusion-06-VUMtDHr8CRwwr3Mj.htm](spells/illusion-06-VUMtDHr8CRwwr3Mj.htm)|Aura of the Unremarkable|auto-trad|
|[illusion-06-WPXzPl7YbMEIGWfi.htm](spells/illusion-06-WPXzPl7YbMEIGWfi.htm)|Mislead|auto-trad|
|[illusion-07-0873MWM0qKDDv81O.htm](spells/illusion-07-0873MWM0qKDDv81O.htm)|Project Image|auto-trad|
|[illusion-07-jBGAYmR0BkkbpJvG.htm](spells/illusion-07-jBGAYmR0BkkbpJvG.htm)|Visions of Danger|auto-trad|
|[illusion-07-O6VQC1Bs4aSYDa6R.htm](spells/illusion-07-O6VQC1Bs4aSYDa6R.htm)|Mask of Terror|auto-trad|
|[illusion-07-VTb0yI6P1bLkzuRr.htm](spells/illusion-07-VTb0yI6P1bLkzuRr.htm)|Shadow Raid|auto-trad|
|[illusion-08-rwCh2qTYPA44KEoK.htm](spells/illusion-08-rwCh2qTYPA44KEoK.htm)|Dream Council|auto-trad|
|[illusion-08-TvZiwZRianfTSbEg.htm](spells/illusion-08-TvZiwZRianfTSbEg.htm)|Hypnopompic Terrors|auto-trad|
|[illusion-08-uEyfLoFQsRKBRIcB.htm](spells/illusion-08-uEyfLoFQsRKBRIcB.htm)|Scintillating Pattern|auto-trad|
|[illusion-08-wfleiawxsfhpRRwf.htm](spells/illusion-08-wfleiawxsfhpRRwf.htm)|Disappearance|auto-trad|
|[illusion-08-YiIOsc8T6E2iDxgh.htm](spells/illusion-08-YiIOsc8T6E2iDxgh.htm)|Undermine Reality|auto-trad|
|[illusion-09-hq57j7Nif1zuQ2Ab.htm](spells/illusion-09-hq57j7Nif1zuQ2Ab.htm)|Unspeakable Shadow|auto-trad|
|[illusion-09-nA0XlPsnMNrQMpio.htm](spells/illusion-09-nA0XlPsnMNrQMpio.htm)|Fantastic Facade|auto-trad|
|[illusion-09-qDjeG6dxT4aEEC6J.htm](spells/illusion-09-qDjeG6dxT4aEEC6J.htm)|Weird|auto-trad|
|[illusion-10-hvKtmoHwekDZ5iOH.htm](spells/illusion-10-hvKtmoHwekDZ5iOH.htm)|Shadow Army|auto-trad|
|[necromancy-01-2gQYrCPwBmwau26O.htm](spells/necromancy-01-2gQYrCPwBmwau26O.htm)|Life Link|auto-trad|
|[necromancy-01-2iQKhCQBijhj5Rf3.htm](spells/necromancy-01-2iQKhCQBijhj5Rf3.htm)|Disrupting Weapons|auto-trad|
|[necromancy-01-4bFxUA59xeq6Snhw.htm](spells/necromancy-01-4bFxUA59xeq6Snhw.htm)|Hallowed Ground|auto-trad|
|[necromancy-01-7tp97g0UCJ9wOrd5.htm](spells/necromancy-01-7tp97g0UCJ9wOrd5.htm)|Withering Grasp|auto-trad|
|[necromancy-01-9u6X9ykhzG11NK1n.htm](spells/necromancy-01-9u6X9ykhzG11NK1n.htm)|Magic Stone|auto-trad|
|[necromancy-01-9WGeBwIIbbUuWKq0.htm](spells/necromancy-01-9WGeBwIIbbUuWKq0.htm)|Animate Dead|auto-trad|
|[necromancy-01-BH3sUerzMb2bWnv1.htm](spells/necromancy-01-BH3sUerzMb2bWnv1.htm)|Call of the Grave|auto-trad|
|[necromancy-01-CxpFy4HJHf4ACbxF.htm](spells/necromancy-01-CxpFy4HJHf4ACbxF.htm)|Putrefy Food and Drink|auto-trad|
|[necromancy-01-D6T17BdazhNy3KPm.htm](spells/necromancy-01-D6T17BdazhNy3KPm.htm)|Soul Siphon|auto-trad|
|[necromancy-01-DYdvMZ8G2LiSLVWw.htm](spells/necromancy-01-DYdvMZ8G2LiSLVWw.htm)|Spider Sting|auto-trad|
|[necromancy-01-ekGHLJSHGgWMUwkY.htm](spells/necromancy-01-ekGHLJSHGgWMUwkY.htm)|Touch of Corruption (Healing)|auto-trad|
|[necromancy-01-fAlzXtQAASaJx0mY.htm](spells/necromancy-01-fAlzXtQAASaJx0mY.htm)|Life Boost|auto-trad|
|[necromancy-01-FedTjedva2rYk33r.htm](spells/necromancy-01-FedTjedva2rYk33r.htm)|Undeath's Blessing|auto-trad|
|[necromancy-01-gSUQlTDYoLDGAsCP.htm](spells/necromancy-01-gSUQlTDYoLDGAsCP.htm)|Hymn of Healing|auto-trad|
|[necromancy-01-GYI4xloAgkm6tTrT.htm](spells/necromancy-01-GYI4xloAgkm6tTrT.htm)|Touch of Undeath|auto-trad|
|[necromancy-01-HG4afO9EOGEU9bZN.htm](spells/necromancy-01-HG4afO9EOGEU9bZN.htm)|Death's Call|auto-trad|
|[necromancy-01-IxyD7YdRbSSucxZp.htm](spells/necromancy-01-IxyD7YdRbSSucxZp.htm)|Lay on Hands (Vs. Undead)|auto-trad|
|[necromancy-01-J7Y7tl0bbdz7TcCc.htm](spells/necromancy-01-J7Y7tl0bbdz7TcCc.htm)|Ray of Enfeeblement|auto-trad|
|[necromancy-01-jFmWSIpJGGebim6y.htm](spells/necromancy-01-jFmWSIpJGGebim6y.htm)|Touch of Corruption|auto-trad|
|[necromancy-01-JUJYVbtNNFdvyrBS.htm](spells/necromancy-01-JUJYVbtNNFdvyrBS.htm)|Necromancer's Generosity|auto-trad|
|[necromancy-01-k34hDOfIIMAxNL4a.htm](spells/necromancy-01-k34hDOfIIMAxNL4a.htm)|Grim Tendrils|auto-trad|
|[necromancy-01-kcelf6IHl3L9VXXg.htm](spells/necromancy-01-kcelf6IHl3L9VXXg.htm)|Disrupt Undead|auto-trad|
|[necromancy-01-KIV2LqzS5KtqOItV.htm](spells/necromancy-01-KIV2LqzS5KtqOItV.htm)|Heal Companion|auto-trad|
|[necromancy-01-kvm68hVtmADiIvN4.htm](spells/necromancy-01-kvm68hVtmADiIvN4.htm)|Jealous Hex|auto-trad|
|[necromancy-01-lZx8jZfKrMEtyGY0.htm](spells/necromancy-01-lZx8jZfKrMEtyGY0.htm)|Rejuvenating Flames|auto-trad|
|[necromancy-01-mAMEt4FFbdqoRnkN.htm](spells/necromancy-01-mAMEt4FFbdqoRnkN.htm)|Chill Touch|auto-trad|
|[necromancy-01-NkeLctXo9FLGnDhi.htm](spells/necromancy-01-NkeLctXo9FLGnDhi.htm)|Divine Plagues|auto-trad|
|[necromancy-01-OJ91rm1FkJSlf3nk.htm](spells/necromancy-01-OJ91rm1FkJSlf3nk.htm)|Ancient Dust|auto-trad|
|[necromancy-01-qXTB7Ec9yYh5JPPV.htm](spells/necromancy-01-qXTB7Ec9yYh5JPPV.htm)|Purify Food and Drink|auto-trad|
|[necromancy-01-rhJyqB9g3ziImQgM.htm](spells/necromancy-01-rhJyqB9g3ziImQgM.htm)|Healer's Blessing|auto-trad|
|[necromancy-01-rVhHaWqUsVUO4GuY.htm](spells/necromancy-01-rVhHaWqUsVUO4GuY.htm)|Eject Soul|auto-trad|
|[necromancy-01-SdXFiQ4Py8761sNO.htm](spells/necromancy-01-SdXFiQ4Py8761sNO.htm)|Glutton's Jaw|auto-trad|
|[necromancy-01-SnjhtQYexDtNDdEg.htm](spells/necromancy-01-SnjhtQYexDtNDdEg.htm)|Stabilize|auto-trad|
|[necromancy-01-uToa7ksKAzmEpkKC.htm](spells/necromancy-01-uToa7ksKAzmEpkKC.htm)|Admonishing Ray|auto-trad|
|[necromancy-01-vQuwLqtFFYt0K15N.htm](spells/necromancy-01-vQuwLqtFFYt0K15N.htm)|Goodberry|auto-trad|
|[necromancy-01-WV2aMNN9DO5ZBjSv.htm](spells/necromancy-01-WV2aMNN9DO5ZBjSv.htm)|Flense|auto-trad|
|[necromancy-01-yHujiDQPdtXW797e.htm](spells/necromancy-01-yHujiDQPdtXW797e.htm)|Spirit Link|auto-trad|
|[necromancy-01-yp4w9SG4RuqRM8KD.htm](spells/necromancy-01-yp4w9SG4RuqRM8KD.htm)|Spirit Object|auto-trad|
|[necromancy-01-zHSp4PzoOE72DV4o.htm](spells/necromancy-01-zHSp4PzoOE72DV4o.htm)|Torturous Trauma|auto-trad|
|[necromancy-01-zJQnkKEKbJqGB3iB.htm](spells/necromancy-01-zJQnkKEKbJqGB3iB.htm)|Goblin Pox|auto-trad|
|[necromancy-01-Zmh4ynfnCtwKeAYl.htm](spells/necromancy-01-Zmh4ynfnCtwKeAYl.htm)|Heal Animal|auto-trad|
|[necromancy-01-zNN9212H2FGfM7VS.htm](spells/necromancy-01-zNN9212H2FGfM7VS.htm)|Lay on Hands|auto-trad|
|[necromancy-02-4Gl3WSUqYjVVIsOg.htm](spells/necromancy-02-4Gl3WSUqYjVVIsOg.htm)|Shadow Zombie|auto-trad|
|[necromancy-02-8ViwItUgwT4lOvvb.htm](spells/necromancy-02-8ViwItUgwT4lOvvb.htm)|False Life|auto-trad|
|[necromancy-02-BCuHKrDeJ4eq53M6.htm](spells/necromancy-02-BCuHKrDeJ4eq53M6.htm)|Remove Paralysis|auto-trad|
|[necromancy-02-c3b6LdLlQDPngNIb.htm](spells/necromancy-02-c3b6LdLlQDPngNIb.htm)|Create Undead|auto-trad|
|[necromancy-02-cwXiKPkZrIupjwlQ.htm](spells/necromancy-02-cwXiKPkZrIupjwlQ.htm)|Summoner's Precaution|auto-trad|
|[necromancy-02-d7Lwx6KAs47MtF0q.htm](spells/necromancy-02-d7Lwx6KAs47MtF0q.htm)|Shield Other|auto-trad|
|[necromancy-02-dLdRqT6UxTKlsPgp.htm](spells/necromancy-02-dLdRqT6UxTKlsPgp.htm)|Death Knell|auto-trad|
|[necromancy-02-dxOF7d5kAWusLKWF.htm](spells/necromancy-02-dxOF7d5kAWusLKWF.htm)|Reaper's Lantern|auto-trad|
|[necromancy-02-eSEmqOBuywoJ6tYd.htm](spells/necromancy-02-eSEmqOBuywoJ6tYd.htm)|Inner Radiance Torrent|auto-trad|
|[necromancy-02-Et8RSCLx8w7uOLvo.htm](spells/necromancy-02-Et8RSCLx8w7uOLvo.htm)|Restore Senses|auto-trad|
|[necromancy-02-fZPCv2VHuM2yPbC8.htm](spells/necromancy-02-fZPCv2VHuM2yPbC8.htm)|Deafness|auto-trad|
|[necromancy-02-H4oF5szC7aogqtvw.htm](spells/necromancy-02-H4oF5szC7aogqtvw.htm)|Worm's Repast|auto-trad|
|[necromancy-02-JhRuR7Jj3ViShpq7.htm](spells/necromancy-02-JhRuR7Jj3ViShpq7.htm)|Ghoulish Cravings|auto-trad|
|[necromancy-02-lOZhcvtej10TqlQm.htm](spells/necromancy-02-lOZhcvtej10TqlQm.htm)|Lifelink Surge|auto-trad|
|[necromancy-02-oNUyCqbpGWHifS02.htm](spells/necromancy-02-oNUyCqbpGWHifS02.htm)|Fungal Hyphae|auto-trad|
|[necromancy-02-oryfsRK27jAUnziw.htm](spells/necromancy-02-oryfsRK27jAUnziw.htm)|Imp Sting|auto-trad|
|[necromancy-02-qJZZdYBdNaWRJFER.htm](spells/necromancy-02-qJZZdYBdNaWRJFER.htm)|Wholeness of Body|auto-trad|
|[necromancy-02-RGBZrVRIEDb2G48h.htm](spells/necromancy-02-RGBZrVRIEDb2G48h.htm)|Soothing Mist|auto-trad|
|[necromancy-02-rthC6dGm3nNrt1xN.htm](spells/necromancy-02-rthC6dGm3nNrt1xN.htm)|Spectral Hand|auto-trad|
|[necromancy-02-Ru9v8U9IRk3LtWx8.htm](spells/necromancy-02-Ru9v8U9IRk3LtWx8.htm)|Feral Shades|auto-trad|
|[necromancy-02-S708JF3E0kuhuRzG.htm](spells/necromancy-02-S708JF3E0kuhuRzG.htm)|Bone Spray|auto-trad|
|[necromancy-02-siU9xRlqWXeKT0mH.htm](spells/necromancy-02-siU9xRlqWXeKT0mH.htm)|Feast of Ashes|auto-trad|
|[necromancy-02-SnaLVgxZ9ryUFmUr.htm](spells/necromancy-02-SnaLVgxZ9ryUFmUr.htm)|Restoration|auto-trad|
|[necromancy-02-WRmh1aKEo0dI0NJs.htm](spells/necromancy-02-WRmh1aKEo0dI0NJs.htm)|Corpse Communion|auto-trad|
|[necromancy-02-X3dYByf3YmkcdwG0.htm](spells/necromancy-02-X3dYByf3YmkcdwG0.htm)|Slough Skin|auto-trad|
|[necromancy-02-XEhzFKNTSARsofav.htm](spells/necromancy-02-XEhzFKNTSARsofav.htm)|Advanced Scurvy|auto-trad|
|[necromancy-02-XFtO4BBI22Uox2QP.htm](spells/necromancy-02-XFtO4BBI22Uox2QP.htm)|Sudden Blight|auto-trad|
|[necromancy-02-xlRv8vCTHh1NeMpw.htm](spells/necromancy-02-xlRv8vCTHh1NeMpw.htm)|Mimic Undead|auto-trad|
|[necromancy-02-xRgU9rrhmGAgG4Rc.htm](spells/necromancy-02-xRgU9rrhmGAgG4Rc.htm)|Gentle Repose|auto-trad|
|[necromancy-02-Xrz6wPXDBUr27izR.htm](spells/necromancy-02-Xrz6wPXDBUr27izR.htm)|Grave Impressions|auto-trad|
|[necromancy-02-zdNUbHqqZzjA07oM.htm](spells/necromancy-02-zdNUbHqqZzjA07oM.htm)|Boneshaker|auto-trad|
|[necromancy-03-0chL1b4OFIZxpN3v.htm](spells/necromancy-03-0chL1b4OFIZxpN3v.htm)|Steal Shadow|auto-trad|
|[necromancy-03-0JWyMwVnLxX9CDYQ.htm](spells/necromancy-03-0JWyMwVnLxX9CDYQ.htm)|Rouse Skeletons|auto-trad|
|[necromancy-03-10siFBMF4pIDhVmf.htm](spells/necromancy-03-10siFBMF4pIDhVmf.htm)|Cup of Dust|auto-trad|
|[necromancy-03-5OcZ3HBkrRFhSWCz.htm](spells/necromancy-03-5OcZ3HBkrRFhSWCz.htm)|Claim Curse|auto-trad|
|[necromancy-03-9gMQPCaFM27PEIh4.htm](spells/necromancy-03-9gMQPCaFM27PEIh4.htm)|Return the Favor|auto-trad|
|[necromancy-03-9iJaD4S7ptUeq5vO.htm](spells/necromancy-03-9iJaD4S7ptUeq5vO.htm)|Positive Attunement|auto-trad|
|[necromancy-03-aq76wpgsVEENDyau.htm](spells/necromancy-03-aq76wpgsVEENDyau.htm)|Shadow Spy|auto-trad|
|[necromancy-03-Cd6Crl4wpQaMSYrF.htm](spells/necromancy-03-Cd6Crl4wpQaMSYrF.htm)|Subjugate Undead|auto-trad|
|[necromancy-03-cqdmSmQnM0q6wbWG.htm](spells/necromancy-03-cqdmSmQnM0q6wbWG.htm)|Drain Life|auto-trad|
|[necromancy-03-gIVaSCrLhhBzGHQY.htm](spells/necromancy-03-gIVaSCrLhhBzGHQY.htm)|Reincarnate|auto-trad|
|[necromancy-03-GUeRTriJkMlMlVrk.htm](spells/necromancy-03-GUeRTriJkMlMlVrk.htm)|Bind Undead|auto-trad|
|[necromancy-03-gWmLT5SkA0qH2mNE.htm](spells/necromancy-03-gWmLT5SkA0qH2mNE.htm)|Envenom Companion|auto-trad|
|[necromancy-03-HIY4XUJZKmFLAJTn.htm](spells/necromancy-03-HIY4XUJZKmFLAJTn.htm)|Life Connection|auto-trad|
|[necromancy-03-M9TiCE1vlG1j2faM.htm](spells/necromancy-03-M9TiCE1vlG1j2faM.htm)|Martyr's Intervention|auto-trad|
|[necromancy-03-N1Z1oLPdBxaSgrEE.htm](spells/necromancy-03-N1Z1oLPdBxaSgrEE.htm)|Vampiric Touch|auto-trad|
|[necromancy-03-pSNLufPPsReKQtJR.htm](spells/necromancy-03-pSNLufPPsReKQtJR.htm)|Armor of Bones|auto-trad|
|[necromancy-03-RneiyehRO6f7LP44.htm](spells/necromancy-03-RneiyehRO6f7LP44.htm)|Remove Disease|auto-trad|
|[necromancy-03-SUKaxVZW2TlM8lu0.htm](spells/necromancy-03-SUKaxVZW2TlM8lu0.htm)|Neutralize Poison|auto-trad|
|[necromancy-03-VosLNn2M8S7JH67D.htm](spells/necromancy-03-VosLNn2M8S7JH67D.htm)|Blindness|auto-trad|
|[necromancy-03-vvbS69EHZuUTq0dr.htm](spells/necromancy-03-vvbS69EHZuUTq0dr.htm)|Life Pact|auto-trad|
|[necromancy-03-xabHqik5SoyDc5Db.htm](spells/necromancy-03-xabHqik5SoyDc5Db.htm)|Moth's Supper|auto-trad|
|[necromancy-03-ziHDISWkFSwz3pmn.htm](spells/necromancy-03-ziHDISWkFSwz3pmn.htm)|Delay Affliction|auto-trad|
|[necromancy-04-07xYlmGX32XtHGEt.htm](spells/necromancy-04-07xYlmGX32XtHGEt.htm)|Vampiric Maiden|auto-trad|
|[necromancy-04-0qA4MfMkFklOz2Lk.htm](spells/necromancy-04-0qA4MfMkFklOz2Lk.htm)|Fearful Feast|auto-trad|
|[necromancy-04-29p7NMY2OTpaINzt.htm](spells/necromancy-04-29p7NMY2OTpaINzt.htm)|Necrotic Radiation|auto-trad|
|[necromancy-04-2RhZkHNv8ajq0yLq.htm](spells/necromancy-04-2RhZkHNv8ajq0yLq.htm)|Fallow Field|auto-trad|
|[necromancy-04-4nHYSMHito1GUXlm.htm](spells/necromancy-04-4nHYSMHito1GUXlm.htm)|Rebuke Death|auto-trad|
|[necromancy-04-4y2DMq9DV5HvmnxC.htm](spells/necromancy-04-4y2DMq9DV5HvmnxC.htm)|Chroma Leach|auto-trad|
|[necromancy-04-5LrQIlimsPXK2xAG.htm](spells/necromancy-04-5LrQIlimsPXK2xAG.htm)|Steal Voice|auto-trad|
|[necromancy-04-5zeSxzDxMSr0wgSF.htm](spells/necromancy-04-5zeSxzDxMSr0wgSF.htm)|Sanguine Mist|auto-trad|
|[necromancy-04-6nTBr5XNuKOuPM5m.htm](spells/necromancy-04-6nTBr5XNuKOuPM5m.htm)|Foul Miasma|auto-trad|
|[necromancy-04-7yWXx3qC4eFNHhxD.htm](spells/necromancy-04-7yWXx3qC4eFNHhxD.htm)|Blight|auto-trad|
|[necromancy-04-8XuNn0h0rHE24m3B.htm](spells/necromancy-04-8XuNn0h0rHE24m3B.htm)|Positive Luminance|auto-trad|
|[necromancy-04-ba1RyDpwq6tfW8VM.htm](spells/necromancy-04-ba1RyDpwq6tfW8VM.htm)|Cloak of Light|auto-trad|
|[necromancy-04-bqx1tJeOZq1Ufhcc.htm](spells/necromancy-04-bqx1tJeOZq1Ufhcc.htm)|Recall Past Life|auto-trad|
|[necromancy-04-eexkxcqnkXazsGfK.htm](spells/necromancy-04-eexkxcqnkXazsGfK.htm)|Enervation|auto-trad|
|[necromancy-04-FCfd1zUrqPaLEtau.htm](spells/necromancy-04-FCfd1zUrqPaLEtau.htm)|Dirge of Remembrance|auto-trad|
|[necromancy-04-fH08MI4KP0KH2EQ9.htm](spells/necromancy-04-fH08MI4KP0KH2EQ9.htm)|Soothing Spring|auto-trad|
|[necromancy-04-FM3SmEW8N1FCRjqt.htm](spells/necromancy-04-FM3SmEW8N1FCRjqt.htm)|Talking Corpse|auto-trad|
|[necromancy-04-gfKhtVsXF3HKSdmY.htm](spells/necromancy-04-gfKhtVsXF3HKSdmY.htm)|Seal Fate|auto-trad|
|[necromancy-04-gzvRDpM6EvcfYHeu.htm](spells/necromancy-04-gzvRDpM6EvcfYHeu.htm)|Tireless Worker|auto-trad|
|[necromancy-04-hdzyDrRdHSse88XM.htm](spells/necromancy-04-hdzyDrRdHSse88XM.htm)|Extract Brain|auto-trad|
|[necromancy-04-ikSb3LRGnrwXJBVX.htm](spells/necromancy-04-ikSb3LRGnrwXJBVX.htm)|Vital Beacon|auto-trad|
|[necromancy-04-IT1aaqDBAISlHDUV.htm](spells/necromancy-04-IT1aaqDBAISlHDUV.htm)|Achaekek's Clutch|auto-trad|
|[necromancy-04-J5MNC4xq3CHH31qT.htm](spells/necromancy-04-J5MNC4xq3CHH31qT.htm)|Eradicate Undeath|auto-trad|
|[necromancy-04-Jli9WBjQZ2MmKJ8y.htm](spells/necromancy-04-Jli9WBjQZ2MmKJ8y.htm)|Spiritual Anamnesis|auto-trad|
|[necromancy-04-LVwmAH5NGvTuuQSU.htm](spells/necromancy-04-LVwmAH5NGvTuuQSU.htm)|Swarming Wasp Stings|auto-trad|
|[necromancy-04-MTNlvZ0A9xY5sOg1.htm](spells/necromancy-04-MTNlvZ0A9xY5sOg1.htm)|Rest Eternal|auto-trad|
|[necromancy-04-mTpPZIJ2sdgusPP1.htm](spells/necromancy-04-mTpPZIJ2sdgusPP1.htm)|Euphoric Renewal|auto-trad|
|[necromancy-04-nEXkk85BQu1cREa4.htm](spells/necromancy-04-nEXkk85BQu1cREa4.htm)|Garden of Death|auto-trad|
|[necromancy-04-OyFCwQuw8XRazsNr.htm](spells/necromancy-04-OyFCwQuw8XRazsNr.htm)|Remove Curse|auto-trad|
|[necromancy-04-PtX16vbWzDehj8qc.htm](spells/necromancy-04-PtX16vbWzDehj8qc.htm)|Pernicious Poltergeist|auto-trad|
|[necromancy-04-QE9f3OxvvBThymD4.htm](spells/necromancy-04-QE9f3OxvvBThymD4.htm)|Ectoplasmic Interstice|auto-trad|
|[necromancy-04-qzsQmpiQodHBBWYI.htm](spells/necromancy-04-qzsQmpiQodHBBWYI.htm)|Malignant Sustenance|auto-trad|
|[necromancy-04-RbORUmnwlB8b3mNf.htm](spells/necromancy-04-RbORUmnwlB8b3mNf.htm)|Internal Insurrection|auto-trad|
|[necromancy-04-S1Msrwi990FE7uMO.htm](spells/necromancy-04-S1Msrwi990FE7uMO.htm)|Call The Blood|auto-trad|
|[necromancy-04-ZeHeNQ5BNq6m5F1j.htm](spells/necromancy-04-ZeHeNQ5BNq6m5F1j.htm)|Take its Course|auto-trad|
|[necromancy-04-ZhJ8d9Uk4lwIx86b.htm](spells/necromancy-04-ZhJ8d9Uk4lwIx86b.htm)|Plant Growth|auto-trad|
|[necromancy-04-zvvHOQV78WKUB33l.htm](spells/necromancy-04-zvvHOQV78WKUB33l.htm)|Life Siphon|auto-trad|
|[necromancy-05-1bw6XJMOERcbC5Iq.htm](spells/necromancy-05-1bw6XJMOERcbC5Iq.htm)|Rip the Spirit|auto-trad|
|[necromancy-05-2YIr0S2Gt14PMMQp.htm](spells/necromancy-05-2YIr0S2Gt14PMMQp.htm)|Grasping Grave|auto-trad|
|[necromancy-05-3r897dYO8oYvuyn5.htm](spells/necromancy-05-3r897dYO8oYvuyn5.htm)|Summon Healing Servitor|auto-trad|
|[necromancy-05-59NR1hA2jPSgg2sW.htm](spells/necromancy-05-59NR1hA2jPSgg2sW.htm)|Blister|auto-trad|
|[necromancy-05-9WlTR9JlEcjRmGiD.htm](spells/necromancy-05-9WlTR9JlEcjRmGiD.htm)|Celestial Brand|auto-trad|
|[necromancy-05-CzjQtkRuRlzRvwzg.htm](spells/necromancy-05-CzjQtkRuRlzRvwzg.htm)|Healing Well|auto-trad|
|[necromancy-05-DgCS456mXKw97vNy.htm](spells/necromancy-05-DgCS456mXKw97vNy.htm)|Ode to Ouroboros|auto-trad|
|[necromancy-05-gsYEuWv04XTDxe91.htm](spells/necromancy-05-gsYEuWv04XTDxe91.htm)|Call Spirit|auto-trad|
|[necromancy-05-hghGRzOSzEl4UXdS.htm](spells/necromancy-05-hghGRzOSzEl4UXdS.htm)|Invoke Spirits|auto-trad|
|[necromancy-05-Hnc7eGi7vyZenAIm.htm](spells/necromancy-05-Hnc7eGi7vyZenAIm.htm)|Breath of Life|auto-trad|
|[necromancy-05-IoHxAkK0uGqrgtWl.htm](spells/necromancy-05-IoHxAkK0uGqrgtWl.htm)|Wyvern Sting|auto-trad|
|[necromancy-05-kqhPt9344UkcGVYO.htm](spells/necromancy-05-kqhPt9344UkcGVYO.htm)|Resurrect|auto-trad|
|[necromancy-05-MlpbeZ61Euhl0d60.htm](spells/necromancy-05-MlpbeZ61Euhl0d60.htm)|Cloudkill|auto-trad|
|[necromancy-05-nQS4vPm5zprqkzFZ.htm](spells/necromancy-05-nQS4vPm5zprqkzFZ.htm)|Curse of Death|auto-trad|
|[necromancy-05-PcmFpaHPCReNp1BD.htm](spells/necromancy-05-PcmFpaHPCReNp1BD.htm)|Over the Coals|auto-trad|
|[necromancy-05-pCvJ4yoZJxDtgUMI.htm](spells/necromancy-05-pCvJ4yoZJxDtgUMI.htm)|Restorative Moment|auto-trad|
|[necromancy-05-PgDFDvX64eswapSS.htm](spells/necromancy-05-PgDFDvX64eswapSS.htm)|Ectoplasmic Expulsion|auto-trad|
|[necromancy-05-qVXraYXlTjissCaG.htm](spells/necromancy-05-qVXraYXlTjissCaG.htm)|Mother's Blessing|auto-trad|
|[necromancy-05-rnFAHvKpcsU4BJD4.htm](spells/necromancy-05-rnFAHvKpcsU4BJD4.htm)|Shall not Falter, Shall not Rout|auto-trad|
|[necromancy-05-RWwiFnjxoJXn7H6D.htm](spells/necromancy-05-RWwiFnjxoJXn7H6D.htm)|Mind Swap|auto-trad|
|[necromancy-05-SzKkzq3Rr6vKIxbp.htm](spells/necromancy-05-SzKkzq3Rr6vKIxbp.htm)|Shepherd of Souls|auto-trad|
|[necromancy-05-VDWIZuLOJqwBthHc.htm](spells/necromancy-05-VDWIZuLOJqwBthHc.htm)|Ravening Maw|auto-trad|
|[necromancy-05-vJuaxTd6q11OjGqA.htm](spells/necromancy-05-vJuaxTd6q11OjGqA.htm)|Abyssal Plague|auto-trad|
|[necromancy-05-wYSLTNvmxbe78l2c.htm](spells/necromancy-05-wYSLTNvmxbe78l2c.htm)|Soulshelter Vessel|auto-trad|
|[necromancy-05-Y1YysjZ40ft0aLFN.htm](spells/necromancy-05-Y1YysjZ40ft0aLFN.htm)|Spiritual Torrent|auto-trad|
|[necromancy-05-z2mfh3oPnfYqXflY.htm](spells/necromancy-05-z2mfh3oPnfYqXflY.htm)|Mariner's Curse|auto-trad|
|[necromancy-05-Z60JRD78wT3afOEJ.htm](spells/necromancy-05-Z60JRD78wT3afOEJ.htm)|Portrait of Spite|auto-trad|
|[necromancy-05-ZLLY6ThJXCCrO0rL.htm](spells/necromancy-05-ZLLY6ThJXCCrO0rL.htm)|Wall of Flesh|auto-trad|
|[necromancy-06-6lO6uMQxbqmYho0e.htm](spells/necromancy-06-6lO6uMQxbqmYho0e.htm)|Necrotize|auto-trad|
|[necromancy-06-9kOI14Jep97TzGO7.htm](spells/necromancy-06-9kOI14Jep97TzGO7.htm)|Life-Giving Form|auto-trad|
|[necromancy-06-ayRXv0wQH00TTNZe.htm](spells/necromancy-06-ayRXv0wQH00TTNZe.htm)|Purple Worm Sting|auto-trad|
|[necromancy-06-B0Ng2VlhEJMuUXUH.htm](spells/necromancy-06-B0Ng2VlhEJMuUXUH.htm)|Bound in Death|auto-trad|
|[necromancy-06-fd31tAHSSGXyOxW6.htm](spells/necromancy-06-fd31tAHSSGXyOxW6.htm)|Vampiric Exsanguination|auto-trad|
|[necromancy-06-g1dDUKsIrdlpdqy9.htm](spells/necromancy-06-g1dDUKsIrdlpdqy9.htm)|Ravenous Darkness|auto-trad|
|[necromancy-06-GzN9bG6cKZ96YC6l.htm](spells/necromancy-06-GzN9bG6cKZ96YC6l.htm)|Claim Undead|auto-trad|
|[necromancy-06-IkGYwHRLhkuoGReG.htm](spells/necromancy-06-IkGYwHRLhkuoGReG.htm)|Raise Dead|auto-trad|
|[necromancy-06-PHVHBbdHeQRfjLmE.htm](spells/necromancy-06-PHVHBbdHeQRfjLmE.htm)|Spirit Blast|auto-trad|
|[necromancy-06-rTvNWcKNpOnGklGF.htm](spells/necromancy-06-rTvNWcKNpOnGklGF.htm)|Gray Shadow|auto-trad|
|[necromancy-06-x5rGOmhDRDVQPrnW.htm](spells/necromancy-06-x5rGOmhDRDVQPrnW.htm)|Field of Life|auto-trad|
|[necromancy-06-yUM5OYeMY8971b2S.htm](spells/necromancy-06-yUM5OYeMY8971b2S.htm)|Terminate Bloodline|auto-trad|
|[necromancy-06-Z82DvtSXafPmh4KV.htm](spells/necromancy-06-Z82DvtSXafPmh4KV.htm)|Shambling Horror|auto-trad|
|[necromancy-06-ZMvplR106Jxl7B15.htm](spells/necromancy-06-ZMvplR106Jxl7B15.htm)|Awaken Entropy|auto-trad|
|[necromancy-07-0jadeyQIItIuRgeH.htm](spells/necromancy-07-0jadeyQIItIuRgeH.htm)|Eclipse Burst|auto-trad|
|[necromancy-07-2Vkd1IxylPceUAAF.htm](spells/necromancy-07-2Vkd1IxylPceUAAF.htm)|Regenerate|auto-trad|
|[necromancy-07-JLdbyGKhjwAAoRLs.htm](spells/necromancy-07-JLdbyGKhjwAAoRLs.htm)|Tempest of Shades|auto-trad|
|[necromancy-07-QVMjPfXlpnmeuWKS.htm](spells/necromancy-07-QVMjPfXlpnmeuWKS.htm)|Leng Sting|auto-trad|
|[necromancy-07-u3pdkfmy0AWICdoM.htm](spells/necromancy-07-u3pdkfmy0AWICdoM.htm)|Ravenous Reanimation|auto-trad|
|[necromancy-07-wU6hNzK8Yfqdmc8m.htm](spells/necromancy-07-wU6hNzK8Yfqdmc8m.htm)|Possession|auto-trad|
|[necromancy-07-Z9OrRXKgAPv6Hn5l.htm](spells/necromancy-07-Z9OrRXKgAPv6Hn5l.htm)|Finger of Death|auto-trad|
|[necromancy-08-4MOew29Z1oCX8O28.htm](spells/necromancy-08-4MOew29Z1oCX8O28.htm)|Moment of Renewal|auto-trad|
|[necromancy-08-bRW61IbjaFea0wcv.htm](spells/necromancy-08-bRW61IbjaFea0wcv.htm)|Bathe in Blood|auto-trad|
|[necromancy-08-gtWxTfMbIN5RHQw6.htm](spells/necromancy-08-gtWxTfMbIN5RHQw6.htm)|All is One, One is All|auto-trad|
|[necromancy-08-Ht35SDf9PDStJfoC.htm](spells/necromancy-08-Ht35SDf9PDStJfoC.htm)|Spirit Song|auto-trad|
|[necromancy-08-it4wx2mDIJDlZGqS.htm](spells/necromancy-08-it4wx2mDIJDlZGqS.htm)|Divine Armageddon|auto-trad|
|[necromancy-08-M0jQlpQYUr0pp2Sv.htm](spells/necromancy-08-M0jQlpQYUr0pp2Sv.htm)|Horrid Wilting|auto-trad|
|[necromancy-08-MS60WhVifb45qORJ.htm](spells/necromancy-08-MS60WhVifb45qORJ.htm)|Spiritual Epidemic|auto-trad|
|[necromancy-08-Ovvflf5aFbmBxqq8.htm](spells/necromancy-08-Ovvflf5aFbmBxqq8.htm)|Quivering Palm|auto-trad|
|[necromancy-08-PgLvO8UNHSj5f61m.htm](spells/necromancy-08-PgLvO8UNHSj5f61m.htm)|Devour Life|auto-trad|
|[necromancy-09-10VcmSYNBrvBphu1.htm](spells/necromancy-09-10VcmSYNBrvBphu1.htm)|Massacre|auto-trad|
|[necromancy-09-drmvQJETA3WZzXyw.htm](spells/necromancy-09-drmvQJETA3WZzXyw.htm)|Voracious Gestalt|auto-trad|
|[necromancy-09-FEsuyf203wTNE2et.htm](spells/necromancy-09-FEsuyf203wTNE2et.htm)|Wail of the Banshee|auto-trad|
|[necromancy-09-GYmXvS9NJ7QwfWGg.htm](spells/necromancy-09-GYmXvS9NJ7QwfWGg.htm)|Bind Soul|auto-trad|
|[necromancy-09-LEMfFL551YB120RN.htm](spells/necromancy-09-LEMfFL551YB120RN.htm)|Blunt the Final Blade|auto-trad|
|[necromancy-09-pZr1xrCpaSu6qrXU.htm](spells/necromancy-09-pZr1xrCpaSu6qrXU.htm)|Clone|auto-trad|
|[necromancy-09-z39jFoNJrobyn3MQ.htm](spells/necromancy-09-z39jFoNJrobyn3MQ.htm)|Undertaker|auto-trad|
|[necromancy-09-ZMY58Yk5hnyfeE3q.htm](spells/necromancy-09-ZMY58Yk5hnyfeE3q.htm)|Linnorm Sting|auto-trad|
|[necromancy-10-HpIJTVqgXorH9X0L.htm](spells/necromancy-10-HpIJTVqgXorH9X0L.htm)|Revival|auto-trad|
|[necromancy-10-IGXGs9PlqUCvODcH.htm](spells/necromancy-10-IGXGs9PlqUCvODcH.htm)|Song of the Fallen|auto-trad|
|[necromancy-10-nuE9qsY2HfPFEgAo.htm](spells/necromancy-10-nuE9qsY2HfPFEgAo.htm)|Raga of Remembrance|auto-trad|
|[necromancy-10-uGXWkR2h8q9MRzEM.htm](spells/necromancy-10-uGXWkR2h8q9MRzEM.htm)|Hero's Defiance|auto-trad|
|[transmutation-01-1lmzILdCFENln8Cy.htm](spells/transmutation-01-1lmzILdCFENln8Cy.htm)|Physical Boost|auto-trad|
|[transmutation-01-8RWfKConLYFZpQ9X.htm](spells/transmutation-01-8RWfKConLYFZpQ9X.htm)|Wild Shape|auto-trad|
|[transmutation-01-aEM2cttJ2eYcLssW.htm](spells/transmutation-01-aEM2cttJ2eYcLssW.htm)|Fleet Step|auto-trad|
|[transmutation-01-AiWtiVmyasyL42J8.htm](spells/transmutation-01-AiWtiVmyasyL42J8.htm)|Crushing Ground|auto-trad|
|[transmutation-01-AUctDF2fqPZN2w4W.htm](spells/transmutation-01-AUctDF2fqPZN2w4W.htm)|Sigil|auto-trad|
|[transmutation-01-BA143r8fCqmSjdRf.htm](spells/transmutation-01-BA143r8fCqmSjdRf.htm)|Nettleskin|auto-trad|
|[transmutation-01-buhP7vfetUxQJlIJ.htm](spells/transmutation-01-buhP7vfetUxQJlIJ.htm)|Restyle|auto-trad|
|[transmutation-01-cOjlzWerBwbPWVkX.htm](spells/transmutation-01-cOjlzWerBwbPWVkX.htm)|Agile Feet|auto-trad|
|[transmutation-01-dINQzhqGmIsqGMUY.htm](spells/transmutation-01-dINQzhqGmIsqGMUY.htm)|Mending|auto-trad|
|[transmutation-01-EE7Q5BHIrfWNCPtT.htm](spells/transmutation-01-EE7Q5BHIrfWNCPtT.htm)|Magic Fang|auto-trad|
|[transmutation-01-F23T5tHPo3WsFiHW.htm](spells/transmutation-01-F23T5tHPo3WsFiHW.htm)|Quick Sort|auto-trad|
|[transmutation-01-gfPjmG6Fe6D3MFjl.htm](spells/transmutation-01-gfPjmG6Fe6D3MFjl.htm)|Pest Form|auto-trad|
|[transmutation-01-GUnw9YXaW3YyaCAU.htm](spells/transmutation-01-GUnw9YXaW3YyaCAU.htm)|Adapt Self|auto-trad|
|[transmutation-01-gwOYh5zMVZB0HNcT.htm](spells/transmutation-01-gwOYh5zMVZB0HNcT.htm)|Unimpeded Stride|auto-trad|
|[transmutation-01-i8PBZsnoCrK7IWph.htm](spells/transmutation-01-i8PBZsnoCrK7IWph.htm)|Tentacular Limbs|auto-trad|
|[transmutation-01-ifXNOhtmU4fKL68v.htm](spells/transmutation-01-ifXNOhtmU4fKL68v.htm)|Redact|auto-trad|
|[transmutation-01-Juk3cD5385Ftybct.htm](spells/transmutation-01-Juk3cD5385Ftybct.htm)|Verminous Lure|auto-trad|
|[transmutation-01-K8vvrOgW4bGakXxm.htm](spells/transmutation-01-K8vvrOgW4bGakXxm.htm)|Dragon Claws|auto-trad|
|[transmutation-01-KcLVELhCUcKXxiKE.htm](spells/transmutation-01-KcLVELhCUcKXxiKE.htm)|Longstrider|auto-trad|
|[transmutation-01-KFpBT6FPfSFhxQ27.htm](spells/transmutation-01-KFpBT6FPfSFhxQ27.htm)|Juvenile Companion|auto-trad|
|[transmutation-01-lbrWMnS2pecKaSVB.htm](spells/transmutation-01-lbrWMnS2pecKaSVB.htm)|Swampcall|auto-trad|
|[transmutation-01-lV8FkHZtzZu7Cy6j.htm](spells/transmutation-01-lV8FkHZtzZu7Cy6j.htm)|Evolution Surge|auto-trad|
|[transmutation-01-lXxP1ziyf4ozkpmv.htm](spells/transmutation-01-lXxP1ziyf4ozkpmv.htm)|Split the Tongue|auto-trad|
|[transmutation-01-m5QS8q5M6K0euKcT.htm](spells/transmutation-01-m5QS8q5M6K0euKcT.htm)|Healing Plaster|auto-trad|
|[transmutation-01-mFHQ2u4LWiejqKQG.htm](spells/transmutation-01-mFHQ2u4LWiejqKQG.htm)|Overstuff|auto-trad|
|[transmutation-01-nnSipUPNd3sm5vYL.htm](spells/transmutation-01-nnSipUPNd3sm5vYL.htm)|Vibrant Thorns|auto-trad|
|[transmutation-01-ps0nmhclT6aIXgd8.htm](spells/transmutation-01-ps0nmhclT6aIXgd8.htm)|Ki Rush|auto-trad|
|[transmutation-01-Q7QQ91vQtyi1Ux36.htm](spells/transmutation-01-Q7QQ91vQtyi1Ux36.htm)|Jump|auto-trad|
|[transmutation-01-rVANhQgB8Uqi9PTl.htm](spells/transmutation-01-rVANhQgB8Uqi9PTl.htm)|Animate Rope|auto-trad|
|[transmutation-01-s3abwDbTV43pGFFW.htm](spells/transmutation-01-s3abwDbTV43pGFFW.htm)|Shillelagh|auto-trad|
|[transmutation-01-sbTxe4CGP4tn6y51.htm](spells/transmutation-01-sbTxe4CGP4tn6y51.htm)|Lashing Rope|auto-trad|
|[transmutation-01-TFitdEOpQC4SzKQQ.htm](spells/transmutation-01-TFitdEOpQC4SzKQQ.htm)|Magic Weapon|auto-trad|
|[transmutation-01-UbHK19RYbxRXWgWX.htm](spells/transmutation-01-UbHK19RYbxRXWgWX.htm)|Temporal Distortion|auto-trad|
|[transmutation-01-UGJzJRJDoonfWqqI.htm](spells/transmutation-01-UGJzJRJDoonfWqqI.htm)|Athletic Rush|auto-trad|
|[transmutation-01-X8PSYw6WC2ePYSXd.htm](spells/transmutation-01-X8PSYw6WC2ePYSXd.htm)|Stumbling Curse|auto-trad|
|[transmutation-01-X9dkmh23lFwMjrYd.htm](spells/transmutation-01-X9dkmh23lFwMjrYd.htm)|Ant Haul|auto-trad|
|[transmutation-01-ZL8NTvB22NeEWhVG.htm](spells/transmutation-01-ZL8NTvB22NeEWhVG.htm)|Ki Strike|auto-trad|
|[transmutation-02-2qGqa33E4GPUCbMV.htm](spells/transmutation-02-2qGqa33E4GPUCbMV.htm)|Humanoid Form|auto-trad|
|[transmutation-02-5KobTMrZeZxuXMgl.htm](spells/transmutation-02-5KobTMrZeZxuXMgl.htm)|Spider Climb|auto-trad|
|[transmutation-02-6Ot4N22t5tPD51BO.htm](spells/transmutation-02-6Ot4N22t5tPD51BO.htm)|Knock|auto-trad|
|[transmutation-02-7OFKYR1VY6EXDuiR.htm](spells/transmutation-02-7OFKYR1VY6EXDuiR.htm)|Clawsong|auto-trad|
|[transmutation-02-aXoh6OQAL57lgh0a.htm](spells/transmutation-02-aXoh6OQAL57lgh0a.htm)|Expeditious Excavation|auto-trad|
|[transmutation-02-b6UnLNikoq2Std1f.htm](spells/transmutation-02-b6UnLNikoq2Std1f.htm)|Magic Warrior Aspect|auto-trad|
|[transmutation-02-CXICME10TkEJxz0P.htm](spells/transmutation-02-CXICME10TkEJxz0P.htm)|Shape Wood|auto-trad|
|[transmutation-02-dileJ0Yxqg76LMvu.htm](spells/transmutation-02-dileJ0Yxqg76LMvu.htm)|Tree Shape|auto-trad|
|[transmutation-02-fTK6ysisGhy3hRvz.htm](spells/transmutation-02-fTK6ysisGhy3hRvz.htm)|Rapid Adaptation|auto-trad|
|[transmutation-02-IhwREVWG0OzzrbWA.htm](spells/transmutation-02-IhwREVWG0OzzrbWA.htm)|Iron Gut|auto-trad|
|[transmutation-02-J6vNvrUT3b1hx2iA.htm](spells/transmutation-02-J6vNvrUT3b1hx2iA.htm)|Entangle|auto-trad|
|[transmutation-02-jW2asKFchuoxniSH.htm](spells/transmutation-02-jW2asKFchuoxniSH.htm)|Dismantle|auto-trad|
|[transmutation-02-mLAYvbafVKfBgEhz.htm](spells/transmutation-02-mLAYvbafVKfBgEhz.htm)|Loose Time's Arrow|auto-trad|
|[transmutation-02-MZGkMsPBztFN0pUO.htm](spells/transmutation-02-MZGkMsPBztFN0pUO.htm)|Water Breathing|auto-trad|
|[transmutation-02-PjhUmyKnq6K5uDby.htm](spells/transmutation-02-PjhUmyKnq6K5uDby.htm)|Shrink|auto-trad|
|[transmutation-02-rdTEF1hfAWbN58NE.htm](spells/transmutation-02-rdTEF1hfAWbN58NE.htm)|Enhance Victuals|auto-trad|
|[transmutation-02-Seaah9amXg70RKw2.htm](spells/transmutation-02-Seaah9amXg70RKw2.htm)|Water Walk|auto-trad|
|[transmutation-02-sLzPzk7DJnfuORJ0.htm](spells/transmutation-02-sLzPzk7DJnfuORJ0.htm)|Animate Object|auto-trad|
|[transmutation-02-tp4K7mYDL5MRHvJc.htm](spells/transmutation-02-tp4K7mYDL5MRHvJc.htm)|Magic Warrior Transformation|auto-trad|
|[transmutation-02-vb2dFNtbofJ7A9BW.htm](spells/transmutation-02-vb2dFNtbofJ7A9BW.htm)|Summoner's Visage|auto-trad|
|[transmutation-02-vctIUOOgSmxAF0KG.htm](spells/transmutation-02-vctIUOOgSmxAF0KG.htm)|Fear the Sun|auto-trad|
|[transmutation-02-vGEgI8e7AW6FQ3tP.htm](spells/transmutation-02-vGEgI8e7AW6FQ3tP.htm)|Animal Feature|auto-trad|
|[transmutation-02-wp09USMB3GIW1qbp.htm](spells/transmutation-02-wp09USMB3GIW1qbp.htm)|Animal Form|auto-trad|
|[transmutation-02-wzctak6BxOW8xvFV.htm](spells/transmutation-02-wzctak6BxOW8xvFV.htm)|Enlarge|auto-trad|
|[transmutation-03-0gdZrT9lwO17EIxc.htm](spells/transmutation-03-0gdZrT9lwO17EIxc.htm)|Ooze Form|auto-trad|
|[transmutation-03-0Rl3W7kiq9xVZRcr.htm](spells/transmutation-03-0Rl3W7kiq9xVZRcr.htm)|Time Pocket|auto-trad|
|[transmutation-03-8OWA91bgm5r6QPaH.htm](spells/transmutation-03-8OWA91bgm5r6QPaH.htm)|Dividing Trench|auto-trad|
|[transmutation-03-B3eLlbaPxOYHcs1o.htm](spells/transmutation-03-B3eLlbaPxOYHcs1o.htm)|Curse Of Lost Time|auto-trad|
|[transmutation-03-DgNOpb8H9MTAu9KL.htm](spells/transmutation-03-DgNOpb8H9MTAu9KL.htm)|Consecrate Flesh|auto-trad|
|[transmutation-03-gPvtmKMRpg9I9D7H.htm](spells/transmutation-03-gPvtmKMRpg9I9D7H.htm)|Earthbind|auto-trad|
|[transmutation-03-HcIAQZjNXHemoXSU.htm](spells/transmutation-03-HcIAQZjNXHemoXSU.htm)|Shifting Sand|auto-trad|
|[transmutation-03-ilGsyGLGjjIPHbyP.htm](spells/transmutation-03-ilGsyGLGjjIPHbyP.htm)|Embrace the Pit|auto-trad|
|[transmutation-03-kWh8sJH7yawidMyW.htm](spells/transmutation-03-kWh8sJH7yawidMyW.htm)|Shrink Item|auto-trad|
|[transmutation-03-LbqunTurwXB3u9Vp.htm](spells/transmutation-03-LbqunTurwXB3u9Vp.htm)|Time Skip|auto-trad|
|[transmutation-03-Llx0xKvtu8S4z6TI.htm](spells/transmutation-03-Llx0xKvtu8S4z6TI.htm)|Day's Weight|auto-trad|
|[transmutation-03-MNiT0dHol5fEcKlz.htm](spells/transmutation-03-MNiT0dHol5fEcKlz.htm)|Threefold Aspect|auto-trad|
|[transmutation-03-o6YCGx4lycsYpww4.htm](spells/transmutation-03-o6YCGx4lycsYpww4.htm)|Haste|auto-trad|
|[transmutation-03-RvBlSIJmxiqfCpR9.htm](spells/transmutation-03-RvBlSIJmxiqfCpR9.htm)|Feet to Fins|auto-trad|
|[transmutation-03-TUbXnR4RAuYzRx1u.htm](spells/transmutation-03-TUbXnR4RAuYzRx1u.htm)|Pyrotechnics|auto-trad|
|[transmutation-03-vh1RpbWfqdNC4L3P.htm](spells/transmutation-03-vh1RpbWfqdNC4L3P.htm)|Meld into Stone|auto-trad|
|[transmutation-03-WsUwpfmhKrKwoIe3.htm](spells/transmutation-03-WsUwpfmhKrKwoIe3.htm)|Slow|auto-trad|
|[transmutation-04-0fYE64odlKqISzft.htm](spells/transmutation-04-0fYE64odlKqISzft.htm)|Rusting Grasp|auto-trad|
|[transmutation-04-1NLMPmyCB2MBoCuR.htm](spells/transmutation-04-1NLMPmyCB2MBoCuR.htm)|Tortoise and the Hare|auto-trad|
|[transmutation-04-8rj45fKzCFcB0fxs.htm](spells/transmutation-04-8rj45fKzCFcB0fxs.htm)|Enlarge Companion|auto-trad|
|[transmutation-04-A2JfEKe6BZcTG1S8.htm](spells/transmutation-04-A2JfEKe6BZcTG1S8.htm)|Fly|auto-trad|
|[transmutation-04-APTMURAW1N0Wpk4w.htm](spells/transmutation-04-APTMURAW1N0Wpk4w.htm)|Precious Metals|auto-trad|
|[transmutation-04-b5sGjGlBf58f8jn0.htm](spells/transmutation-04-b5sGjGlBf58f8jn0.htm)|Air Walk|auto-trad|
|[transmutation-04-bKDsmKVosexwJ80i.htm](spells/transmutation-04-bKDsmKVosexwJ80i.htm)|Mantis Form|auto-trad|
|[transmutation-04-cBUuG1yJHGeKffpg.htm](spells/transmutation-04-cBUuG1yJHGeKffpg.htm)|Localized Quake|auto-trad|
|[transmutation-04-CHZQJg7O7991Vl4m.htm](spells/transmutation-04-CHZQJg7O7991Vl4m.htm)|Familiar Form|auto-trad|
|[transmutation-04-cOwSsSXRsBaXUvlr.htm](spells/transmutation-04-cOwSsSXRsBaXUvlr.htm)|Stasis|auto-trad|
|[transmutation-04-D6BcAoWFxHYTKwVZ.htm](spells/transmutation-04-D6BcAoWFxHYTKwVZ.htm)|Morass of Ages|auto-trad|
|[transmutation-04-DcmWrD0V5PWQQyDm.htm](spells/transmutation-04-DcmWrD0V5PWQQyDm.htm)|Variable Gravity|auto-trad|
|[transmutation-04-e36Z2t6tLdW3RUzZ.htm](spells/transmutation-04-e36Z2t6tLdW3RUzZ.htm)|Elemental Gift|auto-trad|
|[transmutation-04-eb4FXf62NYArTqek.htm](spells/transmutation-04-eb4FXf62NYArTqek.htm)|Artistic Flourish|auto-trad|
|[transmutation-04-gSFg9zKwgcNZLMEs.htm](spells/transmutation-04-gSFg9zKwgcNZLMEs.htm)|Stormwind Flight|auto-trad|
|[transmutation-04-L8pzCOi7Jzx5ALs9.htm](spells/transmutation-04-L8pzCOi7Jzx5ALs9.htm)|Disperse into Air|auto-trad|
|[transmutation-04-McnPlLFvKtQVXNcG.htm](spells/transmutation-04-McnPlLFvKtQVXNcG.htm)|Shape Stone|auto-trad|
|[transmutation-04-mSM659xN2VIAHiF3.htm](spells/transmutation-04-mSM659xN2VIAHiF3.htm)|Wordsmith|auto-trad|
|[transmutation-04-oiUhJbJ3YCKF62Fu.htm](spells/transmutation-04-oiUhJbJ3YCKF62Fu.htm)|Darkened Eyes|auto-trad|
|[transmutation-04-oND3oFBjxrhyVvyG.htm](spells/transmutation-04-oND3oFBjxrhyVvyG.htm)|Community Repair|auto-trad|
|[transmutation-04-Pd2M1XY8EXrSfWgJ.htm](spells/transmutation-04-Pd2M1XY8EXrSfWgJ.htm)|Swarm Form|auto-trad|
|[transmutation-04-Qu4IThrk1wpONwjT.htm](spells/transmutation-04-Qu4IThrk1wpONwjT.htm)|Fey Form|auto-trad|
|[transmutation-04-SDkIFrrO1PsE02Kd.htm](spells/transmutation-04-SDkIFrrO1PsE02Kd.htm)|Shifting Form|auto-trad|
|[transmutation-04-V8wXOsoejQhe6CyG.htm](spells/transmutation-04-V8wXOsoejQhe6CyG.htm)|Gaseous Form|auto-trad|
|[transmutation-04-VuPDHoVEPLbMfCJC.htm](spells/transmutation-04-VuPDHoVEPLbMfCJC.htm)|Bestial Curse|auto-trad|
|[transmutation-04-zjYQyuBNT5VjV4mz.htm](spells/transmutation-04-zjYQyuBNT5VjV4mz.htm)|Elephant Form|auto-trad|
|[transmutation-05-1aXX52MnVbBvT9S7.htm](spells/transmutation-05-1aXX52MnVbBvT9S7.htm)|Establish Nexus|auto-trad|
|[transmutation-05-1K6AYGisvo9gqdhs.htm](spells/transmutation-05-1K6AYGisvo9gqdhs.htm)|Elemental Form|auto-trad|
|[transmutation-05-3E1p58MNQMO4GAxt.htm](spells/transmutation-05-3E1p58MNQMO4GAxt.htm)|Stagnate Time|auto-trad|
|[transmutation-05-5tpk4Q2QI3FVhm99.htm](spells/transmutation-05-5tpk4Q2QI3FVhm99.htm)|Rewinding Step|auto-trad|
|[transmutation-05-92JNBuY5pfFmywOd.htm](spells/transmutation-05-92JNBuY5pfFmywOd.htm)|Desperate Repair|auto-trad|
|[transmutation-05-9o5aG5025ZczjkPb.htm](spells/transmutation-05-9o5aG5025ZczjkPb.htm)|Untwisting Iron Roots|auto-trad|
|[transmutation-05-B3tbO85GBpzQ3u8l.htm](spells/transmutation-05-B3tbO85GBpzQ3u8l.htm)|Wish-Twisted Form|auto-trad|
|[transmutation-05-CcXtbTxtdV9MKQfu.htm](spells/transmutation-05-CcXtbTxtdV9MKQfu.htm)|Mantle of the Frozen Heart|auto-trad|
|[transmutation-05-d9sBzPOXX3KT8uTu.htm](spells/transmutation-05-d9sBzPOXX3KT8uTu.htm)|Grisly Growths|auto-trad|
|[transmutation-05-DcUGLLyaa9tHH1kN.htm](spells/transmutation-05-DcUGLLyaa9tHH1kN.htm)|Incarnate Ancestry|auto-trad|
|[transmutation-05-ddKBoCjmSyPSHcws.htm](spells/transmutation-05-ddKBoCjmSyPSHcws.htm)|Unusual Anatomy|auto-trad|
|[transmutation-05-Fu8Ml47ZfXpSYe7E.htm](spells/transmutation-05-Fu8Ml47ZfXpSYe7E.htm)|Aberrant Form|auto-trad|
|[transmutation-05-HWJODX2zPg5cg34F.htm](spells/transmutation-05-HWJODX2zPg5cg34F.htm)|Dragon Wings|auto-trad|
|[transmutation-05-j2kfNBFpLa0JjVr8.htm](spells/transmutation-05-j2kfNBFpLa0JjVr8.htm)|Mantle of the Magma Heart|auto-trad|
|[transmutation-05-kRxlkPPe6Gr7Du59.htm](spells/transmutation-05-kRxlkPPe6Gr7Du59.htm)|Wind Jump|auto-trad|
|[transmutation-05-lETZeqBPoj2htGVk.htm](spells/transmutation-05-lETZeqBPoj2htGVk.htm)|Quicken Time|auto-trad|
|[transmutation-05-LzfrBDxxPTiuN7uL.htm](spells/transmutation-05-LzfrBDxxPTiuN7uL.htm)|Transmute Rock And Mud|auto-trad|
|[transmutation-05-y5pzaNfb17CM1slC.htm](spells/transmutation-05-y5pzaNfb17CM1slC.htm)|Blackfinger's Blades|auto-trad|
|[transmutation-05-zCcfPS4y5SrZzU2x.htm](spells/transmutation-05-zCcfPS4y5SrZzU2x.htm)|Plant Form|auto-trad|
|[transmutation-06-29AyhknPKiDBcy8s.htm](spells/transmutation-06-29AyhknPKiDBcy8s.htm)|Statuette|auto-trad|
|[transmutation-06-3KiM09e8DN9AdTPA.htm](spells/transmutation-06-3KiM09e8DN9AdTPA.htm)|Hag's Fruit|auto-trad|
|[transmutation-06-5c692cCcTDXjSEzk.htm](spells/transmutation-06-5c692cCcTDXjSEzk.htm)|Dragon Form|auto-trad|
|[transmutation-06-aSDfGcUOUVGU5m1g.htm](spells/transmutation-06-aSDfGcUOUVGU5m1g.htm)|Daemon Form|auto-trad|
|[transmutation-06-dKbv80rlAWVpz83C.htm](spells/transmutation-06-dKbv80rlAWVpz83C.htm)|Demon Form|auto-trad|
|[transmutation-06-dN8QBNuTiaBHCKUe.htm](spells/transmutation-06-dN8QBNuTiaBHCKUe.htm)|Baleful Polymorph|auto-trad|
|[transmutation-06-dR53f0p15EW7D0xC.htm](spells/transmutation-06-dR53f0p15EW7D0xC.htm)|Devil Form|auto-trad|
|[transmutation-06-FKtPRWcs4zhwn9N1.htm](spells/transmutation-06-FKtPRWcs4zhwn9N1.htm)|Form of the Sandpoint Devil|auto-trad|
|[transmutation-06-MMCQsh12TPaDdPbV.htm](spells/transmutation-06-MMCQsh12TPaDdPbV.htm)|Righteous Might|auto-trad|
|[transmutation-06-NCUXsaj2yFxOVhrK.htm](spells/transmutation-06-NCUXsaj2yFxOVhrK.htm)|Create Skinstitch|auto-trad|
|[transmutation-06-Pr1ruNTbzGn3H9w5.htm](spells/transmutation-06-Pr1ruNTbzGn3H9w5.htm)|Stone to Flesh|auto-trad|
|[transmutation-06-vF52ktg0wUIAlf57.htm](spells/transmutation-06-vF52ktg0wUIAlf57.htm)|Mantle of Heaven's Slopes|auto-trad|
|[transmutation-06-xEjGEBvTfDJECSki.htm](spells/transmutation-06-xEjGEBvTfDJECSki.htm)|Ancestral Form|auto-trad|
|[transmutation-06-YtBZq49N4Um1cwm7.htm](spells/transmutation-06-YtBZq49N4Um1cwm7.htm)|Nature's Reprisal|auto-trad|
|[transmutation-06-zhqnMOVPzVvWSUbC.htm](spells/transmutation-06-zhqnMOVPzVvWSUbC.htm)|Tempest Form|auto-trad|
|[transmutation-06-znv4ECL7ZtuiagtA.htm](spells/transmutation-06-znv4ECL7ZtuiagtA.htm)|Flesh to Stone|auto-trad|
|[transmutation-07-3MzuRDc7ccylpW2e.htm](spells/transmutation-07-3MzuRDc7ccylpW2e.htm)|Hasted Assault|auto-trad|
|[transmutation-07-8eOMAIvT7S1CJSit.htm](spells/transmutation-07-8eOMAIvT7S1CJSit.htm)|Untwisting Iron Augmentation|auto-trad|
|[transmutation-07-9aFg4EuOBVz0i3Lb.htm](spells/transmutation-07-9aFg4EuOBVz0i3Lb.htm)|Corrosive Body|auto-trad|
|[transmutation-07-E3zIZ4pjlOuWeGRz.htm](spells/transmutation-07-E3zIZ4pjlOuWeGRz.htm)|Angel Form|auto-trad|
|[transmutation-07-hiVL8qsnTJtpouw0.htm](spells/transmutation-07-hiVL8qsnTJtpouw0.htm)|Divine Vessel|auto-trad|
|[transmutation-07-IHUs4qn27RrdFQ5Y.htm](spells/transmutation-07-IHUs4qn27RrdFQ5Y.htm)|Cosmic Form|auto-trad|
|[transmutation-07-Ww4cPZ3QHTSpaM1m.htm](spells/transmutation-07-Ww4cPZ3QHTSpaM1m.htm)|Unfolding Wind Blitz|auto-trad|
|[transmutation-07-XS7Wyh5YC0NWeWyB.htm](spells/transmutation-07-XS7Wyh5YC0NWeWyB.htm)|Fiery Body|auto-trad|
|[transmutation-08-U0hL0LLaprcnAyzC.htm](spells/transmutation-08-U0hL0LLaprcnAyzC.htm)|Wind Walk|auto-trad|
|[transmutation-08-wTYxxYJWN348oV15.htm](spells/transmutation-08-wTYxxYJWN348oV15.htm)|Medusa's Wrath|auto-trad|
|[transmutation-09-pswdik31kuHEvdno.htm](spells/transmutation-09-pswdik31kuHEvdno.htm)|Shapechange|auto-trad|
|[transmutation-09-TbYqDhlNRiWHe146.htm](spells/transmutation-09-TbYqDhlNRiWHe146.htm)|One With the Land|auto-trad|
|[transmutation-09-YDMOqndvYFu3OjA6.htm](spells/transmutation-09-YDMOqndvYFu3OjA6.htm)|Ki Form|auto-trad|
|[transmutation-10-1dsahW4g1ggXtypx.htm](spells/transmutation-10-1dsahW4g1ggXtypx.htm)|Time Stop|auto-trad|
|[transmutation-10-ckUOoqOM7Kg7VqxB.htm](spells/transmutation-10-ckUOoqOM7Kg7VqxB.htm)|Avatar|auto-trad|
|[transmutation-10-pmP8HhXvvEKP3LqU.htm](spells/transmutation-10-pmP8HhXvvEKP3LqU.htm)|Primal Herd|auto-trad|
|[transmutation-10-qqQYrXaRJXr7uc4i.htm](spells/transmutation-10-qqQYrXaRJXr7uc4i.htm)|Apex Companion|auto-trad|
|[transmutation-10-vQMAdnIwnV9prPiG.htm](spells/transmutation-10-vQMAdnIwnV9prPiG.htm)|Element Embodied|auto-trad|
|[transmutation-10-ZXwxs5tRjEGrjAJT.htm](spells/transmutation-10-ZXwxs5tRjEGrjAJT.htm)|Nature Incarnate|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[abjuration-01-dhpXpzt7TCm8TbHM.htm](spells/abjuration-01-dhpXpzt7TCm8TbHM.htm)|Thermal Stasis|Thermal Stasis|modificada|
|[abjuration-01-lY9fOk1qBDDhBT8s.htm](spells/abjuration-01-lY9fOk1qBDDhBT8s.htm)|Protective Ward|Custodia protectora|modificada|
|[abjuration-05-ZAX0OOcKtYMQlquR.htm](spells/abjuration-05-ZAX0OOcKtYMQlquR.htm)|Symphony of the Unfettered Heart|Symphony of the Unfettered Heart|modificada|
|[conjuration-01-F1nlmqOIucch3Cmt.htm](spells/conjuration-01-F1nlmqOIucch3Cmt.htm)|Pet Cache|Escondrijo para mascotas|modificada|
|[conjuration-02-3VxVbZqIRvpKkg3O.htm](spells/conjuration-02-3VxVbZqIRvpKkg3O.htm)|Fungal Infestation|Infestación fúngica|modificada|
|[conjuration-04-h7RtKSKGViNtD5o4.htm](spells/conjuration-04-h7RtKSKGViNtD5o4.htm)|Coral Eruption|Erupción de Coral|modificada|
|[divination-01-EUMjrJJwSgsqNidi.htm](spells/divination-01-EUMjrJJwSgsqNidi.htm)|Anticipate Peril|Anticipate Peril|modificada|
|[divination-03-2jWVNdVlbJq84dfT.htm](spells/divination-03-2jWVNdVlbJq84dfT.htm)|Battlefield Persistence|Persistencia del campo de batalla|modificada|
|[enchantment-01-xn0V2HDrmDWNzPEt.htm](spells/enchantment-01-xn0V2HDrmDWNzPEt.htm)|Savor the Sting|Saborear el aguijón|modificada|
|[evocation-01-8Uzc9WKqRr755S5d.htm](spells/evocation-01-8Uzc9WKqRr755S5d.htm)|Horizon Thunder Sphere|Horizonte Tronante Esfera|modificada|
|[evocation-01-O9w7r4BKgPogYDDe.htm](spells/evocation-01-O9w7r4BKgPogYDDe.htm)|Produce Flame|Produce Flamígera|modificada|
|[evocation-02-f8hRqLJaxBVhF1u0.htm](spells/evocation-02-f8hRqLJaxBVhF1u0.htm)|Acid Arrow|Flecha ácida|modificada|
|[evocation-03-B8aCUMCHCIMUCEVK.htm](spells/evocation-03-B8aCUMCHCIMUCEVK.htm)|Elemental Motion|Movimiento elemental|modificada|
|[evocation-03-DyiD239dNS7RIxZE.htm](spells/evocation-03-DyiD239dNS7RIxZE.htm)|Searing Light|Luz abrasadora|modificada|
|[evocation-03-kvZCQz4NoMAfjvif.htm](spells/evocation-03-kvZCQz4NoMAfjvif.htm)|Gasping Marsh|Gasping Marsh|modificada|
|[evocation-03-OTd17oXwJH9qb1cS.htm](spells/evocation-03-OTd17oXwJH9qb1cS.htm)|Incendiary Ashes|Cenizas incendiarias|modificada|
|[evocation-09-jrBa9deU2ULFWvSl.htm](spells/evocation-09-jrBa9deU2ULFWvSl.htm)|Meteor Swarm|Enjambre de meteoritos|modificada|
|[illusion-01-R8bqnYiThB6MYTxD.htm](spells/illusion-01-R8bqnYiThB6MYTxD.htm)|Phantom Pain|Dolor fantasmal|modificada|
|[illusion-02-avT46uIH3xYJPSv4.htm](spells/illusion-02-avT46uIH3xYJPSv4.htm)|Teeth to Terror|Dientes para aterrar|modificada|
|[necromancy-01-rfZpqmj0AIIdkVIs.htm](spells/necromancy-01-rfZpqmj0AIIdkVIs.htm)|Heal|Curar|modificada|
|[necromancy-01-wdA52JJnsuQWeyqz.htm](spells/necromancy-01-wdA52JJnsuQWeyqz.htm)|Harm|Dar|modificada|
|[necromancy-02-TaaMEYdZXQXF0Sks.htm](spells/necromancy-02-TaaMEYdZXQXF0Sks.htm)|Blood Vendetta|Vendetta de sangre|modificada|
|[necromancy-04-VXUrO8TwRqBpNzdU.htm](spells/necromancy-04-VXUrO8TwRqBpNzdU.htm)|Bloodspray Curse|Bloodspray Curse|modificada|
|[necromancy-05-ES6FkwXXqYr4ujQH.htm](spells/necromancy-05-ES6FkwXXqYr4ujQH.htm)|Blood Feast|Festín de sangre|modificada|
|[necromancy-05-tf4PMMMzR5xxJDun.htm](spells/necromancy-05-tf4PMMMzR5xxJDun.htm)|Cleansing Flames|Cleansing Flamígera|modificada|
|[transmutation-01-0xR9vrt6uDFl0Umo.htm](spells/transmutation-01-0xR9vrt6uDFl0Umo.htm)|Wild Morph|Morfismo salvaje|modificada|
|[transmutation-01-MPxbKoR54gkYkqLO.htm](spells/transmutation-01-MPxbKoR54gkYkqLO.htm)|Gouging Claw|Gouging Claw|modificada|
|[transmutation-03-AMEu5zzLN7uCX645.htm](spells/transmutation-03-AMEu5zzLN7uCX645.htm)|Ghostly Weapon|Arma fantasmal|modificada|
|[transmutation-03-KktHf7zIAWOr499h.htm](spells/transmutation-03-KktHf7zIAWOr499h.htm)|Ranger's Bramble|Ranger's Bramble|modificada|
|[transmutation-03-qJGW6BbIcU6sfA1d.htm](spells/transmutation-03-qJGW6BbIcU6sfA1d.htm)|Time Jump|Salto en el tiempo|modificada|
|[transmutation-03-XI6Lzd2B5pernkPd.htm](spells/transmutation-03-XI6Lzd2B5pernkPd.htm)|Insect Form|Forma de insecto|modificada|
|[transmutation-04-KhM8MhoUgoUjBMIz.htm](spells/transmutation-04-KhM8MhoUgoUjBMIz.htm)|Dinosaur Form|Forma de dinosaurio|modificada|
|[transmutation-04-NzXpEzcZAjuDTZjK.htm](spells/transmutation-04-NzXpEzcZAjuDTZjK.htm)|Aerial Form|Forma aérea|modificada|
|[transmutation-05-YtesyvfAIwXOqISq.htm](spells/transmutation-05-YtesyvfAIwXOqISq.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[transmutation-06-2B0C22OuX9YrIJ5y.htm](spells/transmutation-06-2B0C22OuX9YrIJ5y.htm)|Ash Form|Forma de Ceniza|modificada|
|[transmutation-08-8AMvNVOUEtxBCDvJ.htm](spells/transmutation-08-8AMvNVOUEtxBCDvJ.htm)|Monstrosity Form|Forma monstruosa|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
